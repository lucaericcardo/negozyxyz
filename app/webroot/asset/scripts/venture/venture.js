(function() {
    'use strict';
    angular.module('venture', ['ngRoute', 'ngAnimate', 'ui.bootstrap','venture.ctrls','venture.services','shared.directives','shared.services','ngActivityIndicator','angularFileUpload','top_bar.services','top_bar.ctrls','toaster']).config([
        '$routeProvider', function($routeProvider) {
             return $routeProvider.when('/', {
             redirectTo: '/'
             }).when('/', {
                templateUrl: '/view/ventures/list.html'
             }).when('/venture/:order_uid', {
                 templateUrl: '/view/ventures/details.html'
             })
        }
    ]);
}).call(this);
