(function() {
    'use strict';
    angular.module('venture.services', [])
        .factory("ventures",function($http){
            return {
                loadVentures: function(){
                    return $http.get('/account/loadUser.json');
                }
            }
        })
}).call(this);
