(function() {
    'use strict';
    angular.module('site.services', []).factory('product',function($http){
        return {
            list: function() {
                return $http.get('/site/loadProducts.json');
            },
            all: function() {
                return $http.get('/site/loadAllProducts.json');
            },
            listCategory: function(collection_id){
                return $http.get('/site/loadProducts/'+collection_id+'.json');
            },
            listSearch: function(q){
                return $http.get('/site/searchProduct/'+q+'.json');
            },
            product: function(url){
                return $http.get('/site/loadProduct/'+url+'.json');
            },
            handleBuy: function(product_id, require_quantity) {
                return $http.post('/site/handleBuy.json', {product_id : product_id, require_quantity : require_quantity});
            },
            addToCart: function(product_id,quantity){
                return $http.post('/carts/addProductToCart.json', {product_id : product_id,quantity:quantity});
            },
            loadCart: function(){
                return $http.get('/carts/loadProduct.json');
            },
            setData: function(cart){
                return $http.post('/carts/setData.json', {cart: cart});
            },
            loadReviewData: function() {
                return $http.post('/carts/loadReviewData.json');
            },
            saveOrder: function(){
                return $http.post('/carts/saveOrder.json');
            }
        }
    })
}).call(this);
