(function() {
    'use strict';
    angular.module('site.controllers', [])
        .controller('HomeCtrl', function($scope,product,$activityIndicator){
            $activityIndicator.startAnimating();
            $(".smoothscroll").mCustomScrollbar({
                advanced: {
                    updateOnContentResize: true
                },
                scrollButtons: {
                    enable: false
                },
                mouseWheelPixels: "100",
                theme: "dark-2"
            });
            $scope.products = [];
            $scope.shop = {};
            $scope.searchProduct = function(){
                if($scope.product_q) {
                    window.location.href = "#/search/" + $scope.product_q + "/";
                }
            }
            product.list($scope.limit,$scope.page).success(function(r){
                $scope.products = r.products;
                $scope.shop = r.shop;
                $activityIndicator.stopAnimating();
            });
            $scope.selected = void 0;
            $scope.states = [];
		})
        .controller('AllCtrl', function($scope,product,$activityIndicator){
            $activityIndicator.startAnimating();
            $(".smoothscroll").mCustomScrollbar({
                advanced: {
                    updateOnContentResize: true
                },
                scrollButtons: {
                    enable: false
                },
                mouseWheelPixels: "100",
                theme: "dark-2"
            });
            $scope.products = [];
            $scope.shop = {};
            $scope.searchProduct = function(){
                if($scope.product_q) {
                    window.location.href = "#/search/" + $scope.product_q + "/";
                }
            }
            product.all($scope.limit,$scope.page).success(function(r){
                $scope.products = r.products;
                $scope.shop = r.shop;
                $activityIndicator.stopAnimating();
            });
            $scope.selected = void 0;
            $scope.states = [];
        })
        .controller('CategoryCtrl', function($scope,$routeParams,product,$activityIndicator){
            $activityIndicator.startAnimating();
            $(".smoothscroll").mCustomScrollbar({
                advanced: {
                    updateOnContentResize: true
                },
                scrollButtons: {
                    enable: false
                },
                mouseWheelPixels: "100",
                theme: "dark-2"
            });
            $scope.products = [];
            $scope.category = "";
            $scope.searchProduct = function(){
                if($scope.product_q) {
                    window.location.href = "#/search/" + $scope.product_q + "/";
                }
            }
            product.listCategory($routeParams.category_id).success(function(r){
                $scope.products = r.products;
                $scope.category = r.category;
                $scope.shop = r.shop;
                $activityIndicator.stopAnimating();
            });
            $scope.selected = void 0;
            $scope.states = [];
        })
        .controller('CollectionCtrl', function($scope,$routeParams,product,$activityIndicator){
            $activityIndicator.startAnimating();
            $(".smoothscroll").mCustomScrollbar({
                advanced: {
                    updateOnContentResize: true
                },
                scrollButtons: {
                    enable: false
                },
                mouseWheelPixels: "100",
                theme: "dark-2"
            });
            $scope.products = [];
            $scope.category = "";
            product.listCategory($routeParams.category_id).success(function(r){
                $scope.products = r.products;
                $scope.shop = r.shop;
                $activityIndicator.stopAnimating();
            });
            $scope.selected = void 0;
            $scope.states = [];
        })
        .controller('SearchCtrl', function($scope,$routeParams,product,$activityIndicator){
            $(".smoothscroll").mCustomScrollbar({
                advanced: {
                    updateOnContentResize: true
                },
                scrollButtons: {
                    enable: false
                },
                mouseWheelPixels: "100",
                theme: "dark-2"
            });
            $scope.products = [];
            $scope.category = "";
            $scope.product_q = $routeParams.q;
            $scope.searchProduct = function(){
                if($scope.product_q) {
                    window.location.href = "#/search/" + $scope.product_q + "/";
                }
            }
            product.listSearch($routeParams.q).success(function(r){
                $scope.products = r.products;
                $scope.category = r.category;
            });
            $scope.selected = void 0;
            $scope.states = [];
        })
		.controller('ProdCtrl', function($scope,$routeParams,product,toaster, $activityIndicator){
			$("select").minimalect();
            $scope.product = {};
            $scope.images = {};
            $scope.tags = [];
            //$scope.categories = [];
            $scope.ui = {};
            $scope.collection = {};
            $scope.shop = {};
            $scope.changeBuyButton = false;
            $activityIndicator.startAnimating();
            product.product($routeParams.url).success(function(r){
                $scope.product  = r.product;
                $scope.images   = r.images;
                $scope.tags     = r.tags;
                $scope.collection = r.collections;
                //$scope.categories = r.categories;
                $scope.ui = r.ui;
                $scope.changeBuyButton = $scope.ui.existNegoziation;
                $scope.shop = r.shop;
                $scope.ui.loaded = true;
                $activityIndicator.stopAnimating();
            });

			  $scope.oneAtATime = true;
			  $scope.groups = [
				{
				  title: "Dynamic Group Header - 1",
				  content: "Dynamic Group Body - 1"
				}, {
				  title: "Dynamic Group Header - 2",
				  content: "Dynamic Group Body - 2"
				}, {
				  title: "Dynamic Group Header - 3",
				  content: "Dynamic Group Body - 3"
				}
			  ];
			  $scope.items = ["Item 1", "Item 2", "Item 3"];
			  $scope.status = {
				isFirstOpen: true,
				isFirstOpen1: true,
				isFirstOpen2: true,
				isFirstOpen3: true,
				isFirstOpen4: true,
				isFirstOpen5: true,
				isFirstOpen6: true
			  };
			  $scope.addItem = function() {
				var newItemNo;
				newItemNo = $scope.items.length + 1;
				$scope.items.push("Item " + newItemNo);
			  };

            $scope.handleBuy = function(product_id) {
                $scope.ui.loaded = false;
                product.handleBuy(product_id, $("select").val()).success(function(r){
                    if(r.data.success) {
                        if(r.data.buyNow) {
                            product.addToCart(product_id,$("select").val()).success(function(e){
                                return document.location = '/site/#paymentChoice';
                            })
                        } else {
                            toaster.pop('success', "", r.data.message);
                            $scope.ui.buyNow = false;
                            $scope.changeBuyButton = true;
                        }
                    } else {
                        toaster.pop('error', "", r.data.message);
                    }
                    $scope.ui.loaded = true;
                });
            }
			})
        .controller('CartCtrl', function($scope,product,$activityIndicator){
            $activityIndicator.startAnimating();
            $scope.cart = {
                total_price     : 0,
                tax_price       : 0,
                product_price   : 0,
                product         : {},
                quantity        : 0,
                user            : {
                    first_name  : "",
                    last_name   : "",
                    address     : "",
                    cap         : "",
                    city        : "",
                    province    : "",
                    country_id  : ""
                },
                shipping        : "",
                payment         : ""
            }
            $scope.shimprents   = [];
            $scope.payments     = [];
            product.loadCart().success(function(r){
                $scope.shimprents           = r.data.shipments;
                $scope.cart.total_price     = r.data.cart.total_price;
                $scope.cart.tax_price       = r.data.cart.tax_price;
                $scope.cart.product_price   = r.data.cart.product_price;
                $scope.cart.quantity        = r.data.cart.quantity;
                $scope.cart.product         = r.data.cart.product;
                $scope.cart.user            = r.data.user;
                $scope.payments             = r.data.payments;
                $scope.cart.shipping        = r.data.shipments[0].id;
                $scope.cart.payment         = r.data.payments[0].id;
                if(r.cart_data && r.cart_data.user) {
                    $scope.cart.user = r.cart_data.user;
                }
                $activityIndicator.stopAnimating();
            });

            $scope.saveCart = function(){
                angular.forEach($scope.payments, function(value, key) {
                    if(value.id == $scope.cart.payment) {
                        $scope.cart.payment = $scope.payments[key];
                    }
                });

                angular.forEach($scope.shimprents, function(value, key) {
                    if(value.id == $scope.cart.shipping) {
                        $scope.cart.shipping = $scope.shimprents[key];
                    }
                });
                product.setData($scope.cart).success(function(r){
                    if(r.data.success) {
                        return document.location = '#/paymentReview';
                    }
                });
            }
        }).controller('ReviewDataCtrl',function($scope,product,$activityIndicator){
            $activityIndicator.startAnimating();
            $scope.cart_data = {};
            product.loadReviewData().success(function(r){
                $scope.cart_data = r.data.data;
                $activityIndicator.stopAnimating();
            });
            $scope.createOrder = function(){
                product.saveOrder().success(function(r){
                    if(r.data.success) {
                        return document.location = '#/thanks';
                    }
                });
            }
        })
}).call(this);
