(function() {
    'use strict';

    angular.module('site', ['ngRoute', 'ngAnimate','site.controllers','site.services','ui.bootstrap','shared.directives','ngActivityIndicator','top_bar.services','top_bar.ctrls', 'shared.services', 'toaster']).config([
        '$routeProvider', function($routeProvider) {
            return $routeProvider.when('/', {
                redirectTo: '/home'
            }).when('/home', {
                templateUrl: '/view/site/home.html'
            }).when('/product/:url', {
                templateUrl: '/view/site/product.html'
            }).when('/category/:category_id', {
                templateUrl: '/view/site/category.html'
            }).when('/collection/:collection_name/:category_id', {
                templateUrl: '/view/site/collection.html'
            }).when('/search/:q', {
                templateUrl: '/view/site/search.html'
            }).when('/paymentChoice', {
                templateUrl: '/view/carts/payment_choice.html'
            }).when('/thanks', {
                templateUrl: '/view/carts/thanks.html'
            }).when('/all', {
                templateUrl: '/view/site/all.html'
            }).when('/paymentReview', {
                templateUrl: '/view/carts/payment_review.html'
            }).otherwise({
                redirectTo: "/404"
            })
        }
    ]);
}).call(this);