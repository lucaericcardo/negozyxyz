(function() {
    'use strict';
    angular.module('cart.services', []).factory('cart',function($http){
        return {
            addProduct: function(product_id,quantity,shop_id){
                return $http.post('/carts/addProduct.json',{product_id:product_id,quantity:quantity,shop_id:shop_id});
            },
            updateProduct: function(product_id,quantity,shop_id){
                return $http.post('/carts/updateProduct.json',{product_id:product_id,quantity:quantity,shop_id:shop_id});
            },
            removeProduct: function(product_id,shop_id){
                return $http.post('/carts/removeProduct.json',{product_id:product_id,shop_id:shop_id})
            },
            load: function(shop_id) {
                return $http.post('/carts/load.json',{shop_id:shop_id});
            }
        }
    })
}).call(this);
