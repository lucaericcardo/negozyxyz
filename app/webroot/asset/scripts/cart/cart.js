(function() {
    'use strict';
    angular.module('cart', ['ngAnimate','cart.controllers','cart.services','ui.bootstrap']);
}).call(this);