(function() {
    'use strict';
    angular.module('cart.controllers', [])
        .controller('cartCtrl', function($scope,$window,cart) {
            $scope.cart = {
                products: [],
                total: 0,
                total_quantity:0
            }
            cart.load(55).success(function(r){
                $scope.cart = r.cart;
            });
            $scope.updateProduct = function(product){
                cart.updateProduct(product.id,product.quantity,55).success(function(r){
                    if(r.data.success) {
                        $scope.cart = r.cart;
                    }
                })
            }
            $scope.removeProduct = function(product) {
                console.log("___");
                cart.removeProduct(product.id,55).success(function(r){
                    if(r.data.success) {
                        $scope.cart = r.cart;
                    }
                })
            }

            $scope.toggleDropdown = function($event) {
                $event.preventDefault();
                $event.stopPropagation();
                return false;
            };
            $scope.toggled = function(open) {
                if(open) {
                    var obj = {
                        height: 350,
                        width: 600,
                        name: "resize"
                    }
                    $window.parent.postMessage(obj, '*');
                } else {
                    var obj = {
                        height: 80,
                        width: 150,
                        name: "resize"
                    }
                    $window.parent.postMessage(obj, '*');
                }
            };
            $($window).on("message", function(e){
                var data = e.originalEvent.data;
                if(data.name == "addToCart") {
                    cart.addProduct(data.product_id,data.quantity,data.shop_id).success(function(r){
                        if(r.data.success) {
                            $scope.cart = r.cart;
                        }
                    })
                }
            });

        })
}).call(this);
