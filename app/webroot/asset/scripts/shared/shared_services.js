(function() {
    'use strict';
    angular.module('shared.services', [])
        .factory("notifies", function ($http) {
            return {
                loadNotifies: function(){
                    return $http.get('/notifies.json');
                },
                shop_notifies: function () {
                    return $http.get('/notifies/shop_notifies.json');
                },
                user_notifies: function () {
                    return $http.get('/notifies/user_notifies.json');
                },
                getAuthRealtimeNotify: function() {
                    return $http.post('/notifies/getAuthRealtimeNotify.json');
                },
                shop_timeline: function() {
                    return $http.post('/notifies/shop_timeline.json');
                },
                user_timeline: function() {
                    return $http.post('/notifies/user_timeline.json');
                }
            }
        })
        .factory("utility", function() {
            return {
                format_uri: function(name) {
                    var r_return;
                    if (name) {
                        r_return = name.replace(/\W/g, '-');
                    } else {
                        r_return = '';
                    }
                    return r_return;
                }
            }
        })
        .factory('socket', function ($rootScope) {
            var socket = io.connect('//notify.services.negozy.com:3002');
            return {
                on: function (eventName, callback) {
                    socket.on(eventName, function () {
                        var args = arguments;
                        $rootScope.$apply(function () {
                            callback.apply(socket, args);
                        });
                    });
                },
                emit: function (eventName, data, callback) {
                    socket.emit(eventName, data, function () {
                        var args = arguments;
                        $rootScope.$apply(function () {
                            if (callback) {
                                callback.apply(socket, args);
                            }
                        });
                    })
                }
            };
        })
}).call(this);

window.onerror = function(message, file, lineNumber) {
    var xmlhttp;
    if (window.XMLHttpRequest) {// code for IE7+, Firefox, Chrome, Opera, Safari
        xmlhttp=new XMLHttpRequest();
    }
    else {// code for IE6, IE5
        xmlhttp=new ActiveXObject("Microsoft.XMLHTTP");
    }
    xmlhttp.onreadystatechange=function() {
        if (xmlhttp.readyState==4 && xmlhttp.status==200) {
            console.log(xmlhttp.responseText);
            return false;
        }
    }
    xmlhttp.open("POST","/negozy_exception.json",true);
    xmlhttp.setRequestHeader("Content-type","application/x-www-form-urlencoded");
    xmlhttp.send('browser='+encodeURI(navigator.appVersion)+'&message='+encodeURI(message)+'&file='+encodeURI(file)+'&lineNumber='+encodeURI(lineNumber));

    return false;
}


