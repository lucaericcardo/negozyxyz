(function() {
    'use strict';
    angular.module('top_bar.ctrls', [])
        .controller('topBarDataController', function($scope,shopTopBar,$rootScope) {
            shopTopBar.isPublic().success(function(r) {
                if(r.data.success) {
                    $rootScope.isShopPublic = r.data.shop.is_public;
                    angular.element(document.getElementsByClassName("shopOnlineSymbol")).removeClass('hide');
                }
            })
        })

        .controller('notifyController', function($scope,notifies,$filter,socket,toaster,$location,$rootScope,$rootElement){

            $scope.shop_notifies = {};
            $scope.shop_notifies.notify = [];
            $scope.shop_notifies.count = 0;
            $scope.shop_notifies.readed = false;

            $scope.user_notifies = {};
            $scope.user_notifies.notify = [];
            $scope.user_notifies.count = 0;
            $scope.user_notifies.readed = false;

            notifies.loadNotifies().success(function(r){
                $scope.notifies = r.notifies;
                var p = $filter('filter')($scope.notifies, {"viewed":false});
                $scope.count_notifies = p.length;
                $scope.shop_notifies.count = r.notifies.shop_notify_count;
                $scope.user_notifies.count = r.notifies.user_notify_count;
            });

            $scope.loadShopNotifies = function(open){
                if(open) {
                    $scope.shop_notifies.notify = [];
                    $scope.shop_notifies.loading = true;
                    notifies.shop_notifies().success(function (r) {
                        $scope.shop_notifies.notify = r.notifies;
                        $scope.shop_notifies.loading = false;
                        $scope.shop_notifies.count = 0;
                    });
                }
            }

            $scope.loadUserNotifies = function(open){
                if(open) {
                    $scope.user_notifies.notify = [];
                    $scope.user_notifies.loading = true;
                    notifies.user_notifies().success(function (r) {
                        $scope.user_notifies.notify = r.notifies;
                        $scope.user_notifies.loading = false;
                        $scope.user_notifies.count = 0;
                    });
                }
            }

            notifies.getAuthRealtimeNotify().success(function(r) {
                socket.emit('auth', r.data);
            });

            socket.on('reciveNotify', function(data) {
                toaster.pop('info', "", data.toasty_html, 60000, 'trustedHtml');
                if(data.user_notify && !data.shop_notify) {
                    $scope.user_notifies.count++;
                    $scope.user_notifies.readed = false;
                } else if(data.shop_notify && !data.user_notify) {
                    $scope.shop_notifies.count++;
                    $scope.shop_notifies.readed = false;
                }
            });

        });
}).call(this);
