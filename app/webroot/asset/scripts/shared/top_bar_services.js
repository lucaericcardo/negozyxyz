(function() {
    'use strict';
    angular.module('top_bar.services', []).
        factory("cart",function($http){
            return {
                addToCart: function (product_id, quantity) {
                    return $http.post('/carts/addProductToCart.json', {product_id: product_id, quantity: quantity});
                }
            }
        }).
        factory("countries", function ($http) {
            return {
                get: function () {
                    return $http.get('/countries.json');
                }
            }
        }).
        factory("shopTopBar", function ($http) {
            return {
                isPublic: function () {
                    return $http.get('/shops/isPublic.json');
                }
            }
        })
}).call(this);
