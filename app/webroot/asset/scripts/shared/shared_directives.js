(function() {
    angular.module('shared.directives', []).directive('imgHolder', [
        function() {
            return {
                restrict: 'A',
                link: function(scope, ele, attrs) {
                    return Holder.run({
                        images: ele[0]
                    });
                }
            };
        }
    ]).filter('ofsetLimitTo', function() {
        return function(items, ofset, limit) {
            if(items != undefined) {
                var filtered = [];
                for (var i = 0; i < items.length; i++) {
                    if(ofset <= i && i <limit) {
                        filtered.push(items[i]);
                    }
                }
                return filtered;
            }
        };
    }).directive('stopEvent', function () {
        return {
            restrict: 'A',
            link: function (scope, element, attr) {
                element.bind('click', function (e) {
                    e.stopPropagation();
                });
            }
        };
    }).directive('compile', function($compile) {
        // directive factory creates a link function
        return function(scope, element, attrs) {
            scope.$watch(
                function(scope) {
                    // watch the 'compile' expression for changes
                    return scope.$eval(attrs.compile);
                },
                function(value) {
                    // when the 'compile' expression changes
                    // assign it into the current DOM
                    element.html(value);

                    // compile the new DOM and link it to the current
                    // scope.
                    // NOTE: we only compile .childNodes so that
                    // we don't get into infinite loop compiling ourselves
                    $compile(element.contents())(scope);
                }
            );
        };
    }).directive('customBackground', function() {
        return {
            restrict: "A",
            controller: [
                '$scope', '$element', '$location', function($scope, $element, $location) {
                    var addBg, path;
                    path = function() {
                        return $location.path();
                    };
                    addBg = function(path) {
                        $element.removeClass('body-home body-special body-tasks body-lock');
                        switch (path) {
                            case '/':
                                return $element.addClass('body-home');
                            case '/404':
                            case '/pages/500':
                            case '/pages/signin':
                            case '/pages/signup':
                            case '/pages/forgot':
                                return $element.addClass('body-special');
                            case '/pages/lock-screen':
                                return $element.addClass('body-special body-lock');
                            case '/tasks':
                                return $element.addClass('body-tasks');
                        }
                    };
                    addBg($location.path());
                    return $scope.$watch(path, function(newVal, oldVal) {
                        if (newVal === oldVal) {
                            return;
                        }
                        return addBg($location.path());
                    });
                }
            ]
        };
    }).directive('uiColorSwitch', [
        function() {
            return {
                restrict: 'A',
                link: function(scope, ele, attrs) {
                    return ele.find('.color-option').on('click', function(event) {
                        var $this, hrefUrl, style;
                        $this = $(this);
                        hrefUrl = void 0;
                        style = $this.data('style');
                        if (style === 'loulou') {
                            hrefUrl = 'styles/main.css';
                            $('link[href^="styles/main"]').attr('href', hrefUrl);
                        } else if (style) {
                            style = '-' + style;
                            hrefUrl = 'styles/main' + style + '.css';
                            $('link[href^="styles/main"]').attr('href', hrefUrl);
                        } else {
                            return false;
                        }
                        return event.preventDefault();
                    });
                }
            };
        }
    ]).directive('toggleMinNav', [
        '$rootScope', function($rootScope) {
            return {
                restrict: 'A',
                link: function(scope, ele, attrs) {
                    var $content, $nav, $window, Timer, app, updateClass;
                    app = $('#app');
                    $window = $(window);
                    $nav = $('#nav-container');
                    $content = $('#content');
                    ele.on('click', function(e) {
                        if (app.hasClass('nav-min')) {
                            app.removeClass('nav-min');
                        } else {
                            app.addClass('nav-min');
                            $rootScope.$broadcast('minNav:enabled');
                        }
                        return e.preventDefault();
                    });
                    Timer = void 0;
                    updateClass = function() {
                        var width;
                        width = $window.width();
                        if (width < 768) {
                            return app.removeClass('nav-min');
                        }
                    };
                    return $window.resize(function() {
                        var t;
                        clearTimeout(t);
                        return t = setTimeout(updateClass, 300);
                    });
                }
            };
        }
    ]).directive('collapseNav', [
        function() {
            return {
                restrict: 'A',
                link: function(scope, ele, attrs) {
                    var $a, $aRest, $lists, $listsRest, app;
                    $lists = ele.find('ul').parent('li');
                    $lists.append('<i class="fa fa-caret-right icon-has-ul"></i>');
                    $a = $lists.children('a');
                    $listsRest = ele.children('li').not($lists);
                    $aRest = $listsRest.children('a');
                    app = $('#app');
                    $a.on('click', function(event) {
                        var $parent, $this;
                        if (app.hasClass('nav-min')) {
                            return false;
                        }
                        $this = $(this);
                        $parent = $this.parent('li');
                        $lists.not($parent).removeClass('open').find('ul').slideUp();
                        $parent.toggleClass('open').find('ul').stop().slideToggle();
                        return event.preventDefault();
                    });
                    $aRest.on('click', function(event) {
                        return $lists.removeClass('open').find('ul').slideUp();
                    });
                    return scope.$on('minNav:enabled', function(event) {
                        return $lists.removeClass('open').find('ul').slideUp();
                    });
                }
            };
        }
    ]).directive('highlightActive', [
        function() {
            return {
                restrict: "A",
                controller: [
                    '$scope', '$element', '$attrs', '$location', function($scope, $element, $attrs, $location) {
                        var highlightActive, links, path;
                        links = $element.find('a');
                        path = function() {
                            return $location.path();
                        };
                        highlightActive = function(links, path) {
                            path = '#' + path;
                            return angular.forEach(links, function(link) {
                                var $li, $link, href;
                                $link = angular.element(link);
                                $li = $link.parent('li');
                                href = $link.attr('href');
                                if ($li.hasClass('active')) {
                                    $li.removeClass('active');
                                }
                                if (path.indexOf(href) === 0) {
                                    return $li.addClass('active');
                                }
                            });
                        };
                        highlightActive(links, $location.path());
                        return $scope.$watch(path, function(newVal, oldVal) {
                            if (newVal === oldVal) {
                                return;
                            }
                            return highlightActive(links, $location.path());
                        });
                    }
                ]
            };
        }
    ]).directive('toggleOffCanvas', [
        function() {
            return {
                restrict: 'A',
                link: function(scope, ele, attrs) {
                    return ele.on('click', function() {
                        return $('#app').toggleClass('on-canvas');
                    });
                }
            };
        }
    ])

         .directive('slimScroll', [
         function() {
         return {
         restrict: 'A',
         link: function(scope, ele, attrs) {
         return ele.slimScroll({
         height: attrs.scrollHeight || '100%'
         });
         }
         };
         }
         ])
        .directive('goBack', [
            function() {
                return {
                    restrict: "A",
                    controller: [
                        '$scope', '$element', '$window', function($scope, $element, $window) {
                            return $element.on('click', function() {
                                return $window.history.back();
                            });
                        }
                    ]
                };
            }
        ]).
        directive('uiFileUpload', [
            function() {
                return {
                    restrict: 'A',
                    link: function(scope, ele) {
                        return ele.bootstrapFileInput();
                    }
                };
            }
        ]).directive("contenteditable", function() {
        return {
            restrict: "A",
            require: "ngModel",
            link: function(scope, element, attrs, ngModel) {

                function read() {
                    ngModel.$setViewValue(element.html());
                }

                ngModel.$render = function() {
                    element.html(ngModel.$viewValue || "");
                };

                element.bind("blur keyup change", function() {
                    scope.$apply(read);
                });

                element.bind("focusout", function() {
                    var value =  element.html();
                    value = value.replace(/\W/g, '_');
                    ngModel.$modelValue = value;
                });

            }
        };
    }).directive('ngThumb', ['$window', function($window) {
            var helper = {
                support: !!($window.FileReader && $window.CanvasRenderingContext2D),
                isFile: function(item) {
                    return angular.isObject(item) && item instanceof $window.File;
                },
                isImage: function(file) {
                    var type = '|' + file.type.slice(file.type.lastIndexOf('/') + 1) + '|';
                    return '|jpg|png|jpeg|bmp|gif|'.indexOf(type) !== -1;
                }
            };
            return {
                restrict: 'A',
                template: '<canvas/>',
                link: function(scope, element, attributes) {
                    if (!helper.support) return;
                    var params = scope.$eval(attributes.ngThumb);
                    if (!helper.isFile(params.file)) return;
                    if (!helper.isImage(params.file)) return;
                    var canvas = element.find('canvas');
                    var reader = new FileReader();
                    reader.onload = onLoadFile;
                    reader.readAsDataURL(params.file);
                    function onLoadFile(event) {
                        var img = new Image();
                        img.onload = onLoadImage;
                        img.src = event.target.result;
                    }
                    function onLoadImage() {
                        var width = params.width || this.width / this.height * params.height;
                        var height = params.height || this.height / this.width * params.width;
                        canvas.attr({ width: width, height: height });
                        canvas[0].getContext('2d').drawImage(this, 0, 0, width, height);
                    }
                }
            };
        }]).directive('piterestGrind', function($timeout) {
            return {
                restrict: 'AE',
                replace: true,
                link: function(scope, ele, attrs) {
                    if (scope.$last === true) {
                        $timeout(function(){
                            $('#blog-landing').unbind('pinterest_grid');;
                            $('#blog-landing').pinterest_grid({
                                no_columns: 3,
                                padding_x: 15,
                                padding_y: 15,
                                margin_bottom: 50,
                                single_column_breakpoint: 1170
                            });
                        });
                    }
                }
            };
        })
        .directive('zoomImage', function($timeout) {
            return {
                restrict: 'AE',
                replace: true,
                link: function(scope, ele, attrs) {
                    if (scope.$last === true) {
                        $timeout(function(){
                            $(".sp-wrap").append('<div class="sp-large"></div><div class="sp-thumbs sp-tb-active"></div>');
                            $(".sp-wrap a").appendTo(".sp-thumbs");
                            $(".sp-thumbs a:first").addClass("sp-current").clone().removeClass("sp-current").appendTo(".sp-large");
                            $(".sp-wrap").css("display", "inline-block");
                            var slideTiming = 300;
                            var maxWidth = $(".sp-large img").width();
                            $(".sp-thumbs").live("click", function(e) {
                                e.preventDefault()
                            });
                            $(".sp-tb-active a").live("click", function(e) {
                                $(".sp-current").removeClass();
                                $(".sp-thumbs").removeClass("sp-tb-active");
                                $(".sp-zoom").remove();
                                var t = $(".sp-large").height();
                                $(".sp-large").css({
                                    overflow: "hidden",
                                    height: t + "px"
                                });
                                $(".sp-large a").remove();
                                $(this).addClass("sp-current").clone().hide().removeClass("sp-current").appendTo(".sp-large").fadeIn(slideTiming, function() {
                                    var e = $(".sp-large img").height();
                                    $(".sp-large").height(t).animate({
                                        height: e
                                    }, "fast", function() {
                                        $(".sp-large").css("height", "auto")
                                    });
                                    $(".sp-thumbs").addClass("sp-tb-active")
                                });
                                e.preventDefault()
                            });
                            $(".sp-large a").live("click", function(e) {
                                var t = $(this).attr("href");
                                $(".sp-large").append('<div class="sp-zoom"><img src="' + t + '"/></div>');
                                $(".sp-zoom").fadeIn();
                                $(".sp-large").css({
                                    left: 0,
                                    top: 0
                                });
                                e.preventDefault()
                            });

                            $(".sp-large").mousemove(function(e) {
                                var t = $(".sp-large").width();
                                var n = $(".sp-large").height();
                                var r = $(".sp-zoom").width();
                                var i = $(".sp-zoom").height();
                                var s = $(this).parent().offset();
                                var o = e.pageX - s.left;
                                var u = e.pageY - s.top;
                                var a = Math.floor(o * (t - r) / t);
                                var f = Math.floor(u * (n - i) / n);
                                $(".sp-zoom").css({
                                    left: a,
                                    top: f
                                })
                            }).mouseout(function() {})
                            $(".sp-zoom").live("click", function(e) {
                                $(this).fadeOut(function() {
                                    $(this).remove()
                                })
                            })

                        });
                    }
                }
            };
        })
        .directive('confirmModal', function($modal) {
            return {
                restrict: 'A',
                scope : {
                    confirmCallback:'&',
                    confirmMessage:'@',
                    data: "="
                },
                link: function (scope, ele, attrs) {
                    ele.bind('click', function () {
                        var modalHtml = '<div class="modal-body">' + scope.confirmMessage+ '</div>';
                        modalHtml += '<div class="modal-footer"><button class="btn btn-primary" ng-click="ok()">OK</button><button class="btn btn-warning" ng-click="cancel()">Cancel</button></div>';
                        var modalInstance = $modal.open({
                            template: modalHtml,
                            controller: 'confirmCtrl'
                        });
                        modalInstance.result.then(function() {
                            scope.confirmCallback(scope.data);
                        }, function() {
                            //Modal dismissed
                        });
                    });
                }
            }
        })

        .directive('validateEquals', [
        function() {
            return {
                require: 'ngModel',
                link: function(scope, ele, attrs, ngModelCtrl) {
                    var validateEqual;
                    validateEqual = function(value) {
                        var valid;
                        valid = value === scope.$eval(attrs.validateEquals);
                        ngModelCtrl.$setValidity('equal', valid);
                        return typeof valid === "function" ? valid({
                            value: void 0
                        }) : void 0;
                    };
                    ngModelCtrl.$parsers.push(validateEqual);
                    ngModelCtrl.$formatters.push(validateEqual);
                    return scope.$watch(attrs.validateEquals, function(newValue, oldValue) {
                        if (newValue !== oldValue) {
                            return ngModelCtrl.$setViewValue(ngModelCtrl.$ViewValue);
                        }
                    });
                }
            };
        }])
        .directive('categoriesStep', function(categories,selectCategories) {
            return {
                restrict: 'AE',
                scope: true,
                link: function ($scope) {
                    selectCategories.init();
                    $scope.obj = selectCategories.data;
                    $scope.selectSelectedItem = function(item) {
                        selectCategories.data.macrocategorySelected = {id : item.Subcategory.Category.Macrocategory.id, label : item.Subcategory.Category.Macrocategory.label};
                        selectCategories.data.categorySelected      = {id : item.Subcategory.Category.id, label : item.Subcategory.Category.label};
                        selectCategories.data.subcategorySelected   = {id : item.Subcategory.id, label : item.Subcategory.label};
                    }

                    $scope.selectMacrocategory = function(itemSel) {
                        selectCategories.data.macrocategorySelected = {id : itemSel.Macrocategory.id, label : itemSel.Macrocategory.label};
                        selectCategories.data.categories = itemSel.Category;
                        selectCategories.data.macrocategoryCompleted = true;
                        selectCategories.data.categoryCompleted = false;
                        selectCategories.data.categorySelected = {};
                    };
                    $scope.selectCategory = function(itemSel) {
                        selectCategories.data.categorySelected = {id : itemSel.id, label : itemSel.label};
                        selectCategories.data.subcategories = itemSel.Subcategory;
                        selectCategories.data.categoryCompleted = true;
                        selectCategories.data.subcategorySelected = {};
                    };
                    $scope.selectSubcategory = function(itemSel) {
                        selectCategories.data.subcategorySelected = {id : itemSel.id, label : itemSel.label};
                        selectCategories.data.subcategoryCompleted = true;
                    };

                    $scope.goToCateStep = function() {
                        categories.validateMacrocategory(selectCategories.data.newMacrocategory).success(function(r) {
                            if(r.data.success) {
                                selectCategories.data.macroAlredyExist = false;
                                selectCategories.data.newMacroOk = true;
                                selectCategories.data.newCate = true;
                                selectCategories.data.macrocategoryCompleted = true;
                            } else {
                                selectCategories.data.newCate = false;
                                selectCategories.data.macrocategoryCompleted = false;
                                selectCategories.data.macroAlredyExist = true;
                                selectCategories.data.newMacroOk = false;
                            }
                            selectCategories.data.showMacroMsg = true;
                        });
                    }
                    $scope.goToSubStep = function() {
                        categories.validateCategory(selectCategories.data.newCategory).success(function(r) {
                            if(r.data.success) {
                                selectCategories.data.cateAlredyExist = false;
                                selectCategories.data.newCateOk = true;
                                selectCategories.data.newSub = selectCategories.data.categoryCompleted = true;
                            } else {
                                selectCategories.data.newSub = false;
                                selectCategories.data.categoryCompleted = false;
                                selectCategories.data.cateAlredyExist = true;
                                selectCategories.data.newCateOk = false;
                            }
                        });
                    }
                    $scope.validateSubcategory = function() {
                        categories.validateSubcategory(selectCategories.data.newSubcategory).success(function(r) {
                            if(r.data.success) {
                                selectCategories.data.subAlredyExist = false;
                                selectCategories.data.newSubOk = true;
                            } else {
                                selectCategories.data.subAlredyExist = true;
                                selectCategories.data.newSubOk = false;
                            }
                        });
                    }

                    $scope.addNewTree = function() {
                        selectCategories.data.macrocategorySelected     = false;
                        selectCategories.data.categorySelected          = false;
                        selectCategories.data.subcategorySelected       = false;
                        selectCategories.data.macrocategoryCompleted    = false;
                        selectCategories.data.categoryCompleted         = false;
                        selectCategories.data.newTree                   = true;
                    }

                    $scope.backMacroStep = function(newMacro) {
                        selectCategories.data.newMacroOk = false;
                        selectCategories.data.macroAlredyExist = false;
                        selectCategories.data.macrocategorySelected = {};
                        selectCategories.data.categorySelected = {};
                        selectCategories.data.subcategorySelected = {};
                        selectCategories.data.subcategoryCompleted = false;
                        selectCategories.data.categoryCompleted = false;
                        selectCategories.data.macrocategoryCompleted = false;
                        selectCategories.data.newCate = false;
                        selectCategories.data.newSub = false;
                        selectCategories.data.newMacro = newMacro;
                        selectCategories.data.newMacrocategory = selectCategories.data.newSubcategory = selectCategories.data.newCategory = '';
                    };
                    $scope.backCateStep = function(newCate) {
                        selectCategories.data.newCateOk = selectCategories.data.cateAlredyExist = false;
                        if(selectCategories.data.newMacro || newCate) {
                            selectCategories.data.newCate = true;
                        } else {
                            selectCategories.data.newCate = false;
                        }
                        selectCategories.data.categorySelected =
                            selectCategories.data.subcategorySelected = {};

                        selectCategories.data.subcategoryCompleted =
                            selectCategories.data.categoryCompleted =
                                selectCategories.data.newSub = false;

                        selectCategories.data.newSubcategory = selectCategories.data.newCategory = '';
                    };
                    $scope.backSubStep = function(newSub) {
                        selectCategories.data.newSubOk = selectCategories.data.subAlredyExist = false;
                        if(selectCategories.data.newCate || newSub) {
                            selectCategories.data.newSub = true;
                        } else {
                            selectCategories.data.newSub = false;
                        }
                        selectCategories.data.subcategorySelected = {};
                        selectCategories.data.subcategoryCompleted = false;
                        selectCategories.data.newSubcategory = '';
                    };
                }
            }
        }).directive('pieChart', [
        function() {
            return {
                restrict: 'E',
                template: '<div style="height: 250px"></div>',
                replace: true,
                link: function($scope, element, attrs) {
                    $scope.$watch(attrs.datachart, function() {
                        var data = $scope[attrs.datachart];
                        data.element = element;
                        if(data.percent ) {
                            data.formatter = function (value, data) {
                                return value + '%';
                            }
                        }
                        Morris.Donut(data);
                    });
                }
            };
        }])
        .directive('areaChart', [
            function() {
                return {
                    restrict: 'E',
                    replace: true,
                    template: '<div style="height: 250px"></div>',
                    link: function($scope, element, attrs) {
                        $scope.$watch(attrs.datachart, function() {
                            var data = $scope[attrs.datachart];
                            if(!$scope.chart) {
                                $scope.chart = [];
                            }
                            if(data && data.loaded) {
                                element.empty()
                                data.element = element;
                                $scope.chart[data.element] = Morris.Line(data);
                            }
                        });

                    }
                };
            }])
}).call(this);