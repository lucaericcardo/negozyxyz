(function() {
    'use strict';
    angular.module('main', ['ngRoute', 'ngAnimate', 'ui.bootstrap','mgo-angular-wizard','main.ctrls','main.services','shared.directives','shared.services','ngActivityIndicator','angularFileUpload','top_bar.services','top_bar.ctrls','toaster']).config([
        '$routeProvider', function($routeProvider) {
            return $routeProvider
            .when('/_=_', {
                redirectTo: '/'
            })
            .when('/', {
                templateUrl: '/view/shops/add'
            })
            .when('/account', {
                templateUrl: '/view/users/account.html'
            })
            .when('/orders', {
                templateUrl: '/view/account/orders.html'
            }).when('/order/:order_uid', {
                templateUrl: '/view/account/order_details.html'
            })

        }
    ]);
}).call(this);
