(function () {
    'use strict';
    angular.module('main.ctrls', [])
        .controller('shopCtrl', function ($scope, WizardHandler, shopService, countries, $activityIndicator) {
            $activityIndicator.startAnimating();
            $scope.shop = {
                name: '',
                url: '',
                business_name: '',
                vat_number: '',
                address: '',
                city: '',
                cap: '',
                state: 'IT',
                province: '',
                macrocategory_id: '',
                phone_number: '',
                code: ''

            };
            $scope.categories = {};
            $scope.showError = false;
            $scope.showErrorData = false;
            $scope.availableUrl = true;
            $scope.availableVatNumber = true;
            $scope.sent = false;
            $scope.verified = false;
            countries.get().success(function (r) {
                $scope.countries = r.countries;
                $activityIndicator.stopAnimating();
            });
            $scope.validate_data_first_step = function () {
                var vat_number = $scope.shop.vat_number;
                if (vat_number) {
                    WizardHandler.wizard().next();
                    shopService.ckPiva($scope.shop.state, vat_number).success(function (r) {
                        shopService.ckVatNumber($scope.shop.vat_number).success(function(r1){
                            if(!r1.data.success){
                                $scope.availableVatNumber = false;
                            } else {
                                $scope.availableVatNumber = true;
                                if(r.valid) {
                                    var address = r.address.split('\n');
                                    var street = address[0];
                                    var address_data = address[1];
                                    address_data = address_data.split(" ");
                                    var cap = address_data[0];
                                    delete address_data[0];
                                    var province = address_data[address_data.length - 1];
                                    delete address_data[address_data.length - 1];
                                    var city = address_data.join(" ", address_data);
                                    $scope.shop.name = r.name.trim();
                                    $scope.shop.business_name = r.name.trim();
                                    $scope.shop.cap = cap.trim();
                                    $scope.shop.city = city.trim();
                                    $scope.shop.address = street.trim();
                                    $scope.shop.address = street.trim();
                                    $scope.shop.province = province.trim();
                                } else {
                                    $scope.shop.name = "";
                                    $scope.shop.business_name = "";
                                    $scope.shop.cap = "";
                                    $scope.shop.city = "";
                                    $scope.shop.address = "";
                                    $scope.shop.address = "";
                                    $scope.shop.province = "";
                                }
                                $scope.showError = false;
                                $scope.showErrorData = false;
                                WizardHandler.wizard().next();
                            }
                        });
                    })
                } else {
                    $scope.showError = true;
                    $scope.showErrorData = true;
                }
            }
            $scope.checkShop = function () {
                if ($scope.shop.name && $scope.shop.url && $scope.shop.macrocategory_id) {
                    shopService.ckName($scope.shop.url).success(function (r) {
                        if (r.data.success) {
                            WizardHandler.wizard().next();
                            $scope.availableUrl = true;
                            $scope.showError = false;
                        } else {
                            $scope.availableUrl = false;
                            $scope.showError = false;
                        }
                    })
                } else {
                    $scope.showError = true;
                }

            }
            $scope.checkShopData = function () {
                if ($scope.shop.vat_number && $scope.shop.business_name
                    && $scope.shop.address && $scope.shop.address
                    && $scope.shop.city && $scope.shop.cap
                    && $scope.shop.state && $scope.shop.province) {
                    $scope.showErrorData = false;
                    shopService.getCategories().success(function (r) {
                        $scope.categories = r.categories;
                        $scope.shop.macrocategory_id = r.categories[0].id;
                        if(!$scope.shop.url) {
                            $scope.shop.url = $scope.shop.business_name.replace(/[^a-zA-Z0-9]/g,'').toLowerCase();
                        }
                        if(!$scope.shop.name) {
                            $scope.shop.name = $scope.shop.business_name;
                        }
                        WizardHandler.wizard().next();
                    })
                } else {
                    $scope.showErrorData = true;
                }
            };
            $scope.sendCode = function () {
                if($scope.shop.phone_number) {
                    shopService.sendCode($scope.shop.phone_number).success(function () {
                        $scope.sent = true;
                    })
                }
            };

            $scope.verifyCode = function () {
                shopService.verifyCode($scope.shop.code).success(function (r) {
                    $scope.showError = false;
                    if(r.data.success) {
                        $scope.verified = true;
                    } else {
                        $scope.showError = true;
                    }
                })
            }
			
			$scope.retry=function(){
				$scope.sent=false;
				$scope.verified=false;
				}

            $scope.saveData = function () {
                shopService.save($scope.shop).success(function (r) {
                    if (r.data.success) {
                        window.location.href = "/dashboard/shop/" + $scope.shop.url + "/";
                    }
                });
            }
        }).controller("mainCtrl", function ($scope, post, $activityIndicator, $filter, FileUploader) {
            $scope.posts = {};
            $scope.post = {
                text: ''
            }
            $scope.tmpPost = {};
            $scope.comment = {
                text: ''
            }

            $scope.uploader = new FileUploader({
                url: "/posts/loadImage",
                onBeforeUploadItem: function (item) {
                    item.url = $scope.uploader.url;
                },
                onSuccessItem: function (item, response) {
                    if (response.data.success) {
                        $scope.tmpPost.image = response.data.url;
                        $scope.posts.unshift($scope.tmpPost);
                        $scope.post.text = '';
                        $scope.tmpPost = {};
                        $scope.uploader.clearQueue();
                    }
                }
            });

            post.getList().success(function (r) {
                $scope.posts = r.posts;
            });

            $scope.like = function (post_id) {
                post.like(post_id).success(function (r) {
                    if (r.data.success) {
                        var p = $filter('filter')($scope.posts, {"post_id": post_id});
                        triggerPreference(post_id, r.data);
                    }
                });
            }
            $scope.dislike = function (post_id) {
                post.dislike(post_id).success(function (r) {
                    if (r.data.success) {
                        triggerPreference(post_id, r.data);
                    }
                });
            }
            $scope.uninteresting = function (post_id) {
                post.uninteresting(post_id).success(function (r) {
                    if (r.data.success) {
                        triggerPreference(post_id, r.data);
                    }
                });
            }
            $scope.addComment = function (post_id) {
                var p = $filter('filter')($scope.posts, {"post_id": post_id});
                post.addComment(post_id, p[0].comment.text).success(function (r) {
                    if (r.data.success) {
                        p[0].comments.push(r.data.comment);
                        p[0].comment.text = "";
                    }
                });
            }
            $scope.save = function () {
                post.add($scope.post).success(function (r) {
                    if (r.data.success) {
                        if ($scope.uploader.queue.length) {
                            $scope.uploader.url = "/posts/loadImage/" + r.data.post.post_id + ".json";
                            $scope.uploader.uploadAll();
                            $scope.tmpPost = r.data.post;
                        } else {
                            $scope.posts.unshift(r.data.post);
                            $scope.post.text = '';
                        }
                    }
                })
            }

            function triggerPreference(post_id, data) {
                var p = $filter('filter')($scope.posts, {"post_id": post_id});
                p[0].preference = 0;
                if (data.trigger.uninteresting == "add") {
                    p[0].uninteresting++;
                    p[0].preference = 3;
                } else if (data.trigger.uninteresting == "remove") {
                    p[0].uninteresting--;
                }
                if (data.trigger.dislike == "add") {
                    p[0].dislikes++;
                    p[0].preference = 2;
                } else if (data.trigger.dislike == "remove") {
                    p[0].dislikes--;
                }
                if (data.trigger.like == "add") {
                    p[0].likes++;
                    p[0].preference = 1;
                } else if (data.trigger.like == "remove") {
                    p[0].likes--;
                }
            }

        }).controller('user_ctrl', function ($scope, $routeParams, users) {
            $scope.user = {};
            users.getUser($routeParams.username).success(function (r) {
                $scope.user = r.user;
            });
        }).controller('account', function ($scope,users,FileUploader) {
            $scope.user = {
                first_name: '',
                last_name: '',
                new_password: '',
                confirm_password: '',
                image: '/asset/admin/images/g1.jpg'
            };
            users.loadUser().success(function (r)
            {
                $scope.user.first_name = r.user.first_name;
                $scope.user.last_name = r.user.last_name;
                if(r.user.image) {
                    $scope.user.image = r.user.image;
                }

            });
            $scope.saveAccount = function(){
                users.updateAccount($scope.user).success(function(r){
                });
            }
            $scope.uploaderProfile = new FileUploader({
                url: "/users/loadProfile.json",
                onBeforeUploadItem: function(item){
                    item.url = $scope.uploaderProfile.url;
                    $scope.loadingImage = true;
                },
                onProgressAll: function(progress){
                    $scope.progress = progress;
                },
                onAfterAddingAll : function()  {
                    $scope.uploaderProfile.uploadAll();
                },
                onSuccessItem: function(item, response){
                    if(response.data.success) {
                        $scope.user.image = response.data.url;
                    }
                    $scope.loadingImage = false;
                },
                onErrorItem: function(item, response, status, headers){
                    $scope.loadingImage = false;
                }
            });

        })
}).call(this);