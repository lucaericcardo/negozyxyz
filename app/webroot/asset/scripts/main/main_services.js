(function() {
    'use strict';
    angular.module('main.services', [])
        .factory("shopService", function ($http) {
            return {
                save: function (shop) {
                    return $http.post('/shops/saveShop.json',{shop:shop});
                },
                ckName: function(url){
                    return $http.get('/shops/checkShopName/'+url+'.json');
                },
                ckPiva: function(countyCode,piva) {
                    return $http.get('http://130.211.96.48:3000/validate_vat_number/'+countyCode+'/'+piva);
                },
                getCategories: function(){
                    return $http.get('/shops/getCategories.json');
                },
                sendCode: function(phone_number) {
                    return $http.post('/shops/sendCode.json',{phone_number:phone_number});
                },
                verifyCode: function(code){
                    return $http.post('/shops/verifyCode.json',{code:code});
                },
                ckVatNumber: function(vat_number){
                    return $http.get('/shops/checkVatNumber/'+vat_number+'.json');
                }
            }
        }).factory("post",function($http){
            return {
                getList: function(){
                    return $http.get('/posts.json');
                },
                like: function(post) {
                    return $http.post('/posts/like.json',{post:post});

                },
                dislike: function(post) {
                    return $http.post('/posts/dislike.json',{post:post});

                },
                uninteresting: function(post) {
                    return $http.post('/posts/uninteresting.json',{post:post});

                },
                addComment: function(post,text) {
                    return $http.post('/posts/addComment.json',{post:post,comment:text});

                },
                add : function(post){
                    return $http.post('/posts/add.json',{post:post});
                }
            }
        }).factory("users",function($http){
            return {
                getUser: function (user) {
                    return $http.get('/users/getUser/' + user + '.json');
                },
                loadUser: function(){
                    return $http.get('/users/loadUser.json');
                },
                updateAccount: function(user) {
                    return $http.post('/users/updateAccount.json',{user:user});
                }
            }
        })
}).call(this);
