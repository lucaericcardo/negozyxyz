(function () {
    'use strict';
    angular.module('admin.ctrls', [])
        .controller('dashboard', function ($scope, $routeParams, chart) {
            $scope.pieData = {};
            $scope.count_users = 0;
            chart.loadData().success(function (r) {
                $scope.pieData = r.pieData;
                $scope.count_users = r.users;
            });
        }).controller('usersCtrl', function ($scope, $routeParams, users,$filter) {
            $scope.users = {};
            $scope.numPerPageOpt        = [5, 10, 20];
            $scope.numPerPage           = $scope.numPerPageOpt[1];
            $scope.currentPage          = 1;
            $scope.currentPageStores    = [];
            $scope.totalUsers           = 0;
            $scope.users                = [];
            $scope.row                  = 'created';
            $scope.orderType            = 'DESC';
            $scope.select = function(page) {
                users.list($scope.numPerPage,page,$scope.row,$scope.orderType).success(function(r){
                    $scope.users = r.users;
                    $scope.totalUsers = r.count;
                });
            };
            $scope.enable = function(user_id){
                users.enable(user_id).success(function(r){
                    if(r.data.success) {
                        var user = $filter('filter')($scope.users, {"id": user_id});
                        if(user[0]) {
                            user[0].confirmed = true;
                        }
                    }
                });
            }
            $scope.onFilterChange = function() {
                $scope.select(1);
                $scope.currentPage = 1;
                return $scope.row = '';
            };
            $scope.onNumPerPageChange = function() {
                $scope.select(1);
                return $scope.currentPage = 1;
            };
            $scope.onOrderChange = function() {
                $scope.select(1);
                return $scope.currentPage = 1;
            };
            $scope.order = function(rowName,orderType) {
                if ($scope.row === rowName && $scope.orderType === orderType) {
                    return;
                }
                $scope.row = rowName;
                $scope.orderType = orderType;
                return $scope.onOrderChange();
            };
            var init = function() {
                return $scope.select($scope.currentPage);
            };
            return init();

        }).controller('add_macrocategories', function ($scope,macrocategories) {
            $scope.macrocategories = {
                labels : ""
            };
            $scope.saveMacorcategories = function() {
                macrocategories.saveMacrocategories($scope.macrocategories.labels).success(function (r) {

                });
            }
        }).controller('macrocategories', function ($scope,macrocategories) {
            $scope.macrocategories = [];
            macrocategories.loadAll().success(function (r) {
                $scope.macrocategories = r.macrocategories;
            });
        })
        .controller('categories', function ($scope,categories) {
            $scope.categories = [];
            categories.loadAll().success(function (r) {
                $scope.categories = r.categories;
            });
        })
        .controller('add_categories', function ($scope,categories,macrocategories) {
            $scope.categories = {
                labels : "",
                macrocategory_id: ""
            };
            $scope.macrocategories = [];
            macrocategories.loadAll().success(function (r) {
                $scope.macrocategories = r.macrocategories;
            });
            $scope.saveCategories = function() {
                categories.saveCategories($scope.categories.labels,$scope.categories.macrocategory_id).success(function (r) {

                });
            }
        })
        .controller('subcategories', function ($scope,subcategories) {
            $scope.subcategories = [];
            subcategories.loadAll().success(function (r) {
                $scope.subcategories = r.subcategories;
            });
        })
        .controller('add_subcategories', function ($scope,subcategories,categories) {
            $scope.subcategories = {
                labels : "",
                category_id: ""
            };
            $scope.categories = [];
            categories.loadAll().success(function (r) {
                $scope.categories = r.categories;
            });
            $scope.saveSubcategories = function() {
                subcategories.saveSubcategories($scope.subcategories.labels,$scope.subcategories.category_id).success(function (r) {

                });
            }
        })
}).call(this);