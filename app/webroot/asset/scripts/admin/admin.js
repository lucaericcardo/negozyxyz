(function() {
    'use strict';
    angular.module('admin', ['ngRoute', 'ngAnimate', 'ui.bootstrap','admin.ctrls','admin.services','shared.services','shared.directives','ngActivityIndicator','top_bar.services','top_bar.ctrls','toaster']).config([
        '$routeProvider', function($routeProvider) {
            return $routeProvider.when('/', {
                redirectTo: '/dashboard'
            }).when('/dashboard', {
                templateUrl: '/view/administrator/dashboard.html'
            }).when('/users', {
                templateUrl: '/view/administrator/users_list.html'
            }).when('/macrocategories', {
                templateUrl: '/view/administrator/macrocategories.html'
            }).when('/add_macrocategory', {
                    templateUrl: '/view/administrator/add_macrocategory.html'
            }).when('/categories', {
                templateUrl: '/view/administrator/categories.html'
            }).when('/add_categories', {
                templateUrl: '/view/administrator/add_categories.html'
            }).when('/subcategories', {
                templateUrl: '/view/administrator/subcategories.html'
            }).when('/add_categories', {
                templateUrl: '/view/administrator/add_categories.html'
            }).when('/add_subcategories', {
                templateUrl: '/view/administrator/add_subcategories.html'
            }).otherwise({
                redirectTo: "/404"
            })
        }
    ]);
}).call(this);
