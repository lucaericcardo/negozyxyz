(function() {
    'use strict';
    angular.module('admin.services', [])
        .factory("chart",function($http){
            return {
                loadData: function () {
                    return $http.get('/administrator/loadData.json');
                }
            }
        })
        .factory("users",function($http){
            return {
                list: function (n, page, row, orderType) {
                    return $http.get('/administrator/loadUsers/' + n + '/' + page + '/' + row + '/' + orderType + '.json');
                },
                enable: function(user_id){
                    return $http.post('/administrator/enableUser.json',{user_id:user_id});
                }
            }
        })
        .factory("macrocategories",function($http){
            return {
                saveMacrocategories: function(macrocategories){
                    return $http.post('/administrator/saveMacrocategories.json',{macrocategories:macrocategories});
                },
                loadAll: function(){
                    return $http.get('/administrator/loadMacrocategories.json');
                }
            }
        })
        .factory("categories",function($http){
            return {
                saveCategories: function(categories,macrocategory_id){
                    return $http.post('/administrator/saveCategories.json',{categories:categories,macrocategory_id:macrocategory_id});
                },
                loadAll: function(){
                    return $http.get('/administrator/loadCategories.json');
                }
            }
        }).factory("subcategories",function($http){
            return {
                saveSubcategories: function(subcategories,category_id){
                    return $http.post('/administrator/saveSubcategories.json',{subcategories:subcategories,category_id:category_id});
                },
                loadAll: function(){
                    return $http.get('/administrator/loadSubcategories.json');
                }
            }
        })
}).call(this);
