(function () {
    'use strict';
    angular.module('account.ctrls', [])
        .controller('AccountCtrl', function ($scope,users,FileUploader,toaster) {
            $scope.user = {
                first_name: '',
                last_name: '',
                new_password: '',
                confirm_password: '',
                image: 'http://cdn.negozy.com/negozy/Img/g1.jpg'
            };
            users.loadUser().success(function (r)
            {
                $scope.user.first_name = r.user.first_name;
                $scope.user.last_name = r.user.last_name;
                if(r.user.image) {
                    $scope.user.image = r.user.image;
                }

            });
            $scope.saveAccount = function(){
                users.updateAccount($scope.user).success(function(r){
                    if(r.data.success) {
                        toaster.pop('success', "", r.data.message);
                    } else {
                        toaster.pop('error', "", r.data.message);
                    }
                });
            }
            $scope.uploaderProfile = new FileUploader({
                url: "/users/loadProfile.json",
                onBeforeUploadItem: function(item){
                    item.url = $scope.uploaderProfile.url;
                    $scope.loadingImage = true;
                },
                onProgressAll: function(progress){
                    $scope.progress = progress;
                },
                onAfterAddingAll : function()  {
                    $scope.uploaderProfile.uploadAll();
                },
                onSuccessItem: function(item, response){
                    if(response.data.success) {
                        $scope.user.image = response.data.url;
                    }
                    $scope.loadingImage = false;
                },
                onErrorItem: function(item, response, status, headers){
                    $scope.loadingImage = false;
                }
            });
        })
        .controller('UserNotifiesCtrl',function($scope,$activityIndicator,notifies){

            $activityIndicator.startAnimating();
            $scope.timeline = [];
            notifies.user_timeline().success(function(r){
                $scope.timeline = r.notifies;
                $activityIndicator.stopAnimating();
            });

        })
        .controller('OrdersCtrl',function($scope, $filter,orders,$activityIndicator){
            var init;
            $scope.numPerPageOpt        = [5, 10, 20];
            $scope.data                 = {};
            $scope.numPerPage           = $scope.numPerPageOpt[1];
            $scope.currentPage          = 1;
            $scope.currentPageStores    = [];
            $scope.orders             = [];
            $scope.totalOrders       = 0;
            $scope.row                  = 'created';
            $scope.orderType            = 'DESC';
            $scope.select = function(page) {
                $activityIndicator.startAnimating();
                orders.getList($scope.numPerPage,page,$scope.row,$scope.orderType).success(function(r){
                    $scope.orders = r.orders;
                    $scope.totalOrders = r.count;
                    $activityIndicator.stopAnimating();
                });
            };
            $scope.onFilterChange = function() {
                $scope.select(1);
                $scope.currentPage = 1;
                return $scope.row = '';
            };
            $scope.onNumPerPageChange = function() {
                $scope.select(1);
                return $scope.currentPage = 1;
            };
            $scope.onOrderChange = function() {
                $scope.select(1);
                return $scope.currentPage = 1;
            };
            $scope.order = function(rowName,orderType) {
                if ($scope.row === rowName && $scope.orderType === orderType) {
                    return;
                }
                $activityIndicator.startAnimating();
                $scope.row = rowName;
                $scope.orderType = orderType;
                return $scope.onOrderChange();
            };
            init = function() {
                return $scope.select($scope.currentPage);
            };
            return init();
        })

        .controller('OrderCtrl',function($scope,$routeParams,orders,$activityIndicator,toaster){
            $activityIndicator.startAnimating();
            $scope.order = {};
            $scope.order_status = [];
            orders.load_order($routeParams.order_uid).success(function(r){
                $scope.order = r.order;
                $activityIndicator.stopAnimating();

            });
            $scope.changeStatus = function(){
                orders.changeStatus($routeParams.order_id,$scope.order.order.status).success(function(r){
                    if(r.data.success) {
                        toaster.pop('success', "", r.data.message);
                    } else {
                        toaster.pop('error', "", r.data.message);
                    }
                });
            }
        })

}).call(this);