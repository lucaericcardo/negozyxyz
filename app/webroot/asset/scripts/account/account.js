(function() {
    'use strict';
    angular.module('account', ['ngRoute', 'ngAnimate', 'ui.bootstrap','account.ctrls','account.services','shared.directives','shared.services','ngActivityIndicator','angularFileUpload','top_bar.services','top_bar.ctrls','toaster']).config([
        '$routeProvider', function($routeProvider) {
             return $routeProvider.when('/', {
             redirectTo: '/'
             }).when('/', {
                templateUrl: '/view/account/account.html'
             }).when('/orders', {
                 templateUrl: '/view/account/orders.html'
             }).when('/order/:order_uid', {
                 templateUrl: '/view/account/order_details.html'
             }).when('/notifies', {
                 templateUrl: '/view/account/notifies.html'
             })
        }
    ]);
}).call(this);
