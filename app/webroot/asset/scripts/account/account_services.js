(function() {
    'use strict';
    angular.module('account.services', [])
        .factory("users",function($http){
            return {
                loadUser: function(){
                    return $http.get('/account/loadUser.json');
                },
                updateAccount: function(user) {
                    return $http.post('/account/updateAccount.json',{user:user});
                }
            }
        })
        .factory("orders",function($http){
            return {
                getList: function (n, page, row, orderType) {
                    return $http.get('/account/get_list/' + n + '/' + page + '/' + row + '/' + orderType + '.json');
                },
                load_order: function(order_id){
                    return $http.post('/account/loadOrder.json',{order_id:order_id});
                }
            }
        })
}).call(this);
