(function() {
    'use strict';
    angular.module('shop.controllers', [])
        .controller('DashboardCtrl',  function($scope,$activityIndicator,$modal,shop, $rootScope) {
        $activityIndicator.startAnimating();
        $scope.dashboardData = {
            name: '',
            product_count: 0,
            order_count: 0,
            collection_count: 0
        };
        shop.dashboardData().success(function (r) {
            $scope.dashboardData = r.data;
            $rootScope.isShopPublic == r.data.is_public;
            $activityIndicator.stopAnimating();
        })

        $scope.publicShop = function () {
            var modalInstance = $modal.open({
                templateUrl: '/view/shops/modal_public_shop.html',
                controller: 'publicShopCtrl',
                backdrop: 'static',
                resolve: {
                    shop_data: function () {
                        return {site_url : $scope.dashboardData.site_url};
                    }
                }
            });
            modalInstance.result.then(function (result) {
                $rootScope.isShopPublic = result;
            });
        };
    })
        .controller('publicShopCtrl',  function($scope,$rootScope,$modalInstance,shop, $timeout,shop_data) {
        $rootScope.shop = shop_data;
        if($rootScope.isShopPublic == false || $rootScope.isShopPublic == undefined && ($rootScope.actions == undefined && $rootScope.count_actions == undefined)) {
            $scope.activity_indicator = true;
            shop.checkShop().success(function(r) {
                if(r.data.success) {
                    $rootScope.actions = r.data.actions;
                    $rootScope.count_actions = r.data.actions_count;
                    $scope.activity_indicator = false;

                    if($rootScope.count_actions == 0 && !(!$rootScope.settingStatus.taxes || !$rootScope.settingStatus.shipping || !$rootScope.settingStatus.payments)) {
                        $scope.public = true;
                        $scope.publicShop();
                    }
                }
            });
        }
        $scope.ok = function () {
            $modalInstance.close(true);
        };
        $scope.refreshPage = function () {
            $modalInstance.close(true);
            //location.reload();
        };
        $scope.cancel = function () {
            $modalInstance.dismiss('cancel');
        };

        $scope.publicShop = function() {
            $scope.activity_indicator = true;
            $timeout(function() {
                shop.publicShop().success(function(r) {
                    if(r.data.success) {
                        $scope.public = true;
                        $scope.activity_indicator = false;
                    }
                });
            },1000);
        }
    })
        .controller('AppStoreCtrl',  function($scope, app_store) {
        $scope.catsapps = [];
        app_store.getApplicationsGroupByCategories().success(function(r) {
            $scope.catsapps = r.data.catsapps;
            for(var k in $scope.catsapps) {
                $scope.catsapps[k].ofset = 0;
                $scope.catsapps[k].limit = 5;
                $scope.catsapps[k].stepper = 5;
            }
        });
        $scope.viewOther = function(cate) {
            if(cate.Application.length > cate.limit) {
                cate.ofset += cate.stepper;
                cate.limit += cate.stepper;
            }
        }
        $scope.viewRetry = function(cate) {
            cate.ofset -= cate.stepper;
            cate.limit -= cate.stepper;
        }
        $scope.retry = function(cate) {
            if(cate.ofset == 0) {
                return false;
            } else {
                return true;
            }
        }
    })
        .controller('SidebarCtrl',  function($rootScope,shop) {
        $rootScope.settingStatus = {
            taxes            : true,
            shipping         : true,
            payments         : true
        };
        shop.checkStatus().success(function(r){
            $rootScope.settingStatus = r.data;
        });
    })
        .controller('ProductCtrl', function($scope,product,tags,categories,FileUploader,$location,$activityIndicator,shop,$modal,$filter,$routeParams) {
            $activityIndicator.startAnimating();
            $scope.product = {
                content             : {
                    name                : '',
                    uri                 : '',
                    description         : '',
                    meta_description    : '',
                    meta_keywords       : '',
                    language_id         : $routeParams.language_id
                },
                images              : [],
                tags                : [],
                price               : '',
                shop_tax_id         : null,
                product_category_id : null,
                shipping_group_id   : null,
                quantity            : 0,
                type_quantity       : 0,
                published           : 0,
                order               : null

            };
            $scope.taxes            = [];
            $scope.shipments        = [];
            $scope.collections      = [];
            $scope.successSave      = false;
            $scope.errorSave        = false;
            $scope.onLoading        = false;
            $scope.progress         = 0;
            $scope.current_image    = '';
            $scope.count_image      = 0;
            $scope.success_images   = [];
            $scope.error_images     = [];

            product.load_data().success(function(r){
                $scope.taxes        = r.data.taxes;
                $scope.shipments    = r.data.shipments;
                $scope.collections  = r.data.collections;
                $activityIndicator.stopAnimating();
            });

            $scope.loadTags = function (query) {
                return tags.find(query);
            };

            $scope.openSeo = function (size) {
                $modal.open({
                    templateUrl: '/view/pages/seo.html',
                    controller: 'SeoModalCtrl',
                    size: size,
                    resolve: {
                        entity: function () {
                            return $scope.product.content;
                        }
                    }
                });
            };

            $scope.addCollectionModal = function () {
                var modalInstance = $modal.open({
                    templateUrl: '/view/product_categories/modal.html',
                    controller: 'modalCollectionCtrl',
                    resolve: {
                        collections_list: function () {
                            return $scope.collections;
                        }
                    }
                });
                modalInstance.result.then(function (data) {
                    $scope.collections                  = data.tree;
                    $scope.product.product_category_id  = data.collection_id;
                });
            };

            $scope.taxModal = function (size) {
                var modalInstance = $modal.open({
                    templateUrl: '/view/taxes/add.html',
                    controller: 'addTaxCtrl',
                    size: size,
                    resolve: {
                        taxes: function () {
                            return $scope.taxes
                        }
                    }
                });
                modalInstance.result.then(function (taxItem) {
                    $scope.product.shop_tax_id = taxItem.id;
                });
            };

            $scope.addShippingModal = function(){
                var modalInstance = $modal.open({
                    templateUrl: '/view/shipping_groups/add.html',
                    controller: 'addShippingGroupCtrl',
                    resolve: {
                        shipments: function () {
                            return $scope.shipments;
                        }
                    }
                });
                modalInstance.result.then(function (new_shipping) {
                    if(new_shipping) {
                        $scope.product.shipping_group_id = new_shipping.id;
                    }
                });
            }

            $scope.changeTotalPrice = function(){
                var current_tax = $filter('filter')($scope.taxes, {"id": $scope.product.shop_tax_id});
                var tax_value = 0;
                if(current_tax.length && $scope.product.price) {
                    if(!current_tax[0].product_tax) {
                        tax_value = current_tax[0].tax;
                        tax_value = ($scope.product.price * tax_value) / 100;
                    }
                }
                var total_price = $scope.product.price + tax_value;
                total_price = $filter('number')(total_price, 2);
                $scope.total_price = total_price;
            }

            $scope.updateURL = function () {
                var name = $scope.product.content.name;
                if (name) {
                    $scope.product.content.uri = name.replace(/\W/g, '-');
                } else {
                    $scope.product.content.uri = '';
                }
            };

            $scope.uploader = new FileUploader({
                url: "/products/loadImage",
                onBeforeUploadItem: function(item){
                    item.url = $scope.uploader.url+".json";
                    $scope.onLoading = true;
                    $scope.current_image = item._file.name;
                },
                onCompleteAll: function() {
                    $activityIndicator.stopAnimating();
                    $scope.successSave  = true;
                    $scope.onLoading    = false;
                },
                onProgressAll: function(progress){
                    $scope.progress = progress;
                },
                onSuccessItem: function(item, response){
                    if(response.data.success) {
                        $scope.count_image++;
                        $scope.success_images.push(item._file.name);
                    }
                },
                onErrorItem: function(item){
                    $scope.error_images.push(item._file.name);
                }
            });
            $scope.sortableOptions = {
                stop: function() {
                    for (var index in $scope.uploader.queue) {
                        $scope.uploader.queue[index].order = index;
                    }
                }
            };

            $scope.save = function () {
                product.save($scope.product).success(function (r) {
                    if (r.data.success) {
                        if ($scope.uploader.queue.length) {
                            $scope.uploader.url = "/products/loadImage/" + r.data.product_id;
                            $scope.uploader.uploadAll();
                        } else {
                            $scope.successSave = true;
                        }
                    } else {
                        $scope.errorSave = true;
                    }
                });
            }
        })
        .controller('ProductEditCtrl', function($scope,product,tags,FileUploader,$routeParams,$activityIndicator,$filter,$modal,toaster) {
            $scope.product = {
                content             : {
                    name                : '',
                    uri                 : '',
                    description         : '',
                    meta_description    : '',
                    meta_keywords       : ''
                },
                images              : [],
                tags                : [],
                price               : '',
                shop_tax_id         : null,
                product_category_id : null,
                shipping_group_id   : null,
                quantity            : 0,
                type_quantity       : 0,
                published           : 0

            };
            $scope.taxes        = [];
            $scope.shipments    = [];
            $scope.collections  = [];

            product.load_product($routeParams.product_id,$routeParams.language_id).success(function(r){
                $scope.product      = r.product;
                $scope.taxes        = r.data.taxes;
                $scope.shipments    = r.data.shipments;
                $scope.collections  = r.data.collections;
                $scope.changeTotalPrice();
                $activityIndicator.stopAnimating();
            });

            $scope.loadTags = function (query) {
                return tags.find(query);
            };

            $scope.openSeo = function (size) {
                $modal.open({
                    templateUrl: '/view/pages/seo.html',
                    controller: 'SeoModalCtrl',
                    size: size,
                    resolve: {
                        entity: function () {
                            return $scope.product.content;
                        }
                    }
                });
            };

            $scope.addCollectionModal = function () {
                var modalInstance = $modal.open({
                    templateUrl: '/view/product_categories/modal.html',
                    controller: 'modalCollectionCtrl',
                    resolve: {
                        collections_list: function () {
                            return $scope.collections;
                        }
                    }
                });
                modalInstance.result.then(function (data) {
                    $scope.collections                  = data.tree;
                    $scope.product.product_category_id  = data.collection_id;
                });
            };

            $scope.taxModal = function (size) {
                var modalInstance = $modal.open({
                    templateUrl: '/view/taxes/add.html',
                    controller: 'addTaxCtrl',
                    size: size,
                    resolve: {
                        taxes: function () {
                            return $scope.taxes
                        }
                    }
                });
                modalInstance.result.then(function (taxItem) {
                    $scope.product.shop_tax_id = taxItem.id;
                });
            };

            $scope.addShippingModal = function(){
                var modalInstance = $modal.open({
                    templateUrl: '/view/shipping_groups/add.html',
                    controller: 'addShippingGroupCtrl',
                    resolve: {
                        shipments: function () {
                            return $scope.shipments;
                        }
                    }
                });
                modalInstance.result.then(function (new_shipping) {
                    if(new_shipping) {
                        $scope.product.shipping_group_id = new_shipping.id;
                    }
                });
            }

            $scope.changeTotalPrice = function(){
                var current_tax = $filter('filter')($scope.taxes, {"id": $scope.product.shop_tax_id});
                var tax_value = 0;
                if(current_tax.length && $scope.product.price) {
                    if(!current_tax[0].product_tax) {
                        tax_value = current_tax[0].tax;
                        tax_value = ($scope.product.price * tax_value) / 100;
                    }
                }
                var total_price = $scope.product.price + tax_value;
                total_price = $filter('number')(total_price, 2);
                $scope.total_price = total_price;
            }

            $scope.updateURL = function () {
                var name = $scope.product.content.name;
                if (name) {
                    $scope.product.content.uri = name.replace(/\W/g, '-');
                } else {
                    $scope.product.content.uri = '';
                }
            };

            $scope.save = function () {
                $activityIndicator.startAnimating();
                product.update($scope.product).success(function (r) {
                    if (r.data.success) {
                        toaster.pop('success', "", r.data.message);
                        $activityIndicator.stopAnimating();
                    } else {
                        toaster.pop('error', "", r.data.message);
                        $activityIndicator.stopAnimating();
                    }
                });
            };

            $scope.uploader = new FileUploader({
                url: "/products/loadImage",
                onBeforeUploadItem: function(item){
                    item.url = $scope.uploader.url;
                    $scope.current_image = item._file.name;
                    $scope.onLoading = true;
                },
                onAfterAddingAll : function(item)  {
                    $scope.progress = 0;
                    $scope.uploader.url = "/products/loadImage/" + $routeParams.product_id + ".json";
                    $scope.uploader.uploadAll();
                },
                onProgressAll: function(progress){
                    $scope.progress = progress;
                },
                onSuccessItem: function(item, response){
                    if(response.data.success) {
                        var newImage = {
                            image       : response.data.url,
                            name        : response.data.name,
                            order       : null
                        }
                        $scope.product.images.push(newImage);
                        $scope.count_image++;
                    }
                    $scope.onLoading = false;
                },
                onErrorItem: function(item, response, status, headers){
                    $scope.onLoading = false;
                }
            });
            $scope.sortableOptions = {
                stop: function(e, ui) {
                    for (var index in $scope.images) {
                        $scope.product.images[index].order = index;
                    }
                }
            };
            $scope.deleteImage = function(image) {
                if(image) {
                    product.remove_image($routeParams.product_id, image).success(function (r) {
                        if(r.data.success) {
                            angular.forEach($scope.product.images, function(value, key) {
                                if(value.name == image) {
                                    $scope.product.images.splice(key, 1);
                                }
                            });
                        }
                    });
                }
            };
        })
        .controller('SettingsCtrl',function($scope, $filter,shop,$activityIndicator,FileUploader,toaster,$timeout, $rootScope){
            $activityIndicator.startAnimating();
            $scope.shop = {};
            $scope.images = [];
            $scope.logo = '/asset/images/no_logo.png';
            $scope.cover = '/asset/images/default_cover.jpg';
            $scope.loadingLogo = false;
            $scope.loadingCover = false;
            $scope.onLoading    = false;
            $scope.latitude     = 42.7945263;
            $scope.longitude    = 11.7693389;
            $scope.zoom         = 6;
            $scope.details      = {};
            $scope.mapInit      = false;
            shop.loadSettings().success(function(r){
                $scope.shop         = r.shop;
                $scope.countries    = r.countries;
                $scope.categories   = r.categories;
                $scope.currencies   = r.currencies;
                if(r.logo) {
                    $scope.logo = r.logo;
                }
                if(r.cover) {
                    $scope.cover = r.cover;
                }
                $activityIndicator.stopAnimating();
                $scope.$watchCollection("details", function (data) {
                    if(data.geometry) {
                        if(data.geometry.location) {
                            /*/*$scope.shop.latitude    =  data.geometry.location.k;
                            $scope.shop.longitude   =  data.geometry.location.D;
                            $scope.shop.latitude    =  data.geometry.location.H;
                            $scope.shop.longitude   =  data.geometry.location.L;*/
                            $scope.shop.longitude   =  data.geometry.location.lng();
                            $scope.shop.latitude    =  data.geometry.location.lat();
                            $scope.map.setCenter(new google.maps.LatLng($scope.shop.latitude, $scope.shop.longitude));
                            for(var key in $scope.map.markers) {
                                $scope.map.markers[key].setPosition(new google.maps.LatLng($scope.shop.latitude, $scope.shop.longitude));
                            }
                        }
                    }
                });
                var latlon = new google.maps.LatLng($scope.latitude, $scope.longitude);
                if (r.shop.latitude && r.shop.longitude) {
                    latlon = new google.maps.LatLng($scope.shop.latitude, $scope.shop.longitude);
                }
                $scope.updateMarker = function(data){
                    $scope.shop.latitude    =  data.latLng.k;
                    $scope.shop.longitude   =  data.latLng.D;
                }
                $scope.$on('mapInitialized', function(event, map) {
                    setTimeout(function(){
                        map.setCenter(latlon);
                        for (var key in map.markers) {
                            map.markers[key].setPosition(latlon);
                        }
                        $scope.map = map;
                    },2000);
                });
                $scope.mapInit = true;
            });
            $scope.save = function() {
                $activityIndicator.startAnimating();
                shop.updateShop($scope.shop).success(function (r) {
                    if (r.data.success) {
                        toaster.pop('success', "", r.data.message);
                    } else {
                        toaster.pop('error', "", r.data.message);
                    }
                    $rootScope.isShopPublic = $scope.shop.is_public;
                    $activityIndicator.stopAnimating();
                })
            }

            $scope.uploaderLogo = new FileUploader({
                url: "/shops/uploadLogo.json",
                onBeforeUploadItem: function(item){
                    $scope.loadingLogo = true;
                    item.url = $scope.uploaderLogo.url;
                },
                onAfterAddingAll : function()  {
                    $scope.uploaderLogo.uploadAll();
                },
                onSuccessItem: function(item, response){
                    if(response.data.success) {
                        $scope.logo = response.data.url;
                    }
                    $scope.loadingLogo = false;
                }
            });
            $scope.uploaderCover = new FileUploader({
                url: "/shops/uploadCover.json",
                onBeforeUploadItem: function(item){
                    $scope.loadingCover = true;
                    item.url = $scope.uploaderCover.url;
                },
                onAfterAddingAll : function()  {
                    $scope.uploaderCover.uploadAll();
                },
                onSuccessItem: function(item, response){
                    if(response.data.success) {
                        $scope.cover = response.data.url;
                    }
                    $scope.loadingCover = false;
                }
            });

        })
        .controller('TaxInformationCtrl',function($scope,shop,$activityIndicator,toaster){
            $activityIndicator.startAnimating();
            $scope.shop = {};
            shop.loadTaxInformation().success(function(r){
                $scope.shop = r.shop;
                $scope.countries = r.countries;
                $scope.currencies = r.currencies;
                $activityIndicator.stopAnimating();
            });
            $scope.save = function(){
                $activityIndicator.startAnimating();
                shop.updateTaxInformation($scope.shop).success(function(r){
                    if(r.data.success) {
                        toaster.pop('success', "", r.data.message);
                    } else {
                        toaster.pop('error', "", r.data.message);
                    }
                    $activityIndicator.stopAnimating();
                })
            }
        })
        .controller('ShopNotifiesCtrl',function($scope,$activityIndicator,notifies,toaster,$rootScope){

            $activityIndicator.startAnimating();
            $scope.timeline = [];
            notifies.shop_timeline().success(function(r){
                $scope.timeline = r.notifies;
                $activityIndicator.stopAnimating();
            });

        })
        .controller('TaxsCtrl',function($scope, $filter,shop,$activityIndicator,$modal,$rootScope){
            $activityIndicator.startAnimating();
            shop.loadTaxes().success(function(r){
                $scope.taxes = r.taxes;
                $activityIndicator.stopAnimating();
            });
            $scope.deleteTax = function(tax_id){
                shop.deleteTax(tax_id).success(function(r){
                    angular.forEach($scope.taxes, function(value, key) {
                        if(value.id== tax_id) {
                            $scope.taxes.splice(key, 1);
                            shop.checkStatus().success(function(r){
                                $rootScope.settingStatus = r.data;
                            });
                        }
                    });
                });
            }
            $scope.open = function (size) {
                $modal.open({
                    templateUrl: '/view/taxes/add.html',
                    controller: 'addTaxCtrl',
                    size: size,
                    resolve: {
                        taxes: function () {
                            return $scope.taxes;
                        }
                    }
                });
            };
            $scope.editModal = function (tax_id) {
                $modal.open({
                    templateUrl: '/view/taxes/edit.html',
                    controller: 'editTaxCtrl',
                    resolve: {
                        taxes: function () {
                            return $scope.taxes;
                        },
                        tax_id: function(){
                            return tax_id;
                        }
                    }
                });
            };
        })
        .controller('addTaxCtrl',function($scope, $modalInstance,shop,taxes,$rootScope) {
            $scope.tax = {
                name: "",
                country_tax: new Array(),
                tax: 0,
                product_tax: true,
                label: ''
            };
            $scope.taxes = taxes;
            $scope.ok = function () {
                shop.addTax($scope.tax).success(function (r) {
                    if (r.data) {
                        $scope.tax.id = r.data.tax_id;
                        $scope.taxes.push($scope.tax);
                        $modalInstance.close($scope.tax);
                        shop.checkStatus().success(function(r){
                            $rootScope.settingStatus = r.data;
                        });
                    }
                });
            };
            $scope.updateLabel = function () {
                $scope.tax.label = $scope.tax.name + " " + $scope.tax.tax + " %";
            }
            $scope.cancel = function () {
                $modalInstance.dismiss('cancel');
            };
        })
        .controller('editTaxCtrl',function($scope, $modalInstance,shop,taxes,tax_id){
            $scope.taxes = taxes;
            shop.loadTax(tax_id).success(function(r){
                if(r.data.success) {
                    $scope.tax = r.data.tax;
                }
            })
            $scope.ok = function () {
                shop.updateTax($scope.tax).success(function(r){
                    if(r.data) {
                        angular.forEach($scope.taxes, function(value, key) {
                            if(value.id == $scope.tax.id) {
                                $scope.taxes[key] = $scope.tax;
                            }
                        });
                        $modalInstance.close($scope.tax);
                    }
                });
            };
            $scope.updateLabel = function() {
                $scope.tax.label = $scope.tax.name + " " + $scope.tax.tax + " %";
            }
            $scope.cancel = function () {
                $modalInstance.dismiss('cancel');
            };
        })
        .controller('ProductsCtrl',function($scope, $filter,shop,product,collections,$activityIndicator,toaster,$modal,Languages){
            $activityIndicator.startAnimating();

            $scope.shop_languages       = [];
            $scope.selected_language    = '';
            $scope.data                 = {};
            $scope.numPerPageOpt        = [5, 10, 20];
            $scope.numPerPage           = $scope.numPerPageOpt[1];
            $scope.currentPage          = 1;
            $scope.currentPageStores    = [];
            $scope.products             = [];
            $scope.totalProducts        = 0;
            $scope.row                  = 'created';
            $scope.orderType            = 'ASC';
            $scope.obj                  = {};
            $scope.obj.selectAll        = false;
            $scope.obj.deselectAll      = true;
            $scope.taxes                = [];
            $scope.shipments            = [];
            $scope.collections          = [];

            Languages.getShopLanguages().success(function(r) {
                $scope.shop_languages       = r.languages;
                $scope.selected_language    = r.default_language;
                $scope.select($scope.currentPage);
            });

            product.load_data().success(function(r){
                $scope.taxes        = r.data.taxes;
                $scope.shipments    = r.data.shipments;
                $scope.collections  = r.data.collections;
            });

            $scope.addToCollection = function(collection_id){
                var products = $filter('filter')($scope.products, {"selected": true});
                $scope.productSelected = [];
                angular.forEach(products, function(value, key) {
                    $scope.productSelected.push(products[key].id);
                });
                product.addToCollection($scope.productSelected,collection_id).success(function(r){
                    if(r.data.success) {
                        $scope.select($scope.currentPage);
                        toaster.pop('success', "", r.data.message);
                    } else {
                        toaster.pop('error', "", r.data.message);
                    }
                });
            }

            $scope.addToShippingGroup = function(shipping_group_id){
                var products = $filter('filter')($scope.products, {"selected": true});
                $scope.productSelected = [];
                angular.forEach(products, function(value, key) {
                    $scope.productSelected.push(products[key].id);
                });
                product.addToShippingGroup($scope.productSelected,shipping_group_id).success(function(r){
                    if(r.data.success) {
                        toaster.pop('success', "", r.data.message);
                    } else {
                        toaster.pop('error', "", r.data.message);
                    }
                });
            }

            $scope.deleteProducts = function(product_url){
                var products = $filter('filter')($scope.products, {"selected": true});
                $scope.productSelected = [];
                angular.forEach(products, function(value, key) {
                    $scope.productSelected.push(products[key].id);
                });
                product.deleteAll($scope.productSelected).success(function(r){
                    if(r.data.success) {
                        toaster.pop('success', "", r.data.message);
                        $scope.select($scope.currentPage);
                    } else {
                        toaster.pop('error', "", r.data.message);
                    }
                });
            }

            $scope.objectKeys = function(obj){
                return Object.keys(obj);
            }

            $scope.clone = function(product_id){
                product.clone(product_id).success(function(r){
                    if(r.data.success) {
                        toaster.pop('success', "", r.data.message);
                        $scope.select($scope.currentPage);
                    } else {
                        toaster.pop('error', "", r.data.message);
                    }
                });
            }

            $scope.updateSelected = function() {
                var selected = false;
                angular.forEach($scope.products, function(value, key) {
                    if($scope.products[key].selected == true) {
                        selected = true;
                    }
                });
                if(!selected) {
                    $scope.obj.selectAll = false;
                    $scope.obj.deselectAll = true;
                }
            }

            $scope.toggleList = function(){
                if($scope.obj.deselectAll) {
                    $scope.obj.deselectAll  = false;
                    $scope.obj.selectAll    = true;
                    angular.forEach($scope.products, function(value, key) {
                        $scope.products[key].selected = true;
                    });
                } else {
                    $scope.obj.deselectAll  = true;
                    $scope.obj.selectAll    = false;
                    angular.forEach($scope.products, function(value, key) {
                        $scope.products[key].selected = false;
                    });
                }
            }

            $scope.checkSelected = function(){
                var keepGoing = false;
                angular.forEach($scope.products, function(value, key) {
                    if($scope.products[key].selected) {
                        keepGoing = true;
                    }
                });
                return keepGoing;
            }

            $scope.deleteAll = function(){
                var selected = [];
                angular.forEach($scope.products, function(value, key) {
                    if(value.selected) {
                        selected.push(value.id);
                    }
                });
                product.delete_all(selected).success(function(r){
                    if(r.data.success) {
                        toaster.pop('success', "", r.data.message);
                    } else {
                        toaster.pop('error', "", r.data.message);
                    }
                    $scope.select($scope.currentPage);
                });
            }

            $scope.selectLanguage = function(lang_id) {
                $scope.selected_language = lang_id;
                $scope.select($scope.currentPage);
            }

            $scope.select = function(page_act) {
                $activityIndicator.startAnimating();
                $scope.obj.deselectAll  = true;
                $scope.obj.selectAll    = false;
                product.get_list($scope.numPerPage, page_act, $scope.row, $scope.orderType, $scope.selected_language).success(function(r){
                    $scope.products             = r.products;
                    $scope.totalProducts        = r.count;
                    $activityIndicator.stopAnimating();
                }).error(function(){
                    $scope.select(1);
                });
            };

            $scope.delete = function(id){
                product.delete(id).success(function(r){
                    if(r.data.success) {
                        $scope.select($scope.currentPage);
                        toaster.pop('success', "", r.data.message);
                    } else {
                        toaster.pop('error', "", r.data.message);
                    }
                })
            }

            $scope.onFilterChange = function() {
                $scope.select(1);
                $scope.currentPage = 1;
                return $scope.row = '';
            };
            $scope.onNumPerPageChange = function() {
                $scope.select(1);
                return $scope.currentPage = 1;
            };
            $scope.onOrderChange = function() {
                $scope.select(1);
                return $scope.currentPage = 1;
            };
            $scope.order = function(rowName,orderType) {
                if ($scope.row === rowName && $scope.orderType === orderType) {
                    return;
                }
                $activityIndicator.startAnimating();
                $scope.row = rowName;
                $scope.orderType = orderType;
                return $scope.onOrderChange();
            };

            /*
            var init;
            $scope.data                 = {};
            $scope.numPerPageOpt        = [5, 10, 20];
            $scope.numPerPage           = $scope.numPerPageOpt[1];
            $scope.currentPage          = 1;
            $scope.currentPageStores    = [];
            $scope.products             = [];
            $scope.collections          = [];
            $scope.shipping_group_list  = [];
            $scope.totalProducts        = 0;
            $scope.slectedCollectionId  = false;
            $scope.row                  = 'name';
            $scope.orderType            = 'ASC';
            $scope.productSelected      = [];
            $scope.selectAll            = false;
            $scope.deselectAll          = true;
            $scope.checkSelected = function(){
                var keepGoing = false;
                angular.forEach($scope.products, function(value, key) {
                    if($scope.products[key].selected) {
                        keepGoing = true;
                    }
                });
                return keepGoing;
            }
            shop.loadShippingGroups().success(function(r){
                $scope.shipping_group_list = r.shipping_group_list;
            })

            $scope.toggleList = function(){
                if($scope.deselectAll) {
                    $scope.deselectAll  = false;
                    $scope.selectAll    = true;
                    angular.forEach($scope.products, function(value, key) {
                        $scope.products[key].selected = true;
                    });
                } else {
                    $scope.deselectAll  = true;
                    $scope.selectAll    = false;
                    angular.forEach($scope.products, function(value, key) {
                        $scope.products[key].selected = false;
                    });
                }
            }

            $scope.addCollectionModal = function () {
                var modalInstance = $modal.open({
                    templateUrl: '/view/collections/modal_add.html',
                    controller: 'addCollectionModal',
                    resolve: {
                        collections_list: function () {
                            return $scope.collections;
                        }
                    }
                });
                modalInstance.result.then(function (collection_id) {
                    $scope.addToColletcion(collection_id)
                });
            };

            $scope.addToColletcion = function(collection_id){
                var products = $filter('filter')($scope.products, {"selected": true});
                $scope.productSelected = [];
                angular.forEach(products, function(value, key) {
                    $scope.productSelected.push(products[key].id);
                });
                product.addToCollection($scope.productSelected,collection_id).success(function(r){
                    if(r.data.success) {
                        var collection = $filter('filter')($scope.collections, {"id": collection_id});
                        var collection_name = "";
                        if(collection[0]) {
                            collection_name = collection[0].name
                            angular.forEach($scope.productSelected, function(value, key) {
                                var product = $filter('filter')($scope.products, {"id":  $scope.productSelected[key]});
                                if(product[0]) {
                                    product[0].collections = [];
                                    product[0].collections.push(collection_name);
                                }
                            });
                        }
                        toaster.pop('success', "", r.data.message +" '"+collection_name+"'");
                    } else {
                        toaster.pop('error', "", r.data.message);
                    }
                });
            }

            $scope.addShippingGroup = function () {
                $modal.open({
                    templateUrl: '/view/shipping_groups/add.html',
                    controller: 'addShippingGroupCtrl',
                    resolve: {
                        shipments: function () {
                            return $scope.shipping_group_list;
                        }
                    }
                });
            };

            $scope.addToShippingGroup = function(shipping_group_id){
                var products = $filter('filter')($scope.products, {"selected": true});
                $scope.productSelected = [];
                angular.forEach(products, function(value, key) {
                    $scope.productSelected.push(products[key].id);
                });
                product.addToShippingGroup($scope.productSelected,shipping_group_id).success(function(r){
                    if(r.data.success) {
                        toaster.pop('success', "", r.data.message);
                    } else {
                        toaster.pop('error', "", r.data.message);
                    }
                });
            }

            collections.list().success(function(r){
                $scope.collections = r.collections;
            });
            $scope.select = function(page) {
                $activityIndicator.startAnimating();
                $scope.deselectAll  = true;
                $scope.selectAll    = false;
                product.getList($scope.numPerPage,page,$scope.row,$scope.orderType).success(function(r){
                    $scope.products = r.products;
                    $scope.totalProducts = r.count;
                    $activityIndicator.stopAnimating();
                });
            };

            $scope.onFilterChange = function() {
                $scope.select(1);
                $scope.currentPage = 1;
                return $scope.row = '';
            };
            $scope.onNumPerPageChange = function() {
                $scope.select(1);
                return $scope.currentPage = 1;
            };
            $scope.onOrderChange = function() {
                $scope.select(1);
                return $scope.currentPage = 1;
            };
            $scope.order = function(rowName,orderType) {
                if ($scope.row === rowName && $scope.orderType === orderType) {
                    return;
                }
                $activityIndicator.startAnimating();
                $scope.row = rowName;
                $scope.orderType = orderType;
                return $scope.onOrderChange();
            };


            $scope.deleteProducts = function(product_url){
                var products = $filter('filter')($scope.products, {"selected": true});
                $scope.productSelected = [];
                angular.forEach(products, function(value, key) {
                    $scope.productSelected.push(products[key].id);
                });
                product.deleteAll($scope.productSelected).success(function(r){
                    if(r.data.success) {
                        toaster.pop('success', "", r.data.message);
                        $scope.select($scope.currentPage);
                    } else {
                        toaster.pop('error', "", r.data.message);
                    }
                });
            }

            $scope.deleteProduct = function(product_url){
                product.delete(product_url).success(function(r){
                    if(r.data.success) {
                        toaster.pop('success', "", r.data.message);
                        $scope.select($scope.currentPage);
                    } else {
                        toaster.pop('error', "", r.data.message);
                    }
                });
            }
            init = function() {
                return $scope.select($scope.currentPage);
            };
            return init();*/
        })
        .controller('OrdersCtrl',function($scope, $filter,orders,$activityIndicator){
            var init;
            $scope.numPerPageOpt        = [5, 10, 20];
            $scope.data                 = {};
            $scope.numPerPage           = $scope.numPerPageOpt[1];
            $scope.currentPage          = 1;
            $scope.currentPageStores    = [];
            $scope.orders             = [];
            $scope.totalOrders       = 0;
            $scope.row                  = 'created';
            $scope.orderType            = 'DESC';
            $scope.select = function(page) {
                $activityIndicator.startAnimating();
                orders.getList($scope.numPerPage,page,$scope.row,$scope.orderType).success(function(r){
                    $scope.orders = r.orders;
                    $scope.totalOrders = r.count;
                    $activityIndicator.stopAnimating();
                });
            };
            $scope.onFilterChange = function() {
                $scope.select(1);
                $scope.currentPage = 1;
                return $scope.row = '';
            };
            $scope.onNumPerPageChange = function() {
                $scope.select(1);
                return $scope.currentPage = 1;
            };
            $scope.onOrderChange = function() {
                $scope.select(1);
                return $scope.currentPage = 1;
            };
            $scope.order = function(rowName,orderType) {
                if ($scope.row === rowName && $scope.orderType === orderType) {
                    return;
                }
                $activityIndicator.startAnimating();
                $scope.row = rowName;
                $scope.orderType = orderType;
                return $scope.onOrderChange();
            };
            init = function() {
                return $scope.select($scope.currentPage);
            };
            return init();
        })

        .controller('OrderCtrl',function($scope,$routeParams,orders,$activityIndicator,toaster){
            $activityIndicator.startAnimating();
            $scope.order = {};
            $scope.order_status = [];
            orders.load_order($routeParams.order_id).success(function(r){
                $scope.order = r.order;
                $scope.order_status = r.order_status;
                $activityIndicator.stopAnimating();

            });
            $scope.changeStatus = function(){
                orders.changeStatus($routeParams.order_id,$scope.order.order.status).success(function(r){
                    if(r.data.success) {
                        toaster.pop('success', "", r.data.message);
                    } else {
                        toaster.pop('error', "", r.data.message);
                    }
                });
            }
        })

        .controller('OrderRequestCtrl',function($scope,$routeParams,orders,$activityIndicator,toaster){
            $activityIndicator.startAnimating();
            $scope.order = {};
            orders.load_order($routeParams.order_id).success(function(r){
                $scope.order = r.order;
                $scope.order_status = r.order_status;
                $activityIndicator.stopAnimating();

            });
            $scope.changeAilability = function(){
                orders.changeAilability($routeParams.order_id,$scope.order.order.available).success(function(r){
                    if(r.data.success) {
                        toaster.pop('success', "", r.data.message);
                    } else {
                        toaster.pop('error', "", r.data.message);
                    }
                });
            }
        })

        .controller('appCtrl',function($scope, $routeParams, app_store, $activityIndicator){
            $activityIndicator.startAnimating();
            $scope.applicationData = {};
            $scope.category = {};
            $scope.company = {};
            $scope.category.limit = 5;
            $scope.category.ofset = 0;
            $scope.category.stepper = 5;
            $scope.company.stepper = 5;
            $scope.company.limit = 5;
            $scope.company.ofset = 0;

            $scope.getToken = function(){
                app_store.getToken().success(function(r){
                    var client = new braintree.api.Client({clientToken: r.data.token});
                    client.tokenizeCard({number: "4111111111111111", expirationDate: "10/20"}, function (err, nonce) {
                        app_store.createPayment(nonce).success(function(r){

                        });
                    });
                });
            }

            $scope.slides = [
				{image: 'http://lorempixel.com/1800/400/technics/1/', description: 'Image 00'},
				{image: 'http://lorempixel.com/1800/400/technics/2/', description: 'Image 01'},
				{image: 'http://lorempixel.com/1800/400/technics/3/', description: 'Image 02'},
				{image: 'http://lorempixel.com/1800/400/technics/4/', description: 'Image 03'},
				{image: 'http://lorempixel.com/1800/400/technics/5/', description: 'Image 04'}
			];
			$scope.direction = 'left';
			$scope.currentIndex = 0;
			$scope.setCurrentSlideIndex = function (index) {
				$scope.direction = (index > $scope.currentIndex) ? 'left' : 'right';
				$scope.currentIndex = index;
			};
			$scope.isCurrentSlideIndex = function (index) {
				return $scope.currentIndex === index;
			};
			$scope.prevSlide = function () {
				$scope.direction = 'left';
				$scope.currentIndex = ($scope.currentIndex < $scope.slides.length - 1) ? ++$scope.currentIndex : 0;
			};
			$scope.nextSlide = function () {
				$scope.direction = 'right';$scope.currentIndex = ($scope.currentIndex > 0) ? --$scope.currentIndex : $scope.slides.length - 1;
			};

            // ritorna l'informazione dell'app, altre app dello sviluppatore, altre app simili
            app_store.getPageData($routeParams.app_url).success(function(r) {
                if(r.data.success) {
                    $scope.pageData = r.data;
                    $scope.installed = r.data.installed;
                    //$scope.loadingButton = false;
                    $scope.slides = r.data.appData.AppScreenshot;
                    $activityIndicator.stopAnimating();
                }
            });

            $scope.installApp = function(app_id) {
                app_store.installApp($routeParams.app_url, app_id).success(function() {
                    location.reload();
                });
            }

            $scope.removeApp = function(app_id) {
                app_store.removeApp($routeParams.app_url, app_id).success(function() {
                    location.reload();
                });
            }
            $scope.viewOther = function(cate, item) {
                if(cate.Application.length > item.limit) {
                    item.ofset += item.stepper;
                    item.limit += item.stepper;
                }
            }
            $scope.viewRetry = function(cate) {
                cate.ofset -= cate.stepper;
                cate.limit -= cate.stepper;
            }
            $scope.retry = function(item) {
                if(item.ofset == 0) {
                    return false;
                } else {
                    return true;
                }
            }

    })
        .controller('confirmCtrl',function($scope,$modalInstance){
            $scope.ok = function() {
                $modalInstance.close();
            };
            $scope.cancel = function() {
                $modalInstance.dismiss('cancel');
            };
    })
        .controller('ShipmentsCtrl',function($scope,$filter,shop,$activityIndicator,$modal,$rootScope){
            $activityIndicator.startAnimating();
            $scope.shipping_group_list = [];
            shop.loadShippingGroups().success(function(r){
                $scope.shipping_group_list = r.shipping_group_list;
                $activityIndicator.stopAnimating();
            })
            $scope.open = function () {
                $modal.open({
                    templateUrl: '/view/shipping_groups/add.html',
                    controller: 'addShippingGroupCtrl',
                    resolve: {
                        shipments: function () {
                            return $scope.shipping_group_list;
                        }
                    }
                });
            };
            $scope.editModal = function (shipping_group_id) {
                $modal.open({
                    templateUrl: '/view/shipping_groups/edit.html',
                    controller: 'editShippingGroupCtrl',
                    resolve: {
                        shipping_group_list: function () {
                            return $scope.shipping_group_list;
                        },
                        shipping_group_id : function(){
                            return shipping_group_id;
                        }
                    }
                });
            };

            $scope.deleteShippingGroup = function(shipping_group_id){
                shop.deleteShippingGroup(shipping_group_id).success(function(r){
                    angular.forEach($scope.shipping_group_list, function(value, key) {
                        if($scope.shipping_group_list[key].id == shipping_group_id) {
                            $scope.shipping_group_list.splice(key, 1);
                        }
                    });
                });
            }


            $scope.addShippingToGroup = function (shipping_group_id) {
                $modal.open({
                    templateUrl: '/view/shipments/add.html',
                    controller: 'addShippingCtrl',
                    resolve: {
                        shipping_group_list: function () {
                            return $scope.shipping_group_list;
                        },
                        shipping_group_id : function(){
                            return shipping_group_id;
                        }
                    }
                });
            };

            $scope.editShipping = function (shipping_id) {
                $modal.open({
                    templateUrl: '/view/shipments/edit.html',
                    controller: 'editShippingCtrl',
                    resolve: {
                        shipping_group_list: function () {
                            return $scope.shipping_group_list;
                        },
                        shipping_id : function(){
                            return shipping_id;
                        }
                    }
                });
            }

            $scope.deleteShipping = function(shipping_id){
                shop.deleteShipping(shipping_id).success(function(r){
                    if(r.data.success) {
                        angular.forEach($scope.shipping_group_list, function(value, key) {
                            angular.forEach($scope.shipping_group_list[key].shipments, function(v, k) {
                                if($scope.shipping_group_list[key].shipments[k].id == shipping_id) {
                                    $scope.shipping_group_list[key].shipments.splice(k, 1);
                                }
                            });
                        });
                    }
                });
            }
    })
        .controller('addShippingGroupCtrl',function($scope,$filter,$modalInstance,shop,countries,shipments,$rootScope,$q){

            $scope.taxes = [];
            $scope.countries = [];
            $scope.shipments = shipments;
            $q.all([
                shop.loadTaxes().success(function(r){
                    $scope.taxes = r.taxes;
                }),
                countries.get().success(function(r){
                    $scope.countries = r.countries;
                })
            ]).then(function(){
                $scope.shipping = {
                    name: "",
                    countries: [115],
                    price: 0,
                    additional_price: 0,
                    start_day: 3,
                    end_day: 10,
                    shop_tax_id: null,
                    country_type: 1
                };
                $scope.reset_country = function(){
                    $scope.shipping.countries = [];
                }
                if($scope.taxes.length) {
                    $scope.shipping.shop_tax_id = $scope.taxes[0].id;
                }
                $scope.ok = function () {
                    shop.addShippingGroup($scope.shipping).success(function(r){
                        if(r.data.success) {
                            var newShipping = r.data.shipping_group
                            $scope.shipments.push(newShipping);
                            shop.checkStatus().success(function(r){
                                $rootScope.settingStatus = r.data;
                            });
                            $modalInstance.close(newShipping);
                        }
                    });
                };
                $scope.cancel = function () {
                    $modalInstance.dismiss('cancel');
                };
            });

    }).controller('addShippingCtrl',function($scope,$filter,$modalInstance,shop,countries,shipping_group_list,shipping_group_id,$rootScope,$q){
            $scope.taxes            = [];
            $scope.countries        = [];
            $scope.shippign_config  = [];
            $scope.shipping = {
                countries: [115],
                price: 0,
                additional_price: 0,
                start_day: 3,
                end_day: 10,
                shop_tax_id: null,
                shipping_group_id: shipping_group_id,
                country_type: 1
            };
            $q.all([
                shop.loadTaxes().success(function(r){
                    $scope.taxes = r.taxes;
                    if($scope.taxes.length) {
                        $scope.shipping.shop_tax_id = $scope.taxes[0].id;
                    }
                }),
                countries.get().success(function(r){
                    $scope.countries = r.countries;
                }),
                shop.loadShippingConfig(shipping_group_id).success(function(r){
                    $scope.shippign_config = r;
                })
            ]).then(function(){

                angular.forEach($scope.shippign_config.unavailable_option.countries, function(value, key) {
                    angular.forEach($scope.countries, function(v,k) {
                        if($scope.countries[k].id == $scope.shippign_config.unavailable_option.countries[key].id) {
                            $scope.countries.splice(k, 1);
                        }
                    })
                });
                $scope.shipping.countries = [$scope.shippign_config.default_country.id];
                $scope.ok = function () {
                    shop.addShipping($scope.shipping).success(function(r){
                        if(r.data.success) {
                            var newShipping = r.data.shipping;
                            angular.forEach(shipping_group_list, function(value, key) {
                                if(value.id == shipping_group_id) {
                                    shipping_group_list[key].shipments.push(newShipping);
                                }
                            });
                            $modalInstance.close();
                        }
                    });
                };
                $scope.cancel = function () {
                    $modalInstance.dismiss('cancel');
                };
            })

        })
        .controller('editShippingGroupCtrl',function($scope,$filter, $modalInstance,shop,shipping_group_list,shipping_group_id){
            shop.loadShippingGroup(shipping_group_id).success(function(r){
                if(r.data.success) {
                    $scope.shipping_group = r.data.shipping_group;
                }
            })
            $scope.updateShippingGroup = function () {
                shop.updateShippingGroup($scope.shipping_group).success(function(r){
                    if(r.data.success) {
                        angular.forEach(shipping_group_list, function(value, key) {
                            if(value.id == $scope.shipping_group.id) {
                                shipping_group_list[key].name = $scope.shipping_group.name;
                            }
                        });
                        $modalInstance.close();
                    }
                });
            };
            $scope.cancel = function () {
                $modalInstance.dismiss('cancel');
            };
        })
        .controller('editShippingCtrl',function($scope,$filter, $modalInstance,shop,countries,shipping_group_list,shipping_id,$q){
            $scope.taxes = [];
            $scope.countries = [];
            $scope.shipping = {
                name: "",
                countries: new Array(),
                price: 0,
                start_day: 3,
                end_day: 10,
                shop_tax_id: ''
            };
            $q.all([
                shop.loadTaxes().success(function(r){
                    $scope.taxes = r.taxes;
                })
            ]).then(function() {
                shop.loadShipping(shipping_id).success(function(r){
                    if(r.data.success) {
                        $scope.shipping = r.data.shipping;
                        $scope.shipping.price = parseFloat($scope.shipping.price);
                        $scope.shipping.additional_price = parseFloat($scope.shipping.additional_price);
                    }
                })
                $scope.ok = function () {
                    shop.updateShipping($scope.shipping).success(function (r) {
                        if (r.data.success) {
                            var newShipping = r.data.shipping;
                            angular.forEach(shipping_group_list, function(value, key) {
                                angular.forEach(shipping_group_list[key].shipments, function(v, k) {
                                    if(shipping_group_list[key].shipments[k].id == shipping_id) {
                                        shipping_group_list[key].shipments[k] = newShipping;
                                    }
                                });
                            });
                            $modalInstance.close();
                        }
                    });
                };
                $scope.cancel = function () {
                    $modalInstance.dismiss('cancel');
                };
            });
        })
        .controller('paymentsCtrl',function($scope,payments,toaster,$activityIndicator,$rootScope,shop){
            $activityIndicator.startAnimating();
            $scope.payments = {
                bonifico: {
                    id:    null,
                    enabled: false,
                    value: false,
                    price: 0
                },
                paypal: {
                    id:    null,
                    enabled: false,
                    value: false,
                    price: 0
                },
                contrassegno: {
                    id:    null,
                    enabled: false,
                    price: 0,
                    value: false
                },
                money: {
                    id:    null,
                    enabled: false,
                    price: 0,
                    value: false
                }
            }
            payments.get().success(function (r) {
                angular.forEach(r.payments, function(value, key) {
                    $scope.payments[key] = r.payments[key];
                    $scope.payments[key].price = parseFloat($scope.payments[key].price);
                });
                $activityIndicator.stopAnimating();
            });
            $scope.save = function() {
                $activityIndicator.startAnimating();
                payments.savePayments($scope.payments).success(function (r) {
                    if(r.data.success) {
                        angular.forEach(r.payments, function(value, key) {
                            $scope.payments[key] = r.payments[key];
                            $scope.payments[key].price = parseFloat($scope.payments[key].price);
                        });
                        toaster.pop('success', "", r.data.message);
                        $activityIndicator.stopAnimating();
                    } else {
                        toaster.pop('error', "", r.data.message);
                        $activityIndicator.stopAnimating();
                    }
                    shop.checkStatus().success(function(r){
                        $rootScope.settingStatus = r.data;
                    });
                });
            }
        })
        .controller('addCollectionCtrl', function ($scope,toaster,collections,FileUploader,$filter,utility,$routeParams,$modal) {

                $scope.successSave          = false;
                $scope.errorSave            = false;
                $scope.onLoading            = false;

                $scope.progress             = 0;
                $scope.current_image        = '';
                $scope.count_image          = 0;

                $scope.success_images       = [];
                $scope.error_images         = [];

                $scope.collection = {
                    name                :   '',
                    uri                 :   '',
                    description         :   '',
                    meta_keywords       :   '',
                    meta_description    :   '',
                    order               :   null,
                    parent_id           :   ($routeParams.category_id) ? $routeParams.category_id : null
                }

                $scope.updateURL = function() {
                    $scope.collection.uri = utility.format_uri($scope.collection.title);
                }

                $scope.openSeo = function (size) {
                    $modal.open({
                        templateUrl: '/view/pages/seo.html',
                        controller: 'SeoModalCtrl',
                        size: size,
                        resolve: {
                            entity: function () {
                                return $scope.collection;
                            }
                        }
                    });
                };

                $scope.save = function() {
                    collections.add($scope.collection, $routeParams.language_id).success(function(r) {
                        if (r.data.success && r.data.category_id) {
                            if ($scope.uploader.queue.length) {
                                $scope.uploader.url = "/product_categories/loadImage/" + r.data.category_id;
                                $scope.uploader.uploadAll();
                            } else {
                                $scope.successSave = true;
                            }
                        } else {
                            $scope.errorSave = true;
                        }
                    });
                }

                $scope.uploader = new FileUploader({
                    url: "/product_categories/loadImage",
                    onAfterAddingFile:function(item){
                        var tmpItem = {};
                        for(var key in $scope.uploader.queue) {
                            tmpItem = $scope.uploader.queue[key]
                            if(item != tmpItem)
                                $scope.uploader.removeFromQueue(tmpItem);
                        }
                    },
                    onBeforeUploadItem: function(item){
                        item.url = $scope.uploader.url+".json";
                        $scope.onLoading = true;
                        $scope.current_image = item._file.name;
                    },
                    onCompleteAll: function() {
                        $scope.successSave  = true;
                        $scope.onLoading    = false;
                    },
                    onProgressAll: function(progress){
                        $scope.progress = progress;
                    },
                    onSuccessItem: function(item, response){
                        if(response.data.success) {
                            $scope.count_image++;
                            $scope.success_images.push(item._file.name);
                        }
                    },
                    onErrorItem: function(item, response, status, headers){
                        $scope.error_images.push(item._file.name);
                    }
                });
        })
        .controller('collectionCtrl',function($scope,$routeParams,$activityIndicator,collections,toaster,Languages,$modal){

            $activityIndicator.startAnimating();

            $scope.shop_languages       = [];
            $scope.selected_language    = '';
            $scope.data                 = {};
            $scope.numPerPageOpt        = [5, 10, 20];
            $scope.numPerPage           = $scope.numPerPageOpt[1];
            $scope.currentPage          = 1;
            $scope.currentPageStores    = [];
            $scope.collections          = [];
            $scope.totalCollections     = 0;
            $scope.row                  = 'created';
            $scope.orderType            = 'ASC';
            $scope.pageSelected         = [];
            $scope.obj                  = {};
            $scope.obj.selectAll        = false;
            $scope.obj.deselectAll      = true;
            $scope.parent_id            = ($routeParams.collection_id) ? $routeParams.collection_id : null;
            $scope.breadcrumb           = [];

            Languages.getShopLanguages().success(function(r) {
                $scope.shop_languages = r.languages;
                $scope.selected_language = r.default_language;
                $scope.select($scope.currentPage);
            });

            $scope.objectKeys = function(obj){
                return Object.keys(obj);
            }

            $scope.updateSelected = function() {
                var selected = false;
                angular.forEach($scope.collections, function(value, key) {
                    if($scope.collections[key].selected == true) {
                        selected = true;
                    }
                });
                if(!selected) {
                    $scope.obj.selectAll = false;
                    $scope.obj.deselectAll = true;
                }
            }

            $scope.toggleList = function(){
                if($scope.obj.deselectAll) {
                    $scope.obj.deselectAll  = false;
                    $scope.obj.selectAll    = true;
                    angular.forEach($scope.collections, function(value, key) {
                        $scope.collections[key].selected = true;
                    });
                } else {
                    $scope.obj.deselectAll  = true;
                    $scope.obj.selectAll    = false;
                    angular.forEach($scope.collections, function(value, key) {
                        $scope.collections[key].selected = false;
                    });
                }
            }

            $scope.checkSelected = function(){
                var keepGoing = false;
                angular.forEach($scope.collections, function(value, key) {
                    if($scope.collections[key].selected) {
                        keepGoing = true;
                    }
                });
                return keepGoing;
            }

            $scope.deleteAll = function(){
                var selected = [];
                angular.forEach($scope.collections, function(value, key) {
                    if(value.selected) {
                        selected.push(value.id);
                    }
                });
                collections.delete_all(selected).success(function(r){
                    if(r.data.success) {
                        toaster.pop('success', "", r.data.message);
                    } else {
                        toaster.pop('error', "", r.data.message);
                    }
                    $scope.select($scope.currentPage);
                });
            }

            $scope.selectLanguage = function(lang_id) {
                $scope.selected_language = lang_id;
                $scope.select($scope.currentPage);
            }

            $scope.select = function(page_act) {
                $activityIndicator.startAnimating();
                $scope.obj.deselectAll  = true;
                $scope.obj.selectAll    = false;
                collections.get_list($scope.numPerPage, page_act, $scope.row, $scope.orderType, $scope.selected_language,$scope.parent_id ).success(function(r){
                    $scope.collections          = r.categories;
                    $scope.totalPageCategories  = r.count;
                    $scope.breadcrumb           = r.breadcrumb;
                    $activityIndicator.stopAnimating();
                });
            };

            $scope.delete = function(id){
                collections.delete(id).success(function(r){
                    if(r.data.success) {
                        $scope.select($scope.currentPage);
                        toaster.pop('success', "", r.data.message);
                    } else {
                        toaster.pop('error', "", r.data.message);
                    }
                })
            }

            $scope.onFilterChange = function() {
                $scope.select(1);
                $scope.currentPage = 1;
                return $scope.row = '';
            };
            $scope.onNumPerPageChange = function() {
                $scope.select(1);
                return $scope.currentPage = 1;
            };
            $scope.onOrderChange = function() {
                $scope.select(1);
                return $scope.currentPage = 1;
            };
            $scope.order = function(rowName,orderType) {
                if ($scope.row === rowName && $scope.orderType === orderType) {
                    return;
                }
                $activityIndicator.startAnimating();
                $scope.row = rowName;
                $scope.orderType = orderType;
                return $scope.onOrderChange();
            };


        })
        .controller('editCollectionCtrl',function($scope,collections,$routeParams,$activityIndicator,toaster,FileUploader,$modal,utility){

            $scope.successSave          = false;
            $scope.errorSave            = false;
            $scope.onLoading            = false;

            $scope.progress             = 0;
            $scope.current_image        = '';
            $scope.count_image          = 0;

            $activityIndicator.startAnimating();
            $scope.collection = {};
            collections.load($routeParams.collection_id,$routeParams.language_id).success(function(r){
                $scope.collection   = r.content;
                $scope.image        = r.image;
                $activityIndicator.stopAnimating();
            });

            $scope.update = function() {
                collections.update($scope.collection).success(function (r) {
                    if(r.data.success) {
                        toaster.pop('success', "", r.data.message);
                    } else {
                        toaster.pop('error', "", r.data.message);
                    }
                })
            }

            $scope.openSeo = function (size) {
                $modal.open({
                    templateUrl: '/view/pages/seo.html',
                    controller: 'SeoModalCtrl',
                    size: size,
                    resolve: {
                        entity: function () {
                            return $scope.collection;
                        }
                    }
                });
            };

            $scope.updateURL = function() {
                $scope.collection.uri = utility.format_uri($scope.collection.title);
            }

            $scope.uploader = new FileUploader({
                url: "/product_categories/loadImage",
                onBeforeUploadItem: function(item){
                    item.url                = $scope.uploader.url;
                    $scope.current_image    = item._file.name;
                    $scope.onLoading        = true;
                },
                onAfterAddingAll : function()  {
                    $scope.progress     = 0;
                    $scope.uploader.url = "/product_categories/loadImage/" + $scope.collection.product_category_id + ".json";
                    $scope.uploader.uploadAll();
                },
                onProgressAll: function(progress){
                    $scope.progress = progress;
                },
                onSuccessItem: function(item, response){
                    if(response.data.success) {
                        $scope.image = {
                            url     : response.data.url,
                            name    : response.data.name
                        };
                    }
                    $scope.onLoading = false;
                },
                onErrorItem: function(){
                    $scope.onLoading = false;
                }
            });
        })
        .controller('modalCollectionCtrl',function($scope,$modalInstance,collections,collections_list){
            $scope.product_category = {
                id          : "",
                title       : "",
                uri         : "",
                parent_id   : null,
                ordern      : null
            }

            $scope.collections = collections_list;

            $scope.updateURL = function () {
                var name = $scope.product_category.title;
                if (name) {
                    $scope.product_category.uri = name.replace(/\W/g, '-');
                } else {
                    $scope.product_category.uri = '';
                }
            };

            $scope.save = function () {
                collections.add_modal($scope.product_category).success(function(r){
                    if(r.data.success) {
                        var return_data = {
                            collection_id   : r.data.collection_id,
                            tree            : r.data.tree
                        };
                        $modalInstance.close(return_data);
                    }
                });
            };
            $scope.cancel = function () {
                $modalInstance.dismiss('cancel');
            };
        })
        .controller('ThemeMainCtrl',function($scope,ThemeSite,$activityIndicator){
            $activityIndicator.startAnimating();
            $scope.active_theme = 1;
            ThemeSite.get_theme_list().success(function(r){
                $activityIndicator.stopAnimating();
                $scope.themes = r.themes;
                $scope.active_theme = r.active;
            });
        })
        .controller('ThemeDetailsCtrl',function($scope,ThemeSite,$activityIndicator,$routeParams,toaster){
            $activityIndicator.startAnimating();
            $scope.direction = 'left';
            $scope.currentIndex = 0;
            $scope.slides = [];
            $scope.theme_active = 1;
            $scope.setCurrentSlideIndex = function (index) {
                $scope.direction = (index > $scope.currentIndex) ? 'left' : 'right';
                $scope.currentIndex = index;
            };
            $scope.isCurrentSlideIndex = function (index) {
                return $scope.currentIndex === index;
            };
            $scope.prevSlide = function () {
                $scope.direction = 'left';
                $scope.currentIndex = ($scope.currentIndex < $scope.slides.length - 1) ? ++$scope.currentIndex : 0;
            };
            $scope.nextSlide = function () {
                $scope.direction = 'right';$scope.currentIndex = ($scope.currentIndex > 0) ? --$scope.currentIndex : $scope.slides.length - 1;
            };
            $scope.enableTheme = function(theme_id){
                ThemeSite.enable_theme(theme_id).success(function(r){
                    if(r.data.success) {
                        $scope.theme_active = theme_id;
                        toaster.pop('success', "", r.data.message);
                    } else {
                        toaster.pop('error', "", r.data.message);
                    }
                })
            }
            ThemeSite.load_theme($routeParams.theme_uri).success(function(r){
                $activityIndicator.stopAnimating();
                $scope.theme = r.data.theme;
                $scope.slides =  r.data.theme.screenshots;
                $scope.theme_active = r.data.theme_active;
            });
        })
        .controller('PagesCtrl', function($scope,$activityIndicator,page,toaster,Languages) {
            $activityIndicator.startAnimating();

            $scope.shop_languages       = [];
            $scope.selected_language    = '';
            $scope.data                 = {};
            $scope.numPerPageOpt        = [5, 10, 20];
            $scope.numPerPage           = $scope.numPerPageOpt[1];
            $scope.currentPage          = 1;
            $scope.currentPageStores    = [];
            $scope.pages                = [];
            $scope.totalPages           = 0;
            $scope.row                  = 'created';
            $scope.orderType            = 'ASC';
            $scope.pageSelected         = [];
            $scope.obj                  = {};
            $scope.obj.selectAll        = false;
            $scope.obj.deselectAll      = true;
            $scope.categories           = [];

            Languages.getShopLanguages().success(function(r) {
                $scope.shop_languages = r.languages;
                $scope.selected_language = r.default_language;
                $scope.select($scope.currentPage);
            });

            $scope.objectKeys = function(obj){
                return Object.keys(obj);
            }

            $scope.updateSelected = function() {
                var selected = false;
                angular.forEach($scope.pages, function(value, key) {
                    if($scope.pages[key].selected == true) {
                        selected = true;
                    }
                });
                if(!selected) {
                    $scope.obj.selectAll = false;
                    $scope.obj.deselectAll = true;
                }
            }

            $scope.toggleList = function(){
                if($scope.obj.deselectAll) {
                    $scope.obj.deselectAll  = false;
                    $scope.obj.selectAll    = true;
                    angular.forEach($scope.pages, function(value, key) {
                        $scope.pages[key].selected = true;
                    });
                } else {
                    $scope.obj.deselectAll  = true;
                    $scope.obj.selectAll    = false;
                    angular.forEach($scope.pages, function(value, key) {
                        $scope.pages[key].selected = false;
                    });
                }
            }

            $scope.checkSelected = function(){
                var keepGoing = false;
                angular.forEach($scope.pages, function(value, key) {
                    if($scope.pages[key].selected) {
                        keepGoing = true;
                    }
                });
                return keepGoing;
            }

            $scope.selectLanguage = function(lang_id) {
                $scope.selected_language = lang_id;
                $scope.select($scope.currentPage);
            }

            $scope.deletePage = function(idPage) {
                page.deletePage(idPage).success(function(r){
                    if(r.data.success) {
                        angular.forEach($scope.pages, function(value, key) {
                            if(value.page_id== idPage) {
                                $scope.pages.splice(key, 1);
                            }
                        });
                        toaster.pop('success', "", r.data.message);
                    } else {
                        toaster.pop('error', "", r.data.message);
                    }
                });
            }

            $scope.addPageToCategory = function(category_id){
                var selected = [];
                angular.forEach($scope.pages, function(value, key) {
                    if(value.selected) {
                        selected.push(value.page_id);
                    }
                });
                page.add_to_category(selected,category_id).success(function(r){
                    if(r.data.success) {
                        toaster.pop('success', "", r.data.message);
                    } else {
                        toaster.pop('error', "", r.data.message);
                    }
                    $scope.select($scope.currentPage);
                });
            }

            $scope.deleteAll = function(){
                var selected = [];
                angular.forEach($scope.pages, function(value, key) {
                    if(value.selected) {
                        selected.push(value.page_id);
                    }
                });
                page.delete_all(selected).success(function(r){
                    if(r.data.success) {
                        toaster.pop('success', "", r.data.message);
                    } else {
                        toaster.pop('error', "", r.data.message);
                    }
                    $scope.select($scope.currentPage);
                });
            }

            $scope.select = function(page_act) {
                $activityIndicator.startAnimating();
                $scope.obj.deselectAll  = true;
                $scope.obj.selectAll    = false;
                page.getList($scope.numPerPage, page_act, $scope.row, $scope.orderType, $scope.selected_language).success(function(r){
                    $scope.pages        = r.pages;
                    $scope.totalPages   = r.count;
                    $scope.categories   = r.categories;
                    $activityIndicator.stopAnimating();
                });
            };

            $scope.onFilterChange = function() {
                $scope.select(1);
                $scope.currentPage = 1;
                return $scope.row = '';
            };
            $scope.onNumPerPageChange = function() {
                $scope.select(1);
                return $scope.currentPage = 1;
            };
            $scope.onOrderChange = function() {
                $scope.select(1);
                return $scope.currentPage = 1;
            };
            $scope.order = function(rowName,orderType) {
                if ($scope.row === rowName && $scope.orderType === orderType) {
                    return;
                }
                $activityIndicator.startAnimating();
                $scope.row = rowName;
                $scope.orderType = orderType;
                return $scope.onOrderChange();
            };
        })
        .controller('SeoModalCtrl', function ($scope, $modalInstance,entity) {
            $scope.entity = entity;
            $scope.ok = function () {
                $modalInstance.close($scope.entity);
            };
            $scope.cancel = function () {
                $modalInstance.dismiss('cancel');
            };
        })
        .controller('PageAddCtrl', function($scope,$routeParams,$activityIndicator,$modal,$filter,FileUploader, utility, page, toaster, $location,page_categories,$timeout) {
            $activityIndicator.startAnimating();

            $scope.successSave          = false;
            $scope.errorSave            = false;
            $scope.onLoading            = false;

            $scope.progress             = 0;
            $scope.current_image        = '';
            $scope.count_image          = 0;

            $scope.success_images       = [];
            $scope.error_images         = [];

            $scope.category_page = {};
            $scope.page = {
                published: true
            };
            $scope.categories_page = [];

            page.loadPageData($routeParams.page_id,$routeParams.lang_id).success(function(r) {
                if(!r.data.success) {
                    $location.path('#error');
                }
                $scope.categories_page = r.data.categories;
                $activityIndicator.stopAnimating();
            });

            $scope.updateURLfromNamePage = function() {
                $scope.page.uri = utility.format_uri($scope.page.title);
            }

            $scope.openSeo = function (size) {
                $modal.open({
                    templateUrl: '/view/pages/seo.html',
                    controller: 'SeoModalCtrl',
                    size: size,
                    resolve: {
                        entity: function () {
                            return $scope.page;
                        }
                    }
                });
            };

            $scope.addPageCategoryModal = function() {
                var modal_add_category = $modal.open({
                    templateUrl: '/view/page_categories/add.html',
                    controller: 'PageCategoryAdd',
                    resolve: {
                        parent_id: function () {
                            return null;
                        },
                        language_id: function () {
                            return $routeParams.lang_id;
                        },
                        categories: function(){
                            return [];
                        }
                    }
                });
                modal_add_category.result.then(function (category_id) {
                    $activityIndicator.startAnimating();
                    page_categories.tree_list().success(function(r){
                        $scope.categories_page = r.categories;
                        $scope.page.category_page_id = category_id;
                        $activityIndicator.stopAnimating();
                    });
                });
            }

            $scope.savePage = function() {
                if($routeParams.page_id && $routeParams.page_id != undefined) {
                    $scope.page.id = $routeParams.page_id;
                }
                $scope.page.lang_id = $routeParams.lang_id;
                page.savePage($scope.page, $routeParams.lang_id).success(function(r) {
                    if (r.data.success && r.data.page_id) {
                        if ($scope.uploader.queue.length) {
                            $scope.uploader.url = "/pages/loadImage/" + r.data.page_id;
                            $scope.uploader.uploadAll();
                        } else {
                            $scope.successSave = true;
                        }
                    } else {
                        $scope.errorSave = true;
                    }
                });
            }

            $scope.uploader = new FileUploader({
                url: "/pages/loadImage",
                onBeforeUploadItem: function(item){
                    item.url = $scope.uploader.url+".json";
                    $scope.onLoading = true;
                    $scope.current_image = item._file.name;
                },
                onCompleteAll: function() {
                    $activityIndicator.stopAnimating();
                    $scope.successSave  = true;
                    $scope.onLoading    = false;
                },
                onProgressAll: function(progress){
                    $scope.progress = progress;
                },
                onSuccessItem: function(item, response){
                    if(response.data.success) {
                        $scope.count_image++;
                        $scope.success_images.push(item._file.name);
                    }
                },
                onErrorItem: function(item){
                    $scope.error_images.push(item._file.name);
                }
            });

            $scope.sortableOptions = {
                stop: function() {
                    for (var index in $scope.uploader.queue) {
                        $scope.uploader.queue[index].order = index;
                    }
                }
            };
        })
        .controller('PageEditCtrl', function($scope,$routeParams,$activityIndicator,$modal,$filter,FileUploader, utility, page, toaster, page_categories,$timeout) {
            $activityIndicator.startAnimating();

            $scope.successSave          = false;
            $scope.errorSave            = false;
            $scope.onLoading            = false;
            $scope.progress             = 0;
            $scope.current_image        = '';
            $scope.count_image          = 0;

            $scope.category_page        = {};
            $scope.page                 = {};
            $scope.categories_page      = [];
            $scope.images               = [];
            $scope.count_image          = 0;

            page.loadPage($routeParams.page_id,$routeParams.lang_id).success(function(r) {
                $scope.page             = r.page_content;
                $scope.categories_page  = r.categories;
                $scope.images           = r.images;
                $activityIndicator.stopAnimating();
            });

            $scope.uploader = new FileUploader({
                url: "/pages/loadImage",
                onBeforeUploadItem: function(item){
                    item.url = $scope.uploader.url;
                    $scope.current_image = item._file.name;
                    $scope.onLoading = true;
                },
                onAfterAddingAll : function(item)  {
                    $scope.progress = 0;
                    $scope.uploader.url = "/pages/loadImage/" + $routeParams.page_id + ".json";
                    $scope.uploader.uploadAll();
                },
                onProgressAll: function(progress){
                    $scope.progress = progress;
                },
                onSuccessItem: function(item, response){
                    if(response.data.success) {
                        var newImage = {
                            order       : null,
                            product_id  : null,
                            image       : response.data.url,
                            name        : response.data.name
                        }
                        $scope.images.push(newImage);
                        $scope.count_image++;
                    }
                    $scope.onLoading = false;
                },
                onErrorItem: function(){
                    $scope.onLoading = false;
                }
            });

            $scope.updateURLfromNamePage = function() {
                $scope.page.uri = utility.format_uri($scope.page.title);
            }

            $scope.openSeo = function(size) {
                $modal.open({
                    templateUrl: '/view/pages/seo.html',
                    controller: 'SeoModalCtrl',
                    resolve: {
                        entity: function () {
                            return $scope.page;
                        }
                    }
                });
            }
            $scope.addPageCategoryModal = function() {
                var modal_add_category = $modal.open({
                    templateUrl: '/view/page_categories/add.html',
                    controller: 'PageCategoryAdd',
                    resolve: {
                        parent_id: function () {
                            return null;
                        },
                        language_id: function () {
                            return $routeParams.lang_id;
                        },
                        categories: function(){
                            return [];
                        }
                    }
                });
                modal_add_category.result.then(function (category_id) {
                    $activityIndicator.startAnimating();
                    page_categories.tree_list().success(function(r){
                        $scope.categories_page = r.categories;
                        $scope.page.category_page_id = category_id;
                        $activityIndicator.stopAnimating();
                    });
                });
            }

            $scope.savePage = function () {
                $activityIndicator.startAnimating();
                page.update($scope.page,$scope.images).success(function (r) {
                    if (r.data.success) {
                        $scope.page.page_content_id = r.data.page_content_id;
                        toaster.pop('success', "", r.data.message);
                    } else {
                        toaster.pop('error', "", r.data.message);
                    }
                    $activityIndicator.stopAnimating();
                });
            };

            $scope.sortableOptions = {
                stop: function(e, ui) {
                    for (var index in $scope.images) {
                        $scope.images[index].order = index;
                    }
                }
            };

            $scope.deleteImage = function(image) {
                if(image) {
                    page.remove_image($routeParams.page_id, image).success(function (r) {
                        if(r.data.success) {
                            angular.forEach($scope.images, function(value, key) {
                                if(value.name == image) {
                                    $scope.images.splice(key, 1);
                                }
                            });
                        }
                    });
                }
            };
        })

        .controller('PageCategoriesCtrl', function($scope,$routeParams,$activityIndicator,page_categories,toaster,Languages,$modal) {

            $activityIndicator.startAnimating();

            $scope.shop_languages       = [];
            $scope.selected_language    = '';
            $scope.data                 = {};
            $scope.numPerPageOpt        = [5, 10, 20];
            $scope.numPerPage           = $scope.numPerPageOpt[1];
            $scope.currentPage          = 1;
            $scope.currentPageStores    = [];
            $scope.page_categories      = [];
            $scope.totalPageCategories  = 0;
            $scope.row                  = 'created';
            $scope.orderType            = 'ASC';
            $scope.pageSelected         = [];
            $scope.obj                  = {};
            $scope.obj.selectAll        = false;
            $scope.obj.deselectAll      = true;
            $scope.parent_id            = ($routeParams.category_page_id) ? $routeParams.category_page_id : null


            Languages.getShopLanguages().success(function(r) {
                $scope.shop_languages = r.languages;
                $scope.selected_language = r.default_language;
                $scope.select($scope.currentPage);
            });

            $scope.objectKeys = function(obj){
                return Object.keys(obj);
            }

            $scope.updateSelected = function() {
                var selected = false;
                angular.forEach($scope.page_categories, function(value, key) {
                    if($scope.page_categories[key].selected == true) {
                        selected = true;
                    }
                });
                if(!selected) {
                    $scope.obj.selectAll = false;
                    $scope.obj.deselectAll = true;
                }
            }

            $scope.toggleList = function(){
                if($scope.obj.deselectAll) {
                    $scope.obj.deselectAll  = false;
                    $scope.obj.selectAll    = true;
                    angular.forEach($scope.page_categories, function(value, key) {
                        $scope.page_categories[key].selected = true;
                    });
                } else {
                    $scope.obj.deselectAll  = true;
                    $scope.obj.selectAll    = false;
                    angular.forEach($scope.page_categories, function(value, key) {
                        $scope.page_categories[key].selected = false;
                    });
                }
            }

            $scope.checkSelected = function(){
                var keepGoing = false;
                angular.forEach($scope.page_categories, function(value, key) {
                    if($scope.page_categories[key].selected) {
                        keepGoing = true;
                    }
                });
                return keepGoing;
            }

            $scope.deleteAll = function(){
                var selected = [];
                angular.forEach($scope.page_categories, function(value, key) {
                    if(value.selected) {
                        selected.push(value.category_id);
                    }
                });
                page_categories.delete_all(selected).success(function(r){
                    if(r.data.success) {
                        toaster.pop('success', "", r.data.message);
                    } else {
                        toaster.pop('error', "", r.data.message);
                    }
                    $scope.select($scope.currentPage);
                });
            }

            $scope.selectLanguage = function(lang_id) {
                $scope.selected_language = lang_id;
                $scope.select($scope.currentPage);
            }

            $scope.select = function(page_act) {
                $activityIndicator.startAnimating();
                $scope.obj.deselectAll  = true;
                $scope.obj.selectAll    = false;
                page_categories.getList($scope.numPerPage, page_act, $scope.row, $scope.orderType, $scope.selected_language,$scope.parent_id ).success(function(r){
                    $scope.page_categories = r.categories;
                    $scope.totalPageCategories = r.count;
                    $activityIndicator.stopAnimating();
                });
            };

            $scope.addCategory = function() {
                var modal_add = $modal.open({
                    templateUrl: '/view/page_categories/add.html',
                    controller: 'PageCategoryAdd',
                    resolve: {
                        parent_id: function () {
                            return $scope.parent_id;
                        },
                        language_id: function () {
                            return $scope.selected_language;
                        },
                        categories: function(){
                            return [];
                        }
                    }
                });
                modal_add.result.then(function (category_id) {
                    $scope.select($scope.currentPage);
                });
            }

            $scope.editCategory = function(language_id,category_page_id) {
                var modal_edit = $modal.open({
                    templateUrl: '/view/page_categories/edit.html',
                    controller: 'PageCategoryEdit',
                    resolve: {
                        category_page_id: function () {
                            return category_page_id;
                        },
                        language_id: function () {
                            return language_id;
                        }
                    }
                });
                modal_edit.result.then(function () {
                    $scope.select($scope.currentPage);
                });
            }
            $scope.delete = function(id){
                page_categories.delete(id).success(function(r){
                    if(r.data.success) {
                        $scope.select($scope.currentPage);
                        toaster.pop('success', "", r.data.message);
                    } else {
                        toaster.pop('error', "", r.data.message);
                    }
                })
            }

            $scope.onFilterChange = function() {
                $scope.select(1);
                $scope.currentPage = 1;
                return $scope.row = '';
            };
            $scope.onNumPerPageChange = function() {
                $scope.select(1);
                return $scope.currentPage = 1;
            };
            $scope.onOrderChange = function() {
                $scope.select(1);
                return $scope.currentPage = 1;
            };
            $scope.order = function(rowName,orderType) {
                if ($scope.row === rowName && $scope.orderType === orderType) {
                    return;
                }
                $activityIndicator.startAnimating();
                $scope.row = rowName;
                $scope.orderType = orderType;
                return $scope.onOrderChange();
            };
        })

        .controller('PageCategoryAdd', function ($scope, $modalInstance, utility , toaster, parent_id,language_id,page_categories,categories) {
            $scope.category_page = {
                title       : '',
                parent_id   : parent_id,
                uri         : ''
            };
            $scope.categories = categories;
            $scope.updateURLfromNameCatPag = function () {
                $scope.category_page.uri = utility.format_uri($scope.category_page.title);
            }
            $scope.save = function () {
                page_categories.add($scope.category_page, language_id).success(function(r) {
                    if(r.data.success) {
                        $modalInstance.close(r.data.category_id);
                        toaster.pop('success', "", r.data.message);
                    } else {
                        toaster.pop('error', "", r.data.message);
                    }
                });
            };
            $scope.cancel = function () {
                $modalInstance.dismiss('cancel');
            };
        })
        .controller('PageCategoryEdit', function ($scope, $modalInstance, utility , toaster, category_page_id,language_id,page_categories) {
            $scope.category_page = {};
            page_categories.load(category_page_id,language_id).success(function(r){
                $scope.category_page = r.content;
            });
            $scope.updateURLfromNameCatPag = function () {
                $scope.category_page.uri = utility.format_uri($scope.category_page.title);
            }
            $scope.save = function () {
                page_categories.update($scope.category_page, language_id).success(function(r) {
                    if(r.data.success) {
                        $modalInstance.close();
                        toaster.pop('success', "", r.data.message);
                    } else {
                        toaster.pop('error', "", r.data.message);
                    }
                });
            };
            $scope.cancel = function () {
                $modalInstance.dismiss('cancel');
            };
        })

        .controller('LanguagesCtrl', function ($scope,Languages,$modal,$activityIndicator,toaster) {
            $scope.languages = [];
            $scope.default_language_id = null;
            $activityIndicator.startAnimating();
            Languages.list().success(function(r){
                $scope.languages = r.data.languages;
                $scope.default_language_id = r.data.default_language_id;
                $activityIndicator.stopAnimating();
            });
            $scope.addLanguage = function (shipping_group_id) {
                $modal.open({
                    templateUrl: '/view/languages/add.html',
                    controller: 'addLanguage',
                    resolve: {
                        shop_language: function () {
                            return $scope.languages;
                        }
                    }
                });
            };
            $activityIndicator.startAnimating();
            $scope.deleteLanguage = function(language_id){
                Languages.delete(language_id).success(function(r){
                    if(r.data.success) {
                        angular.forEach($scope.languages, function(value, key) {
                            if(value.id == language_id) {
                                $scope.languages.splice(key, 1);
                            }
                        });
                        toaster.pop('success', "", r.data.message);
                    } else {
                        toaster.pop('error', "", r.data.message);
                    }
                    $activityIndicator.stopAnimating();
                })
            }
        })
        .controller('addLanguage',function($scope,Languages,$modalInstance,shop_language){
            $scope.cancel = function () {
                $modalInstance.dismiss('cancel');
            };
            $scope.languages    = [];
            $scope.language     = {
                language_id : null
            };
            Languages.available_languages().success(function(r){
                $scope.languages = r.languages;
            })
            $scope.saveLanguage = function(){
                Languages.add($scope.language.language_id).success(function(r){
                    if(r.data.success) {
                        shop_language.push(r.data.language)
                        $modalInstance.dismiss('cancel');
                    }
                })
            }
        })
}).call(this);