(function() {
    'use strict';
    angular.module('shop', ['ngRoute', 'ngAnimate', 'ui.bootstrap','plugins','shared.directives','shared.services','shop.controllers','shop.services','ngTagsInput','angularFileUpload','ngActivityIndicator','ui.sortable','textAngular','top_bar.services','top_bar.ctrls','toaster','ngMap','ngAutocomplete','ngSanitize', 'ui.select','oi.multiselect'])
        .config([
            '$routeProvider', function($routeProvider) {
                for (var template in pluginRoute.views) {
                    $routeProvider.when(
                        pluginRoute.views[template].url, {
                            templateUrl: pluginRoute.views[template].template
                        }
                    );
                }
                $routeProvider.when('/', {
                    redirectTo: '/dashboard'
                }).when('/dashboard', {
                    templateUrl: '/view/shops/dashboard.html'
                }).when('/products', {
                    templateUrl: '/view/products.html'
                }).when('/products/add/:language_id', {
                    templateUrl: '/view/products/add.html'
                }).when('/products/edit/:language_id/:product_id', {
                    templateUrl: '/view/products/edit.html'
                }).when('/app-store', {
                    templateUrl: '/view/appStore/'
                }).when('/app-store/apps/:category', {
                    templateUrl: '/view/appStore/apps.html'
                }).when('/app-store/:app_url', {
                    templateUrl: '/view/appStore/app/',
                    controller:'appCtrl'
                }).when('/help', {
                    templateUrl: '/view/help/list.html'
                }).when('/settings/shop', {
                    templateUrl: '/view/shops/settings.html'
                }).when('/settings/information', {
                    templateUrl: '/view/shops/tax_information.html'
                }).when('/settings/tax', {
                    templateUrl: '/view/taxes/list.html'
                }).when('/settings/shipping', {
                    templateUrl: '/view/shipments/list.html'
                }).when('/settings/payments', {
                    templateUrl: '/view/payments/list.html'
                }).when('/orders', {
                    templateUrl: '/view/orders/list.html'
                }).when('/orders/:order_id', {
                    templateUrl: '/view/orders/order.html'
                }).when('/orders/request/:order_id', {
                    templateUrl: '/view/orders/request.html'
                }).when('/shop_notifies', {
                    templateUrl: '/view/notifies/shop_notifies.html'
                }).when('/collections', {
                    templateUrl: '/view/product_categories.html'
                }).when('/collections/:collection_id',{
                    templateUrl: '/view/product_categories.html'
                }).when('/collections/add/:language_id', {
                    templateUrl: '/view/product_categories/add.html'
                }).when('/collections/add/:language_id/:category_id', {
                    templateUrl: '/view/product_categories/add.html'
                }).when('/collections/edit/:language_id/:collection_id', {
                    templateUrl: '/view/product_categories/edit.html'
                    /*}).when('/themes', {
                     templateUrl: '/view/themes/list.html'
                     }).when('/theme/:theme_uri', {
                     templateUrl: '/view/themes/theme.html'
                     })*/
                }).when('/pages/', {
                    templateUrl: '/view/pages.html'
                })
                .when('/pages/add/:lang_id', {
                    templateUrl: '/view/pages/add.html'
                })
                .when('/pages/edit/:lang_id/:page_id', {
                    templateUrl: '/view/pages/edit.html'
                })
                .when('/categories', {
                    templateUrl: '/view/page_categories.html'
                })
                .when('/languages/', {
                    templateUrl: '/view/languages.html'
                })
                .otherwise({
                    redirectTo: "/404"
                });
                return $routeProvider;
            }
        ])
        .config(['$httpProvider', function($httpProvider) {
            $httpProvider.interceptors.push('errorHttpInterceptor');
        }]);
}).call(this);