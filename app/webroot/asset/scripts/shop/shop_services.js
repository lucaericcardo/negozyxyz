(function() {
    'use strict';
    angular.module('shop.services', [])
    .factory("countries", function ($http) {
        return {
            get: function () {
                return $http.get('/countries.json');
            }
        }
    }).factory("currencies", function ($http) {
        return {
            get: function () {
                return $http.get('/currencies.json');
            }
        }
    }).factory('dashboardTabs',function($http) {
        // Deprecata
        return {
            get: function () {
                return $http.get('/user_profile/getDashboardTabs.json')
            }
        }
    }).factory('tabTemplate',function($http){
        // Deprecata
        return {
            get: function(template){
                return $http.get('/admin_asset/views/tabs/'+template+'.html');
            }
        }
    }).factory('dashboardData', function () {
        // Deprecata
        return {
            getTotal: function () {
                return 0;
            },
            getSell: function (tasks) {
                return 0;
            },
            getVisitor: function () {
                return 0;
            }
        };
    })
        .factory('product',function($http) {
            return {
                save: function (product) {
                    return $http.post('/products/add.json', {
                        product: product
                    });
                },
                update: function (product) {
                    return $http.post('/products/update.json', {
                        product: product
                    });
                },
                delete: function (product_id) {
                    return $http.post('/products/delete.json', {product_id: product_id});
                },
                clone: function (product_id) {
                    return $http.post('/products/clone_product.json', {product_id: product_id});
                },
                deleteAll: function (products) {
                    return $http.post('/products/deleteAll.json', {products: products});
                },
                get_list: function (n, page, row, orderType,language_id) {
                    return $http.get('/products/get_list/' + n + '/' + page + '/' + row + '/' + orderType + '/' + language_id + '.json');
                },
                load_product: function (id,language_id) {
                    return $http.get('/products/loadProduct/' + id+ '/'+ language_id +'.json');
                },
                remove_image: function (product_id, image) {
                    return $http.get('/products/removeImage/' + product_id + '/' + image + '.json');
                },
                load_data: function () {
                    return $http.get('/products/loadProductData.json');
                },
                addToCollection: function(products,product_category_id) {
                    return $http.post('/products/addToCollection.json',{products:products,product_category_id:product_category_id});
                },
                addToShippingGroup: function(products,shipping_group_id){
                    return $http.post('/products/addToShippingGroup.json',{products:products,shipping_group_id:shipping_group_id});
                }

            }
    })
        .factory('page',function($http) {
            return {
                getList: function (n, page, row, orderType, language_id) {
                    return $http.get('/pages/get_list/' + n + '/' + page + '/' + row + '/' + orderType + '/' + language_id +'.json');
                },
                deletePage: function (idPage) {
                    return $http.post('/pages/deletePage.json',{idPage:idPage});
                },
                loadPageData: function(page_id,language_id){
                    return $http.post('/pages/loadPageData.json', {page_id:page_id,language_id:language_id});
                },
                savePage: function(page) {
                    return $http.post('/pages/add.json', {page:page});
                },
                update: function(page,images) {
                    return $http.post('/pages/update.json', {page:page,images:images});
                },
                loadPage: function(page_id,language_id) {
                    return $http.post('/pages/loadPage.json', {page_id:page_id,language_id:language_id});
                },
                remove_image: function (page_id, image) {
                    return $http.get('/pages/removeImage/' + page_id + '/' + image + '.json');
                },
                add_to_category: function(pages,category_id){
                    return $http.post('/pages/add_to_category.json', {pages:pages,category_id:category_id});
                },
                delete_all: function(pages){
                    return $http.post('/pages/delete_all.json', {pages:pages});
                }
            }
        })
        .factory('page_categories',function($http) {
            return {
                getList: function (n, page, row, orderType,language_id,parent_id) {
                    if(parent_id) {
                        return $http.get('/page_categories/get_list/' + n + '/' + page + '/' + row + '/' + orderType + '/' + language_id +'/'+parent_id +'.json');
                    } else {
                        return $http.get('/page_categories/get_list/' + n + '/' + page + '/' + row + '/' + orderType + '/' + language_id +'.json');
                    }
                },
                add: function(category_page, language_id) {
                    return $http.post('/page_categories/add.json',{category_page:category_page, language_id:language_id});
                },
                update: function(category_page, language_id) {
                    return $http.post('/page_categories/update.json',{category_page:category_page, language_id:language_id});
                },
                delete: function(id) {
                    return $http.post('/page_categories/delete.json',{id:id});
                },
                load: function(category_page_id, language_id) {
                    return $http.post('/page_categories/load.json',{category_page_id:category_page_id, language_id:language_id});
                },
                tree_list: function(){
                    return $http.get('/page_categories/tree_list.json');
                },
                delete_all: function(category_page){
                    return $http.post('/page_categories/delete_all.json', {category_page:category_page});
                }
            }
        })
        .factory('orders',function($http){
        return {
            getList: function (n, page, row, orderType) {
                return $http.get('/orders/get_list/' + n + '/' + page + '/' + row + '/' + orderType + '.json');
            },
            load_order: function(order_id){
                return $http.post('/orders/loadOrder.json',{order_id:order_id});
            },
            changeStatus: function(order_id,order_status){
                return $http.post('/orders/changeStatus.json',{order_id:order_id,order_status:order_status});
            },
            changeAilability: function(order_id,available){
                return $http.post('/orders/changeAilability.json',{order_id:order_id,available:available});
            }
        }
    }).factory('shop',function($http) {
        return {
            get: function () {
                return $http.get('/shops.json');
            },
            dashboardData: function() {
                return $http.get('/shops/dashboardData.json');
            },
            loadSettings: function() {
                return $http.get('/shops/loadSettings.json');
            },
            loadTaxInformation: function(){
                return $http.get('/shops/loadTaxInformation.json');
            },
            updateShop: function(shop){
                return $http.post('/shops/updateShop.json',{shop:shop});
            },
            updateTaxInformation: function(shop){
                return $http.post('/shops/updateTaxInformation.json',{shop:shop});
            },
            addTax: function(tax) {
                return $http.post('/taxes/add.json',{tax:tax});
            },
            loadTaxes: function(){
                return $http.post('/taxes.json');
            },
            loadTax: function(tax_id){
                return $http.post('/taxes/loadTax.json',{tax_id:tax_id});
            },
            updateTax : function(tax){
                return $http.post('/taxes/update.json',{tax:tax});
            },
            deleteTax: function(tax_id) {
                return $http.post('/taxes/delete.json',{tax_id:tax_id});
            },
            loadShippingGroup : function(shipping_group_id){
                return $http.post('/shipping_groups/loadShippingGroup.json',{shipping_group_id:shipping_group_id});
            },
            loadShippingGroups : function(){
                return $http.get('/shipping_groups.json');
            },
            addShippingGroup : function(shipping_group){
                return $http.post('/shipping_groups/add.json',{shipping_group:shipping_group});
            },
            updateShippingGroup : function(shipping_group){
                return $http.post('/shipping_groups/update.json',{shipping_group:shipping_group});
            },
            deleteShippingGroup: function(shipping_group_id) {
                return $http.post('/shipping_groups/delete.json',{shipping_group_id:shipping_group_id});
            },
            loadShipments: function() {
                return $http.post('/shipments.json');
            },
            loadShipping : function(shipping_id){
                return $http.post('/shipments/loadShipping.json',{shipping_id:shipping_id});
            },
            updateShipping : function(shipping){
                return $http.post('/shipments/update.json',{shipping:shipping});
            },
            addShipping : function(shipping){
                return $http.post('/shipments/add.json',{shipping:shipping});
            },
            deleteShipping: function(shipping_id) {
                return $http.post('/shipments/delete.json',{shipping_id:shipping_id});
            },
            loadShippingConfig: function(shipping_group_id) {
                return $http.post('/shipping_groups/loadShippingConfig.json',{shipping_group_id:shipping_group_id});
            },
            removeImage: function(image) {
                return $http.get('/shops/removeImage/'+image+'.json');
            },
            checkStatus: function(){
                return $http.get('/shops/checkShopStatus.json');
            },
            checkShop: function(){
                return $http.get('/shops/checkShop.json');
            },
            getShopData: function(){
                return $http.get('/shops/getShopData.json');
            },
            publicShop: function(){
                return $http.get('/shops/publicShop.json');
            }
        }
    }).factory('tags',function($http){
        return {
            list: function(){
                return $http.get('/tags.json');
            },
            find: function(query) {
                return $http.get('/tags/search/'+query+'.json');
            }
        }
    }).factory('payments',function($http){
            return {
                get: function(){
                    return $http.get('/payments.json');
                },
                savePayments: function(payments) {
                    return $http.post('/payments/save.json',{payments:payments});
                }
            }
    }).factory('categories',function($http){
        return {

            list: function(){
                return $http.get('/categories.json');
            },
            validateMacrocategory: function(name){
                return $http.post('/categories/validateMacrocategory.json', {name:name});
            },
            validateCategory: function(name){
                return $http.post('/categories/validateCategory.json', {name:name});
            },
            validateSubcategory: function(name){
                return $http.post('/categories/validateSubcategory.json', {name:name});
            }
        }
    })
    .factory("users",function($http){
        return {
            getUser: function (user) {
                return $http.get('/users/getUser/' + user + '.json');
            },
            loadUser: function(){
                return $http.get('/users/loadUser.json');
            },
            updateAccount: function(user) {
                return $http.post('/users/updateAccount.json',{user:user});
            }
        }
    })
    .factory("collections",function($http){
        return {
            get_list: function(n, page, row, orderType,language_id,parent_id){
                if(parent_id) {
                    return $http.get('/product_categories/get_list/' + n + '/' + page + '/' + row + '/' + orderType + '/' + language_id + '/' + parent_id + '.json');
                } else {
                    return $http.get('/product_categories/get_list/' + n + '/' + page + '/' + row + '/' + orderType + '/' + language_id + '/.json');
                }
            },
            add: function(product_category,language_id){
                return $http.post('/product_categories/add.json',{product_category:product_category,language_id:language_id});
            },
            add_modal: function(product_category){
                return $http.post('/product_categories/add_modal.json',{product_category:product_category});
            },
            update: function(product_category){
                return $http.post('/product_categories/update.json',{product_category:product_category});
            },
            delete: function(product_category_id){
                return $http.post('/product_categories/delete.json',{product_category_id:product_category_id});
            },
            delete_all: function(product_categories){
                return $http.post('/product_categories/delete_all.json',{product_categories:product_categories});
            },
            load: function(product_category_id,language_id){
                return $http.post('/product_categories/load.json',{product_category_id:product_category_id,language_id:language_id});
            }
        }
    })
    .factory("app_store",function($http){
        return {
            getApplicationsGroupByCategories: function () {
                return $http.get('/appStore/getApplicationsGroupByCategories.json');
            },
            getPageData: function (request_uri) {
                return $http.post('/appStore/getPageData.json', {request_uri : request_uri});
            },
            installApp: function (request_uri, app_id) {
                return $http.post('/appStore/installApp.json', {request_uri : request_uri, app_id : app_id});
            },
            removeApp: function (request_uri, app_id) {
                return $http.post('/appStore/removeApp.json', {request_uri : request_uri, app_id : app_id});
            },
            getToken: function(){
                return $http.post('/appStore/getTokenPay.json');
            },
            createPayment: function(nonce){
                return $http.post('/appStore/createPayment.json',{nonce:nonce});
            }
        }
    })
    .factory('errorHttpInterceptor', ['$q', function ($q) {
        return {
            response: function(response){
                if (response.status === 401) {
                    //console.log("Response 401");
                    document.location = rejection.headers('X-extra-redirect');
                }
                return response || $q.when(response);
            },
            responseError: function(rejection) {
                if (rejection.status === 401) {
                    //console.log("Response Error 401",rejection);
                    document.location = rejection.headers('X-extra-redirect');
                }
                return $q.reject(rejection);
            }
        };
    }])
        .factory('ThemeSite',function($http){
            return {
                get_theme_list: function(){
                    return $http.get('/themes/get_list.json');
                },
                load_theme: function(theme_uri){
                    return $http.post('/themes/load_theme.json',{theme_uri:theme_uri});
                },
                enable_theme: function(theme_id){
                    return $http.post('/themes/enable_theme.json',{theme_id:theme_id});
                }
            }
        })
        .factory('Languages',function($http){
            return {
                list: function(){
                    return $http.get('/languages/list_languages.json');
                },
                available_languages: function(){
                    return $http.get('/languages/available_languages.json');
                },
                add: function(language_id){
                    return $http.post('/languages/add.json',{language_id:language_id});
                },
                delete: function(language_id){
                    return $http.post('/languages/delete.json',{language_id:language_id});
                },
                getShopLanguages: function(){
                    return $http.get('/languages/getShopLanguage.json');
                }
            }
        })
}).call(this);
