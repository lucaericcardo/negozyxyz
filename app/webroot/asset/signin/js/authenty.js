(function($) {
  
	// custom checkbox skin plugin
	$('input').iCheck({
    checkboxClass: 'icheckbox_minimal-orange',
    radioClass: 'iradio_minimal-orange',
    increaseArea: '20%' // optional
  });
	
	// sign in form 1
	$('#signIn_1').click(function (e) {  
	   
			var username = $.trim($('#LoginEmail').val());
	    	var password = $.trim($('#LoginPassword').val());

	    if ( username === '' || password === '' || (!IsEmail(username)) ) {
        $('#form_1 .fa-sign-in').removeClass('success').addClass('fail');
				$('#form_1').addClass('fail');
	    } else {
	   		$('#form_1 .fa-sign-in').removeClass('fail').addClass('success');
				$('#form_1').removeClass('fail')//.removeClass('animated');
				//return false;
	    }
	});
	
	// sign in form 2
	$('#signIn_2').click(function (e) {  
	   
			var nome = $.trim($('#SingupFirstName').val());
	    	var cognome= $.trim($('#SingupLastName').val());
			var mail = $.trim($('#SingupEmail').val());
			var pas = $.trim($('#SingupPassword').val());
            var terms = $('#terCon').is(':checked');

        // decommentare la seguente riga per la convalida dei termini e condizioni
	    if ( nome === '' || cognome === '' || mail === '' || pas === '' || (!IsEmail(mail)) || terms === false) {
        //if ( nome === '' || cognome === '' || mail === '' || pas === '' || (!IsEmail(mail))) {
        $('#form_2 .fa-user').removeClass('success').addClass('fail');
				$('#form_2').addClass('fail');
	    } else {
	   		$('#form_2 .fa-user').removeClass('fail').addClass('success');
				$('#form_2').removeClass('fail')//.removeClass('animated');
				//return false;
	    }
	});
	
	function IsEmail(username) {
	  var regex = /^([a-zA-Z0-9_.+-])+\@(([a-zA-Z0-9-])+\.)+([a-zA-Z0-9]{2,4})+$/;
	  return regex.test(username);
	}
	
	function IsEmail(mail) {
	  var regex = /^([a-zA-Z0-9_.+-])+\@(([a-zA-Z0-9-])+\.)+([a-zA-Z0-9]{2,4})+$/;
	  return regex.test(mail);
	}
	

})(jQuery);
