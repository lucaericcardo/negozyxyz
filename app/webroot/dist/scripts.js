(function() {
    'use strict';
    angular.module('account', ['ngRoute', 'ngAnimate', 'ui.bootstrap','account.ctrls','account.services','shared.directives','shared.services','ngActivityIndicator','angularFileUpload','top_bar.services','top_bar.ctrls','toaster']).config([
        '$routeProvider', function($routeProvider) {
             return $routeProvider.when('/', {
             redirectTo: '/'
             }).when('/', {
                templateUrl: '/view/account/account.html'
             }).when('/orders', {
                 templateUrl: '/view/account/orders.html'
             }).when('/order/:order_uid', {
                 templateUrl: '/view/account/order_details.html'
             }).when('/requests', {
                 templateUrl: '/view/account/my_requests.html'
             })
        }
    ]);
}).call(this);

(function () {
    'use strict';
    angular.module('account.ctrls', [])
        .controller('AccountCtrl', function ($scope,users,FileUploader,toaster) {
            $scope.user = {
                first_name: '',
                last_name: '',
                new_password: '',
                confirm_password: '',
                image: 'http://cdn.negozy.com/negozy/Img/g1.jpg'
            };
            users.loadUser().success(function (r)
            {
                $scope.user.first_name = r.user.first_name;
                $scope.user.last_name = r.user.last_name;
                if(r.user.image) {
                    $scope.user.image = r.user.image;
                }

            });
            $scope.saveAccount = function(){
                users.updateAccount($scope.user).success(function(r){
                    if(r.data.success) {
                        toaster.pop('success', "", r.data.message);
                    } else {
                        toaster.pop('error', "", r.data.message);
                    }
                });
            }
            $scope.uploaderProfile = new FileUploader({
                url: "/users/loadProfile.json",
                onBeforeUploadItem: function(item){
                    item.url = $scope.uploaderProfile.url;
                    $scope.loadingImage = true;
                },
                onProgressAll: function(progress){
                    $scope.progress = progress;
                },
                onAfterAddingAll : function()  {
                    $scope.uploaderProfile.uploadAll();
                },
                onSuccessItem: function(item, response){
                    if(response.data.success) {
                        $scope.user.image = response.data.url;
                    }
                    $scope.loadingImage = false;
                },
                onErrorItem: function(item, response, status, headers){
                    $scope.loadingImage = false;
                }
            });
        })
        .controller('MyRequestsCtrl',function($scope,$rootScope,$activityIndicator,my_requests,toaster,cart, notifies){
            $activityIndicator.startAnimating();
            $rootScope.open_requests = [];
            $rootScope.closed_requests = [];

            my_requests.get().success(function(r) {
                if(r.data.success) {
                    angular.forEach(r.data.requests, function(value, key) {
                        if(value.status == 'open') {
                            $rootScope.open_requests.push(r.data.requests[key]);
                        } else if(value.status == 'closed') {
                            $rootScope.closed_requests.push(r.data.requests[key]);
                        }
                    });
                }
                $activityIndicator.stopAnimating();
            });

            $scope.buyNow = function(product_id, negoziatyon_id) {
                notifies.buyNow(product_id, negoziatyon_id).success(function(r){
                    if(r.data.success && r.data.token) {
                        document.location = '/checkout/setOrder/'+r.data.token;
                    }
                });
            }

            /*
            $scope.buyNow = function(product_id, negoziatyon_id) {
                notifies.buyNow(product_id, negoziatyon_id).success(function(r){
                    if(r.data.success && r.data.token) {
                        document.location = '/checkout/setOrder/'+r.data.token;
                    }
                });
            }

            $scope.available = function(notify, notify_id, negoziatyon_id, state) {
                notifies.available(notify_id, negoziatyon_id, state).success(function(r){
                    //location.reload();
                    notify.response = true;
                    toaster.pop('info', "", "Risposta inviata");
                    if(state == false) {
                        angular.forEach($scope.open_notifies, function(value, key) {
                            if(value.id == notify_id) {
                                $scope.open_notifies.splice(key, 1);
                                $scope.closed_notifies.push(notify);
                            }
                        });
                    }
                });
            }
            */
        })
        .controller('OrdersCtrl',function($scope, $filter,orders,$activityIndicator){
            var init;
            $scope.numPerPageOpt        = [5, 10, 20];
            $scope.data                 = {};
            $scope.numPerPage           = $scope.numPerPageOpt[1];
            $scope.currentPage          = 1;
            $scope.currentPageStores    = [];
            $scope.orders             = [];
            $scope.totalOrders       = 0;
            $scope.row                  = 'created';
            $scope.orderType            = 'DESC';
            $scope.select = function(page) {
                $activityIndicator.startAnimating();
                orders.getList($scope.numPerPage,page,$scope.row,$scope.orderType).success(function(r){
                    $scope.orders = r.orders;
                    $scope.totalOrders = r.count;
                    $activityIndicator.stopAnimating();
                });
            };
            $scope.onFilterChange = function() {
                $scope.select(1);
                $scope.currentPage = 1;
                return $scope.row = '';
            };
            $scope.onNumPerPageChange = function() {
                $scope.select(1);
                return $scope.currentPage = 1;
            };
            $scope.onOrderChange = function() {
                $scope.select(1);
                return $scope.currentPage = 1;
            };
            $scope.order = function(rowName,orderType) {
                if ($scope.row === rowName && $scope.orderType === orderType) {
                    return;
                }
                $activityIndicator.startAnimating();
                $scope.row = rowName;
                $scope.orderType = orderType;
                return $scope.onOrderChange();
            };
            init = function() {
                return $scope.select($scope.currentPage);
            };
            return init();
        })

        .controller('OrderCtrl',function($scope,$routeParams,orders,$activityIndicator,toaster){
            $activityIndicator.startAnimating();
            $scope.order = {};
            $scope.order_status = [];
            orders.load_order($routeParams.order_uid).success(function(r){
                $scope.order = r.order;
                $activityIndicator.stopAnimating();

            });
            $scope.changeStatus = function(){
                orders.changeStatus($routeParams.order_id,$scope.order.order.status).success(function(r){
                    if(r.data.success) {
                        toaster.pop('success', "", r.data.message);
                    } else {
                        toaster.pop('error', "", r.data.message);
                    }
                });
            }
        })

}).call(this);
(function() {
    'use strict';
    angular.module('account.services', [])
        .factory("users",function($http){
            return {
                loadUser: function(){
                    return $http.get('/account/loadUser.json');
                },
                updateAccount: function(user) {
                    return $http.post('/account/updateAccount.json',{user:user});
                }
            }
        })
        .factory("orders",function($http){
            return {
                getList: function (n, page, row, orderType) {
                    return $http.get('/account/get_list/' + n + '/' + page + '/' + row + '/' + orderType + '.json');
                },
                load_order: function(order_id){
                    return $http.post('/account/loadOrder.json',{order_id:order_id});
                }
            }
        })
}).call(this);

(function() {
    'use strict';
    angular.module('admin', ['ngRoute', 'ngAnimate', 'ui.bootstrap','admin.ctrls','admin.services','shared.services','shared.directives','ngActivityIndicator','top_bar.services','top_bar.ctrls','toaster']).config([
        '$routeProvider', function($routeProvider) {
            return $routeProvider.when('/', {
                redirectTo: '/dashboard'
            }).when('/dashboard', {
                templateUrl: '/view/administrator/dashboard.html'
            }).when('/users', {
                templateUrl: '/view/administrator/users_list.html'
            }).when('/macrocategories', {
                templateUrl: '/view/administrator/macrocategories.html'
            }).when('/add_macrocategory', {
                    templateUrl: '/view/administrator/add_macrocategory.html'
            }).when('/categories', {
                templateUrl: '/view/administrator/categories.html'
            }).when('/add_categories', {
                templateUrl: '/view/administrator/add_categories.html'
            }).when('/subcategories', {
                templateUrl: '/view/administrator/subcategories.html'
            }).when('/add_categories', {
                templateUrl: '/view/administrator/add_categories.html'
            }).when('/add_subcategories', {
                templateUrl: '/view/administrator/add_subcategories.html'
            }).otherwise({
                redirectTo: "/404"
            })
        }
    ]);
}).call(this);

(function () {
    'use strict';
    angular.module('admin.ctrls', [])
        .controller('dashboard', function ($scope, $routeParams, chart) {
            $scope.pieData = {};
            $scope.count_users = 0;
            chart.loadData().success(function (r) {
                $scope.pieData = r.pieData;
                $scope.count_users = r.users;
            });
        }).controller('usersCtrl', function ($scope, $routeParams, users,$filter) {
            $scope.users = {};
            $scope.numPerPageOpt        = [5, 10, 20];
            $scope.numPerPage           = $scope.numPerPageOpt[1];
            $scope.currentPage          = 1;
            $scope.currentPageStores    = [];
            $scope.totalUsers           = 0;
            $scope.users                = [];
            $scope.row                  = 'created';
            $scope.orderType            = 'DESC';
            $scope.select = function(page) {
                users.list($scope.numPerPage,page,$scope.row,$scope.orderType).success(function(r){
                    $scope.users = r.users;
                    $scope.totalUsers = r.count;
                });
            };
            $scope.enable = function(user_id){
                users.enable(user_id).success(function(r){
                    if(r.data.success) {
                        var user = $filter('filter')($scope.users, {"id": user_id});
                        if(user[0]) {
                            user[0].confirmed = true;
                        }
                    }
                });
            }
            $scope.onFilterChange = function() {
                $scope.select(1);
                $scope.currentPage = 1;
                return $scope.row = '';
            };
            $scope.onNumPerPageChange = function() {
                $scope.select(1);
                return $scope.currentPage = 1;
            };
            $scope.onOrderChange = function() {
                $scope.select(1);
                return $scope.currentPage = 1;
            };
            $scope.order = function(rowName,orderType) {
                if ($scope.row === rowName && $scope.orderType === orderType) {
                    return;
                }
                $scope.row = rowName;
                $scope.orderType = orderType;
                return $scope.onOrderChange();
            };
            var init = function() {
                return $scope.select($scope.currentPage);
            };
            return init();

        }).controller('add_macrocategories', function ($scope,macrocategories) {
            $scope.macrocategories = {
                labels : ""
            };
            $scope.saveMacorcategories = function() {
                macrocategories.saveMacrocategories($scope.macrocategories.labels).success(function (r) {

                });
            }
        }).controller('macrocategories', function ($scope,macrocategories) {
            $scope.macrocategories = [];
            macrocategories.loadAll().success(function (r) {
                $scope.macrocategories = r.macrocategories;
            });
        })
        .controller('categories', function ($scope,categories) {
            $scope.categories = [];
            categories.loadAll().success(function (r) {
                $scope.categories = r.categories;
            });
        })
        .controller('add_categories', function ($scope,categories,macrocategories) {
            $scope.categories = {
                labels : "",
                macrocategory_id: ""
            };
            $scope.macrocategories = [];
            macrocategories.loadAll().success(function (r) {
                $scope.macrocategories = r.macrocategories;
            });
            $scope.saveCategories = function() {
                categories.saveCategories($scope.categories.labels,$scope.categories.macrocategory_id).success(function (r) {

                });
            }
        })
        .controller('subcategories', function ($scope,subcategories) {
            $scope.subcategories = [];
            subcategories.loadAll().success(function (r) {
                $scope.subcategories = r.subcategories;
            });
        })
        .controller('add_subcategories', function ($scope,subcategories,categories) {
            $scope.subcategories = {
                labels : "",
                category_id: ""
            };
            $scope.categories = [];
            categories.loadAll().success(function (r) {
                $scope.categories = r.categories;
            });
            $scope.saveSubcategories = function() {
                subcategories.saveSubcategories($scope.subcategories.labels,$scope.subcategories.category_id).success(function (r) {

                });
            }
        })
}).call(this);
(function() {
    'use strict';
    angular.module('admin.services', [])
        .factory("chart",function($http){
            return {
                loadData: function () {
                    return $http.get('/administrator/loadData.json');
                }
            }
        })
        .factory("users",function($http){
            return {
                list: function (n, page, row, orderType) {
                    return $http.get('/administrator/loadUsers/' + n + '/' + page + '/' + row + '/' + orderType + '.json');
                },
                enable: function(user_id){
                    return $http.post('/administrator/enableUser.json',{user_id:user_id});
                }
            }
        })
        .factory("macrocategories",function($http){
            return {
                saveMacrocategories: function(macrocategories){
                    return $http.post('/administrator/saveMacrocategories.json',{macrocategories:macrocategories});
                },
                loadAll: function(){
                    return $http.get('/administrator/loadMacrocategories.json');
                }
            }
        })
        .factory("categories",function($http){
            return {
                saveCategories: function(categories,macrocategory_id){
                    return $http.post('/administrator/saveCategories.json',{categories:categories,macrocategory_id:macrocategory_id});
                },
                loadAll: function(){
                    return $http.get('/administrator/loadCategories.json');
                }
            }
        }).factory("subcategories",function($http){
            return {
                saveSubcategories: function(subcategories,category_id){
                    return $http.post('/administrator/saveSubcategories.json',{subcategories:subcategories,category_id:category_id});
                },
                loadAll: function(){
                    return $http.get('/administrator/loadSubcategories.json');
                }
            }
        })
}).call(this);

(function() {
    'use strict';
    angular.module('cart', ['ngAnimate','cart.controllers','cart.services','ui.bootstrap']);
}).call(this);
(function() {
    'use strict';
    angular.module('cart.controllers', [])
        .controller('cartCtrl', function($scope,$window,cart) {
            $scope.cart = {
                products: [],
                total: 0,
                total_quantity:0
            }
            cart.load(55).success(function(r){
                $scope.cart = r.cart;
            });
            $scope.updateProduct = function(product){
                cart.updateProduct(product.id,product.quantity,55).success(function(r){
                    if(r.data.success) {
                        $scope.cart = r.cart;
                    }
                })
            }
            $scope.removeProduct = function(product) {
                console.log("___");
                cart.removeProduct(product.id,55).success(function(r){
                    if(r.data.success) {
                        $scope.cart = r.cart;
                    }
                })
            }

            $scope.toggleDropdown = function($event) {
                $event.preventDefault();
                $event.stopPropagation();
                return false;
            };
            $scope.toggled = function(open) {
                if(open) {
                    var obj = {
                        height: 350,
                        width: 600,
                        name: "resize"
                    }
                    $window.parent.postMessage(obj, '*');
                } else {
                    var obj = {
                        height: 80,
                        width: 150,
                        name: "resize"
                    }
                    $window.parent.postMessage(obj, '*');
                }
            };
            $($window).on("message", function(e){
                var data = e.originalEvent.data;
                if(data.name == "addToCart") {
                    cart.addProduct(data.product_id,data.quantity,data.shop_id).success(function(r){
                        if(r.data.success) {
                            $scope.cart = r.cart;
                        }
                    })
                }
            });

        })
}).call(this);

(function() {
    'use strict';
    angular.module('cart.services', []).factory('cart',function($http){
        return {
            addProduct: function(product_id,quantity,shop_id){
                return $http.post('/carts/addProduct.json',{product_id:product_id,quantity:quantity,shop_id:shop_id});
            },
            updateProduct: function(product_id,quantity,shop_id){
                return $http.post('/carts/updateProduct.json',{product_id:product_id,quantity:quantity,shop_id:shop_id});
            },
            removeProduct: function(product_id,shop_id){
                return $http.post('/carts/removeProduct.json',{product_id:product_id,shop_id:shop_id})
            },
            load: function(shop_id) {
                return $http.post('/carts/load.json',{shop_id:shop_id});
            }
        }
    })
}).call(this);

(function() {
    'use strict';
    angular.module('main', ['ngRoute', 'ngAnimate', 'ui.bootstrap','mgo-angular-wizard','main.ctrls','main.services','shared.directives','shared.services','ngActivityIndicator','angularFileUpload','top_bar.services','top_bar.ctrls','toaster']).config([
        '$routeProvider', function($routeProvider) {
            return $routeProvider
            .when('/_=_', {
                redirectTo: '/'
            })
            .when('/', {
                templateUrl: '/view/shops/add'
            })
            .when('/account', {
                templateUrl: '/view/users/account.html'
            })
            .when('/orders', {
                templateUrl: '/view/account/orders.html'
            }).when('/order/:order_uid', {
                templateUrl: '/view/account/order_details.html'
            })

        }
    ]);
}).call(this);

(function () {
    'use strict';
    angular.module('main.ctrls', [])
        .controller('shopCtrl', function ($scope, WizardHandler, shopService, countries, $activityIndicator) {
            $activityIndicator.startAnimating();
            $scope.shop = {
                name: '',
                url: '',
                business_name: '',
                vat_number: '',
                address: '',
                city: '',
                cap: '',
                state: 'IT',
                province: '',
                macrocategory_id: '',
                phone_number: '',
                code: ''

            };
            $scope.categories = {};
            $scope.showError = false;
            $scope.showErrorData = false;
            $scope.availableUrl = true;
            $scope.availableVatNumber = true;
            $scope.sent = false;
            $scope.verified = false;
            countries.get().success(function (r) {
                $scope.countries = r.countries;
                $activityIndicator.stopAnimating();
            });
            $scope.validate_data_first_step = function () {
                var vat_number = $scope.shop.vat_number;
                if (vat_number) {
                    shopService.ckPiva($scope.shop.state, vat_number).success(function (r) {
                        shopService.ckVatNumber($scope.shop.vat_number).success(function(r1){
                            if(!r1.data.success){
                                $scope.availableVatNumber = false;
                            } else {
                                $scope.availableVatNumber = true;
                                if(r.valid) {
                                    var address = r.address.split('\n');
                                    var street = address[0];
                                    var address_data = address[1];
                                    address_data = address_data.split(" ");
                                    var cap = address_data[0];
                                    delete address_data[0];
                                    var province = address_data[address_data.length - 1];
                                    delete address_data[address_data.length - 1];
                                    var city = address_data.join(" ", address_data);
                                    $scope.shop.name = r.name.trim();
                                    $scope.shop.business_name = r.name.trim();
                                    $scope.shop.cap = cap.trim();
                                    $scope.shop.city = city.trim();
                                    $scope.shop.address = street.trim();
                                    $scope.shop.address = street.trim();
                                    $scope.shop.province = province.trim();
                                } else {
                                    $scope.shop.name = "";
                                    $scope.shop.business_name = "";
                                    $scope.shop.cap = "";
                                    $scope.shop.city = "";
                                    $scope.shop.address = "";
                                    $scope.shop.address = "";
                                    $scope.shop.province = "";
                                }
                                $scope.showError = false;
                                $scope.showErrorData = false;
                                WizardHandler.wizard().next();
                            }
                        });
                    })
                } else {
                    $scope.showError = true;
                    $scope.showErrorData = true;
                }
            }
            $scope.checkShop = function () {
                if ($scope.shop.name && $scope.shop.url && $scope.shop.macrocategory_id) {
                    shopService.ckName($scope.shop.url).success(function (r) {
                        if (r.data.success) {
                            WizardHandler.wizard().next();
                            $scope.availableUrl = true;
                            $scope.showError = false;
                        } else {
                            $scope.availableUrl = false;
                            $scope.showError = false;
                        }
                    })
                } else {
                    $scope.showError = true;
                }

            }
            $scope.checkShopData = function () {
                if ($scope.shop.vat_number && $scope.shop.business_name
                    && $scope.shop.address && $scope.shop.address
                    && $scope.shop.city && $scope.shop.cap
                    && $scope.shop.state && $scope.shop.province) {
                    $scope.showErrorData = false;
                    shopService.getCategories().success(function (r) {
                        $scope.categories = r.categories;
                        $scope.shop.macrocategory_id = r.categories[0].id;
                        if(!$scope.shop.url) {
                            $scope.shop.url = $scope.shop.business_name.replace(/[^a-zA-Z0-9]/g,'').toLowerCase();
                        }
                        if(!$scope.shop.name) {
                            $scope.shop.name = $scope.shop.business_name;
                        }
                        WizardHandler.wizard().next();
                    })
                } else {
                    $scope.showErrorData = true;
                }
            };
            $scope.sendCode = function () {
                if($scope.shop.phone_number) {
                    shopService.sendCode($scope.shop.phone_number).success(function () {
                        $scope.sent = true;
                    })
                }
            };

            $scope.verifyCode = function () {
                shopService.verifyCode($scope.shop.code).success(function (r) {
                    $scope.showError = false;
                    if(r.data.success) {
                        $scope.verified = true;
                    } else {
                        $scope.showError = true;
                    }
                })
            }
			
			$scope.retry=function(){
				$scope.sent=false;
				$scope.verified=false;
				}

            $scope.saveData = function () {
                shopService.save($scope.shop).success(function (r) {
                    if (r.data.success) {
                        window.location.href = "/dashboard/shop/" + $scope.shop.url + "/";
                    }
                });
            }
        }).controller("mainCtrl", function ($scope, post, $activityIndicator, $filter, FileUploader) {
            $scope.posts = {};
            $scope.post = {
                text: ''
            }
            $scope.tmpPost = {};
            $scope.comment = {
                text: ''
            }

            $scope.uploader = new FileUploader({
                url: "/posts/loadImage",
                onBeforeUploadItem: function (item) {
                    item.url = $scope.uploader.url;
                },
                onSuccessItem: function (item, response) {
                    if (response.data.success) {
                        $scope.tmpPost.image = response.data.url;
                        $scope.posts.unshift($scope.tmpPost);
                        $scope.post.text = '';
                        $scope.tmpPost = {};
                        $scope.uploader.clearQueue();
                    }
                }
            });

            post.getList().success(function (r) {
                $scope.posts = r.posts;
            });

            $scope.like = function (post_id) {
                post.like(post_id).success(function (r) {
                    if (r.data.success) {
                        var p = $filter('filter')($scope.posts, {"post_id": post_id});
                        triggerPreference(post_id, r.data);
                    }
                });
            }
            $scope.dislike = function (post_id) {
                post.dislike(post_id).success(function (r) {
                    if (r.data.success) {
                        triggerPreference(post_id, r.data);
                    }
                });
            }
            $scope.uninteresting = function (post_id) {
                post.uninteresting(post_id).success(function (r) {
                    if (r.data.success) {
                        triggerPreference(post_id, r.data);
                    }
                });
            }
            $scope.addComment = function (post_id) {
                var p = $filter('filter')($scope.posts, {"post_id": post_id});
                post.addComment(post_id, p[0].comment.text).success(function (r) {
                    if (r.data.success) {
                        p[0].comments.push(r.data.comment);
                        p[0].comment.text = "";
                    }
                });
            }
            $scope.save = function () {
                post.add($scope.post).success(function (r) {
                    if (r.data.success) {
                        if ($scope.uploader.queue.length) {
                            $scope.uploader.url = "/posts/loadImage/" + r.data.post.post_id + ".json";
                            $scope.uploader.uploadAll();
                            $scope.tmpPost = r.data.post;
                        } else {
                            $scope.posts.unshift(r.data.post);
                            $scope.post.text = '';
                        }
                    }
                })
            }

            function triggerPreference(post_id, data) {
                var p = $filter('filter')($scope.posts, {"post_id": post_id});
                p[0].preference = 0;
                if (data.trigger.uninteresting == "add") {
                    p[0].uninteresting++;
                    p[0].preference = 3;
                } else if (data.trigger.uninteresting == "remove") {
                    p[0].uninteresting--;
                }
                if (data.trigger.dislike == "add") {
                    p[0].dislikes++;
                    p[0].preference = 2;
                } else if (data.trigger.dislike == "remove") {
                    p[0].dislikes--;
                }
                if (data.trigger.like == "add") {
                    p[0].likes++;
                    p[0].preference = 1;
                } else if (data.trigger.like == "remove") {
                    p[0].likes--;
                }
            }

        }).controller('user_ctrl', function ($scope, $routeParams, users) {
            $scope.user = {};
            users.getUser($routeParams.username).success(function (r) {
                $scope.user = r.user;
            });
        }).controller('account', function ($scope,users,FileUploader) {
            $scope.user = {
                first_name: '',
                last_name: '',
                new_password: '',
                confirm_password: '',
                image: '/asset/admin/images/g1.jpg'
            };
            users.loadUser().success(function (r)
            {
                $scope.user.first_name = r.user.first_name;
                $scope.user.last_name = r.user.last_name;
                if(r.user.image) {
                    $scope.user.image = r.user.image;
                }

            });
            $scope.saveAccount = function(){
                users.updateAccount($scope.user).success(function(r){
                });
            }
            $scope.uploaderProfile = new FileUploader({
                url: "/users/loadProfile.json",
                onBeforeUploadItem: function(item){
                    item.url = $scope.uploaderProfile.url;
                    $scope.loadingImage = true;
                },
                onProgressAll: function(progress){
                    $scope.progress = progress;
                },
                onAfterAddingAll : function()  {
                    $scope.uploaderProfile.uploadAll();
                },
                onSuccessItem: function(item, response){
                    if(response.data.success) {
                        $scope.user.image = response.data.url;
                    }
                    $scope.loadingImage = false;
                },
                onErrorItem: function(item, response, status, headers){
                    $scope.loadingImage = false;
                }
            });

        })
}).call(this);
(function() {
    'use strict';
    angular.module('main.services', [])
        .factory("shopService", function ($http) {
            return {
                save: function (shop) {
                    return $http.post('/shops/saveShop.json',{shop:shop});
                },
                ckName: function(url){
                    return $http.get('/shops/checkShopName/'+url+'.json');
                },
                ckPiva: function(countyCode,piva) {
                    return $http.get('http://130.211.96.48:3000/validate_vat_number/'+countyCode+'/'+piva);
                },
                getCategories: function(){
                    return $http.get('/shops/getCategories.json');
                },
                sendCode: function(phone_number) {
                    return $http.post('/shops/sendCode.json',{phone_number:phone_number});
                },
                verifyCode: function(code){
                    return $http.post('/shops/verifyCode.json',{code:code});
                },
                ckVatNumber: function(vat_number){
                    return $http.get('/shops/checkVatNumber/'+vat_number+'.json');
                }
            }
        }).factory("post",function($http){
            return {
                getList: function(){
                    return $http.get('/posts.json');
                },
                like: function(post) {
                    return $http.post('/posts/like.json',{post:post});

                },
                dislike: function(post) {
                    return $http.post('/posts/dislike.json',{post:post});

                },
                uninteresting: function(post) {
                    return $http.post('/posts/uninteresting.json',{post:post});

                },
                addComment: function(post,text) {
                    return $http.post('/posts/addComment.json',{post:post,comment:text});

                },
                add : function(post){
                    return $http.post('/posts/add.json',{post:post});
                }
            }
        }).factory("users",function($http){
            return {
                getUser: function (user) {
                    return $http.get('/users/getUser/' + user + '.json');
                },
                loadUser: function(){
                    return $http.get('/users/loadUser.json');
                },
                updateAccount: function(user) {
                    return $http.post('/users/updateAccount.json',{user:user});
                }
            }
        })
}).call(this);

(function() {
    angular.module('shared.directives', []).directive('imgHolder', [
        function() {
            return {
                restrict: 'A',
                link: function(scope, ele, attrs) {
                    return Holder.run({
                        images: ele[0]
                    });
                }
            };
        }
    ]).filter('ofsetLimitTo', function() {
        return function(items, ofset, limit) {
            if(items != undefined) {
                var filtered = [];
                for (var i = 0; i < items.length; i++) {
                    if(ofset <= i && i <limit) {
                        filtered.push(items[i]);
                    }
                }
                return filtered;
            }
        };
    }).directive('stopEvent', function () {
        return {
            restrict: 'A',
            link: function (scope, element, attr) {
                element.bind('click', function (e) {
                    e.stopPropagation();
                });
            }
        };
    }).directive('compile', function($compile) {
        // directive factory creates a link function
        return function(scope, element, attrs) {
            scope.$watch(
                function(scope) {
                    // watch the 'compile' expression for changes
                    return scope.$eval(attrs.compile);
                },
                function(value) {
                    // when the 'compile' expression changes
                    // assign it into the current DOM
                    element.html(value);

                    // compile the new DOM and link it to the current
                    // scope.
                    // NOTE: we only compile .childNodes so that
                    // we don't get into infinite loop compiling ourselves
                    $compile(element.contents())(scope);
                }
            );
        };
    }).directive('customBackground', function() {
        return {
            restrict: "A",
            controller: [
                '$scope', '$element', '$location', function($scope, $element, $location) {
                    var addBg, path;
                    path = function() {
                        return $location.path();
                    };
                    addBg = function(path) {
                        $element.removeClass('body-home body-special body-tasks body-lock');
                        switch (path) {
                            case '/':
                                return $element.addClass('body-home');
                            case '/404':
                            case '/pages/500':
                            case '/pages/signin':
                            case '/pages/signup':
                            case '/pages/forgot':
                                return $element.addClass('body-special');
                            case '/pages/lock-screen':
                                return $element.addClass('body-special body-lock');
                            case '/tasks':
                                return $element.addClass('body-tasks');
                        }
                    };
                    addBg($location.path());
                    return $scope.$watch(path, function(newVal, oldVal) {
                        if (newVal === oldVal) {
                            return;
                        }
                        return addBg($location.path());
                    });
                }
            ]
        };
    }).directive('uiColorSwitch', [
        function() {
            return {
                restrict: 'A',
                link: function(scope, ele, attrs) {
                    return ele.find('.color-option').on('click', function(event) {
                        var $this, hrefUrl, style;
                        $this = $(this);
                        hrefUrl = void 0;
                        style = $this.data('style');
                        if (style === 'loulou') {
                            hrefUrl = 'styles/main.css';
                            $('link[href^="styles/main"]').attr('href', hrefUrl);
                        } else if (style) {
                            style = '-' + style;
                            hrefUrl = 'styles/main' + style + '.css';
                            $('link[href^="styles/main"]').attr('href', hrefUrl);
                        } else {
                            return false;
                        }
                        return event.preventDefault();
                    });
                }
            };
        }
    ]).directive('toggleMinNav', [
        '$rootScope', function($rootScope) {
            return {
                restrict: 'A',
                link: function(scope, ele, attrs) {
                    var $content, $nav, $window, Timer, app, updateClass;
                    app = $('#app');
                    $window = $(window);
                    $nav = $('#nav-container');
                    $content = $('#content');
                    ele.on('click', function(e) {
                        if (app.hasClass('nav-min')) {
                            app.removeClass('nav-min');
                        } else {
                            app.addClass('nav-min');
                            $rootScope.$broadcast('minNav:enabled');
                        }
                        return e.preventDefault();
                    });
                    Timer = void 0;
                    updateClass = function() {
                        var width;
                        width = $window.width();
                        if (width < 768) {
                            return app.removeClass('nav-min');
                        }
                    };
                    return $window.resize(function() {
                        var t;
                        clearTimeout(t);
                        return t = setTimeout(updateClass, 300);
                    });
                }
            };
        }
    ]).directive('collapseNav', [
        function() {
            return {
                restrict: 'A',
                link: function(scope, ele, attrs) {
                    var $a, $aRest, $lists, $listsRest, app;
                    $lists = ele.find('ul').parent('li');
                    $lists.append('<i class="fa fa-caret-right icon-has-ul"></i>');
                    $a = $lists.children('a');
                    $listsRest = ele.children('li').not($lists);
                    $aRest = $listsRest.children('a');
                    app = $('#app');
                    $a.on('click', function(event) {
                        var $parent, $this;
                        if (app.hasClass('nav-min')) {
                            return false;
                        }
                        $this = $(this);
                        $parent = $this.parent('li');
                        $lists.not($parent).removeClass('open').find('ul').slideUp();
                        $parent.toggleClass('open').find('ul').stop().slideToggle();
                        return event.preventDefault();
                    });
                    $aRest.on('click', function(event) {
                        return $lists.removeClass('open').find('ul').slideUp();
                    });
                    return scope.$on('minNav:enabled', function(event) {
                        return $lists.removeClass('open').find('ul').slideUp();
                    });
                }
            };
        }
    ]).directive('highlightActive', [
        function() {
            return {
                restrict: "A",
                controller: [
                    '$scope', '$element', '$attrs', '$location', function($scope, $element, $attrs, $location) {
                        var highlightActive, links, path;
                        links = $element.find('a');
                        path = function() {
                            return $location.path();
                        };
                        highlightActive = function(links, path) {
                            path = '#' + path;
                            return angular.forEach(links, function(link) {
                                var $li, $link, href;
                                $link = angular.element(link);
                                $li = $link.parent('li');
                                href = $link.attr('href');
                                if ($li.hasClass('active')) {
                                    $li.removeClass('active');
                                }
                                if (path.indexOf(href) === 0) {
                                    return $li.addClass('active');
                                }
                            });
                        };
                        highlightActive(links, $location.path());
                        return $scope.$watch(path, function(newVal, oldVal) {
                            if (newVal === oldVal) {
                                return;
                            }
                            return highlightActive(links, $location.path());
                        });
                    }
                ]
            };
        }
    ]).directive('toggleOffCanvas', [
        function() {
            return {
                restrict: 'A',
                link: function(scope, ele, attrs) {
                    return ele.on('click', function() {
                        return $('#app').toggleClass('on-canvas');
                    });
                }
            };
        }
    ])
        /*
         .directive('slimScroll', [
         function() {
         return {
         restrict: 'A',
         link: function(scope, ele, attrs) {
         return ele.slimScroll({
         height: attrs.scrollHeight || '100%'
         });
         }
         };
         }
         ])*/.directive('goBack', [
            function() {
                return {
                    restrict: "A",
                    controller: [
                        '$scope', '$element', '$window', function($scope, $element, $window) {
                            return $element.on('click', function() {
                                return $window.history.back();
                            });
                        }
                    ]
                };
            }
        ]).
        directive('uiFileUpload', [
            function() {
                return {
                    restrict: 'A',
                    link: function(scope, ele) {
                        return ele.bootstrapFileInput();
                    }
                };
            }
        ]).directive("contenteditable", function() {
        return {
            restrict: "A",
            require: "ngModel",
            link: function(scope, element, attrs, ngModel) {

                function read() {
                    ngModel.$setViewValue(element.html());
                }

                ngModel.$render = function() {
                    element.html(ngModel.$viewValue || "");
                };

                element.bind("blur keyup change", function() {
                    scope.$apply(read);
                });

                element.bind("focusout", function() {
                    var value =  element.html();
                    value = value.replace(/\W/g, '_');
                    ngModel.$modelValue = value;
                });

            }
        };
    }).directive('ngThumb', ['$window', function($window) {
            var helper = {
                support: !!($window.FileReader && $window.CanvasRenderingContext2D),
                isFile: function(item) {
                    return angular.isObject(item) && item instanceof $window.File;
                },
                isImage: function(file) {
                    var type = '|' + file.type.slice(file.type.lastIndexOf('/') + 1) + '|';
                    return '|jpg|png|jpeg|bmp|gif|'.indexOf(type) !== -1;
                }
            };
            return {
                restrict: 'A',
                template: '<canvas/>',
                link: function(scope, element, attributes) {
                    if (!helper.support) return;
                    var params = scope.$eval(attributes.ngThumb);
                    if (!helper.isFile(params.file)) return;
                    if (!helper.isImage(params.file)) return;
                    var canvas = element.find('canvas');
                    var reader = new FileReader();
                    reader.onload = onLoadFile;
                    reader.readAsDataURL(params.file);
                    function onLoadFile(event) {
                        var img = new Image();
                        img.onload = onLoadImage;
                        img.src = event.target.result;
                    }
                    function onLoadImage() {
                        var width = params.width || this.width / this.height * params.height;
                        var height = params.height || this.height / this.width * params.width;
                        canvas.attr({ width: width, height: height });
                        canvas[0].getContext('2d').drawImage(this, 0, 0, width, height);
                    }
                }
            };
        }]).directive('piterestGrind', function($timeout) {
            return {
                restrict: 'AE',
                replace: true,
                link: function(scope, ele, attrs) {
                    if (scope.$last === true) {
                        $timeout(function(){
                            $('#blog-landing').unbind('pinterest_grid');;
                            $('#blog-landing').pinterest_grid({
                                no_columns: 3,
                                padding_x: 15,
                                padding_y: 15,
                                margin_bottom: 50,
                                single_column_breakpoint: 1170
                            });
                        });
                    }
                }
            };
        })
        .directive('zoomImage', function($timeout) {
            return {
                restrict: 'AE',
                replace: true,
                link: function(scope, ele, attrs) {
                    if (scope.$last === true) {
                        $timeout(function(){
                            $(".sp-wrap").append('<div class="sp-large"></div><div class="sp-thumbs sp-tb-active"></div>');
                            $(".sp-wrap a").appendTo(".sp-thumbs");
                            $(".sp-thumbs a:first").addClass("sp-current").clone().removeClass("sp-current").appendTo(".sp-large");
                            $(".sp-wrap").css("display", "inline-block");
                            var slideTiming = 300;
                            var maxWidth = $(".sp-large img").width();
                            $(".sp-thumbs").live("click", function(e) {
                                e.preventDefault()
                            });
                            $(".sp-tb-active a").live("click", function(e) {
                                $(".sp-current").removeClass();
                                $(".sp-thumbs").removeClass("sp-tb-active");
                                $(".sp-zoom").remove();
                                var t = $(".sp-large").height();
                                $(".sp-large").css({
                                    overflow: "hidden",
                                    height: t + "px"
                                });
                                $(".sp-large a").remove();
                                $(this).addClass("sp-current").clone().hide().removeClass("sp-current").appendTo(".sp-large").fadeIn(slideTiming, function() {
                                    var e = $(".sp-large img").height();
                                    $(".sp-large").height(t).animate({
                                        height: e
                                    }, "fast", function() {
                                        $(".sp-large").css("height", "auto")
                                    });
                                    $(".sp-thumbs").addClass("sp-tb-active")
                                });
                                e.preventDefault()
                            });
                            $(".sp-large a").live("click", function(e) {
                                var t = $(this).attr("href");
                                $(".sp-large").append('<div class="sp-zoom"><img src="' + t + '"/></div>');
                                $(".sp-zoom").fadeIn();
                                $(".sp-large").css({
                                    left: 0,
                                    top: 0
                                });
                                e.preventDefault()
                            });

                            $(".sp-large").mousemove(function(e) {
                                var t = $(".sp-large").width();
                                var n = $(".sp-large").height();
                                var r = $(".sp-zoom").width();
                                var i = $(".sp-zoom").height();
                                var s = $(this).parent().offset();
                                var o = e.pageX - s.left;
                                var u = e.pageY - s.top;
                                var a = Math.floor(o * (t - r) / t);
                                var f = Math.floor(u * (n - i) / n);
                                $(".sp-zoom").css({
                                    left: a,
                                    top: f
                                })
                            }).mouseout(function() {})
                            $(".sp-zoom").live("click", function(e) {
                                $(this).fadeOut(function() {
                                    $(this).remove()
                                })
                            })

                        });
                    }
                }
            };
        })
        .directive('confirmModal', function($modal) {
            return {
                restrict: 'A',
                scope : {
                    confirmCallback:'&',
                    confirmMessage:'@',
                    data: "="
                },
                link: function (scope, ele, attrs) {
                    ele.bind('click', function () {
                        var modalHtml = '<div class="modal-body">' + scope.confirmMessage+ '</div>';
                        modalHtml += '<div class="modal-footer"><button class="btn btn-primary" ng-click="ok()">OK</button><button class="btn btn-warning" ng-click="cancel()">Cancel</button></div>';
                        var modalInstance = $modal.open({
                            template: modalHtml,
                            controller: 'confirmCtrl'
                        });
                        modalInstance.result.then(function() {
                            scope.confirmCallback(scope.data);
                        }, function() {
                            //Modal dismissed
                        });
                    });
                }
            }
        })

        .directive('validateEquals', [
        function() {
            return {
                require: 'ngModel',
                link: function(scope, ele, attrs, ngModelCtrl) {
                    var validateEqual;
                    validateEqual = function(value) {
                        var valid;
                        valid = value === scope.$eval(attrs.validateEquals);
                        ngModelCtrl.$setValidity('equal', valid);
                        return typeof valid === "function" ? valid({
                            value: void 0
                        }) : void 0;
                    };
                    ngModelCtrl.$parsers.push(validateEqual);
                    ngModelCtrl.$formatters.push(validateEqual);
                    return scope.$watch(attrs.validateEquals, function(newValue, oldValue) {
                        if (newValue !== oldValue) {
                            return ngModelCtrl.$setViewValue(ngModelCtrl.$ViewValue);
                        }
                    });
                }
            };
        }])
        .directive('categoriesStep', function(categories,selectCategories) {
            return {
                restrict: 'AE',
                scope: true,
                link: function ($scope) {
                    selectCategories.init();
                    $scope.obj = selectCategories.data;
                    $scope.selectSelectedItem = function(item) {
                        selectCategories.data.macrocategorySelected = {id : item.Subcategory.Category.Macrocategory.id, label : item.Subcategory.Category.Macrocategory.label};
                        selectCategories.data.categorySelected      = {id : item.Subcategory.Category.id, label : item.Subcategory.Category.label};
                        selectCategories.data.subcategorySelected   = {id : item.Subcategory.id, label : item.Subcategory.label};
                    }

                    $scope.selectMacrocategory = function(itemSel) {
                        selectCategories.data.macrocategorySelected = {id : itemSel.Macrocategory.id, label : itemSel.Macrocategory.label};
                        selectCategories.data.categories = itemSel.Category;
                        selectCategories.data.macrocategoryCompleted = true;
                        selectCategories.data.categoryCompleted = false;
                        selectCategories.data.categorySelected = {};
                    };
                    $scope.selectCategory = function(itemSel) {
                        selectCategories.data.categorySelected = {id : itemSel.id, label : itemSel.label};
                        selectCategories.data.subcategories = itemSel.Subcategory;
                        selectCategories.data.categoryCompleted = true;
                        selectCategories.data.subcategorySelected = {};
                    };
                    $scope.selectSubcategory = function(itemSel) {
                        selectCategories.data.subcategorySelected = {id : itemSel.id, label : itemSel.label};
                        selectCategories.data.subcategoryCompleted = true;
                    };

                    $scope.goToCateStep = function() {
                        categories.validateMacrocategory(selectCategories.data.newMacrocategory).success(function(r) {
                            if(r.data.success) {
                                selectCategories.data.macroAlredyExist = false;
                                selectCategories.data.newMacroOk = true;
                                selectCategories.data.newCate = true;
                                selectCategories.data.macrocategoryCompleted = true;
                            } else {
                                selectCategories.data.newCate = false;
                                selectCategories.data.macrocategoryCompleted = false;
                                selectCategories.data.macroAlredyExist = true;
                                selectCategories.data.newMacroOk = false;
                            }
                            selectCategories.data.showMacroMsg = true;
                        });
                    }
                    $scope.goToSubStep = function() {
                        categories.validateCategory(selectCategories.data.newCategory).success(function(r) {
                            if(r.data.success) {
                                selectCategories.data.cateAlredyExist = false;
                                selectCategories.data.newCateOk = true;
                                selectCategories.data.newSub = selectCategories.data.categoryCompleted = true;
                            } else {
                                selectCategories.data.newSub = false;
                                selectCategories.data.categoryCompleted = false;
                                selectCategories.data.cateAlredyExist = true;
                                selectCategories.data.newCateOk = false;
                            }
                        });
                    }
                    $scope.validateSubcategory = function() {
                        categories.validateSubcategory(selectCategories.data.newSubcategory).success(function(r) {
                            if(r.data.success) {
                                selectCategories.data.subAlredyExist = false;
                                selectCategories.data.newSubOk = true;
                            } else {
                                selectCategories.data.subAlredyExist = true;
                                selectCategories.data.newSubOk = false;
                            }
                        });
                    }

                    $scope.addNewTree = function() {
                        selectCategories.data.macrocategorySelected     = false;
                        selectCategories.data.categorySelected          = false;
                        selectCategories.data.subcategorySelected       = false;
                        selectCategories.data.macrocategoryCompleted    = false;
                        selectCategories.data.categoryCompleted         = false;
                        selectCategories.data.newTree                   = true;
                    }

                    $scope.backMacroStep = function(newMacro) {
                        selectCategories.data.newMacroOk = false;
                        selectCategories.data.macroAlredyExist = false;
                        selectCategories.data.macrocategorySelected = {};
                        selectCategories.data.categorySelected = {};
                        selectCategories.data.subcategorySelected = {};
                        selectCategories.data.subcategoryCompleted = false;
                        selectCategories.data.categoryCompleted = false;
                        selectCategories.data.macrocategoryCompleted = false;
                        selectCategories.data.newCate = false;
                        selectCategories.data.newSub = false;
                        selectCategories.data.newMacro = newMacro;
                        selectCategories.data.newMacrocategory = selectCategories.data.newSubcategory = selectCategories.data.newCategory = '';
                    };
                    $scope.backCateStep = function(newCate) {
                        selectCategories.data.newCateOk = selectCategories.data.cateAlredyExist = false;
                        if(selectCategories.data.newMacro || newCate) {
                            selectCategories.data.newCate = true;
                        } else {
                            selectCategories.data.newCate = false;
                        }
                        selectCategories.data.categorySelected =
                            selectCategories.data.subcategorySelected = {};

                        selectCategories.data.subcategoryCompleted =
                            selectCategories.data.categoryCompleted =
                                selectCategories.data.newSub = false;

                        selectCategories.data.newSubcategory = selectCategories.data.newCategory = '';
                    };
                    $scope.backSubStep = function(newSub) {
                        selectCategories.data.newSubOk = selectCategories.data.subAlredyExist = false;
                        if(selectCategories.data.newCate || newSub) {
                            selectCategories.data.newSub = true;
                        } else {
                            selectCategories.data.newSub = false;
                        }
                        selectCategories.data.subcategorySelected = {};
                        selectCategories.data.subcategoryCompleted = false;
                        selectCategories.data.newSubcategory = '';
                    };
                }
            }
        }).directive('pieChart', [
        function() {
            return {
                restrict: 'E',
                template: '<div style="height: 250px"></div>',
                replace: true,
                link: function($scope, element, attrs) {
                    $scope.$watch(attrs.datachart, function() {
                        var data = $scope[attrs.datachart];
                        data.element = element;
                        if(data.percent ) {
                            data.formatter = function (value, data) {
                                return value + '%';
                            }
                        }
                        Morris.Donut(data);
                    });
                }
            };
        }])
        .directive('areaChart', [
            function() {
                return {
                    restrict: 'E',
                    replace: true,
                    template: '<div style="height: 250px"></div>',
                    link: function($scope, element, attrs) {
                        $scope.$watch(attrs.datachart, function() {
                            var data = $scope[attrs.datachart];
                            if(!$scope.chart) {
                                $scope.chart = [];
                            }
                            if(data && data.loaded) {
                                element.empty()
                                data.element = element;
                                $scope.chart[data.element] = Morris.Line(data);
                            }
                        });

                    }
                };
            }])
}).call(this);
(function() {
    'use strict';
    angular.module('shared.services', [])
        /*.factory('logger', [
            function() {

                var logIt;
                toastr.options = {
                    "closeButton": true,
                    "positionClass": "toast-bottom-right",
                    "timeOut" : 60000
                };
                logIt = function(message, type) {
                    return toastr[type](message);
                };
                return {
                    log: function(message) {
                        logIt(message, 'info');
                    },
                    logWarning: function(message) {
                        logIt(message, 'warning');
                    },
                    logSuccess: function(message) {
                        logIt(message, 'success');
                    },
                    logError: function(message) {
                        logIt(message, 'error');
                    }
                };

            }
        ])*/.factory("my_requests", function ($http) {
            return {
                get: function() {
                    return $http.get('/notifies/getMyRequests.json');
                }
            }
        }).factory("notifies", function ($http) {
            return {
                loadNotifies: function () {
                    return $http.get('/notifies.json');
                },
                readNotifies: function () {
                    return $http.get('/notifies/read.json');
                },
                viewsShopNotify: function() {
                 return $http.get('/notifies/viewsShopNotify.json');
                 },
                getShopNotify: function() {
                    return $http.get('/notifies/getShopNotify.json');
                },
                viewsUserNotify: function() {
                    return $http.get('/notifies/viewsUserNotify.json');
                },
                getAuthRealtimeNotify: function() {
                    return $http.post('/notifies/getAuthRealtimeNotify.json');
                },
                buyNow: function(product_id, negozyation_id) {
                    return $http.post('/notifies/buyNow.json', {product_id : product_id, negozyation_id : negozyation_id});
                },
                available: function(notify_id, negoziatyon_id, state) {
                    return $http.post('/notifies/available.json', {negoziatyon_id : negoziatyon_id, state : state, notify_id : notify_id});
                }
            }
        }).factory('socket', function ($rootScope) {
            var socket = io.connect('//notify.services.negozy.com:3002');
            return {
                on: function (eventName, callback) {
                    socket.on(eventName, function () {
                        var args = arguments;
                        $rootScope.$apply(function () {
                            callback.apply(socket, args);
                        });
                    });
                },
                emit: function (eventName, data, callback) {
                    socket.emit(eventName, data, function () {
                        var args = arguments;
                        $rootScope.$apply(function () {
                            if (callback) {
                                callback.apply(socket, args);
                            }
                        });
                    })
                }
            };
        })
}).call(this);

window.onerror = function(message, file, lineNumber) {
    var xmlhttp;
    if (window.XMLHttpRequest) {// code for IE7+, Firefox, Chrome, Opera, Safari
        xmlhttp=new XMLHttpRequest();
    }
    else {// code for IE6, IE5
        xmlhttp=new ActiveXObject("Microsoft.XMLHTTP");
    }
    xmlhttp.onreadystatechange=function() {
        if (xmlhttp.readyState==4 && xmlhttp.status==200) {
            console.log(xmlhttp.responseText);
            return false;
        }
    }
    xmlhttp.open("POST","/negozy_exception.json",true);
    xmlhttp.setRequestHeader("Content-type","application/x-www-form-urlencoded");
    xmlhttp.send('browser='+encodeURI(navigator.appVersion)+'&message='+encodeURI(message)+'&file='+encodeURI(file)+'&lineNumber='+encodeURI(lineNumber));

    return false;
}



(function() {
    'use strict';
    angular.module('top_bar.ctrls', [])
        .controller('topBarDataController', function($scope,shopTopBar,$rootScope) {
            shopTopBar.isPublic().success(function(r) {
                if(r.data.success) {
                    $rootScope.isShopPublic = r.data.shop.is_public;
                    angular.element(document.getElementsByClassName("shopOnlineSymbol")).removeClass('hide');
                }
            })
        }).controller('notifyController', function($scope,notifies,$filter,socket,toaster,cart, $location, $rootScope, $rootElement, my_requests){

            $scope.shop_notifies = {};
            $scope.shop_notifies.notify = [];
            $scope.shop_notifies.count = 0;
            $scope.shop_notifies.readed = false;

            $scope.user_notifies = {};
            $scope.user_notifies.notify = [];
            $scope.user_notifies.count = 0;
            $scope.user_notifies.readed = false;


            $scope.count_notifies = 0;
            $scope.readed  = false;

            notifies.loadNotifies().success(function(r){
                $scope.notifies = r.notifies;
                var p = $filter('filter')($scope.notifies, {"viewed":false});
                $scope.count_notifies = p.length;

                //$scope.count_post_notifies = r.count_notifies.post_notify_count;
                $scope.shop_notifies.count = r.count_notifies.shop_notify_count;
                $scope.user_notifies.count = r.count_notifies.user_notify_count;
            });

            /*
            $('.newsticker').newsTicker({
                row_height: 48,
                max_rows: 1,
                speed: 600,
                direction: 'up',
                duration: 20000,
                autostart: 1,
                pauseOnHover: 1
            });
            */

            /*
            $scope.readNotifies = function(e){
                if(e && !$scope.readed && $scope.count_notifies) {
                    notifies.readNotifies().success(function(r){
                        $scope.count_notifies = 0;
                        $scope.readed = true;
                    });
                }
            };
            */
            // Notifiche dello shop
            $scope.readShopNotifies = function(e) {
                if(e && !$scope.shop_notifies.readed) {
                    $scope.shop_notifies.loading = true;
                    notifies.viewsShopNotify(false).success(function(r){
                        $scope.shop_notifies.count = 0;
                        $scope.shop_notifies.readed = true;
                        $scope.shop_notifies.notify = r.shop_notifies;
                        $scope.shop_notifies.loading = false;
                    });
                }
            };
            notifies.getAuthRealtimeNotify().success(function(r) {
                socket.emit('auth', r.data);
            });
            socket.on('reciveNotify', function(data) {
                toaster.pop('info', "", data.toasty_html, 60000, 'trustedHtml');

                if(data.user_notify && !data.shop_notify) {
                    $scope.user_notifies.count++;
                    $scope.user_notifies.readed = false;
                } else if(data.shop_notify && !data.user_notify) {
                    $scope.shop_notifies.count++;
                    $scope.shop_notifies.readed = false;
                }

                // si devono aggiornare i dati realivi alle negoziazioni richiamando i servizi opportuni
                if($location.path() == '/requests' && $rootElement.attr('data-ng-app') == 'account') {
                    // aggiornamento notifiche utente
                    my_requests.get().success(function(r) {
                        if(r.data.success) {
                            $rootScope.open_requests = [];
                            $rootScope.closed_requests = [];
                            angular.forEach(r.data.requests, function(value, key) {
                                if(value.status == 'open') {
                                    $rootScope.open_requests.push(r.data.requests[key]);
                                } else if(value.status == 'closed') {
                                    $rootScope.closed_requests.push(r.data.requests[key]);
                                }
                            });
                            $rootScope.closed_requests = $filter('orderBy')($rootScope.closed_requests, 'data_closed', true);
                            $rootScope.open_requests = $filter('orderBy')($rootScope.open_requests, 'data_closed', true);
                        }
                    });
                } else if($location.path() == '/requests' && $rootElement.attr('data-ng-app') == 'shop') {
                    // aggiornamento notifiche shop
                    notifies.viewsShopNotify(true).success(function(r) {
                        $rootScope.open_notifies = [];
                        $rootScope.closed_notifies = [];
                        angular.forEach(r.shop_notifies, function(value, key) {
                            if(value.status == 'open') {
                                $rootScope.open_notifies.push(r.shop_notifies[key]);
                            } else if(value.status == 'closed') {
                                $rootScope.closed_notifies.push(r.shop_notifies[key]);
                            }
                        });
                        $rootScope.closed_notifies = $filter('orderBy')($rootScope.closed_notifies, 'created', true);
                        $rootScope.open_notifies = $filter('orderBy')($rootScope.open_notifies, 'created', true);
                    });
                }
            });

            $scope.available = function(notify_id, negoziatyon_id, state) {
                notifies.available(notify_id, negoziatyon_id, state).success(function(r){
                    if(state == false) {
                        angular.forEach($rootScope.open_notifies, function(value, key) {
                            if(value.id == notify_id) {
                                $rootScope.closed_notifies.push($rootScope.open_notifies[key]);
                                $rootScope.open_notifies.splice(key, 1);
                            }
                        });
                    }
                    $rootScope.closed_notifies = $filter('orderBy')($rootScope.closed_notifies, 'created', true);
                    $rootScope.open_notifies = $filter('orderBy')($rootScope.open_notifies, 'created', true);
                });
            }
            $scope.buyNow = function(product_id, negoziatyon_id) {
                notifies.buyNow(product_id, negoziatyon_id).success(function(r){
                    if(r.data.success && r.data.token) {
                        document.location = '/checkout/setOrder/'+r.data.token;
                    }
                });
            }

            // Notifiche dell'utente
            $scope.readUserNotifies = function(e) {
                if(e && !$scope.user_notifies.readed) {
                    $scope.user_notifies.loading = true;
                    notifies.viewsUserNotify().success(function(r){
                        $scope.user_notifies.count = 0;
                        $scope.user_notifies.readed = true;
                        $scope.user_notifies.notify = r.user_notifies;
                        $scope.user_notifies.loading = false;
                    });
                }
            }
        });
}).call(this);

(function() {
    'use strict';
    angular.module('top_bar.services', []).
        factory("cart",function($http){
            return {
                addToCart: function (product_id, quantity) {
                    return $http.post('/carts/addProductToCart.json', {product_id: product_id, quantity: quantity});
                }
            }
        }).
        factory("countries", function ($http) {
            return {
                get: function () {
                    return $http.get('/countries.json');
                }
            }
        }).
        factory("shopTopBar", function ($http) {
            return {
                isPublic: function () {
                    return $http.get('/shops/isPublic.json');
                }
            }
        })
}).call(this);

(function() {
    'use strict';
    angular.module('shop', ['ngRoute', 'ngAnimate', 'ui.bootstrap','plugins','shared.directives','shared.services','shop.controllers','shop.services','ngTagsInput','angularFileUpload','ngActivityIndicator','ui.sortable','textAngular','top_bar.services','top_bar.ctrls','toaster','ngMap','ngAutocomplete','ngSanitize', 'ui.select'])
        .config([
            '$routeProvider', function($routeProvider) {
                for (var template in pluginRoute.views) {
                    $routeProvider.when(
                        pluginRoute.views[template].url, {
                            templateUrl: pluginRoute.views[template].template
                        }
                    );
                }

                $routeProvider.when('/', {
                    redirectTo: '/dashboard'
                }).when('/dashboard', {
                    templateUrl: '/view/shops/dashboard.html'
                }).when('/products', {
                    templateUrl: '/view/products/list.html'
                }).when('/products/add', {
                    templateUrl: '/view/products/add.html'
                }).when('/products/edit/:url', {
                    templateUrl: '/view/products/edit.html'
                }).when('/app-store', {
                    templateUrl: '/view/appStore/'
                }).when('/app-store/apps/:category', {
                    templateUrl: '/view/appStore/apps.html'
                }).when('/app-store/:app_url', {
                    templateUrl: '/view/appStore/app/',
                    controller:'appCtrl'
                }).when('/help', {
                    templateUrl: '/view/help/list.html'
                }).when('/settings/shop', {
                    templateUrl: '/view/shops/settings.html'
                }).when('/settings/information', {
                    templateUrl: '/view/shops/tax_information.html'
                }).when('/settings/tax', {
                    templateUrl: '/view/taxes/list.html'
                }).when('/settings/shipping', {
                    templateUrl: '/view/shipments/list.html'
                }).when('/settings/payments', {
                    templateUrl: '/view/payments/list.html'
                }).when('/settings/terms', {
                    templateUrl: '/view/shops/terms.html'
                }).when('/orders', {
                    templateUrl: '/view/orders/list.html'
                }).when('/orders/:order_id', {
                    templateUrl: '/view/orders/order.html'
                }).when('/notifies', {
                    templateUrl: '/view/notifies/notifies.html'
                }).when('/requests', {
                    templateUrl: '/view/notifies/requests.html'
                }).when('/collections', {
                    templateUrl: '/view/collections/list.html'
                }).when('/collections/add', {
                    templateUrl: '/view/collections/add.html'
                }).when('/collections/edit/:collection_id', {
                    templateUrl: '/view/collections/edit.html'
                }).when('/themes', {
                    templateUrl: '/view/themes/list.html'
                }).when('/theme/:theme_uri', {
                    templateUrl: '/view/themes/theme.html'
                }).otherwise({
                    redirectTo: "/404"
                    //templateUrl: '/errors/400'
                });
                return $routeProvider;
            }
        ])
        .config(['$httpProvider', function($httpProvider) {
            $httpProvider.interceptors.push('errorHttpInterceptor');
        }]);
}).call(this);
(function() {
    'use strict';
    angular.module('shop.controllers', [])
        .controller('DashboardCtrl',  function($scope,$activityIndicator,$modal,shop, $rootScope) {
        $activityIndicator.startAnimating();
        $scope.dashboardData = {
            name: '',
            product_count: 0,
            order_count: 0,
            collection_count: 0
        };
        shop.dashboardData().success(function (r) {
            $scope.dashboardData = r.data;
            $rootScope.isShopPublic == r.data.is_public;
            $activityIndicator.stopAnimating();
        })

        $scope.publicShop = function () {
            var modalInstance = $modal.open({
                templateUrl: '/view/shops/modal_public_shop.html',
                controller: 'publicShopCtrl',
                backdrop: 'static',
                resolve: {
                    shop_data: function () {
                        return {site_url : $scope.dashboardData.site_url};
                    }
                }
            });
            modalInstance.result.then(function (result) {
                $rootScope.isShopPublic = result;
            });
        };
    })
        .controller('publicShopCtrl',  function($scope,$rootScope,$modalInstance,shop, $timeout,shop_data) {
        $rootScope.shop = shop_data;
        if($rootScope.isShopPublic == false || $rootScope.isShopPublic == undefined && ($rootScope.actions == undefined && $rootScope.count_actions == undefined)) {
            $scope.activity_indicator = true;
            shop.checkShop().success(function(r) {
                if(r.data.success) {
                    $rootScope.actions = r.data.actions;
                    $rootScope.count_actions = r.data.actions_count;
                    $scope.activity_indicator = false;

                    if($rootScope.count_actions == 0 && !(!$rootScope.settingStatus.taxes || !$rootScope.settingStatus.shipping || !$rootScope.settingStatus.payments || !$rootScope.settingStatus.terms)) {
                        $scope.public = true;
                        $scope.publicShop();
                    }
                }
            });
        }
        $scope.ok = function () {
            $modalInstance.close(true);
        };
        $scope.refreshPage = function () {
            $modalInstance.close(true);
            //location.reload();
        };
        $scope.cancel = function () {
            $modalInstance.dismiss('cancel');
        };

        $scope.publicShop = function() {
            $scope.activity_indicator = true;
            $timeout(function() {
                shop.publicShop().success(function(r) {
                    if(r.data.success) {
                        $scope.public = true;
                        $scope.activity_indicator = false;
                    }
                });
            },1000);
        }
    })
        .controller('AppStoreCtrl',  function($scope, app_store) {
        $scope.catsapps = [];
        app_store.getApplicationsGroupByCategories().success(function(r) {
            $scope.catsapps = r.data.catsapps;
            for(var k in $scope.catsapps) {
                $scope.catsapps[k].ofset = 0;
                $scope.catsapps[k].limit = 5;
                $scope.catsapps[k].stepper = 5;
            }
        });

        $scope.viewOther = function(cate) {
            if(cate.Application.length > cate.limit) {
                cate.ofset += cate.stepper;
                cate.limit += cate.stepper;
            }
        }
        $scope.viewRetry = function(cate) {
            cate.ofset -= cate.stepper;
            cate.limit -= cate.stepper;
        }
        $scope.retry = function(cate) {
            if(cate.ofset == 0) {
                return false;
            } else {
                return true;
            }
        }
    })
        .controller('SidebarCtrl',  function($rootScope,shop) {
        $rootScope.settingStatus = {
            taxes            : true,
            shipping         : true,
            payments         : true,
            terms            : true
        };
        shop.checkStatus().success(function(r){
            $rootScope.settingStatus = r.data;
        });
    })
        .controller('ProductCtrl', function($scope,product,tags,categories,FileUploader,$location,$activityIndicator,selectCategories,shop,$modal,$filter) {
            $activityIndicator.startAnimating();
            var name                    = '';
            //$scope.categoriesList       = [];
            //$scope.categoriesSelected   = [];
            $scope.successSave          = false;
            $scope.errorSave            = false;
            $scope.onLoading            = false;
            //$scope.errorCategories      = false;
            $scope.collections          = [];
            $scope.shipments            = [];
            $scope.tags                 = [];
            $scope.taxes                = [];
            $scope.shop_url             = '';
            $scope.progress             = 0;
            $scope.current_image        = '';
            $scope.count_image          = 0;
            $scope.total_price          = 0;
            $scope.success_images       = [];
            $scope.error_images         = [];
            $scope.product              = {
                'name': '',
                'url': '',
                'description': '',
                'price': '',
                'quantity': '',
                'type_quantity': 1,
                'shop_tax_id': '',
                'collection_id': '',
                'published': true
            };
            product.load_data().success(function(r){
                //$scope.categoriesSelected = r.data.categories_selected;
                //$scope.categoriesList = r.data.categories;
                $scope.taxes = r.data.taxes;
                if(r.data.taxes.length) {
                    $scope.product.shop_tax_id = r.data.taxes[0].id;
                }
                $scope.collections = r.data.collections;
                if(r.data.collections.length) {
                    $scope.product.collection_id = r.data.collections[0].id;
                }
                $scope.shipments = r.data.shipments;
                $scope.shop_url = r.data.shop.url;
                $activityIndicator.stopAnimating();
            });
            $scope.loadTags = function (query) {
                return tags.find(query);
            };
            $scope.changeTotalPrice = function(){
                var current_tax = $filter('filter')($scope.taxes, {"id": $scope.product.shop_tax_id});
                var tax_value = 0;
                if(current_tax.length && $scope.product.price) {
                    if(!current_tax[0].product_tax) {
                        tax_value = current_tax[0].tax;
                        tax_value = ($scope.product.price * tax_value) / 100;
                    }
                }
                var total_price = $scope.product.price + tax_value;
                total_price = $filter('number')(total_price, 2);
                $scope.total_price = total_price;
            }
            $scope.open = function (size) {
                var modalInstance = $modal.open({
                    templateUrl: '/view/taxes/add.html',
                    controller: 'addTaxCtrl',
                    size: size,
                    resolve: {
                        taxes: function () {
                            return $scope.taxes
                        }
                    }
                });
                modalInstance.result.then(function (taxItem) {
                    $scope.product.shop_tax_id = taxItem.id;
                });
            };
            $scope.addCollectionModal = function () {
                var modalInstance = $modal.open({
                    templateUrl: '/view/collections/modal_add.html',
                    controller: 'addCollectionModal',
                    resolve: {
                        collections_list: function () {
                            return $scope.collections;
                        }
                    }
                });
                modalInstance.result.then(function (collection_id) {
                    $scope.product.collection_id = collection_id;
                });
            };

            $scope.addShippingModal = function(){
                $modal.open({
                    templateUrl: '/view/shipments/add.html',
                    controller: 'addShippingCtrl',
                    resolve: {
                        shipments: function () {
                            return $scope.shipments;
                        }
                    }
                });
            }

            $scope.uploader = new FileUploader({
                url: "/products/loadImage",
                onBeforeUploadItem: function(item){
                    item.url = $scope.uploader.url+".json";
                    $scope.onLoading = true;
                    $scope.current_image = item._file.name;
                },
                onCompleteAll: function() {
                    $activityIndicator.stopAnimating();
                    $scope.successSave  = true;
                    $scope.onLoading    = false;
                },
                onProgressAll: function(progress){
                    $scope.progress = progress;
                },
                onSuccessItem: function(item, response){
                    if(response.data.success) {
                        $scope.count_image++;
                        $scope.success_images.push(item._file.name);
                    }
                },
                onErrorItem: function(item, response, status, headers){
                    $scope.error_images.push(item._file.name);
                }
            });
            $scope.updateURLfromName = function () {
                name = $scope.product.name;
                if (name) {
                    $scope.product.url = name.replace(/\W/g, '-');
                } else {
                    $scope.product.url = '';
                }
            }
            $scope.sortableOptions = {
                stop: function(e, ui) {
                    for (var index in $scope.uploader.queue) {
                        $scope.uploader.queue[index].order = index;
                    }
                }
            };

            $scope.saveProduct = function () {
                //$scope.errorCategories = false;
                //$scope.categories = selectCategories.calcCategories();
                if($scope.product.name && $scope.product.url && $scope.product.price){ // && $scope.categories) {
                    //$activityIndicator.startAnimating();
                    product.save($scope.product, $scope.tags).success(function (r) {
                        if (r.data.success && r.data.product_url) {
                            if ($scope.uploader.queue.length) {
                                $scope.uploader.url = "/products/loadImage/" + r.data.product_url;
                                $scope.uploader.uploadAll();
                            } else {
                                $scope.successSave = true;
                            }
                        } else {
                            $scope.errorSave = true;
                        }
                    });
                }
                /*
                else {

                    if(!$scope.categories) {
                        $scope.errorCategories = true;
                    }
                }
                 */
            }
        })
        .controller('ProductEditCtrl', function($scope,product,tags,selectCategories,FileUploader,$routeParams,$activityIndicator,$filter,$modal,toaster) {
            var name = '';
            $activityIndicator.startAnimating();
            //$scope.categoriesList       = [];
            //$scope.categoriesSelected   = [];
            $scope.successSave          = false;
            $scope.errorSave            = false;
            $scope.onLoading            = false;
            //$scope.errorCategories      = false;
            $scope.collections          = [];
            $scope.tags                 = [];
            $scope.taxes                = [];
            $scope.shipments            = [];
            $scope.shop_url             = '';
            $scope.progress             = 0;
            $scope.current_image        = '';
            $scope.count_image          = 0;
            $scope.total_price          = 0;
            product.load_product($routeParams.url).success(function(r){
                $scope.product = r.product;
                $scope.product.price = parseFloat($scope.product.price);
                $scope.native_url = r.product.url;
                $scope.tags = r.tags;
                $scope.images = r.images;
                $scope.taxes = r.data.taxes;
                $scope.collections = r.data.collections;
                $scope.shipments = r.data.shipments;
                //$scope.categoriesList = r.data.categories;
                //$scope.categoriesSelected = r.data.categories_selected;
                $scope.shop_url = r.data.shop.url;;
                //selectCategories.setCategories(r.categories,r.data.categories);
                $scope.changeTotalPrice();
                $activityIndicator.stopAnimating();
            });
            $scope.loadTags = function (query) {
                return tags.find(query);
            };
            $scope.open = function (size) {
                var modalInstance = $modal.open({
                    templateUrl: '/view/taxes/add.html',
                    controller: 'addTaxCtrl',
                    size: size,
                    resolve: {
                        taxes: function () {
                            return $scope.taxes
                        }
                    }
                });
                modalInstance.result.then(function (taxItem) {
                    $scope.product.shop_tax_id = taxItem.id;
                });
            };
            $scope.addShippingModal = function(){
                $modal.open({
                    templateUrl: '/view/shipments/add.html',
                    controller: 'addShippingCtrl',
                    resolve: {
                        shipments: function () {
                            return $scope.shipments;
                        }
                    }
                });
            }
            $scope.changeTotalPrice = function(){
                var current_tax = $filter('filter')($scope.taxes, {"id": $scope.product.shop_tax_id});
                var tax_value = 0;
                if(current_tax.length && $scope.product.price) {
                    if(!current_tax[0].product_tax) {
                        tax_value = current_tax[0].tax;
                        tax_value = ($scope.product.price * tax_value) / 100;
                    }
                }
                var total_price = $scope.product.price + tax_value;
                total_price = $filter('number')(total_price, 2);
                $scope.total_price = total_price;
            }
            $scope.uploader = new FileUploader({
                url: "/products/loadImage",
                onBeforeUploadItem: function(item){
                    item.url = $scope.uploader.url;
                    $scope.current_image = item._file.name;
                    $scope.onLoading = true;
                },
                onAfterAddingAll : function(item)  {
                    $scope.progress = 0;
                    $scope.uploader.url = "/products/loadImage/" + $routeParams.url + ".json";
                    $scope.uploader.uploadAll();
                },
                onProgressAll: function(progress){
                    $scope.progress = progress;
                },
                onSuccessItem: function(item, response){
                    if(response.data.success) {
                        var newImage = {
                            order: null,
                            product_id: null,
                            thumb: response.data.url,
                            name: response.data.name
                        }
                        $scope.images.push(newImage);
                        $scope.count_image++;
                    }
                    $scope.onLoading = false;
                },
                onErrorItem: function(item, response, status, headers){
                    $scope.onLoading = false;
                }
            });
            $scope.sortableOptions = {
                stop: function(e, ui) {
                    for (var index in $scope.images) {
                        $scope.images[index].order = index;
                    }
                }
            };
            $scope.updateURLfromName = function () {
                name = $scope.product.name;
                if (name) {
                    $scope.product.url = name.replace(/\W/g, '-');
                } else {
                    $scope.product.url = '';
                }
            };
            $scope.deleteImage = function(image) {
                if(image) {
                    product.remove_image($scope.product.url, image).success(function (r) {
                        if(r.data.success) {
                            angular.forEach($scope.images, function(value, key) {
                               if(value.name == image) {
                                   $scope.images.splice(key, 1);
                               }
                            });
                        }
                    });
                }
            };
            $scope.addCollectionModal = function () {
                var modalInstance = $modal.open({
                    templateUrl: '/view/collections/modal_add.html',
                    controller: 'addCollectionModal',
                    resolve: {
                        collections_list: function () {
                            return $scope.collections;
                        }
                    }
                });
                modalInstance.result.then(function (collection_id) {
                    $scope.product.collection_id = collection_id;
                });
            };
            $scope.saveProduct = function () {
                //$scope.categories = selectCategories.calcCategories();
                if($scope.product.name && $scope.product.url && $scope.product.price){ //&& $scope.categories) {
                    $activityIndicator.startAnimating();
                    product.update($scope.product, $scope.tags, $scope.categories,$scope.images).success(function (r) {
                        if (r.data.success && r.data.product_url) {
                            toaster.pop('success', "", r.data.message);
                            $activityIndicator.stopAnimating();
                        } else {
                            toaster.pop('error', "", r.data.message);
                            $activityIndicator.stopAnimating();
                        }
                    });
                } /*else {
                    if(!$scope.categories) {
                        $scope.errorCategories = true;
                    }
                }*/
            };
        })
        .controller('SettingsCtrl',function($scope, $filter,shop,$activityIndicator,FileUploader,toaster,$timeout, $rootScope){
            $activityIndicator.startAnimating();
            $scope.shop = {};
            $scope.images = [];
            $scope.logo = '/asset/images/no_logo.png';
            $scope.cover = '/asset/images/default_cover.jpg';
            $scope.loadingLogo = false;
            $scope.loadingCover = false;
            $scope.onLoading    = false;
            $scope.latitude     = 42.7945263;
            $scope.longitude    = 11.7693389;
            $scope.zoom         = 6;
            $scope.details      = {};
            $scope.mapInit      = false;
            shop.loadSettings().success(function(r){
                $scope.shop = r.shop;
                $scope.countries    = r.countries;
                $scope.categories   = r.categories;
                $scope.currencies   = r.currencies;
                if(r.images.length) {
                    $scope.images = r.images;
                }
                if(r.logo) {
                    $scope.logo = r.logo;
                }
                if(r.cover) {
                    $scope.cover = r.cover;
                }

                $activityIndicator.stopAnimating();
                $scope.$watchCollection("details", function (data) {
                    if(data.geometry) {
                        if(data.geometry.location) {
                            $scope.shop.latitude    =  data.geometry.location.k;
                            $scope.shop.longitude   =  data.geometry.location.D;
                            $scope.map.setCenter(new google.maps.LatLng($scope.shop.latitude, $scope.shop.longitude));
                            for(var key in $scope.map.markers) {
                                $scope.map.markers[key].setPosition(new google.maps.LatLng($scope.shop.latitude, $scope.shop.longitude));
                            }
                        }
                    }
                });
                var latlon = new google.maps.LatLng($scope.latitude, $scope.longitude);
                if (r.shop.latitude && r.shop.longitude) {
                    latlon = new google.maps.LatLng($scope.shop.latitude, $scope.shop.longitude);
                }
                $scope.updateMarker = function(data){
                    $scope.shop.latitude    =  data.latLng.k;
                    $scope.shop.longitude   =  data.latLng.D;
                }
                $scope.$on('mapInitialized', function(event, map) {
                    setTimeout(function(){
                        map.setCenter(latlon);
                        for (var key in map.markers) {
                            map.markers[key].setPosition(latlon);
                        }
                        $scope.map = map;
                    },2000);
                });
                $scope.mapInit = true;
            });
            $scope.save = function() {
                $activityIndicator.startAnimating();

                shop.updateShop($scope.shop).success(function(r){
                    if(r.data.success) {
                        toaster.pop('success', "", r.data.message);
                    } else {
                        toaster.pop('error', "", r.data.message);
                    }
                    $rootScope.isShopPublic = $scope.shop.is_public;
                    $activityIndicator.stopAnimating();
                })
            }
            $scope.uploaderImages = new FileUploader({
                url: "/shops/uploadImage.json",
                onBeforeUploadItem: function(item){
                    $scope.onLoading = true;
                    item.url = $scope.uploaderImages.url;
                    $scope.current_image = item._file.name;
                },
                onAfterAddingAll : function(item)  {
                    $scope.progress = 0;
                    $scope.uploaderImages.uploadAll();
                },
                onProgressAll: function(progress){
                    $scope.progress = progress;
                },
                onSuccessItem: function(item, response){
                    if(response.data.success) {
                        var newImage = {
                            image: response.data.url,
                            name: response.data.name
                        }
                        $scope.images.push(newImage);
                        $scope.count_image++;
                    }
                    $scope.onLoading = false;
                },
                onErrorItem: function(item, response, status, headers){
                    $scope.onLoading = false;
                }
            });

            $scope.deleteImage = function(image) {
                if(image) {
                    shop.removeImage(image).success(function (r) {
                        if(r.data.success) {
                            angular.forEach($scope.images, function(value, key) {
                                if(value.name == image) {
                                    $scope.images.splice(key, 1);
                                }
                            });
                        }
                    });
                }
            };

            $scope.uploaderLogo = new FileUploader({
                url: "/shops/uploadLogo.json",
                onBeforeUploadItem: function(item){
                    $scope.loadingLogo = true;
                    item.url = $scope.uploaderLogo.url;
                },
                onAfterAddingAll : function()  {
                    $scope.uploaderLogo.uploadAll();
                },
                onSuccessItem: function(item, response){
                    if(response.data.success) {
                        $scope.logo = response.data.url;
                    }
                    $scope.loadingLogo = false;
                }
            });
            $scope.uploaderCover = new FileUploader({
                url: "/shops/uploadCover.json",
                onBeforeUploadItem: function(item){
                    $scope.loadingCover = true;
                    item.url = $scope.uploaderCover.url;
                },
                onAfterAddingAll : function()  {
                    $scope.uploaderCover.uploadAll();
                },
                onSuccessItem: function(item, response){
                    if(response.data.success) {
                        $scope.cover = response.data.url;
                    }
                    $scope.loadingCover = false;
                }
            });

        })
        .controller('TaxInformationCtrl',function($scope,shop,$activityIndicator,toaster){
            $activityIndicator.startAnimating();
            $scope.shop = {};
            shop.loadTaxInformation().success(function(r){
                $scope.shop = r.shop;
                $scope.countries = r.countries;
                $scope.currencies = r.currencies;
                $activityIndicator.stopAnimating();
            });
            $scope.save = function(){
                $activityIndicator.startAnimating();
                shop.updateTaxInformation($scope.shop).success(function(r){
                    if(r.data.success) {
                        toaster.pop('success', "", r.data.message);
                    } else {
                        toaster.pop('error', "", r.data.message);
                    }
                    $activityIndicator.stopAnimating();
                })
            }
        })
        .controller('ShopNotifiesCtrl',function($scope,$activityIndicator,notifies,toaster,cart,$rootScope,$filter){
            $activityIndicator.startAnimating();
            $rootScope.notifies = {};
            $rootScope.open_notifies = [];
            $rootScope.closed_notifies = [];
            notifies.getShopNotify(true).success(function(r) {
                angular.forEach(r.shop_notifies, function(value, key) {
                    if(value.status == 'open') {
                        $rootScope.open_notifies.push(r.shop_notifies[key]);
                    } else if(value.status == 'closed') {
                        $rootScope.closed_notifies.push(r.shop_notifies[key]);
                    }
                });
                $activityIndicator.stopAnimating();
            });

            $scope.buyNow = function(product_id, negoziatyon_id) {
                notifies.buyNow(product_id, negoziatyon_id).success(function(r){
                    if(r.data.success && r.data.token) {
                        document.location = '/checkout/setOrder/'+r.data.token;
                    }
                });
            }

            $scope.available = function(notify, notify_id, negoziatyon_id, state) {
                toaster.clear();
                notifies.available(notify_id, negoziatyon_id, state).success(function(r){
                    //location.reload();
                    if(r.data.success) {
                        notify.response = true;
                        notify.actions = r.data.message;
                        toaster.pop('info', "", "Risposta inviata");
                        if(state == false) {
                            angular.forEach($rootScope.open_notifies, function(value, key) {
                                if(value.id == negoziatyon_id) {
                                    $rootScope.open_notifies.splice(key, 1);
                                    $rootScope.closed_notifies.push(notify);
                                }
                            });
                        }
                        $rootScope.closed_notifies = $filter('orderBy')($rootScope.closed_notifies, 'created', true);
                        $rootScope.open_notifies = $filter('orderBy')($rootScope.open_notifies, 'created', true);
                    }
                });
            }
        })
        .controller('TaxsCtrl',function($scope, $filter,shop,$activityIndicator,$modal,$rootScope){
            $activityIndicator.startAnimating();
            shop.loadTaxes().success(function(r){
                $scope.taxes = r.taxes;
                $activityIndicator.stopAnimating();
            });
            $scope.deleteTax = function(tax_id){
                shop.deleteTax(tax_id).success(function(r){
                    angular.forEach($scope.taxes, function(value, key) {
                        if(value.id== tax_id) {
                            $scope.taxes.splice(key, 1);
                            shop.checkStatus().success(function(r){
                                $rootScope.settingStatus = r.data;
                            });
                        }
                    });
                });
            }
            $scope.open = function (size) {
                $modal.open({
                    templateUrl: '/view/taxes/add.html',
                    controller: 'addTaxCtrl',
                    size: size,
                    resolve: {
                        taxes: function () {
                            return $scope.taxes;
                        }
                    }
                });
            };
            $scope.editModal = function (tax_id) {
                $modal.open({
                    templateUrl: '/view/taxes/edit.html',
                    controller: 'editTaxCtrl',
                    resolve: {
                        taxes: function () {
                            return $scope.taxes;
                        },
                        tax_id: function(){
                            return tax_id;
                        }
                    }
                });
            };
        })
        .controller('addTaxCtrl',function($scope, $modalInstance,shop,taxes,$rootScope) {
            $scope.tax = {
                name: "",
                country_tax: new Array(),
                tax: 0,
                product_tax: true,
                label: ''
            };
            $scope.taxes = taxes;
            $scope.ok = function () {
                shop.addTax($scope.tax).success(function (r) {
                    if (r.data) {
                        $scope.tax.id = r.data.tax_id;
                        $scope.taxes.push($scope.tax);
                        $modalInstance.close($scope.tax);
                        shop.checkStatus().success(function(r){
                            $rootScope.settingStatus = r.data;
                        });
                    }
                });
            };
            $scope.updateLabel = function () {
                $scope.tax.label = $scope.tax.name + " " + $scope.tax.tax + " %";
            }
            $scope.cancel = function () {
                $modalInstance.dismiss('cancel');
            };
        })
        .controller('editTaxCtrl',function($scope, $modalInstance,shop,taxes,tax_id){
            $scope.taxes = taxes;
            shop.loadTax(tax_id).success(function(r){
                if(r.data.success) {
                    $scope.tax = r.data.tax;
                }
            })
            $scope.ok = function () {
                shop.updateTax($scope.tax).success(function(r){
                    if(r.data) {
                        angular.forEach($scope.taxes, function(value, key) {
                            if(value.id == $scope.tax.id) {
                                $scope.taxes[key] = $scope.tax;
                            }
                        });
                        $modalInstance.close($scope.tax);
                    }
                });
            };
            $scope.updateLabel = function() {
                $scope.tax.label = $scope.tax.name + " " + $scope.tax.tax + " %";
            }
            $scope.cancel = function () {
                $modalInstance.dismiss('cancel');
            };
        })
        .controller('ProductsCtrl',function($scope, $filter,product,collections,$activityIndicator,toaster,$modal){
            var init;
            $scope.data                 = {};
            $scope.numPerPageOpt        = [5, 10, 20];
            $scope.numPerPage           = $scope.numPerPageOpt[1];
            $scope.currentPage          = 1;
            $scope.currentPageStores    = [];
            $scope.products             = [];
            $scope.collections          = [];
            $scope.totalProducts        = 0;
            $scope.slectedCollectionId  = false;
            $scope.row                  = 'name';
            $scope.orderType            = 'ASC';
            $scope.productSelected      = [];
            $scope.selectAll            = false;
            $scope.deselectAll          = true;
            $scope.checkSelected = function(){
                var keepGoing = false;
                angular.forEach($scope.products, function(value, key) {
                    if($scope.products[key].selected) {
                        keepGoing = true;
                    }
                });
                return keepGoing;
            }

            $scope.toggleList = function(){
                if($scope.deselectAll) {
                    $scope.deselectAll  = false;
                    $scope.selectAll    = true;
                    angular.forEach($scope.products, function(value, key) {
                        $scope.products[key].selected = true;
                    });
                } else {
                    $scope.deselectAll  = true;
                    $scope.selectAll    = false;
                    angular.forEach($scope.products, function(value, key) {
                        $scope.products[key].selected = false;
                    });
                }
            }

            $scope.addCollectionModal = function () {
                var modalInstance = $modal.open({
                    templateUrl: '/view/collections/modal_add.html',
                    controller: 'addCollectionModal',
                    resolve: {
                        collections_list: function () {
                            return $scope.collections;
                        }
                    }
                });
                modalInstance.result.then(function (collection_id) {
                    $scope.addToColletcion(collection_id)
                });
            };

            $scope.addToColletcion = function(collection_id){
                var products = $filter('filter')($scope.products, {"selected": true});
                $scope.productSelected = [];
                angular.forEach(products, function(value, key) {
                    $scope.productSelected.push(products[key].id);
                });
                product.addToCollection($scope.productSelected,collection_id).success(function(r){
                    if(r.data.success) {
                        var collection = $filter('filter')($scope.collections, {"id": collection_id});
                        var collection_name = "";
                        if(collection[0]) {
                            collection_name = collection[0].name
                            angular.forEach($scope.productSelected, function(value, key) {
                                var product = $filter('filter')($scope.products, {"id":  $scope.productSelected[key]});
                                if(product[0]) {
                                    product[0].collections = [];
                                    product[0].collections.push(collection_name);
                                }
                            });
                        }
                        toaster.pop('success', "", r.data.message +" '"+collection_name+"'");
                    } else {
                        toaster.pop('error', "", r.data.message);
                    }
                });
            }

            collections.list().success(function(r){
                $scope.collections = r.collections;
            });
            $scope.select = function(page) {
                $activityIndicator.startAnimating();
                $scope.deselectAll  = true;
                $scope.selectAll    = false;
                product.getList($scope.numPerPage,page,$scope.row,$scope.orderType).success(function(r){
                    $scope.products = r.products;
                    $scope.totalProducts = r.count;
                    $activityIndicator.stopAnimating();
                });
            };

            $scope.onFilterChange = function() {
                $scope.select(1);
                $scope.currentPage = 1;
                return $scope.row = '';
            };
            $scope.onNumPerPageChange = function() {
                $scope.select(1);
                return $scope.currentPage = 1;
            };
            $scope.onOrderChange = function() {
                $scope.select(1);
                return $scope.currentPage = 1;
            };
            $scope.order = function(rowName,orderType) {
                if ($scope.row === rowName && $scope.orderType === orderType) {
                    return;
                }
                $activityIndicator.startAnimating();
                $scope.row = rowName;
                $scope.orderType = orderType;
                return $scope.onOrderChange();
            };


            $scope.deleteProducts = function(product_url){
                var products = $filter('filter')($scope.products, {"selected": true});
                $scope.productSelected = [];
                angular.forEach(products, function(value, key) {
                    $scope.productSelected.push(products[key].id);
                });
                product.deleteAll($scope.productSelected).success(function(r){
                    if(r.data.success) {
                        toaster.pop('success', "", r.data.message);
                        $scope.select($scope.currentPage);
                    } else {
                        toaster.pop('error', "", r.data.message);
                    }
                });
            }

            $scope.deleteProduct = function(product_url){
                product.delete(product_url).success(function(r){
                    if(r.data.success) {
                        toaster.pop('success', "", r.data.message);
                        $scope.select($scope.currentPage);
                    } else {
                        toaster.pop('error', "", r.data.message);
                    }
                });
            }
            init = function() {
                return $scope.select($scope.currentPage);
            };
            return init();
        })
        .controller('OrdersCtrl',function($scope, $filter,orders,$activityIndicator){
            var init;
            $scope.numPerPageOpt        = [5, 10, 20];
            $scope.data                 = {};
            $scope.numPerPage           = $scope.numPerPageOpt[1];
            $scope.currentPage          = 1;
            $scope.currentPageStores    = [];
            $scope.orders             = [];
            $scope.totalOrders       = 0;
            $scope.row                  = 'created';
            $scope.orderType            = 'DESC';
            $scope.select = function(page) {
                $activityIndicator.startAnimating();
                orders.getList($scope.numPerPage,page,$scope.row,$scope.orderType).success(function(r){
                    $scope.orders = r.orders;
                    $scope.totalOrders = r.count;
                    $activityIndicator.stopAnimating();
                });
            };
            $scope.onFilterChange = function() {
                $scope.select(1);
                $scope.currentPage = 1;
                return $scope.row = '';
            };
            $scope.onNumPerPageChange = function() {
                $scope.select(1);
                return $scope.currentPage = 1;
            };
            $scope.onOrderChange = function() {
                $scope.select(1);
                return $scope.currentPage = 1;
            };
            $scope.order = function(rowName,orderType) {
                if ($scope.row === rowName && $scope.orderType === orderType) {
                    return;
                }
                $activityIndicator.startAnimating();
                $scope.row = rowName;
                $scope.orderType = orderType;
                return $scope.onOrderChange();
            };
            init = function() {
                return $scope.select($scope.currentPage);
            };
            return init();
        })

        .controller('OrderCtrl',function($scope,$routeParams,orders,$activityIndicator,toaster){
            $activityIndicator.startAnimating();
            $scope.order = {};
            $scope.order_status = [];
            orders.load_order($routeParams.order_id).success(function(r){
                $scope.order = r.order;
                $scope.order_status = r.order_status;
                $activityIndicator.stopAnimating();

            });
            $scope.changeStatus = function(){
                orders.changeStatus($routeParams.order_id,$scope.order.order.status).success(function(r){
                    if(r.data.success) {
                        toaster.pop('success', "", r.data.message);
                    } else {
                        toaster.pop('error', "", r.data.message);
                    }
                });
            }
        })
        .controller('appCtrl',function($scope, $routeParams, app_store, $activityIndicator){
            $activityIndicator.startAnimating();
            $scope.applicationData = {};
            $scope.category = {};
            $scope.company = {};
            $scope.category.limit = 5;
            $scope.category.ofset = 0;
            $scope.category.stepper = 5;
            $scope.company.stepper = 5;
            $scope.company.limit = 5;
            $scope.company.ofset = 0;

            $scope.slides = [
				{image: 'http://lorempixel.com/1800/400/technics/1/', description: 'Image 00'},
				{image: 'http://lorempixel.com/1800/400/technics/2/', description: 'Image 01'},
				{image: 'http://lorempixel.com/1800/400/technics/3/', description: 'Image 02'},
				{image: 'http://lorempixel.com/1800/400/technics/4/', description: 'Image 03'},
				{image: 'http://lorempixel.com/1800/400/technics/5/', description: 'Image 04'}
			];
			$scope.direction = 'left';
			$scope.currentIndex = 0;
			$scope.setCurrentSlideIndex = function (index) {
				$scope.direction = (index > $scope.currentIndex) ? 'left' : 'right';
				$scope.currentIndex = index;
			};
			$scope.isCurrentSlideIndex = function (index) {
				return $scope.currentIndex === index;
			};
			$scope.prevSlide = function () {
				$scope.direction = 'left';
				$scope.currentIndex = ($scope.currentIndex < $scope.slides.length - 1) ? ++$scope.currentIndex : 0;
			};
			$scope.nextSlide = function () {
				$scope.direction = 'right';$scope.currentIndex = ($scope.currentIndex > 0) ? --$scope.currentIndex : $scope.slides.length - 1;
			};

            // ritorna l'informazione dell'app, altre app dello sviluppatore, altre app simili
            app_store.getPageData($routeParams.app_url).success(function(r) {
                if(r.data.success) {
                    $scope.pageData = r.data;
                    $scope.installed = r.data.installed;
                    //$scope.loadingButton = false;
                    $scope.slides = r.data.appData.AppScreenshot;
                    $activityIndicator.stopAnimating();
                }
            });

            $scope.installApp = function(app_id) {
                app_store.installApp($routeParams.app_url, app_id).success(function() {
                    location.reload();
                });
            }

            $scope.removeApp = function(app_id) {
                app_store.removeApp($routeParams.app_url, app_id).success(function() {
                    location.reload();
                });
            }
            $scope.viewOther = function(cate, item) {
                if(cate.Application.length > item.limit) {
                    item.ofset += item.stepper;
                    item.limit += item.stepper;
                }
            }
            $scope.viewRetry = function(cate) {
                cate.ofset -= cate.stepper;
                cate.limit -= cate.stepper;
            }
            $scope.retry = function(item) {
                if(item.ofset == 0) {
                    return false;
                } else {
                    return true;
                }
            }

    })
        .controller('confirmCtrl',function($scope,$modalInstance){
            $scope.ok = function() {
                $modalInstance.close();
            };
            $scope.cancel = function() {
                $modalInstance.dismiss('cancel');
            };
    })
        .controller('ShipmentsCtrl',function($scope,$filter,shop,$activityIndicator,$modal,$rootScope){
            $activityIndicator.startAnimating();
            $scope.shipments = [];
            shop.loadShipments().success(function(r){
                $scope.shipments = r.shipments_list;
                $activityIndicator.stopAnimating();
            })
            $scope.open = function () {
                $modal.open({
                    templateUrl: '/view/shipments/add.html',
                    controller: 'addShippingCtrl',
                    resolve: {
                        shipments: function () {
                            return $scope.shipments;
                        }
                    }
                });
            };
            $scope.editModal = function (shipping_id) {
                $modal.open({
                    templateUrl: '/view/shipments/edit.html',
                    controller: 'editShippingCtrl',
                    resolve: {
                        shipments: function () {
                            return $scope.shipments;
                        },
                        shipping_id : function(){
                            return shipping_id;
                        }
                    }
                });
            };
            $scope.deleteShipping = function(shipping_id){
                shop.deleteShipping(shipping_id).success(function(r){
                    angular.forEach($scope.shipments, function(value, key) {
                        if(value.id== shipping_id) {
                            $scope.shipments.splice(key, 1);
                            shop.checkStatus().success(function(r){
                                $rootScope.settingStatus = r.data;
                            });
                        }
                    });
                });
            }
    })
        .controller('addShippingCtrl',function($scope,$filter, $modalInstance,shop,countries,shipments,$rootScope){
            $scope.shipping = {
                name: "",
                countries: [115],
                price: 0,
                start_day: 3,
                end_day: 10,
                shop_tax_id: ''
            };
            $scope.taxes = [];
            $scope.countries = [];
            shop.loadTaxes().success(function(r){
                $scope.taxes = r.taxes;
                if($scope.taxes.length) {
                    $scope.shipping.shop_tax_id = $scope.taxes[0].id;
                }
            });
            countries.get().success(function(r){
                $scope.countries = r.countries;
                $scope.shipping.countries.push(115);
            });
            $scope.shipments = shipments;
            $scope.ok = function () {
                shop.addShipping($scope.shipping).success(function(r){
                    if(r.data) {
                        var newShipping = {};
                        newShipping.id          = r.data.shipping_id;
                        newShipping.name        = $scope.shipping.name;
                        newShipping.start_day   = $scope.shipping.start_day;
                        newShipping.end_day     = $scope.shipping.end_day;
                        newShipping.end_day     = $scope.shipping.end_day;
                        newShipping.price       = r.data.price;
                        newShipping.countries   = [];
                        angular.forEach($scope.shipping.countries, function(value, key) {
                            for(key in $scope.countries) {
                                if(($scope.countries[key].id == value)) {
                                    newShipping.countries.push($scope.countries[key].name);
                                }
                            }
                        });
                        $scope.shipments.push(newShipping);
                        $modalInstance.close(newShipping);
                        shop.checkStatus().success(function(r){
                            $rootScope.settingStatus = r.data;
                        });
                    }
                });
            };
            $scope.cancel = function () {
                $modalInstance.dismiss('cancel');
            };
    })
        .controller('editShippingCtrl',function($scope,$filter, $modalInstance,shop,countries,shipments,shipping_id){
            $scope.shipping = {
                name: "",
                countries: new Array(),
                price: 0,
                start_day: 3,
                end_day: 10,
                shop_tax_id: ''
            };
            $scope.shipments = shipments;
            $scope.taxes = [];
            $scope.countries = [];
            shop.loadShipping(shipping_id).success(function(r){
                if(r.data.success) {
                    $scope.shipping = r.data.shipping;
                    $scope.countries = r.data.countries;
                    $scope.shipping.price = parseFloat($scope.shipping.price);
                    $scope.taxes = r.data.taxes;
                }
            })
            $scope.ok = function () {
                shop.updateShipping($scope.shipping).success(function(r){
                    if(r.data) {
                        var newShipping = {};
                        newShipping.id          = $scope.shipping.id;
                        newShipping.name        = $scope.shipping.name;
                        newShipping.start_day   = $scope.shipping.start_day;
                        newShipping.end_day     = $scope.shipping.end_day;
                        newShipping.end_day     = $scope.shipping.end_day;
                        newShipping.price       = r.data.price;
                        newShipping.countries   = [];
                        angular.forEach($scope.shipping.countries, function(value, key) {
                            for(key in $scope.countries) {
                                if(($scope.countries[key].id == value)) {
                                    newShipping.countries.push($scope.countries[key].name);
                                }
                            }
                        });
                        angular.forEach($scope.shipments, function(value, key) {
                            if(value.id == $scope.shipping.id) {
                                $scope.shipments[key] = newShipping;
                            }
                        });
                        $modalInstance.close(newShipping);
                    }
                });
            };
            $scope.cancel = function () {
                $modalInstance.dismiss('cancel');
            };
        })
        .controller('paymentsCtrl',function($scope,payments,toaster,$activityIndicator,$rootScope,shop){
            $activityIndicator.startAnimating();
            $scope.payments = {
                bonifico: {
                    id:    null,
                    enabled: false,
                    value: false,
                    price: 0
                },
                paypal: {
                    id:    null,
                    enabled: false,
                    value: false,
                    price: 0
                },
                contrassegno: {
                    id:    null,
                    enabled: false,
                    price: 0,
                    value: false
                },
                money: {
                    id:    null,
                    enabled: false,
                    price: 0,
                    value: false
                }
            }
            payments.get().success(function (r) {
                angular.forEach(r.payments, function(value, key) {
                    $scope.payments[key] = r.payments[key];
                    $scope.payments[key].price = parseFloat($scope.payments[key].price);
                });
                $activityIndicator.stopAnimating();
            });
            $scope.save = function() {
                $activityIndicator.startAnimating();
                payments.savePayments($scope.payments).success(function (r) {
                    if(r.data.success) {
                        angular.forEach(r.payments, function(value, key) {
                            $scope.payments[key] = r.payments[key];
                            $scope.payments[key].price = parseFloat($scope.payments[key].price);
                        });
                        toaster.pop('success', "", r.data.message);
                        $activityIndicator.stopAnimating();
                    } else {
                        toaster.pop('error', "", r.data.message);
                        $activityIndicator.stopAnimating();
                    }
                    shop.checkStatus().success(function(r){
                        $rootScope.settingStatus = r.data;
                    });
                });
            }
        })
        .controller('addCollectionCtrl', function ($scope,toaster,collections,FileUploader,$filter) {
            $scope.collection = {
                name        :   '',
                url         :   '',
                description :   ''
            }
            $scope.errorSave = false;
            $scope.successSave = false;
            $scope.onLoading            = false;
            $scope.progress             = 0;
            $scope.current_image        = '';
            $scope.count_image          = 0;
            $scope.success_images       = [];
            $scope.error_images         = [];
            $scope.colors               = [];

            collections.getColors().success(function(r) {
                if(r.data.success) {
                    $scope.colors = r.data.colors;
                    //$scope.collection.color = $scope.colors[0];
                    $scope.collection.color = $filter('filter')($scope.colors, {key: 'bg-steel'});  // grigio di default
                    $scope.collection.color = $scope.collection.color[0];
                }
            });

            $scope.saveCollection = function() {
                collections.add($scope.collection).success(function (r) {
                    if (r.data.success) {
                        if ($scope.uploader.queue.length) {
                            $scope.uploader.url = "/collections/loadImage/" + r.data.collection.id;
                            $scope.uploader.uploadAll();
                        } else {
                            $scope.successSave = true;
                        }
                    } else {
                        $scope.errorSave = true;
                    }
                });
            }

            $scope.setColor = function(color){
                $scope.collection.color = color;
            }

            $scope.uploader = new FileUploader({
                url: "/collections/loadImage",
                onAfterAddingFile:function(item){
                    var tmpItem = {};
                    for(var key in $scope.uploader.queue) {
                        tmpItem = $scope.uploader.queue[key]
                        if(item != tmpItem)
                            $scope.uploader.removeFromQueue(tmpItem);
                    }
                },
                onBeforeUploadItem: function(item){
                    item.url = $scope.uploader.url+".json";
                    $scope.onLoading = true;
                    $scope.current_image = item._file.name;
                },
                onCompleteAll: function() {
                    $scope.successSave  = true;
                    $scope.onLoading    = false;
                },
                onProgressAll: function(progress){
                    $scope.progress = progress;
                },
                onSuccessItem: function(item, response){
                    if(response.data.success) {
                        $scope.count_image++;
                        $scope.success_images.push(item._file.name);
                    }
                },
                onErrorItem: function(item, response, status, headers){
                    $scope.error_images.push(item._file.name);
                }
            });

        })
        .controller('collectionCtrl',function($scope,collections,$activityIndicator){
            $scope.numPerPageOpt        = [5, 10, 20];
            $scope.numPerPage           = $scope.numPerPageOpt[1];
            $scope.currentPage          = 1;
            $scope.currentPageStores    = [];
            $scope.collections          = [];
            $scope.totalCollections     = 0;
            $scope.row                  = 'name';
            $scope.orderType            = 'ASC';

            $activityIndicator.startAnimating();
            $scope.collections = [];
            $scope.deleteCollection = function(collection_id){
                collections.remove(collection_id).success(function(r){
                    if(r.data.success) {
                        angular.forEach($scope.collections, function(value, key) {
                            if(value.id== collection_id) {
                                $scope.collections.splice(key, 1);
                            }
                        });
                    }
                })
            }

            $scope.select = function(page) {
                $activityIndicator.startAnimating();
                $scope.deselectAll  = true;
                $scope.selectAll    = false;
                collections.get_list($scope.numPerPage,page,$scope.row,$scope.orderType).success(function(r){
                    $scope.collections = r.collections;
                    $scope.totalCollections = r.count;
                    $activityIndicator.stopAnimating();
                });
            };

            $scope.onFilterChange = function() {
                $scope.select(1);
                $scope.currentPage = 1;
                return $scope.row = '';
            };
            $scope.onNumPerPageChange = function() {
                $scope.select(1);
                return $scope.currentPage = 1;
            };
            $scope.onOrderChange = function() {
                $scope.select(1);
                return $scope.currentPage = 1;
            };
            $scope.order = function(rowName,orderType) {
                if ($scope.row === rowName && $scope.orderType === orderType) {
                    return;
                }
                $activityIndicator.startAnimating();
                $scope.row = rowName;
                $scope.orderType = orderType;
                return $scope.onOrderChange();
            };

            var init = function() {
                return $scope.select($scope.currentPage);
            };
            return init();


        })
        .controller('editCollectionCtrl',function($scope,collections,$routeParams,$activityIndicator,toaster,FileUploader,$filter){

            $scope.successSave          = false;
            $scope.errorSave            = false;
            $scope.onLoading            = false;

            $scope.progress             = 0;
            $scope.current_image        = '';
            $scope.count_image          = 0;
            $scope.colors               = [];

            $activityIndicator.startAnimating();
            $scope.collection = {};
            collections.load($routeParams.collection_id).success(function(r){
                $scope.collection = r.collection;
                $scope.image = r.images[0];
                $scope.colors = r.colors;
                $scope.collection.color = $filter('filter')($scope.colors, {key: $scope.collection.color});
                $scope.collection.color = $scope.collection.color[0];
                $activityIndicator.stopAnimating();
            });
            $scope.updateCollection = function() {
                collections.update($scope.collection).success(function (r) {
                    if(r.data.success) {
                        toaster.pop('success', "", r.data.message);
                    } else {
                        toaster.pop('error', "", r.data.message);
                    }
                })
            }
            $scope.setColor = function(color){
                $scope.collection.color = color;
            }

            $scope.uploader = new FileUploader({
                url: "/collections/loadImage",
                onBeforeUploadItem: function(item){
                    item.url = $scope.uploader.url;
                    $scope.current_image = item._file.name;
                    $scope.onLoading = true;
                },
                onAfterAddingAll : function(item)  {
                    $scope.progress = 0;
                    $scope.uploader.url = "/collections/loadImage/" + $scope.collection.id + ".json";
                    $scope.uploader.uploadAll();
                },
                onProgressAll: function(progress){
                    $scope.progress = progress;
                },
                onSuccessItem: function(item, response){
                    if(response.data.success) {
                        $scope.image = {
                            order: null,
                            product_id: null,
                            image: response.data.url,
                            name: response.data.name
                        };
                    }
                    $scope.onLoading = false;
                },
                onErrorItem: function(item, response, status, headers){
                    $scope.onLoading = false;
                }
            });
            $scope.sortableOptions = {
                stop: function(e, ui) {
                    for (var index in $scope.images) {
                        $scope.images[index].order = index;
                    }
                }
            };
        })
        .controller('addCollectionModal',function($scope,$modalInstance,collections,collections_list,$filter){
            $scope.collection = {
                id          : "",
                name        : "",
                description : "",
                color       : ""
            }
            $scope.colors = [];

            $scope.setColor = function(color){
                $scope.collection.color = color;
            }

            collections.getColors().success(function(r) {
                if(r.data.success) {
                    $scope.colors = r.data.colors;
                    //$scope.collection.color = $scope.colors[0];
                    $scope.collection.color = $filter('filter')($scope.colors, {key: 'bg-steel'});  // grigio di default
                    $scope.collection.color = $scope.collection.color[0];
                }
            });

            $scope.ok = function () {
                collections.add($scope.collection).success(function(r){
                    if(r.data.success) {
                        var tmpCollection = {
                            id      : r.data.collection.id,
                            name    : r.data.collection.name
                        }
                        collections_list.push(tmpCollection);
                        $modalInstance.close(r.data.collection.id);
                    }
                });
            };
            $scope.cancel = function () {
                $modalInstance.dismiss('cancel');
            };
        })
        .controller('TermsCtrl',function($scope,shop,toaster,$rootScope){
            shop.loadTerms().success(function(r){
                $scope.terms = r.terms;
            })
            $scope.saveTerms = function(){
                shop.saveTerms($scope.terms).success(function(r){
                    if(r.data.success) {
                        toaster.pop('success', "", r.data.message);
                    } else {
                        toaster.pop('error', "", r.data.message);
                    }
                    shop.checkStatus().success(function(r){
                        $rootScope.settingStatus = r.data;
                    });
                });
            }
        })
        .controller('ThemeMainCtrl',function($scope,ThemeSite,$activityIndicator){
            $activityIndicator.startAnimating();
            $scope.active_theme = 1;
            ThemeSite.get_theme_list().success(function(r){
                $activityIndicator.stopAnimating();
                $scope.themes = r.themes;
                $scope.active_theme = r.active;
            });
        })
        .controller('ThemeDetailsCtrl',function($scope,ThemeSite,$activityIndicator,$routeParams,toaster){
            $activityIndicator.startAnimating();
            $scope.direction = 'left';
            $scope.currentIndex = 0;
            $scope.slides = [];
            $scope.theme_active = 1;
            $scope.setCurrentSlideIndex = function (index) {
                $scope.direction = (index > $scope.currentIndex) ? 'left' : 'right';
                $scope.currentIndex = index;
            };
            $scope.isCurrentSlideIndex = function (index) {
                return $scope.currentIndex === index;
            };
            $scope.prevSlide = function () {
                $scope.direction = 'left';
                $scope.currentIndex = ($scope.currentIndex < $scope.slides.length - 1) ? ++$scope.currentIndex : 0;
            };
            $scope.nextSlide = function () {
                $scope.direction = 'right';$scope.currentIndex = ($scope.currentIndex > 0) ? --$scope.currentIndex : $scope.slides.length - 1;
            };
            $scope.enableTheme = function(theme_id){
                ThemeSite.enable_theme(theme_id).success(function(r){
                    if(r.data.success) {
                        $scope.theme_active = theme_id;
                        toaster.pop('success', "", r.data.message);
                    } else {
                        toaster.pop('error', "", r.data.message);
                    }
                })
            }
            ThemeSite.load_theme($routeParams.theme_uri).success(function(r){
                $activityIndicator.stopAnimating();
                $scope.theme = r.data.theme;
                $scope.slides =  r.data.theme.screenshots;
                $scope.theme_active = r.data.theme_active;
            });
        })
}).call(this);
(function() {
    'use strict';
    angular.module('shop.services', [])
    .factory("countries", function ($http) {
        return {
            get: function () {
                return $http.get('/countries.json');
            }
        }
    }).factory("currencies", function ($http) {
        return {
            get: function () {
                return $http.get('/currencies.json');
            }
        }
    }).factory('dashboardTabs',function($http) {
        // Deprecata
        return {
            get: function () {
                return $http.get('/user_profile/getDashboardTabs.json')
            }
        }
    }).factory('tabTemplate',function($http){
        // Deprecata
        return {
            get: function(template){
                return $http.get('/admin_asset/views/tabs/'+template+'.html');
            }
        }
    }).factory('dashboardData', function () {
        // Deprecata
        return {
            getTotal: function () {
                return 0;
            },
            getSell: function (tasks) {
                return 0;
            },
            getVisitor: function () {
                return 0;
            }
        };
    }).factory('product',function($http) {
            return {
                save: function (product, tags, categories) {
                    return $http.post('/products/add.json', {product: product, tags: tags, categories: categories});
                },
                update: function (product, tags, categories, images) {
                    return $http.post('/products/update.json', {
                        product: product,
                        tags: tags,
                        images: images,
                        categories: categories
                    });
                },
                delete: function (url) {
                    return $http.post('/products/delete.json', {product: url});
                },
                deleteAll: function (products) {
                    return $http.post('/products/deleteAll.json', {products: products});
                },
                getList: function (n, page, row, orderType) {
                    return $http.get('/products/get_list/' + n + '/' + page + '/' + row + '/' + orderType + '.json');
                },
                load_product: function (url) {
                    return $http.get('/products/loadProduct/' + url + '.json');
                },
                remove_image: function (product_url, image) {
                    return $http.get('/products/removeImage/' + product_url + '/' + image + '.json');
                },
                load_data: function () {
                    return $http.get('/products/loadProductData.json');
                },
                addToCollection: function(products,collection) {
                    return $http.post('/products/addToCollection.json',{products:products,collection:collection});
                }

            }
    }).factory('orders',function($http){
        return {
            getList: function (n, page, row, orderType) {
                return $http.get('/orders/get_list/' + n + '/' + page + '/' + row + '/' + orderType + '.json');
            },
            load_order: function(order_id){
                return $http.post('/orders/loadOrder.json',{order_id:order_id});
            },
            changeStatus: function(order_id,status_id){
                return $http.post('/orders/changeStatus.json',{order_id:order_id,order_status:status_id});
            }
        }
    }).factory('shop',function($http) {
        return {
            get: function () {
                return $http.get('/shops.json');
            },
            dashboardData: function() {
                return $http.get('/shops/dashboardData.json');
            },
            loadSettings: function() {
                return $http.get('/shops/loadSettings.json');
            },
            loadTaxInformation: function(){
                return $http.get('/shops/loadTaxInformation.json');
            },
            updateShop: function(shop){
                return $http.post('/shops/updateShop.json',{shop:shop});
            },
            updateTaxInformation: function(shop){
                return $http.post('/shops/updateTaxInformation.json',{shop:shop});
            },
            addTax: function(tax) {
                return $http.post('/taxes/add.json',{tax:tax});
            },
            loadTaxes: function(){
                return $http.post('/taxes.json');
            },
            loadTax: function(tax_id){
                return $http.post('/taxes/loadTax.json',{tax_id:tax_id});
            },
            updateTax : function(tax){
                return $http.post('/taxes/update.json',{tax:tax});
            },
            deleteTax: function(tax_id) {
                return $http.post('/taxes/delete.json',{tax_id:tax_id});
            },
            loadShipments: function() {
                return $http.post('/shipments.json');
            },
            loadShipping : function(shipping_id){
                return $http.post('/shipments/loadShipping.json',{shipping_id:shipping_id});
            },
            updateShipping : function(shipping){
                return $http.post('/shipments/update.json',{shipping:shipping});
            },
            addShipping : function(shipping){
                return $http.post('/shipments/add.json',{shipping:shipping});
            },
            deleteShipping: function(shipping_id) {
                return $http.post('/shipments/delete.json',{shipping_id:shipping_id});
            },
            removeImage: function(image) {
                return $http.get('/shops/removeImage/'+image+'.json');
            },
            loadTerms: function(){
                return $http.get('/shops/loadTerms.json');
            },
            saveTerms: function(terms){
                return $http.post('/shops/saveTerms.json',{terms:terms});
            },
            checkStatus: function(){
                return $http.get('/shops/checkShopStatus.json');
            },
            checkShop: function(){
                return $http.get('/shops/checkShop.json');
            },
            getShopData: function(){
                return $http.get('/shops/getShopData.json');
            },
            publicShop: function(){
                return $http.get('/shops/publicShop.json');
            }
        }
    }).factory('tags',function($http){
        return {
            list: function(){
                return $http.get('/tags.json');
            },
            find: function(query) {
                return $http.get('/tags/search/'+query+'.json');
            }
        }
    }).factory('payments',function($http){
            return {
                get: function(){
                    return $http.get('/payments.json');
                },
                savePayments: function(payments) {
                    return $http.post('/payments/save.json',{payments:payments});
                }
            }
    }).factory('categories',function($http){
        return {

            list: function(){
                return $http.get('/categories.json');
            },
            validateMacrocategory: function(name){
                return $http.post('/categories/validateMacrocategory.json', {name:name});
            },
            validateCategory: function(name){
                return $http.post('/categories/validateCategory.json', {name:name});
            },
            validateSubcategory: function(name){
                return $http.post('/categories/validateSubcategory.json', {name:name});
            }
        }
    }).factory('selectCategories',function(){

        return {
            default: {
                macrocategorySelected: {},
                categorySelected: {},
                subcategorySelected: {},
                macroAlredyExist: false,
                cateAlredyExist: false,
                subAlredyExist: false,
                newMacroOk: false,
                newCateOk: false,
                newSubOk: false,
                newTree: false,
                macrocategoryCompleted: false,
                categoryCompleted: false,
                subcategoryCompleted: false,
                newMacro: false,
                newCate: false,
                newSub: false,
                newMacrocategory: '',
                newCategory: '',
                newSubcategory: '',
                subcategories: [],
                categories: []
            },
            data: {},
            calcCategories: function(){
                var selectCategory = this;
                if(selectCategory.data.macrocategorySelected && (Object.keys(selectCategory.data.macrocategorySelected).length|| (selectCategory.data.newMacrocategory != '' && selectCategory.data.newMacroOk)) &&
                    (selectCategory.data.categorySelected && Object.keys(selectCategory.data.categorySelected).length || (selectCategory.data.newCategory != '' && selectCategory.data.newCateOk)) &&
                    (selectCategory.data.subcategorySelected && Object.keys(selectCategory.data.subcategorySelected).length || (selectCategory.data.newSubcategory != '' && selectCategory.data.newSubOk))) {

                    return {
                        macrocategory   : (Object.keys(selectCategory.data.macrocategorySelected).length) ? selectCategory.data.macrocategorySelected : {label : selectCategory.data.newMacrocategory},
                        category        : (Object.keys(selectCategory.data.categorySelected).length) ? selectCategory.data.categorySelected : {label : selectCategory.data.newCategory},
                        subcategory     : (Object.keys(selectCategory.data.subcategorySelected).length) ? selectCategory.data.subcategorySelected : {label : selectCategory.data.newSubcategory}
                    };
                } else {
                    return false;
                }
            },
            setCategories: function(data,categories) {
                var selectCategory = this;
                selectCategory.data.macrocategorySelected = data.Category.Macrocategory;
                selectCategory.data.macrocategoryCompleted = selectCategory.data.categoryCompleted =  true;
                selectCategory.data.categorySelected =  {
                    'id' : data.Category.id,
                    'label' : data.Category.label
                }
                selectCategory.data.subcategorySelected = data.Subcategory;
                angular.forEach(categories, function(value, key) {
                    if(value.Macrocategory.id == selectCategory.data.macrocategorySelected.id) {
                        selectCategory.data.categories = value.Category;
                        angular.forEach(value.Category, function(v,k) {
                            if(v.id == selectCategory.data.categorySelected.id) {
                                selectCategory.data.subcategories = v.Subcategory;
                            }
                        })
                    }
                });
            },
            init: function() {
                var selectCategory = this;
                selectCategory.data = {};
                angular.forEach(selectCategory.default, function(value, key) {
                    selectCategory.data[key] = value;
                });
                return selectCategory.data;
            }
        }
    }).factory("users",function($http){
        return {
            getUser: function (user) {
                return $http.get('/users/getUser/' + user + '.json');
            },
            loadUser: function(){
                return $http.get('/users/loadUser.json');
            },
            updateAccount: function(user) {
                return $http.post('/users/updateAccount.json',{user:user});
            }
        }
    })
    .factory("collections",function($http){
        return {
            add: function (collection) {
                return $http.post('/collections/add.json', {collection:collection});
            },
            list: function(){
                return $http.get('/collections/collection_list.json');
            },
            get_list: function(n, page, row, orderType){
                return $http.get('/collections/get_list/' + n + '/' + page + '/' + row + '/' + orderType + '.json');
            },
            remove: function(collection_id){
                return $http.post('/collections/delete.json', {collection_id:collection_id});
            },
            load: function(collection_id){
                return $http.get('/collections/get/' + collection_id + '.json');
            },
            update : function(collection){
                return $http.post('/collections/update.json',{collection:collection});
            },
            getColors : function(){
                return $http.get('/collections/getColors.json');
            }
        }
    })
    .factory("cart",function($http){
        return {
            addToCart: function (product_id, quantity) {
                return $http.post('/carts/addProductToCart.json', {product_id: product_id, quantity: quantity});
            }
        }
    })
    .factory("app_store",function($http){
        return {
            getApplicationsGroupByCategories: function () {
                return $http.get('/appStore/getApplicationsGroupByCategories.json');
            },
            getPageData: function (request_uri) {
                return $http.post('/appStore/getPageData.json', {request_uri : request_uri});
            },
            installApp: function (request_uri, app_id) {
                return $http.post('/appStore/installApp.json', {request_uri : request_uri, app_id : app_id});
            },
            removeApp: function (request_uri, app_id) {
                return $http.post('/appStore/removeApp.json', {request_uri : request_uri, app_id : app_id});
            }
        }
    })
    .factory('errorHttpInterceptor', ['$q', function ($q) {
        return {
            response: function(response){
                if (response.status === 401) {
                    //console.log("Response 401");
                    document.location = rejection.headers('X-extra-redirect');
                }
                return response || $q.when(response);
            },
            responseError: function(rejection) {
                if (rejection.status === 401) {
                    //console.log("Response Error 401",rejection);
                    document.location = rejection.headers('X-extra-redirect');
                }
                return $q.reject(rejection);
            }
        };
    }])
        .factory('ThemeSite',function($http){
            return {
                get_theme_list: function(){
                    return $http.get('/themes/get_list.json');
                },
                load_theme: function(theme_uri){
                    return $http.post('/themes/load_theme.json',{theme_uri:theme_uri});
                },
                enable_theme: function(theme_id){
                    return $http.post('/themes/enable_theme.json',{theme_id:theme_id});
                }
            }
        })
}).call(this);

(function() {
    'use strict';

    angular.module('site', ['ngRoute', 'ngAnimate','site.controllers','site.services','ui.bootstrap','shared.directives','ngActivityIndicator','top_bar.services','top_bar.ctrls', 'shared.services', 'toaster']).config([
        '$routeProvider', function($routeProvider) {
            return $routeProvider.when('/', {
                redirectTo: '/home'
            }).when('/home', {
                templateUrl: '/view/site/home.html'
            }).when('/product/:url', {
                templateUrl: '/view/site/product.html'
            }).when('/category/:category_id', {
                templateUrl: '/view/site/category.html'
            }).when('/collection/:collection_name/:category_id', {
                templateUrl: '/view/site/collection.html'
            }).when('/search/:q', {
                templateUrl: '/view/site/search.html'
            }).when('/paymentChoice', {
                templateUrl: '/view/carts/payment_choice.html'
            }).when('/thanks', {
                templateUrl: '/view/carts/thanks.html'
            }).when('/all', {
                templateUrl: '/view/site/all.html'
            }).when('/paymentReview', {
                templateUrl: '/view/carts/payment_review.html'
            }).otherwise({
                redirectTo: "/404"
            })
        }
    ]);
}).call(this);
(function() {
    'use strict';
    angular.module('site.controllers', [])
        .controller('HomeCtrl', function($scope,product,$activityIndicator){
            $activityIndicator.startAnimating();
            $(".smoothscroll").mCustomScrollbar({
                advanced: {
                    updateOnContentResize: true
                },
                scrollButtons: {
                    enable: false
                },
                mouseWheelPixels: "100",
                theme: "dark-2"
            });
            $scope.products = [];
            $scope.shop = {};
            $scope.searchProduct = function(){
                if($scope.product_q) {
                    window.location.href = "#/search/" + $scope.product_q + "/";
                }
            }
            product.list($scope.limit,$scope.page).success(function(r){
                $scope.products = r.products;
                $scope.shop = r.shop;
                $activityIndicator.stopAnimating();
            });
            $scope.selected = void 0;
            $scope.states = [];
		})
        .controller('AllCtrl', function($scope,product,$activityIndicator){
            $activityIndicator.startAnimating();
            $(".smoothscroll").mCustomScrollbar({
                advanced: {
                    updateOnContentResize: true
                },
                scrollButtons: {
                    enable: false
                },
                mouseWheelPixels: "100",
                theme: "dark-2"
            });
            $scope.products = [];
            $scope.shop = {};
            $scope.searchProduct = function(){
                if($scope.product_q) {
                    window.location.href = "#/search/" + $scope.product_q + "/";
                }
            }
            product.all($scope.limit,$scope.page).success(function(r){
                $scope.products = r.products;
                $scope.shop = r.shop;
                $activityIndicator.stopAnimating();
            });
            $scope.selected = void 0;
            $scope.states = [];
        })
        .controller('CategoryCtrl', function($scope,$routeParams,product,$activityIndicator){
            $activityIndicator.startAnimating();
            $(".smoothscroll").mCustomScrollbar({
                advanced: {
                    updateOnContentResize: true
                },
                scrollButtons: {
                    enable: false
                },
                mouseWheelPixels: "100",
                theme: "dark-2"
            });
            $scope.products = [];
            $scope.category = "";
            $scope.searchProduct = function(){
                if($scope.product_q) {
                    window.location.href = "#/search/" + $scope.product_q + "/";
                }
            }
            product.listCategory($routeParams.category_id).success(function(r){
                $scope.products = r.products;
                $scope.category = r.category;
                $scope.shop = r.shop;
                $activityIndicator.stopAnimating();
            });
            $scope.selected = void 0;
            $scope.states = [];
        })
        .controller('CollectionCtrl', function($scope,$routeParams,product,$activityIndicator){
            $activityIndicator.startAnimating();
            $(".smoothscroll").mCustomScrollbar({
                advanced: {
                    updateOnContentResize: true
                },
                scrollButtons: {
                    enable: false
                },
                mouseWheelPixels: "100",
                theme: "dark-2"
            });
            $scope.products = [];
            $scope.category = "";
            product.listCategory($routeParams.category_id).success(function(r){
                $scope.products = r.products;
                $scope.shop = r.shop;
                $activityIndicator.stopAnimating();
            });
            $scope.selected = void 0;
            $scope.states = [];
        })
        .controller('SearchCtrl', function($scope,$routeParams,product,$activityIndicator){
            $(".smoothscroll").mCustomScrollbar({
                advanced: {
                    updateOnContentResize: true
                },
                scrollButtons: {
                    enable: false
                },
                mouseWheelPixels: "100",
                theme: "dark-2"
            });
            $scope.products = [];
            $scope.category = "";
            $scope.product_q = $routeParams.q;
            $scope.searchProduct = function(){
                if($scope.product_q) {
                    window.location.href = "#/search/" + $scope.product_q + "/";
                }
            }
            product.listSearch($routeParams.q).success(function(r){
                $scope.products = r.products;
                $scope.category = r.category;
            });
            $scope.selected = void 0;
            $scope.states = [];
        })
		.controller('ProdCtrl', function($scope,$routeParams,product,toaster, $activityIndicator){
			$("select").minimalect();
            $scope.product = {};
            $scope.images = {};
            $scope.tags = [];
            //$scope.categories = [];
            $scope.ui = {};
            $scope.collection = {};
            $scope.shop = {};
            $scope.changeBuyButton = false;
            $activityIndicator.startAnimating();
            product.product($routeParams.url).success(function(r){
                $scope.product  = r.product;
                $scope.images   = r.images;
                $scope.tags     = r.tags;
                $scope.collection = r.collections;
                //$scope.categories = r.categories;
                $scope.ui = r.ui;
                $scope.changeBuyButton = $scope.ui.existNegoziation;
                $scope.shop = r.shop;
                $scope.ui.loaded = true;
                $activityIndicator.stopAnimating();
            });

			  $scope.oneAtATime = true;
			  $scope.groups = [
				{
				  title: "Dynamic Group Header - 1",
				  content: "Dynamic Group Body - 1"
				}, {
				  title: "Dynamic Group Header - 2",
				  content: "Dynamic Group Body - 2"
				}, {
				  title: "Dynamic Group Header - 3",
				  content: "Dynamic Group Body - 3"
				}
			  ];
			  $scope.items = ["Item 1", "Item 2", "Item 3"];
			  $scope.status = {
				isFirstOpen: true,
				isFirstOpen1: true,
				isFirstOpen2: true,
				isFirstOpen3: true,
				isFirstOpen4: true,
				isFirstOpen5: true,
				isFirstOpen6: true
			  };
			  $scope.addItem = function() {
				var newItemNo;
				newItemNo = $scope.items.length + 1;
				$scope.items.push("Item " + newItemNo);
			  };

            $scope.handleBuy = function(product_id) {
                $scope.ui.loaded = false;
                product.handleBuy(product_id, $("select").val()).success(function(r){
                    if(r.data.success) {
                        if(r.data.buyNow) {
                            product.addToCart(product_id,$("select").val()).success(function(e){
                                return document.location = '/site/#paymentChoice';
                            })
                        } else {
                            toaster.pop('success', "", r.data.message);
                            $scope.ui.buyNow = false;
                            $scope.changeBuyButton = true;
                        }
                    } else {
                        toaster.pop('error', "", r.data.message);
                    }
                    $scope.ui.loaded = true;
                });
            }
			})
        .controller('CartCtrl', function($scope,product,$activityIndicator){
            $activityIndicator.startAnimating();
            $scope.cart = {
                total_price     : 0,
                tax_price       : 0,
                product_price   : 0,
                product         : {},
                quantity        : 0,
                user            : {
                    first_name  : "",
                    last_name   : "",
                    address     : "",
                    cap         : "",
                    city        : "",
                    province    : "",
                    country_id  : ""
                },
                shipping        : "",
                payment         : ""
            }
            $scope.shimprents   = [];
            $scope.payments     = [];
            product.loadCart().success(function(r){
                $scope.shimprents           = r.data.shipments;
                $scope.cart.total_price     = r.data.cart.total_price;
                $scope.cart.tax_price       = r.data.cart.tax_price;
                $scope.cart.product_price   = r.data.cart.product_price;
                $scope.cart.quantity        = r.data.cart.quantity;
                $scope.cart.product         = r.data.cart.product;
                $scope.cart.user            = r.data.user;
                $scope.payments             = r.data.payments;
                $scope.cart.shipping        = r.data.shipments[0].id;
                $scope.cart.payment         = r.data.payments[0].id;
                if(r.cart_data && r.cart_data.user) {
                    $scope.cart.user = r.cart_data.user;
                }
                $activityIndicator.stopAnimating();
            });

            $scope.saveCart = function(){
                angular.forEach($scope.payments, function(value, key) {
                    if(value.id == $scope.cart.payment) {
                        $scope.cart.payment = $scope.payments[key];
                    }
                });

                angular.forEach($scope.shimprents, function(value, key) {
                    if(value.id == $scope.cart.shipping) {
                        $scope.cart.shipping = $scope.shimprents[key];
                    }
                });
                product.setData($scope.cart).success(function(r){
                    if(r.data.success) {
                        return document.location = '#/paymentReview';
                    }
                });
            }
        }).controller('ReviewDataCtrl',function($scope,product,$activityIndicator){
            $activityIndicator.startAnimating();
            $scope.cart_data = {};
            product.loadReviewData().success(function(r){
                $scope.cart_data = r.data.data;
                $activityIndicator.stopAnimating();
            });
            $scope.createOrder = function(){
                product.saveOrder().success(function(r){
                    if(r.data.success) {
                        return document.location = '#/thanks';
                    }
                });
            }
        })
}).call(this);

(function() {
    'use strict';
    angular.module('site.services', []).factory('product',function($http){
        return {
            list: function() {
                return $http.get('/site/loadProducts.json');
            },
            all: function() {
                return $http.get('/site/loadAllProducts.json');
            },
            listCategory: function(collection_id){
                return $http.get('/site/loadProducts/'+collection_id+'.json');
            },
            listSearch: function(q){
                return $http.get('/site/searchProduct/'+q+'.json');
            },
            product: function(url){
                return $http.get('/site/loadProduct/'+url+'.json');
            },
            handleBuy: function(product_id, require_quantity) {
                return $http.post('/site/handleBuy.json', {product_id : product_id, require_quantity : require_quantity});
            },
            addToCart: function(product_id,quantity){
                return $http.post('/carts/addProductToCart.json', {product_id : product_id,quantity:quantity});
            },
            loadCart: function(){
                return $http.get('/carts/loadProduct.json');
            },
            setData: function(cart){
                return $http.post('/carts/setData.json', {cart: cart});
            },
            loadReviewData: function() {
                return $http.post('/carts/loadReviewData.json');
            },
            saveOrder: function(){
                return $http.post('/carts/saveOrder.json');
            }
        }
    })
}).call(this);

(function() {
    'use strict';
    angular.module('venture', ['ngRoute', 'ngAnimate', 'ui.bootstrap','venture.ctrls','venture.services','shared.directives','shared.services','ngActivityIndicator','angularFileUpload','top_bar.services','top_bar.ctrls','toaster']).config([
        '$routeProvider', function($routeProvider) {
             return $routeProvider.when('/', {
             redirectTo: '/'
             }).when('/', {
                templateUrl: '/view/ventures/list.html'
             }).when('/venture/:order_uid', {
                 templateUrl: '/view/ventures/details.html'
             })
        }
    ]);
}).call(this);

(function () {
    'use strict';
    angular.module('venture.ctrls', [])
        .controller('VentureCtrl', function ($scope) {

        })
}).call(this);
(function() {
    'use strict';
    angular.module('venture.services', [])
        .factory("ventures",function($http){
            return {
                loadVentures: function(){
                    return $http.get('/account/loadUser.json');
                }
            }
        })
}).call(this);
