<div class="lapagina" data-ng-controller="categories">
    <nav class="navbar navbar-default navbar-static-top" role="navigation">
        <div class="navbar-header">
            <ul class="nav navbar-nav">
                <li>
                    <ol class="breadcrumb">
                        <li class="active"><?php echo __("Lista delle categorie"); ?></li>
                    </ol>
                </li>
            </ul>
            <ul class="nav navbar-nav navbar-right">
                <div class="navbar-form">
                    <a href="#/add_categories" class="btn btn-success" type="submit"><i class="fa fa-plus"></i> <?php echo __("Aggiungi categoria"); ?></a>
                </div>
            </ul>
        </div>
    </nav>
    <div class="page page-table">
        <section class="panel panel-default table-dynamic">
            <div class="panel-heading"><strong><?php echo __("Categorie"); ?></strong></div>
            <div class="table-responsive">
                <table class="table table-bordered table-striped table-responsive">
                    <thead>
                    <tr>
                        <th><div class="th">
                                <?php echo __("Nome"); ?>
                            </div></th>
                        <th><div class="th">
                                <?php echo __("Default"); ?>
                            </div></th>
                        <th><div class="th">
                                <?php echo __("Macrocategoria"); ?>
                            </div></th>
                    </tr>
                    </thead>
                    <tbody>
                    <tr data-ng-repeat="category in categories">
                        <td>{{category.label}}</td>
                        <td>{{category.default}}</td>
                        <td>{{category.shop_id}}</td>
                    </tr>
                    </tbody>
                </table>
            </div>
        </section>
    </div>
</div>
