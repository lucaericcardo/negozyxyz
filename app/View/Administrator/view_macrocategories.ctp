<div class="lapagina" data-ng-controller="macrocategories">
    <nav class="navbar navbar-default navbar-static-top" role="navigation">
        <div class="navbar-header">
            <ul class="nav navbar-nav">
                <li>
                    <ol class="breadcrumb">
                        <li class="active"><?php echo __("Lista delle macrocategorie"); ?></li>
                    </ol>
                </li>
            </ul>
            <ul class="nav navbar-nav navbar-right">
                <div class="navbar-form">
                    <a href="#/add_macrocategory" class="btn btn-success" type="submit"><i class="fa fa-plus"></i> <?php echo __("Aggiungi macrocategoria"); ?></a>
                </div>
            </ul>
        </div>
    </nav>
    <div class="page page-table">
        <section class="panel panel-default table-dynamic">
            <div class="panel-heading"><strong><?php echo __("Macrocategorie"); ?></strong></div>
            <div class="table-responsive">
                <table class="table table-bordered table-striped table-responsive">
                    <thead>
                    <tr>
                        <th><div class="th"><?php echo __("Nome"); ?></div></th>
                        <th><div class="th"><?php echo __("Default"); ?></div></th>
                    </tr>
                    </thead>
                    <tbody>
                    <tr data-ng-repeat="macrocategory in macrocategories">
                        <td>{{macrocategory.label}}</td>
                        <td>{{macrocategory.default}}</td>
                    </tr>
                    </tbody>
                </table>
            </div>
        </section>
    </div>
</div>