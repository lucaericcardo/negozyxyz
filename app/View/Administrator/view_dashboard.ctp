<div class="lapagina">
    <nav class="navbar navbar-default navbar-static-top" role="navigation">
        <div class="navbar-header">
            <ol class="breadcrumb nav navbar-nav">
                <li class="active"><?php echo __("Dashboard"); ?></li>
            </ol>
        </div>
    </nav>
    <div class="page page-dashboard" data-ng-controller="dashboard">
        <div class="row">
            <div class="col-xs-12">

                <div class="row">
                    <div class="col-md-6 col-xs-12">
                        <div class="panel panel-default">
                            <div class="panel-heading">Sorgenti ADV</div>
                            <pie-chart dataChart="pieData"></pie-chart>
                        </div>

                    </div>
                    <div class="col-lg-6 col-xsm-6">
                        <div class="panel mini-box">
                            <span class="box-icon bg-info">
                                <i class="fa fa-users"></i>
                            </span>
                            <div class="box-info">
                                <p class="size-h2">{{count_users}}</p>
                                    <p class="text-muted"><span data-i18n="New users">Utenti registrati</span></p>
                            </div>
                        </div>
                    </div>
        </div>
    </div>
</div>
