<div class="lapagina">
    <nav class="navbar navbar-default navbar-static-top" role="navigation">
        <div class="navbar-header">
            <ul class="nav navbar-nav">
                <li>
                    <ol class="breadcrumb">
                        <li><a href="#/macrocategories/"><?php echo __("Lista delle macrocategorie"); ?></a></li>
                        <li class="active"><a><?php echo __("Aggiungi macrocategoria"); ?></a></li>
                    </ol>
                </li>
            </ul>
        </div>
    </nav>
    <div class="page page-table" data-ng-controller="add_macrocategories">
        <section class="panel panel-default table-dynamic">
            <div class="panel-heading"><strong><span class="glyphicon glyphicon-th"></span></strong></div>
            <div class="panel-body">
            <div ng-show="successSave" class="callout callout-success">
                <p><?php echo __("Macrocategorie aggiunte con successo"); ?></p>
                <a href="#/products"><?php echo __("Torna alla lista delle macrocategorie"); ?></a>
            </div>
            <div ng-show="errorSave" class="callout callout-danger">
                <?php echo __("Si è verificato un problema durante il salvataggio."); ?>
            </div>
            <form ng-if="!successSave && !errorSave" name="product_form" class="form-validation">
                <div class="row">
                    <div class="col-md-2">
                        <h4><?php echo __("Lista delle macrocategorie"); ?></h4>
                    </div>
                    <div class="col-md-10">
                        <div class="form-group">
                            <label for=""><?php echo __("Nome"); ?></label>
                            <textarea class="form-control" required data-ng-model="macrocategories.labels"></textarea>
                        </div>

                    </div>
                </div>
                <div class="row">
                    <div class="col-md-10 col-md-offset-2">
                        <button data-ng-click="saveMacorcategories()" class="btn btn-success btn-lg" type="button"><?php echo __("Salva"); ?></button>
                    </div>
                </div>
            </form>
        </section>
    </div>
</div>