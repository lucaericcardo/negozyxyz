<div class="lapagina">
    <nav class="navbar navbar-default navbar-static-top" role="navigation">
        <div class="navbar-header">
            <ul class="nav navbar-nav">
                <li>
                    <ol class="breadcrumb">
                        <li class="active"><?php echo __("Utenti"); ?></li>
                    </ol>
                </li>
            </ul>
        </div>
    </nav>

    <div class="page page-table" data-ng-controller="usersCtrl">
        <div class="row">
            <div class="col-lg-10 col-lg-offset-1 col-md-12">
                <section class="panel panel-default table-dynamic">
                    <div class="table-responsive">
                        <table class="table table-bordered table-striped table-responsive">
                            <thead>
                            <tr>
                                <th>
                                    <div class="th">
                                        <?php echo __("Immagine"); ?>
                                    </div>
                                </th>
                                <th>
                                    <div class="th">
                                        <?php echo __("Nome completo"); ?>
                                    </div>
                                </th>
                                <th>
                                    <div class="th">
                                        <?php echo __("Data di registrazione"); ?>
                                        <span class="glyphicon glyphicon-chevron-up" data-ng-click=" order('created','ASC') " data-ng-class="{active: row == 'created' && orderType == 'ASC'}"></span>
                                        <span class="glyphicon glyphicon-chevron-down" data-ng-click=" order('created','DESC') " data-ng-class="{active: row == 'created' && orderType == 'DESC'}"></span>
                                    </div>
                                </th>
                                <th>
                                    <div class="th">
                                        <?php echo __("Email"); ?>
                                        <span class="glyphicon glyphicon-chevron-up" data-ng-click=" order('email','ASC') " data-ng-class="{active: row == 'email' && orderType == 'ASC'}"></span>
                                        <span class="glyphicon glyphicon-chevron-down" data-ng-click=" order('email','DESC') " data-ng-class="{active: row == 'email' && orderType == 'DESC'}"></span>
                                    </div>
                                </th>
                                <th>
                                    <div class="th">
                                        <?php echo __("Abilitato"); ?>
                                        <span class="glyphicon glyphicon-chevron-up" data-ng-click=" order('confirmed','ASC')" data-ng-class="{active: row == 'confirmed' && orderType == 'ASC'}"></span>
                                        <span class="glyphicon glyphicon-chevron-down" data-ng-click=" order('confirmed','DESC') " data-ng-class="{active: row == 'confirmed' && orderType == 'DESC'}"></span>
                                    </div>
                                </th>
                                <th>
                                    <div class="th">
                                        <?php echo __("Azioni"); ?>
                                    </div>
                                </th>
                            </tr>
                            </thead>
                            <tbody>
                            <tr data-ng-repeat="user in users">
                                <td>
                                    <img ng-if="user.image" height="100" ng-src="{{user.image}}" />
                                    <img ng-if="!user.image" height="100" ng-src="http://bergen.katolsk.no/wp-content/plugins/special-recent-posts/images/no-thumb.png" />
                                </td>
                                <td>{{user.first_name}} {{user.last_name}}</td>
                                <td>{{user.created}}</td>
                                <td>{{user.email}}</td>
                                <td>{{user.confirmed}}</td>
                                <td class="text-right">
                                    <button data-ng-if="!user.confirmed" data-ng-click="enable(user.id)" class="btn btn-primary btn-sm"><?php echo __("Abilita"); ?></button>
                                </td>
                            </tr>
                            </tbody>
                        </table>
                    </div>
                    <footer class="table-footer">
                        <div class="row">
                            <div class="col-md-6 page-num-info">
                                <span>
                                    <?php echo __("Elementi per pagina"); ?>
                                    <select data-ng-model="numPerPage" data-ng-options="num for num in numPerPageOpt" data-ng-change="onNumPerPageChange()">
                                    </select>
                                </span>
                            </div>
                            <div class="col-md-6 text-right pagination-container">
                                <pagination class="pagination-sm" ng-model="currentPage" total-items="totalUsers" max-size="4" ng-change="select(currentPage)" items-per-page="numPerPage" rotate="false" boundary-links="true"></pagination>
                            </div>
                        </div>
                    </footer>
                </section>
            </div>
        </div>
    </div>
</div>