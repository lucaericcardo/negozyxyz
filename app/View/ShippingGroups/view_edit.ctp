<div class="modal-header">
    <h3 class="modal-title"><?php echo __("Modifica gruppo di spedizione"); ?></h3>
</div>
<div class="modal-body">
    <form name="editShippingGroup" class="form-horizontal form-validation">
        <div class="form-group">
            <label for="" class="col-sm-4"><?php echo __("Nome"); ?></label>
            <div class="col-sm-8">
                <input type="text" class="form-control ng-pristine ng-valid" required  data-ng-model="shipping_group.name">
            </div>
        </div>
    </form>
</div>
<div class="modal-footer">
    <button class="btn btn-primary"  ng-disabled="editShippingGroup.$invalid" ng-click="updateShippingGroup()"><?php echo __("Salva"); ?></button>
    <button class="btn btn-warning" ng-click="cancel()"><?php echo __("Annulla"); ?></button>
</div>