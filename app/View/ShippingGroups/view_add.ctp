<div class="modal-header">
    <h3 class="modal-title"><?php echo __("Aggiungi gruppo di spedizione"); ?></h3>
</div>
<div class="modal-body">
    <form name="addShipping" class="form-horizontal form-validation">
        <div class="form-group">
            <label for="" class="col-sm-4"><?php echo __("Nome"); ?></label>
            <div class="col-sm-8">
                <input type="text" class="form-control ng-pristine ng-valid" required  data-ng-model="shipping.name">
            </div>
        </div>
        <div class="form-group">
            <label for="" class="col-sm-4"><?php echo __("Prezzo"); ?></label>
            <div class="col-sm-8">
                <input data-min="0" type="number" required  class="form-control ng-pristine ng-valid" data-ng-model="shipping.price">
            </div>
        </div>
        <div class="form-group">
            <label for="" class="col-sm-4"><?php echo __("Prezzo prodotto aggiuntivo"); ?></label>
            <div class="col-sm-8">
                <input data-min="0" type="number" required  class="form-control ng-pristine ng-valid" data-ng-model="shipping.additional_price">
            </div>
        </div>
        <div class="form-group">
            <label for="" class="col-sm-4"><?php echo __("Imposta"); ?></label>
            <div class="col-sm-8">
                <select class="form-control" data-ng-options="tax.id as tax.label for tax in taxes" data-ng-model="shipping.shop_tax_id"></select>
            </div>
        </div>
        <div class="form-group">
            <label for="" class="col-sm-4"><?php echo __("Tempo di consegna"); ?> <small>(<?php echo __("giorni") ?>)</small> </label>
            <div class="col-sm-4">
                <input type="text" class="form-control ng-pristine ng-valid" required  data-ng-model="shipping.start_day" >
            </div>
            <div class="col-sm-4">
                <input type="text" class="form-control ng-pristine ng-valid" required  data-ng-model="shipping.end_day">
            </div>
        </div>
        <div class="form-group">
            <div class="col-sm-12">
                <label class="ui-radio"><input data-ng-model="shipping.country_type" value="1" type="radio"><span><?php echo __("Seleziona paesi"); ?></span></label>
                <label class="ui-radio"><input data-ng-model="shipping.country_type" value="2" type="radio"><span><?php echo __("Unione Europea"); ?></span></label>
                <label class="ui-radio"><input data-ng-model="shipping.country_type" value="3" type="radio"><span><?php echo __("Tutti i paesi"); ?></span></label>
                <oi-multiselect
                    ng-options="country.id as country.name for country in countries | limitTo: 10 "
                    ng-model="shipping.countries"
                    ng-disabled="shipping.country_type != 1"
                    multiple
                    placeholder="Select"
                    ></oi-multiselect>
            </div>
        </div>

    </form>
</div>
<div class="modal-footer">
    <button class="btn btn-primary"  ng-disabled="addShipping.$invalid" ng-click="ok()"><?php echo __("Salva"); ?></button>
    <button class="btn btn-warning" ng-click="cancel()"><?php echo __("Annulla"); ?></button>
</div>