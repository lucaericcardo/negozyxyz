<section id="signin_alt" class="authenty signin-alt">
    <div class="section-content">
      <div class="wrap">
          <div class="container">
                <div class="row" data-animation="fadeInUp">
                	<div class="col-sm-6 col-sm-offset-3 text-center">
                    	<img src="//cdn.negozy.com/negozy/Logos/LogoBig.png" alt="" class="img-responsive">
                    </div>
                </div>
                <hr class="clearfix" />
                <div class="row" data-animation="fadeInUp">
                	<div class="col-sm-6 col-sm-offset-3">
                    	<div class="row">
                          <div class="col-sm-6">
                            <div class="sns-signin">
                                <a href="/users/facebook_signup" class="facebook"><i class="fa fa-facebook"></i><span><?php echo __("Entra con Facebook"); ?></span></a>
                            </div>
                          </div>
                          <div class="col-sm-6">
                            <div class="sns-signin">
                                <a href="/users/google_signup" class="google-plus"><i class="fa fa-google-plus"></i><span><?php echo __("Entra con Google+"); ?></span></a>
                            </div>
                          </div>
                  		</div>
                    </div>
                </div>
	            <hr class="clearfix" />
                <div class="row" data-animation="fadeInUp">  
                  <div class="col-md-5">
                        <div class="normal-signin">
                            <div data-animation="bounceIn" id="form_1">
                              <div class="form-header">
                                  <i class="fa fa-sign-in"></i>
                                  <h1>Accedi</h1>
                              </div>
                              <div class="form-main">
                                  <?php
                                  if(count($signin_errors)) {
                                      ?>
                                      <div class="alert alert-danger">
                                          <?php
                                          foreach($signin_errors as $error) {
                                              echo '<p>' .$error. '</p>';
                                          }
                                          ?>
                                      </div>
                                    <?php
                                  }
                                  echo $this->Form->create("User",array(
                                      'action' => 'signin'
                                  ));
                                  ?>
                                      <div class="form-group">
                                          <?php
                                          echo $this->Session->flash();
                                          echo $this->Form->input("Login.email",array(
                                                  "label" => false,
                                                  "div" => array(
                                                      "class" => "un-wrap"
                                                  ),
                                                  'required' => true,
                                                  "beforeInput" => '<i class="fa fa-user"></i>',
                                                  'placeholder' => 'Email',
                                              )
                                          );
                                          echo $this->Form->input("Login.password",array(
                                              "label" => false,
                                              "div" => array(
                                                  "class" => "pw-wrap"
                                              ),
                                              'required' => true,
                                              "beforeInput" => '<i class="fa fa-lock"></i>',
                                              'placeholder' => 'password',
                                          ));
                                          ?>
                                    </div>
                                    <button type="submit" class="btn btn-block signin" id="signIn_1">Accedi</button>
                                  <?php
                                  echo $this->Form->end();
                                  ?>
                                </div>
                              <div class="form-footer">
                                <i class="fa fa-unlock-alt"></i>
                                <a href="/users/recoveryPassword" id="forgot_from_1">Password Dimenticata?</a>
                              </div>
                            </div>		
                    	</div>
                    </div>
                    <div class="col-md-2">
                        <div class="horizontal-divider"></div>
                    </div>
                    <div class="col-md-5">
                    	<div class="normal-registrazione">
                        	<div data-animation="bounceIn" id="form_2">
                        	<div class="form-header">
                                  <i class="fa fa-user"></i>
                                  <h1>Registrati</h1>
                            </div>
                            <?php echo $this->Form->create("User"); ?>
                            <div class="form-main">
                                <?php
                                if(count($signup_errors)) {
                                    ?>
                                    <div class="alert alert-danger">
                                        <?php
                                        foreach($signup_errors as $error) {
                                            echo '<p>' .reset($error). '</p>';
                                        }
                                        ?>
                                    </div>
                                <?php
                                }

                                echo $this->Form->input("Singup.first_name",array(
                                        "label" => false,
                                        'required' => true,
                                        'placeholder' => __('Nome'),
                                    )
                                );
                                echo $this->Form->input("Singup.last_name",array(
                                        "label" => false,
                                        'required' => true,
                                        'placeholder' => __('Cognome'),
                                    )
                                );
                                echo $this->Form->input("Singup.email",array(
                                        "label" => false,
                                        'required' => true,
                                        'placeholder' => 'Email',
                                    )
                                );
                                echo $this->Form->input("Singup.password",array(
                                    "label" => false,
                                    'required' => true,
                                    'placeholder' => 'password',
                                ));
                                ?>
                                <button type="submit" class="btn btn-block signin" id="signIn_2">Registrati</button>
                              </div>
                              <div class="form-footer">
                              	<p>Con riferimento all'<a href="javascript:;" data-toggle="modal" data-target="#term_cond">informativa sulla privacy</a>, presto il mio consenso per:</p>
                                <div class="clearfix"></div>
                                <input id="infoProm" type="checkbox" class="form-control">
                                <label for="infoProm">L'invio delle informazioni promozionali </label>
                                <div class="clearfix"></div>
                                <input id="terCon" type="checkbox" class="form-control" required="required">
                                <label for="terCon">La creazione del mio profilo all'interno di Negozy</label>
                              </div>
                            </div>
                            <?php
                            echo $this->form->end();
                            ?>
                        </div>
                    </div>
                </div>
                <hr class="clearfix" />
            </div>
        </div>
    </div>
</section>

<!-- Modal -->
<div class="modal fade" id="term_cond" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Chiudi</span></button>
                <h4 class="modal-title" id="myModalLabel">Informativa sulla privacy e cookie</h4>
            </div>
            <div class="modal-body">
                <?php
                    if($modal_terms) {
                        echo $modal_terms;
                    } else {
                        echo __('Pagina in costruzione');
                    }
                ?>
            </div>
            <?php /*?><div class="modal-footer">
                <button type="button" class="btn btn-danger" data-dismiss="modal"><?php echo __('Chiudi'); ?></button>
                <button type="button" class="btn btn-success" data-dismiss="modal" onclick="terms_cond()"><?php echo __('Accetto'); ?></button>
            </div><?php */?>
        </div>
    </div>
</div>

<script type="text/javascript">
    function terms_cond() {
        $('#terCon').prop('checked', true);
        $('.icheckbox_minimal-orange').addClass('checked');
    }
</script>