<style>
.relative {
position:relative

}
.segui_btn {
position:absolute;
top:0px;
right:0px;

} 
</style>
<div class="lapagina" data-ng-controller="user_ctrl">
  <nav class="navbar navbar-default navbar-static-top" role="navigation">
    <div class="navbar-header">
      <div class="navbar-center"> 
        <!--<img class="navbar-brand" alt="" src="/app/webroot/asset/admin/images/pixmania.jpg">--> 
      </div>
	  <ul class="nav navbar-nav">
	  	<li>
		  <ol class="breadcrumb">
			<li><a href="#/home">Bacheca</a></li>  
			<li class="active">{{user.first_name}} {{user.last_name}}</li>
		  </ol>
		</li>
	  </ul>
    </div>
  </nav>
  
  <div class="page page-services" >
    <div class="row">
      <div class="col-lg-8 col-lg-offset-2 col-md-10 col-md-offset-1">
        <div class="panel panel-profile">
          <div class="panel-heading text-center bg-info relative"> 
		  	
			<!-- pulsante che appare quando una persona è da seguire -->
			<a href="#/products/add" class="btn btn-lg btn-success segui_btn" type="submit"><i class="fa fa-plus"></i> <?php echo __("Segui"); ?></a>
			
			<!-- pulsante che appare quando una persona è seguita -->
			<!--<a href="#/products/add" class="btn btn-lg btn-warning segui_btn" type="submit"><i class="fa fa-check"></i> <?php echo __("Segui già"); ?></a>-->
			
		  <img class="img-circle img80_80" src="/asset/admin/images/g1.jpg" alt="">
            <h3 class="ng-binding">{{user.first_name}} {{user.last_name}}</h3>
          </div>
          <div class="list-justified-container">
            <ul class="list-justified text-center">
              <li>
                <p class="size-h3">679</p>
                <p class="text-muted">Post</p>
              </li>
              
              <li>
                <p class="size-h3">23</p>
                <p class="text-muted">Following</p>
              </li>
            </ul>
          </div>
        </div>
        <div class="row">
          <div class="col-sm-6">
            <div class="panel mini-box"> <span class="box-icon bg-success"> <i class="fa fa-shopping-cart"></i></span>
              <div class="box-info">
                <p class="size-h2">5</p>
                <p class="text-muted">Negozi</p>
              </div>
            </div>
          </div>
          <div class="col-sm-6">
            <div class="panel mini-box"> <span class="box-icon bg-info"> <i class="fa fa-check"></i> </span>
              <div class="box-info">
                <p class="size-h2">84</p>
                <p class="text-muted">Prodotti</p>
              </div>
            </div>
          </div>
        </div>
        <div class="panel panel-default">
          <div class="panel-heading"><strong><span class="glyphicon glyphicon-th"></span> Profile Info</strong></div>
          <div class="panel-body">
            <div class="media">
              <div class="media-body">
                <ul class="list-unstyled list-info">
                  <li class="ng-binding"> <span class="icon glyphicon glyphicon-user"></span>
                    <label><?php echo __("Nome"); ?></label>
                      {{user.first_name}} {{user.last_name}} </li>
                  <li> <span class="icon glyphicon glyphicon-envelope"></span>
                    <label>Email</label>
                      {{user.email}} </li>
                  <li> <span class="icon glyphicon glyphicon-home"></span>
                    <label>Address</label>
                    Street 123, Avenue 45, Country </li>
                  <li> <span class="icon glyphicon glyphicon-earphone"></span>
                    <label>Contact</label>
                    (+012) 345 6789 </li>
                  <li> <span class="icon glyphicon glyphicon-flag"></span>
                    <label>Nationality</label>
                    Australia </li>
                </ul>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
  
  
</div>
