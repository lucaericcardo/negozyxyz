<section id="password_recovery" class="authenty password-recovery">
    <div class="section-content">
        <div class="wrap">
            <div class="container">
                <div class="form-wrap">
                    <div class="row">
                        <div class="col-xs-12 col-sm-3 brand" data-animation="fadeInUp">
                        	<h2>
                            <img src="asset/countdown/img/logo.png" alt="" class="img-responsive">
                            </h2>
                        </div>
                        <div class="col-sm-1 hidden-xs">
                            <div class="horizontal-divider"></div>
                        </div>
                        <div class="col-xs-12 col-sm-8 main" data-animation="fadeInLeft" data-animation-delay=".5s">
                            <h2>Dimenticato la password?</h2>
                            <p>Niente paura. Inserisci la tua email, riceverai le istruzioni per crearne una nuova.</p>
                            <form>
                              <div class="form-group">
                                <input type="email" class="form-control" placeholder="Email" required>
                              </div>
                                <div class="row">
                                    <div class="col-xs-12 col-sm-4 col-sm-offset-8">
                                        <button type="submit" class="btn btn-block reset">Reset Password</button>
                                    </div>
                                </div>
                            </form>	
                        </div>
                    </div>
                        
                </div>
            </div>
        </div>
    </div>
</section>