<section id="password_recovery" class="authenty password-recovery">
    <div class="section-content">
        <div class="wrap">
            <div class="container">
                <div class="form-wrap">
                    <div class="row">
                        <div class="col-xs-12 col-sm-3 brand" data-animation="fadeInUp">
                        	<h2>
                            <a href="/signin"><img src="/asset/countdown/img/logo.png" alt="" class="img-responsive"></a>
                            </h2>
                            <p>Inserisci la tua nuova password.</p> 
                        </div>
                        <div class="col-sm-1 hidden-xs">
                            <div class="horizontal-divider"></div>
                        </div>
                        <div class="col-xs-12 col-sm-8 main">
                        	<div class="alert alert-danger <?php echo $errorNoMatching; ?>" data-animation="fadeInUp" data-animation-delay=".5s">
                            	<strong>Controlla di aver digitato la stessa password.</strong>
                            </div>

                            <div class="alert alert-danger <?php echo $error; ?>" data-animation="fadeInUp" data-animation-delay=".5s">
                                <strong>Impossibile cambiare la password. Ritena <a href="/users/recoveryPassword">Recupero password</a> o contatta l'amministritore.</strong>
                            </div>


                            <div data-animation="fadeInUp" data-animation-delay=".5s" class="<?php echo $success; ?>">
                                <h2>Password modificata correttamente</h2>
                                <form action="/signin" method="post">
                                  <div class="form-group">
                                    <button type="submit" class="btn btn-block reset">Vai al login</button>
                                  </div>
                                </form>
                            </div>

                            <form data-animation="fadeInLeft" data-animation-delay=".5s" class="<?php echo $form; ?>" action="/users/changePassword" method="POST">
                                <input name="token" type="hidden" value="<?php echo $token; ?>" />
                              <div class="form-group">
                                <input name="password" autocomplete="off" type="password" class="form-control" placeholder="Nuova Password" required>
                              </div>
                              <div class="form-group">
                                <input name="confirm_password" autocomplete="off" type="password" class="form-control" placeholder="Ripeti Nuova Password" required>
                              </div>
                                <div class="row">
                                    <div class="col-xs-12 col-sm-4 col-sm-offset-8">
                                        <button type="submit" class="btn btn-block reset">Salva</button>
                                    </div>
                                </div>
                            </form>	
                        </div>
                    </div>
                        
                </div>
            </div>
        </div>
    </div>
</section>