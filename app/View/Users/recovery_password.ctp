<section id="password_recovery" class="authenty password-recovery">
    <div class="section-content">
        <div class="wrap">
            <div class="container">
                <div class="form-wrap">
                    <div class="row">
                        <div class="col-xs-12 col-sm-3 brand" data-animation="fadeInUp">
                        	<h2>
                            <a href="/signin"><img src="/asset/countdown/img/logo.png" alt="" class="img-responsive"></a>
                            </h2>
                            <p>Inserisci il tuo indirizzo email.</p>
                        </div>
                        <div class="col-sm-1 hidden-xs">
                            <div class="horizontal-divider"></div>
                        </div>
                        <div class="col-xs-12 col-sm-8 main">
                        	<div class="alert alert-danger <?php echo $error; ?>" data-animation="fadeInUp" data-animation-delay=".5s">
                                <strong>L'email inserita non risulta registrata, controlla di averla digitata correttamente.</strong>
                            </div>

                            <div class="<?php echo $success; ?>" data-animation="fadeInLeft" data-animation-delay=".5s">
                                <p>Ti abbiamo mandato una mail all'indirizzo <?php echo $email; ?>, controlla di averla ricevuta e segui le indicazioni per poter inserire una nuova password.</p>
                                <form>
                                  <div class="form-group">
                                    <button type="button" class="btn btn-block reset" onclick="resend()">Inviami nuovamente la mail</button>
                                  </div>
                                </form>
                            </div>
                            
                            <form name="recoveryPassword" class="<?php echo $form; ?>" data-animation="fadeInLeft" data-animation-delay=".5s" action="/users/recoveryPassword" method="POST">
                                <input type="hidden" name="resend" value="<?php echo $resend; ?>">
                              <div class="form-group">
                                <input type="email" name="email" class="form-control" placeholder="email@example.com" value="<?php echo $email; ?>" required>
                              </div>
                                <div class="row">
                                    <div class="col-xs-12 col-sm-4 col-sm-offset-8">
                                        <button type="submit" class="btn btn-block reset">Invia</button>
                                    </div>
                                </div>
                            </form>	
                        </div>
                    </div>
                        
                </div>
            </div>
        </div>
    </div>
</section>
<script type="text/javascript">
    function resend() {
        document.recoveryPassword.resend.value = 'true';
        document.recoveryPassword.submit();
    }
</script>