<div class="lapagina" data-ng-controller="ThemeMainCtrl">
    <nav class="navbar navbar-default navbar-static-top" role="navigation">
        <div class="navbar-header">
            <ul class="nav navbar-nav">
                <li>
                    <ol class="breadcrumb">
                        <li class="active"><?php echo __("Personalizza"); ?></li>
                    </ol>
                </li>
            </ul>
        </div>
    </nav>
    <div id="apps-cat">
        <div class="page page-services">
            <div class="nome-categoria clearfix">
                <h2 class="pull-left"><?php echo __("Lista temi"); ?></h2>
                <div class="clearfix"></div>
                <div class="contCard">
                    <div data-ng-repeat="theme in themes" class="panel panel-box card">
                        <div  style="height: 150px" class="panel-top bg-info">
                            <a href="#/theme/{{theme.uri}}"><img style="max-width: 150px" data-ng-src="{{theme.icon}}"/></a>
                        </div>
                        <div class="panel-bottom bg-reverse">
                            <p class="size-h4"><a href="#/theme/{{theme.uri}}">{{theme.name}}</a></p>
                            <i data-ng-if="theme.id == active_theme" class="fa fa-check-circle color-success"></i>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>