<div class="lapagina" data-ng-controller="ThemeDetailsCtrl">
    <nav class="navbar navbar-default navbar-static-top" role="navigation">
        <div class="navbar-header">
            <ul class="nav navbar-nav">
                <li>
                    <ol class="breadcrumb">
                        <li><a href="#/themes"><?php echo __("Personalizza"); ?></a></li>
                        <li class="active">{{theme.name}}</li>
                    </ol>
                </li>
            </ul>
        </div>
    </nav>
    <div id="apps-app">
        <div class="page page-services">
            <div class="row">
                <div class="col-xs-12 col-sm-12 col-md-6">
                    <div class="media">
                        <a href="javascript:;" class="pull-left">
                            <img style="max-width: 150px" class="img-rounded img128_128" data-ng-src="{{theme.icon}}" alt="">
                        </a>
                        <div class="media-body">
                            <div class="pull-left">
                                <h3 class="media-heading">{{theme.name}}</h3>
                            </div>
                            <button data-ng-show="theme_active != theme.id" class="btn btn-success btn-w-xs pull-right" type="button" data-ng-click="enableTheme(theme.id)"><?php echo __("Abilita"); ?></button>
                            <div data-ng-show="theme_active == theme.id" class="alert alert-info pull-right">
                                <p><?php echo __("Tema in uso"); ?></p>
                            </div>
                            <div class="clearfix"></div>
                            <hr />
                            <h4 data-ng-show="theme.description">Descrizione</h4>
                            {{theme.description}}
                        </div>
                    </div>
                </div>
                <div class="col-xs-12 col-sm-12 col-md-6" data-ng-show="slides.length">
                    <div class="slider">
                        <img ng-repeat="slide in slides" style="width: 100%" class="slide slide-animation nonDraggableImage"
                             ng-swipe-right="nextSlide()" ng-swipe-left="prevSlide()"
                             ng-hide="!isCurrentSlideIndex($index)" ng-src="{{slide}}">

                        <a class="arrow prev" href="javascript:;" ng-click="nextSlide()"></a>
                        <a class="arrow next" href="javascript:;" ng-click="prevSlide()"></a>
                        <nav class="nav">
                            <div class="wrapper">
                                <ul class="dots">
                                    <li class="dot" ng-repeat="slide in slides">
                                        <a href="javascript:;" ng-class="{'active':isCurrentSlideIndex($index)}" ng-click="setCurrentSlideIndex($index);"></a>
                                    </li>
                                </ul>
                            </div>
                        </nav>
                    </div>

                </div>
            </div>
        </div>
    </div>
</div>