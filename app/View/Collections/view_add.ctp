<div class="lapagina">
    <nav class="navbar navbar-default navbar-static-top" role="navigation">
        <div class="navbar-header">
            <ul class="nav navbar-nav">
                <li>
                    <ol class="breadcrumb">
                        <li><a href="#/collections"><?php echo __("Collezioni"); ?></a></li>
                        <li class="active"><a><?php echo __("Aggiungi collezione"); ?></a></li>
                    </ol>
                </li>
            </ul>
        </div>
    </nav>
    <div class="page page-table">
        <div class="row">
            <div class="col-lg-10 col-lg-offset-1 col-md-12">
                <section class="panel panel-default table-dynamic" data-ng-controller="addCollectionCtrl">
                    <div class="panel-body">

                        <div ng-show="successSave" class="callout callout-success">
                            <p><?php echo __("Collezione aggiunta con successo"); ?></p>
                            <a href="#/collections"><?php echo __("Torna alla lista delle collezioni"); ?></a>
                        </div>

                        <div ng-if="onLoading">
                            <p class="text-muted"><i class="fa fa-spinner fa-spin"></i> Caricamento {{current_image}} in corso.</p>
                            <progress class="progress-striped active">
                                <bar value="progress" type="success"><span>{{count_image}} / {{uploader.queue.length}}</span></bar>
                            </progress>

                        </div>
                        <div ng-if="onLoading || successSave">
                            <ul class="list-unstyled">
                                <li data-ng-repeat="success_image in success_images" class="text-success"><i class="fa fa-check"></i> {{success_image}} <b><?php echo __("aggiunto con successo"); ?></b></li>
                            </ul>
                            <ul class="list-unstyled">
                                <li data-ng-repeat="error_image in error_images" class="text-danger"><i class="fa fa-times"></i> {{error_image}} <b><?php echo __("errore nel caricamento"); ?></b></li>
                            </ul>
                        </div>
                        <div ng-show="errorSave" class="callout callout-danger">
                            <?php echo __("Si è verificato un problema durante l'aggiunta della collection."); ?>
                        </div>


                        <div class="clearfix"></div>
                        <form ng-if="!onLoading && !successSave" name="collection_form" class="form-validation">
                            <div class="row">
                                <div class="col-md-2">
                                    <h4><?php echo __("Dati collezione"); ?></h4>
                                </div>
                                <div class="col-md-10">
                                    <div class="form-group">
                                        <label for=""><?php echo __("Nome"); ?> *</label>
                                        <input type="text" class="form-control" required data-ng-model="collection.name">
                                    </div>
                                    <div class="form-group">
                                        <label for=""><?php echo __("Descrizione"); ?></label>
                                        <div text-angular data-ng-model="collection.description"></div>
                                    </div>
                                </div>
                            </div>
                            <hr/>
                            <div class="divider"></div>
                            <div class="row">
                                <div class="col-md-2">
                                    <h4><?php echo __("Colore collezione"); ?></h4>
                                </div>
                                <div class="col-md-10">
                                    <div class="form-group">
                                        <div class="btn-group" dropdown is-open="status.isopen1">
                                            <button type="button" class="btn btn-default dropdown-toggle">
                                                <span data-ng-if="!collection.color">Scegli</span>
                                                <span data-ng-if="collection.color"><i class="glyphicon glyphicon-stop" data-ng-class="collection.color.key"></i> {{collection.color.label}}</span>
                                                <span class="caret"></span> </button>
                                            <ul class="dropdown-menu collection-color" role="menu">
                                                <li data-ng-repeat="item in colors"><a data-ng-click="setColor(item)" href="javascript:void(0);"><i class="glyphicon glyphicon-stop" data-ng-class="item.key"></i> {{item.label}}</a></a></li>
                                            </ul>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <hr/>
                            <div class="divider"></div>
                            <div class="row">
                                <div class="col-md-2">
                                    <h4><?php echo __("Copertina collezione"); ?></h4>
                                </div>
                                <div class="col-md-10">
                                    <input accept="image/*" title="<?php echo __("Seleziona immagine") ?>" data-ui-file-upload type="file" nv-file-select uploader="uploader" class="btn-primary">
                                    <ul class="list-unstyled" id="uploadImg" ui-sortable="sortableOptions" ng-model="uploader.queue">
                                        <li ng-repeat="item in uploader.queue">
                                            <div ng-thumb="{ file: item._file, height: 100 }"></div>
                                            <div>
                                                <button ng-click="item.remove()" class="btn btn-danger btn-xs"><i class="fa fa-trash-o"></i></button>
                                            </div>
                                        </li>
                                    </ul>
                                </div>
                            </div>
                            <hr/>
                            <div class="divider"></div>
                            <div class="row">
                                <div class="col-md-10 col-md-offset-2">
                                    <button data-ng-click="saveCollection()" ng-disabled="collection_form.$invalid" class="btn btn-success btn-lg" type="button"><?php echo __("Salva"); ?></button>
                                </div>
                            </div>
                        </form>
                    </div>
                </section>
            </div>
        </div>
    </div>
</div>