<div class="lapagina" data-ng-controller="collectionCtrl">
    <nav class="navbar navbar-default navbar-static-top" role="navigation">
        <div class="navbar-header">
            <ul class="nav navbar-nav">
                <li>
                    <ol class="breadcrumb">
                        <li class="active"><?php echo __("Collezioni"); ?></li>
                    </ol>
                </li>
            </ul>
            <ul class="nav navbar-nav navbar-right">
                <div class="navbar-form">
                    <a href="#/collections/add" class="btn btn-success" type="submit"><i class="fa fa-plus"></i> <?php echo __("Aggiungi collezione"); ?></a>
                </div>
            </ul>
        </div>
    </nav>

    <div ng-if="!collections.length" class="page-err">
        <div class="err-container">
            <div class="text-center">
                <div class="err-status">
                    <h1><i class="fa fa-list"></i></h1>
                </div>
                <div class="err-message">
                    <h2><?php echo __("Attualmente non sono presenti collezioni."); ?></h2>
                </div>
                <br >
                <a href="#/collections/add" class="btn btn-default btn-lg"><i class="fa fa-plus"></i> <?php echo __("Aggiungi collezioni"); ?></a>
                <br >
                <br >
                <br >
            </div>
        </div>
    </div>

    <div ng-show="collections.length" class="page page-table">
        <div class="row">
            <div class="col-lg-10 col-lg-offset-1 col-md-12">
                <section class="panel panel-default table-dynamic">
                    <div class="panel-body">
                        <div class="table-responsive">
                            <table class="table table-bordered table-striped table-responsive">
                                <thead>
                                <tr>
                                	<th>
                                        <div class="th">
                                            <?php echo __("Copertina"); ?>
                                        </div>
                                    </th>
                                    <th>
                                        <div class="th">
                                            <?php echo __("Nome"); ?>
                                            <span class="glyphicon glyphicon-chevron-up" data-ng-click=" order('name','ASC') " data-ng-class="{active: row == 'name' && orderType == 'ASC'}"></span>
                                            <span class="glyphicon glyphicon-chevron-down" data-ng-click=" order('name','DESC') " data-ng-class="{active: row == 'name' && orderType == 'DESC'}"></span>
                                        </div>
                                    </th>
                                    <th>
                                        <div class="th">
                                            <?php echo __("Colore"); ?>
                                            <span class="glyphicon glyphicon-chevron-up" data-ng-click=" order('color','ASC') " data-ng-class="{active: row == 'color' && orderType == 'ASC'}"></span>
                                            <span class="glyphicon glyphicon-chevron-down" data-ng-click=" order('color','DESC') " data-ng-class="{active: row == 'color' && orderType == 'DESC'}"></span>
                                        </div>
                                    </th>
                                    <th>
                                        <div class="th">
                                            <?php echo __("Data di creazione"); ?>
                                            <span class="glyphicon glyphicon-chevron-up" data-ng-click=" order('created','ASC') " data-ng-class="{active: row == 'created' && orderType == 'ASC'}"></span>
                                            <span class="glyphicon glyphicon-chevron-down" data-ng-click=" order('created','DESC') " data-ng-class="{active: row == 'created' && orderType == 'DESC'}"></span>
                                        </div>
                                    </th>
                                    <th class="text-right" width="120">
                                        <div class="th">
                                            <?php echo __("Azioni"); ?>
                                        </div>
                                    </th>
                                </tr>
                                </thead>
                                <tbody>
                                <tr data-ng-repeat="collection in collections">
                                    <td>
                                        <img ng-if="collection.image" data-ng-src="{{collection.image}}" height="50" style="border: solid 1px #CCC;" />
                                        <img ng-if="!collection.image" height="50" ng-src="http://cdn.negozy.com/negozy/Logos/no_logo.png" style="border: solid 1px #CCC;" />
                                    </td>
                                    <td>{{collection.name}}</td>
                                    <td><i class="glyphicon glyphicon-stop {{collection.color}}"></i></td>
                                    <td>{{collection.timestamp_created | date:'dd/MM/yyyy  H:mm'}}</td>
                                    <td class="text-right">
                                        <a class="btn-icon btn-icon-sm bg-primary" href="#/collections/edit/{{collection.id}}"><i class="fa fa-pencil-square-o"></i><?php //echo __("Modifica"); ?></a>
                                        <a class="btn-icon btn-icon-sm bg-danger" href="javascript:;" confirm-callback="deleteCollection(collection.id)" confirm-message="<?php echo __("Sei sicuro di voler eliminare la collezione ?"); ?>" confirm-modal><i class="glyphicon glyphicon-trash"></i><?php //echo __("Elimina"); ?></a>
                                    </td>
                                </tr>
                                </tbody>
                            </table>
                        </div>
                        <footer class="table-footer">
                            <div class="row">
                                <div class="col-md-6 page-num-info">
                                    <span>
                                        <?php echo __("Elementi per pagina"); ?>
                                        <select data-ng-model="numPerPage" data-ng-options="num for num in numPerPageOpt" data-ng-change="onNumPerPageChange()">
                                        </select>
                                    </span>
                                </div>
                                <div class="col-md-6 text-right pagination-container">
                                    <pagination class="pagination-sm" ng-model="currentPage" total-items="totalCollections" max-size="4" ng-change="select(currentPage)" items-per-page="numPerPage" rotate="false" boundary-links="true"></pagination>
                                </div>
                            </div>
                        </footer>
                    </div>
                </section>
            </div>
        </div>
    </div>
</div>