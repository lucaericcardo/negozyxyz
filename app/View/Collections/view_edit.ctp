<div class="lapagina">
    <nav class="navbar navbar-default navbar-static-top" role="navigation">
        <div class="navbar-header">
            <ul class="nav navbar-nav">
                <li>
                    <ol class="breadcrumb">
                        <li><a href="#/collections"><?php echo __("Collezioni"); ?></a></li>
                        <li class="active"><a><?php echo __("Modifica collezione"); ?></a></li>
                    </ol>
                </li>
            </ul>
        </div>
    </nav>
    <div class="page page-table">
        <div class="row">
            <div class="col-lg-10 col-lg-offset-1 col-md-12">
                <section class="panel panel-default table-dynamic" data-ng-controller="editCollectionCtrl">
                    <div class="panel-body">
                        <div class="clearfix"></div>
                        <form name="collection_form" class="form-validation">
                            <div class="row">
                                <div class="col-md-2">
                                    <h4><?php echo __("Dati collezione"); ?></h4>
                                </div>
                                <div class="col-md-10">
                                    <div class="form-group">
                                        <label for=""><?php echo __("Nome"); ?> *</label>
                                        <input type="text" class="form-control" required data-ng-model="collection.name">
                                    </div>
                                    <div class="form-group">
                                        <label for=""><?php echo __("Descrizione"); ?></label>
                                        <div text-angular data-ng-model="collection.description"></div>
                                    </div>
                                </div>
                            </div>
                            <hr/>
                            <div class="divider"></div>
                            <div class="row">
                                <div class="col-md-2">
                                	<h4><?php echo __("Colore collezione"); ?></h4>
                                </div>
                                <div class="col-md-10"> 
                                    <div class="form-group">
                                        <div class="btn-group" dropdown is-open="status.isopen1">
                                            <button type="button" class="btn btn-default dropdown-toggle">
                                                <span data-ng-if="!collection.color">Scegli</span>
                                                <span data-ng-if="collection.color"><i class="glyphicon glyphicon-stop" data-ng-class="collection.color.key"></i> {{collection.color.label}}</span>
                                                <span class="caret"></span> </button>
                                            <ul class="dropdown-menu collection-color" role="menu">
                                                <li data-ng-repeat="item in colors"><a data-ng-click="setColor(item)" href="javascript:void(0);"><i class="glyphicon glyphicon-stop" data-ng-class="item.key"></i> {{item.label}}</a></a></li>
                                            </ul>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <hr/>
                            <div class="divider"></div>
                            <div class="row">
                                <div class="col-md-2">
                                    <h4><?php echo __("Copertina collezione"); ?></h4>
                                </div>
                                <div class="col-md-10">
                                    <input class="btn-primary"  ng-disabled="onLoading" accept="image/*" id="collection_image" data-ng-model="image" title="<?php echo __("Seleziona immagine") ?>" data-ui-file-upload type="file" nv-file-select uploader="uploader">
                                    <div class="divider"></div>
                                    <div ng-if="onLoading">
                                        <p class="text-muted"><i class="fa fa-spinner fa-spin"></i> Caricamento {{current_image}} in corso.</p>
                                        <progress  class="progress-striped active">
                                            <bar value="progress" type="success"><span>{{progress}}</span></bar>
                                        </progress>
                                    </div>
                                    <img ng-src="{{image.image}}" height="100" />
                                </div>
                            </div>
                            <hr/>
                            <div class="divider"></div>
                            <div class="row">
                                <div class="col-md-10 col-md-offset-2">
                                    <button data-ng-click="updateCollection()" ng-disabled="collection_form.$invalid" class="btn btn-success btn-lg" type="button"><?php echo __("Salva"); ?></button>
                                </div>
                            </div>
                        </form>
                    </div>
                </section>
            </div>
        </div>
    </div>
</div>