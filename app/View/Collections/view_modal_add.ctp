<div class="modal-header">
    <h3 class="modal-title"><?php echo __("Aggiungi nuova collezione"); ?></h3>
</div>
<div class="modal-body">
    <form name="addCollection" class="form-horizontal form-validation">
        <div class="form-group">
            <label for="" class="col-sm-4"><?php echo __("Nome"); ?></label>
            <div class="col-sm-8">
                <input type="text" class="form-control" required data-ng-model="collection.name">
            </div>
        </div>
        <div class="form-group">
            <label for="" class="col-sm-4"><?php echo __("Colore"); ?></label>
            <div class="col-sm-8">
                <div class="btn-group" dropdown is-open="status.isopen1">
                    <button type="button" class="btn btn-default dropdown-toggle">
                        <span data-ng-if="!collection.color">Scegli</span>
                        <span data-ng-if="collection.color"><i class="glyphicon glyphicon-stop" data-ng-class="collection.color.key"></i> {{collection.color.label}}</span>
                        <span class="caret"></span> </button>
                    <ul class="dropdown-menu collection-color" role="menu">
                        <li data-ng-repeat="item in colors"><a data-ng-click="setColor(item)" href="javascript:void(0);"><i class="glyphicon glyphicon-stop" data-ng-class="item.key"></i> {{item.label}}</a></a></li>
                    </ul>
                </div>
            </div>
        </div>
    </form>
</div>
<div class="modal-footer">
    <button class="btn btn-primary"  ng-disabled="addCollection.$invalid" ng-click="ok()"><?php echo __("Salva"); ?></button>
    <button class="btn btn-warning" ng-click="cancel()"><?php echo __("Annulla"); ?></button>
</div>