<div class="lapagina" data-ng-controller="VentureCtrl">
    <div class="page page-table">
        <div class="row">
            <div class="col-lg-10 col-lg-offset-1 col-md-12">
                <section class="panel panel-default table-dynamic">
                    <div class="panel-body">
                        <h1><?php echo __("Iniziative attive"); ?></h1>
                        <div class="table-responsive">
                            <table class="table table-bordered table-striped table-responsive">
                                <thead>
                                <tr>
                                    <th>
                                        <div class="th">
                                            <?php echo __("Codice"); ?>
                                        </div>
                                    </th>
                                    <th>
                                        <div class="th">
                                            <?php echo __("Stato"); ?>
                                        </div>
                                    </th>
                                    <th>
                                        <div class="th">
                                            <?php echo __("Data"); ?>
                                        </div>
                                    </th>
                                    <th>
                                        <div class="th">
                                            <?php echo __("Totale"); ?>
                                        </div>
                                    </th>
                                    <th>
                                        <div class="th">
                                            <?php echo __("Azione"); ?>
                                        </div>
                                    </th>
                                </tr>
                                </thead>
                                <tbody>
                                <tr data-ng-repeat="order in orders">
                                    <td>{{order.uid}}</td>
                                    <td>{{order.status}}</td>
                                    <td>{{order.date | date:'dd/MM/yyyy  H:mm'}}</td>
                                    <td>{{order.amount}}</td>
                                    <td><a class="btn btn-info" data-ng-href="#/order/{{order.id}}" > <i class="fa fa-eye"></i></a></td>
                                </tr>
                                </tbody>
                            </table>
                        </div>
                        <footer class="table-footer">
                            <div class="row">
                                <div class="col-md-6 page-num-info">
                                    <span>
                                        <?php echo __("Elementi per pagina"); ?>
                                        <select data-ng-model="numPerPage" data-ng-options="num for num in numPerPageOpt" data-ng-change="onNumPerPageChange()">
                                        </select>
                                    </span>
                                </div>
                                <div class="col-md-6 text-right pagination-container">
                                    <pagination class="pagination-sm" ng-model="currentPage" total-items="totalOrders" max-size="4" ng-change="select(currentPage)" items-per-page="numPerPage" rotate="false" boundary-links="true"></pagination>
                                </div>
                            </div>
                        </footer>
                    </div>
                </section>
            </div>
        </div>
    </div>
</div>