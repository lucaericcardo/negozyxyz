<div class="lapagina add-page" >
    <nav class="navbar navbar-default navbar-static-top" role="navigation">
        <div class="navbar-header">
            <ul class="nav navbar-nav">
                <li>
                    <ol class="breadcrumb">
                        <li><a href="#/collections/"><?php echo __("Le tue collezioni"); ?></a></li>
                        <li class="active"><a><?php echo __("Modifica collezione"); ?></a></li>
                    </ol>
                </li>
            </ul>
            <ul class="nav navbar-nav navbar-right">
                <div class="navbar-form">
                    <img src="/asset/img/easyadmin.png" style="height:30px"/>
                </div>
            </ul>
        </div>
    </nav>
    <div class="page page-table" data-ng-controller="editCollectionCtrl">
        <div class="row">
            <div class="col-sm-12 col-md-8 col-lg-9">
                <section class="panel panel-default">
                    <div class="panel-heading"><strong><span class="flag-icon flag-icon-it"></span>&nbsp;<?php echo __("Titolo"); ?> </strong>
                    </div>
                    <div class="panel-body">
                        <input type="text" data-ng-model="collection.title" class="form-control" data-ng-change="updateURL()">
                        <span>uri: <a contenteditable data-ng-model="collection.uri"></a></span>
                    </div>
                </section>

                <section class="panel panel-default">
                    <div class="panel-heading"><strong><span class="flag-icon flag-icon-it"></span>&nbsp;<?php echo __("Descrizione"); ?></strong></div>
                    <div class="panel-body">
                        <div text-angular data-ng-model="collection.description"></div>
                    </div>
                </section>

                <section class="panel panel-default">
                    <div class="panel-body text-right">
                        <button class="btn btn-success btn-lg" ng-click="update();" ng-disabled="!collection.title" type="button"><?php echo __("Salva"); ?></button>
                    </div>
                </section>
            </div>
            <div class="col-sm-12 col-md-4 col-lg-3 ">
                <section class="panel panel-default">
                    <div class="panel-heading"><strong><i class="fa fa-sliders"></i> <?php echo __("Impostazioni"); ?></strong></div>
                    <div class="panel-body">
                        <button class="btn btn-info btn-block" ng-click="openSeo()" type="button"><i class="fa fa-search"></i> <?php echo __("Motori di ricerca"); ?></button>
                        <hr />
                        <label>Ordine</label>
                        <input type="text" data-ng-model="collection.order" class="form-control">
                    </div>
                </section>
                <section class="panel panel-default table-dynamic" >
                    <div class="panel-heading"><strong><i class="glyphicon glyphicon-picture"></i> <?php echo __("Immagini") ?></strong></div>
                    <div class="panel-body">
                        <input class="btn-primary"  ng-disabled="onLoading" accept="image/*" id="collection_image" data-ng-model="image" title="<?php echo __("Seleziona immagine") ?>" data-ui-file-upload type="file" nv-file-select uploader="uploader">
                        <div class="divider"></div>
                        <div ng-if="onLoading">
                            <p class="text-muted"><i class="fa fa-spinner fa-spin"></i> Caricamento {{current_image}} in corso.</p>
                            <progress  class="progress-striped active">
                                <bar value="progress" type="success"><span>{{progress}}</span></bar>
                            </progress>
                        </div>
                        <img ng-src="{{image.url}}" height="100" />
                    </div>
                </section>
            </div>
        </div>
    </div>
</div>