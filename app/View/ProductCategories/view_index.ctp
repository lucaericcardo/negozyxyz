<div class="lapagina" data-ng-controller="collectionCtrl">
    <nav class="navbar navbar-default navbar-static-top" role="navigation">
        <div class="navbar-header">
            <ul class="nav navbar-nav">
                <li>
                    <ol class="breadcrumb">
                        <li ng-repeat="bread in breadcrumb" ng-class="{'active':$last}" ><a href="{{bread.url}}">{{bread.title}}</a></li>
                    </ol>
                </li>
            </ul>
            <ul class="nav navbar-nav navbar-right">
                <div class="navbar-form">
                    <a href="#/collections/add/{{selected_language}}/{{parent_id}}" class="btn btn-success" type="submit"><i class="fa fa-plus"></i> <?php echo __("Aggiungi collezione"); ?></a>
                </div>
            </ul>
        </div>
    </nav>

    <div ng-if="!collections.length" class="page-err">
        <div class="err-container">
            <div class="text-center">
                <div class="err-status">
                    <h1><i class="fa fa-tags"></i></h1>
                </div>
                <div class="err-message">
                    <h2><?php echo __("Attualmente non sono presenti collezioni."); ?></h2>
                </div>
                <br >
                <a href="#/collections/add/{{selected_language}}/{{parent_id}}" class="btn btn-default btn-lg"><i class="fa fa-plus"></i> <?php echo __("Aggiungi collezione"); ?></a>
                <br >
                <br >
                <br >
            </div>
        </div>
    </div>

    <div ng-show="collections.length" class="page page-table">
    	<div class="row">
          <div class="col-md-12">
              <!-- new code -->
              <section class="panel panel-default">
                  <div class="panel-heading">
                      <div class="pull-left">
                          <div data-ng-hide="!checkSelected()">
                              <strong class="hidden-xs"><span class="fa fa-level-up fa-rotate-180"></span> <?php echo __("Se selezionate"); ?>:</strong>
                              <button confirm-callback="deleteAll()" confirm-message="<?php echo __("Sei sicuro di voler eliminare le collezioni selezionate?"); ?>" confirm-modal class="btn btn-danger btn-xs" type="button"><?php echo __("Elimina"); ?></button>
                          </div>
                          <div data-ng-hide="checkSelected()">
                              <strong class="hidden-xs"><span class="fa fa-level-up fa-rotate-180"></span> Seleziona pi&uacute; collezioni per effettuare operazioni multiple:</strong>
                          </div>
                      </div>
                      <div is-open="status.isopen" dropdown="" class="btn-group pull-right">
                          <button class="btn btn-primary dropdown-toggle btn-xs" type="button" aria-haspopup="true" aria-expanded="false"> <?php echo __("Visualizza lingua"); ?> <span class="caret"></span></button>
                          <ul role="menu" class="dropdown-menu">
                              <li data-ng-class="{'active':(lang_id == selected_language)}" data-ng-repeat="(id, lang_id) in objectKeys(shop_languages)" data-ng-click="selectLanguage(lang_id)"><a href="javascript:;">{{shop_languages[lang_id]}}</a></li>
                          </ul>
                      </div>
                      <div class="clearfix"></div>
                  </div>
                  <div class="panel-body">
                      <div class="table-responsive">
                          <table class="table table-striped">
                              <thead>
                              <tr>
                                  <th style="width: 68px;">
                                      <label class="ui-checkbox">
                                          <input type="checkbox" data-ng-model="obj.selectAll" data-ng-click="toggleList()">
                                          <span style="color:#FFF"></span>
                                      </label>
                                  </th>
                                  <th><?php echo __("Immagine"); ?></th>
                                  <th><?php echo __("Collezione"); ?></th>
                                  <th><?php echo __("Ordine"); ?></th>
                                  <th data-ng-repeat="(id, lang_id) in objectKeys(shop_languages)" style="width: 100px;">{{shop_languages[lang_id]}}</th>
                                  <th class="text-right"><?php echo __("Azioni"); ?></th>
                              </tr>
                              </thead>
                              <tbody>
                                  <tr data-ng-repeat="collection in collections">
                                      <td>
                                          <label class="ui-checkbox">
                                              <input type="checkbox" value="{{collection.id}}" data-ng-model="collection.selected" data-ng-change="updateSelected()">
                                              <span style="color:#FFF"></span>
                                          </label>
                                      </td>
                                      <td>
                                          <img height="50" ng-src="{{collection.image}}" />
                                      </td>
                                      <td>
                                          <a href="#/collections/edit/{{selected_language}}/{{collection.id}}" class="text-info"  ng-if="collection.content_id"><b>{{collection.title}}</b></a>
                                          <a href="#/collections/edit/{{selected_language}}/{{collection.id}}" class="text-primary"  ng-if="!collection.content_id"><i class="primary">{{collection.title}}</i></a>
                                      </td>
                                      <td>{{collection.order}}</td>
                                      <td data-ng-repeat="collection_content in collection.contents">
                                          <a href="#/collections/edit/{{collection_content.language_id}}/{{collection.id}}" ng-if="collection_content.content_id" class="btn btn-success btn-xs btn-block" type="button">
                                              <i class="glyphicon glyphicon-pencil"></i>&nbsp;<?php echo __("Modifica"); ?>
                                          </a>
                                          <a
                                              href="#/collections/edit/{{collection_content.language_id}}/{{collection.id}}"
                                              ng-if="!collection_content.content_id"
                                              tooltip-append-to-body="true"
                                              tooltip="<?php echo __("Il contenuto di questa categoria ancora non esiste, clicca qui per crearlo."); ?>"
                                              tooltip-placement="top"
                                              class="btn btn-primary btn-xs btn-block"
                                              type="button"><i class=" glyphicon glyphicon-pencil"></i>&nbsp;<?php echo __("Crea"); ?>
                                          </a>
                                      </td>
                                      <td class="text-right">
                                          <span
                                              tooltip-append-to-body="true"
                                              tooltip="<?php echo __("Categorie figlie"); ?>"
                                              tooltip-placement="top">
                                                <a href="#/collections/{{collection.id}}" class="btn btn-primary btn-xs" type="button"><i class="fa fa-list"></i></a>
                                          </span>
                                          <button class="btn btn-danger btn-xs" type="button" confirm-callback="delete(collection.id)" confirm-message="<?php echo __("Sei sicuro di voler eliminare questa collezione?"); ?>" confirm-modal><i class=" glyphicon glyphicon-trash"></i></button>
                                      </td>
                                  </tr>
                              </tbody>
                          </table>
                      </div>

                      <footer class="table-footer">
                          <div class="row">
                              <div class="col-md-6 page-num-info">
                                    <span>
                                        <?php echo __("Elementi per pagina"); ?>
                                        <select data-ng-model="numPerPage" data-ng-options="num for num in numPerPageOpt" data-ng-change="onNumPerPageChange()">
                                        </select>
                                    </span>
                              </div>
                              <div class="col-md-6 text-right pagination-container">
                                  <pagination class="pagination-sm" ng-model="currentPage" total-items="totalPages" max-size="4" ng-change="select(currentPage)" items-per-page="numPerPage" rotate="false" boundary-links="true"></pagination>
                              </div>
                          </div>
                      </footer>

                  </div>
              </section>
              <!-- new code -->
        	</div>
        </div>
    </div>
</div>