<div class="modal-header">
    <h3 class="modal-title"><?php echo __("Aggiungi nuova collezione"); ?></h3>
</div>
<div class="modal-body">
    <form name="addCollection" class="form-horizontal form-validation">
        <div class="form-group">
            <label for="" class="col-sm-4"><?php echo __("Nome"); ?></label>
            <div class="col-sm-8">
                <input type="text" class="form-control" required data-ng-model="product_category.title" data-ng-change="updateURL()">
                <span>URL: <a contenteditable data-ng-model="product_category.uri"></a></span>
            </div>
        </div>
        <div class="form-group">
            <label for="" class="col-sm-4"><?php echo __("Collezione padre"); ?></label>
            <div class="col-sm-8">
                <select class="form-control" data-ng-options="collection_el.id as collection_el.name for collection_el in collections" data-ng-model="product_category.parent_id">
                    <option value=""><?php echo __("Seleziona una collezione"); ?></option>
                </select>
            </div>
        </div>
    </form>
</div>
<div class="modal-footer">
    <button class="btn btn-primary" ng-disabled="addCollection.$invalid" ng-click="save()"><?php echo __("Salva"); ?></button>
    <button class="btn btn-warning" ng-click="cancel()"><?php echo __("Annulla"); ?></button>
</div>