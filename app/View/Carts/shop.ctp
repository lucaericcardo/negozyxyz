<div id="widget-container" data-ng-controller="cartCtrl">
    <div id="myDropdown" class="dropdown pull-left">
        <a class="dropdown-toggle" type="button" data-toggle="dropdown" aria-expanded="true" href="#"><img class="img-circle img30_30" alt="" src="<?php echo $image; ?>"></a>
        <ul class="dropdown-menu pull-right" role="menu">
            <li role="presentation"><a target="_parent" role="menuitem" tabindex="-1" href="http://my.negozy.com/account/#/">Il tuo account</a></li>
            <li role="presentation"><a target="_parent" role="menuitem" tabindex="-1" href="http://my.negozy.com/account/#/orders">I tui ordini</a></li>
            <li role="presentation"><a target="_parent" role="menuitem" tabindex="-1" href="http://my.negozy.com/account/#/orders">Le tue richieste</a></li>
        </ul>
    </div>
    <div id="cart" class="dropdown pull-left" on-toggle="toggled(open)" ng-click="toggleDropdown($event)">
        <button id="cart" class="btn btn-default btn-xs pull-right dropdown-toggle"><span class="glyphicon glyphicon-shopping-cart"></span> <span class="label label-primary">{{cart.total_quantity}}</span></button>
        <div class="dropdown-menu pull-right" role="menu">
            <div class="container" style="width: 450px">
                <div class="row">
                    <div class="col-xs-12">

                        <div data-ng-repeat="product in cart.products" class="row">
                            <div class="col-xs-2"><img style="max-height: 80px" class="img-responsive" ng-src="{{product.image}}">
                            </div>
                            <div class="col-xs-4">
                                <h6 class="product-name">{{product.name}}</h6>
                            </div>
                            <div class="col-xs-6">
                                <div class="col-xs-6 text-right">
                                    <h6><strong>{{product.unit_price}}<span class="text-muted">x</span></strong></h6>
                                </div>
                                <div class="col-xs-4">
                                    <input type="text" data-ng-change="updateProduct(product)" class="form-control input-sm" data-ng-model="product.quantity">
                                </div>
                                <div class="col-xs-2">
                                    <button data-ng-click="removeProduct(product)" type="button" class="btn btn-link btn-xs">
                                        <span class="glyphicon glyphicon-trash"> </span>
                                    </button>
                                </div>
                            </div>
                            <hr>
                        </div>
                        <div class="row text-center">
                            <div class="col-xs-9">
                                <h4 class="text-right">Totale <strong>{{cart.total}}</strong></h4>
                            </div>
                        </div>
                        <div class="row">
                            <div class="text-center">
                                <div class="col-xs-12">
                                    <button type="button" class="btn btn-success btn-block">
                                        Checkout
                                    </button>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

</div>