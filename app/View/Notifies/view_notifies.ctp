<div class="lapagina">
	<nav class="navbar navbar-default navbar-static-top" role="navigation">
    	<div class="navbar-header">
    		<div class="navbar-center">
                <!--<img class="navbar-brand" alt="" ng-src="{{logo}}">-->
            </div>
            <ol class="breadcrumb nav navbar-nav">
                <li class="active"><?php echo __("Le tue notifiche"); ?></li>
            </ol>
        </div>
	</nav>
	
    <div class="page page-services">
        <div class="row">
        	<div class="col-lg-10 col-lg-offset-1">
                <ul class="list-group">
                  <!-- ngRepeat: notify in notifies -->
                  <li class="list-group-item"> <a class="media" href="javascript:;"> <span class="pull-left media-icon"> <span data-ng-class="notify.class_notify" class="square-icon sm bg-danger"><i data-ng-class="notify.icon_notify" class="fa fa-thumbs-o-down"></i></span> </span>
                    <div class="media-body"> <span class="block ng-binding">A Luca Vignali non piace il tuo post </span> <span class="text-muted block ng-binding">15/11/2014 @ 9:50AM</span> </div>
                    </a> </li>
                  <!-- end ngRepeat: notify in notifies -->
                  <li class="list-group-item"> <a class="media" href="javascript:;"> <span class="pull-left media-icon"> <span data-ng-class="notify.class_notify" class="square-icon sm bg-success"><i data-ng-class="notify.icon_notify" class="fa fa-thumbs-o-up"></i></span> </span>
                    <div class="media-body"> <span class="block ng-binding">A Luca Vignali piace il tuo post </span> <span class="text-muted block ng-binding">14/11/2014 @ 6:09PM</span> </div>
                    </a> </li>
                  <!-- end ngRepeat: notify in notifies -->
                  <li class="list-group-item"> <a class="media" href="javascript:;"> <span class="pull-left media-icon"> <span data-ng-class="notify.class_notify" class="square-icon sm bg-success"><i data-ng-class="notify.icon_notify" class="fa fa-thumbs-o-up"></i></span> </span>
                    <div class="media-body"> <span class="block ng-binding">A Alberto Pacini piace il tuo post il mio post</span> <span class="text-muted block ng-binding">14/11/2014 @ 5:47PM</span> </div>
                    </a> </li>
                  <!-- end ngRepeat: notify in notifies -->
                  <li class="list-group-item"> <a class="media" href="javascript:;"> <span class="pull-left media-icon"> <span data-ng-class="notify.class_notify" class="square-icon sm bg-success"><i data-ng-class="notify.icon_notify" class="fa fa-thumbs-o-up"></i></span> </span>
                    <div class="media-body"> <span class="block ng-binding">A Luca Vignali piace il tuo post il mio post</span> <span class="text-muted block ng-binding">14/11/2014 @ 2:48PM</span> </div>
                    </a> </li>
                  <!-- end ngRepeat: notify in notifies -->
                  <li class="list-group-item"> <a class="media" href="javascript:;"> <span class="pull-left media-icon"> <span data-ng-class="notify.class_notify" class="square-icon sm bg-info"><i data-ng-class="notify.icon_notify" class="fa fa-align-justify"></i></span> </span>
                    <div class="media-body"> <span class="block ng-binding">A Luca Vignali ha commentato il tuo post il mio po</span> <span class="text-muted block ng-binding">14/11/2014 @ 2:29PM</span> </div>
                    </a> </li>
                  <!-- end ngRepeat: notify in notifies -->
                </ul>
            </div>
        </div>
    </div>
</div>

