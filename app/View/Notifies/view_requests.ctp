<div class="lapagina" data-ng-controller="ShopNotifiesCtrl">
	<nav class="navbar navbar-default navbar-static-top" role="navigation">
    	<div class="navbar-header">
    		<div class="navbar-center">
                <!--<img class="navbar-brand" alt="" ng-src="{{logo}}">-->
            </div>
            <ol class="breadcrumb nav navbar-nav">
                <li class="active"><?php echo __("Le tue notifiche"); ?></li>
            </ol>
        </div>
	</nav>
	
    <div class="page page-services">
        <div class="row">
        	<div class="col-lg-10 col-lg-offset-1">
            	<div class="panel panel-default">
                	<div class="panel-body">
                        <tabset class="ui-tab">
                            <tab heading="<?php echo __('Aperte'); ?>">
                                <h2 data-ng-hide="open_notifies.length"><?php echo __('Nessuna notifica attualmente presente'); ?></h2>
                                <div class="table-responsive" data-ng-show="open_notifies.length">
                                    <!--<table class="table table-hover">
                                        <thead>
                                            <tr>
                                                <th><?php echo __('Notifica'); ?></th>
                                                <th><?php echo __('Data e Ora'); ?></th>
                                                <th style="width:230px"><?php echo __('Azione'); ?></th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                        <tr data-ng-repeat="notify in open_notifies">
                                            <td>
                                                <div class="media">
                                                    <a class="pull-left" href="javascript:;">
                                                        <img class="img-circle img30_30" alt="" data-ng-src="{{notify.product_thumb}}">
                                                    </a>
                                                    <div class="media-body">
                                                        <p>{{notify.message}}</p>
                                                    </div>
                                                </div>
                                            </td>
                                            <td>{{notify.created.sec*1000 | date:'dd/MM/yyyy h:mma'}}</td>
                                            <td data-ng-show="notify.type == 0 && !notify.response">
                                                <a class="btn btn-sm btn-danger" data-ng-click="available(notify, notify.id, notify.negozyation_id, false)">Non è disponibile</a>
                                                <a class="btn btn-sm btn-success" data-ng-click="available(notify, notify.id, notify.negozyation_id, true)">E' disponibile</a>
                                            </td>
                                            <td data-ng-show="notify.type == 1 && !notify.response">
                                                <a class="btn btn-sm btn-success" data-ng-click="buyNow(notify.product_id, notify.negozyation_id)">Compra</a>
                                            </td>
                                            <td data-ng-show="notify.response"><?php echo __('Risposta inviata'); ?></td>
                                        </tr>
                                        </tbody>
                                    </table>-->
                                    <table class="table table-hover">
                                        <thead>
                                        <tr>
                                            <th><?php echo __('Prodotto'); ?></th>
                                            <th><?php echo __('Quantità'); ?></th>
                                            <th><?php echo __('Shop'); ?></th>
                                            <th><?php echo __('Data'); ?></th>
                                            <th><?php echo __('Azioni'); ?></th>
                                        </tr>
                                        </thead>
                                        <tbody>
                                        <tr data-ng-repeat="request in open_notifies">
                                            <td>
                                                <div class="media">
                                                    <a class="pull-left" href="javascript:;">
                                                        <img class="img-circle img30_30" alt="" data-ng-src="{{request.product_thumb}}">
                                                    </a>
                                                    <div class="media-body">
                                                        <p>{{request.product_name}}</p>
                                                    </div>
                                                </div>
                                            </td>
                                            <td>{{request.product_quantity}}</td>
                                            <td>{{request.to}}</td>
                                            <td>{{request.created*1000 | date:'dd/MM/yyyy  h:mma'}}</td>
                                            <td>
                                                <div data-ng-show="request.actions == 'take_disponibility'">
                                                    <a class="btn btn-sm btn-danger" data-ng-click="available(request, request.notify_id, request.id, false)">Non è disponibile</a>
                                                    <a class="btn btn-sm btn-success" data-ng-click="available(request, request.notify_id, request.id, true)">E' disponibile</a>
                                                </div>
                                                <td data-ng-show="request.response">{{request.actions}}</td>
                                            </td>
                                        </tr>
                                        </tbody>
                                    </table>

                                </div>
                            </tab>
                            <tab heading="<?php echo __('Chiuse'); ?>">
                                <h2 data-ng-hide="closed_notifies.length"><?php echo __('Nessuna notifica attualmente presente'); ?></h2>
                                <div class="table-responsive" data-ng-show="closed_notifies.length">
                                    <!--<table class="table table-hover">
                                        <thead>
                                            <tr>
                                                <th><?php /*echo __('Notifica'); */?></th>
                                                <th><?php /*echo __('Data e Ora'); */?></th>
                                                <th style="width:230px"><?php /*echo __('Azioni'); */?></th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                        <tr data-ng-repeat="c_notify in closed_notifies | orderBy:created:reverse">
                                            <td>
                                                <div class="media">
                                                    <a class="pull-left" href="javascript:;">
                                                        <img class="img-circle img30_30" alt="" data-ng-src="{{c_notify.product_thumb}}">
                                                    </a>
                                                    <div class="media-body">
                                                        <p>{{c_notify.message}}</p>
                                                    </div>
                                                </div>
                                            </td>
                                            <td>{{c_notify.created.sec*1000 | date:'dd/MM/yyyy h:mma'}}</td>
                                            <td><?php /*echo __('Nessuna azione disponibile'); */?></td>
                                        </tr>
                                        </tbody>
                                    </table>-->
                                    <table class="table table-hover">
                                        <thead>
                                        <tr>
                                            <th><?php echo __('Prodotto'); ?></th>
                                            <th><?php echo __('Quantità'); ?></th>
                                            <th><?php echo __('Shop'); ?></th>
                                            <th><?php echo __('Data di chiusura'); ?></th>
                                            <th><?php echo __('Azioni'); ?></th>
                                        </tr>
                                        </thead>
                                        <tbody>
                                        <tr data-ng-repeat="c_requests in closed_notifies | orderBy:created:reverse">
                                            <td>
                                                <div class="media">
                                                    <a class="pull-left" href="javascript:;">
                                                        <img class="img-circle img30_30" alt="" data-ng-src="{{c_requests.product_thumb}}">
                                                    </a>
                                                    <div class="media-body">
                                                        <p>{{c_requests.product_name}}</p>
                                                    </div>
                                                </div>
                                            </td>
                                            <td>{{c_requests.product_quantity}}</td>
                                            <td>{{c_requests.to}}</td>
                                            <td>{{c_requests.data_closed*1000 | date:'dd/MM/yyyy  h:mma'}}</td>
                                            <td>{{c_requests.actions}}</td>
                                        </tr>
                                        </tbody>
                                    </table>
                                </div>
                            </tab>
                        </tabset>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
