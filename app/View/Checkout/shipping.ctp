<div class="container main-container headerOffset">
  <div class="row">
      <?php echo $this->element("checkout/topbar"); ?>
  </div>
  <div class="row">
    <div class="col-lg-9 col-md-9 col-sm-12">
      <div class="row userInfo">
        <div class="col-xs-12 col-sm-12">
          <div class="w100 clearfix">
              <?php echo $this->element("checkout/stepper"); ?>
            <!--/orderStep--> 
          </div>
          <div class="w100 clearfix">
            <div class="row userInfo">
              <div class="col-lg-12">
                  <h2 class="block-title-2"> <?php echo __("Spedizione"); ?> </h2>
                  <p><?php echo __("Seleziona il tuo metodo di spedizione"); ?></p>
                  <hr />
              </div>
                <?php
                echo $this->Form->create("Shipping");
                ?>
              <div class="col-xs-12 col-sm-12">
                <div class="w100 row">
                  <div class="form-group col-lg-12 col-sm-12 col-md-12 -col-xs-12">
                      <?php
                      if(count($shipping_options)) {
                          ?>
                          <table style="width:100%" class="table-bordered table tablelook2">
                              <tbody>
                              <tr>
                                  <td><?php echo __("Vettore"); ?> </td>
                                  <td><?php echo __("Informazioni"); ?></td>
                                  <td><?php echo __("Prezzo"); ?></td>
                              </tr>
                              <?php
                              $i = 1;
                              foreach ($shipping_options as $shipment) {
                                  if (isset($order['shipping']['id'])) {
                                      $checked = ($order['shipping']['id'] == $shipment['id']) ? 'checked' : '';
                                  } else {
                                      $checked = ($i == 1) ? 'checked' : '';
                                  }

                                  ?>
                                  <tr>
                                      <td>
                                          <label>
                                              <input <?php echo $checked; ?> type="radio" name="shipping"
                                                                             value="<?php echo $shipment['id']; ?>">
                                              <i class="fa fa-truck fa-2x"></i>
                                          </label>
                                      </td>
                                      <td><?php echo $shipment['name'] . " <br >" . "(" . $shipment['delivery'] . " " . __("giorni") . " )"; ?></td>
                                      <td><?php echo $this->Number->currency($shipment['price'], $order['shop']['currency']); ?></td>
                                  </tr>
                                  <?php
                                  $i++;
                              }
                              ?>
                              </tbody>
                          </table>
                      <?php
                      } else {
                      ?>
                          <div class="alert alert-danger">
                              <?php echo __("Siamo spiacenti, il venditore non effettua spedizioni verso il tuo paese."); ?>
                          </div>
                      <?php
                      }
                      ?>
                  </div>
                </div>
                <div class="cartFooter w100">
                  <div class="box-footer">
                    <div class="pull-left"> <a class="btn btn-default" href="/checkout/address/<?php echo $token; ?>"> <i class="fa fa-arrow-left"></i> &nbsp; <?php echo __("Indirizzo"); ?>  </a> </div>
                    <?php
                    if(count($shipping_options)) {
                        ?>
                        <div class="pull-right">
                            <button type="submit" class="btn btn-primary btn-small "> <?php echo __("Continua"); ?>
                                &nbsp; <i class="fa fa-arrow-circle-right"></i></button>
                        </div>
                    <?php
                    }
                    ?>
                  </div>
                </div>
              </div>
                <?php
                echo $this->Form->end();
                ?>
            </div>
          </div>
        </div>
      </div>
    </div>
    <!--/row end-->
    <div class="col-lg-3 col-md-3 col-sm-12 rightSidebar">
        <?php echo $this->element("checkout/box"); ?>
    </div>
    <!--/rightSidebar-->
  </div>
  <!--/row-->
  <div style="clear:both"></div>
</div>
<!-- /main-container -->
