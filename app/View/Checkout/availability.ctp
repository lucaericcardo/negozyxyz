<div class="container main-container headerOffset">
  <div class="row">
    <div class="col-lg-12 col-md-12  col-sm-12 text-center">
      <h1 class="section-title-inner text-warning"><img src="<?php echo $order['shop']['logo']; ?>" alt="<?php  echo $order['shop']['name']; ?>" style="max-height:50px">  <?php  echo $order['shop']['name']; ?></h1>
      <h3><?php echo __("Prima di effettuare l'acquisto devi verificare la disponibilità del prodotto.");?></h3>
      <div class="divider"></div>
      <div class="row">
      	<div class="col-xs-12 col-md-6 col-md-offset-3">
          <div class="media">
            <a class="pull-left"><img src="<?php echo $order['product']['image']; ?>" alt="<?php echo $order['product']['name']; ?>" style="max-height:200px; border:solid 1px #ccc"></a>
            <div class="media-body">
                <h4 class="media-heading"><?php echo $order['product']['name']; ?></h4>
                <?php echo $this->Number->currency($order['product']['price_total'], $order['shop']['currency']); ?>
            </div>
          </div>
      	</div>
      </div>
      <div class="divider"></div>
      <a class="btn btn-default" href="http://<?php echo $order['shop']['url']; ?>.negozy.com">Annulla e torna al negozio</a>
      <a class="btn btn-primary" href="/checkout/process_availability/<?php echo $token; ?>">Richiedi la disponibiltà al venditore</a>
    </div>
  </div> <!--/row-->
  <div style="clear:both"></div>
</div>