<div class="container main-container headerOffset">
  <div class="row">
      <?php echo $this->element("checkout/topbar"); ?>
  </div>
  <div class="row">
    <div class="col-lg-9 col-md-9 col-sm-12">
      <div class="row userInfo">
          <?php
          echo $this->Form->create("Shipping");
          ?>
        <div class="col-xs-12 col-sm-12">
          <div class="w100 clearfix">
              <?php echo $this->element("checkout/stepper"); ?>
            <!--orderStep--> 
          </div>
          <div class="w100 clearfix">
            <div class="row userInfo">
              <div class="col-lg-12">
                <h2 class="block-title-2"> <?php echo __("Metodo di pagamento"); ?> </h2>
                <p><?php echo __("Seleziona una metodologia di pagamento"); ?></p>
                <hr>
              </div>

              <div class="col-xs-12 col-sm-12">
                  <?php
                  $i = 1;
                  if(count($payment_options)){
                      foreach($payment_options as $payment_option) {
                          if (isset($order['payment']['id'])) {
                              $checked = ($order['payment']['id'] == $payment_option['id']) ? 'checked' : '';
                          } else {
                              $checked = ($i == 1) ? 'checked' : '';
                          }
                          $price = ($payment_option['price']) ? $this->Number->currency($payment_option['price'], $order['shop']['currency']) : '';
                          ?>
                          <div class="w100 clearfix ng-scope">
                              <div class="col-xs-12">
                                  <div class="panel panel-default">
                                      <div class="panel-heading">
                                          <h3 class="panel-title">
                                              <label>
                                                  <input <?php echo $checked; ?> type="radio" name="payment"
                                                                                 value="<?php echo $payment_option['id']; ?>">
                                                  <strong><?php echo $payment_option['name'] . " " . (($price) ? '+ ' . $price : ''); ?></strong>
                                              </label>
                                          </h3>
                                      </div>
                                  </div>
                              </div>
                          </div>
                          <?php
                          $i++;
                      }
                  } else {
                    ?>
                      <div class="alert alert-danger">
                          <?php echo __("Siamo spiacenti, il venditore non è abilitato a ricevere pagamenti."); ?>
                      </div>
                  <?php
                  }
                  ?>
                <!--/row-->
              </div>

            </div>
          </div>
          <!--/row end-->
          <div class="cartFooter w100">
            <div class="box-footer">
              <div class="pull-left"> <a class="btn btn-default" href="/checkout/shipping/<?php echo $token; ?>"> <i class="fa fa-arrow-left"></i> &nbsp; <?php echo __("Spedizione"); ?> </a> </div>
                <?php
                if(count($payment_options)) {
                    ?>
                    <div class="pull-right">
                        <button type="submit" class="btn btn-primary btn-small "> <?php echo __("Continua"); ?> &nbsp;
                            <i class="fa fa-arrow-circle-right"></i></button>
                    </div>
                <?php
                }
                ?>
            </div>
          </div>
        </div>
          <?php
          echo $this->Form->end();
          ?>
        <!--/ cartFooter -->
      </div>
    </div>
    <!--/row end-->
    
    <div class="col-lg-3 col-md-3 col-sm-12 rightSidebar">
        <?php echo $this->element("checkout/box"); ?>
    </div>
    <!--/rightSidebar--> 
    
  </div>
  <!--/row-->
  
  <div style="clear:both"></div>
</div>