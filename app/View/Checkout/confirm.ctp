<div class="container main-container headerOffset">
    <div class="row">
        <?php echo $this->element("checkout/topbar"); ?>
    </div>
    <div class="row">
        <div class="col-lg-9 col-md-9 col-sm-12">
            <div class="row userInfo">
                <?php
                echo $this->Form->create("Shipping");
                ?>
                <div class="col-xs-12 col-sm-12">
                    <div class="w100 clearfix">
                        <?php echo $this->element("checkout/stepper"); ?>
                        <!--orderStep-->
                    </div>
                    <div class="w100 clearfix">
                    	<div class="row userInfo">
                            <div class="col-sm-6">
                            	<h2 class="block-title-2"> <?php echo __("Indirizzo di spedizione"); ?> </h2>
                                <div class="well">
                                	<dl>
                                      <dt><?php echo $address['first_name'] . " " . $address['last_name']; ?></dt>
                                      
                                      <dt><?php echo $address['address']; ?></dt>
                                      
                                      <dt><?php echo $address['postal_code']; ?></dt>
                                      
                                      <dt><?php echo $address['city']; ?></dt>
                                      
                                      <dt><?php echo $address['province']; ?></dt>
                                      
                                      <dt><?php echo $country['name']; ?></dt>
                                      
                                      <dt><?php echo $address['phone_number']; ?></dt>
                                    </dl>
                                </div>
                            </div>
                            <div class="col-sm-6">
                            	<div class="row">
                                    <div class="col-lg-12">
                                        <h2 class="block-title-2"> <?php echo __("Metodo di spedizione"); ?> </h2>
                                        <div class="well">
                                            <dl>
                                              <dt><?php echo $order['shipping']['name']." (".$order['shipping']['start_day']." - ".$order['shipping']['end_day']." giorni)"; ?></dt>
                                              <dd><?php echo $this->Number->currency($order['shipping']['price_total'], $order['shop']['currency']); ?></dd>
                                            </dl>
                                        </div>
                                    </div>
                                    <div class="col-lg-12">
                                        <h2 class="block-title-2"> <?php echo __("Metodo di pagamento"); ?> </h2>
                                        <div class="well">
                                            <dl>
                                              <dt><?php echo $order['payment']['label']; ?></dt>
                                              <dd><?php echo $order['payment']['value']; ?></dd>
                                                <?php
                                                if($order['payment']['price_total']) {
                                                    ?>
                                                    <dd><?php echo $this->Number->currency($order['payment']['price_total'], $order['shop']['currency']); ?></dd>
                                                <?php
                                                }
                                                ?>
                                            </dl>
        
                                        </div>
                                    </div>
                            	</div>
                            </div>
                        </div>
                        <div class="row userInfo">
                            <div class="col-lg-12">
                                <h2 class="block-title-2"> <?php echo __("Riepilogo ordine"); ?> </h2>
                            </div>
                            <div class="col-xs-12 col-sm-12">
                                <div class="cartContent w100 checkoutReview ">
                                    <table class="cartTable table-responsive table-bordered"   style="width:100%">
                                      <tbody>
                                        <tr class="CartProduct cartTableHeader">
                                          <th style="width:15%"> <?php echo __("Foto"); ?> </th>
                                          <th class="checkoutReviewTdDetails"  ><?php echo __("Prodotto"); ?></th>
                                          <th style="width:20%" ><?php echo __("Prezzo Unitario"); ?></th>
                                          <th class="hidden-xs" style="width:15%"><?php echo __("Quantit&agrave;"); ?></th>
                                          <th style="width:15%"><?php echo __("Totale"); ?></th>
                                        </tr>
                                        <tr class="CartProduct">
                                          <td  class="CartProductThumb"><div> <a><img src="<?php echo $order['product']['image']; ?>"></a> </div></td>
                                          <td ><div class="CartDescription">
                                              <h4><?php echo $order['product']['name']; ?></h4>
                                            </div></td>
                                          <td class="delete"><div class="price "><?php echo $this->Number->currency($order['product']['unit']['price_total'], $order['shop']['currency']); ?> </div></td>
                                          <td class="hidden-xs"><?php echo $order['product']['quantity']; ?></td>
                                          <td class="price"><?php echo $this->Number->currency($order['product']['price_total'], $order['shop']['currency']); ?></td>
                                        </tr>
                                        
                                      </tbody>
                                    </table>
                                  </div>

                                  <!--cartContent-->
                                  
                                  <div class="w100 costDetails">
                                    <div class="table-block" id="order-detail-content">
                                      <table class="std table table-striped" id="cart-summary">
                                      <tr class="cart-total-price ">
                                          <td><?php echo __("Totale imponibile prodotti"); ?></td>
                                          <td  class="price"><?php echo $this->Number->currency($order['product']['price_no_tax'], $order['shop']['currency']); ?> </td>
                                      </tr>
                                        <tr style="" >
                                          <td><?php echo __("Totale imponibile spedizioni"); ?></td>
                                          <td  class="price"><?php echo $this->Number->currency($order['shipping']['price_no_tax'], $order['shop']['currency']); ?></td>
                                        </tr>
                                        <tr >
                                          <td><?php echo __("Totale IVA"); ?></td>
                                          <td id="total-tax" class="price"><?php echo $this->Number->currency($order['total_tax'], $order['shop']['currency']); ?></td>
                                        </tr>
                                        <tr >
                                          <td><?php echo __("Extra"); ?></td>
                                          <td id="total-tax" class="price"><?php echo $this->Number->currency($order['payment']['price_total'], $order['shop']['currency']); ?></td>
                                        </tr>
                                        <tr >
                                          <td > <?php echo __("Totale"); ?> </td>
                                          <td id="total-price" class="price"><?php echo $this->Number->currency($order['total'], $order['shop']['currency']); ?> </td>
                                        </tr>
                                        <tbody>
                                        </tbody>
                                      </table>
                                    </div>
                                  </div>
                                  <!--/costDetails-->
                            </div>

                        </div>
                    </div>
                    <!--/row end-->
                    <div class="cartFooter w100">
                        <div class="box-footer">
                            <div class="pull-left"> <a class="btn btn-default" href="/checkout/payments/<?php echo $token; ?>"> <i class="fa fa-arrow-left"></i> &nbsp; <?php echo __("Pagamento"); ?> </a> </div>
                            <div class="pull-right"> <a class="btn btn-primary btn-small" href="/checkout/process/<?php echo $token; ?>"> <?php echo __("Conferma"); ?> &nbsp; <i class="fa fa-arrow-circle-right"></i> </a> </div>
                        </div>
                    </div>
                </div>
                <?php
                echo $this->Form->end();
                ?>
                <!--/ cartFooter -->
            </div>
        </div>
        <!--/row end-->

        <div class="col-lg-3 col-md-3 hidden-sm hidden-xs rightSidebar">
            <?php echo $this->element("checkout/box"); ?>
        </div>
        <!--/rightSidebar-->

    </div>
    <!--/row-->

    <div style="clear:both"></div>
</div>