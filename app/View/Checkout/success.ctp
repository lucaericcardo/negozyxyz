<div class="container main-container headerOffset">
      <div class="row">
        <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
          <h1>Ordine completato correttamente </h1>
          <h5>Ti ringraziamo della fiducia accordata, non appena riceveremo il pagamento, procederemo all'evasione dell'ordine.</h5>
           <div class="divider"></div>
            <?php
            if($order_pending) {
                ?>
                <div class="alert alert-danger" role="alert">
                    <h3 style="margin-top:10px"><strong><i class="fa fa-exclamation-triangle"></i>Importante!</strong>
                        Per poter evadere l'ordine è necessario cliccare sul link ricevuto per email</h3>
                </div>
                <?php
            }
            ?>
          <a class="btn btn-primary" href="<?php echo $site_url; ?>">Torna allo shop </a>
          <div class="divider"></div>
          <img src="//cdn.negozy.com/negozy/Img/conferma-ordine.jpg" class="img-responsive" />
        </div>
      </div>
  	<div style="clear:both">  </div>
</div><!-- /.main-container -->