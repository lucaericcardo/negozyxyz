<div class="container main-container headerOffset">
  <div class="row">
    <div class="col-lg-9 col-md-9 col-sm-7">
        <?php echo $this->element("checkout/topbar"); ?>
    </div>
  </div>
  <div class="row">
    <div class="col-lg-9 col-md-9 col-sm-12">
      <div class="row userInfo">
        <div class="col-xs-12 col-sm-12">
          <div class="w100 clearfix">
              <?php echo $this->element("checkout/stepper"); ?>
          </div>
            <?php
            echo $this->Form->create("UserAddress");
            ?>
          <div class="w100 clearfix">
            <div class="row userInfo">
              <div class="col-lg-12">
                  <h2 class="block-title-2"> <?php echo __("Indirizzo di spedizione"); ?> </h2>
                  <p><?php echo __("Inserici l'indirizzo per la spedizione"); ?></p>
                  <hr />
              </div>
                <div class="col-md-12 col-lg-6">
                    <?php
                    echo $this->Form->input("first_name",array(
                        "label" => __("Nome")." *",
                        "placeholder" => __("Nome"),
                        "tabindex" => "1"
                    ));
                    echo $this->Form->input("address",array(
                        "label" => __("Indirizzo")." *",
                        "placeholder" => __("Indirizzo"),
                        "tabindex" => "3"
                    ));
                    echo $this->Form->input("city",array(
                        "label" => __("Città")." *",
                        "placeholder" => __("Città"),
                        "tabindex" => "5"
                    ));
                    echo $this->Form->input("country_id",array(
                        "label" => __("Paese")." *",
                        "options" => $countries,
                        "tabindex" => "7",
                        "default" => 115
                    ));
                    ?>
                </div>
                <div class="col-md-12 col-lg-6">
                    <?php
                    echo $this->Form->input("last_name",array(
                        "label" => __("Cognome")." *",
                        "placeholder" => __("Cognome"),
                        "tabindex" => "2"
                    ));

                    echo $this->Form->input("postal_code",array(
                        "label" => __("CAP")." *",
                        "placeholder" => __("Codice postale"),
                        "tabindex" => "4"
                    ));

                    echo $this->Form->input("province",array(
                        "label" => __("Provincia")." *",
                        "placeholder" => __("Provincia"),
                        "tabindex" => "6"
                    ));

                    echo $this->Form->input("phone_number",array(
                        "label" => __("Telefono")." *",
                        "placeholder" => __("Telefono"),
                        "tabindex" => "8"
                    ));
                    ?>
                </div>
            </div>
          </div>
          <div class="cartFooter w100">
            <div class="box-footer">
                <div class="pull-left"> <a class="btn btn-default" href="http://<?php echo $order['shop']['url']; ?>.negozy.com""> <i class="fa fa-arrow-left"></i> &nbsp; <?php echo __("Torna allo shop"); ?> </a> </div>
              <div class="pull-right"> <button type="submit" class="btn btn-primary btn-small "> <?php echo __("Continua"); ?> &nbsp; <i class="fa fa-arrow-circle-right"></i> </button> </div>
            </div>
          </div>
            <?php
            echo $this->Form->end();
            ?>
        </div>
      </div>
    </div>
    <div class="col-lg-3 col-md-3 col-sm-12 rightSidebar">
      <?php echo $this->element("checkout/box"); ?>
    </div>
  </div> <!--/row-->
  <div style="clear:both"></div>
</div>