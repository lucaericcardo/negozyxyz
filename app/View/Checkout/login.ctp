<div class="container main-container headerOffset">
  <div class="row">
  
    <div class="col-lg-12 col-md-12  col-sm-12">
    
      <h1 class="section-title-inner text-center"><span><i class="fa fa-lock"></i> Autenticazione necessaria</span></h1>
      
      <div class="row userInfo">
      <div class="divider"></div>
      	<div class="col-xs-12 col-sm-6 col-md-4 col-md-offset-2">
          <div class="sns-signin">
	          <a href="http://it.negozy.com/users/facebook_signup" class="facebook"><i class="fa fa-facebook"></i><span>Entra con Facebook</span></a>
          </div>
        </div>
        <div class="col-xs-12 col-sm-6 col-md-4">
          <div class="sns-signin">
	          <a href="http://it.negozy.com/users/google_signup" class="google-plus"><i class="fa fa-google-plus"></i><span>Entra con Google+</span></a>
          </div>
          <div class="divider"></div>
        </div>
      
        <div class="col-xs-12 col-sm-6 col-md-4 col-md-offset-1">
	        <div class="divider"></div>
          <h2 class="block-title-2"> Inserisci i tuoi dati per proseguire l'ordine </h2>
            <?php
                echo $this->Form->create("User",array(
                    'url' => 'http://it.negozy.com/signin'
                ));
                echo $this->Form->input("Singup.first_name",array(
                        "label" => __('Nome'),
                        'required' => true,
                        'placeholder' => __('Nome'),
                    )
                );
                echo $this->Form->input("Singup.last_name",array(
                        "label" => __('Cognome'),
                        'required' => true,
                        'placeholder' => __('Cognome'),
                    )
                );
                echo $this->Form->input("Singup.email",array(
                        "label" => false,
                        'required' => true,
                        'placeholder' => 'Email',
                    )
                );
                echo $this->Form->input("Singup.password",array(
                    "label" => 'Password',
                    'required' => true,
                    'placeholder' => 'password',
                ));
                ?>
                <button type="submit" class="btn btn-primary"><i class="fa fa-user"></i> Prosegui</button>
          <?php
          echo $this->form->end();
          ?>
        </div>
        
        
        <div class="col-xs-12 col-sm-6 col-md-4 col-md-offset-2">
        <div class="divider"></div>
          <h2 class="block-title-2"><span>Effettua il login e prosegui con l'ordine</span></h2>
            <?php
            echo $this->Form->create('User',array(
                'url' => 'http://it.negozy.com/signin'
            ));
            ?>
            <div class="form-group">
                <?php
                echo $this->Session->flash();
                echo $this->Form->input("Login.email",array(
                        "label" => "Email",
                        'required' => true,
                        'placeholder' => 'Email',
                    )
                );
                echo $this->Form->input("Login.password",array(
                    "label" => "Password",
                    "div" => array(
                        "class" => "pw-wrap"
                    ),
                    'required' => true,
                    'placeholder' => 'password',
                ));
                ?>

            </div>
            <button type="submit" class="btn btn-primary"><i class="fa fa-sign-in"></i> Login</button>
            <?php
            echo $this->Form->end();
            ?>
        </div>
        
        
        
      </div> <!--/row end--> 
      
    </div>
  </div> <!--/row--> <!--/row-->
  
  <div style="clear:both"></div>
</div>