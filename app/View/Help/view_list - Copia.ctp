<div class="lapagina">
  <nav class="navbar navbar-default navbar-static-top" role="navigation">
    <div class="navbar-header">
      <ul class="nav navbar-nav">
        <li>
          <ol class="breadcrumb">
            <li class="active"><a>Aiuto</a></li>
          </ol>
        </li>
      </ul>
    </div>
  </nav>
  <div class="page page-table">
    <div class="row">
      <div class="col-lg-10 col-lg-offset-1 col-md-12">
        <accordion close-others="oneAtATime" class="ui-accordion ui-accordion-success aiuto"> 
          
          <!-- singolo accordion -->
          <accordion-group heading="Cosa comporta la registrazione a NEGOZY.com" is-open="status.isFirstOpen1">
            <p>La registrazione è stata impostata su tecnologie che rendano immediato l’accesso attraverso registrazioni già operative presso i più diffusi social web, in questo modo non sarà necessario reimmettere i propri dati per ottenere acceso anche a Negozy.</p>
            <p>In alternativa è possibile inserire a mano i propri dati anagrafici essenziali: Nome, Cognome, email e procedere con l’identificazione di una password personale che potrà essere usata, congiuntamente con l’indirizzo email, per accedere nel futuro al proprio spazio all’interno di Negozy. La registrazione non ha scadenza, non comporta alcun tipo di pagamento, non obbliga ad alcuna ulteriore attività e non vincola in alcun modo il sottoscrittore, semplicemente rende disponibile la piattaforma Negozy agli usi propri per cui è stata ideata: la diffusione di prodotti e servizi verso il mercato web.</p>
            <p>Una volta effettuata la registrazione sarà infatti possibile impostare il proprio spazio su Negozy predisponendo la propria offerta commerciale attraverso facili strumenti di personalizzazione in modo da aprire il proprio negozio, la propria attività all’intero ecosistema digitale, senza alcun costo, senza alcun vincolo!</p>
          </accordion-group>
          <!--/singolo accordion --> 
          
          <!-- singolo accordion -->
          <accordion-group heading="Dashboard">
            <p>La pagina che si apre al primo accesso rappresenta un riepilogo delle attività personali operative sulla piattaforma. Rappresenta una videata di sola consultazione in cui è facile ed immediato avere dati inerenti la propria attività on line che possano permettere analisi ulteriori ed aiutare ad indirizzare la propria offerta nelle direzioni più efficaci.</p>
          </accordion-group>
          <!--/singolo accordion --> 
          
          <!-- singolo accordion -->
          <accordion-group heading="Impostazioni">
            <p>Attraverso il menù reso disponibile dalla voce Impostazioni è possibile impostare e modificare in ogni momento, tutte le caratteristiche del proprio negozio. Tali valori rappresentano il negozio nel suo complesso, avranno quindi influenza su tutti gli articoli (prodotti e servizi) inseriti.</p>
          </accordion-group>
          <!--/singolo accordion --> 
          
          <!-- singolo accordion -->
          <accordion-group heading="Come creare il proprio negozio su Negozy">
            <p>La prima operazione da fare una volta acceduti al portale Negozy è quella di creare e personalizzare il proprio negozio, una volta effettuata questa operazione si potranno inserire nel negozio tutti gli articoli, prodotti e servizi in modo da allestire la propria vetrina e presentarsi al web in maniera completa ed efficace.</p>
          </accordion-group>
          <!--/singolo accordion --> 
          
          <!-- singolo accordion -->
          <accordion-group heading="Impostazioni &raquo; Dati Negozio">
            <p>Per creare il proprio negozio accedere al menù <b>"impostazioni"</b> presente sul lato sinistro della pagina e cliccare su <b>"Dati Negozio"</b>. Si aprirà una videata in cui sarà possibile inserire tutti i dati generali del proprio negozio.
</p><p>Premettiamo che ogni valorizzazione potrà essere poi analizzata nell’anteprima (prima della pubblicazione) del sito e potrà essere corretta o perfezionata in ogni momento.
</p><p>
Partendo dall’alto della videata si troveranno le impostazioni grafiche, attraverso la sezione di una immagine per il LOGO del negozio ed una seconda immagine o fotografia che possa rappresentare lo sfondo. Per selezionare le immagini sarà sufficiente cliccare su “Cambia Logo” o “Cambia Copertina” e selezionare sulla videata che appare il file da utilizzare. La ricerca sarà impostata sul proprio PC in modo da scegliere l’immagine o la foto giusta che poi verrà caricata in automatico sul server Negozy, quindi non sarà necessario mantenere il file sul proprio PC e potrà essere rimosso al termine dell’operazione di caricamento.
Il logo impostato sarà utilizzato per essere ben visibile sulla pagina principale del proprio sito e come logo (in generale) in tutte le videate in cui è rappresentato il negozio.mLa foto (o immagine) rappresentativa del negozio avrà una dimensione superiore e verrà impiegata nella home page del proprio sito. Dovrà quindi essere una immagine che rappresenti il negozio quindi la foto del punto vendita o una raccolta dei prodotti o la foto dei proprietari/gestori dell’attività o altro che possa essere di presentazione all’intero sito e commerce. Per questo motivo è consigliabile individuare immagine colorate, allegre, accattivanti e di alta qualità (risoluzione grafica).</p><p>
 Una volta selezionate le immagini per il LOGO e la home page, si potrà procedere con la descrizione dell’attività attraverso appositi box in cui poter inserire testo. Nel primo box va inserito il nome del negozio, la ragione sociale dell’attività. Successivamente va impostata la categoria commerciale, importante per efficientare le ricerche sul web, e per farlo è sufficiente selezionare la categoria di appartenenza attraverso l’apposita lista che si apre cliccando su “Categorie”. 
 </p><p>
<b>ATTENZIONE</b>: <u>nel caso non venga trovata la categoria commerciale adeguata alla propria attività sarà necessario procedere inserendo la voce “Altra” prevista nella lista delle categorie e contestualmente inviare una comunicazione al Team Negozy specificando il proprio settore merceologico suggerendo la categoria da aggiungere. Non appena la nuova categoria sarà disponibile verrete avvisati con una email. L’indirizzo da utilizzare per questa e altre tipologie di comunicazione/segnalazione è: <a href="mailto:info@negozy.com">info@negozy.com</a></u>
</p><p>
Il box successivo rappresenta la possibilità di scrivere il testo medio di descrizione generale dell’attività, le caratteristiche salienti, i punti di forza, alcune referenze importanti, cenni sulla storia etc. Si tratta del testo che apparirà nella pagina del sito in alto vicino al logo ed all’immagine di sfondo, non dovrà prendere molto spazio, si tratta di circa 500 caratteri, e dovrà solo presentare velocemente il negozio, l’azienda, l’attività. Ulteriori dettagli potranno essere inseriti più avanti in altri spazi dedicati.
</p><p>
<u>Nel box successivo potrà essere digitato il testo esteso con cui si descrive nei dettagli (a libera discrezione dell’utente) la propria attività, la propria storia, referenze complete punti di forza rispetto alla concorrenza etc. Per ampliare la descrizione della propria attività è possibile anche inserire alcune foto, immagini o altro attraverso la selezione immagini posta sotto il box di testo. Il materiale di testo ed immagini inserito verrà impaginato in una apposita pagina, facente parte del proprio sito, che verrà dedicata all’Azienda in modo da dare ai visitatori un valore di valutazione ulteriore dei prodotti esposti potendo capire anche da dove provengono.
Una volta definita anche la sezione relativa all’Azienda si potrà passare ad inserire alcune informazioni in merito al contatto diretto tra Cliente ed Azienda, nei box successivi è possibile inserire i riferimenti ai quali il negoziante si rende disponibile per un contatto diretto:
indirizzo fisico del punto vendita o dei vari punti vendita, o la sede legale dell’azienda o del professionista etc. Indirizzo email per richiesta info, numero/i di telefono fax e altro.</u>
</p><p>
Infine si ha la possibilità di inserire quello che viene chiamato URL del negozio, ossia il testo che permette di indirizzare la navigazione sul web verso il vostro sito e-commerce, il testo che decidete di inserire sarà quello utilizzato per tracciare il vostro sito sia all’interno del portale Negozy.com che all’eterno. Per questo è conveniente utilizzare il nome stesso del negozio o dell’attività in modo che faccia parte del vostro indirizzo digitale. <b>E’ importante che tale valore non preveda spazi o caratteri speciali (*,+,è,à,ù,ò,’,?,!,ì,^ etc.) e nessuna lettera accentata al proprio interno.</b>
</p>
            <div class="row">
              <div class="col-lg-8 col-lg-offset-2 col-md-12">
                <div class="thumbnail">
                  <div class="embed-responsive embed-responsive-16by9">
                    <iframe class="embed-responsive-item" src="//www.youtube.com/embed/SGQDLp4tAUU" frameborder="0" allowfullscreen></iframe>
                  </div>
                  <div class="caption">
                    <h3>Impostazioni - Dati Negozio</h3>
                  </div>
                </div>
              </div>
            </div>
            
          </accordion-group>
          <!--/singolo accordion --> 
          
          <!-- singolo accordion -->
          <accordion-group heading="Impostazioni &raquo; Dati Fiscali">
            <p>Una volta completata la descrizione del negozio si potrà passare alla voce di menù successiva
              
              presente nel menù <span class="highlight">"Impostazioni"</span> sotto la voce “Dati Negozio”, è necessario quindi cliccare su <span class="highlight">"Dati Fiscali"</span> ed inserire le informazioni così come vengono richieste nella relativa pagina.</p>
            <div class="row">
              <div class="col-lg-8 col-lg-offset-2 col-md-12">
                <div class="thumbnail">
                  <div class="embed-responsive embed-responsive-16by9">
                    <iframe class="embed-responsive-item" src="//www.youtube.com/embed/znHSEZPkPB8" frameborder="0" allowfullscreen></iframe>
                  </div>
                  <div class="caption">
                    <h3>Impostazioni - Dati Fiscali</h3>
                  </div>
                </div>
              </div>
            </div>
            
          </accordion-group>
          <!--/singolo accordion --> 
          
          <!-- singolo accordion -->
          <accordion-group heading="Impostazioni &raquo; Imposte">
            <p> Inserite tutte le informazioni e salvato la videata con il tasto SALVA posto in basso, si passa 
              
              alla voce di menù successiva <span class="highlight">"Imposte"</span>. Attraverso questa videata è possibile personalizzare 
              
              le proprie voci di imposta ed utilizzarle all’occorrenza all’interno del portale Negozy (ad 
              
              esempio nel caricamento dei prezzi sui prodotti /servizi etc.) Al primo accesso non ci saranno 
              
              voci preimpostate, si potrà procedere a creare una nuova voce attraverso il tasto posto in alto 
              
              a destra “Aggiungi Imposta”.</p>
            <p> Per creare una nuova voce è necessario inserire il nome 
              
              dell’imposta (scelta libera da parte dell’utente) ed il valore percentuale dell’imposta. Nel caso 
              
              dell’IVA potranno essere inserite più imposte (da poter poi selezionare in base alle esigenze) 
              
              con valori percentuali diversi a seconda dei casi. </p>
            <div class="row">
              <div class="col-lg-8 col-lg-offset-2 col-md-12">
                <div class="thumbnail">
                  <div class="embed-responsive embed-responsive-16by9">
                    <iframe class="embed-responsive-item" src="//www.youtube.com/embed/7L3NKQi1Dkg" frameborder="0" allowfullscreen></iframe>
                  </div>
                  <div class="caption">
                    <h3>Impostazioni - Imposte</h3>
                  </div>
                </div>
              </div>
            </div>
            
          </accordion-group>
          <!--/singolo accordion --> 
          
          <!-- singolo accordion -->
          <accordion-group heading="Impostazioni &raquo; Spedizioni">
            <p> La fase successiva prevede la creazione di un profilo per le spedizioni dei materiali. Per far
              
              questo sarà sufficiente accedere alla voce di menù successiva <span class="highlight">"Spedizioni"</span>. Anche in questo 
              
              caso al primo accesso non ci saranno valori preimpostati e sarà necessario creare i propri a 
              
              seconda delle esigenze. Attraverso il tasto “Aggiungi spedizione” in alto a destra si potrà 
              
              procedere alla creazione di uno specifico vettore a seconda del rapporto già essere o dei 
              
              parametri economici verificati.</p>
            <p> Si potranno inserire quindi diversi vettori (spedizionieri) che 
              
              potranno risultare più efficaci ed economici a seconda del materiale (peso, volume, fragilità 
              
              etc.) e della distanza. In questo modo sarà possibile selezionare quello migliore per ogni 
              
              specifica esigenza di spedizione. </p>
            <div class="row">
              <div class="col-lg-8 col-lg-offset-2 col-md-12">
                <div class="thumbnail">
                  <div class="embed-responsive embed-responsive-16by9">
                    <iframe class="embed-responsive-item" src="//www.youtube.com/embed/OYXznZS-AW4" frameborder="0" allowfullscreen></iframe>
                  </div>
                  <div class="caption">
                    <h3>Impostazioni - Spedizioni</h3>
                  </div>
                </div>
              </div>
            </div>
            
          </accordion-group>
          <!--/singolo accordion --> 
          
          <!-- singolo accordion -->
          <accordion-group heading="Impostazioni &raquo; Metodi di Pagamento">
            <p> Ultimo settaggio da operare riguarda i <span class="highlight">"metodi di pagamento"</span> che si intende predisporre sul
              
              proprio e-commerce. A seconda delle condizioni e delle convenienze personali sarà possibile 
              
              impostare per la vendita dei propri prodotti diverse metodologie di transazione dando la 
              
              possibilità all’acquirente di poter scegliere tra quelle previste o adattarsi alla sola selezionata 
              
              come valida. L’operazione di scelta multipla (eventualmente tutte le possibilità) o di unica 
              
              possibilità potrà essere impostata dal venditore in questa videata. </p>
            <div class="row">
              <div class="col-lg-8 col-lg-offset-2 col-md-12">
                <div class="thumbnail">
                  <div class="embed-responsive embed-responsive-16by9">
                    <iframe class="embed-responsive-item" src="//www.youtube.com/embed/rAHTfV5MDmI" frameborder="0" allowfullscreen></iframe>
                  </div>
                  <div class="caption">
                    <h3>Impostazioni - Metodi di Pagamento</h3>
                  </div>
                </div>
              </div>
            </div>
            
          </accordion-group>
          <!--/singolo accordion -->  
          
          <!-- singolo accordion -->
          <accordion-group heading="Flusso di Caricamento Prodotti/Servizi">
            <p>Una volta creato il proprio negozio è necessario inserirvi i prodotti o servizi da diffondere
              
              verso gli acquirenti. Per fare questo è sufficiente accedere alla pagina “Prodotti” facendo click 
              
              sulla apposita voce del menù a sinistra e cliccare sul tasto “Aggiungi prodotto” presente in alto 
              
              a destra. A questo punto si aprirà una videata in cui si potranno inserire tutti i dati relativi al 
              
              singolo prodotto da inserire, cominciando dal nome del prodotto e dalla sua descrizione. Il 
              
              nome del prodotto sarà utilizzato per la ricerca da parte degli acquirenti, si consiglia quindi 
              
              un nome “parlante” o nel caso di prodotti di brand particolarmente noti scrivere anche il 
              
              nome del produttore e/o il nome tecnico dell’oggetto. Nella descrizione riportare tutte le 
              
              caratteristiche del prodotto o servizio con particolare attenzione a mettere in evidenza quelle 
              
              di maggior interesse da parte del pubblico. Si passa quindi ad inserire il prezzo del prodotto, il 
              
              valore da inserire sarà il valore economico del prodotto stesso, senza eventuali spese di 
              
              spedizione. L’aliquota IVA o eventuali altre imposte che aggravino il prezzo potranno essere 
              
              inserite nel campo “Imposta” mentre il sistema calcola in automatico il valore del prezzo 
              
              finale. Si passa quindi a definire la disponibilità del prodotto potendo impostarla in tre 
              
              modalità differenti:</p>
            <p> <b>Quantità DEFINITA</b>, ossia il sistema è in grado di dare disponibilità immediata fino ad un
              
              numero massimo impostato su tale campo per questo prodotto. Superata tale disponibilità il 
              
              sistema non permetterà più l’acquisto diretto ma invierà una richiesta al venditore con i dati 
              
              della richiesta di acquisto per autorizzare la transazione. </p>
            <p> <b>Quantità ILLIMITATA</b>, ossia il sistema porrà sempre il prodotto in condizioni di essere
              
              venduto subito. </p>
            <p> <b>Richiedi Disponibilità</b>, con questa opzione il sistema imposta sempre una trattativa di 
              
              vendita con messaggi istantanei tra il compratore ed il venditore in modo da poter veicolare 
              
              un accordo sulla tempistica di spedizione che possa prevedere anche il tempo di 
              
              approvvigionamento per lo specifico materiale. Attraverso questa opzione è possibile mettere 
              
              in vendita beni di cui non si dispone fisicamente avendo garanzia del fatto che la transazione 
              
              di vendita non potrà prescindere da una negoziazione tra le parti. (vedi anche <span class="highlight">Flusso di 
              
              verifica disponibilità</span>). </p>
            <p> Con il tasto “Seleziona Immagini” è possibile importare sul negozio on line foto, disegni ed
              
              immagini di ogni tipo a descrizione del prodotto e del suo impiego, tali immagini verranno 
              
              utilizzate nella scheda prodotto con la possibilità di ingrandimento dei dettagli.</p>
            <p> Caratteristica importante per ogni prodotto è la corretta catalogazione in apposite categorie 
              
              prodotto. Attraverso i tasti di scelta categoria è possibile identificare un percorso che 
              
              caratterizzi il prodotto in una apposita sottocategoria inserita in una specifica categoria a sua 
              
              volta appartenente ad una macrocategoria. Questo meccanismo permette di ordinare con 
              
              criterio i prodotti e di poterli rendere disponibili per le ricerche degli acquirenti sia sulla 
              
              pagina personale del negozio su cui risiedono, sia nella ricerca generica per prodotto fatta 
              
              anche fuori dallo specifico sito personale. Per operare la scelta migliore quindi selezionare, 
              
              partendo dalla macrocategoria fino ad arrivare alla sottocategoria, la scelta più rispondente al 
              
              proprio prodotto. Nel caso in cui non si trovi alcuna possibilità nel menù predisposto si ha la 
              
              possibilità di creare nuove voci semplicemente clicando su “crea nuova” che appare in basso 
              
              al menù di scelta categorie. A questo punto sarà sufficiente digitare il nome della nuova voce e 
              
              cliccare sul simbolino “+” a destra del testo inserito per confermare la creazione e si potrà 
              
              procedere con la catalogazione del prodotto. Questa possibilità di creazione nuove voci è 
              
              disponibile su tutte le gerarchie.</p>
            <p> Ultima voce da inserire per redere il prodotto disponibile alla vetrina vendite è il così detto 
              
              TAG. Si tratta di più parole che possono ricondurre al prodotto, ossia il sistema permette di 
              
              inserire tutte le parole che per affinità possono essere riconducibili, e soprattutto possano 
              
              ricondurre al prodotto in base alle ricerche effettuate. Le parole inserite in questo campo 
              
              saranno delle chiavi che si attivano sulle ricerche operate dagli acquirenti, quindi se nella 
              
              stringa della ricerca di un potenziale acquirente compare una o più di tali parole il prodotto 
              
              verrà selezionato nei risultati della ricerca. Tali parole quindi dovranno essere rispondenti al 
              
              prodotto stesso e afferenti alla categoria merceologica altrimenti si avrà l’effetto inverso e le 
              
              ricerche non saranno efficaci. Per inserire uno o più TAG è sufficiente scrivere il testo scelto e 
              
              premere invio. Per cancellare un TAG è sufficiente cliccare sulla x posta nella sua icona.
              
              Tutta la messaggistica istantanea viene gestita dal tasto in alto a destra che riporta anche il 
              
              numero degli eventuali messaggi non letti. </p>
          </accordion-group>
          <!--/singolo accordion --> 
          
          <!-- singolo accordion -->
          <accordion-group heading="Flusso di verifica disponibilità">
            <p>Nei casi in cui il sistema non verifica la disponibilità di un prodotto nella misura in cui venga 
              
              richiesto per l’acquisto, o nel caso in cui il venditore abbia espressamente impostato il 
              
              parametro di “Richiesta disponibilità”,  la piattaforma Negozy.com attiva un flusso che ha 
              
              come obiettivo quello di permettere comunque la vendita.</p>
            <p> Per farlo sono stati predisposti 
              
              strumenti di messaggistica instantanea che avvisano il venditore con popup automatici sul 
              
              portale del potenziale acquirente e della richiesta di disponibilità per un determinato 
              
              prodotto. Nella fase di richiesta disponibilità il compratore rimane in attesa di un riscontro 
              
              per poter perfezionare la vendita ma ha già espresso la sua volontà di acquisto.</p>
            <p> A questo 
              
              punto il venditore può rispondere immediatamente (all’arrivo del popup) dando disponibilità 
              
              
              o rifiutandola, oppure può prendere tempo e gestire la richiesta anziché direttamente sul 
              
              popup di attivazione, da una apposita area in cui ha sotto controllo tutte le richieste (aperte e 
              
              chiuse). Rispondendo all’acquirente potrà quindi verificare i tempi di approvvigionamento e 
              
              gestire la transazione al meglio. Una volta ricevuta conferma da parte del venditore, 
              
              l’acquirente procede con il pagamento effettivo.</p>
          </accordion-group>
          <!--/singolo accordion --> 
          
          <!-- singolo accordion -->
          <accordion-group heading="Videata Prodotti">
            <p>Dalla pagina dei prodotti si potranno avere a colpo d’occhio tutti gli articoli (prodotti e/o
              
              servizi) caricati sul proprio spazio personale. Tali articoli potranno essere poi  pubblicati sul 
              
              proprio negozio on line o rimanere non visibili al pubblico.  Attraverso il tasto “ELIMINA” 
              
              l’articolo verrà rimosso da Negozy (ivi comprese descrizioni, foto, altre info),  i dati 
              
              eventualmente generati da tale articolo a livello di vendite, quindi ad esempio i dati di 
              
              statistica vendite, riscontri da parte della comunity (attività social di Negozy) etc. verranno 
              
              mantenuti pur cancellando l’articolo dalla vetrina.</p>
            <p> Attraverso il tasto “MODIFICA” si potrà 
              
              entrare un una apposita videata dedicata alla gestione del singolo prodotto/servizio. In questa 
              
              sezione è possibile modificare ogni caratteristica inerente il singolo articolo semplicemente 
              
              sovrascivendo i testi presenti (precedentemente inseriti) o togliendo/aggiungendo le 
              
              immagini del prodotto. (Per la corretta compilazione della videata fare riferimento al <span class="highlight">"Flusso 
              
              di caricamento Prodotti/Servizi"</span>)</p>
          </accordion-group>
          <!--/singolo accordion --> 
          
          <!-- singolo accordion -->
          <accordion-group heading="Videata Ordini">
            <p> La videata rappresenta un riepilogo della situazione relativa alle vendite dei prodotti.
              
              Attraverso questo riepilogo sarà possibile avere in tempo reale la situazione delle vendite e lo 
              
              stato delle trattative in corso su tutti i prodotti/servizi del proprio negozio on line. Ogni riga 
              
              presente nel riepilogo rappresenta un singolo ordine descritto nelle sue caratteristiche di: </p>
            <ul>
              <li>Nome del Cliente</li>
              <li>Nome del prodotto ordinato</li>
              <li>Quantità di prodotto ordinata</li>
              <li>Stato dell'ordine</li>
              <li>Data</li>
              <li>Tipologia di pagamento</li>
              <li>Totale economico dell'ordine</li>
              <li>Note</li>
            </ul>
            <p> I dati relativi al prodotto (nome prodotto e prezzo) vengono presi in automatico dalla videata 
              
              su cui si è inserito il prodotto (vedi <span class="highlight">Videata Prodotti</span>) mentre i dati relativi alla tipologia di 
              
              pagamento vengono presi dalle impostazioni inserite nella videata in cui si sono inseriti i dati 
              
              relativi al proprio negozio e vengono utilizzati per tutti i prodotti del negozio stesso (vedi 
              
              Videata <span class="highlight">Impostazioni -> Metodi di Pagamento</span>). La quantità di prodotto ordinata viene 
              
              richiesta direttamente dal Cliente e, se la giacenza dichiarata dal venditore per quel prodotto 
              
              è sufficiente, rappresenterà la quantità in ordine. Se nella videata prodotti il venditore ha 
              
              impostato una giacenza inferiore alla richiesta o ha impostato la funzione di “Richiedi 
              
              disponibilità”, la richiesta da parte del Cliente viene gestita attraverso scambio di 
              
              comunicazioni dirette tra venditore e acquirente fino a che il venditore non approva la 
              
              quantità richiesta dando vita ad un nuovo ordine. (Per i dettagli di tale flusso vedere "<span class="highlight">Flusso 
              
              di verifica disponibilità</span>"). </p>
            <p>Lo stato dell’ordine viene impostato automaticamente a seconda della situazione in cui 
              
              l’ordine si trova. Tali step sono identificati nelle seguenti voci: </p>
            <p><b>"Richiesta Disponibilità"</b>, è la situazione in cui un Cliente ha dichiarato di voler acquistare 
              
              un prodotto ma non essendo presente disponibilità il sistema ha inviato una richiesta di 
              
              acquisto al venditore. In questa situazione il compratore rimane in attesa di un riscontro alla 
              
              sua richiesta di acquisto ed il venditore deve valutare la possibilità di reperire il prodotto per 
              
              perfezionare la vendita. In tale situazione il Cliente si è impegnato ad acquistare di 
              
              conseguenza la risposta da parte del venditore dovrebbe essere gestita nel giro di poco tempo 
              
              o contestualmente alla richiesta stessa in modo da non bloccare il flusso della vendita. Se 
              
              trascorre troppo tempo la vendita (in realtà già definita) potrebbe sfumare. Cliccando sullo 
              
              stato “Richiesta Disponibilità” si potrà accedere alla lista completa delle richieste ancora 
              
              aperte, nonché alla lista delle richeiste chiuse). </p>
            <p> <b>"In Attesa di Pagamento"</b>, è la situazione in cui il Cliente ha effettuato il pagamento ed il 
              
              venditore avrà la possibilità di verificare l’incasso prima di effettuare la spedizione. Fino a 
              
              questo stato il sistema procede in automatico, a questo punto il venditore avrà modo di 
              
              operare manualmente il cambio stato una volta verificato l’incasso, o comunque a sua 
              
              discrezione, facendo procedere il flusso verso la spedizione del prodotto (vedi sotto). 
              
              Cliccando sullo stato “In Attesa di Pagamento” si potrà accedere alla videata di riepilogo dei 
              
              dati lasciati dal compratore, ivi compresi i dati relativi alla spedizione. </p>
            <p> <b>"In Attesa di Spedizione"</b>, è lo stato in cui il venditore può impostare l’ordine nel caso in cui 
              
              la spedizione non sia ancora pronta. Cliccando sullo stato “In Attesa di Spedizione” si potrà 
              
              accedere alla videata di riepilogo dei dati lasciati dal compratore, ivi compresi i dati relativi 
              
              alla spedizione. </p>
            <p> <b>"Spedito"</b>, è lo stato in cui il venditore può impostare l’ordine nel caso in cui abbia già 
              
              approntato la spedizione verso il compratore. </p>
            <p> <b>"Chiuso"</b>, è lo stato in cui il venditore può impostare l’ordine nel momento in cui il vettore da 
              
              lui scelto per la spedizione confermi l’avvenuta consegna del prodotto al compratore. Lo stato 
              
              Chiuso” determina la fine del flusso e l’avvenuta vendita.
              
              Lungo tutto il flusso il venditore ha la possibilità di annotare informazioni relative all’ordine, 
              
              al compratore o altro nell’apposito spazio per le note presente sulla riga dell’ordine. </p>
          </accordion-group>
          <!--/singolo accordion --> 
          
        </accordion>
      </div>
    </div>
    <!--<div class="row">
      <div class="col-lg-10 col-lg-offset-1 col-md-12">
        <p class="lead">Di seguito alcuni video dimostrativi per aiutare l'utente nella configurazione delle operazioni preliminari del negozio.</p>
        <div class="row">
          <div class="col-sm-6">
            <div class="thumbnail">
              <iframe width="100%" height="315" src="//www.youtube.com/embed/SGQDLp4tAUU" frameborder="0" allowfullscreen></iframe>
              <div class="caption">
                <h3>Impostazioni - Dati Negozio</h3>
              </div>
            </div>
          </div>
          <div class="col-sm-6">
            <div class="thumbnail">
              <iframe width="100%" height="315" src="//www.youtube.com/embed/znHSEZPkPB8" frameborder="0" allowfullscreen></iframe>
              <div class="caption">
                <h3>Impostazioni -  Dati Fiscali</h3>
              </div>
            </div>
          </div>
          <div class="col-sm-6">
            <div class="thumbnail">
              <iframe width="100%" height="315" src="//www.youtube.com/embed/7L3NKQi1Dkg" frameborder="0" allowfullscreen></iframe>
              <div class="caption">
                <h3>Impostazioni - Imposte</h3>
              </div>
            </div>
          </div>
          <div class="col-sm-6">
            <div class="thumbnail">
              <iframe width="100%" height="315" src="//www.youtube.com/embed/OYXznZS-AW4" frameborder="0" allowfullscreen></iframe>
              <div class="caption">
                <h3>Impostazioni -  Spedizioni</h3>
              </div>
            </div>
          </div>
          <div class="col-sm-6">
            <div class="thumbnail">
              <iframe width="100%" height="315" src="//www.youtube.com/embed/rAHTfV5MDmI" frameborder="0" allowfullscreen></iframe>
              <div class="caption">
                <h3>Impostazioni -  Metodi di Pagamento</h3>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>--> 
  </div>
</div>
