<div class="lapagina">
  <nav class="navbar navbar-default navbar-static-top" role="navigation">
    <div class="navbar-header">
      <ul class="nav navbar-nav">
        <li>
          <ol class="breadcrumb">
            <li class="active"><a>Aiuto</a></li>
          </ol>
        </li>
      </ul>
    </div>
  </nav>
  <div id="help-page" class="page page-table" data-target='#AnchorList' scroll-spy=''>
  	 <div id="risposta-0"></div>
     <div class="row" >
        <div class="col-sm-12 col-md-4 col-lg-3 affix"> 
            <section class="panel panel-warning mail-categories">
                <div class="panel-heading bg-warning">Domande Frequenti</div>
                <ul class="list-group" id='AnchorList'>
                    <li class="list-group-item"><a href='javascript:void(0);' scroll-to='risposta-1'>
                        Cosa comporta la registrazione a NEGOZY.com
                    </a></li>
                    <li class="list-group-item"><a href='javascript:void(0);' scroll-to='risposta-2'>
                        Dashboard
                    </a></li> 
                    <li class="list-group-item"><a href='javascript:void(0);' scroll-to='risposta-3'>
                        Impostazioni
                    </a></li>
                    <li class="list-group-item"><a href='javascript:void(0);' scroll-to='risposta-4'>
                        Come creare il proprio negozio su Negozy
                    </a></li>
                    <li class="list-group-item"><a href='javascript:void(0);' scroll-to='risposta-5'>
                        Impostazioni &raquo; Dati Negozio
                    </a></li>
					<li class="list-group-item"><a href='javascript:void(0);' scroll-to='risposta-6'>
                        Impostazioni &raquo; Dati Fiscali
                    </a></li>
					<li class="list-group-item"><a href='javascript:void(0);' scroll-to='risposta-7'>
                        Impostazioni &raquo; Imposte
                    </a></li>
					<li class="list-group-item"><a href='javascript:void(0);' scroll-to='risposta-8'>
                        Impostazioni &raquo; Spedizioni
                    </a></li>
					<li class="list-group-item"><a href='javascript:void(0);' scroll-to='risposta-9'>
                        Impostazioni &raquo; Metodi di Pagamento
                    </a></li>
					<li class="list-group-item"><a href='javascript:void(0);' scroll-to='risposta-10'>
                        Flusso di Caricamento Prodotti/Servizi
                    </a></li>
					<li class="list-group-item"><a href='javascript:void(0);' scroll-to='risposta-11'>
                        Flusso di verifica disponibilità
                    </a></li>
					<li class="list-group-item"><a href='javascript:void(0);' scroll-to='risposta-12'>
                        Videata Prodotti
                    </a></li>
					<li class="list-group-item"><a href='javascript:void(0);' scroll-to='risposta-13'>
                        Videata Ordini
                    </a></li>
					<li class="list-group-item"><a href='javascript:void(0);' scroll-to='risposta-14'>
                        Videata Collezioni
                    </a></li>
                </ul>
            </section>
        </div>
        <div class="col-sm-12 col-md-offset-4 col-md-8 col-lg-offset-3 col-lg-9">
		
			<div id="risposta-1"></div>
            <section class="panel panel-default mail-container">
                <div class="panel-heading"><strong><span class="glyphicon glyphicon-th"></span> Cosa comporta la registrazione a NEGOZY.com</strong></div>
                <div class="panel-body">
                    <div class="mail-content">
                       <p>La registrazione &egrave;
						stata impostata su tecnologie che rendano immediato l&rsquo;accesso
						attraverso registrazioni gi&agrave; operative presso i pi&ugrave;
						diffusi social web, in questo modo non sar&agrave; necessario
						reimmettere i propri dati per ottenere acceso anche a Negozy. 
						</p>
						<p>In alternativa &egrave;
						possibile inserire a mano i propri dati anagrafici essenziali: Nome,
						Cognome, email e procedere con l&rsquo;identificazione di una
						password personale che potr&agrave; essere usata, congiuntamente con
						l&rsquo;indirizzo email, per accedere nel futuro al proprio spazio
						all&rsquo;interno di Negozy.</p>
						<p>La registrazione non ha
						scadenza, non comporta alcun tipo di pagamento, non obbliga ad alcuna
						ulteriore attivit&agrave; e non vincola in alcun modo il
						sottoscrittore, semplicemente rende disponibile la piattaforma Negozy
						agli usi propri per cui &egrave; stata ideata: la diffusione di
						prodotti e servizi verso il mercato web.</p>
						<p>Una volta effettuata la
						registrazione sar&agrave; infatti possibile impostare il proprio
						spazio su Negozy predisponendo la propria offerta commerciale
						attraverso facili strumenti di personalizzazione in modo da aprire il
						proprio negozio, la propria attivit&agrave; all&rsquo;intero
						ecosistema digitale, senza alcun costo, senza alcun vincolo!</p>
						<p>
                    </div>
                    <div class="mail-actions">
						<a class="btn btn-sm btn-primary pull-right" href='javascript:void(0);' scroll-to='risposta-0'>
							Torna su <i class="fa fa-mail-reply fa-rotate-90"></i>
						</a>
					</div>
                </div>
            </section>
			
			<div id="risposta-2" class="h15"></div>
			<section class="panel panel-default mail-container">
                <div class="panel-heading"><strong><span class="glyphicon glyphicon-th"></span> Dashboard</strong></div>
                <div class="panel-body">
                    <div class="mail-content">
                       <p>La pagina che si apre al
						primo accesso rappresenta un riepilogo delle attivit&agrave;
						personali operative sulla piattaforma. Rappresenta una videata di
						sola consultazione in cui &egrave; facile ed immediato avere dati
						inerenti la propria attivit&agrave; on line che possano permettere
						analisi ulteriori ed aiutare ad indirizzare la propria offerta nelle
						direzioni pi&ugrave; efficaci.</p>
						
                    </div>
                    <div class="mail-actions">
						<a class="btn btn-sm btn-primary pull-right" href='javascript:void(0);' scroll-to='risposta-0'>
							Torna su <i class="fa fa-mail-reply fa-rotate-90"></i>
						</a>
					</div>
                </div>
            </section>
			
			<div id="risposta-3" class="h15"></div>
			<section class="panel panel-default mail-container">
                <div class="panel-heading"><strong><span class="glyphicon glyphicon-th"></span> Impostazioni</strong></div>
                <div class="panel-body">
                    <div class="mail-content">
                       <p>Attraverso il men&ugrave; reso
						disponibile dalla voce Impostazioni &egrave; possibile impostare e
						modificare in ogni momento, tutte le caratteristiche del proprio
						negozio. Tali valori rappresentano il negozio nel suo complesso,
						avranno quindi influenza su tutti gli articoli (prodotti e servizi)
						inseriti.</p>
						
                    </div>
                    <div class="mail-actions">
						<a class="btn btn-sm btn-primary pull-right" href='javascript:void(0);' scroll-to='risposta-0'>
							Torna su <i class="fa fa-mail-reply fa-rotate-90"></i>
						</a>
					</div>
                </div>
            </section>
			
			<div id="risposta-4" class="h15"></div>
			<section class="panel panel-default mail-container">
                <div class="panel-heading"><strong><span class="glyphicon glyphicon-th"></span> Come creare il proprio negozio su Negozy</strong></div>
                <div class="panel-body">
                    <div class="mail-content">
                       <p>La prima operazione da
						fare una volta acceduti al portale Negozy &egrave; quella di creare e
						personalizzare il proprio negozio, una volta effettuata questa
						operazione si potranno inserire nel negozio tutti gli articoli,
						prodotti e servizi in modo da allestire la propria vetrina e
						presentarsi al web in maniera completa ed efficace.</p>
                    </div>
                    <div class="mail-actions">
						<a class="btn btn-sm btn-primary pull-right" href='javascript:void(0);' scroll-to='risposta-0'>
							Torna su <i class="fa fa-mail-reply fa-rotate-90"></i>
						</a>
					</div>
                </div>
            </section>
			
			<div id="risposta-5" class="h15"></div>
			<section class="panel panel-default mail-container">
                <div class="panel-heading"><strong><span class="glyphicon glyphicon-th"></span> Impostazioni <font face="Wingdings, serif">&#61664;</font>
Dati Negozio</strong></div>
                <div class="panel-body">
                    <div class="mail-content">
                       <p>Per creare il proprio
						negozio accedere al men&ugrave; &ldquo;<font color="#4f81bd"><b>impostazioni</b></font>&rdquo;
						presente sul lato sinistro della pagina e cliccare su &ldquo;<font color="#4f81bd"><b>Dati
						Negozio</b></font>&rdquo;. Si aprir&agrave; una videata in cui
						sar&agrave; possibile inserire tutti i dati generali del proprio
						negozio.</p>
						<p>Premettiamo che ogni
						valorizzazione potr&agrave; essere poi analizzata nell&rsquo;anteprima
						(prima della pubblicazione) del sito e potr&agrave; essere corretta o
						perfezionata in ogni momento.</p>
						<p>Partendo dall&rsquo;alto
						della videata si troveranno le impostazioni grafiche, attraverso la
						sezione di una immagine per il LOGO del negozio ed una seconda
						immagine o fotografia che possa rappresentare lo sfondo. Per
						selezionare le immagini sar&agrave; sufficiente cliccare su &ldquo;Cambia
						Logo&rdquo; o &ldquo;Cambia Copertina&rdquo; e selezionare sulla
						videata che appare il file da utilizzare. La ricerca sar&agrave;
						impostata sul proprio PC in modo da scegliere l&rsquo;immagine o la
						foto giusta che poi verr&agrave; caricata in automatico sul server
						Negozy, quindi non sar&agrave; necessario mantenere il file sul
						proprio PC e potr&agrave; essere rimosso al termine dell&rsquo;operazione
						di caricamento.</p>
						<p>Il logo impostato sar&agrave;
						utilizzato per essere ben visibile sulla pagina principale del
						proprio sito e come logo (in generale) in tutte le videate in cui &egrave;
						rappresentato il negozio.mLa foto (o immagine) rappresentativa del
						negozio avr&agrave; una dimensione superiore e verr&agrave; impiegata
						nella home page del proprio sito. Dovr&agrave; quindi essere una
						immagine che rappresenti il negozio quindi la foto del punto vendita
						o una raccolta dei prodotti o la foto dei proprietari/gestori
						dell&rsquo;attivit&agrave; o altro che possa essere di presentazione
						all&rsquo;intero sito e commerce. Per questo motivo &egrave;
						consigliabile individuare immagine colorate, allegre, accattivanti e
						di alta qualit&agrave; (risoluzione grafica).</p>
						<p> Una volta selezionate le
						immagini per il LOGO e la home page, si potr&agrave; procedere con la
						descrizione dell&rsquo;attivit&agrave; attraverso appositi box in cui
						poter inserire testo. Nel primo box va inserito il nome del negozio,
						la ragione sociale dell&rsquo;attivit&agrave;. Successivamente va
						impostata la categoria commerciale, importante per efficientare le
						ricerche sul web, e per farlo &egrave; sufficiente selezionare la
						categoria di appartenenza attraverso l&rsquo;apposita lista che si
						apre cliccando su &ldquo;Categorie&rdquo;. 
						</p>
						<p><font color="#4f81bd"><b>ATTENZIONE</b></font>:
						<span style="background: #ffff00">nel caso non venga trovata la
						categoria commerciale adeguata alla propria attivit&agrave; sar&agrave;
						necessario procedere inserendo la voce &ldquo;Altra&rdquo; prevista
						nella lista delle categorie e contestualmente inviare una
						comunicazione al Team Negozy specificando il proprio settore
						merceologico suggerendo la categoria da aggiungere. Non appena la
						nuova categoria sar&agrave; disponibile verrete avvisati con una
						email. L&rsquo;indirizzo da utilizzare per questa e altre tipologie
						di comunicazione/segnalazione &egrave;: </span><font color="#345a8a"><b><span style="background: #ffff00">info@negozy.com</span></b></font></p>
						<p><br>
						</p>
						<p>Il box successivo
						rappresenta la possibilit&agrave; di scrivere il testo medio di
						descrizione generale dell&rsquo;attivit&agrave;, le caratteristiche
						salienti, i punti di forza, alcune referenze importanti, cenni sulla
						storia etc. Si tratta del testo che apparir&agrave; nella pagina del
						sito in alto vicino al logo ed all&rsquo;immagine di sfondo, non
						dovr&agrave; prendere molto spazio, si tratta di circa 500 caratteri,
						e dovr&agrave; solo presentare velocemente il negozio, l&rsquo;azienda,
						l&rsquo;attivit&agrave;. Ulteriori dettagli potranno essere inseriti
						pi&ugrave; avanti in altri spazi dedicati.</p>
						<p><span style="background: #ffff00">Nel
						box successivo potr&agrave; essere digitato il testo esteso con cui
						si descrive nei dettagli (a libera discrezione dell&rsquo;utente) la
						propria attivit&agrave;, la propria storia, referenze complete punti
						di forza rispetto alla concorrenza etc. Per ampliare la descrizione
						della propria attivit&agrave; &egrave; possibile anche inserire
						alcune foto, immagini o altro attraverso la selezione immagini posta
						sotto il box di testo. Il materiale di testo ed immagini inserito
						verr&agrave; impaginato in una apposita pagina, facente parte del
						proprio sito, che verr&agrave; dedicata all&rsquo;Azienda in modo da
						dare ai visitatori un valore di valutazione ulteriore dei prodotti
						esposti potendo capire anche da dove provengono.</span></p>
						<p><span style="background: #ffff00">Una
						volta definita anche la sezione relativa all&rsquo;Azienda si potr&agrave;
						passare ad inserire alcune informazioni in merito al contatto diretto
						tra Cliente ed Azienda, nei box successivi &egrave; possibile
						inserire i riferimenti ai quali il negoziante si rende disponibile
						per un contatto diretto:</span></p>
						<p><span style="background: #ffff00">indirizzo
						fisico del punto vendita o dei vari punti vendita, o la sede legale
						dell&rsquo;azienda o del professionista etc. Indirizzo email per
						richiesta info, numero/i di telefono fax e altro.</span></p>
						<p>Infine si ha la
						possibilit&agrave; di inserire quello che viene chiamato URL del
						negozio, ossia il testo che permette di indirizzare la navigazione
						sul web verso il vostro sito e-commerce, il testo che decidete di
						inserire sar&agrave; quello utilizzato per tracciare il vostro sito
						sia all&rsquo;interno del portale Negozy.com che all&rsquo;eterno.
						Per questo &egrave; conveniente utilizzare il nome stesso del negozio
						o dell&rsquo;attivit&agrave; in modo che faccia parte del vostro 
						indirizzo digitale. <b>E&rsquo; importante che tale valore non
						preveda spazi o caratteri speciali (*,+,&egrave;,&agrave;,&ugrave;,&ograve;,&rsquo;,?,!,&igrave;,^
						etc.) e nessuna lettera accentata al proprio interno.</b></p>
						<br>
						<div class="row">
						  <div class="col-lg-8 col-lg-offset-2 col-md-12">
							<div class="thumbnail">
							  <div class="embed-responsive embed-responsive-16by9">
								<iframe class="embed-responsive-item" src="//www.youtube.com/embed/SGQDLp4tAUU" frameborder="0" allowfullscreen></iframe>
							  </div>
							  <div class="caption">
								<h3>Impostazioni - Dati Negozio</h3>
							  </div>
							</div>
						  </div>
						</div>
                    </div>
					<div class="mail-actions">
						<a class="btn btn-sm btn-primary pull-right" href='javascript:void(0);' scroll-to='risposta-0'>
							Torna su <i class="fa fa-mail-reply fa-rotate-90"></i>
						</a>
					</div>
                </div>
            </section>
			
			<div id="risposta-6" class="h15"></div>
			<section class="panel panel-default mail-container">
                <div class="panel-heading"><strong><span class="glyphicon glyphicon-th"></span> Impostazioni <font face="Wingdings, serif">&#61664;</font>
Dati Fiscali</strong></div>
                <div class="panel-body">
                    <div class="mail-content">
                       <p>Una volta completata la
						descrizione del negozio si potr&agrave; passare alla voce di men&ugrave;
						successiva presente nel men&ugrave; &ldquo;<font color="#4f81bd"><b>Impostazioni</b></font>&rdquo;
						sotto la voce &ldquo;Dati Negozio&rdquo;, &egrave; necessario quindi
						cliccare su &ldquo;<font color="#4f81bd"><b>Dati
						Fiscali</b></font>&rdquo; ed inserire le informazioni cos&igrave;
						come vengono richieste nella relativa pagina.</p>
						<br>
						<div class="row">
						  <div class="col-lg-8 col-lg-offset-2 col-md-12">
							<div class="thumbnail">
							  <div class="embed-responsive embed-responsive-16by9">
								<iframe class="embed-responsive-item" src="//www.youtube.com/embed/znHSEZPkPB8" frameborder="0" allowfullscreen></iframe>
							  </div>
							  <div class="caption">
								<h3>Impostazioni - Dati Fiscali</h3>
							  </div>
							</div>
						  </div>
						</div>
                    </div>
					<div class="mail-actions">
						<a class="btn btn-sm btn-primary pull-right" href='javascript:void(0);' scroll-to='risposta-0'>
							Torna su <i class="fa fa-mail-reply fa-rotate-90"></i>
						</a>
					</div>
                </div>
            </section>
			
			<div id="risposta-7" class="h15"></div>
			<section class="panel panel-default mail-container">
                <div class="panel-heading"><strong><span class="glyphicon glyphicon-th"></span> Impostazioni <font face="Wingdings, serif">&#61664;</font>
Imposte</strong></div>
                <div class="panel-body">
                    <div class="mail-content">
                       <p>Inserite tutte le
						informazioni e salvato la videata con il tasto SALVA posto in basso,
						si passa alla voce di men&ugrave; successiva &ldquo;<font color="#4f81bd"><b>Imposte</b></font>&rdquo;.
						Attraverso questa videata &egrave; possibile personalizzare le
						proprie voci di imposta ed utilizzarle all&rsquo;occorrenza
						all&rsquo;interno del portale Negozy (ad esempio nel caricamento dei
						prezzi sui prodotti /servizi etc.) Al primo accesso non ci saranno
						voci preimpostate, si potr&agrave; procedere a creare una nuova voce
						attraverso il tasto posto in alto a destra &ldquo;Aggiungi Imposta&rdquo;.
						Per creare una nuova voce &egrave; necessario inserire il nome
						dell&rsquo;imposta (scelta libera da parte dell&rsquo;utente) ed il
						valore percentuale dell&rsquo;imposta. Nel caso dell&rsquo;IVA
						potranno essere inserite pi&ugrave; imposte (da poter poi selezionare
						in base alle esigenze) con valori percentuali diversi a seconda dei
						casi.</p>
						<br>
						<div class="row">
						  <div class="col-lg-8 col-lg-offset-2 col-md-12">
							<div class="thumbnail">
							  <div class="embed-responsive embed-responsive-16by9">
								<iframe class="embed-responsive-item" src="//www.youtube.com/embed/7L3NKQi1Dkg" frameborder="0" allowfullscreen></iframe>
							  </div>
							  <div class="caption">
								<h3>Impostazioni - Imposte</h3>
							  </div>
							</div>
						  </div>
						</div>
                    </div>
					<div class="mail-actions">
						<a class="btn btn-sm btn-primary pull-right" href='javascript:void(0);' scroll-to='risposta-0'>
							Torna su <i class="fa fa-mail-reply fa-rotate-90"></i>
						</a>
					</div>
                </div>
            </section>
			
			<div id="risposta-8" class="h15"></div>
			<section class="panel panel-default mail-container">
                <div class="panel-heading"><strong><span class="glyphicon glyphicon-th"></span> Impostazioni <font face="Wingdings, serif">&#61664;</font>
Spedizioni</strong></div>
                <div class="panel-body">
                    <div class="mail-content">
                       <p>La fase successiva
						prevede la creazione di un profilo per le spedizioni dei materiali.
						Per far questo sar&agrave; sufficiente accedere alla voce di men&ugrave;
						successiva &ldquo;<font color="#4f81bd"><b>Spedizioni</b></font>&rdquo;.
						Anche in questo caso al primo accesso non ci saranno valori
						preimpostati e sar&agrave; necessario creare i propri a seconda delle
						esigenze. Attraverso il tasto &ldquo;Aggiungi spedizione&rdquo; in
						alto a destra si potr&agrave; procedere alla creazione di uno
						specifico vettore a seconda del rapporto gi&agrave; essere o dei
						parametri economici verificati. Si potranno inserire quindi diversi
						vettori (spedizionieri) che potranno risultare pi&ugrave; efficaci ed
						economici a seconda del materiale (peso, volume, fragilit&agrave;
						etc.) e della distanza. In questo modo sar&agrave; possibile
						selezionare quello migliore per ogni specifica esigenza di
						spedizione.</p>
						<br>
						<div class="row">
						  <div class="col-lg-8 col-lg-offset-2 col-md-12">
							<div class="thumbnail">
							  <div class="embed-responsive embed-responsive-16by9">
								<iframe class="embed-responsive-item" src="//www.youtube.com/embed/OYXznZS-AW4" frameborder="0" allowfullscreen></iframe>
							  </div>
							  <div class="caption">
								<h3>Impostazioni - Spedizioni</h3>
							  </div>
							</div>
						  </div>
						</div>
                    </div>
					<div class="mail-actions">
						<a class="btn btn-sm btn-primary pull-right" href='javascript:void(0);' scroll-to='risposta-0'>
							Torna su <i class="fa fa-mail-reply fa-rotate-90"></i>
						</a>
					</div>
                </div>
            </section>
			
			<div id="risposta-9" class="h15"></div>
			<section class="panel panel-default mail-container">
                <div class="panel-heading"><strong><span class="glyphicon glyphicon-th"></span> Impostazioni <font face="Wingdings, serif">&#61664;</font>
Metodi di Pagamento</strong></div>
                <div class="panel-body">
                    <div class="mail-content">
                       <p>Ultimo settaggio da
						operare riguarda i &ldquo;<font color="#4f81bd"><b>metodi
						di pagamento</b></font>&rdquo; che si intende predisporre sul
						proprio e-commerce. A seconda delle condizioni e delle convenienze
						personali sar&agrave; possibile impostare per la vendita dei propri
						prodotti diverse metodologie di transazione dando la possibilit&agrave;
						all&rsquo;acquirente di poter scegliere tra quelle previste o
						adattarsi alla sola selezionata come valida. L&rsquo;operazione di
						scelta multipla (eventualmente tutte le possibilit&agrave;) o di
						unica possibilit&agrave; potr&agrave; essere impostata dal venditore
						in questa videata.</p>
						<br>
						<div class="row">
						  <div class="col-lg-8 col-lg-offset-2 col-md-12">
							<div class="thumbnail">
							  <div class="embed-responsive embed-responsive-16by9">
								<iframe class="embed-responsive-item" src="//www.youtube.com/embed/rAHTfV5MDmI" frameborder="0" allowfullscreen></iframe>
							  </div>
							  <div class="caption">
								<h3>Impostazioni - Metodi di Pagamento</h3>
							  </div>
							</div>
						  </div>
						</div>
                    </div>
					<div class="mail-actions">
						<a class="btn btn-sm btn-primary pull-right" href='javascript:void(0);' scroll-to='risposta-0'>
							Torna su <i class="fa fa-mail-reply fa-rotate-90"></i>
						</a>
					</div>
                </div>
            </section>
			
			<div id="risposta-10" class="h15"></div>
			<section class="panel panel-default mail-container">
                <div class="panel-heading"><strong><span class="glyphicon glyphicon-th"></span> Flusso di Caricamento Prodotti/Servizi</strong></div>
                <div class="panel-body">
                    <div class="mail-content">
                       <p>Una volta creato il
						proprio negozio &egrave; necessario inserirvi i prodotti o servizi da
						diffondere verso gli acquirenti. Per fare questo &egrave; sufficiente
						accedere alla pagina &ldquo;Prodotti&rdquo; facendo click sulla
						apposita voce del men&ugrave; a sinistra e cliccare sul tasto
						&ldquo;Aggiungi prodotto&rdquo; presente in alto a destra. A questo
						punto si aprir&agrave; una videata in cui si potranno inserire tutti
						i dati relativi al singolo prodotto da inserire, cominciando dal nome
						del prodotto e dalla sua descrizione. Il nome del prodotto sar&agrave;
						utilizzato per la ricerca da parte degli acquirenti, si consiglia
						quindi un nome &ldquo;parlante&rdquo; o nel caso di prodotti di brand
						particolarmente noti scrivere anche il nome del produttore e/o il
						nome tecnico dell&rsquo;oggetto. Nella descrizione riportare tutte le
						caratteristiche del prodotto o servizio con particolare attenzione a
						mettere in evidenza quelle di maggior interesse da parte del
						pubblico. Si passa quindi ad inserire il prezzo del prodotto, il
						valore da inserire sar&agrave; il valore economico del prodotto
						stesso, senza eventuali spese di spedizione. L&rsquo;aliquota IVA o
						eventuali altre imposte che aggravino il prezzo potranno essere
						inserite nel campo &ldquo;Imposta&rdquo; mentre il sistema calcola in
						automatico il valore del prezzo finale. Si passa quindi a definire la
						disponibilit&agrave; del prodotto potendo impostarla in tre modalit&agrave;
						differenti:</p>
						<p><b>Quantit&agrave;
						DEFINITA</b>, ossia il sistema &egrave; in grado di dare
						disponibilit&agrave; immediata fino ad un numero massimo impostato su
						tale campo per questo prodotto. Superata tale disponibilit&agrave; il
						sistema non permetter&agrave; pi&ugrave; l&rsquo;acquisto diretto ma
						invier&agrave; una richiesta al venditore con i dati della richiesta
						di acquisto per autorizzare la transazione.</p>
						<p><b>Quantit&agrave;
						ILLIMITATA</b>, ossia il sistema porr&agrave; sempre il prodotto in
						condizioni di essere venduto subito.</p>
						<p><b>Richiedi
						Disponibilit&agrave;</b>, con questa opzione il sistema imposta
						sempre una trattativa di vendita con messaggi istantanei tra il
						compratore ed il venditore in modo da poter veicolare un accordo
						sulla tempistica di spedizione che possa prevedere anche il tempo di
						approvvigionamento per lo specifico materiale. Attraverso questa
						opzione &egrave; possibile mettere in vendita beni di cui non si
						dispone fisicamente avendo garanzia del fatto che la transazione di
						vendita non potr&agrave; prescindere da una negoziazione tra le
						parti. (vedi anche <font color="#4f81bd"><i>Flusso
						di verifica disponibilit&agrave;</i></font>).</p>
						<p>Con il tasto &ldquo;Seleziona
						Immagini&rdquo; &egrave; possibile importare sul negozio on line
						foto, disegni ed immagini di ogni tipo a descrizione del prodotto e
						del suo impiego, tali immagini verranno utilizzate nella scheda
						prodotto con la possibilit&agrave; di ingrandimento dei dettagli.</p>
						<p>Caratteristica importante
						per ogni prodotto &egrave; la corretta catalogazione in apposite
						categorie prodotto. Attraverso i tasti di scelta categoria &egrave;
						possibile identificare un percorso che caratterizzi il prodotto in
						una apposita sottocategoria inserita in una specifica categoria a sua
						volta appartenente ad una macrocategoria. Questo meccanismo permette
						di ordinare con criterio i prodotti e di poterli rendere disponibili
						per le ricerche degli acquirenti sia sulla pagina personale del
						negozio su cui risiedono, sia nella ricerca generica per prodotto
						fatta anche fuori dallo specifico sito personale. Per operare la
						scelta migliore quindi selezionare, partendo dalla macrocategoria
						fino ad arrivare alla sottocategoria, la scelta pi&ugrave;
						rispondente al proprio prodotto.</p>
						<p><font color="#4f81bd"><b>ATTENZIONE</b></font>:
						<span style="background: #ffff00">nel caso non venga trovata la
						categoria commerciale adeguata al prodotto/servizio sar&agrave;
						necessario procedere inserendo la voce &ldquo;Altra&rdquo; prevista
						nella lista delle categorie e contestualmente inviare una
						comunicazione al Team Negozy specificando il nome del prodotto
						inserito e suggerendo la macrocategoria, categoria e sottocategoria
						da aggiungere. Non appena la nuova categoria sar&agrave; disponibile
						verrete avvisati con una email. L&rsquo;indirizzo da utilizzare per
						questa e altre tipologie di comunicazione/segnalazione &egrave;:
						</span><font color="#345a8a"><b><span style="background: #ffff00">info@negozy.com</span></b></font></p>
						<p>Oltre alla catalogazione
						del singolo prodotto/servizio &egrave; possibile inserire il prodotto
						stesso in una specifica Collezione. Per creare le collezioni
						personalizzate vedi <font color="#4f81bd"><b>Videata
						Collezioni</b></font>. Oltre quanto previsto nella apposita
						videata di gestione delle collezioni, anche in questa fase sar&agrave;
						possibile inserire il prodotto in una collezione esistente ma anche
						creare una nuova specifica collezione attraverso il tasto &ldquo;+&rdquo;
						posto di lato alla lista delle Collezioni.</p>
						<p>Ultima voce da inserire
						per rendere il prodotto disponibile alla vetrina vendite &egrave; il
						cos&igrave; detto TAG. Si tratta di pi&ugrave; parole che possono
						ricondurre al prodotto, ossia il sistema permette di inserire tutte
						le parole che per affinit&agrave; possono essere riconducibili, e
						soprattutto possano ricondurre al prodotto in base alle ricerche
						effettuate. Le parole inserite in questo campo saranno delle chiavi
						che si attivano sulle ricerche operate dagli acquirenti, quindi se
						nella stringa della ricerca di un potenziale acquirente compare una o
						pi&ugrave; di tali parole il prodotto verr&agrave; selezionato nei
						risultati della ricerca. Tali parole quindi dovranno essere
						rispondenti al prodotto stesso e afferenti alla categoria
						merceologica altrimenti si avr&agrave; l&rsquo;effetto inverso e le
						ricerche non saranno efficaci. Per inserire uno o pi&ugrave; TAG &egrave;
						sufficiente scrivere il testo scelto e premere invio. Per cancellare
						un TAG &egrave; sufficiente cliccare sulla x posta nella sua icona.</p>
						<p>Tutta la messaggistica
						istantanea viene gestita dal tasto in alto a destra che riporta anche
						il numero degli eventuali messaggi non letti.</p>
						
                    </div>
                    <div class="mail-actions">
						<a class="btn btn-sm btn-primary pull-right" href='javascript:void(0);' scroll-to='risposta-0'>
							Torna su <i class="fa fa-mail-reply fa-rotate-90"></i>
						</a>
					</div>
                </div>
            </section>
			
			<div id="risposta-11" class="h15"></div>
			<section class="panel panel-default mail-container">
                <div class="panel-heading"><strong><span class="glyphicon glyphicon-th"></span> Flusso di verifica disponibilit&agrave;</strong></div>
                <div class="panel-body">
                    <div class="mail-content">
                       <p>Nei casi in cui il
						sistema non verifica la disponibilit&agrave; di un prodotto nella
						misura in cui venga richiesto per l&rsquo;acquisto, o nel caso in cui
						il venditore abbia espressamente impostato il parametro di &ldquo;Richiesta
						disponibilit&agrave;&rdquo;,  la piattaforma Negozy.com attiva un
						flusso che ha come obiettivo quello di permettere comunque la
						vendita. Per farlo sono stati predisposti strumenti di messaggistica
						instantanea che avvisano il venditore con popup automatici sul
						portale del potenziale acquirente e della richiesta di disponibilit&agrave;
						per un determinato prodotto. Nella fase di richiesta disponibilit&agrave;
						il compratore rimane in attesa di un riscontro per poter perfezionare
						la vendita ma ha gi&agrave; espresso la sua volont&agrave; di
						acquisto. A questo punto il venditore pu&ograve; rispondere
						immediatamente (all&rsquo;arrivo del popup) dando disponibilit&agrave;
						o rifiutandola, oppure pu&ograve; prendere tempo e gestire la
						richiesta anzich&eacute; direttamente sul popup di attivazione, da
						una apposita area in cui ha sotto controllo tutte le richieste
						(aperte e chiuse). Rispondendo all&rsquo;acquirente potr&agrave;
						quindi verificare i tempi di approvvigionamento e gestire la
						transazione al meglio. Una volta ricevuta conferma da parte del
						venditore, l&rsquo;acquirente procede con il pagamento effettivo.</p>
						<p><br>
						</p>					
                    </div>
                    <div class="mail-actions">
						<a class="btn btn-sm btn-primary pull-right" href='javascript:void(0);' scroll-to='risposta-0'>
							Torna su <i class="fa fa-mail-reply fa-rotate-90"></i>
						</a>
					</div>
                </div>
            </section>
			
			<div id="risposta-12" class="h15"></div>
			<section class="panel panel-default mail-container">
                <div class="panel-heading"><strong><span class="glyphicon glyphicon-th"></span> Videata Prodotti</strong></div>
                <div class="panel-body">
                    <div class="mail-content">
                       <p>Dalla pagina dei prodotti
						si potranno avere a colpo d&rsquo;occhio tutti gli articoli (prodotti
						e/o servizi) caricati sul proprio spazio personale. <span style="background: #ffff00">Tali
						articoli potranno essere poi  pubblicati sul proprio negozio on line
						o rimanere non visibili al pubblico</span>.  Attraverso il tasto
						&ldquo;ELIMINA&rdquo; l&rsquo;articolo verr&agrave; rimosso dal
						proprio sito, in questo caso la piattaforma Negozy manterr&agrave;
						autonomamente traccia di tutti i dati collegati all&rsquo;articolo/servizio
						(foto, descrizioni, prezzo, dati statistici, riscontri da parte della
						community inerenti le attivit&agrave; social di Negozy, etc.) anche i
						documenti ordine gi&agrave; emessi su quel prodotto/servizio prima
						della sua cancellazione verranno mantenuti nella sezione Ordini. Tale
						gestione garantisce la disponibilit&agrave; dello storico sul
						prodotto pur cancellandolo dal negozio per il futuro (tale attivit&agrave;
						&egrave; gestita in automatico dalla piattaforma Negozy e non
						necessita di alcun intervento lato utente).</p>
						<p>Una volta creato il
						prodotto e valorizzate le sue caratteristiche, attraverso il tasto
						&ldquo;MODIFICA&rdquo; si potr&agrave; entrare in una apposita
						videata dedicata alla gestione del singolo prodotto/servizio. In
						questa sezione &egrave; possibile modificare ogni caratteristica
						inerente il singolo articolo semplicemente sovrascrivendo i testi
						presenti (precedentemente inseriti) o togliendo/aggiungendo le
						immagini del prodotto. (Per la corretta compilazione della videata
						fare riferimento al &ldquo;<font color="#4f81bd"><i>Flusso
						di caricamento Prodotti/Servizi</i></font>&rdquo;).</p>
						<p>Dalla lista completa dei
						prodotti del negozio si ha la possibilit&agrave; di selezionare uno o
						pi&ugrave; prodotti spuntando il flag posto alla sinistra di ogni
						immagine. In questo modo verr&agrave; evidenziata in alto una nuova
						funzionalit&agrave; che consentir&agrave; l&rsquo;inserimento
						contestuale di tutti i prodotti/servizi selezionati in una specifica
						Collezione.</p>
						
                    </div>
                    <div class="mail-actions">
						<a class="btn btn-sm btn-primary pull-right" href='javascript:void(0);' scroll-to='risposta-0'>
							Torna su <i class="fa fa-mail-reply fa-rotate-90"></i>
						</a>
					</div>
                </div>
            </section>
			
			<div id="risposta-13" class="h15"></div>
			<section class="panel panel-default mail-container">
                <div class="panel-heading"><strong><span class="glyphicon glyphicon-th"></span> Videata Ordini</strong></div>
                <div class="panel-body">
                    <div class="mail-content">
                       <p>La videata rappresenta un
						riepilogo della situazione relativa alle vendite dei prodotti.
						Attraverso questo riepilogo sar&agrave; possibile avere in tempo
						reale la situazione delle vendite e lo stato delle trattative in
						corso su tutti i prodotti/servizi del proprio negozio on line. Ogni
						riga presente nel riepilogo rappresenta un singolo ordine descritto
						nelle sue caratteristiche di:</p>
						<p><br>
						</p>
						<ul>
							<li><p>Nome del Cliente</p>
							</li><li><p>Nome del prodotto
							ordinato</p>
							</li><li><p>Quantit&agrave; di
							prodotto ordinata</p>
							</li><li><p>Stato dell&rsquo;ordine</p>
							</li><li><p><span style="background: #ffff00">Data</span></p>
							</li><li><p>Tipologia di
							pagamento</p>
							</li><li><p>Totale economico
							dell&rsquo;ordine</p>
							</li><li><p><span style="background: #ffff00">Note</span></p>
						</li></ul>
						<p><br>
						</p>
						<p>I dati relativi al
						prodotto (nome prodotto e prezzo) vengono presi in automatico dalla
						videata su cui si &egrave; inserito il prodotto (vedi <font color="#4f81bd"><i>Videata
						Prodotti</i></font>) mentre i dati relativi alla tipologia di
						pagamento vengono presi dalle impostazioni inserite nella videata in
						cui si sono inseriti i dati relativi al proprio negozio e vengono
						utilizzati per tutti i prodotti del negozio stesso (vedi Videata
						<font color="#4f81bd"><i>Impostazioni -&gt;
						Metodi di Pagamento</i></font>). La quantit&agrave; di
						prodotto ordinata viene richiesta direttamente dal Cliente e, se la
						giacenza dichiarata dal venditore per quel prodotto &egrave;
						sufficiente, rappresenter&agrave; la quantit&agrave; in ordine. Se
						nella videata prodotti il venditore ha impostato una giacenza
						inferiore alla richiesta o ha impostato la funzione di &ldquo;Richiedi
						disponibilit&agrave;&rdquo;, la richiesta da parte del Cliente viene
						gestita attraverso scambio di comunicazioni dirette tra venditore e
						acquirente fino a che il venditore non approva la quantit&agrave;
						richiesta dando vita ad un nuovo ordine. (Per i dettagli di tale
						flusso vedere &ldquo;<font color="#4f81bd"><i>Flusso
						di verifica disponibilit&agrave;</i></font>&rdquo;)</p>
						<p><span style="background: #ffff00">Lo
						stato dell&rsquo;ordine viene impostato automaticamente a seconda
						della situazione in cui l&rsquo;ordine si trova. Tali step sono
						identificati nelle seguenti voci:</span></p>
						<p><span style="background: #ffff00">&ldquo;<b>Richiesta
						Disponibilit&agrave;&rdquo;</b></span><span style="background: #ffff00">,
						&egrave; la situazione in cui un Cliente ha dichiarato di voler
						acquistare un prodotto ma non essendo presente disponibilit&agrave;
						il sistema ha inviato una richiesta di acquisto al venditore. In
						questa situazione il compratore rimane in attesa di un riscontro alla
						sua richiesta di acquisto ed il venditore deve valutare la
						possibilit&agrave; di reperire il prodotto per perfezionare la
						vendita. In tale situazione il Cliente si &egrave; impegnato ad
						acquistare di conseguenza la risposta da parte del venditore dovrebbe
						essere gestita nel giro di poco tempo o contestualmente alla
						richiesta stessa in modo da non bloccare il flusso della vendita. Se
						trascorre troppo tempo la vendita (in realt&agrave; gi&agrave;
						definita) potrebbe sfumare. Cliccando sullo stato &ldquo;Richiesta
						Disponibilit&agrave;&rdquo; si potr&agrave; accedere alla lista
						completa delle richieste ancora aperte, nonch&eacute; alla lista
						delle richeiste chiuse).</span></p>
						<p><span style="background: #ffff00">&ldquo;<b>Rifiutato
						dal venditore&rdquo;</b></span><span style="background: #ffff00">, &egrave;
						la situazione in cui il venditore non conferma la disponibilit&agrave;
						del prodotto e nega la vendita dello stesso. Tale stato &egrave;
						conclusivo e l&rsquo;ordine rimarr&agrave; come storico nella lista
						degli ordini con tale stato.</span></p>
						<p><span style="background: #ffff00">&ldquo;<b>In
						Attesa di Pagamento&rdquo;</b></span><span style="background: #ffff00">,
						&egrave; la situazione in cui il Cliente ha effettuato il pagamento
						ed il venditore avr&agrave; la possibilit&agrave; di verificare
						l&rsquo;incasso prima di effettuare la spedizione. Fino a questo
						stato il sistema procede in automatico, a questo punto il venditore
						avr&agrave; modo di operare manualmente il cambio stato una volta
						verificato l&rsquo;incasso, o comunque a sua discrezione, facendo
						procedere il flusso verso la spedizione del prodotto (vedi sotto).
						Cliccando sullo stato &ldquo;In Attesa di Pagamento&rdquo; si potr&agrave;
						accedere alla videata di riepilogo dei dati lasciati dal compratore,
						ivi compresi i dati relativi alla spedizione.</span></p>
						<p><span style="background: #ffff00">&ldquo;<b>In
						Attesa di Spedizione&rdquo;</b></span><span style="background: #ffff00">,
						&egrave; lo stato in cui il venditore pu&ograve; impostare l&rsquo;ordine
						nel caso in cui la spedizione non sia ancora pronta. Cliccando sullo
						stato &ldquo;In Attesa di Spedizione&rdquo; si potr&agrave; accedere
						alla videata di riepilogo dei dati lasciati dal compratore, ivi
						compresi i dati relativi alla spedizione.</span></p>
						<p><span style="background: #ffff00">&ldquo;<b>Spedito&rdquo;</b></span><span style="background: #ffff00">,
						&egrave; lo stato in cui il venditore pu&ograve; impostare l&rsquo;ordine
						nel caso in cui abbia gi&agrave; approntato la spedizione verso il
						compratore.</span></p>
						<p><span style="background: #ffff00">&ldquo;<b>Chiuso&rdquo;</b></span><span style="background: #ffff00">,
						&egrave; lo stato in cui il venditore pu&ograve; impostare l&rsquo;ordine
						nel momento in cui il vettore da lui scelto per la spedizione
						confermi l&rsquo;avvenuta consegna del prodotto al compratore. Lo
						stato Chiuso&rdquo; determina la fine del flusso e l&rsquo;avvenuta
						vendita.</span></p>
						<p><span style="background: #ffff00">Lungo
						tutto il flusso il venditore ha la possibilit&agrave; di annotare
						informazioni relative all&rsquo;ordine, al compratore o altro
						nell&rsquo;apposito spazio per le note presente sulla riga
						dell&rsquo;ordine.</span></p>
                    </div>
                    <div class="mail-actions">
						<a class="btn btn-sm btn-primary pull-right" href='javascript:void(0);' scroll-to='risposta-0'>
							Torna su <i class="fa fa-mail-reply fa-rotate-90"></i>
						</a>
					</div>
                </div>
            </section>
			
			<div id="risposta-14" class="h15"></div>
			<section class="panel panel-default mail-container">
                <div class="panel-heading"><strong><span class="glyphicon glyphicon-th"></span> Videata Collezioni</strong></div>
                <div class="panel-body">
                    <div class="mail-content">
                       <p>Nella home page del
						proprio sito web verr&agrave; predisposto uno spazio, appena sotto la
						foto principale del negozio, dedicato ai box vetrina. Ogni box
						vetrina da la possibilit&agrave; di mettere in evidenza alcuni
						articoli in base alle intenzioni del venditore: ad esempio alcuni
						articoli possono essere raggruppati perch&eacute; coinvolti da una
						particolare offerta, o articoli anche eterogenei possano essere
						presentati raggruppati perch&eacute; facenti parte di un set completo
						o di un accostamento particolarmente accattivante per l&rsquo;acquirente
						o ancora nei casi in cui il venditore voglia presentare cataloghi
						specifici dedicati a particolari eventi o festivit&agrave;. Tali
						vetrine possono chiaramente essere utilizzate anche allo scopo di
						catalogare e rendere facilmente raggiungibili i prodotti/servizi per
						categorie omogenee, quindi rappresentare la effettiva catalogazione
						dei prodotti per tipologia, area d&rsquo;impiego, dimensioni, modelli
						etc.</p>
						<p>Per ottenere questo
						livello di raggruppamento dei prodotti/servizi si possono utilizzare
						le <font color="#4f81bd"><b>Collezioni</b></font>,
						accedendo alla videata Collezioni viene presentata la possibilit&agrave;
						di creare (o modificare in un secondo momento) le singole collezioni
						desiderate. Il risultato sar&agrave; che ogni collezione creata
						apparir&agrave; sulla propria home page secondo le caratteristiche
						impostate (foto, colore, descrizione) e conterr&agrave; tutti e soli
						gli articoli inseriti. Tali articoli continueranno a far parte anche
						della lista completa delle disponibilit&agrave; presenti nella home e
						raggiungibili dalla ricerca sul sito, ma se inseriti in una specifica
						collezione potranno apparire all&rsquo;acquirente entrando nella
						collezione stessa e saranno presentati insieme agli articoli raccolti
						nella specifica collezione selezionata.</p>
						<p>Per creare una nuova
						Collezione &egrave; sufficiente inserire un nome da associare alla
						collezione (primo box dall&rsquo;alto). Tale nome apparir&agrave;
						sulla home page come nome della collezione. Successivamente si ha la
						possibilit&agrave; di inserire una descrizione dettagliata della
						collezione specifica, chiaramente tale descrizione dovr&agrave;
						raccordare tutti gli articoli che si intende inserire nella
						collezione cercando di incentivare l&rsquo;obiettivo della collezione
						stessa, ossia enfatizzare la bellezza del set completo dei prodotti
						(non solo del singolo prodotto), l&rsquo;importanza dell&rsquo;iniziativa
						che raccorpa i prodotti della collezione, la convenienza economica
						dei prodotti presenti se si tratta di una collezione destinata ai
						prodotti scontati etc. etc. In generale lo scopo &egrave; quello di
						descrivere ed incentivare le caratteristiche comuni a tutti gli
						articoli che si intende inserire nella collezione. Lo scopo delle
						collezioni pu&ograve; anche essere, pi&ugrave; semplicemente, quello
						di creare un catalogo omogeneo di prodotti che possa raccoglierli per
						le varie categorie e caratteristiche omogenee.</p>
						<p>Il passo successivo &egrave;
						quello di impostare un colore di sfondo ed una immagine/foto da
						associare alla specifica collezione, tali parametri grafici saranno
						utilizzati per presentare la collezione nella home page del sito
						personale.</p>
						
                    </div>
                    <div class="mail-actions">
						<a class="btn btn-sm btn-primary pull-right" href='javascript:void(0);' scroll-to='risposta-0'>
							Torna su <i class="fa fa-mail-reply fa-rotate-90"></i>
						</a>
					</div>
                </div>
            </section>
			
			
			
			
			
			
			<!--
			<section id="risposta-" class="panel panel-default mail-container">
                <div class="panel-heading"><strong><span class="glyphicon glyphicon-th"></span> Titolo</strong></div>
                <div class="panel-body">
                    <div class="mail-content">
                       testo
						
                    </div>
                    <div class="mail-actions">
                        <a href="#/mail/compose" class="btn btn-sm btn-primary">Reply <i class="fa fa-mail-reply"></i></a>
                        <a href="#/mail/compose" class="btn btn-sm btn-default">Forward <i class="fa fa-mail-forward"></i></a>
                    </div>
                </div>
            </section>
			-->
			
        </div>
    </div>
  </div>
</div>
