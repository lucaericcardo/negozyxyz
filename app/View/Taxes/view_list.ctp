<div class="lapagina" data-ng-controller="TaxsCtrl">
    <nav class="navbar navbar-default navbar-static-top" role="navigation">
        <div class="navbar-header">
            <ol class="breadcrumb nav navbar-nav">
                <li class="active"><?php echo __("Impostazioni"); ?></li>
                <li class="active"><?php echo __("Imposte"); ?></li>
            </ol>
            <ul class="nav navbar-nav navbar-right">
                <div class="navbar-form">
                    <button class="btn btn-success" ng-click="open()"><i class="fa fa-plus"></i> <?php echo __("Aggiungi imposta"); ?></button>
                </div>
            </ul>
        </div>
    </nav>


    <div ng-if="!taxes.length" class="page-err">
        <div class="alert alert-danger" style="margin-bottom:0" data-ng-if="!settingStatus.taxes">
            <i class="fa fa-exclamation-triangle"></i> <?php echo __("Per poter pubblicare lo shop è necessario impostare correttamente almeno un'imposta"); ?>
        </div>
        <div class="err-container">
            <div class="text-center">
                <div class="err-status">
                    <h1><i class="fa fa-university"></i></h1>
                </div>
                <div class="err-message">
                    <h2><?php echo __("Attualmente non sono presenti imposte."); ?></h2>
                </div>
                <br />
                <button ng-click="open()" class="btn btn-default btn-lg"><i class="fa fa-plus"></i> <?php echo __("Aggiungi nuova imposta"); ?></button>
                <br >
                <br >
                <br >
            </div>
        </div>
    </div>

    <div ng-if="taxes.length" class="page" >
        <div class="row">
            <div class="col-lg-10 col-lg-offset-1 col-md-12">
            <div class="alert alert-danger" data-ng-if="!settingStatus.taxes">
                <i class="fa fa-exclamation-triangle"></i> <?php echo __("Per poter pubblicare lo shop è necessario impostare correttamente almeno un'imposta"); ?>
            </div>
            <section class="panel panel-default table-dynamic">
            	<div class="panel-heading">
                    <h4><?php echo __("Scegli e configura le imposte che desideri applicare"); ?></h4>
                </div>
                <div class="panel-body">
                <div class="table-responsive">
                    <table class="table table-bordered table-striped table-responsive">
                        <thead>
                        <tr>
                            <th><div class="th">
                                    <?php echo __("Nome"); ?>
                                </div></th>
                            <th><div class="th">
                                    <?php echo __("Tipo"); ?>
                                </div></th>
                            <th><div class="th">
                                    <?php echo __("Valore imposta"); ?>
                                </div></th>
                            <th><div class="th">
                                    <?php echo __("Prezzi inclusi"); ?>
                                </div></th>
                            <th class="text-right" width="120">
                            	<div class="th">
                                    <?php echo __("Azioni"); ?>
                                </div>
                            </th>
                        </tr>
                        </thead>
                        <tbody>
                        <tr data-ng-repeat="tax in taxes">
                            <td>{{tax.label}}</td>
                            <td>{{tax.name}}</td>
                            <td>{{tax.tax}} %</td>
                            <td>
                                <span data-ng-if="tax.product_tax" class="label label-success"><i class="fa fa-check"></i> </span>
                                <span data-ng-if="!tax.product_tax" class="label label-danger"><i class="fa fa-times"></i> </span>
                            </td>
                            <td class="text-right">
                                <a data-ng-if="tax.id" href="javascript:;" class="btn-icon btn-icon-sm bg-primary" data-ng-click="editModal(tax.id)"><i class="fa fa-pencil-square-o"></i> <?php //echo __("Modifica"); ?></a>
                                <a data-ng-if="tax.id" href="javascript:;" class="btn-icon btn-icon-sm bg-danger" confirm-callback="deleteTax(tax.id)" confirm-message="<?php echo __("Sei sicuro di voler eleminare l'imposta?"); ?>" confirm-modal>
                                    <i class="glyphicon glyphicon-trash"></i>
                                    <?php //echo __("Elimina"); ?>
                                </a>
                            </td>
                        </tr>
                        </tbody>
                    </table>
                </div>
                </div>
            </section>
            </div>
        </div>
    </div>
</div>
