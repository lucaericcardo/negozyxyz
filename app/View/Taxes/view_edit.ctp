<div class="modal-header">
    <h3 class="modal-title"><?php echo __("Modifica imposta"); ?></h3>
</div>
<div class="modal-body">
    <form name="addTax" class="form-horizontal form-validation">
        <div class="form-group">
            <label for="" class="col-sm-4"><?php echo __("Tipo"); ?></label>
            <div class="col-sm-8">
                <input type="text" data-ng-change="updateLabel()" class="form-control ng-pristine ng-valid" required  data-ng-model="tax.name">
            </div>
        </div>
        <div class="form-group">
            <label for="" class="col-sm-4"><?php echo __("Valore Imposta"); ?> %</label>
            <div class="col-sm-8">
                <input data-min="0" data-max="100" type="number" data-ng-change="updateLabel()" required  class="form-control ng-pristine ng-valid" data-ng-model="tax.tax">
            </div>
        </div>
        <div class="form-group">
            <label for="" class="col-sm-4"><?php echo __("Nome"); ?></label>
            <div class="col-sm-8">
                <input type="text" class="form-control ng-pristine ng-valid" required  data-ng-model="tax.label" disabled="disabled">
            </div>
        </div>
        <div class="row">
            <div class="col-sm-6">
                <div class="well">
                    <label class="ui-radio" for="inclusa">
                        <input data-ng-value="true" type="radio" data-ng-model="tax.product_tax" name="radio1" id="inclusa">
                        <span>L'imposta sarà <b>inclusa</b> ai prezzi dei vostri prodotti e alle spese di spedizione.</span>
                    </label>
                    <div class="divider"></div>
                </div>
            </div>
            <div class="col-sm-6">
                <div class="well">
                    <label class="ui-radio" for="esclusa">
                        <input data-ng-value="false" type="radio" data-ng-model="tax.product_tax" name="radio1" id="esclusa">
                        <span>L'imposta sarà <b>esclusa</b> dai prezzi dei vostri prodotti e alle spese di spedizione.</span>
                    </label>
                    <div class="divider"></div>
                </div>
            </div>
        </div>
    </form>
</div>
<div class="modal-footer">
    <button class="btn btn-primary"  ng-disabled="addTax.$invalid" ng-click="ok()"><?php echo __("Salva"); ?></button>
    <button class="btn btn-warning" ng-click="cancel()"><?php echo __("Annulla"); ?></button>
</div>