<div class="modal-header">
    <h3 class="modal-title"><?php echo __("Pubblica il tuo shop"); ?></h3>
</div>
<div class="modal-body">
    <div data-ng-show="(!settingStatus.taxes || !settingStatus.shipping || !settingStatus.payments) && !isShopPublic && !activity_indicator">
        <h4><?php echo __('Prima di pubblicare il tuo shop devi'); ?>:</h4>
        <ul>
            <li data-ng-show="!settingStatus.taxes"><i class="fa fa-exclamation-triangle text-danger"></i> <?php echo __("Completare la sezione delle"); ?>
                <strong><?php echo __('Imposte'); ?></strong>
            </li>
            <li data-ng-show="!settingStatus.shipping"><i class="fa fa-exclamation-triangle text-danger"></i> <?php echo __("Completare la sezione delle"); ?>
                <strong><?php echo __('Spedizioni'); ?></strong>
            </li>
            <li data-ng-show="!settingStatus.payments"><i class="fa fa-exclamation-triangle text-danger"></i> <?php echo __("Completare la sezione delle"); ?>
                <strong><?php echo __('Metodi Pagamento'); ?></strong>
            </li>
        </ul>
    </div>
    <div class="clearfix" data-ng-show="count_actions != 0 && (!settingStatus.taxes || !settingStatus.shipping || !settingStatus.payments || !settingStatus.terms)"></div>
    <div data-ng-show="count_actions != 0 && !public && !activity_indicator && !isShopPublic">
        <h4><?php echo __('Prima di pubblicare il tuo shop è consigliato eseguire le seguenti azioni'); ?>:</h4>
        <ul>
            <li data-ng-repeat="action in actions"><i class="fa fa-exclamation-circle text-warning"></i> {{action}}</li>
        </ul>
    </div>
    <div data-ng-show="activity_indicator" ng-activity-indicator="DottedDark" skip-ng-show="yes" style="margin-left: 45%"></div>
    <blockquote data-ng-show="public || isShopPublic">
        <span class="fa-stack fa-4x text-primary">
          <i class="fa fa-circle fa-stack-2x"></i>
          <i class="fa fa-check fa-stack-1x fa-inverse"></i>
        </span>
        <small><?php echo __('Il tuo shop è stato pubblicato correttamente con il seguente indirizzo'); ?></small>
        <p><a href="{{shop.site_url}}">{{shop.site_url}}</a></p>
    </blockquote>
    <div class="clearfix" data-ng-show="count_actions != 0 && (!settingStatus.taxes || !settingStatus.shipping || !settingStatus.payments || !settingStatus.terms)"></div>
</div>
<div class="modal-footer">
    <button
        class="btn btn-primary"
        data-ng-hide="(!settingStatus.taxes || !settingStatus.shipping || !settingStatus.payments || !settingStatus.terms) || count_actions != 0 && !public && !isShopPublic"
        data-ng-click="refreshPage()"
    >
        <?php echo __("Ok"); ?>
    </button>
    <button
        class="btn btn-primary"
        data-ng-show="(!settingStatus.taxes || !settingStatus.shipping || !settingStatus.payments || !settingStatus.terms) || count_actions != 0 && !public && !isShopPublic"
        data-ng-click="cancel()"
    >
        <?php echo __("Ok, ho capito!"); ?>
    </button>
    <button
        class="btn btn-success"
        data-ng-show="!(!settingStatus.taxes || !settingStatus.shipping || !settingStatus.payments || !settingStatus.terms) && count_actions != 0 && !public && !isShopPublic"
        data-ng-click="publicShop()"
    >
        <?php echo __("Pubblica ugualmente"); ?>
    </button>
</div>