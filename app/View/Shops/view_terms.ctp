<div class="lapagina">
    <nav class="navbar navbar-default navbar-static-top" role="navigation">
        <div class="navbar-header">
            <ol class="breadcrumb nav navbar-nav">
                <li class="active"><?php echo __("Impostazioni"); ?></li>
                <li class="active"><?php echo __("Aspetti legali"); ?></li>
            </ol>
        </div>
    </nav>
    <div class="page" data-ng-controller="TermsCtrl">

        <div class="row">
            <div class="col-lg-10 col-lg-offset-1 col-md-12">
            	<div class="alert alert-danger" data-ng-if="!settingStatus.terms">
                	<i class="fa fa-exclamation-triangle"></i> <?php echo __("Per poter pubblicare lo shop è necessario compilare almeno un aspetto legale"); ?>
                </div>
            
                <!-- Input -->
                <section class="panel panel-default">
                    <div class="panel-heading"><strong><?php echo __("Termini e condizioni"); ?></strong></div>
                    <div class="panel-body">
                        <form name="product_data" class="form-validation">
                            <div class="form-horizontal">
                                <div class="form-group">
                                    <label for="label-focus" class="col-sm-2">
										<?php echo __("Testo"); ?><div class="divider"></div>
                                    </label>
                                    <div class="col-sm-10">
                                        <div text-angular data-ng-model="terms.terms"></div>
                                    </div>
                                </div>
                            </div>
                        </form>
                    </div>
                </section>
                <section class="panel panel-default">
                    <div class="panel-heading"><strong><?php echo __("Privacy"); ?></strong></div>
                    <div class="panel-body">
                        <form name="product_data" class="form-validation">
                            <div class="form-horizontal">
                                <div class="form-group">
                                    <label for="label-focus" class="col-sm-2"><?php echo __("Testo"); ?></label>
                                    <div class="col-sm-10">
                                        <div text-angular data-ng-model="terms.privacy"></div>
                                    </div>
                                </div>
                            </div>
                        </form>
                    </div>
                </section>
                <section class="panel panel-default">
                    <div class="panel-heading"><strong><?php echo __("Policy di cancellazione"); ?></strong></div>
                    <div class="panel-body">
                        <form name="product_data" class="form-validation">
                            <div class="form-horizontal">
                                <div class="form-group">
                                    <label for="label-focus" class="col-sm-2"><?php echo __("Testo"); ?></label>
                                    <div class="col-sm-10">
                                        <div text-angular data-ng-model="terms.cancellationPolicy"></div>
                                    </div>
                                </div>
                            </div>
                        </form>
                    </div>
                </section>
                <!-- end Input -->
                <button data-ng-click="saveTerms()" class="btn btn-success btn-lg" type="button"><?php echo __("Salva"); ?></button>
            </div>
        </div>
    </div>
</div>
