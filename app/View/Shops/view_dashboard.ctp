<div class="lapagina" data-ng-controller="DashboardCtrl">
	<nav class="navbar navbar-default navbar-static-top" role="navigation">
    	<div class="navbar-header">
            <ol class="breadcrumb nav navbar-nav">
                <li class="active"><?php echo __("Dashboard"); ?></li>
            </ol>
            <ul class="nav navbar-nav navbar-right">
                <div class="navbar-form">
                    <button data-ng-show="!isShopPublic" class="btn btn-success" data-ng-click="publicShop()"><i class="fa fa-cloud-upload"></i> <?php echo __("Pubblica il tuo shop"); ?></button>
                </div>
            </ul>
        </div>
	</nav>
    <div class="page page-about">
    	<div class="row">
        	<div class="col-lg-10 col-lg-offset-1 col-md-12">
                <div class="page-container">
                    <div>
                        <h2><strong><?php echo __("Benvenuto"); ?></strong> {{dashboardData.name}}</h2>
                        <p class="lead"><?php echo __("Di seguito i dati relativi al tuo negozio all'interno del portale negozy.com"); ?></p>
                    </div>
                    <h2><?php echo __("Attualmente hai"); ?>:</h2>
                    <div class="row stat-container">
                        <div class="col-sm-4">
                            <section class="stat-item">
                                <span class="stat-num">{{dashboardData.product_count}}</span>
                                <span class="stat-desc"><?php echo __("Prodotti caricati"); ?></span>
                            </section>
                        </div>
                        <div class="col-sm-4">
                            <section class="stat-item">
                                <span class="stat-num">{{dashboardData.order_count}}</span>
                                <span class="stat-desc"><?php echo __("Nuovi ordini"); ?></span>
                            </section>
                        </div>
                        <div class="col-sm-4">
                            <section class="stat-item">
                                <span class="stat-num">{{dashboardData.product_category_count}}</span>
                                <span class="stat-desc"><?php echo __("Collezioni"); ?></span>
                            </section>
                        </div>
                    </div>
                    
                    <div data-ng-show="!dashboardData.product_count" class="row info-container">
                        <div class="col-lg-12 col-md-12">
                            <h2><?php echo __("Come posso inserire un nuovo prodotto?"); ?></h2>
                            <p class="lead"><?php echo __("Nella sidebar qui a fianco, cliccando sulla voce \"Prodotti\", si visualizzerà la lista con tutti i prodotti caricati."); ?></p>
                            <p class="desc"><?php echo __("Per poter aggiungere un nuovo prodotto, basterà cliccare sul pulsante in alto a destra \"Aggiungi Prodotto\"."); ?></p>
                        </div>
                       <!--
                        <div class="col-lg-8 col-md-6">
                            <h2><strong>Come posso farmi pagare</strong> se vendo un prodotto?</h2>
                            <p class="lead">Clicca sul pulsante qua sotto per poter aprire un conto su PayPal.com, altrimenti se già sei in possesso di un acount, puoi impostarlo cliccando qui a fianco sul pulsante "Impostazioni" e successivamente Scegliere "Metodi di Pagamento".</p>
                            <p class="desc">Milioni di clienti usano PayPal per un semplice motivo: è facile.<br /> Inserisci solo email e password e in pochi clic il pagamento è completato.</p>
                            <a class="btn btn-line-primary btn-w-md" href="https://www.paypal.com/signup/account?country.x=IT&locale.x=it_IT" target="_blank">Apri un conto PayPal</a>
                        </div>
                        -->
                    </div>
            
                </div>
            </div>
        </div>
    </div>
</div>
