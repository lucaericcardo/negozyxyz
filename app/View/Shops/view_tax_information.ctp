<div class="lapagina">
    <nav class="navbar navbar-default navbar-static-top" role="navigation">
        <div class="navbar-header">
            <ol class="breadcrumb nav navbar-nav">
                <li class="active"><?php echo __("Impostazioni"); ?></li>
                <li class="active"><?php echo __("Dati fiscali"); ?></li>
            </ol>
        </div>
    </nav>
    <div class="page" data-ng-controller="TaxInformationCtrl">
        <div class="row">
            <div class="col-lg-10 col-lg-offset-1 col-md-12">
                <section class="panel panel-default">
                    <div class="panel-heading"><strong><?php echo __("Dati fiscali"); ?></strong></div>
                    <div class="panel-body">
                        <form name="product_tax_information" class="form-validation">
                        <div class="form-horizontal">
                            <div class="form-group">
                                <label for="" class="col-sm-2"><?php echo __("Ragione Sociale"); ?></label>
                                <div class="col-sm-10">
                                    <input type="text" required="required" data-ng-model="shop.business_name" class="form-control">
                                </div>
                            </div>
                            <div class="form-group">
                                <label for="" class="col-sm-2"><?php echo __("P.IVA"); ?></label>
                                <div class="col-sm-10">
                                    <input type="text" required="required" data-ng-model="shop.vat_number" class="form-control">
                                </div>
                            </div>
                            <div class="form-group">
                                <label for="label-focus" class="col-sm-2"><?php echo __("Indirizzo"); ?></label>
                                <div class="col-sm-10">
                                    <input type="text" required="required" data-ng-model="shop.address" class="form-control">
                                </div>
                            </div>
                            <div class="form-group">
                                <label for="label-focus" class="col-sm-2"><?php echo __("CAP"); ?></label>
                                <div class="col-sm-10">
                                    <input type="text" required="required" data-ng-model="shop.cap" class="form-control">
                                </div>
                            </div>
                            <div class="form-group">
                                <label for="label-focus" class="col-sm-2"><?php echo __("Città"); ?></label>
                                <div class="col-sm-10">
                                    <input type="text" required="required" data-ng-model="shop.city" class="form-control">
                                </div>
                            </div>
                            <div class="form-group">
                                <label for="label-focus" class="col-sm-2"><?php echo __("Provincia"); ?></label>
                                <div class="col-sm-10">
                                    <input type="text" required="required" data-ng-model="shop.province" class="form-control">
                                </div>
                            </div>
                            <div class="form-group">
                                <label for="label-focus" class="col-sm-2"><?php echo __("Stato"); ?></label>
                                <div class="col-sm-10">
                                    <select data-ng-model="shop.state" class="form-control ng-pristine ng-valid" data-ng-options="country.alpha as country.name for country in countries"></select>
                                </div>
                            </div>
                            <div class="form-group">
                                <label for="" class="col-sm-2"><?php echo __("Valuta"); ?></label>
                                <div class="col-sm-10">
                                    <select data-ng-model="shop.currency" class="form-control" data-ng-options="currency.id as currency.name for currency in currencies"></select>
                                </div>
                            </div>
                        </div>
                        </form>
                    </div>
                </section>
                <button data-ng-click="save()" ng-disabled="product_tax_information.$invalid" class="btn btn-success btn-lg" type="button">Salva tutto</button>
            </div>
        </div>
    </div>
</div>
