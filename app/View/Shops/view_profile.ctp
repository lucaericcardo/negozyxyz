<div class="lapagina">
  <nav class="navbar navbar-default navbar-static-top" role="navigation">
    <div class="navbar-header">
	  <ul class="nav navbar-nav">
	  	<li>
		  <ol class="breadcrumb">
			<li><a href="#/home">Bacheca</a></li>  
			<li class="active">Negozio</li>
		  </ol>
		</li>
	  </ul>
      
    </div>
  </nav>
  
  
  <div class="page page-services">
    <div class="row">
      <div class="col-lg-8 col-lg-offset-2 col-md-10 col-md-offset-1">
	    <section class="panel panel-box copertina relative">
					<div class="panel-left panel-item bg-info">
                    	<!-- pulsante che appare quando una persona è da seguire -->
                        <a href="#/products/add" class="btn btn-lg btn-success segui_btn" type="submit"><i class="fa fa-plus"></i> <?php echo __("Segui"); ?></a>
                        
                    	<a class="pull-right profile avatar_negozio" href="">
                            <img class="img110_110" src="/asset/admin/images/pixmania.jpg" alt="">
                        </a>
						<!--<div><img src="/app/webroot/asset/admin/images/pixmania.jpg" alt=""></div>-->
						<h3 class="ng-binding text-left titolo_shop text-thin">Ferramenta Trambusti</h3>
						<p>Aenean quis eros pretium, tempus arcu id, viverra purus. Praesent vel blandit diam. Donec pharetra et ipsum non porta. Suspendisse sollicitudin elementum nisi eget aliquet. Maecenas semper tortor lectus, id sollicitudin urna blandit vitae. Morbi id nisl eu massa tincidunt dignissim. Sed nec nunc accumsan, commodo felis nec, blandit est.</p>
						
                        
                        <!-- pulsante che appare quando una persona è seguita -->
                        <!--<a href="#/products/add" class="btn btn-lg btn-warning" type="submit"><i class="fa fa-check"></i> <?php echo __("Segui già"); ?></a>-->
					</div>
					<div style="background-image: url(/img/foto_negozio.jpeg);" class="panel-right panel-item bg-reverse"></div>
				</section>
		
        
        <div class="row">
          <div class="col-sm-6">
            <div class="panel mini-box"> <span class="box-icon bg-success"> <i class="fa fa-shopping-cart"></i></span>
              <div class="box-info">
                <p class="size-h2">5</p>
                <p class="text-muted">Categorie</p>
              </div>
            </div>
          </div>
          <div class="col-sm-6">
            <div class="panel mini-box"> <span class="box-icon bg-info"> <i class="fa fa-check"></i> </span>
              <div class="box-info">
                <p class="size-h2">84</p>
                <p class="text-muted">Prodotti</p>
              </div>
            </div>
          </div>
        </div>
        <div class="panel panel-default">
          <div class="panel-heading"><strong><span class="glyphicon glyphicon-th"></span> Profile Info</strong></div>
          <div class="panel-body">
            <div class="media">
              <div class="media-body">
                <ul class="list-unstyled list-info">
                  <li class="ng-binding"> <span class="icon glyphicon glyphicon-user"></span>
                    <label>User name</label>
                    Lisa Doe </li>
                  <li> <span class="icon glyphicon glyphicon-envelope"></span>
                    <label>Email</label>
                    name@example.com </li>
                  <li> <span class="icon glyphicon glyphicon-home"></span>
                    <label>Address</label>
                    Street 123, Avenue 45, Country </li>
                  <li> <span class="icon glyphicon glyphicon-earphone"></span>
                    <label>Contact</label>
                    (+012) 345 6789 </li>
                  <li> <span class="icon glyphicon glyphicon-flag"></span>
                    <label>Nationality</label>
                    Australia </li>
                </ul>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
  
  
</div>
