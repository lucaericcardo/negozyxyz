<div class="lapagina">
    <div class="page" data-ng-controller="shopCtrl" id="addNegozio">
        <div class="row">
            <div class="col-lg-6 col-lg-offset-3 col-md-8 col-md-offset-2 col-sm-12">
                <section class="panel panel-default">
            <div class="panel-heading"><strong><?php echo __("Aggiungi il tuo negozio") ?></strong></div>
            <div class="panel-body">
                <wizard on-finish="finishedWizard()">
                    <wz-step>
                        <div>
                            <h2 class="text-center text-thin"><?php echo __("Iniziamo!"); ?></h2>
                            <div ng-if="showError" class="alert alert-danger">
                                Compilare correttamente i dati
                            </div>
                            <div class="row">
                                <form name="shop_name_form" class="form-validation" >
                                    <div class="form-group">
                                        <label for=""> <?php echo __("Seleziona il tuo paese"); ?> </label>
                                        <select  data-ng-options="country.alpha as country.name for country in countries" data-ng-model="shop.state" class="form-control input-lg"></select>
                                    </div>
                                    <div class="form-group">
                                        <label for=""> <?php echo __("Partita IVA o Codice fiscale"); ?> </label>
                                        <input data-ng-model="shop.vat_number" ng-pattern="/^[A-Za-z0-9]*$/"  required  type="text" class="form-control input-lg" placeholder="<?php echo __("Es: 0123456789"); ?>" />
                                        <span ng-if="!availableVatNumber" class="text-danger"><?php echo __("Partiva iva non disponibile"); ?></span>
                                    </div>
                                </form>
                            </div>
                            <hr />
                            <input class="btn btn-success pull-right btn-lg" type="button" data-ng-click="validate_data_first_step();" value="<?php echo __("Avanti"); ?>" />
                        </div>
                    </wz-step>
                    <wz-step>
                        <div>
                            <h2 class="text-center text-thin" ><?php echo __("Inserisci i dati fiscali della tua attività."); ?></h2>
                            <div ng-if="showErrorData" class="alert alert-danger">
                                Compilare correttamente i dati
                            </div>
                            <form name="shop_data_form"  class="form-validation"  novalidate>
                                <div class="row">
                                    <div class="form-group">
                                        <div class="col-md-6">
                                            <label for=""><?php echo __("Ragione Sociale"); ?></label>
                                            <input required type="text" data-ng-model="shop.business_name" name="business_name" class="form-control input-lg">
                                        </div>
                                        <div class="col-md-6">
                                            <label for=""><?php echo __("Partita IVA o Codice Fiscale"); ?></label>
                                            <input required type="text" data-ng-model="shop.vat_number" name="vat_number" class="form-control input-lg">
                                        </div>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="form-group">
                                        <div class="col-md-12">
                                            <label for=""><?php echo __("Indirizzo"); ?></label>
                                            <input required type="text" data-ng-model="shop.address" name="address" class="form-control input-lg">
                                        </div>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="form-group">
                                        <div class="col-md-6">
                                            <label for=""><?php echo __("Città"); ?></label>
                                            <input required type="text" data-ng-model="shop.city" name="city" class="form-control input-lg">
                                        </div>
                                        <div class="col-md-6">
                                            <label for=""><?php echo __("CAP"); ?></label>
                                            <input required type="text" ng-pattern="/^(\d{5}(-\d{4})?|[A-Z]\d[A-Z] *\d[A-Z]\d)$/" data-ng-model="shop.cap" name="cap" class="form-control input-lg">
                                        </div>
                                    </div>
                                </div>

                                <div class="row">
                                    <div class="form-group">
                                        <div class="col-md-6">
                                            <label for=""><?php echo __("Stato"); ?></label>
                                            <select  data-ng-options="country.alpha as country.name for country in countries" data-ng-model="shop.state" class="form-control input-lg"></select>
                                        </div>
                                        <div class="col-md-6">
                                            <label for=""><?php echo __("Provincia"); ?></label>
                                            <input required type="text" data-ng-model="shop.province" name="province" class="form-control input-lg">
                                        </div>
                                    </div>
                                </div>
                            </form>
                            <hr />
                            <a href="javascript:void(0)" class="pull-left" wz-previous ><?php echo __("Indietro"); ?></a>
                            <input  class="btn btn-success pull-right btn-lg" ng-click="checkShopData();"  type="button" value="<?php echo __("Avanti"); ?>" />
                        </div>
                    </wz-step>
                    <wz-step>
                        <div>
                            <h2 class="text-center text-thin"><?php echo __("Inserici le informazioni del tuo negozio"); ?></h2>
                            <div ng-if="showError" class="alert alert-danger">
                                Compilare correttamente i dati
                            </div>
                            <div class="row">
                                <form name="shop_name_form" class="form-validation" >
                                    <div class="form-group">
                                        <label for=""> <?php echo __("Nome del Negozio"); ?> </label>
                                        <input data-ng-model="shop.name" required  type="text" class="form-control input-lg" placeholder="<?php echo __("Inserici il nome del negozio"); ?>" />
                                    </div>
                                    <div class="form-group">
                                        <label for=""> <?php echo __("Categoria Merceologica"); ?> </label>
                                        <select data-ng-options="category.id as category.label for category in categories "  data-ng-model="shop.macrocategory_id" class="form-control input-lg">
                                        </select>
                                    </div>
                                    <div class="form-group">
                                        <label for=""><?php echo __("Url del Negozio"); ?></label>
                                        <div class="input-group">
                                            <input required data-ng-model="shop.url" ng-minlength="5" ng-pattern="/^[a-z0-9]*$/" placeholder="<?php echo __("Inserici l'url del negozio"); ?>" type="text" class="form-control input-lg" />
                                            <span class="input-group-addon">.negozy.com</span>
                                        </div>
                                        <span><?php echo __("Il valore del campo deve contenere un minimo di 5 caratteri minuscoli."); ?></span>
                                        <span ng-if="!availableUrl" class="text-danger"><?php echo __("URL del negozio non disponibile"); ?></span>
                                    </div>
                                </form>
                            </div>
                            <hr />
                            <a href="javascript:void(0)" class="pull-left" wz-previous ><?php echo __("Indietro"); ?></a>
                            <input class="btn btn-success pull-right btn-lg" type="button" data-ng-click="checkShop();" value="<?php echo __("Avanti"); ?>" />
                        </div>
                    </wz-step>
                    <wz-step>
                        <div>
                            <div class="row">
                                <form ng-if="!sent && !verified" class="form-validation" >
                                    <h2 class="text-center text-thin"><?php echo __("Conferma il tuo numero di cellulare"); ?></h2>
                                    <div class="form-group">
                                        <label for=""> <?php echo __("Numero di cellulare"); ?> </label>
                                        <div class="input-group">
                                            <div class="input-group-addon">+39</div>
                                            <input data-ng-model="shop.phone_number" required  type="tel" class="form-control input-lg" placeholder="<?php echo __("Inserici il tuo numero di telefono"); ?>" />
                                        </div>
                                    </div>
                                    <button  ng-click="sendCode()" class="btn btn-primary btn-block pull-right btn-lg" type="button" ><?php echo __("Conferma numero"); ?></button>
                                </form>
                                <form ng-if="sent && !verified" class="form-validation" >
                                    <h2 class="text-center text-thin"><?php echo __("Ti è stato inviato un SMS al numero +39 {{shop.phone_number}} con il codice per confermare il tuo account."); ?></h2>
                                    <button data-ng-click="retry()" class="btn btn-primary btn-block pull-right btn-lg" type="button" ><?php echo __("Riprova"); ?></button>
                                    <br><br>
                                    <div ng-if="showError" class="alert alert-danger">
                                        <?php echo __("Il codice inserito non è valido!"); ?>
                                    </div>
                                    <div class="form-group">
                                        <label for=""> <?php echo __("Inserici il codice ricevuto via SMS"); ?> </label>
                                        <input data-ng-model="shop.code" required  type="tel" class="form-control input-lg" placeholder="<?php echo __("Inserici il codice ricevuto via SMS"); ?>" />
                                    </div>
                                    <button ng-click="verifyCode()" class="btn btn-primary btn-block pull-right btn-lg" type="button" ><?php echo __("Verifica codice"); ?></button>
                                </form>
                                <div ng-if="verified">
                                    <h1 class="text-center text-thin"><?php echo __("Congratulazioni!"); ?></h1>
                                    <h2 class="text-center text-thin"><?php echo __("Il tuo numero di cellulare è stato verificato con successo."); ?></h2>
                                    <input type="button" data-ng-click="saveData()" value="<?php echo __("Crea il negozio"); ?>" class="btn btn-block btn-lg btn-success" />
                                </div>
                            </div>
                            <hr />
                            <a href="javascript:void(0)" class="pull-left" wz-previous ><?php echo __("Indietro"); ?></a>
                        </div>
                    </wz-step>
                </wizard>
            </div>
        </section>
            </div>
        </div>
    </div>
</div>