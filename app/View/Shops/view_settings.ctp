<style type="text/css">
    html, body, #map_canvas {
        height: 250px;
        width: 100%;
        margin: 0px;
    }

    #map_canvas {
        position: relative;
    }

    .angular-google-map-container {
        position: absolute;
        top: 0;
        bottom: 0;
        right: 0;
        left: 0;
    }
</style>
<div class="lapagina">
    <nav class="navbar navbar-default navbar-static-top" role="navigation">
        <div class="navbar-header">
            <ol class="breadcrumb nav navbar-nav">
                <li class="active"><?php echo __("Impostazioni"); ?></li>
                <li class="active"><?php echo __("Dati Negozio"); ?></li>
            </ol>
        </div>
    </nav>
    <div class="page" data-ng-controller="SettingsCtrl">
        <div class="row">
          <div class="col-lg-10 col-lg-offset-1 col-md-12">
            <section class="panel panel-box copertina relative">
                <div class="panel-left panel-item bg-info">
                    <input type="file" accept="image" data-ng-model="logo" title="Cambia Copertina" data-ui-file-upload nv-file-select uploader="uploaderCover" class="btn btn-lg btn-danger segui_btn">
                    <span class="pull-right profile avatar_negozio visible-xs">
                        <img data-ng-if="logo" class="img110_110" ng-src="{{logo}}" alt="{{shop.name}}">
                        <img data-ng-if="!logo" class="img110_110" ng-src="/asset/site/img/no-img.jpg" alt="{{shop.name}}">
                        <input type="file" accept="image/*" data-ng-model="logo" title="Cambia Logo" data-ui-file-upload nv-file-select uploader="uploaderLogo" class="btn btn-danger">
                        <div class="content-loading" data-ng-if="loadingLogo" ng-activity-indicator="CircledDark" skip-ng-show="yes" style="top:36%; left:36%"></div>
                    </span>
                    <div class="titolo_shop">
                        <h3 class="ng-binding text-left text-thin">{{shop.name}}</h3>
                        <h4 class="text-left text-thin">{{shop.category}}</h4>
                    </div>
                </div>
                <div ng-style="{'background-image': 'url('+cover+')'}" class="panel-right panel-item bg-reverse">
                	<span class="pull-left profile avatar_negozio hidden-xs">
                        <img data-ng-if="logo" class="img110_110" ng-src="{{logo}}" alt="{{shop.name}}">
                        <img data-ng-if="!logo" class="img110_110" ng-src="/asset/site/img/no-img.jpg" alt="{{shop.name}}">
                        <input type="file" accept="image/*" data-ng-model="logo" title="Cambia Logo" data-ui-file-upload nv-file-select uploader="uploaderLogo" class="btn btn-danger">
                        <div class="content-loading" data-ng-if="loadingLogo" ng-activity-indicator="CircledDark" skip-ng-show="yes" style="top:36%; left:36%"></div>
                    </span>
                	<div class="content-loading" data-ng-if="loadingCover" ng-activity-indicator="CircledDark" skip-ng-show="yes" style="top:48%; left:65%"></div>
                </div>
            </section>
          
            <!-- Input -->
            <form name="product_data" class="form-validation">
                <section class="panel panel-default">
                    <div class="panel-heading"><strong><?php echo __("Negozio"); ?></strong></div>
                    <div class="panel-body">
                        <div class="form-horizontal">
                            <div class="form-group">
                                <label for="" class="col-sm-2"><?php echo __("Nome"); ?></label>
                                <div class="col-sm-10">
                                    <input required="required" type="text" data-ng-model="shop.name" class="form-control">
                                </div>
                            </div>
                            <div class="form-group">
                                <label for="" class="col-sm-2"><?php echo __("Categoria"); ?></label>
                                <div class="col-sm-10">
                                    <select data-ng-model="shop.macrocategory_id" class="form-control" data-ng-options="category.id as category.label for category in categories"></select>
                                </div>
                            </div>
                            <div class="form-group">
                                <label for="" class="col-sm-2"><?php echo __("URL negozio"); ?></label>
                                <div class="col-sm-10">
                                    <div class="input-group">
                                        <input required="required" ng-minlength="5" ng-pattern="/^[a-z0-9]*$/" type="text" data-ng-model="shop.url" class="form-control">
                                        <span class="input-group-addon">.negozy.com</span>
                                    </div>
                                </div>
                            </div>
                            <div class="form-group">
                                <label for="label-focus" class="col-sm-2"><?php echo __("Pubblicato"); ?></label>
                                <div class="col-sm-10">
                                    <label class="switch switch-warning" tooltip-placement="bottom" tooltip="<?php echo __('Una volta disabilitata la publicazione dello shop sarà possibile ripubblicare lo shop dalla dashboard'); ?>">
                                        <input type="checkbox" data-ng-disabled="!shop.is_public" data-ng-model="shop.is_public"><i></i></label>
                                </div>
                            </div>
                        </div>
                    </div>
                </section>
                <section class="panel panel-default">
                    <div class="panel-heading"><strong><?php echo __("Contatti"); ?></strong></div>
                    <div class="panel-body">
                        <div class="form-horizontal">
                        	<div class="form-group">
                                <label for="" class="col-sm-2"><?php echo __("Indirizzo"); ?></label>
                                <div class="col-sm-10">
                                    <input type="text" class="form-control" ng-autocomplete ng-model="shop.contact_address" details="details"/>
                                </div>
                            </div>
                            <div class="form-group">
                                <label for="" class="col-sm-2"><?php echo __("Coordinate mappa"); ?></label>
                                <div class="col-sm-10">
                                    <map data-ng-if="mapInit" zoom="11" draggable="true" center="coordinates">
                                        <marker on-dragend="updateMarker()" draggable="true" position="coordinates" />
                                        <control name="overviewMap" opened="true" />
                                    </map>
                                </div>
                            </div>
                            <div class="form-group">
                                <label for="" class="col-sm-2"><?php echo __("Numero di telefono"); ?></label>
                                <div class="col-sm-10">
                                    <input type="text" data-ng-model="shop.phone_number" class="form-control">
                                </div>
                            </div>
                            <div class="form-group">
                                <label for="" class="col-sm-2"><?php echo __("Email"); ?></label>
                                <div class="col-sm-10">
                                    <input type="email" data-ng-model="shop.email" class="form-control">
                                </div>
                            </div>
                        </div>
                    </div>
                </section>
            </form>
            <!-- end Input -->
        	<button data-ng-click="save()" ng-disabled="product_data.$invalid" class="btn btn-success btn-lg" type="button"><?php echo __("Salva"); ?></button>
          </div>
	    </div>
    </div>
</div>
