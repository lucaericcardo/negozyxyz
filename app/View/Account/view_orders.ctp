<div class="lapagina" data-ng-controller="OrdersCtrl">

    <div ng-show="!orders.length" class="page-err">
        <div class="err-container">
            <div class="text-center">
                <div class="err-status">
                    <h1><i class="fa fa-minus-square-o"></i></h1>
                </div>
                <div class="err-message">
                    <h2><?php echo __("Attualmente non sono presenti ordini."); ?></h2>
                </div>
            </div>
        </div>
    </div>

    <div ng-show="orders.length" class="page page-table">
        <div class="row">
            <div class="col-lg-10 col-lg-offset-1 col-md-12">
                <section class="panel panel-default table-dynamic">
                    <div class="panel-body">
                        <div class="table-responsive">
                            <table class="table table-bordered table-striped table-responsive">
                                <thead>
                                <tr>
                                    <th>
                                        <div class="th">
                                            <?php echo __("Codice"); ?>
                                        </div>
                                    </th>
                                    <th>
                                        <div class="th">
                                            <?php echo __("Negozio"); ?>
                                        </div>
                                    </th>
                                    <th>
                                        <div class="th">
                                            <?php echo __("Stato"); ?>
                                        </div>
                                    </th>
                                    <th>
                                        <div class="th">
                                            <?php echo __("Data"); ?>
                                        </div>
                                    </th>
                                    <th>
                                        <div class="th">
                                            <?php echo __("Totale"); ?>
                                        </div>
                                    </th>
                                    <th>
                                        <div class="th">
                                            <?php echo __("Azione"); ?>
                                        </div>
                                    </th>
                                </tr>
                                </thead>
                                <tbody>
                                <tr data-ng-repeat="order in orders">
                                    <td>{{order.uid}}</td>
                                    <td><a target="_blank" ng-href="{{order.link}}">{{order.shop}}</td>
                                    <td><span ng-class="order.label_class">{{order.status}}</span></td>
                                    <td>{{order.date | date:'dd/MM/yyyy  H:mm'}}</td>
                                    <td>{{order.amount}}</a></td>
                                    <td>
                                        <a data-ng-if="order.request && order.available" title="Acquista" class="btn btn-primary" data-ng-href="{{order.request_link}}" > <i class="fa fa-shopping-cart"></i></a>
                                        <a title="Visualizza" class="btn btn-info" data-ng-href="#/order/{{order.id}}" > <i class="fa fa-eye"></i></a>
                                    </td>
                                </tr>
                                </tbody>
                            </table>
                        </div>
                        <footer class="table-footer">
                            <div class="row">
                                <div class="col-md-6 page-num-info">
                                    <span>
                                        <?php echo __("Elementi per pagina"); ?>
                                        <select data-ng-model="numPerPage" data-ng-options="num for num in numPerPageOpt" data-ng-change="onNumPerPageChange()">
                                        </select>
                                    </span>
                                </div>
                                <div class="col-md-6 text-right pagination-container">
                                    <pagination class="pagination-sm" ng-model="currentPage" total-items="totalOrders" max-size="4" ng-change="select(currentPage)" items-per-page="numPerPage" rotate="false" boundary-links="true"></pagination>
                                </div>
                            </div>
                        </footer>
                    </div>
                </section>
            </div>
        </div>
    </div>
</div>