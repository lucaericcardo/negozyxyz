<div class="lapagina">
    <nav class="navbar navbar-default navbar-static-top" role="navigation">
        <div class="navbar-header">
            <ol class="breadcrumb nav navbar-nav">
                <li class="active"><?php echo __("Le tue notifiche"); ?></li>
            </ol>
        </div>
    </nav>
    <div class="page page-services" data-ng-controller="UserNotifiesCtrl">
        <div class="ui-timline-container">
            <section class="ui-timeline" data-ng-repeat="(key,notifies) in timeline">
                <article class="tl-item">
                    <div class="tl-body">
                        <div class="tl-entry">
                            <div class="tl-caption">
                                <a href="javascript:;" class="btn btn-primary btn-block">{{notifies.date}}</a>
                            </div>
                        </div>
                    </div>
                </article>
                <article data-ng-repeat="(k,v) in notifies.values" class="tl-item" data-ng-class-odd="'alt'">
                    <div class="tl-body">
                        <div class="tl-entry">
                            <div class="tl-time">{{v.time}}</div>
                            <div class="tl-icon square-icon sm bg-primary"><i class="fa fa-shopping-cart"></i></div>
                            <div class="tl-content">
                                <h4 class="tl-tile text-primary">{{v.from}}</h4>
                                <p>Ordine n. <a ng-href="{{v.link}}">{{v.order_uid}}</a></p>
                                <p>{{v.message}}</p>
                            </div>
                        </div>
                    </div>
                </article>
            </section>
        </div>
    </div>
</div>