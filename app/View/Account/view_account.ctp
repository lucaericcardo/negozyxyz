<div class="lapagina" data-ng-controller="AccountCtrl">
    <nav class="navbar navbar-default navbar-static-top" role="navigation">
        <div class="navbar-header">
            <ul class="nav navbar-nav">
                <li>
                    <ol class="breadcrumb">
                        <li class="active"><a><?php echo __("Modifica il tuo account"); ?></a></li>
                    </ol>
                </li>
            </ul>
        </div>
    </nav>

    <div class="page page-table">
        <div class="row">
            <div class="col-lg-10 col-lg-offset-1 col-md-12">
                <section class="panel panel-default">
                    <div class="panel-heading"><strong><?php echo __('Informazioni account'); ?></strong></div>
                    <div class="panel-body">
                        <form name="editAccount" class="form-validation">
                            <div class="form-horizontal">
                                <div class="form-group">
                                    <label for="label-focus" class="col-sm-2"><?php echo __('Nome'); ?></label>
                                    <div class="col-sm-10">
                                        <div class="divider"></div>
                                        <input autocomplete="off" data-ng-model="user.first_name" required="true" type="text" class="form-control">
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label for="label-focus" class="col-sm-2"><?php echo __('Cognome'); ?></label>
                                    <div class="col-sm-10">
                                        <div class="divider"></div>
                                        <input autocomplete="off" data-ng-model="user.last_name" required="true" type="text" class="form-control">
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label for="label-focus" class="col-sm-2"><?php echo __('Password'); ?></label>
                                    <div class="col-sm-10">
                                        <div class="well"> <?php echo __('Nuova password'); ?>
                                            <div class="divider"></div>
                                            <input autocomplete="off" name="new_password" data-ng-model="user.new_password" type="password" class="form-control" id="label-focus">
                                            <div class="divider"></div>
                                            <?php echo __('Conferma nuova password'); ?>
                                            <div class="divider"></div>
                                            <input autocomplete="off" name="confirm_password" data-validate-equals="user.new_password" data-ng-model="user.confirm_password" type="password" class="form-control">
                                        </div>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label for="label-focus" class="col-sm-2"><?php echo __('Immagine'); ?></label>
                                    <div class="col-sm-10">
                                        <div class="divider"></div>
                                        <div style="max-height:100px; margin:0 auto; background:#FFF">
                                            <img alt="" ng-src="{{user.image}}" class="img-circle img80_80">
                                            <input type="file" accept="image/*" data-ng-model="logo" title="Upload" data-ui-file-upload nv-file-select uploader="uploaderProfile">
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </form>
                    </div>
                </section>
                <button class="btn btn-success btn-lg pull-right" ng-disabled="editAccount.$invalid" data-ng-click="saveAccount()" type="button"><?php echo __('Salva'); ?></button>
            </div>
        </div>
</div>
</div>
