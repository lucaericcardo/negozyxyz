<table style="width:100%">
    <tr>
        <td style="background-color:#f5f5f5">
            <p style="font-family: 'Helvetica Neue', Helvetica, Arial, sans-serif;font-size:14px; margin:0px; padding:5px;">
                <b><?php echo __("Foto"); ?>:</b>
            </p>
        </td>
        <td style="background-color:#f5f5f5">
            <p style="font-family: 'Helvetica Neue', Helvetica, Arial, sans-serif;font-size:14px; margin:0px; padding:5px;">
                <b><?php echo __("Prodotto"); ?>:</b>
            </p>
        </td>
        <td style="background-color:#f5f5f5">
            <p style="font-family: 'Helvetica Neue', Helvetica, Arial, sans-serif;font-size:14px; margin:0px; padding:5px;">
                <b><?php echo __("Prezzo Unitario"); ?>:</b>
            </p>
        </td>
        <td style="background-color:#f5f5f5">
            <p style="font-family: 'Helvetica Neue', Helvetica, Arial, sans-serif;font-size:14px; margin:0px; padding:5px;">
                <b><?php echo __("Quantità"); ?>:</b>
            </p>
        </td>
        <td style="background-color:#f5f5f5">
            <p style="font-family: 'Helvetica Neue', Helvetica, Arial, sans-serif;font-size:14px; margin:0px; padding:5px;">
                <b><?php echo __("Totale"); ?>:</b>
            </p>
        </td>
    </tr>
    <?php
    foreach($order['products'] as $product) {

        ?>
        <tr>
            <td style="background-color:#f5f5f5">
                <p style="font-family: 'Helvetica Neue', Helvetica, Arial, sans-serif;font-size:12px; margin:0px; padding:5px;">
                    <img src="<?php echo $product['image']; ?>" height="40px"/>
                </p>
            </td>
            <td style="background-color:#f5f5f5">
                <p style="font-family: 'Helvetica Neue', Helvetica, Arial, sans-serif;font-size:12px; margin:0px; padding:5px;">
                    <?php echo $product['name']; ?>
                </p>
            </td>
            <td style="background-color:#f5f5f5">
                <p style="font-family: 'Helvetica Neue', Helvetica, Arial, sans-serif;font-size:12px; margin:0px; padding:5px;">
                    <?php echo $this->Number->currency($product['product_unit']['price_total'], $order['shop']['currency']); ?>
                </p>
            </td>
            <td style="background-color:#f5f5f5">
                <p style="font-family: 'Helvetica Neue', Helvetica, Arial, sans-serif;font-size:12px; margin:0px; padding:5px;">
                    <?php echo $product['quantity']; ?>
                </p>
            </td>
            <td style="background-color:#f5f5f5">
                <p style="font-family: 'Helvetica Neue', Helvetica, Arial, sans-serif;font-size:12px; margin:0px; padding:5px;">
                    <?php echo $this->Number->currency($product['product_total']['price_total'], $order['shop']['currency']); ?>
                </p>
            </td>
        </tr>
    <?php
    }
    ?>
    <tr>
        <td colspan="5">
            <table width="100%" cellpadding="0" cellspacing="0">
                <tr>
                    <td style="text-align:left">
                        <p style="font-family: 'Helvetica Neue', Helvetica, Arial, sans-serif;font-size:12px; margin:0px; padding:5px;">
                            <?php echo __("Imponibile prodotti"); ?>
                        </p>
                    </td>
                    <td style="text-align:right">
                        <p style="font-family: 'Helvetica Neue', Helvetica, Arial, sans-serif;font-size:12px; margin:0px; padding:5px;">
                            <?php echo $this->Number->currency($order['product_taxable'], $order['shop']['currency']); ?>
                        </p>
                    </td>
                </tr>
                <tr>
                    <td style="text-align:left; background-color:#f5f5f5">
                        <p style="font-family: 'Helvetica Neue', Helvetica, Arial, sans-serif;font-size:12px; margin:0px; padding:5px;">
                            <?php echo __("Imponibile spedizioni"); ?>
                        </p>
                    </td>
                    <td style="text-align:right; background-color:#f5f5f5">
                        <p style="font-family: 'Helvetica Neue', Helvetica, Arial, sans-serif;font-size:12px; margin:0px; padding:5px;">
                            <?php echo $this->Number->currency($order['shipping_taxable'], $order['shop']['currency']); ?>
                        </p>
                    </td>
                </tr>
                <tr>
                    <td style="text-align:left;">
                        <p style="font-family: 'Helvetica Neue', Helvetica, Arial, sans-serif;font-size:12px; margin:0px; padding:5px;">
                            <?php echo __("IVA prodotti"); ?>
                        </p>
                    </td>
                    <td style="text-align:right;">
                        <p style="font-family: 'Helvetica Neue', Helvetica, Arial, sans-serif;font-size:12px; margin:0px; padding:5px;">
                            <?php echo $this->Number->currency($order['product_tax'], $order['shop']['currency']); ?>
                        </p>
                    </td>
                </tr>
                <tr>
                    <td style="text-align:left; background-color:#f5f5f5">
                        <p style="font-family: 'Helvetica Neue', Helvetica, Arial, sans-serif;font-size:12px; margin:0px; padding:5px;">
                            <?php echo __("IVA spedizioni"); ?>
                        </p>
                    </td>
                    <td style="text-align:right; background-color:#f5f5f5">
                        <p style="font-family: 'Helvetica Neue', Helvetica, Arial, sans-serif;font-size:12px; margin:0px; padding:5px;">
                            <?php echo $this->Number->currency($order['shipping_tax'], $order['shop']['currency']); ?>
                        </p>
                    </td>
                </tr>
                <tr>
                    <td style="text-align:left;">
                        <p style="font-family: 'Helvetica Neue', Helvetica, Arial, sans-serif;font-size:12px; margin:0px; padding:5px;">
                            <?php echo __("Extra"); ?>
                        </p>
                    </td>
                    <td style="text-align:right;">
                        <p style="font-family: 'Helvetica Neue', Helvetica, Arial, sans-serif;font-size:12px; margin:0px; padding:5px;">
                            <?php echo $this->Number->currency($order['payment']['price'], $order['shop']['currency']); ?>
                        </p>
                    </td>
                </tr>
                <tr>
                    <td style="text-align:left;background-color:#f5f5f5;">
                        <p style="font-family: 'Helvetica Neue', Helvetica, Arial, sans-serif;font-size:16px; margin:0px; padding:5px;">
                            <?php echo __("Totale"); ?>
                        </p>
                    </td>
                    <td style="text-align:right;background-color:#f5f5f5;">
                        <p style="font-family: 'Helvetica Neue', Helvetica, Arial, sans-serif;font-size:16px; margin:0px; padding:5px;">
                            <b><?php echo $this->Number->currency($order['total'], $order['shop']['currency']); ?></b>
                        </p>
                    </td>
                </tr>
            </table>
        </td>
    </tr>
</table>
