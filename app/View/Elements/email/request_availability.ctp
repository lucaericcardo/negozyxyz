<table style="width:100%">
    <tr>
        <td style="background-color:#f5f5f5">
            <p style="font-family: 'Helvetica Neue', Helvetica, Arial, sans-serif;font-size:14px; margin:0px; padding:5px;">
                <b><?php echo __("Foto"); ?>:</b>
            </p>
        </td>
        <td style="background-color:#f5f5f5">
            <p style="font-family: 'Helvetica Neue', Helvetica, Arial, sans-serif;font-size:14px; margin:0px; padding:5px;">
                <b><?php echo __("Prodotto"); ?>:</b>
            </p>
        </td>
        <td style="background-color:#f5f5f5">
            <p style="font-family: 'Helvetica Neue', Helvetica, Arial, sans-serif;font-size:14px; margin:0px; padding:5px;">
                <b><?php echo __("Prezzo Unitario"); ?>:</b>
            </p>
        </td>
        <td style="background-color:#f5f5f5">
            <p style="font-family: 'Helvetica Neue', Helvetica, Arial, sans-serif;font-size:14px; margin:0px; padding:5px;">
                <b><?php echo __("Quantità"); ?>:</b>
            </p>
        </td>
        <td style="background-color:#f5f5f5">
            <p style="font-family: 'Helvetica Neue', Helvetica, Arial, sans-serif;font-size:14px; margin:0px; padding:5px;">
                <b><?php echo __("Totale"); ?>:</b>
            </p>
        </td>
    </tr>
    <tr>
        <td style="background-color:#f5f5f5">
            <p style="font-family: 'Helvetica Neue', Helvetica, Arial, sans-serif;font-size:12px; margin:0px; padding:5px;">
                <img src="<?php echo $order['product']['image']; ?>" height="40px" />
            </p>
        </td>
        <td style="background-color:#f5f5f5">
            <p style="font-family: 'Helvetica Neue', Helvetica, Arial, sans-serif;font-size:12px; margin:0px; padding:5px;">
                <?php echo $order['product']['name']; ?>
            </p>
        </td>
        <td style="background-color:#f5f5f5">
            <p style="font-family: 'Helvetica Neue', Helvetica, Arial, sans-serif;font-size:12px; margin:0px; padding:5px;">
                <?php echo $this->Number->currency($order['product']['unit']['price_total'], $order['shop']['currency']); ?>
            </p>
        </td>
        <td style="background-color:#f5f5f5">
            <p style="font-family: 'Helvetica Neue', Helvetica, Arial, sans-serif;font-size:12px; margin:0px; padding:5px;">
                <?php echo $order['product']['quantity']; ?>
            </p>
        </td>
        <td style="background-color:#f5f5f5">
            <p style="font-family: 'Helvetica Neue', Helvetica, Arial, sans-serif;font-size:12px; margin:0px; padding:5px;">
                <?php echo $this->Number->currency($order['product']['price_total'], $order['shop']['currency']); ?>
            </p>
        </td>
    </tr>
</table>
