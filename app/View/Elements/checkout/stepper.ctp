<ul class="orderStep orderStepLook2">
    <li <?php echo ($tabs['address']) ? 'class="active"' : ''; ?> ><a><i class="fa fa-map-marker"></i> <span> Indirizzo</span></a></li>
    <li <?php echo ($tabs['shipping']) ? 'class="active"' : ''; ?> ><a><i class="fa fa-truck"> </i><span>Spedizione</span></a></li>
    <li <?php echo ($tabs['payments']) ? 'class="active"' : ''; ?> ><a><i class="fa fa-money"> </i><span>Pagamento</span></a></li>
    <li <?php echo ($tabs['confirm']) ? 'class="active"' : ''; ?> ><a><i class="fa fa-check-square"> </i><span>Riepilogo Ordine</span></a></li>
</ul>