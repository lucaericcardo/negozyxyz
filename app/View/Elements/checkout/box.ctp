<div class="w100 cartMiniTable">
    <table id="cart-summary" class="std table">
    	<thead>
        	<tr>
            	<th colspan="2" class="price text-center">Dettaglio Acquisto</td>
            </tr>
        </thead>
        <tbody>
        <tr >
            <td><img src="<?php echo $order['product']['image']; ?>" /></td>

            <td><?php echo $order['product']['name'];?></td>
        </tr>
        <tr >
            <td><?php echo __("Quantit&agrave;"); ?></td>
            <td class="price" ><?php echo $order['product']['quantity']; ?></td>
        </tr>
        <tr >
            <td><?php echo __("Prodotto"); ?></td>
            <td class="price" ><?php echo $this->Number->currency($order['product']['unit']['price_total'], $order['shop']['currency']); ?></td>
        </tr>
        <tr  style="">
            <td><?php echo __("Spedizioni"); ?></td>
            <td class="price" ><span><?php echo $this->Number->currency($order['shipping']['unit']['price_total'], $order['shop']['currency']); ?></span></td>
        </tr>
        <tr >
            <td><?php echo __("Extra"); ?></td>
            <td class="price" id="total-tax"><?php echo $this->Number->currency($order['payment']['price_total'], $order['shop']['currency']); ?></td>
        </tr>
        <tr >
            <td > <?php echo __("Totale"); ?></td>
            <td class=" site-color" id="total-price"><?php echo $this->Number->currency($order['total'], $order['shop']['currency']); ?></td>
        </tr>
        </tbody>
        <tbody>
        </tbody>
    </table>
</div>