<div class="clearfix"></div>
<br>
<div class="alert alert-danger">
    <h4>Attenzione!</h4>
    <p>Non effettuiamo spedizioni verso il tuo paese!</p>
    <br />
    <a class="btn btn-default btn- pull-left" href="<?php echo "/checkoutV1/index/".$cart['shop']['url']."/".$cart['token']; ?>">Indietro</a>
    <div class="clearfix"></div>
</div>