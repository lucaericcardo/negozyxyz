<div class="row">
    <div class="col-xs-12">
        <div class="alert alert-warning">
            <b>Attenzione!</b>
            Uno o più prodotti presenti all'interno del carrello sono in stato di
            "Richiesta disponibilità".<br>
            Prima di poter effettuare l'ordine, è necessario verificare la
            disponibilità dei prodotti.
        </div>
    </div>
</div>