<header class="clearfix">
    <div class="logo hidden-xs">
        <a href="/">
            <span><img src="/asset/images/logoNegozy.png" /></span>
        </a>
    </div>
    <div class="top-nav">
        <ul class="nav-left list-unstyled" data-ng-controller="topBarDataController">
            <li>
                <a href="#/" data-toggle-min-nav class="toggle-min" ><i class="fa fa-bars"></i></a>
            </li>
            <li class="visible-xs nav-profile">
                <a href="/dashboard/#/">
                    <img src="/asset/images/logoNegozy.png" height="25" class="sm" />
                    <img src="/asset/images/only_shoppers.png" class="img30_30" />
                </a>
            </li>

            <li class="dropdown text-normal <?php echo ($image) ? 'nav-profile' : ''; ?> ">
                <a href="javascript:;" class="dropdown-toggle <?php echo ($image) ? '' : 'piuPadding'; ?>"
                   data-toggle="dropdown">
                    <?php
                    if ($image) {
                        echo '<img src="' . $image . '" alt="" class="img-circle img30_30">';
                    } else {
                        echo '<i class="fa fa-user"></i>';
                    }
                    ?>
                    <span class="hidden-xs">
                    <span><?php echo $user['first_name'] . " " . $user['last_name']; ?></span>
                </span>
                <i class="fa fa-caret-down"></i>
                </a>
                <ul class="dropdown-menu dropdown-dark with-arrow">
                    <li>
                        <a href="/account/#">
                            <span><?php echo __("Il tuo account"); ?></span>
                        </a>
                    </li>
                    <li>
                        <a href="/account/#/orders">
                            <span><?php echo __("I tuoi ordini"); ?></span>
                        </a>
                    </li>
                    <li>
                        <a href="/users/logout">
                            <span><?php echo __("Esci"); ?></span>
                        </a>
                    </li>
                </ul>
            </li>
            <?php
            if(isset($shop['Shop']) ) {
                if($shop['Shop']['is_public']) {
                    $titleIcon = __('Lo shop è online');
                    $classOnline = 'text-success';
                } else {
                    $titleIcon = __('Lo shop non è ancora stato pubblicato');
                    $classOnline = 'text-danger';
                }

				$logo= null;
				if(isset($shop['ShopLogo']) && count($shop['ShopLogo'])) {
					$logo = reset($shop['ShopLogo']);
				}
                ?>
                <li class="text-normal <?php echo ($logo) ? 'nav-profile' : ''; ?>">
                    <a href="/dashboard/shop/<?php echo $shop['Shop']['url'];?>/" class="<?php echo ($logo) ? '' : 'piuPadding'; ?>">
                        <?php
                        if(isset($logo)) {
                            echo '<img src="'.$logo['image'].'" alt="" class="img-circle img30_30">';
                        } else {
                            echo '<i class="fa fa-shopping-cart"></i>';
                        }
                        ?>
                        <span class="hidden-xs" style="padding-right: 2px">
                            <?php echo $shop['Shop']['name'];?>
                        </span>

                        <i style="width: inherit; margin-right: 0px" data-ng-show="isShopPublic" class="fa fa-circle text-success hide shopOnlineSymbol" tooltip-placement="bottom" tooltip="<?php echo __('Lo shop è online'); ?>"></i>
                        <i style="width: inherit; margin-right: 0px" data-ng-show="!isShopPublic" class="fa fa-circle text-danger hide shopOnlineSymbol" tooltip-placement="bottom" tooltip="<?php echo __('Lo shop non è ancora stato pubblicato'); ?>"></i>
                    </a>
                </li>
            <?php
            }
            ?>
        </ul>

            <ul class="nav-right pull-right list-unstyled" data-ng-controller="notifyController">
                <toaster-container></toaster-container>
                <?php
                if(isset($shop['Shop']) ) {
                ?>
                <li class="dropdown" on-toggle="loadShopNotifies(open)">
                    <a href="javascript:;" class="dropdown-toggle bg-orange" data-toggle="dropdown">
                        <img src="/asset/images/only_shoppers_white.png"/>
                        <span class="badge badge-info">{{shop_notifies.count}}</span>
                    </a>

                    <div class="dropdown-menu pull-right with-arrow panel panel-default" style="width:360px">
                        <div class="panel-heading">
                            <span><?php echo __('Notifiche dello shop'); ?></span> <!-- {{shop_notifies.notify.length}} -->
                        </div>
                        <div class="panel-body" ng-show="shop_notifies.loading">
                            <div ng-activity-indicator="DottedDark" skip-ng-show="yes" style="margin-left: 40%"></div>
                        </div>

                        <div class="panel-body" ng-hide="shop_notifies.notify.length || shop_notifies.loading">
                            <h5><?php echo __('Nessuna notifica presente per lo shop'); ?></h5>
                        </div>

                        <ul class="list-group" style="max-height:326px; overflow:hidden;">
                            <li class="list-group-item" data-ng-repeat="shop_notify in shop_notifies.notify" data-ng-class="{'active':!shop_notify.visualized}">
                                <div class="media">
                                    <span class="pull-left media-icon">
                                        <span class="square-icon sm bg-primary"><i class="fa fa-shopping-cart"></i></span>
                                    </span>
                                    <div class="media-body">
                                        <span class="block"><b>{{shop_notify.message}}</b></span>
                                        <span class="block">{{shop_notify.created.sec*1000 | date:'dd/MM/yyyy @ h:mma'}}</span>
                                    </div>
                                </div>
                            </li>
                        </ul>
                        <div class="panel-footer">
                            <a href="/dashboard/shop/<?php echo $shop['Shop']['url']; ?>/#/shop_notifies"><?php echo __('Vai alla lista delle notifiche.'); ?></a>
                        </div>
                    </div>
                </li>
                <?php
                }
                ?>

                <li class="dropdown" on-toggle="loadUserNotifies(open)">
                    <a href="javascript:;" class="dropdown-toggle bg-success" data-toggle="dropdown">
                        <i class="fa fa-bell-o nav-icon"></i>
                        <span class="badge badge-info">{{user_notifies.count}}</span>
                    </a>

                    <div class="dropdown-menu pull-right with-arrow panel panel-default" style="width:360px">
                        <div class="panel-heading">
                            <span><?php echo __('Notifiche dell\'utente'); ?></span> <!-- {{shop_notifies.notify.length}} -->
                        </div>
                        <div class="panel-body" ng-show="user_notifies.loading">
                            <div ng-activity-indicator="DottedDark" skip-ng-show="yes" style="margin-left: 40%"></div>
                        </div>

                        <div class="panel-body" ng-hide="user_notifies.notify.length || user_notifies.loading">
                            <h5><?php echo __('Nessuna notifica presente per l\'utente'); ?></h5>
                        </div>

                        <ul class="list-group" style="max-height:326px; overflow:hidden;">
                            <li class="list-group-item" data-ng-repeat="user_notify in user_notifies.notify" data-ng-class="{'active':!user_notify.visualized}">
                                <div class="media">
                                    <span class="pull-left media-icon">
                                        <span class="square-icon sm bg-primary"><i class="fa fa-shopping-cart"></i></span>
                                    </span>
                                    <div class="media-body">
                                        <span class="block"><b>{{user_notify.message}}</b></span>
                                        <span class="block">{{user_notify.created.sec*1000 | date:'dd/MM/yyyy @ h:mma'}}</span>
                                    </div>
                                </div>
                            </li>
                        </ul>
                        <div class="panel-footer">
                            <a href="/account/#/notifies"><?php echo __('Vai alla lista delle notifiche.'); ?></a>
                        </div>
                    </div>
                </li>
            </ul>

        <div class="clearfix"></div>
    </div>
    <button class="navbar-toggle" type="button" toggle-off-canvas>
        <span class="icon-bar"></span>
        <span class="icon-bar"></span>
        <span class="icon-bar"></span>
    </button>
</header>
