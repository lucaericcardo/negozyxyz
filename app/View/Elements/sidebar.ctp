<div id="nav-wrapper" data-ng-controller="SidebarCtrl">
    <ul id="nav" data-slim-scroll data-collapse-nav data-highlight-active >
        <li><a href="#/dashboard"> <i class="fa fa-dashboard"></i><span><?php echo __("Dashboard"); ?></span></a></li>
        <li><a href="#/products"> <i class="fa fa-archive"></i><span><?php echo __("Prodotti"); ?></span></a></li>
        <li><a href="#/collections"> <i class="fa fa-tags"></i><span><?php echo __("Collezioni"); ?></span></a></li>
        <li><a href="#/orders"> <i class="fa fa-reorder"></i><span><?php echo __("Ordini"); ?></span></a></li>
        <li><a href="#/categories"> <i class="fa fa-list"></i><span><?php echo __("Categorie"); ?></span></a></li>
        <li><a href="#/pages"> <i class="fa fa-file-text"></i><span><?php echo __("Pagine"); ?></span></a></li>
        <li><a href="#/languages"> <i class="fa fa-globe"></i><span><?php echo __("Lingue"); ?></span></a></li>
        <!--<li><a href="#/themes"> <i class="fa fa-magic"></i><span><?php echo __("Temi"); ?></span></a></li>-->
        <li>
        	<a href="#/settings">
                <i data-ng-if="!settingStatus.taxes || !settingStatus.shipping || !settingStatus.payments" class="fa fa-exclamation-triangle text-danger"></i>
                <i data-ng-if="settingStatus.taxes && settingStatus.shipping && settingStatus.payments" class="fa fa-cog"></i><span><?php echo __("Impostazioni"); ?></span></a>
            <ul>
                <li><a href="#/settings/shop"><?php echo __("Dati Negozio"); ?></a></li>
                <li><a href="#/settings/information"><?php echo __("Dati Fiscali"); ?></a></li>
                <li><a href="#/settings/tax"><i data-ng-if="!settingStatus.taxes" class="fa fa-exclamation-triangle text-danger"></i> <?php echo __("Imposte"); ?></a></li>
                <li><a href="#/settings/shipping"><i data-ng-if="!settingStatus.shipping" class="fa fa-exclamation-triangle text-danger"></i> <?php echo __("Spedizioni"); ?></a></li>
                <li><a href="#/settings/payments"><i data-ng-if="!settingStatus.payments" class="fa fa-exclamation-triangle text-danger"></i> <?php echo __("Metodi Pagamento"); ?></a></li>
            </ul>
        </li>
        <?php
        if(count($applications)) {
            ?>
            <li>
                <a href="#/app"><i class="fa fa-th"></i><span><?php echo __("Applicazioni"); ?></span></a>
                <ul>
                    <?php
                    foreach($applications as $application) {
                    ?>
                        <li><a href="#/app/<?php echo $application['request_uri']; ?>"><?php echo $application['name']; ?></a></li>
                    <?php
                    }
                    ?>
                </ul>
            </li>
        <?php
        }
        ?>
        <li><a href="<?php echo $shop['Shop']['site_url']; ?>" target="_blank"> <i class="fa fa-share"></i><span><?php echo __("Anteprima sito negozio"); ?></span></a></li>
        <?php
        //var_dump($applications);
        ?>
    </ul>
</div>