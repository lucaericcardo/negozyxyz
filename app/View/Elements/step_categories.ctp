<div data-ng-if="!obj.newTree && categoriesSelected.length" class="row altezzaSelect">
    <div class="col-md-6">
    <div class="divider"></div>
        <div>
          <div class="btn-group dropup btn-block" dropdown>
            <button type="button" class="btn btn-primary dropdown-toggle btn-block" ng-disabled="disabled">
                <span data-ng-if="obj.macrocategorySelected.label && obj.categorySelected.label && obj.subcategorySelected.label">{{obj.macrocategorySelected.label}} > {{obj.categorySelected.label}} > {{obj.subcategorySelected.label}}</span>
                <span data-ng-if="!obj.macrocategorySelected.label || !obj.categorySelected.label || !obj.subcategorySelected.label"><?php echo __('Scegli categoria usata di recente'); ?></span>
                <span class="caret"></span> </button>
            <div class="dropdown-menu btn-block" role="menu" style="padding-top:0">
                <div class="panel panel-default">
                    <div class="panel-body">
                        <ul class="list-unstyled">
                          <li data-ng-repeat="item in categoriesSelected"><a data-ng-click="selectSelectedItem(item)" href="javascript:void(0)" >{{item.Subcategory.Category.Macrocategory.label}} > {{item.Subcategory.Category.label}} > {{item.Subcategory.label}}</a> </li>
                        </ul>
                    </div>
                    </div>
                </div>
            </div>
            
          </div>
      </div>
     
    <div class="col-md-6">
    	 <div class="divider"></div>
        <button type="button" class="btn btn-primary" ng-click="addNewTree()"><i class="fa fa-plus"></i> Aggiungi Nuova Categoria</button>
    </div>
</div>
<div data-ng-if="obj.newTree || !categoriesSelected.length" class="row altezzaSelect">
  <div class="col-md-4">
    <label><?php echo __('Macrocategoria'); ?> *</label>
    <div data-ng-show="obj.newMacro">
      <div class="input-group"> <span class="input-group-btn">
        <button class="btn btn-default" type="button" data-ng-click="backMacroStep(false)"><i class="fa fa-times"></i> </button>
        </span>
        <input type="text" class="form-control" data-ng-model="obj.newMacrocategory">
        <span class="input-group-btn" data-ng-show="obj.newMacrocategory">
        <button class="btn btn-success" type="button" data-ng-click="goToCateStep()"><i class="fa fa-plus-circle"></i> </button>
        </span> </div>
      <p class="text-danger" data-ng-show="obj.macroAlredyExist"><?php echo __('Macrocategoria già esistente'); ?></p>
      <p class="text-success" data-ng-show="obj.newMacroOk"><?php echo __('Macrocategoria valida'); ?></p>
    </div>
    <div data-ng-hide="obj.newMacro">
      <div class="btn-group dropup" dropdown>
        <button type="button" class="btn btn-primary dropdown-toggle" ng-disabled="disabled"> {{obj.macrocategorySelected.label || "<?php echo __('Seleziona'); ?>"}} <span class="caret"></span> </button>
        <div class="dropdown-menu" role="menu" style="padding-top:0">
        	<div class="panel panel-default">
                <div class="panel-heading">
                	<div class="input-group">
                      <input type="text" class="form-control" data-ng-model="macrocategorySearch" stop-event>
                      <span class="input-group-addon"><i class="fa fa-search"></i></span>
                    </div>
                </div>
                <div class="panel-body">
                    <ul class="list-unstyled">
                      <li data-ng-show="(categoriesList | filter:macrocategorySearch).length == 0"><a href="javascript:void(0)"><?php echo __('Nessuna macrocategoria'); ?></a></li>
                      <li data-ng-repeat="item in categoriesList | filter:macrocategorySearch"><a href="javascript:void(0)" data-ng-click="selectMacrocategory(item);">{{item.Macrocategory.label}}</a> </li>
                    </ul>
                </div>
                <div class="panel-footer">
                	<a href="javascript:void(0)" data-ng-click="backMacroStep(true)" class="text-primary"><i class="fa fa-plus"></i> <?php echo __('crea nuova'); ?></a> </div>
                </div>
            </div>
        </div>
        
      </div>
  </div>
  <div class="col-md-4">
    <div <?php /*?>data-ng-show="obj.macrocategoryCompleted"<?php */?>>
      <label><?php echo __('Categoria'); ?> *</label>
      <div data-ng-show="obj.newCate">
        <div class="input-group"> <span class="input-group-btn">
          <button class="btn btn-default" type="button" data-ng-click="backCateStep(false)"><i class="fa fa-times"></i> </button>
          </span>
          <input type="text" class="form-control" data-ng-model="obj.newCategory">
          <span class="input-group-btn" data-ng-show="obj.newCategory">
          <button class="btn btn-success" type="button" data-ng-click="goToSubStep()"><i class="fa fa-plus-circle"></i> </button>
          </span> </div>
        <p class="text-danger" data-ng-show="obj.cateAlredyExist"><?php echo __('Categoria già esistente'); ?></p>
        <p class="text-success" data-ng-show="obj.newCateOk"><?php echo __('Categoria valida'); ?></p>
      </div>
      <div data-ng-hide="obj.newCate">
        <div class="btn-group dropup" dropdown>
          <button type="button" ng-class="{'btn-primary':obj.macrocategoryCompleted}" class="btn btn-default btn-block dropdown-toggle" ng-disabled="!obj.macrocategoryCompleted"> {{obj.categorySelected.label || "<?php echo __('Seleziona'); ?>"}} <span class="caret"></span> </button>
          <div class="dropdown-menu" role="menu" style="padding-top:0">
        	<div class="panel panel-default">
                <div class="panel-heading">
                	<div class="input-group">
                      <input type="text" class="form-control" data-ng-model="categorySearch" stop-event>
                      <span class="input-group-addon"><i class="fa fa-search"></i></span>
                    </div>
                </div>
                <div class="panel-body">
                    <ul class="list-unstyled">
                      <li data-ng-show="(obj.categories | filter:categorySearch).length == 0"><a href="javascript:void(0)"><?php echo __('Nessuna categoria'); ?></a></li>
            <li data-ng-repeat="item in obj.categories | filter:categorySearch"><a href="javascript:void(0)" data-ng-click="selectCategory(item);">{{item.label}}</a> </li>
                    </ul>
                </div>
                <div class="panel-footer">
                	<a href="javascript:void(0)" data-ng-click="backCateStep(true)" class="text-primary"><i class="fa fa-plus"></i> <?php echo __('crea nuova'); ?></a> </div>
                </div>
            </div>
        </div>
         </div>
    </div>
  </div>
  <div class="col-md-4">
    <div <?php /*?>data-ng-show="obj.macrocategoryCompleted && obj.categoryCompleted"<?php */?>>
      <label><?php echo __('Sottocategoria'); ?> *</label>
      <div data-ng-show="obj.newSub">
        <div class="input-group"> <span class="input-group-btn">
          <button class="btn btn-default" type="button" data-ng-click="backSubStep(false)"><i class="fa fa-times"></i> </button>
          </span>
          <input type="text" class="form-control" data-ng-model="obj.newSubcategory">
          <span class="input-group-btn" data-ng-show="obj.newSubcategory">
          <button class="btn btn-success" type="button" data-ng-click="validateSubcategory()"><i class="fa fa-plus-circle"></i></button>
          </span> </div>
        <p class="text-danger" data-ng-show="obj.subAlredyExist"><?php echo __('Sottocategoria già esistente'); ?></p>
        <p class="text-success" data-ng-show="obj.newSubOk"><?php echo __('Sottocategoria valida'); ?></p>
      </div>
      <div data-ng-hide="obj.newSub">
        <div class="btn-group dropup" dropdown>
          <button type="button" ng-class="{'btn-primary':obj.macrocategoryCompleted && obj.categoryCompleted}" class="btn btn-default btn-block dropdown-toggle" ng-disabled="!(obj.macrocategoryCompleted && obj.categoryCompleted)"> {{obj.subcategorySelected.label || "<?php echo __('Seleziona'); ?>"}} <span class="caret"></span> </button>
          <div class="dropdown-menu" role="menu" style="padding-top:0">
        	<div class="panel panel-default">
                <div class="panel-heading">
                	<div class="input-group">
                      <input type="text" class="form-control" data-ng-model="subcategorySearch" stop-event>
                      <span class="input-group-addon"><i class="fa fa-search"></i></span>
                    </div>
                </div>
                <div class="panel-body">
                    <ul class="list-unstyled">
                      <li data-ng-show="(obj.subcategories | filter:subcategorySearch).length == 0"><a href="javascript:void(0)"><?php echo __('Nessuna subcategoria'); ?></a></li>
            		  <li data-ng-repeat="item in obj.subcategories | filter:subcategorySearch"><a href="javascript:void(0)" data-ng-click="selectSubcategory(item);">{{item.label}}</a></li>
                    </ul>
                </div>
                <div class="panel-footer">
                	<a href="javascript:void(0)" data-ng-click="backSubStep(true)" class="text-primary"><i class="fa fa-plus"></i> <?php echo __('crea nuova'); ?></a> </div>
                </div>
            </div>
            
          
        </div>
         </div>
    </div>
  </div>
</div>
