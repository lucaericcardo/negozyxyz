<div class='media'>
    <a class='pull-left'><img class='img64_64' src='<?php echo $product_thumb; ?>' /></a>
    <div class='media-body'>
        <p><small><?php echo $message; ?></small></p>
    </div>
</div>
<?php if($available) { ?>
<div class='media'>
    <button class='btn btn-sm btn-success pull-right' data-ng-click="buyNow('<?php echo $product_id; ?>', '<?php echo $negozyation_id; ?>')"><?php echo __('Compra'); ?></button>
</div>
<?php } ?>
<div class='clearfix'></div>
