<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <title>Dropdown Menu UI - Bootsnipp.com</title>
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link href="//netdna.bootstrapcdn.com/bootstrap/3.2.0/css/bootstrap.min.css" rel="stylesheet" id="bootstrap-css">
    <style type="text/css">
        body {
            background-color: transparent;
        }
        #widget-container {
            position: absolute;
            top: 5px;
            right: 5px;
        }
        #myDropdown img{
            height: 30px;
            max-width: 30px;
        }
        #cart {
            margin: 7px;
        }
    </style>
    <!--
    <script src="//code.jquery.com/jquery-1.10.2.min.js"></script>
    <script src="//netdna.bootstrapcdn.com/bootstrap/3.2.0/js/bootstrap.min.js"></script>

    <script type="text/javascript">
        var qt = 0;
        var eventMethod = window.addEventListener ? "addEventListener" : "attachEvent";
        var eventer = window[eventMethod];
        var messageEvent = eventMethod == "attachEvent" ? "onmessage" : "message";
        eventer(messageEvent,function(e) {
            if(e.data.name == "addToCart") {
                $.ajax({
                    url: '/carts/addProduct.json',
                    data: e.data,
                    type: 'POST',
                    dataType: 'json',
                    success: function(r){
                        $('#qt').text(r.cart.total_quantity);
                    }
                });
            }
        },false);

        function updateProduct(product_id = null,product_quantity=null,shop_id=55){
            $.ajax({
                url: '/carts/updateProduct.json',
                data: {product_id:product_id,shop_id:shop_id,quantity:product_quantity},
                type: 'POST',
                dataType: 'json',
                success: function(r){
                    $('#qt').text(r.cart.total_quantity);
                }
            })
        }

        function removeProduct(product_id = null,shop_id=55){
            $.ajax({
                url: '/carts/removeProduct.json',
                data: {product_id:product_id,shop_id:shop_id},
                type: 'POST',
                dataType: 'json',
                success: function(r){
                    $('#qt').text(r.cart.total_quantity);
                }
            })
        }


        $( document ).ready(function() {
            $('.dropdown-menu').on("click",function(e){
                e.stopPropagation();
            })
            $('#myDropdown').on('show.bs.dropdown', function (e) {
                setTimeout(function(){
                    $('#myDropdown').addClass('open');
                },50)
                var iframe_height = parseInt( 350)
                var obj ={
                    size: iframe_height,
                    name: "resize"
                }
                window.parent.postMessage( obj, '*');
                e.stopPropagation();
            });
            $('#myDropdown').on('hide.bs.dropdown', function (e) {
                var iframe_height = parseInt($('html').height())
                var obj ={
                    size: 80,
                    name: "resize"
                }
                window.parent.postMessage( obj, '*');
            });

            $('#cart').on('show.bs.dropdown', function (e) {
                setTimeout(function(){
                    $('#cart').addClass('open');
                },50)
                var iframe_height = parseInt( 450)
                var obj ={
                    size: iframe_height,
                    name: "resize"
                }
                window.parent.postMessage( obj, '*');
                e.stopPropagation();
            });
            $('#cart').on('hide.bs.dropdown', function (e) {
                var obj ={
                    size: 80,
                    name: "resize"
                }
                window.parent.postMessage( obj, '*');
            });

        });
    </script>-->
</head>
<body data-ng-app="cart">
<?php echo $this->fetch('content'); ?>
<script type="text/javascript" src="/asset/js/jquery.js"></script>
<script type="text/javascript" src="/asset/js/angular.min.js"></script>
<script type="text/javascript" src="/asset/js/angular-route.min.js"></script>
<script type="text/javascript" src="/asset/js/angular-animate.min.js"></script>
<script type="text/javascript" src="/asset/js/underscore-min.js"></script>
<script type="text/javascript" src="/asset/js/ui-bootstrap.min.js"></script>
<script type="text/javascript" src="/asset/js/ui-bootstrap-tpls.min.js"></script>
<script type="text/javascript" src="/asset/scripts/cart/cart.js"></script>
<script type="text/javascript" src="/asset/scripts/cart/cart_ctrls.js"></script>
<script type="text/javascript" src="/asset/scripts/cart/cart_services.js"></script>
</body>
</html>
