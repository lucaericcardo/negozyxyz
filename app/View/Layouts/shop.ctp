<!doctype html>
<!--[if gt IE 8]><!--> <html class="no-js"> <!--<![endif]-->
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
    <title>Negozy</title>
    <meta name="description" content="">
    <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0">
    <link rel="stylesheet" href="/asset/css/style.min.css">
    <link rel="stylesheet" href="/asset/css/toastr.min.css">
    <link rel="stylesheet" href="/asset/css/select.css">
    <link rel="stylesheet" href="/asset/css/newsTicker.css">
    <link rel="stylesheet" href="/asset/css/multiselect.min.css">
    <script type="text/javascript" src="//notify.services.negozy.com:3002/socket.io/socket.io.js"></script>
    <script src="https://js.braintreegateway.com/v2/braintree.js"></script>
    <?php
    $file_loaded = array();
    foreach($applications_config as $config) {
        if (count($config['css'])) {
            foreach ($config['css'] as $css_file) {
                if(!in_array($css_file,$file_loaded)) {
                    $file_loaded[] = $css_file;
                    echo '<link rel="stylesheet" href="'.$css_file.'">' . "\n";
                }
            }
        }
    }

    ?>

    <!--
    <link rel="stylesheet/less" type="text/css" href="/asset/admin/less/style.less" />
	<script src="/asset/admin/js/less.2.0.0-b1.min.js"></script>
    -->
</head>
<body data-ng-app="shop" id="app" data-custom-background="" data-off-canvas-nav="">
<!--[if lt IE 9]>
<p class="browsehappy">You are using an <strong>outdated</strong> browser. Please <a href="http://browsehappy.com/">upgrade your browser</a> to improve your experience.</p>
<![endif]-->
<div>
    <div data-ng-cloak="" class="no-print">
        <section id="header" class="top-header">
            <?php echo $this->element('topbar'); ?>
        </section>
        <aside id="nav-container">
            <?php echo $this->element('sidebar'); ?>
        </aside>
    </div>
    <div>
        <div class="content-loading" ng-activity-indicator="DottedDark"></div>
        <section id="content" ng-hide="AILoading" data-ng-view=""></section><!-- class="animate-fade-up" -->
    </div>
</div>
<script type="text/javascript" src="//notify.services.negozy.com:3002/socket.io/socket.io.js"></script>
<script type="text/javascript" src="/asset/js/jquery.js"></script>
<script type="text/javascript" src="/asset/js/angular.min.js"></script>
<script type="text/javascript" src="/asset/js/angular-route.min.js"></script>
<script type="text/javascript" src="/asset/js/angular-animate.min.js"></script>
<script type="text/javascript" src="/asset/js/angular-sanitize.js"></script>
<script type="text/javascript" src="/asset/js/select.js"></script>
<script type="text/javascript" src="/asset/js/multiselect-tpls.min.js"></script>
<script type="text/javascript" src="/asset/js/toastr_jirikavi.js"></script>
<script type="text/javascript" src="/asset/js/jquery.slimscroll.min.js"></script>
<script type="text/javascript" src="//ajax.googleapis.com/ajax/libs/jqueryui/1.11.2/jquery-ui.min.js"></script>
<script type="text/javascript" src="/asset/js/underscore-min.js"></script>
<script type="text/javascript" src="/asset/js/angular-file-upload.min.js"></script>
<script type="text/javascript" src="/asset/js/ngActivityIndicator.min.js"></script>
<script type="text/javascript" src="/asset/js/sortable.js"></script>
<script type="text/javascript" src="/asset/js/textAngular.min.js"></script>
<script type="text/javascript" src="/asset/js/textAngular-sanitize.js"></script>
<?php
$angular_plugins = array();
$angular_views = array();
foreach($applications_config as $config) {
    if(count($config['js'])) {
        foreach($config['js'] as $js_file){
            if(!in_array($js_file,$file_loaded)) {
                $file_loaded[] = $js_file;
                echo '<script type="text/javascript" src="' . $js_file . '"></script>' . "\n";
            }
        }
    }
    if(count($config['scripts'])) {
        foreach($config['scripts'] as $name => $scripts_file){
            $angular_plugins[] = '"'.$name.'"';
            if(!in_array($scripts_file,$file_loaded)) {
                $file_loaded[] = $scripts_file;
                echo '<script type="text/javascript" src="' . $scripts_file . '"></script>' . "\n";
            }
        }
    }
    if(count($config['views'])) {
        foreach ($config['views'] as $k => $views) {
            $angular_views[$config['name']."_".$k] = $views;
        }
    }
}

echo '<script type="text/javascript">' . "\n";
echo 'angular.module("plugins", ['.implode(",",$angular_plugins).'])' . "\n";
echo 'var pluginRoute = {views:'.json_encode($angular_views,TRUE).'}' . "\n";
echo '</script>' . "\n";
?>
<script type="text/javascript" src="/asset/scripts/shop/shop.js"></script>
<script type="text/javascript" src="/asset/scripts/shop/shop_ctrls.js"></script>
<script type="text/javascript" src="/asset/scripts/shop/shop_services.js"></script>
<script type="text/javascript" src="/asset/scripts/shared/top_bar_ctrls.js"></script>
<script type="text/javascript" src="/asset/scripts/shared/top_bar_services.js"></script>
<script type="text/javascript" src="/asset/scripts/shared/shared_directives.js"></script>
<script type="text/javascript" src="/asset/scripts/shared/shared_services.js"></script>

<script type="text/javascript" src="/asset/js/bootstrap.file-input.js"></script>
<script type="text/javascript" src="/asset/js/ng-tags-input.min.js"></script>
<script type="text/javascript" src="/asset/js/ui-bootstrap-tpls.min.js"></script>
<script type="text/javascript" src="http://maps.googleapis.com/maps/api/js?libraries=places&sensor=false"></script>
<script type="text/javascript" src="/asset/js/ng-map.min.js"></script>
<script type="text/javascript" src="/asset/js/ngAutocomplete.js"></script>
<script type="text/javascript" src="/asset/js/jquery.newsTicker.min.js"></script>


<script id="_webengage_script_tag" type="text/javascript">
    var webengage; !function(e,t,n){function o(e,t){e[t[t.length-1]]=function(){r.__queue.push([t.join("."),arguments])}}var i,s,r=e[n],g=" ",l="init options track screen onReady".split(g),a="feedback survey notification".split(g),c="options render clear abort".split(g),p="Open Close Submit Complete View Click".split(g),u="identify login logout setAttribute".split(g);if(!r||!r.__v){for(e[n]=r={__queue:[],__v:"5.0",user:{}},i=0;i<l.length;i++)o(r,[l[i]]);for(i=0;i<a.length;i++){for(r[a[i]]={},s=0;s<c.length;s++)o(r[a[i]],[a[i],c[s]]);for(s=0;s<p.length;s++)o(r[a[i]],[a[i],"on"+p[s]])}for(i=0;i<u.length;i++)o(r.user,["user",u[i]]);setTimeout(function(){var f=t.createElement("script"),d=t.getElementById("_webengage_script_tag");f.type="text/javascript",f.async=!0,f.src=("https:"==t.location.protocol?"https://ssl.widgets.webengage.com":"http://cdn.widgets.webengage.com")+"/js/widget/webengage-min-v-5.0.js",d.parentNode.insertBefore(f,d)})}}(window,document,"webengage");
    webengage.init("~c2ab3a22");
</script>

</body>
</html>