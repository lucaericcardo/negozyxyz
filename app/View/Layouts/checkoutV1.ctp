<!doctype html>
<!--[if gt IE 8]><!--> <html class="no-js"> <!--<![endif]-->
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
    <title>Negozy</title>
    <meta name="description" content="">
    <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0">
    <link href="//netdna.bootstrapcdn.com/bootstrap/3.3.4/css/bootstrap.min.css" rel="stylesheet" id="bootstrap-css">
    <style>
        body {
            padding-top: 70px;
        }
        .navbar-brand {
            padding:5px 15px !important;
        }
        .navbar-default {
            background-color: #FFF;
        }
        .navbar-header {
            float: left;
            text-align: center;
            width: 100%;
        }
        .navbar-brand {
            float:none;
        }
        /* Sticky footer styles
-------------------------------------------------- */
        html {
            position: relative;
            min-height: 100%;
        }
        body {
            /* Margin bottom by footer height */
            margin-bottom: 90px;
        }
        .footer {
            position: absolute;
            bottom: 0;
            width: 100%;
            /* Set the fixed height of the footer here */
            height: 60px;
            background-color: #f5f5f5;
        }
        .container .text-muted {
            margin: 20px 0;
        }

    </style>
</head>
<body>
<nav class="navbar navbar-default navbar-fixed-top">
    <div class="container">
        <div class="navbar-header">
            <img class="navbar-brand" src="<?php echo ($cart['shop']['image']) ? $cart['shop']['image'] : '/asset/images/logoNegozy.png' ;?>" />
        </div>

    </div>
</nav>
<div class="container">
    <?php echo $this->fetch('content'); ?>
</div>
<!--/container-->

<footer class="footer">
    <div class="container">
        <p class="text-muted">@ Negozy</p>
    </div>
</footer>

<script type="text/javascript" src="/asset/js/jquery.js"></script>
<script type="text/javascript">
    $( "#checkboxBilling" ).click(function() {
        $( '#FormBilling' ).toggleClass( "hide" );
    });
</script>
</body>
</html>