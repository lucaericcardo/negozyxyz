<!doctype html>
<!--[if gt IE 8]><!--> <html class="no-js"> <!--<![endif]-->
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
    <title>Negozy</title>
    <meta name="description" content="">
    <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0">
    <!--
    <link rel="stylesheet/less" type="text/css" href="/asset/admin/less/style.less" />
    <script src="/asset/admin/js/less.2.0.0-b1.min.js"></script>
    -->
    <link rel="stylesheet" href="/asset/css/checkout.css">
</head>
<body id="app" data-ng-app="main" data-custom-background="" data-off-canvas-nav="">
<!--[if lt IE 9]>
<p class="browsehappy">You are using an <strong>outdated</strong> browser. Please <a href="http://browsehappy.com/">upgrade your browser</a> to improve your experience.</p>
<![endif]-->
<div id="mainTmp">
    <div data-ng-cloak="" class="no-print">
        <section id="header" class="top-header">
            <?php echo $this->element('topbar'); ?>
        </section>
    </div>
    <div class="view-container">
        <section id="content">
            <?php echo $this->fetch('content'); ?>
        </section>
        <div class="footer">  
            <div class="row">
              <div class="col-lg-5 col-lg-offset-1 col-md-6 col-sm-6 col-xs-6 hidden-xs">
                <p>&copy; 2014 - <img src="//cdn.negozy.com/negozy/Logos/logoNegozy.png" height="15"> - Realizzato e sviluppato da <a href="http://www.lhsgroup.it" target="_blank">LHSGroup</a></p>
              </div>
              <div class="col-lg-5 col-md-6 text-right col-sm-6 col-xs-12">
                  <ul class="list-inline">
                  	<li><?php echo "<span class='hidden-xs'>". $shop_name." </span> - ".(is_numeric($shop_piva{0}) ? 'P.Iva' : 'CF')." : <span class=\"text-uppercase\">".$shop_piva."</span>"; ?></li>
                    <br>
                    <?php
                      foreach($terms as $term) {
                          echo '<li><a target="_blank" href="'.$site_url.$term['url'].'">'.$term['name'].'</a></li>';
                      }
                      ?>
                  </ul>
              </div>
            </div>
        </div>
    </div>
</div>
<script type="text/javascript" src="//notify.services.negozy.com:3002/socket.io/socket.io.js"></script>
<script type="text/javascript" src="/asset/js/jquery.js"></script>
<script type="text/javascript" src="/asset/js/angular.min.js"></script>
<script type="text/javascript" src="/asset/js/angular-route.min.js"></script>
<script type="text/javascript" src="/asset/js/angular-animate.min.js"></script>
<script type="text/javascript" src="/asset/js/toastr_jirikavi.js"></script>
<script type="text/javascript" src="//ajax.googleapis.com/ajax/libs/jqueryui/1.11.2/jquery-ui.min.js"></script>
<script type="text/javascript" src="/asset/js/underscore-min.js"></script>
<script type="text/javascript" src="/asset/js/ui-bootstrap.min.js"></script>
<script type="text/javascript" src="/asset/js/angular-wizard.min.js"></script>
<script type="text/javascript" src="/asset/js/ngActivityIndicator.min.js"></script>
<script type="text/javascript" src="/asset/js/bootstrap.file-input.js"></script>
<script type="text/javascript" src="/asset/js/angular-file-upload.min.js"></script>
<script type="text/javascript" src="/asset/scripts/main/main.js"></script>
<script type="text/javascript" src="/asset/scripts/main/main_ctrls.js"></script>
<script type="text/javascript" src="/asset/scripts/main/main_services.js"></script>
<script type="text/javascript" src="/asset/scripts/shared/top_bar_ctrls.js"></script>
<script type="text/javascript" src="/asset/scripts/shared/top_bar_services.js"></script>
<script type="text/javascript" src="/asset/scripts/shared/shared_directives.js"></script>
<script type="text/javascript" src="/asset/scripts/shared/shared_services.js"></script>
<script type="text/javascript" src="/asset/js/jquery.mousewheel-3.0.6.pack.js"></script>
<script type="text/javascript" src="/asset/js/pinterest_grid.js"></script>
<script type="text/javascript" src="/asset/js/jquery.newsTicker.min.js"></script>
<script type="text/javascript" src="/asset/js/ui-bootstrap-tpls.min.js"></script>
<script type="text/javascript" src="/asset/scripts/cart/cart.js"></script>
<script type="text/javascript" src="/asset/scripts/cart/cart_ctrls.js"></script>
<script type="text/javascript" src="/asset/scripts/cart/cart_services.js"></script>
<?php /*?>
<script type="text/javascript" src="/asset/js/ion.checkRadio.min.js"></script>
<script type="text/javascript">
$(function() {
	//$("input[type='radio'], input[type='checkbox']").ionCheckRadio();
});
</script><?php */?>
</body>
</html>