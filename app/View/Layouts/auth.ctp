<!doctype html>
<!--[if gt IE 8]><!--> <html class="no-js"> <!--<![endif]-->
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
    <title>Negozy</title>
    <meta name="description" content="">
    <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0">
    <!--
    <link rel="stylesheet/less" type="text/css" href="/assets/less/login.less" />
    <script src="/assets/js/less.2.0.0-b1.min.js"></script>-->
    <link href="/asset/signin/css/login.min.css" rel="stylesheet">
    <!-- HTML5 shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!--[if lt IE 9]>
      <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
      <script src="https://oss.maxcdn.com/libs/respond.js/1.3.0/respond.min.js"></script>
    <![endif]-->
</head>
<body>
	<?php echo $this->fetch('content'); ?>
    <script type="text/javascript" src="http://code.jquery.com/jquery-1.9.0.min.js"></script>
    <script type="text/javascript" src="http://ajax.googleapis.com/ajax/libs/jqueryui/1.10.0/jquery-ui.min.js"></script>
	<script type="text/javascript" src="https://maxcdn.bootstrapcdn.com/bootstrap/3.2.0/js/bootstrap.min.js"></script>
    <script type="text/javascript" src="/asset/signin/js/jquery.icheck.min.js"></script>
    <script type="text/javascript" src="/asset/signin/js/waypoints.min.js"></script>
    <script type="text/javascript" src="/asset/signin/js/authenty.js"></script>
    <script type="text/javascript" src="/asset/signin/js/app.js"></script>

</body>
</html>