<div class="lapagina" data-ng-controller="LanguagesCtrl">
    <nav class="navbar navbar-default navbar-static-top" role="navigation">
        <div class="navbar-header">
            <ul class="nav navbar-nav">
                <li>
                    <ol class="breadcrumb">
                        <li class="active"><?php echo __("Lingue"); ?></li>
                    </ol>
                </li>
            </ul>
            <ul class="nav navbar-nav navbar-right">
                <div class="navbar-form">
                    <a href="javascript:void(0)" ng-click="addLanguage()" class="btn btn-success" type="submit"><i class="fa fa-plus"></i> <?php echo __("Aggiungi lingua"); ?></a>
                </div>
            </ul>
        </div>
    </nav>

    <div ng-if="!languages.length" class="page-err">
        <div class="err-container">
            <div class="text-center">
                <div class="err-status">
                    <h1><i class="fa fa-globe"></i></h1>
                </div>
                <div class="err-message">
                    <h2><?php echo __("Attualmente non sono presenti lingue."); ?></h2>
                </div>
                <br >
                <a href="javascript:void(0)" ng-click="addLanguage()" class="btn btn-default btn-lg"><i class="fa fa-plus"></i> <?php echo __("Aggiungi lingua"); ?></a>
                <br >
                <br >
                <br >
            </div>
        </div>
    </div>

    <div ng-show="languages.length" class="page page-table">
        <div class="row">
            <div class="col-lg-10 col-lg-offset-1 col-md-12">
                <section class="panel panel-default table-dynamic">
                    <div class="panel-body">
                        <div class="table-responsive">
                            <table class="table table-bordered table-striped table-responsive">
                                <thead>
                                <tr>
                                    <th>
                                        <div class="th">
                                            <?php echo __("Lingua"); ?>
                                        </div>
                                    </th>
                                    <th>
                                        <div class="th">
                                            <?php echo __("Codice"); ?>
                                        </div>
                                    </th>
                                    <th>
                                        <div class="th">
                                            <?php echo __("Data di creazione"); ?>
                                        </div>
                                    </th>
                                    <th class="text-right" width="120">
                                        <div class="th">
                                            <?php echo __("Azioni"); ?>
                                        </div>
                                    </th>
                                </tr>
                                </thead>
                                <tbody>
                                <tr data-ng-repeat="language in languages">
                                    <td>{{language.lang}}</td>
                                    <td>{{language.code}}</td>
                                    <td>{{language.timestamp_created | date:'dd/MM/yyyy  H:mm'}}</td>
                                    <td  class="text-right">
                                        <a ng-if="language.language_id != default_language_id" class="btn-icon btn-icon-sm bg-danger" href="javascript:;" confirm-callback="deleteLanguage(language.id)" confirm-message="<?php echo __("Sei sicuro di voler eliminare la lingua ?"); ?>" confirm-modal><i class="glyphicon glyphicon-trash"></i></a>
                                    </td>
                                </tr>
                                </tbody>
                            </table>
                        </div>
                    </div>
                </section>
            </div>
        </div>
    </div>
</div>