<div class="modal-header">
    <h3 class="modal-title"><?php echo __("Aggiungi lingua"); ?></h3>
</div>
<div class="modal-body">
    <form name="addLanguage" class="form-horizontal form-validation">
        <div class="form-group">
            <div class="col-sm-12">
                <oi-multiselect
                    ng-options="language.id as language.lang for language in languages | limitTo: 10 "
                    ng-model="language.language_id"
                    placeholder="Select"
                    required
                    ></oi-multiselect>
            </div>
        </div>
    </form>
</div>
<div class="modal-footer">
    <button class="btn btn-primary" ng-disabled="addLanguage.$invalid" ng-click="saveLanguage()"><?php echo __("Salva"); ?></button>
    <button class="btn btn-warning" ng-click="cancel()"><?php echo __("Annulla"); ?></button>
</div>