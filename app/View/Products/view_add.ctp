<div class="lapagina add-page" >
<nav class="navbar navbar-default navbar-static-top" role="navigation">
    <div class="navbar-header">
        <ul class="nav navbar-nav">
            <li>
                <ol class="breadcrumb">
                    <li><a href="#/products"><?php echo __("I tuoi prodotti"); ?></a></li>
                    <li class="active"><a><?php echo __("Aggiungi prodotto"); ?></a></li>
                </ol>
            </li>
        </ul>
        <ul class="nav navbar-nav navbar-right">
            <div class="navbar-form">
                <img src="/asset/img/easyadmin.png" style="height:30px"/>
            </div>
        </ul>
    </div>
</nav>
<div class="page page-table" data-ng-controller="ProductCtrl">
<div class="row">
    <div class="col-sm-12">
        <div class="panel-body">
            <div ng-show="successSave" class="callout callout-success">
                <p><?php echo __("Prodotto aggiunto con successo"); ?></p>
                <a href="#/products"><?php echo __("Torna alla lista dei prodotti"); ?></a>
            </div>
            <div ng-if="onLoading">
                <p class="text-muted"><i class="fa fa-spinner fa-spin"></i> Caricamento {{current_image}} in corso.</p>
                <progress class="progress-striped active">
                    <bar value="progress" type="success"><span>{{count_image}} / {{uploader.queue.length}}</span></bar>
                </progress>
            </div>
            <div ng-if="onLoading || successSave">
                <ul class="list-unstyled">
                    <li data-ng-repeat="success_image in success_images" class="text-success"><i class="fa fa-check"></i> {{success_image}} <b><?php echo __("aggiunto con successo"); ?></b></li>
                </ul>
                <ul class="list-unstyled">
                    <li data-ng-repeat="error_image in error_images" class="text-danger"><i class="fa fa-times"></i> {{error_image}} <b><?php echo __("errore nel caricamento"); ?></b></li>
                </ul>
            </div>
            <div ng-show="errorSave" class="callout callout-danger">
                <?php echo __("Si è verificato un problema durante l'aggiunta del prodotto."); ?>
            </div>
        </div>
    </div>
</div>
<div class="row" ng-if="!onLoading && !successSave">
<div class="col-sm-12 col-md-8 col-lg-9">
    <section class="panel panel-default">
        <div class="panel-heading"><strong><span class="flag-icon flag-icon-it"></span>&nbsp;<?php echo __("Nome"); ?> </strong>
        </div>
        <div class="panel-body">
            <input type="text" data-ng-model="product.content.name" class="form-control" data-ng-change="updateURL()">
            <span>uri: <a contenteditable data-ng-model="product.content.uri"></a></span>
        </div>
    </section>

    <section class="panel panel-default">
        <div class="panel-heading"><strong><span class="flag-icon flag-icon-it"></span>&nbsp;<?php echo __("Descrizione"); ?></strong></div>
        <div class="panel-body">
            <div text-angular data-ng-model="product.content.description"></div>
        </div>
    </section>

    <div class="row">
        <div class="col-sm-12">
            <section class="panel panel-default table-dynamic">
                <div class="panel-heading"><strong> <?php echo __("Prezzo"); ?></strong></div>
                <div class="panel-body">
                    <div class="row">
                        <div class="col-md-4">
                            <label for=""><?php echo __("Prezzo"); ?></label>
                            <input type="number" data-ng-change="changeTotalPrice()" step="0.01" min="0" class="form-control" placeholder="0.00" data-ng-model="product.price" required>
                        </div>
                        <div class="col-md-4">
                            <label for=""><?php echo __("Imposta"); ?></label>
                            <div class="input-group">
                                <select data-ng-change="changeTotalPrice()" class="form-control" data-ng-options="tax.id as tax.label for tax in taxes" data-ng-model="product.shop_tax_id">
                                    <option value=""><?php echo __("Seleziona un'imposta"); ?></option>
                                </select>
                                        <span class="input-group-btn">
                                            <button type="button" class="form-control btn btn-primary" ng-click="taxModal()"><i class="fa fa-plus"></i></button>
                                        </span>
                            </div>
                        </div>
                        <div class="col-md-4">
                            <label for=""><?php echo __("Prezzo finale"); ?></label>
                            <input type="text" disabled readonly class="form-control" data-ng-model="total_price" />
                        </div>
                    </div>
                </div>
            </section>
        </div>
    </div>

    <div class="row">
        <div class="col-sm-12">
            <section class="panel panel-default table-dynamic">
                <div class="panel-heading"><strong> <?php echo __("Quantità"); ?></strong></div>
                <div class="panel-body">
                    <div class="row">
                        <div class="col-sm-4">
                            <label for=""><?php echo __("Definita"); ?></label>
                            <div class="input-group">
                                <label class="switch switch-info">
                                    <input type="radio" value="0" data-ng-model="product.type_quantity">
                                    <i></i>
                                </label>
                            </div>
                            <div class="row">
                                <div class="col-sm-6">
                                    <input ng-disabled="product.type_quantity != 0" type="number" class="form-control col-sm-2" min="0" placeholder="1" data-ng-model="product.quantity">
                                </div>
                            </div>
                        </div>
                        <div class="col-sm-4">
                            <div class="input-group">
                                <label for=""><?php echo __("Illimitata"); ?></label>
                                <div class="clearfix"></div>
                                <label class="switch switch-info">
                                    <input type="radio" value="1" data-ng-model="product.type_quantity" >
                                    <i></i>
                                </label>
                            </div>
                        </div>
                        <div class="col-sm-4">
                            <div class="input-group">
                                <label for=""><?php echo __("Richiedi Disponibilit&agrave;"); ?></label>
                                <div class="clearfix"></div>
                                <label class="switch switch-info">
                                    <input type="radio" value="2" data-ng-model="product.type_quantity">
                                    <i></i>
                                </label>
                            </div>
                        </div>
                    </div>
                </div>
            </section>
        </div>
    </div>

    <div class="row">
        <div class="col-sm-12">
            <section class="panel panel-default table-dynamic">
                <div class="panel-heading"><strong> <?php echo __("Collezione"); ?></strong></div>
                <div class="panel-body">
                    <div class="row">
                        <div class="col-xs-12">
                            <div class="input-group">
                                <select class="form-control" data-ng-options="collection.id as collection.name for collection in collections" data-ng-model="product.product_category_id">
                                    <option value=""><?php echo __("Seleziona una collezione"); ?></option>
                                </select>
                                <span class="input-group-btn">
                                    <button type="button" ng-click="addCollectionModal()" class="form-control btn btn-primary" ><i class="fa fa-plus"></i></button>
                                </span>
                            </div>
                        </div>
                    </div>
                </div>
            </section>
        </div>
    </div>

    <div class="row">
        <div class="col-sm-12">
            <section class="panel panel-default table-dynamic">
                <div class="panel-heading"><strong> <?php echo __("Spedizioni"); ?></strong></div>
                <div class="panel-body">
                    <div class="row">
                        <div class="col-xs-12">
                            <div class="input-group">
                                <select class="form-control" data-ng-options="shipping.id as shipping.name for shipping in shipments" data-ng-model="product.shipping_group_id">
                                    <option value=""><?php echo __("Seleziona un gruppo di spedizione"); ?></option>
                                </select>
                                <span class="input-group-btn">
                                    <button type="button" class="form-control btn btn-primary" ng-click="addShippingModal()"><i class="fa fa-plus"></i></button>
                                </span>
                            </div>
                        </div>
                    </div>
                </div>
            </section>
        </div>
    </div>

    <div class="row">
        <div class="col-sm-12">
            <section class="panel panel-default table-dynamic">
                <div class="panel-heading"><strong> <?php echo __("Tags"); ?></strong></div>
                <div class="panel-body">
                    <div class="row">
                        <div class="col-xs-12">
                            <tags-input placeholder="<?php echo __('Aggiungi tag'); ?>" ng-model="product.tags" class="ui-tags-input">
                                <auto-complete minLength="1" source="loadTags($query)"></auto-complete>
                            </tags-input>
                        </div>
                    </div>
                </div>
            </section>
        </div>
    </div>

    <section class="panel panel-default">
        <div class="panel-body text-right">
            <button class="btn btn-success btn-lg" ng-click="save();" type="button"><?php echo __("Salva"); ?></button>
        </div>
    </section>
</div>
<div class="col-sm-12 col-md-4 col-lg-3 ">
    <section class="panel panel-default">
        <div class="panel-heading"><strong><i class="fa fa-sliders"></i> <?php echo __("Impostazioni"); ?></strong></div>
        <div class="panel-body">
            <button class="btn btn-info btn-block" ng-click="openSeo()" type="button"><i class="fa fa-search"></i> <?php echo __("Motori di ricerca"); ?></button>
            <hr>
            <div class="row">
                <div class="col-sm-12">
                    <div class="input-group">
                        <label><?php echo __("Visibile"); ?></label>
                        <div class="clearfix"></div>
                        <label class="switch switch-info">
                            <input type="checkbox" value="1" data-ng-model="product.published" >
                            <i></i>
                        </label>
                    </div>
                </div>
            </div>
        </div>
    </section>

    <section class="panel panel-default table-dynamic" >
        <div class="panel-heading"><strong><i class="glyphicon glyphicon-picture"></i> <?php echo __("Immagini") ?></strong></div>
        <div class="panel-body">
            <input class="btn-primary"  ng-disabled="onLoading" accept="image/*" id="page_image" data-ng-model="images" title="<?php echo __("Seleziona immagini") ?>" data-ui-file-upload type="file" nv-file-select uploader="uploader" multiple>
            <div class="divider"></div>
            <div class="container-immagini">
                <ul class="list-unstyled" id="uploadImg" ui-sortable="sortableOptions" ng-model="uploader.queue">
                    <li ng-repeat="item in uploader.queue">
                        <div ng-thumb="{ file: item._file, height: 100 }"></div>
                        <div>
                            <button ng-click="item.remove()" class="btn btn-danger btn-xs"><i class="fa fa-trash-o"></i></button>
                        </div>
                    </li>
                </ul>
            </div>
        </div>
    </section>

</div>
</div>
</div>
</div>