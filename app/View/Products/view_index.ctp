<div class="lapagina" data-ng-controller="ProductsCtrl">
    <nav class="navbar navbar-default navbar-static-top" role="navigation">
        <div class="navbar-header">
            <ul class="nav navbar-nav">
                <li>
                    <ol class="breadcrumb">
                        <li class="active"><?php echo __("I tuoi prodotti"); ?></li>
                    </ol>
                </li>
            </ul>
            <ul class="nav navbar-nav navbar-right">
                <div class="navbar-form">
                    <a href="#/products/add/{{selected_language}}" class="btn btn-success"><i class="fa fa-plus"></i> <?php echo __("Aggiungi prodotto"); ?></a>
                </div>
            </ul>
        </div>
    </nav>

    <div ng-if="!products.length" class="page-err">
        <div class="err-container">
            <div class="text-center">
                <div class="err-status">
                    <h1><i class="fa fa-archive"></i></h1>
                </div>
                <div class="err-message">
                    <h2><?php echo __("Attualmente non sono presenti prodotti."); ?></h2>
                </div>
                <br >
                <a href="#/products/add/{{selected_language}}" class="btn btn-default btn-lg"><i class="fa fa-plus"></i> <?php echo __("Aggiungi prodotto"); ?></a>
                <br >
                <br >
                <br >
            </div>
        </div>
    </div>

    <div ng-show="products.length" class="page page-table">
        <div class="row">
            <div class="col-md-12">
                <!-- new code -->
                <section class="panel panel-default">
                    <div class="panel-heading">
                        <div class="pull-left">
                            <div data-ng-hide="!checkSelected()">
                                <div class="btn-group" dropdown="">
                                    <button class="btn btn-info dropdown-toggle btn-xs" type="button" aria-haspopup="true" aria-expanded="true">
                                        <i class="fa fa-tags"></i> <?php echo __("Aggiungi alla collezione"); ?> <span class="caret"></span>
                                    </button>
                                    <ul role="menu" class="dropdown-menu">
                                        <li data-ng-repeat="collection in collections"><a data-ng-click="addToCollection(collection.id)" href="javascript:;">{{collection.name}}</a></li>
                                        <li class="divider"></li>
                                        <li><a data-ng-click="addCollectionModal()" href="javascript:;"><?php echo __("Aggiungi a nuova collezione"); ?></a></li>
                                    </ul>
                                </div>
                                <div class="btn-group" dropdown="">
                                    <button class="btn btn-info dropdown-toggle btn-xs" type="button" aria-haspopup="true" aria-expanded="true">
                                        <i class="fa fa-truck"></i> <?php echo __("Aggiungi al gruppo di spedizione"); ?> <span class="caret"></span>
                                    </button>
                                    <ul role="menu" class="dropdown-menu">
                                        <li data-ng-repeat="shipping_group in shipments"><a data-ng-click="addToShippingGroup(shipping_group.id)" href="javascript:;">{{shipping_group.name}}</a></li>
                                        <li class="divider"></li>
                                        <li><a data-ng-click="addShippingGroup()" href="javascript:;"><?php echo __("Aggiungi nuovo gruppo di spedizione"); ?></a></li>
                                    </ul>
                                </div>
                                <button class="btn btn-danger btn-xs" confirm-callback="deleteProducts()" confirm-message="<?php echo __("Sei sicuro di voler eliminare i prodotti selezionati ?"); ?>" confirm-modal>
                                    <i class="fa fa-trash-o"></i> <?php echo __("Elimina"); ?>
                                </button>
                            </div>
                            <div data-ng-hide="checkSelected()">
                                <strong class="hidden-xs"><span class="fa fa-level-up fa-rotate-180"></span> Seleziona pi&uacute; prodotti per effettuare operazioni multiple:</strong>
                            </div>
                        </div>
                        <div is-open="status.isopen2" dropdown="" class="btn-group pull-right">
                            <button class="btn btn-primary dropdown-toggle btn-xs" type="button" aria-haspopup="true" aria-expanded="false"> Visualizza Lingua <span class="caret"></span> </button>
                            <ul role="menu" class="dropdown-menu">
                                <li data-ng-class="{'active':(lang_id == selected_language)}" data-ng-repeat="(id, lang_id) in objectKeys(shop_languages)" data-ng-click="selectLanguage(lang_id)"><a href="javascript:;">{{shop_languages[lang_id]}}</a></li>
                            </ul>
                        </div>
                        <div class="clearfix"></div>
                    </div>
                    <div class="panel-body">
                        <div class="table-responsive">
                            <table class="table table-striped">
                                <thead>
                                <tr>
                                    <th style="width: 68px;">
                                        <label class="ui-checkbox">
                                            <input type="checkbox" data-ng-model="obj.selectAll" data-ng-click="toggleList()">
                                            <span style="color:#FFF"></span>
                                        </label>
                                    </th>
                                    <th><?php echo __("Immagine"); ?></th>
                                    <th><?php echo __("Prodotto"); ?></th>
                                    <th><?php echo __("Collezione"); ?></th>
                                    <th><?php echo __("Prezzo"); ?></th>
                                    <th><?php echo __("Quantità"); ?></th>
                                    <th><?php echo __("Visibile"); ?></th>
                                    <th data-ng-repeat="(id, lang_id) in objectKeys(shop_languages)" style="width: 100px;">{{shop_languages[lang_id]}}</th>
                                    <th class="text-right"><?php echo __("Azioni"); ?></th>
                                </tr>
                                </thead>
                                <tbody>
                                <tr data-ng-repeat="product in products">
                                    <td>
                                        <label class="ui-checkbox">
                                            <input type="checkbox" value="{{product.id}}" data-ng-model="product.selected" data-ng-change="updateSelected()">
                                            <span style="color:#FFF"></span>
                                        </label>
                                    </td>
                                    <td>
                                        <img height="50" src="{{product.image}}" />
                                    </td>
                                    <td>
                                        <a href="#/products/edit/{{selected_language}}/{{product.id}}" class="text-info"  ng-if="product.content_id"><b>{{product.name}}</b></a>
                                        <a href="#/products/edit/{{selected_language}}/{{product.id}}" class="text-primary"  ng-if="!product.content_id"><i class="primary">{{product.name}}</i></a>
                                    </td>
                                    <td>{{product.product_category}}</td>
                                    <td>{{product.price}}</td>
                                    <td>{{product.quantity}}</td>
                                    <td>
                                        <span ng-if="!product.published" class="label label-danger"><i class="fa fa-times"></i></span>
                                        <span ng-if="product.published" class="label label-success"><i class="fa fa-check"></i></span>
                                    </td>
                                    <td data-ng-repeat="content in product.contents">
                                        <a href="#/products/edit/{{content.lang_id}}/{{product.id}}" ng-if="content.content_id" class="btn btn-success btn-xs btn-block" type="button">
                                            <i class="glyphicon glyphicon-pencil"></i>&nbsp;Modifica
                                        </a>
                                        <a
                                            href="#/products/edit/{{content.lang_id}}/{{product.id}}"
                                            ng-if="!content.content_id"
                                            tooltip-append-to-body="true"
                                            tooltip="Il contenuto in questa lingua non esiste, clicca qui per inserirlo"
                                            tooltip-placement="top"
                                            class="btn btn-primary btn-xs btn-block"
                                            type="button"><i class=" glyphicon glyphicon-pencil"></i>&nbsp;Crea
                                        </a>
                                    </td>
                                    <td class="text-right">
                                        <div is-open="product.isopen" dropdown="" class="btn-group pull-right">
                                            <button class="btn btn-default dropdown-toggle btn-xs" type="button" aria-haspopup="true" aria-expanded="false"> Azioni <span class="caret"></span> </button>
                                            <ul role="menu" class="dropdown-menu">
                                                <?php
                                                foreach($plugin_elements as $element) {
                                                    echo '<li>'.$element.'</li>';
                                                }
                                                ?>
                                                <li><a href="javascript:void(0);" confirm-callback="clone(product.id)" confirm-message="<?php echo __("Sei sicuro di voler clonare questo prodotto ?"); ?>" confirm-modal><i class="fa fa-files-o"></i> <?php echo __("Duplica"); ?></a></li>
                                                <li><a href="javascript:void(0);" confirm-callback="delete(product.id)" confirm-message="<?php echo __("Sei sicuro di voler eliminare questo prodotto ?"); ?>" confirm-modal><i class=" glyphicon glyphicon-trash"></i> <?php echo __("Elimina"); ?></a></li>
                                            </ul>
                                        </div>
                                    </td>
                                </tr>
                                </tbody>
                            </table>
                        </div>


                        <footer class="table-footer">
                            <div class="row">
                                <div class="col-md-6 page-num-info">
                                    <span>
                                        <?php echo __("Elementi per pagina"); ?>
                                        <select data-ng-model="numPerPage" data-ng-options="num for num in numPerPageOpt" data-ng-change="onNumPerPageChange()">
                                        </select>
                                    </span>
                                </div>
                                <div class="col-md-6 text-right pagination-container">
                                    <pagination class="pagination-sm" ng-model="currentPage" total-items="totalProducts" max-size="4" ng-change="select(currentPage)" items-per-page="numPerPage" rotate="false" boundary-links="true"></pagination>
                                </div>
                            </div>
                        </footer>

                    </div>
                </section>
                <!-- new code -->
            </div>
        </div>
    </div>
</div>