<div class="lapagina" data-ng-controller="OrderCtrl">
	<nav class="navbar navbar-default navbar-static-top" role="navigation">
    	<div class="navbar-header">
        	<ul class="nav navbar-nav">
                <li>
                    <ol class="breadcrumb">
                        <li><a href="#/orders"><?php echo __("Ordini"); ?></a></li>
                        <li class="active"><?php echo __("Ordine n."); ?> {{order.order.uid}} </li>
                    </ol>
                </li>
            </ul>
        </div>
	</nav>
    <div class="page page-table">
        <div class="row">
            <div class="col-md-12">
                <section class="panel panel-default table-dynamic">
                    <div class="panel-heading">
                        <?php echo __("Ordine n.") ?> <b>{{order.order.uid}}</b> <?php echo __("del"); ?> <b>{{order.order.date | date:'dd/MM/yyyy'}}</b>
                    </div>
                    <div class="panel-body">
                        <div class="row userInfo">
                            <div class="col-sm-6">
                            	<div class="row">
                                	<div class="col-sm-12">
                                        <h2 class="block-title-2"><?php echo __("Stato dell'ordine"); ?></h2>
                                        <div class="callout callout-info">
                                            <select class="form-control" data-ng-options="status.id as status.name for status in order_status" data-ng-model="order.order.status"></select>
                                            <hr />
                                            <button data-ng-click="changeStatus()" class="btn btn-success"><?php echo __("Salva stato dell'ordine"); ?></button>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="col-sm-6">
                                <div class="row">

                                    <div class="col-sm-6">
                                        <h2 class="block-title-2"><?php echo __("Indirizzo di spedizione"); ?></h2>
                                        <div class="well">
                                            <dl>
                                                <dt>{{order.shipping_address.full_name}}</dt>
                                                <dt>{{order.shipping_address.business_name}}</dt
                                                <dt>{{order.shipping_address.tax_code}}</dt>
                                                <dt>{{order.shipping_address.address}}</dt>
                                                <dt>{{order.shipping_address.postal_code}}</dt>
                                                <dt>{{order.shipping_address.city}}</dt>
                                                <dt>{{order.shipping_address.province}}</dt>
                                                <dt>{{order.shipping_address.country}}</dt>
                                                <dt>{{order.shipping_address.phone_number}}</dt>
                                                <dt>{{order.shipping_address.email}}</dt>
                                            </dl>
                                        </div>
                                    </div>
                                    <div class="col-sm-6">
                                        <h2 class="block-title-2"><?php echo __("Indirizzo di fatturazione"); ?></h2>
                                        <div class="well">
                                            <dl>
                                                <dt>{{order.billing_address.full_name}}</dt>
                                                <dt>{{order.billing_address.business_name}}</dt>
                                                <dt>{{order.billing_address.tax_code}}</dt>
                                                <dt>{{order.billing_address.address}}</dt>
                                                <dt>{{order.billing_address.postal_code}}</dt>
                                                <dt>{{order.billing_address.city}}</dt>
                                                <dt>{{order.billing_address.province}}</dt>
                                                <dt>{{order.billing_address.country}}</dt>
                                                <dt>{{order.billing_address.phone_number}}</dt>
                                                <dt>{{order.billing_address.email}}</dt>
                                            </dl>
                                        </div>
                                    </div>
                                    <div class="col-lg-12">
                                        <h2 class="block-title-2"><?php echo __("Metodo di pagamento"); ?></h2>
                                        <div class="well">
                                            <dl>
                                              <dt>{{order.payment.label}}</dt>
                                              <dd>{{order.payment.value}}</dd>
                                            </dl>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="row userInfo">
                            <div class="col-lg-12">
                                <h2 class="block-title-2"> <?php echo __("Riepilogo ordine"); ?> </h2>
                            </div>
                            <div class="col-xs-12 col-sm-12">
                                <table class="table table-responsive table-bordered" style="width:100%">
                                <tbody>
                                <tr class="CartProduct cartTableHeader">
                                  <th style="width:15%"> <?php echo __("Foto"); ?> </th>
                                  <th class="checkoutReviewTdDetails"><?php echo __("Prodotto"); ?></th>
                                  <th style="width:20%"><?php echo __("Prezzo Unitario"); ?></th>
                                  <th class="hidden-xs" style="width:15%"><?php echo __("Quantità"); ?></th>
                                  <th style="width:15%" class="text-right"><?php echo __("Totale"); ?></th>
                                </tr>
                                <tr class="CartProduct" ng-repeat="product in order.products">
                                  <td class="CartProductThumb">
                                      <div> <a><img data-ng-src="{{product.image}}"></a> </div>
                                  </td>
                                  <td>
                                    <div class="CartDescription">
                                        <h4>{{product.name}}</h4>
                                    </div>
                                  </td>
                                  <td class="delete"><div class="price ">{{product.unit}}</div></td>
                                  <td class="hidden-xs">{{product.quantity}}</td>
                                  <td class="price text-right">{{product.price}}</td>
                                </tr>
                                
                                </tbody>
                                </table>
                                <table class="table-bordered table table-striped">
                                <tbody>
                                <tr class="cart-total-price ">
                                    <td><?php echo __("Totale imponibile prodotti"); ?></td>
                                    <td width="15%" class="price text-right">{{order.total.product_taxable}} </td>
                                </tr>
                                <tr style="">
                                  <td><?php echo __("Totale imponibile spedizioni"); ?></td>
                                  <td width="15%" class="price text-right">{{order.total.shipping_taxable}}</td>
                                </tr>
                                <tr>
                                    <td><?php echo __("IVA prodotti"); ?></td>
                                    <td width="15%" id="total-tax" class="price text-right">{{order.total.product_tax}}</td>
                                </tr>
                                <tr>
                                  <td><?php echo __("IVA spedizioni"); ?></td>
                                  <td width="15%" id="total-tax" class="price text-right">{{order.total.shipping_tax}}</td>
                                </tr>
                                <tr>
                                  <td><?php echo __("Extra"); ?></td>
                                  <td width="15%" id="total-tax" class="price text-right">{{order.total.extra}}</td>
                                </tr>
                                <tr>
                                  <td><?php echo __("Totale"); ?>  </td>
                                  <td width="15%" id="total-price" class="price text-right">{{order.total.amount}} </td>
                                </tr>
                                </tbody><tbody>
                                </tbody>
                                </table>
                            </div>                        
                        </div>
                    </div>
                </section>
            </div>
        </div>
    </div>
</div>