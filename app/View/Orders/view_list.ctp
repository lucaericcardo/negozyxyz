<div class="lapagina" data-ng-controller="OrdersCtrl">
	<nav class="navbar navbar-default navbar-static-top" role="navigation">
    	<div class="navbar-header">
            <ol class="breadcrumb nav navbar-nav">
                <li class="active"><?php echo __("Ordini"); ?></li>
            </ol>
        </div>
	</nav>
	<div ng-show="!orders.length" class="page-err">
        <div class="err-container">
            <div class="text-center">
                <div class="err-status">
                     <h1><i class="fa fa-minus-square-o"></i></h1>
                </div>
                <div class="err-message">
                    <h2><?php echo __("Attualmente non sono presenti ordini."); ?></h2>
                </div>
            </div>
        </div>
    </div>

    <div ng-show="orders.length" class="page page-table">
        <div class="row">
            <div class="col-xs-12">
                <section class="panel panel-default table-dynamic">
                    <div class="panel-body">
                        <div class="table-responsive">
                            <table class="table table-striped">
                                <thead>
                                <tr>
                                    <th>
                                        <div class="th">
                                            <?php echo __("Cliente"); ?>
                                            <span class="glyphicon glyphicon-chevron-up" data-ng-click=" order('first_name','ASC') " data-ng-class="{active: row == 'first_name' && orderType == 'ASC'}"></span>
                                            <span class="glyphicon glyphicon-chevron-down" data-ng-click=" order('first_name','DESC') " data-ng-class="{active: row == 'first_name' && orderType == 'DESC'}"></span>
                                        </div>
                                    </th>
                                    <th>
                                        <div class="th">
                                            <?php echo __("Email"); ?>
                                            <span class="glyphicon glyphicon-chevron-up" data-ng-click=" order('email','ASC') " data-ng-class="{active: row == 'email' && orderType == 'ASC'}"></span>
                                            <span class="glyphicon glyphicon-chevron-down" data-ng-click=" order('email','DESC') " data-ng-class="{active: row == 'email' && orderType == 'DESC'}"></span>
                                        </div>
                                    </th>
                                    <th>
                                        <div class="th">
                                            <?php echo __("Codice"); ?>
                                            <span class="glyphicon glyphicon-chevron-up" data-ng-click=" order('order_uid','ASC') " data-ng-class="{active: row == 'order_uid' && orderType == 'ASC'}"></span>
                                            <span class="glyphicon glyphicon-chevron-down" data-ng-click=" order('order_uid','DESC') " data-ng-class="{active: row == 'order_uid' && orderType == 'DESC'}"></span>
                                        </div>
                                    </th>
                                    <th>
                                        <div class="th">
                                            <?php echo __("Stato"); ?>
                                            <span class="glyphicon glyphicon-chevron-up" data-ng-click=" order('order_status_id','ASC') " data-ng-class="{active: row == 'order_status_id' && orderType == 'ASC'}"></span>
                                            <span class="glyphicon glyphicon-chevron-down" data-ng-click=" order('order_status_id','DESC') " data-ng-class="{active: row == 'order_status_id' && orderType == 'DESC'}"></span>
                                        </div>
                                    </th>
                                    <th>
                                        <div class="th">
                                            <?php echo __("Data"); ?>
                                            <span class="glyphicon glyphicon-chevron-up" data-ng-click=" order('created','ASC') " data-ng-class="{active: row == 'created' && orderType == 'ASC'}"></span>
                                            <span class="glyphicon glyphicon-chevron-down" data-ng-click=" order('created','DESC') " data-ng-class="{active: row == 'created' && orderType == 'DESC'}"></span>
                                        </div>
                                    </th>
                                    <th>
                                        <div class="th">
                                            <?php echo __("Totale"); ?>
                                            <span class="glyphicon glyphicon-chevron-up" data-ng-click=" order('total_amount','ASC') " data-ng-class="{active: row == 'total_amount' && orderType == 'ASC'}"></span>
                                            <span class="glyphicon glyphicon-chevron-down" data-ng-click=" order('total_amount','DESC') " data-ng-class="{active: row == 'total_amount' && orderType == 'DESC'}"></span>
                                        </div>
                                    </th>
                                    <th>
                                        <div class="th">
                                            <?php echo __("Azione"); ?>
                                        </div>
                                    </th>
                                </tr>
                                </thead>
                                <tbody>
                                <tr data-ng-repeat="order in orders">
                                    <td>{{order.user}}</td>
                                    <td>{{order.email}}</td>
                                    <td>{{order.uid}}</td>
                                    <td><span ng-class="order.label_class">{{order.status}}</span></td>
                                    <td>{{order.date | date:'dd/MM/yyyy  H:mm'}}</td>
                                    <td>{{order.amount}}</td>
                                    <td>
                                        <a ng-if="!order.request" class="btn-icon btn-icon-sm bg-info" ng-href="#/orders/{{order.id}}" > <i class="fa fa-eye"></i></a>
                                        <a ng-if="order.request" class="btn-icon btn-icon-sm bg-info" ng-href="#/orders/request/{{order.id}}" > <i class="fa fa-eye"></i></a>
                                    </td>
                                </tr>
                                </tbody>
                            </table>
                        </div>
                        <footer class="table-footer">
                            <div class="row">
                                <div class="col-md-6 page-num-info">
                                    <span>
                                        <?php echo __("Elementi per pagina"); ?>
                                        <select data-ng-model="numPerPage" data-ng-options="num for num in numPerPageOpt" data-ng-change="onNumPerPageChange()">
                                        </select>
                                    </span>
                                </div>
                                <div class="col-md-6 text-right pagination-container">
                                    <pagination class="pagination-sm" ng-model="currentPage" total-items="totalOrders" max-size="4" ng-change="select(currentPage)" items-per-page="numPerPage" rotate="false" boundary-links="true"></pagination>
                                </div>
                            </div>
                        </footer>
                    </div>
                </section>
            </div>
        </div>
    </div>

</div>