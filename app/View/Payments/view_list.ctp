<div class="lapagina">
    <nav class="navbar navbar-default navbar-static-top" role="navigation">
        <div class="navbar-header">
            <ul class="nav navbar-nav">
                <li>
                    <ol class="breadcrumb">
                        <li class="active"><a><?php echo __("Metodi di pagamento"); ?></a></li>
                    </ol>
                </li>
            </ul>
        </div>
    </nav>
    <div class="page page-table" data-ng-controller="paymentsCtrl">
    	<div class="row">
          <div class="col-lg-10 col-lg-offset-1 col-md-12">
          	<div class="alert alert-danger" data-ng-if="!settingStatus.payments">
                <i class="fa fa-exclamation-triangle"></i> <?php echo __("Per poter pubblicare lo shop è necessario impostare correttamente ed attivare almeno un metodo di pagamento"); ?>
            </div>
            <section class="panel panel-default">
                    
                <div class="panel-heading">
                    <h4>Scegli e configura i metodi di pagamento che desideri ricevere</h4>
                </div>
                
                <div class="panel-body">
                    <form name="payments" class="form-horizontal form-validation">
                    <div class="table-responsive">
                        <table class="table table-striped table-bordered">
                            <thead>
                                <tr>
                                    <th width="100">Attivo</th>
                                    <th>Nome</th>
                                    <th>Valore</th>
                                    <th width="150">Prezzo Extra</th>
                                </tr>
                            </thead>
                            <tbody>
                                <tr>
                                	<td>
                                        <label class="switch switch-info">
                                            <input data-ng-model="payments.bonifico.enabled" type="checkbox">
                                            <i></i>
                                        </label>
                                    </td>
                                    <td>Bonifico</td>
                                    <td><input type="text" data-ng-model="payments.bonifico.value"  ng-required="payments.bonifico.enabled" data-ng-disabled="!payments.bonifico.enabled" id="bonifico" class="form-control" placeholder="IBAN"></td>
                                    <td><input type="number" data-ng-model="payments.bonifico.price" data-ng-disabled="!payments.bonifico.enabled" class="form-control" min="0"></td>
                                </tr>
                                <tr>
                                	<td>
                                        <label class="switch switch-info">
                                            <input data-ng-model="payments.paypal.enabled" type="checkbox">
                                            <i></i>
                                        </label>
                                    </td>
                                    <td>PayPal e carte di credito</td>
                                    <td>
                                        <div class="input-group">
                                                <input type="text" data-ng-model="payments.paypal.value" ng-required="payments.paypal.enabled" data-ng-disabled="!payments.paypal.enabled" id="paypal" class="form-control" placeholder="Email registrata a PayPal" style="min-width:200px;">
                                                <span class="input-group-btn">
                                                    <a target="_blank" href="https://www.paypal.com/signup/account?country.x=IT&locale.x=it_IT" class="btn btn-primary">Apri un conto PayPal!</a>
                                                </span>
                                            </div>
                                    </td>
                                    <td><input type="number" data-ng-model="payments.paypal.price" data-ng-disabled="!payments.paypal.enabled" class="form-control" min="0" ></td>
                                </tr>
                                <tr>
                                	<td>
                                        <label class="switch switch-info">
                                            <input data-ng-model="payments.contrassegno.enabled" type="checkbox">
                                            <i></i>
                                        </label>
                                    </td>
                                    <td>Contrassegno</td>
                                    <td><p>La merce viene pagata al corriere in contanti</p> <input type="hidden" data-ng-model="payments.contrassegno.value" value="La merce viene pagata al corriere in contanti"></td>
                                    <td><input type="number" data-ng-model="payments.contrassegno.price" data-ng-disabled="!payments.contrassegno.enabled" class="form-control" min="0"></td>
                                </tr>
                                <tr>
                                    <td>
                                        <label class="switch switch-info">
                                            <input data-ng-model="payments.money.enabled" type="checkbox">
                                            <i></i>
                                        </label>
                                    </td>
                                    <td>Contanti</td>
                                    <td> <p>Pagamento in contanti sul posto</p><input type="hidden" data-ng-model="payments.money.value" value="Pagamento in contanti sul posto"></td>
                                    <td><input type="number" data-ng-model="payments.money.price" data-ng-disabled="!payments.money.enabled" class="form-control" min="0"></td>
                                </tr>
                            </tbody>
                        </table>
                    </div>
                    </form>
                </div>
            </section>
            <button type="button" ng-disabled="payments.$invalid" data-ng-click="save()" class="btn btn-success btn-lg">Salva</button>
		  </div>
        </div>
    </div>
</div>