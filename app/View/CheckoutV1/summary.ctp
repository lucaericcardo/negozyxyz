
<div class="row">
<?php
if(isset($cart['wrong_shipping']) && $cart['wrong_shipping'])
{
    echo $this->element("checkout/no_shipping",array("cart",$cart));
}
else
{
    ?>
    <div class="col-sm-6">
        <div class="row">
            <div class="col-sm-6">
                <h4>Indirizzo di spedizione</h4>
                <hr />
                <address>
                    <?php
                    echo '<p>'.$shipping_address['first_name'].' '.$shipping_address['last_name'].'</p>';
                    if($shipping_address['business_name']) {
                        echo '<p>'.$shipping_address['business_name'].'</p>';
                    }
                    if($shipping_address['tax_code']) {
                        echo '<p>'.$shipping_address['tax_code'].'</p>';
                    }
                    echo '<p>'.$shipping_address['phone_number'].'</p>';
                    echo '<p>'.$shipping_address['address'].' '.$shipping_address["postal_code"].' '.$shipping_address["city"].' ('.$shipping_address["province"].')</p>';
                    echo '<p>'.$countries[$shipping_address['country_id']].'</p>';
                    ?>
                </address>
            </div>
            <div class="col-sm-6">
                <h4>Indirizzo di fatturazione</h4>
                <hr />
                <address>
                    <?php
                    if(($billing_address['same_shipping'])){
                        echo '<p>'.$shipping_address['first_name'].' '.$shipping_address['last_name'].'</p>';
                        if($shipping_address['business_name']) {
                            echo '<p>'.$shipping_address['business_name'].'</p>';
                        }
                        if($shipping_address['tax_code']) {
                            echo '<p>'.$shipping_address['tax_code'].'</p>';
                        }
                        echo '<p>'.$shipping_address['phone_number'].'</p>';
                        echo '<p>'.$shipping_address['address'].' '.$shipping_address["postal_code"].' '.$shipping_address["city"].' ('.$shipping_address["province"].')</p>';
                        echo '<p>'.$countries[$shipping_address['country_id']].'</p>';
                    } else {
                        echo '<p>' . $billing_address['first_name'] . ' ' . $billing_address['last_name'] . '</p>';
                        if ($billing_address['business_name']) {
                            echo '<p>' . $billing_address['business_name'] . '</p>';
                        }
                        if ($billing_address['tax_code']) {
                            echo '<p>' . $billing_address['tax_code'] . '</p>';
                        }
                        echo '<p>' . $billing_address['phone_number'] . '</p>';
                        echo '<p>' . $billing_address['address'] . ' ' . $billing_address["postal_code"] . ' ' . $billing_address["city"] . ' (' . $billing_address["province"] . ')</p>';
                        echo '<p>' . $countries[$billing_address['country_id']] . '</p>';
                    }
                    ?>
                </address>
            </div>
        </div>
        <div class="clearfix"></div>
        <div class="row">
            <div class="col-sm-6">
                <h4>Costi di spedizione</h4>
                <hr />
                <p><?php echo $this->Number->currency($cart['shipping_subtotal'], $cart['shop']['currency']); ?></p>
            </div>
            <div class="col-sm-6">
                <h4>Metodo di pagamento</h4>
                <hr />
                <address>
                    <?php
                    echo $cart['payment']['label'];
                    ?>
                </address>
            </div>
        </div>
        <?php
        $label_process = __("Procedi all'acquisto");
        if($cart['request_availability'] && !$available)
        {
            echo $this->element("checkout/warning_available");
            $label_process = __("Richiedi disponibilità");
        }
        ?>
        <hr class="clearfix" />
        <a class="btn btn-default btn- pull-left" href="<?php echo "/checkoutV1/index/".$cart['shop']['url']."/".$cart['token']; ?>">Indietro</a>
        <a class="btn btn-primary pull-right" href="<?php echo "/checkoutV1/process/".$cart['shop']['url']."/".$cart['token']; ?>"><?php echo $label_process; ?></a>
        <div class="clearfix"></div>

    </div>

    <div class="col-sm-5 col-lg-offset-1">
        <div class="panel panel-default">
            <div class="panel-heading">Riepilogo ordine</div>
            <ul class="list-group">
                <?php
                if (count($cart['products'])) {
                    foreach ($cart['products'] as $product) {
                        ?>
                        <li class="list-group-item">
                            <div class="media">
                                <div class="media-left">
                                    <img style="max-width: 60px" class="img-rounded" src="<?php echo $product['image']; ?>"/>
                                </div>
                                <div class="media-body">
                                    <b><?php echo $product['name']; ?></b>
                                    <p><small><?php echo $product['quantity'] . " <b>x</b> " . $this->Number->currency($product['product_unit']['price_total'], $cart['shop']['currency']); ?></small></p>
                                </div>
                                <div class="media-right">
                                    <b><?php echo $this->Number->currency($product['product_total']['price_total'], $cart['shop']['currency']); ?></b>
                                </div>
                            </div>
                        </li>
                        <div class="cleafix"></div>
                    <?php
                    }
                }
                ?>
                <li class="list-group-item">
                    <div class="row">
                        <div class="col-xs-8 text-left">
                            Imponibile prodotti
                        </div>
                        <div class="col-xs-4 text-right">
                            <?php echo $this->Number->currency($cart['product_taxable'], $cart['shop']['currency']); ?>
                        </div>
                        <div class="col-xs-8 text-left">
                            Imponibile spedizioni
                        </div>
                        <div class="col-xs-4 text-right">
                            <?php echo $this->Number->currency($cart['shipping_taxable'], $cart['shop']['currency']); ?>
                        </div>
                        <div class="col-xs-8 text-left">
                            IVA prodotti
                        </div>
                        <div class="col-xs-4 text-right">
                            <?php echo $this->Number->currency($cart['product_tax'], $cart['shop']['currency']); ?>
                        </div>
                        <div class="col-xs-8 text-left">
                            IVA spedizioni
                        </div>
                        <div class="col-xs-4 text-right">
                            <?php echo $this->Number->currency($cart['shipping_tax'], $cart['shop']['currency']); ?>
                        </div>
                        <div class="col-xs-8 text-left">
                            Extra
                        </div>
                        <div class="col-xs-4 text-right">
                            <?php echo $this->Number->currency($cart['extra_subtotal'], $cart['shop']['currency']); ?>
                        </div>
                    </div>
                </li>
                <li class="list-group-item">
                    <div class="row">
                        <div class="col-xs-8 text-left">
                            <h4>Totale</h4>
                        </div>
                        <div class="col-xs-4 text-right">
                            <h4><?php echo $this->Number->currency($cart['total'], $cart['shop']['currency']); ?></h4>
                        </div>
                    </div>
                </li>
            </ul>
        </div>
    </div>
<?php
}
?>
</div>
