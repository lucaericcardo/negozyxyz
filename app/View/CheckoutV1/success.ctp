<div class="container main-container headerOffset">
    <div class="row">
        <div class="col-xs-12 text-center">
            <div class="page-header" style="border-bottom: none">
                <h1>Ti ringraziamo della fiducia accordata,<br> <small>non appena riceveremo il pagamento, procederemo all'evasione dell'ordine.</small></h1>
            </div>
            <a class="btn btn-success btn-lg" href="<?php echo "http://".$cart['shop']['domain']; ?>">Torna allo shop </a>
        </div>
    </div>
    <div class="row">
        <div class="col-xs-12 col-sm-8 col-sm-offset-2">
            <hr>
            <img src="//cdn.negozy.com/negozy/Img/conferma-ordine.jpg" class="img-responsive img-rounded" />
        </div>
    </div>
    <div style="clear:both">  </div>
</div><!-- /.main-container -->