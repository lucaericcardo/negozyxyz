<div class="container main-container ">
    <div class="container">
      <div class="row">
        <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
            <h1>Si sono verificati degli errori nel proseguimento dell'operazione.</h1>
            <p>Codice di errore: <?php echo $error['code']; ?></p>
            <p><?php echo $error['message']; ?></p>
            <?php
            if(isset($cart) && isset($cart['shop'])){
            ?>
            <a class="btn btn-primary" href="http://<?php echo $cart['shop']['domain']; ?>">Torna allo shop</a>
            <div class="divider"></div>
            <?php
            }
            ?>
        </div>
      </div>
    </div>
  	<div style="clear:both">  </div>
</div><!-- /.main-container -->