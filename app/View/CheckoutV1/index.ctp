<div class="container">
    <div class="row">
        <div class="col-sm-12">
            <img style="max-height: 60px" src="<?php echo ($cart['shop']['image']) ? $cart['shop']['image'] : '/asset/images/logoNegozy.png' ;?>" />
        </div>
    </div>
    <div class="row">
        <div class="col-sm-6">
            <?php
            echo $this->Form->create("User",array( 'novalidate' => true));
            ?>
            <h4> Indirizzo di spedizione </h4>
            <hr>
            <div class="row">
                <div class="col-xs-6">
                    <?php
                    echo $this->Form->input("UserAddressShipping.first_name",array(
                        "label" => __("Nome")." *",
                        "placeholder" => __("Nome"),
                        "tabindex" => "3"
                    ));
                    ?>
                </div>
                <div class="col-xs-6">
                    <?php
                    echo $this->Form->input("UserAddressShipping.last_name",array(
                        "label" => __("Cognome")." *",
                        "placeholder" => __("Cognome"),
                        "tabindex" => "4"
                    ));
                    ?>
                </div>
            </div>
            <div class="row">
                <div class="col-xs-6">
                    <?php
                    echo $this->Form->input("UserAddressShipping.business_name",array(
                        "label" => __("Ragione sociale"),
                        "placeholder" => __("Ragione sociale"),
                        "tabindex" => "4"
                    ));
                    ?>
                </div>
                <div class="col-xs-6">
                    <?php
                    echo $this->Form->input("UserAddressShipping.tax_code",array(
                        "label" => __("C.F. / P.IVA"),
                        "placeholder" => __("Codifice fiscale o partita iva"),
                        "tabindex" => "5"
                    ));
                    ?>
                </div>
            </div>
            <div class="row">
                <div class="col-xs-12">
                    <?php
                    echo $this->Form->input("UserAddressShipping.address",array(
                        "label" => __("Indirizzo")." *",
                        "placeholder" => __("Indirizzo"),
                        "tabindex" => "6"
                    ));
                    ?>
                </div>
            </div>
            <div class="row">
                <div class="col-xs-6">
                    <?php
                    echo $this->Form->input("UserAddressShipping.city",array(
                        "label" => __("Città")." *",
                        "placeholder" => __("Città"),
                        "tabindex" => "7"
                    ));
                    ?>
                </div>
                <div class="col-xs-6">
                    <?php
                    echo $this->Form->input("UserAddressShipping.postal_code",array(
                        "label" => __("CAP")." *",
                        "placeholder" => __("Codice postale"),
                        "tabindex" => "8"
                    ));
                    ?>
                </div>
            </div>
            <div class="row">
                <div class="col-xs-6">
                    <?php
                    echo $this->Form->input("UserAddressShipping.province",array(
                        "label" => __("Provincia")." *",
                        "placeholder" => __("Provincia"),
                        "tabindex" => "9"
                    ));
                    ?>
                </div>
                <div class="col-xs-6">
                    <?php
                    echo $this->Form->input("UserAddressShipping.country_id",array(
                        "label" => __("Paese")." *",
                        "options" => $countries,
                        "tabindex" => "10",
                        "default" => 115
                    ));
                    ?>
                </div>
            </div>
            <div class="row">
                <div class="col-xs-12">
                <?php
                echo $this->Form->input("UserAddressShipping.phone_number",array(
                    "label" => __("Telefono")." *",
                    "placeholder" => __("Telefono"),
                    "tabindex" => "11"
                ));
                ?>
                </div>
            </div>
            <h4> Indirizzo di fatturazione </h4>
            <hr>
            <p>Il tuo indirizzo di spedizione è lo stesso di fatturazione?
                <?php
                $check_options = array(
                    "label"     => false,
                    "id"        => "checkboxBilling",
                    "tabindex"  => "11"
                );
                if(isset($this->data['UserAddressBilling']['same_shipping']) && ($this->data['UserAddressBilling']['same_shipping'] == 0)) {
                    $check_options['checked'] = false;
                } else {
                    $check_options['checked'] = true;
                }
                echo $this->Form->checkbox("UserAddressBilling.same_shipping",$check_options);
                ?>
            </p>
            <div id="FormBilling" class="<?php echo (!isset($this->data['UserAddressBilling']) || (isset($this->data['UserAddressBilling']) && $this->data['UserAddressBilling']['same_shipping'])) ? 'hide' : ''; ?>">
                <div class="row">
                    <div class="col-xs-6">
                        <?php
                        echo $this->Form->input("UserAddressBilling.first_name",array(
                            "label" => __("Nome")." *",
                            "placeholder" => __("Nome"),
                            "tabindex" => "12"
                        ));
                        ?>
                    </div>
                    <div class="col-xs-6">
                        <?php
                        echo $this->Form->input("UserAddressBilling.last_name",array(
                            "label" => __("Cognome")." *",
                            "placeholder" => __("Cognome"),
                            "tabindex" => "13"
                        ));
                        ?>
                    </div>
                </div>
                <div class="row">
                    <div class="col-xs-6">
                        <?php
                        echo $this->Form->input("UserAddressBilling.business_name",array(
                            "label" => __("Ragione sociale"),
                            "placeholder" => __("Ragione sociale"),
                            "tabindex" => "4"
                        ));
                        ?>
                    </div>
                    <div class="col-xs-6">
                        <?php
                        echo $this->Form->input("UserAddressBilling.tax_code",array(
                            "label" => __("C.F. / P.IVA"),
                            "placeholder" => __("Codifice fiscale o partita iva"),
                            "tabindex" => "5"
                        ));
                        ?>
                    </div>
                </div>
                <div class="row">
                    <div class="col-xs-12">
                        <?php
                        echo $this->Form->input("UserAddressBilling.address",array(
                            "label" => __("Indirizzo")." *",
                            "placeholder" => __("Indirizzo"),
                            "tabindex" => "14"
                        ));
                        ?>
                    </div>
                </div>
                <div class="row">
                    <div class="col-xs-6">
                        <?php
                        echo $this->Form->input("UserAddressBilling.city",array(
                            "label" => __("Città")." *",
                            "placeholder" => __("Città"),
                            "tabindex" => "15"
                        ));
                        ?>
                    </div>
                    <div class="col-xs-6">
                        <?php
                        echo $this->Form->input("UserAddressBilling.postal_code",array(
                            "label" => __("CAP")." *",
                            "placeholder" => __("Codice postale"),
                            "tabindex" => "16"
                        ));
                        ?>
                    </div>
                </div>
                <div class="row">
                    <div class="col-xs-6">
                        <?php
                        echo $this->Form->input("UserAddressBilling.province",array(
                            "label" => __("Provincia")." *",
                            "placeholder" => __("Provincia"),
                            "tabindex" => "17"
                        ));
                        ?>
                    </div>
                    <div class="col-xs-6">
                        <?php
                        echo $this->Form->input("UserAddressBilling.country_id",array(
                            "label" => __("Paese")." *",
                            "options" => $countries,
                            "tabindex" => "18",
                            "default" => 115
                        ));
                        ?>
                    </div>
                </div>
                <div class="row">
                    <div class="col-xs-12">
                        <?php
                        echo $this->Form->input("UserAddressBilling.phone_number",array(
                            "label" => __("Telefono")." *",
                            "placeholder" => __("Telefono"),
                            "tabindex" => "19"
                        ));
                        ?>
                    </div>
                </div>
            </div>
            <br/>
            <?php
            if(!$logged) {
            ?>
            <h4> Dati di accesso </h4>
            <hr>
            <?php
            echo $this->Form->input("User.email", array(
                "label"         => __("Email") . " *",
                "placeholder"   => __("Email"),
                "tabindex"      => "1",
                "autocomplete"  => "off",
                "required"      => "required"
            ));
            echo $this->Form->input("User.password", array(
                "label"         => __("Password") . " *",
                "placeholder"   => __("Password"),
                "tabindex"      => "2",
                "autocomplete"  => "off"
            ));
            ?>
            <br/>
            <?php
            }
            ?>
            <h4>Metodo di pagamento</h4>
            <hr />
            <ul class="list-group">
                <?php
                $i = 1;
                foreach($payments as $payment) {
                    ?>
                    <li class="list-group-item">
                        <?php
                        echo $this->Form->input("Payment.type",array(
                            "type" => "radio",
                            "label" => false,
                            "options" => array(
                                $payment['id'] => $payment['name'],
                            ),
                            "value" => (isset($cart['payment']['id'])) ? $cart['payment']['id'] : reset($payments)['id']
                        ));
                        if($payment['price']) {
                            ?>
                            <small>Prezzo aggiuntivo <?php echo $this->Number->currency($payment['price'], $cart['shop']['currency']); ?></small>
                        <?php
                        }
                        ?>
                    </li>
                <?php
                    $i++;
                }
                ?>
            </ul>
            <?php
            if($cart['request_availability'] && !$available)
            {
                echo $this->element("checkout/warning_available");
            }
            ?>
            <hr class="cleafix" />
            <a class="btn btn-default btn- pull-left" href="<?php echo "http://".$cart['shop']['domain']."/checkout"; ?>">Torna allo shop</a>
            <button type="submit" class="btn btn-primary pull-right">Calcola spedizioni</button>
            <div class="clearfix"></div>
            <?php
            echo $this->Form->end();
            ?>
        </div>

        <div class="col-sm-5 col-lg-offset-1">
            <div class="panel panel-default">
                <div class="panel-heading">Riepilogo ordine</div>
                <ul class="list-group">
                    <?php
                    if(count($cart['products'])) {
                        foreach($cart['products'] as $product) {
                            ?>
                            <li class="list-group-item">
                                <div class="media">
                                    <div class="media-left">
                                        <img style="max-width: 60px" class="img-rounded" src="<?php echo $product['image']; ?>"/>
                                    </div>
                                    <div class="media-body">
                                        <b><?php echo $product['name']; ?></b>
                                        <p><small><?php echo $product['quantity'] . " <b>x</b> " . $this->Number->currency($product['product_unit']['price_total'], $cart['shop']['currency']); ?></small></p>
                                    </div>
                                    <div class="media-right">
                                        <b><?php echo $this->Number->currency($product['product_total']['price_total'], $cart['shop']['currency']); ?></b>
                                    </div>
                                </div>
                            </li>
                        <?php
                        }
                    }
                    ?>
                </ul>
            </div>
        </div>
    </div>
</div>