<div class="lapagina" data-ng-controller="ShipmentsCtrl">
    <nav class="navbar navbar-default navbar-static-top" role="navigation">
        <div class="navbar-header">
            <ol class="breadcrumb nav navbar-nav">
                <li class="active"><?php echo __("Impostazioni"); ?></li>
                <li class="active"><?php echo __("Spedizioni"); ?></li>
            </ol>
            <ul class="nav navbar-nav navbar-right">
                <div class="navbar-form">
                    <button class="btn btn-success" ng-click="open()"><i class="fa fa-plus"></i> <?php echo __("Aggiungi spedizione"); ?></button>
                </div>
            </ul>
        </div>
    </nav>
    
    
    <div ng-if="!shipping_group_list.length" class="page-err">
    	<div class="alert alert-danger" style="margin-bottom:0" data-ng-if="!settingStatus.shipping">
            <i class="fa fa-exclamation-triangle"></i> <?php echo __("Per poter pubblicare lo shop è necessario impostare correttamente almeno un metodo di spedizione"); ?>
        </div>
        <div class="err-container">
            <div class="text-center">
                <div class="err-status">
                    <h1><i class="fa fa-truck"></i></h1>
                </div>
                <div class="err-message">
                    <h2><?php echo __("Attualmente non sono presenti metodi di spedizione."); ?></h2>
                </div>
                <br />
                <button ng-click="open()" class="btn btn-default btn-lg"><i class="fa fa-plus"></i> <?php echo __("Aggiungi metodo di spedizione"); ?></button>
                <br >
                <br >
                <br >
            </div>
        </div>
    </div>

    <div ng-if="shipping_group_list.length" class="page" >
        <div class="row" >
            <div class="col-lg-10 col-lg-offset-1 col-md-12">
            <section data-ng-repeat="shipping_group in shipping_group_list" class="panel panel-default table-dynamic">
            	<div class="panel-heading">
                    <h4 class="pull-left">{{shipping_group.name}} <small>({{shipping_group.count}} <?php echo __('Prodotti'); ?>)</small></h4>
                    <div class="pull-right">
                        <a class="btn-icon btn-icon-sm bg-success" href="javascript:;" data-ng-click="addShippingToGroup(shipping_group.id)"><i class="fa fa-plus-square-o"></i></a>
                        <a class="btn-icon btn-icon-sm bg-primary" href="javascript:;" data-ng-click="editModal(shipping_group.id)"><i class="fa fa-pencil-square-o"></i></a>
                        <a href="javascript:;" confirm-callback="deleteShippingGroup(shipping_group.id)" confirm-message="<?php echo __("Sei sicuro di voler eleminare il gruppo di spedizione?"); ?>" confirm-modal class="btn-icon btn-icon-sm bg-danger">
                            <i class="fa fa-trash-o"></i>
                        </a>
                    </div>
                    <div class="clearfix"></div>
                </div>
                <div class="panel-body">

                    <div class="table-responsive">
                        <table class="table table-bordered table-striped table-responsive">
                            <thead>
                            <tr>
                                <th>
                                    <div class="th">
                                        <?php echo __("Nome"); ?>
                                    </div>
                                </th>
                                <th>
                                    <div class="th">
                                        <?php echo __("Prezzo"); ?>
                                    </div>
                                </th>
                                <th>
                                    <div class="th">
                                        <?php echo __("Prezzo prodotto aggiuntivo"); ?>
                                    </div>
                                </th>
                                <th>
                                    <div class="th">
                                        <?php echo __("Tempo di consegna"); ?>
                                    </div>
                                </th>
                                <th>
                                    <div class="th">
                                        <?php echo __("Imposta"); ?>
                                    </div>
                                </th>
                                <th>
                                    <div class="th">
                                        <?php echo __("Azioni"); ?>
                                    </div>
                                </th>
                            </tr>
    
                            </thead>
                            <tbody>
                            {{shipping_group_list.shipments}}
                            <tr data-ng-repeat="shipping in shipping_group.shipments">
                                <td>
                                    <p data-ng-show="shipping.country_type == 1">
                                        <span  data-ng-repeat="country in shipping.countries"> {{country}} <span data-ng-if="!$last">,<span> </span>
                                    </p>
                                    <p data-ng-show="shipping.country_type == 2"><?php echo __("Unione Europea"); ?></p>
                                    <p data-ng-show="shipping.country_type == 3"><?php echo __("Tutti i paesi"); ?></p>
                                </td>
                                <td>{{shipping.price}}</td>
                                <td>{{shipping.additional_price}}</td>
                                <td>{{shipping.start_day}}/{{shipping.end_day}} <?php echo __("giorni"); ?></td>
                                <td>{{shipping.tax}}</td>
                                <td class="text-right">
                                    <a class="btn-icon btn-icon-sm bg-primary" href="javascript:;" data-ng-click="editShipping(shipping.id)"><i class="fa fa-pencil-square-o"></i></a>
                                    <a href="javascript:;" confirm-callback="deleteShipping(shipping.id)" confirm-message="<?php echo __("Sei sicuro di voler eleminare la località di spedizione?"); ?>" confirm-modal class="btn-icon btn-icon-sm bg-danger">
                                        <i class="fa fa-trash-o"></i>
                                    </a>
                                </td>
                            </tr>
                            </tbody>
                        </table>
                    </div>
                </div>
            </section>
            </div>
        </div>
    </div>
</div>
