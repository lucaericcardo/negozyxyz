<div class="lapagina" data-ng-controller="ProductsCtrl">
    <nav class="navbar navbar-default navbar-static-top" role="navigation">
        <div class="navbar-header">
            <ul class="nav navbar-nav">
                <li>
                    <ol class="breadcrumb">
                        <li class="active"><?php echo __("I tuoi prodotti"); ?></li>
                    </ol>
                </li>
            </ul>
            <ul class="nav navbar-nav navbar-right">
                <div class="navbar-form">
                    <a href="#/products/add" class="btn btn-success" type="submit"><i class="fa fa-plus"></i> <?php echo __("Aggiungi prodotto"); ?></a>
                </div>
            </ul>
        </div>
    </nav>

    <div ng-if="!products.length" class="page-err">
        <div class="err-container">
            <div class="text-center">
                <div class="err-status">
                    <h1><i class="fa fa-archive"></i></h1>
                </div>
                <div class="err-message">
                    <h2><?php echo __("Attualmente non sono presenti prodotti."); ?></h2>
                </div>
                <br >
                <a href="#/products/add" class="btn btn-default btn-lg"><i class="fa fa-plus"></i> <?php echo __("Aggiungi prodotto"); ?></a>
                <br >
                <br >
                <br >
            </div>
        </div>
    </div>

    <div ng-show="products.length" class="page page-table">
    	<div class="row">
          <div class="col-lg-10 col-lg-offset-1 col-md-12">
                <section class="panel panel-default table-dynamic">
                	<div class="panel-heading">
                    	<div class="pull-left" style="margin-right:20px;">
                    		<h5 data-ng-show="!checkSelected()"><i class="fa fa-level-up fa-rotate-180"></i> Seleziona pi&uacute; prodotti per effettuare operazioni multiple: </h5>
                            <h5 data-ng-hide="!checkSelected()"><i class="fa fa-level-up fa-rotate-180"></i> Se selezionati: </h5>
                        </div>
                        <div class="pull-left" data-ng-hide="!checkSelected()" >
                            <div class="btn-group" dropdown="">
                                <button class="btn btn-info dropdown-toggle" type="button" aria-haspopup="true" aria-expanded="true">
                                    <i class="fa fa-tags"></i> <?php echo __("Aggiungi alla collezione"); ?> <span class="caret"></span>
                                </button>
                                <ul role="menu" class="dropdown-menu">
                                    <li data-ng-repeat="collection in collections"><a data-ng-click="addToColletcion(collection.id)" href="javascript:;">{{collection.name}}</a></li>
                                    <li class="divider"></li>
                                    <li><a data-ng-click="addCollectionModal()" href="javascript:;"><?php echo __("Aggiungi a nuova collezione"); ?></a></li>
                                </ul>
                            </div>
                            <div class="btn-group" dropdown="">
                                <button class="btn btn-info dropdown-toggle" type="button" aria-haspopup="true" aria-expanded="true">
                                    <i class="fa fa-truck"></i> <?php echo __("Aggiungi al gruppo di spedizione"); ?> <span class="caret"></span>
                                </button>
                                <ul role="menu" class="dropdown-menu">
                                    <li data-ng-repeat="shipping_group in shipping_group_list"><a data-ng-click="addToShippingGroup(shipping_group.id)" href="javascript:;">{{shipping_group.name}}</a></li>
                                    <li class="divider"></li>
                                    <li><a data-ng-click="addShippingGroup()" href="javascript:;"><?php echo __("Aggiungi nuovo gruppo di spedizione"); ?></a></li>
                                </ul>
                            </div>
                            <button class="btn btn-danger" confirm-callback="deleteProducts()" confirm-message="<?php echo __("Sei sicuro di voler eliminare i prodotti selezionati ?"); ?>" confirm-modal>
                            	<i class="fa fa-trash-o"></i> <?php echo __("Elimina"); ?>
                            </button>
                        </div>
                        <div class="clearfix"></div>
                    </div>
                    <div class="panel-body">
                        <div class="table-responsive">
                            <table class="table table-bordered table-striped">
                                <thead>
                                <tr>
                                    <th class="text-center">
                                        <div class="th" data-ng-click="toggleList()" style="padding-right:8px;">
                                            <span data-ng-if="deselectAll" class="fa fa-square-o"></span>
                                            <span data-ng-if="selectAll" class="fa fa-check-square-o"></span>
                                        </div>
                                    </th>
                                    <th>
                                        <div class="th">
                                            <?php echo __("Immagine"); ?>
                                        </div>
                                    </th>
                                    <th>
                                        <div class="th">
                                            <?php echo __("Nome"); ?>
                                            <span class="glyphicon glyphicon-chevron-up" data-ng-click=" order('name','ASC') " data-ng-class="{active: row == 'name' && orderType == 'ASC'}"></span>
                                            <span class="glyphicon glyphicon-chevron-down" data-ng-click=" order('name','DESC') " data-ng-class="{active: row == 'name' && orderType == 'DESC'}"></span>
                                        </div>
                                    </th>
                                    <th>
                                        <div class="th">
                                            <?php echo __("Prezzo"); ?>
                                            <span class="glyphicon glyphicon-chevron-up" data-ng-click=" order('price','ASC') " data-ng-class="{active: row == 'price' && orderType == 'ASC'}"></span>
                                            <span class="glyphicon glyphicon-chevron-down" data-ng-click=" order('price','DESC') " data-ng-class="{active: row == 'price' && orderType == 'DESC'}"></span>
                                        </div>
                                    </th>
                                    <th>
                                        <div class="th">
                                            <?php echo __("Ultima Modifica"); ?>
                                            <span class="glyphicon glyphicon-chevron-up" data-ng-click=" order('updated','ASC') " data-ng-class="{active: row == 'updated' && orderType == 'ASC'}"></span>
                                            <span class="glyphicon glyphicon-chevron-down" data-ng-click=" order('updated','DESC') " data-ng-class="{active: row == 'updated' && orderType == 'DESC'}"></span>
                                        </div>
                                    </th>
                                    <th>
                                        <div class="th">
                                            <?php echo __("Quantità"); ?>
                                            <span class="glyphicon glyphicon-chevron-up" data-ng-click=" order('quantity','ASC')" data-ng-class="{active: row == 'quantity' && orderType == 'ASC'}"></span>
                                            <span class="glyphicon glyphicon-chevron-down" data-ng-click=" order('quantity','DESC') " data-ng-class="{active: row == 'quantity' && orderType == 'DESC'}"></span>
                                        </div>
                                    </th>
                                    <th>
                                        <div class="th">
                                            <?php echo __("Collezioni"); ?>
                                        </div>
                                    </th>
                                    <th>
                                        <div class="th">
                                            <?php echo __("Visibile"); ?>
                                            <span class="glyphicon glyphicon-chevron-up" data-ng-click=" order('published','ASC')" data-ng-class="{active: row == 'published' && orderType == 'ASC'}"></span>
                                            <span class="glyphicon glyphicon-chevron-down" data-ng-click=" order('published','DESC') " data-ng-class="{active: row == 'published' && orderType == 'DESC'}"></span>
                                        </div>
                                    </th>
                                    <th class="text-right" width="120">
                                        <div class="th">
                                            <?php echo __("Azioni"); ?>
                                        </div>
                                    </th>
                                </tr>
                                </thead>
                                <tbody>
                                <tr data data-ng-repeat="product in products">
                                    <td class="text-center">
                                        <input type="checkbox" value="{{product.id}}" data-ng-model="product.selected">
                                    </td>
                                    <td>
                                        <img ng-if="product.image" height="100" ng-src="{{product.image}}" style="border: solid 1px #CCC;" />
                                        <img ng-if="!product.image" height="100" ng-src="http://cdn.negozy.com/negozy/Logos/no_logo.png" style="border: solid 1px #CCC;" />
                                    </td>
                                    <td>{{product.name}}</td>
                                    <td>{{product.price}}</td>
                                    <td>{{product.timestamp_updated | date:'dd/MM/yyyy  H:mm'}}</td>
                                    <td>
                                        <span data-ng-if="product.type_quantity == 0">{{product.quantity}}</span>
                                        <span data-ng-if="product.type_quantity == 1"><?php echo __("Illimitati"); ?></span>
                                        <span data-ng-if="product.type_quantity == 2"><?php echo __("Richiedi Disponibilit&agrave;"); ?></span>
                                    </td>
                                    <td>
                                        <span data-ng-if="product.collections.length" data-ng-repeat="collection_name in product.collections">{{collection_name}}</span>
                                        <span data-ng-if="!product.collections.length">-</span>
                                    </td>
                                    <td>
                                        <span data-ng-if="product.published"><?php echo __("Visibile"); ?></span>
                                        <span data-ng-if="!product.published"><?php echo __("Non visibile"); ?></span>
                                    </td>
                                    <td class="text-right">
                                        <a class="btn-icon btn-icon-sm bg-primary" href="#/products/edit/{{product.url}}" > <i class="fa fa-pencil-square-o"></i> <?php //echo __("Modifica"); ?></a>
                                        <a class="btn-icon btn-icon-sm bg-danger" href="javascript:;" confirm-callback="deleteProduct(product.url)" confirm-message="<?php echo __("Sei sicuro di voler eliminare il prodotto ?"); ?>" confirm-modal><i class="fa fa-trash-o"></i> <?php //echo __("Elimina"); ?></a>

                                        <?php
                                        foreach($plugin_elements as $element) {
                                            echo $element;
                                        }
                                        ?>
                                    </td>
                                </tr>
                                </tbody>
                            </table>
                        </div>
                        
                        <footer class="table-footer">
                            <div class="row">
                                <div class="col-md-6 page-num-info">
                                    <span>
                                        <?php echo __("Elementi per pagina"); ?>
                                        <select data-ng-model="numPerPage" data-ng-options="num for num in numPerPageOpt" data-ng-change="onNumPerPageChange()">
                                        </select>
                                    </span>
                                </div>
                                <div class="col-md-6 text-right pagination-container">
                                    <pagination class="pagination-sm" ng-model="currentPage" total-items="totalProducts" max-size="4" ng-change="select(currentPage)" items-per-page="numPerPage" rotate="false" boundary-links="true"></pagination>
                                </div>
                            </div>
                        </footer>
                    </div>
                </section>
        	</div>
        </div>
    </div>
</div>