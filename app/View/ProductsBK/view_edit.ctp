<div class="lapagina">
    <nav class="navbar navbar-default navbar-static-top" role="navigation">
        <div class="navbar-header">
            <ul class="nav navbar-nav">
                <li>
                    <ol class="breadcrumb">
                        <li><a href="#/products/"><?php echo __("I tuoi prodotti"); ?></a></li>
                        <li class="active"><a><?php echo __("Modifica prodotto"); ?></a></li>
                    </ol>
                </li>
            </ul>
        </div>
    </nav>
    <div data-ng-controller="ProductEditCtrl" class="page page-table">
    	<div class="row">
          <div class="col-lg-10 col-lg-offset-1 col-md-12">

            <section class="panel panel-default table-dynamic">
                <div class="panel-body">
                    <form name="product_form" class="form-validation">
                        <div class="row">
                            <div class="col-md-2">
                                <h4><?php echo __("Scheda prodotto"); ?></h4>
                            </div>
                            <div class="col-md-10">
                                <div class="form-group">
                                    <label for=""><?php echo __("Nome"); ?> *</label>
                                    <input type="text" data-ng-change="updateURLfromName()" class="form-control" required data-ng-model="product.name">
                                    <span>{{shop_url}}.negozy.com/product/<a contenteditable data-ng-model="product.url"></a></span>
                                </div>
                                <div class="form-group">
                                    <label for=""><?php echo __("Descrizione"); ?></label>
                                    <div text-angular data-ng-model="product.description"></div>
                                </div>
                            </div>
                        </div>
                        <hr/>
                        
                        <div class="divider"></div>
                        <div class="row">
                            <div class="col-md-2">
                                <h4><?php echo __("Prezzo"); ?></h4>
                            </div>
                            <div class="col-md-10">
                                <div class="row">
                                    <div class="col-md-4">
                                        <label for=""><?php echo __("Prezzo"); ?> *</label>
                                        <input type="number" data-ng-change="changeTotalPrice()" min="0" step="0.01" class="form-control" placeholder="0.00" data-ng-model="product.price" required>
                                    </div>
                                    <div class="col-md-4">
                                        <label for=""><?php echo __("Imposta"); ?></label>
                                        <div class="input-group">
                                            <select data-ng-change="changeTotalPrice()" class="form-control" data-ng-options="tax.id as tax.label for tax in taxes" data-ng-model="product.shop_tax_id"></select>
                                            <span class="input-group-btn">
                                                <button type="button" class="form-control btn btn-primary" ng-click="open()"><i class="fa fa-plus"></i></button>
                                            </span>
                                        </div>
                                    </div>
                                    <div class="col-md-4">
                                        <label for=""><?php echo __("Prezzo finale"); ?></label>
                                        <input type="text" disabled readonly class="form-control" data-ng-model="total_price" />
                                    </div>
                                </div>
                            </div>
                        </div>
                        <hr/>
                        <div class="divider"></div>
                        <div class="row">
                            <div class="col-md-2">
                                <h4><?php echo __("Quantità"); ?></h4>
                            </div>
                            <div class="col-md-10">
                                <div class="row">
                                    <div class="col-sm-4">
                                    	<label for=""><?php echo __("Definita"); ?></label>
                                            <div class="input-group">
                                            <label class="switch switch-info">
                                                <input type="radio" value="0" data-ng-model="product.type_quantity">
                                                <i></i>
                                            </label>
                                        </div>
                                        <div class="row">
                                        	<div class="col-sm-6">
                                        		<input ng-disabled="product.type_quantity != 0" type="number" class="form-control" min="0" placeholder="1" data-ng-model="product.quantity">
                                        	</div>
                                        </div>
                                    </div>
                                    <div class="col-sm-4">
                                    	<div class="input-group">
                                            <label for=""><?php echo __("Illimitata"); ?></label>
                                            <div class="clearfix"></div>
                                            <label class="switch switch-info">
                                                <input type="radio" value="1" data-ng-model="product.type_quantity" >
                                                <i></i>
                                            </label>
                                        </div>
                                    </div>
                                    <div class="col-sm-4">
                                        <div class="input-group">
                                            <label for=""><?php echo __("Richiedi Disponibilit&agrave;"); ?></label>
                                            <div class="clearfix"></div>
                                            <label class="switch switch-info">
                                                <input type="radio" value="2" data-ng-model="product.type_quantity">
                                                <i></i>
                                            </label>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <hr/>
                        
                        <div class="divider"></div>
                        <div class="row">
                            <div class="col-md-2">
                                <h4><?php echo __("Immagini"); ?></h4>
                            </div>
                            <div class="col-md-10">
                                <input class="btn-primary"  ng-disabled="onLoading" accept="image/*" id="product_image" data-ng-model="images" title="<?php echo __("Seleziona immagini") ?>" data-ui-file-upload type="file" nv-file-select uploader="uploader" multiple>
                                <div class="divider"></div>
                                <div ng-if="onLoading">
                                    <p class="text-muted"><i class="fa fa-spinner fa-spin"></i> Caricamento {{current_image}} in corso.</p>
                                    <progress  class="progress-striped active">
                                        <bar value="progress" type="success"><span>{{progress}}</span></bar>
                                    </progress>
                                </div>
                                <ul class="list-unstyled" id="uploadImg" ui-sortable="sortableOptions" ng-model="images">
                                    <li data-ng-repeat="image in images">
                                        <div><img ng-src="{{image.thumb}}" height="100" /></div>
                                        <div>
                                            <button ng-click="deleteImage(image.name)" class="btn btn-danger btn-xs"><i class="fa fa-trash-o"></i></button>
                                        </div>
                                    </li>
                                </ul>
                            </div>
                        </div>
                        <hr/>
                        
                        <div class="divider"></div>
                        <?php /*
                        <div class="row">
                            <div class="col-md-2">
                                <h4><?php echo __("Categoria"); ?></h4>
                            </div>
                            <div class="col-md-10" categories-step="selectCategory">
                                <div ng-show="errorCategories" class="callout callout-danger">
                                    <?php echo __("Seleziona l'alberatura delle categorie."); ?>
                                </div>
                                <?php echo $this->element('shop/step_categories'); ?>
                            </div>
                        </div>
                        */ ?>
                        <div class="row">
                            <div class="col-md-2">
                                <h4><?php echo __("Collezione"); ?></h4>
                            </div>
                            <div class="col-md-4">
                                <div class="input-group">
                                    <select class="form-control" data-ng-options="collection.id as collection.name for collection in collections" data-ng-model="product.collection_id"></select>
                                    <span class="input-group-btn">
                                        <button type="button" class="form-control btn btn-primary" ng-click="addCollectionModal()"><i class="fa fa-plus"></i></button>
                                    </span>
                                </div>
                            </div>
                        </div>
                        <hr/>
                        <div class="divider"></div>
                        <div class="row">
                            <div class="col-md-2">
                                <h4><?php echo __("Spedizione"); ?></h4>
                            </div>
                            <div class="col-md-4">
                                <div class="input-group">
                                    <select class="form-control" data-ng-options="shipping.id as shipping.name for shipping in shipments" data-ng-model="product.shipping_group_id"></select>
                                    <span class="input-group-btn">
                                        <button type="button" class="form-control btn btn-primary" ng-click="addShippingModal()"><i class="fa fa-plus"></i></button>
                                    </span>
                                </div>
                            </div>
                        </div>
                        <div class="divider"></div>
                        <hr/>
        
                        <div class="row">
                            <div class="col-md-2">
                                <h4><?php echo __("Tags"); ?></h4>
                            </div>
                            <div class="col-md-10">
                                <tags-input ng-model="tags" class="ui-tags-input">
                                    <auto-complete source="loadTags($query)"></auto-complete>
                                </tags-input>
                            </div>
                        </div>
        
                        <div class="divider"></div>
                        <hr/>

                        <div class="row">
                            <div class="col-md-2">
                                <h4><?php echo __("Visibile"); ?></h4>
                            </div>
                            <div class="col-md-10">
                                <div class="row">
                                    <div class="col-sm-4">
                                        <div class="input-group">
                                            <label class="switch switch-info">
                                                <input type="checkbox" value="1" data-ng-model="product.published" >
                                                <i></i>
                                            </label>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>

                        <div class="divider"></div>
                        <hr/>

                        <div class="row">
                            <div class="col-md-10 col-md-offset-2">
                                <button ng-disabled="product_form.$invalid" data-ng-click="saveProduct()" class="btn btn-success btn-lg" type="button"><?php echo __("Salva"); ?></button>
                            </div>
                        </div>
                    </form>
                </div>
            </section>
          </div>
        </div>          
    </div>
</div>