<div class="lapagina" data-ng-controller="PagesCtrl">
    <nav class="navbar navbar-default navbar-static-top" role="navigation">
        <div class="navbar-header">
            <ul class="nav navbar-nav">
                <li>
                    <ol class="breadcrumb">
                        <li class="active"><?php echo __("Le tue pagine"); ?></li>
                    </ol>
                </li>
            </ul>
            <ul class="nav navbar-nav navbar-right">
                <div class="navbar-form">
                    <a href="#/pages/add/{{selected_language}}" class="btn btn-success" type="submit"><i class="fa fa-plus"></i> <?php echo __("Aggiungi pagina"); ?></a>
                </div>
            </ul>
        </div>
    </nav>

    <div ng-if="!pages.length" class="page-err">
        <div class="err-container">
            <div class="text-center">
                <div class="err-status">
                    <h1><i class="fa fa-file-text"></i></h1>
                </div>
                <div class="err-message">
                    <h2><?php echo __("Attualmente non sono presenti pagine."); ?></h2>
                </div>
                <br >
                <a href="#/pages/add/{{selected_language}}" class="btn btn-default btn-lg"><i class="fa fa-plus"></i> <?php echo __("Aggiungi pagina"); ?></a>
                <br >
                <br >
                <br >
            </div>
        </div>
    </div>

    <div ng-show="pages.length" class="page page-table">
    	<div class="row">
          <div class="col-md-12">

              <!-- new code -->
              <section class="panel panel-default">
                  <div class="panel-heading">
                      <div class="pull-left">
                          <div data-ng-hide="!checkSelected()">
                              <strong class="hidden-xs"><span class="fa fa-level-up fa-rotate-180"></span> Se selezionati:</strong>
                              <div is-open="status.isopen_category" dropdown="" class="btn-group">
                                  <button class="btn btn-info dropdown-toggle btn-xs" type="button" aria-haspopup="true" aria-expanded="false"> Organizza in categorie <span class="caret"></span> </button>
                                  <ul role="menu" class="dropdown-menu">
                                      <li data-ng-repeat="category in categories">
                                          <a ng-click="addPageToCategory(category.id)" href="javascript:;">{{category.name}}</a>
                                      </li>
                                  </ul>
                              </div>
                              <button confirm-callback="deleteAll()" confirm-message="<?php echo __("Sei sicuro di voler eliminare le pagine selezionate?"); ?>" confirm-modal class="btn btn-danger btn-xs" type="button">Elimina</button>
                          </div>
                          <div data-ng-hide="checkSelected()">
                              <strong class="hidden-xs"><span class="fa fa-level-up fa-rotate-180"></span> Seleziona pi&uacute; pagine per effettuare operazioni multiple:</strong>
                          </div>
                      </div>
                      <div is-open="status.isopen2" dropdown="" class="btn-group pull-right">
                          <button class="btn btn-primary dropdown-toggle btn-xs" type="button" aria-haspopup="true" aria-expanded="false"> Visualizza Lingua <span class="caret"></span> </button>
                          <ul role="menu" class="dropdown-menu">
                              <li data-ng-class="{'active':(lang_id == selected_language)}" data-ng-repeat="(id, lang_id) in objectKeys(shop_languages)" data-ng-click="selectLanguage(lang_id)"><a href="javascript:;">{{shop_languages[lang_id]}}</a></li>
                          </ul>
                      </div>
                      <div class="clearfix"></div>
                  </div>
                  <div class="panel-body">

                      <div class="table-responsive">
                          <table class="table table-striped">
                              <thead>
                              <tr>
                                  <th style="width: 68px;">
                                      <label class="ui-checkbox">
                                          <input type="checkbox" data-ng-model="obj.selectAll" data-ng-click="toggleList()">
                                          <span style="color:#FFF"></span>
                                      </label>
                                  </th>
                                  <th>
                                      Titolo Pagina
                                  </th>
                                  <th>Categoria</th>
                                  <th>Ordine</th>
                                  <th>Visibile</th>
                                  <th data-ng-repeat="(id, lang_id) in objectKeys(shop_languages)" style="width: 100px;">{{shop_languages[lang_id]}}</th>
                                  <th class="text-right">Azioni</th>
                              </tr>
                              </thead>
                              <tbody>
                                  <tr data-ng-repeat="page in pages">
                                      <td>
                                          <label class="ui-checkbox">
                                              <input type="checkbox" value="{{page.page_id}}" data-ng-model="page.selected" data-ng-change="updateSelected()">
                                              <span style="color:#FFF"></span>
                                          </label>
                                      </td>
                                      <td>
                                          <a href="#/pages/edit/{{selected_language}}/{{page.page_id}}" class="text-info"  ng-if="page.content_id"><b>{{page.title}}</b></a>
                                          <a href="#/pages/edit/{{selected_language}}/{{page.page_id}}" class="text-primary"  ng-if="!page.content_id"><i class="primary">{{page.title}}</i></a>
                                      </td>
                                      <td>{{page.category_page.title}}</td>
                                      <td>{{page.order}}</td>
                                      <td>
                                          <span ng-if="!page.published" class="label label-danger"><i class="fa fa-times"></i></span>
                                          <span ng-if="page.published" class="label label-success"><i class="fa fa-check"></i></span>
                                      </td>
                                      <td data-ng-repeat="page_cont in page.contents">
                                          <a href="#/pages/edit/{{page_cont.lang_id}}/{{page.page_id}}" ng-if="page_cont.content_id" class="btn btn-success btn-xs btn-block" type="button">
                                              <i class="glyphicon glyphicon-pencil"></i>&nbsp;Modifica
                                          </a>
                                          <a
                                              href="#/pages/edit/{{page_cont.lang_id}}/{{page.page_id}}"
                                              ng-if="!page_cont.content_id"
                                              tooltip-append-to-body="true"
                                              tooltip="La pagina non esiste ancora, clicca qui per crearla"
                                              tooltip-placement="top"
                                              class="btn btn-primary btn-xs btn-block"
                                              type="button"><i class=" glyphicon glyphicon-pencil"></i>&nbsp;Crea
                                          </a>
                                      </td>
                                      <td class="text-right"><button ng-if="!page.default" class="btn btn-danger btn-xs" type="button" confirm-callback="deletePage(page.page_id)" confirm-message="<?php echo __("Sei sicuro di voler eliminare questa pagina?"); ?>" confirm-modal><i class=" glyphicon glyphicon-trash"></i></button> </td>
                                  </tr>
                              </tbody>
                          </table>
                      </div>

                      <footer class="table-footer">
                          <div class="row">
                              <div class="col-md-6 page-num-info">
                                    <span>
                                        <?php echo __("Elementi per pagina"); ?>
                                        <select data-ng-model="numPerPage" data-ng-options="num for num in numPerPageOpt" data-ng-change="onNumPerPageChange()">
                                        </select>
                                    </span>
                              </div>
                              <div class="col-md-6 text-right pagination-container">
                                  <pagination class="pagination-sm" ng-model="currentPage" total-items="totalPages" max-size="4" ng-change="select(currentPage)" items-per-page="numPerPage" rotate="false" boundary-links="true"></pagination>
                              </div>
                          </div>
                      </footer>

                  </div>
              </section>
              <!-- new code -->
        	</div>
        </div>
    </div>
</div>