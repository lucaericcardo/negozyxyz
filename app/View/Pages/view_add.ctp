<div class="lapagina add-page" >
    <nav class="navbar navbar-default navbar-static-top" role="navigation">
        <div class="navbar-header">
            <ul class="nav navbar-nav">
                <li>
                    <ol class="breadcrumb">
                        <li><a href="#/pages/"><?php echo __("Le tue pagine"); ?></a></li>
                        <li class="active"><a><?php echo __("Aggiungi pagina"); ?></a></li>
                    </ol>
                </li>
            </ul>
            <ul class="nav navbar-nav navbar-right">
                <div class="navbar-form">
                    <img src="/asset/img/easyadmin.png" style="height:30px"/>
                </div>
            </ul>
        </div>
    </nav>
    <div class="page page-table" data-ng-controller="PageAddCtrl">
        <div class="row">
            <div class="col-sm-12">
                <div class="panel-body">
                    <div ng-show="successSave" class="callout callout-success">
                        <p><?php echo __("Pagina aggiunta con successo"); ?></p>
                        <a href="#/pages"><?php echo __("Torna alla lista delle pagine"); ?></a>
                    </div>
                    <div ng-if="onLoading">
                        <p class="text-muted"><i class="fa fa-spinner fa-spin"></i> Caricamento {{current_image}} in corso.</p>
                        <progress class="progress-striped active">
                            <bar value="progress" type="success"><span>{{count_image}} / {{uploader.queue.length}}</span></bar>
                        </progress>
                    </div>
                    <div ng-if="onLoading || successSave">
                        <ul class="list-unstyled">
                            <li data-ng-repeat="success_image in success_images" class="text-success"><i class="fa fa-check"></i> {{success_image}} <b><?php echo __("aggiunto con successo"); ?></b></li>
                        </ul>
                        <ul class="list-unstyled">
                            <li data-ng-repeat="error_image in error_images" class="text-danger"><i class="fa fa-times"></i> {{error_image}} <b><?php echo __("errore nel caricamento"); ?></b></li>
                        </ul>
                    </div>
                    <div ng-show="errorSave" class="callout callout-danger">
                        <?php echo __("Si è verificato un problema durante l'aggiunta della pagina."); ?>
                    </div>
                </div>
            </div>
        </div>
    	<div class="row" ng-if="!onLoading && !successSave">
            <div class="col-sm-12 col-md-8 col-lg-9">
              <section class="panel panel-default">
                  <div class="panel-heading"><strong><span class="flag-icon flag-icon-it"></span>&nbsp;<?php echo __("Titolo"); ?> </strong>
                  </div>
                  <div class="panel-body">
                      <input type="text" data-ng-model="page.title" class="form-control" data-ng-change="updateURLfromNamePage()">
                      <span>uri: <a contenteditable data-ng-model="page.uri"></a></span>
                  </div>
              </section>

              <section class="panel panel-default">
                  <div class="panel-heading"><strong><span class="flag-icon flag-icon-it"></span>&nbsp;<?php echo __("Contenuto"); ?></strong></div>
                  <div class="panel-body">
                      <div text-angular data-ng-model="page.html"></div>
                  </div>
              </section>

              <div class="row">
                  <div class="col-sm-12">
                      <section class="panel panel-default table-dynamic">
                          <div class="panel-heading"><strong> <?php echo __("Categoria"); ?></strong></div>
                          <div class="panel-body">
                              <div class="row">
                                  <div class="col-xs-6">
                                      <div class="input-group">
                                          <select class="form-control" data-ng-options="category.id as category.name for category in categories_page" data-ng-model="page.category_page_id">
                                              <option value=""><?php echo __("Seleziona una categoria"); ?></option>
                                          </select>
                                            <span class="input-group-btn">
                                                <button type="button" class="form-control btn btn-primary" ng-click="addPageCategoryModal()"><i class="fa fa-plus"></i></button>
                                            </span>
                                      </div>
                                  </div>
                              </div>
                          </div>
                      </section>
                  </div>
              </div>

              <section class="panel panel-default">
                  <div class="panel-body text-right">
                      <button class="btn btn-success btn-lg" ng-click="savePage();" ng-disabled="!page.title" type="button"><?php echo __("Salva"); ?></button>
                  </div>
              </section>
          </div>
            <div class="col-sm-12 col-md-4 col-lg-3 ">
                <section class="panel panel-default">
                    <div class="panel-heading"><strong><i class="fa fa-sliders"></i> <?php echo __("Impostazioni"); ?></strong></div>
                    <div class="panel-body">
                        <button class="btn btn-info btn-block" ng-click="openSeo()" type="button"><i class="fa fa-search"></i> <?php echo __("Motori di ricerca"); ?></button>
                        <hr>
                        <label>Ordine</label>
                        <input type="text" data-ng-model="page.order" class="form-control">
                        <hr>
                        <div class="row">
                            <div class="col-sm-12">
                                <div class="input-group">
                                    <label><?php echo __("Visibile"); ?></label>
                                    <div class="clearfix"></div>
                                    <label class="switch switch-info">
                                        <input type="checkbox" value="1" data-ng-model="page.published" >
                                        <i></i>
                                    </label>
                                </div>
                            </div>
                        </div>
                    </div>
                </section>

                <section class="panel panel-default table-dynamic" >
                    <div class="panel-heading"><strong><i class="glyphicon glyphicon-picture"></i> <?php echo __("Immagini") ?></strong></div>
                    <div class="panel-body">
                        <input accept="image/*" title="<?php echo __("Seleziona immagini") ?>" data-ui-file-upload type="file" nv-file-select uploader="uploader" multiple class="btn-primary btn-block">
                        <div class="container-immagini">
                            <ul class="list-unstyled" id="uploadImg" ui-sortable="sortableOptions" ng-model="uploader.queue">
                                <li ng-repeat="item in uploader.queue">
                                    <div ng-thumb="{ file: item._file, height: 100 }"></div>
                                    <div>
                                        <button ng-click="item.remove()" class="btn btn-danger btn-xs"><i class="fa fa-trash-o"></i></button>
                                    </div>
                                </li>
                            </ul>
                        </div>
                    </div>
                </section>

            </div>
        </div>
    </div>
</div>