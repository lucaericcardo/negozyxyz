<div class="modal-header">
    <h3 class="modal-title"><?php echo __("Aggiungi contenuti seo"); ?></h3>
</div>
<div class="modal-body">
    <blockquote>
        <small>Il meta tag KEYWORDS ha lo scopo di indicare ai motori di ricerca una lista di parole chiave inerenti ai contenuti della pagina web in cui appare il tag-keywords.</small>
        <small>Per l'inserimento è sufficiente scrivere il tag e premere invio.</small>
    </blockquote>
    <tags-input placeholder="<?php echo __('Aggiungi KEYWORDS'); ?>" data-ng-model="entity.meta_keywords" class="ui-tags-input">
    </tags-input>
    <hr>
    <blockquote>
        <small>Il meta tag DESCRIPTION è un breve riassunto dei contenuti che sono presentati nella pagina.</small>
    </blockquote>
    <textarea name="" id="" class="form-control" rows="4" placeholder="Aggiungi DESCRIPTION" data-ng-model="entity.meta_description"></textarea>
</div>
<div class="modal-footer">
    <button class="btn btn-primary" ng-click="ok()"><?php echo __("Salva"); ?></button>
    <button class="btn btn-warning" ng-click="cancel()"><?php echo __("Annulla"); ?></button>
</div>