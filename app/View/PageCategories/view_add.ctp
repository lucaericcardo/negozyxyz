<div class="modal-header">
    <h3 class="modal-title"><?php echo __("Aggiungi nuova categoria"); ?></h3>
</div>
<div class="modal-body">
    <form name="addCategory" class="form-horizontal form-validation">
        <div class="form-group">
            <label for="" class="col-sm-4"><?php echo __("Nome"); ?></label>
            <div class="col-sm-8">
                <input type="text" class="form-control" required data-ng-model="category_page.title" data-ng-change="updateURLfromNameCatPag()">
                <span>uri: <a contenteditable data-ng-model="category_page.uri"></a></span>
            </div>
        </div>
    </form>
</div>
<div class="modal-footer">
    <button class="btn btn-primary" ng-disabled="addCategory.$invalid" ng-click="save()"><?php echo __("Salva"); ?></button>
    <button class="btn btn-warning" ng-click="cancel()"><?php echo __("Annulla"); ?></button>
</div>