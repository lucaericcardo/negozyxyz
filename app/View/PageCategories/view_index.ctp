<div class="lapagina" data-ng-controller="PageCategoriesCtrl">
    <nav class="navbar navbar-default navbar-static-top" role="navigation">
        <div class="navbar-header">
            <ul class="nav navbar-nav">
                <li>
                    <ol class="breadcrumb">
                        <li ng-repeat="bread in breadcrumb" ng-class="{'active':$last}" ><a href="{{bread.url}}">{{bread.title}}</a></li>
                    </ol>
                </li>
            </ul>
            <ul class="nav navbar-nav navbar-right">
                <div class="navbar-form">
                    <a href="javascript:void(0)" ng-click="addCategory()" class="btn btn-success" type="submit"><i class="fa fa-plus"></i> <?php echo __("Aggiungi categoria"); ?></a>
                </div>
            </ul>
        </div>
    </nav>

    <div ng-if="!page_categories.length" class="page-err">
        <div class="err-container">
            <div class="text-center">
                <div class="err-status">
                    <h1><i class="fa fa-file-text"></i></h1>
                </div>
                <div class="err-message">
                    <h2><?php echo __("Attualmente non sono presenti categorie."); ?></h2>
                </div>
                <br >
                <a href="javascript:void(0)" ng-click="addCategory()" class="btn btn-default btn-lg"><i class="fa fa-plus"></i> <?php echo __("Aggiungi categoria"); ?></a>
                <br >
                <br >
                <br >
            </div>
        </div>
    </div>

    <div ng-show="page_categories.length" class="page page-table">
        <div class="row">
            <div class="col-md-12">
                <!-- new code -->
                <section class="panel panel-default">
                    <div class="panel-heading">
                        <div class="pull-left">
                            <div data-ng-hide="!checkSelected()">
                                <strong class="hidden-xs"><span class="fa fa-level-up fa-rotate-180"></span> Se selezionati:</strong>
                                <button confirm-callback="deleteAll()" confirm-message="<?php echo __("Sei sicuro di voler eliminare le categorie selezionate?"); ?>" confirm-modal class="btn btn-danger btn-xs" type="button">Elimina</button>
                            </div>
                            <div data-ng-hide="checkSelected()">
                                <strong class="hidden-xs"><span class="fa fa-level-up fa-rotate-180"></span> Seleziona pi&uacute; prodotti per effettuare operazioni multiple:</strong>
                            </div>
                        </div>
                        <div is-open="status.isopen2" dropdown="" class="btn-group pull-right">
                            <button class="btn btn-primary dropdown-toggle btn-xs" type="button" aria-haspopup="true" aria-expanded="false"> Visualizza Lingua <span class="caret"></span> </button>
                            <ul role="menu" class="dropdown-menu">
                                <li data-ng-class="{'active':(lang_id == selected_language)}" data-ng-repeat="(id, lang_id) in objectKeys(shop_languages)" data-ng-click="selectLanguage(lang_id)"><a href="javascript:;">{{shop_languages[lang_id]}}</a></li>
                            </ul>
                        </div>
                        <div class="clearfix"></div>
                    </div>
                    <div class="panel-body">
                        <div class="table-responsive">
                            <table class="table table-striped">
                                <thead>
                                <tr>
                                    <th style="width: 68px;">
                                        <label class="ui-checkbox">
                                            <input type="checkbox" data-ng-model="obj.selectAll" data-ng-click="toggleList()">
                                            <span style="color:#FFF"></span>
                                        </label>
                                    </th>
                                    <th><?php echo __("Categoria"); ?></th>
                                    <th><?php echo __("Data Creazione"); ?></th>
                                    <th data-ng-repeat="(id, lang_id) in objectKeys(shop_languages)" style="width: 100px;">{{shop_languages[lang_id]}}</th>
                                    <th class="text-right"><?php echo __("Azioni"); ?></th>
                                </tr>
                                </thead>
                                <tbody>
                                <tr data-ng-repeat="page_category in page_categories">
                                    <td>
                                        <label class="ui-checkbox">
                                            <input type="checkbox" value="{{page_category.category_id}}" data-ng-model="page_category.selected" data-ng-change="updateSelected()">
                                            <span style="color:#FFF"></span>
                                        </label>
                                    </td>
                                    <td>
                                        <a href="javascript:void(0)" ng-click="editCategory(selected_language,page_category.category_id)" class="text-info"  ng-if="page_category.content_id"><b>{{page_category.title}}</b></a>
                                        <a href="javascript:void(0)" ng-click="editCategory(selected_language,page_category.category_id)" class="text-primary"  ng-if="!page_category.content_id"><i class="primary">{{page_category.title}}</i></a>
                                    </td>
                                    <td>{{page_category.created | date:'dd/MM/yyyy  H:mm'}}</td>
                                    <td data-ng-repeat="content in page_category.contents">
                                        <a href="javascript:void(0)" ng-click="editCategory(content.lang_id,page_category.category_id)" ng-if="content.content_id" class="btn btn-success btn-xs btn-block" type="button">
                                            <i class="glyphicon glyphicon-pencil"></i>&nbsp;Modifica
                                        </a>
                                        <a
                                            href="javascript:void(0)"
                                            ng-click="editCategory(content.lang_id,page_category.category_id)"
                                            ng-if="!content.content_id"
                                            tooltip-append-to-body="true"
                                            tooltip="Il contenuto in questa lingua non esiste, clicca qui per inserirlo"
                                            tooltip-placement="top"
                                            class="btn btn-primary btn-xs btn-block"
                                            type="button"><i class=" glyphicon glyphicon-pencil"></i>&nbsp;Crea
                                        </a>
                                    </td>
                                    <td class="text-right">
                                          <span ng-if="!page_category.default"
                                              tooltip-append-to-body="true"
                                              tooltip="<?php echo __("Elimina categoria"); ?>"
                                              tooltip-placement="top">
                                            <button class="btn btn-danger btn-xs" type="button" confirm-callback="delete(page_category.category_id)" confirm-message="<?php echo __("Sei sicuro di voler eliminare questa categoria ?"); ?>" confirm-modal><i class=" glyphicon glyphicon-trash"></i></button>
                                          </span>
                                    </td>
                                </tr>
                                </tbody>
                            </table>
                        </div>


                        <footer class="table-footer">
                            <div class="row">
                                <div class="col-md-6 page-num-info">
                                    <span>
                                        <?php echo __("Elementi per pagina"); ?>
                                        <select data-ng-model="numPerPage" data-ng-options="num for num in numPerPageOpt" data-ng-change="onNumPerPageChange()">
                                        </select>
                                    </span>
                                </div>
                                <div class="col-md-6 text-right pagination-container">
                                    <pagination class="pagination-sm" ng-model="currentPage" total-items="totalPageCategories" max-size="4" ng-change="select(currentPage)" items-per-page="numPerPage" rotate="false" boundary-links="true"></pagination>
                                </div>
                            </div>
                        </footer>

                    </div>
                </section>
                <!-- new code -->
            </div>
        </div>
    </div>
</div>