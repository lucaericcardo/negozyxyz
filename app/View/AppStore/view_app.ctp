<div class="lapagina">
	<nav class="navbar navbar-default navbar-static-top" role="navigation">
    	<div class="navbar-header">
    		<div class="navbar-center">	
                <img class="navbar-brand" alt="" data-ng-src="{{pageData.appData.AppCompanyDeveloper.AppCompanyIcon.url}}">
            </div>
            <ul class="nav navbar-nav">
				<li>
                <ol class="breadcrumb">
                    <li><a href="javascript:;"><?php echo __("App Store"); ?></a></li> 
                    <li class="active">{{pageData.appData.Application.name}}</li>
                </ol>
                </li>
           	</ul>
        </div>
	</nav>
    <div id="apps-app">
        <div class="page page-services">
            <div class="row">
                <div class="col-xs-12 col-sm-12 col-md-6">
                	<div class="media">
                        <a href="javascript:;" class="pull-left">
                            <img class="img-rounded img128_128" data-ng-src="{{pageData.appData.AppIcon.url}}" alt="">
                        </a>
                        <div class="media-body">
                        	<div class="pull-left">
                                <h3 class="media-heading">{{pageData.appData.Application.name}}</h3>
                                <p>{{pageData.appData.AppCompanyDeveloper.name}}</p>
                            </div>

                            <div data-ng-show="loadingButton">
                                <div ng-activity-indicator="DottedDark" skip-ng-show="yes" style="margin-left: 85%"></div>
                            </div>

                            <div data-ng-hide="loadingButton">
                                <!-- bottone per l'installazione gratis -->
                                <button class="btn btn-success btn-w-xs pull-right" type="button" data-ng-hide="installed" data-ng-click="installApp(pageData.appData.Application.id)"><?php echo __("INSTALLA"); ?></button>

                                <!-- bottone per disintallare-->
                                <button class="btn btn-danger btn-w-xs pull-right" type="button" data-ng-show="installed" data-ng-click="removeApp(pageData.appData.Application.id)"><?php echo __("DISINSTALLA"); ?></button>
                            </div>
                            <!-- bottone per l'app a pagamento -->
                            <!--<button class="btn btn-success btn-w-xs pull-right" type="button">0,89 €</button>-->

                            <div class="clearfix"></div>
                            <hr />
                            <h4 data-ng-show="pageData.appData.Application.description">Descrizione</h4>
                            {{pageData.appData.Application.description}}
                        </div>
                    </div>
                </div>
                <div class="col-xs-12 col-sm-12 col-md-6" data-ng-show="slides.length">
                    <div class="slider">
                        <img ng-repeat="slide in slides" class="slide slide-animation nonDraggableImage"
                             ng-swipe-right="nextSlide()" ng-swipe-left="prevSlide()"
                             ng-hide="!isCurrentSlideIndex($index)" ng-src="{{slide.url}}">
                    
                        <a class="arrow prev" href="javascript:;" ng-click="nextSlide()"></a>
                        <a class="arrow next" href="javascript:;" ng-click="prevSlide()"></a>
                        <nav class="nav">
                            <div class="wrapper">
                                <ul class="dots">
                                    <li class="dot" ng-repeat="slide in slides">
                                        <a href="javascript:;" ng-class="{'active':isCurrentSlideIndex($index)}"
                                           ng-click="setCurrentSlideIndex($index);">{{slide.title}}</a></li>
                                </ul>
                            </div>
                        </nav>
                    </div>
                    
                </div>
            </div>		
        </div>
    </div>
    <div id="apps-cat">
        <div class="page page-services">
            <div class="nome-categoria clearfix" data-ng-show="pageData.otherCompanyApp.Application.length">
                    <h2 class="pull-left"><?php echo __("Altri di"); ?> {{pageData.otherCompanyApp.AppCompanyDeveloper.name}}</h2>
                    <button class="btn btn-w-md btn-gap-v btn-warning pull-right" data-ng-click="viewOther(pageData.otherCompanyApp, company)" type="button"><?php echo __('Vedi altro'); ?>...</button>
                    <button class="btn btn-primary pull-right" data-ng-show="retry(company)" data-ng-click="viewRetry(company)" type="button"> <i class="fa fa fa-long-arrow-left"></i> </button>
                <div class="clearfix"></div>
                <div class="contCard">

                    <!-- card -->
                    <div
                        class="panel panel-box card"
                        data-ng-repeat="app in pageData.otherCompanyApp.Application | ofsetLimitTo:company.ofset:company.limit"
                        >
                        <div class="panel-top bg-info">
                            <a href="#/app-store/{{app.request_uri}}"><img data-ng-src="{{app.AppIcon.url || 'http://cdn.negozy.com/negozy/Img/default.jpg'}}" /></a>
                        </div>
                        <div class="panel-bottom bg-reverse">
                            <p class="size-h4"><a href="#/app-store/{{app.request_uri}}">{{app.name}}</a></p>
                            <p class="text-muted"><a href="#/app-store/{{app.request_uri}}">{{pageData.otherCompanyApp.AppCompanyDeveloper.name}}</a></p>

                        </div>
                        <!--
                        <div class="panel-bottom">
                            <p class="text-muted text-left"><a class="text-info" href="#/app-store/{{app.request_uri}}">€ 2,50</a></p>
                        </div>
                        -->
                    </div>

                </div>
            </div>
            <div class="nome-categoria" data-ng-show="pageData.otherCategoryApp.Application.length">
                <div class="clearfix"></div>
                    <h2 class="pull-left"><?php echo __("Applicazioni simili"); ?></h2>
                    <button class="btn btn-w-md btn-gap-v btn-warning pull-right" data-ng-click="viewOther(pageData.otherCategoryApp, category)" type="button"><?php echo __('Vedi altro'); ?>...</button>
                    <button class="btn btn-primary pull-right" data-ng-show="retry(category)" data-ng-click="viewRetry(category)" type="button"> <i class="fa fa fa-long-arrow-left"></i> </button>
                <div class="clearfix"></div>
                
                <!-- card -->
                <div
                    class="panel panel-box card"
                    data-ng-repeat="app in pageData.otherCategoryApp.Application | ofsetLimitTo:category.ofset:category.limit"
                    >
                    <div class="panel-top bg-info">
                        <a href="#/app-store/{{app.request_uri}}"><img data-ng-src="{{app.AppIcon.url || 'http://cdn.negozy.com/negozy/Img/default.jpg'}}" /></a>
                    </div>
                    <div class="panel-bottom bg-reverse">
                        <p class="size-h4"><a href="#/app-store/{{app.request_uri}}">{{app.name}}</a></p>
                        <p class="text-muted"><a href="#/app-store/{{app.request_uri}}">{{app.AppCompanyDeveloper.name}}</a></p>
                        
                    </div>
                    <!--
                    <div class="panel-bottom">
                    <p class="text-muted text-left"><a class="text-info" href="#/app-store/{{app.request_uri}}">€ 2,50</a></p>
                    </div>
                    -->
                </div>

            </div>
        </div>
    </div>
</div>