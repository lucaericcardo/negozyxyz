<div class="lapagina" data-ng-controller="AppStoreCtrl">
	<nav class="navbar navbar-default navbar-static-top" role="navigation">
    	<div class="navbar-header">
            <ul class="nav navbar-nav">
				<li>
                <ol class="breadcrumb">
                    <!-- <li><a href="javascript:;">Prodotti</a></li>  -->
                    <li class="active"><?php echo __("App Store"); ?></li>
                </ol>
                </li>
           	</ul>
        </div>
	</nav>
    <div id="apps-cat">
        <div class="page page-services">
            <div class="nome-categoria clearfix" data-ng-repeat="cate in catsapps">
                    <h2 class="pull-left">{{cate.AppCategory.name}}</h2>
                    <button class="btn btn-w-md btn-gap-v btn-warning pull-right" type="button" data-ng-click="viewOther(cate)"><?php echo __('Vedi altro'); ?>...</button>
                    <button class="btn btn-primary pull-right" data-ng-show="retry(cate)" data-ng-click="viewRetry(cate)" type="button"> <i class="fa fa fa-long-arrow-left"></i> </button>
                <div class="clearfix"></div>
                <div class="contCard">
                    <div class="panel panel-box card" data-ng-repeat="apps in cate.Application | ofsetLimitTo:cate.ofset:cate.limit">
                        <div class="panel-top bg-info">
                            <a href="#/app-store/{{apps.request_uri}}"><img data-ng-src="{{apps.AppIcon.url || 'http://cdn.negozy.com/negozy/Img/default.jpg'}}" style="width:140px" /></a>
                        </div>
                        <div class="panel-bottom bg-reverse">
                            <p class="size-h4"><a href="#/app-store/{{apps.request_uri}}">{{apps.name}}</a></p>
                            <p class="text-muted"><a href="#/app-store/{{apps.request_uri}}">{{apps.AppCompanyDeveloper.name}}</a></p>
                            
                        </div>
                        <!--
                        <div class="panel-bottom">
                        <p class="text-muted text-left"><a class="text-info" href="#/app-store/app/app">€ 2,50</a></p>
                        </div>
                        -->
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>