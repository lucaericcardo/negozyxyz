<div class="lapagina">
  <nav class="navbar navbar-default navbar-static-top" role="navigation">
    <div class="navbar-header">
      <div class="navbar-center"> 
        <!--<img class="navbar-brand" alt="" src="/app/webroot/asset/admin/images/pixmania.jpg">--> 
      </div>
      <ol class="breadcrumb nav navbar-nav">
        <!-- <li><a href="javascript:;">Bacheca</a></li>  -->
        <li class="active">Lisa Doe</li>
      </ol>
      <ul class="nav navbar-nav navbar-right">
        <div class="navbar-form"> <a href="#/products/add" class="btn btn-success" type="submit"><i class="fa fa-plus"></i> <?php echo __("Segui"); ?></a> </div>
      </ul>
    </div>
  </nav>
  
  
  <div class="page page-services">
    <div class="row">
      <div class="col-md-6 col-md-offset-3">
        <div class="panel panel-profile">
          <div class="panel-heading text-center bg-info"> <img class="img-circle img80_80" src="/img/g1.jpg" alt="">
            <h3 class="ng-binding">Lisa Doe</h3>
            <p>Project Manager</p>
          </div>
          <div class="list-justified-container">
            <ul class="list-justified text-center">
              <li>
                <p class="size-h3">679</p>
                <p class="text-muted">Post</p>
              </li>
              
              <li>
                <p class="size-h3">23</p>
                <p class="text-muted">Following</p>
              </li>
            </ul>
          </div>
        </div>
        <div class="row">
          <div class="col-sm-6">
            <div class="panel mini-box"> <span class="box-icon bg-success"> <i class="fa fa-shopping-cart"></i></span>
              <div class="box-info">
                <p class="size-h2">5</p>
                <p class="text-muted">Negozi</p>
              </div>
            </div>
          </div>
          <div class="col-sm-6">
            <div class="panel mini-box"> <span class="box-icon bg-info"> <i class="fa fa-check"></i> </span>
              <div class="box-info">
                <p class="size-h2">84</p>
                <p class="text-muted">Prodotti</p>
              </div>
            </div>
          </div>
        </div>
        <div class="panel panel-default">
          <div class="panel-heading"><strong><span class="glyphicon glyphicon-th"></span> Profile Info</strong></div>
          <div class="panel-body">
            <div class="media">
              <div class="media-body">
                <ul class="list-unstyled list-info">
                  <li class="ng-binding"> <span class="icon glyphicon glyphicon-user"></span>
                    <label>User name</label>
                    Lisa Doe </li>
                  <li> <span class="icon glyphicon glyphicon-envelope"></span>
                    <label>Email</label>
                    name@example.com </li>
                  <li> <span class="icon glyphicon glyphicon-home"></span>
                    <label>Address</label>
                    Street 123, Avenue 45, Country </li>
                  <li> <span class="icon glyphicon glyphicon-earphone"></span>
                    <label>Contact</label>
                    (+012) 345 6789 </li>
                  <li> <span class="icon glyphicon glyphicon-flag"></span>
                    <label>Nationality</label>
                    Australia </li>
                </ul>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
  
  
</div>
