<?php 
class Configuration
{
  // For a full list of configuration parameters refer in wiki page (https://github.com/paypal/sdk-core-php/wiki/Configuring-the-SDK)
  public static function getConfig()
  {
    $config = array(
        // values: 'sandbox' for testing
        //       'live' for production
        "mode" => "live",
        'log.LogEnabled' => true,
        'log.FileName' => '../PayPal.log',
        'log.LogLevel' => 'FINE'
  
        // These values are defaulted in SDK. If you want to override default values, uncomment it and add your value.
        // "http.ConnectionTimeOut" => "5000",
        // "http.Retry" => "2",
       
    );
    return $config;
  }
  
  // Creates a configuration array containing credentials and other required configuration parameters.
  public static function getAcctAndConfig()
  {
    $config = array(
        // Signature Credential

        "acct1.UserName" => "paypal_api1.lhsgroup.net",
        "acct1.Password" => "JN9M5NCSGV8H9QHP",
        "acct1.Signature" => "AFcWxV21C7fd0v3bYYYRCpSSRl31AJptVyABfpz2Bb0UnWXQuhN.TCjt",

         // Test Account
         /*
         "acct1.UserName" => "alberto_shop_api1.negozy.com",
         "acct1.Password" => "BQPLFQDQNBPHU5F8",
         "acct1.Signature" => "AFcWxV21C7fd0v3bYYYRCpSSRl31AtQegG5qddHw2YS7.3CNB90KyPo4",
         */

        );
    
    return array_merge($config, self::getConfig());;
  }

}


