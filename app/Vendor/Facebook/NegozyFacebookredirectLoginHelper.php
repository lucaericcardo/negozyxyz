<?php
class NegozyFacebookRedirectLoginHelper extends \Facebook\FacebookRedirectLoginHelper {

    protected function storeState($state) {
        CakeSession::write('state', $state);
    }

    protected function loadState(){
        return $this->state = CakeSession::read('state');
    }
}