<?php
Router::connect('/my/:action/*', array('controller' => 'notifies'));
Router::connect('/dashboard/shop/:shop_url', array('controller' => 'dashboard', 'action' => 'shop'),array('pass' => array('shop_url')));
Router::connect('/', array('controller' => 'dashboard', 'action' => 'main'));
Router::connect(
    "/view/:controller/:action/*",
    array('prefix' => 'view', 'view' => true)
);
Router::connect(
    "/application/:plugin/:controller/:action/*",
    array('prefix' => 'application','application' => true)
);
Router::parseExtensions("json","html","js");
Router::mapResources("/");
CakePlugin::routes();
require CAKE . 'Config' . DS . 'routes.php';
