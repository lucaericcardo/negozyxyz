<?php
$config['checkout_config']['error'] = array(
    "10404" => array(
        "code"      => "10404",
        "message"   => "",
        "type"      => "Carrello non presente."
    ),
    "10401" => array(
        "code"      => "10401",
        "message"   => "",
        "type"      => "Carrello non di proprietà dell'utente."
    ),
    "10405" => array(
        "code"      => "10405",
        "message"   => "",
        "type"      => "Acquisto già completato."
    ),
    "10407" => array(
        "code"      => "10407",
        "message"   => "",
        "type"      => "Acquisto non completato."
    ),
    "10502" => array(
        "code"      => "10502",
        "message"   => "",
        "type"      => "Errore nel salvataggio del carrello."
    ),
    "10504" => array(
        "code"      => "10504",
        "message"   => "",
        "type"      => "Prodotti presenti nel carello esauriti."
    )

);
