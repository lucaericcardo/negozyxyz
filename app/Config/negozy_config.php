<?php
$config['negozy_config'] = array(
    "login_url"         => "http://my.negozy.com/users/signin",
    "logout_url"        => "http://my.negozy.com/",
    "site_url"          => "http://my.negozy.com/",
    "my_url"            => "http://my.negozy.com/",
    "login_redirect"    => "http://my.negozy.com/",
    "negozy_url"        => "http://my.negozy.com/",
    "negozy_domain"     => "negozy.com",
    "signup_redirect"   => "http://it.negozy.com/pages/thanks",
    "paypal_url"        => "https://www.paypal.com/"
);
