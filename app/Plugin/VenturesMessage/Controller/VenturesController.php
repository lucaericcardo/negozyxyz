<?php
class VenturesController extends VenturesMessageAppController {

    public $components  = array("RequestHandler",'Paginator','Mandrillapp');
    public $uses        = array("Venture","User");

    public function application_index(){
        $this->layout = "ajax";
    }

    public function application_add(){
        $this->layout = "ajax";
    }

    public function application_edit(){
        $this->layout = "ajax";
    }

    public function load($venture_id = NULL){
        $data = array(
            "success" => false,
            "venture" => array()
        );
        if(isset($venture_id)) {
            $venture = $this->Venture->find("first",array(
                "conditions" => array(
                    "Venture.id" => $venture_id
                ),
                "contain" => array(
                    "User" => array(
                        "fields" => array(
                            "User.id",
                            "User.first_name",
                            "User.last_name",
                        ),
                        "Shop" => array(
                            "fields" => array(
                                "name"
                            )
                        )
                    )
                )
            ));
            if($venture) {
                $data['success'] = TRUE;
                $user_data = Set::Extract('/User/.',$venture);
                $users = array();
                $users_ids = array();
                if(count($user_data)) {
                    foreach($user_data as $user) {
                        if(isset($user['id'])) {
                            $shop = reset($user['Shop']);
                            $name = $user['first_name'] . " " . $user['last_name'];
                            $name .= (isset($shop['name'])) ? ' (' . $shop['name'] . ')' : '';
                            $users[] = array(
                                "id" => $user['id'],
                                "name" => $name
                            );
                            $users_ids[] = $user['id'];
                        }
                    }
                }
                if($venture['Venture']['broadcast']) {
                    $type = "broadcast";
                } else if($venture['Venture']['with_shop']) {
                    $type = "with_shop";
                } else if($venture['Venture']['without_shop']) {
                    $type = "without_shop";
                } else {
                    $type = "user_list";
                }
                $data['venture'] = array(
                    "id"        => $venture['Venture']['id'],
                    "name"      => $venture['Venture']['title'],
                    "text"      => $venture['Venture']['text'],
                    "message"   => $venture['Venture']['message'],
                    "from_date" => $venture['Venture']['from_date'],
                    "to_date"   => $venture['Venture']['to_date'],
                    "type"      => $type,
                    "users"     => $users_ids
                );
            }
        }
        $this->set(array(
            'data' => $data,
            'users' => $users,
            '_serialize' => array('data','users')
        ));
    }

    public function get_list($n=10,$page=1,$row="created",$orderType="ASC"){
        $this->response->compress();
        $this->Paginator->settings = array(
            "recursive" => -1,
            "fields" => array(
                "Venture.id",
                "Venture.title",
                "timestamp_from_date",
                "timestamp_to_date",
                "timestamp_created"
            ),
            "order" => "Venture.".$row." ".$orderType,
            "limit" => $n,
            "page" => $page,
            "recursive" => -1
        );
        $count = $this->Venture->find("count",array(
            "recursive" => -1,
        ));
        $ventures = $this->Paginator->paginate('Venture');
        $ventures = Set::extract("/Venture/.",$ventures);
        $this->set(array(
            'ventures' => $ventures,
            'count' => $count,
            '_serialize' => array('ventures','count')
        ));
    }

    public function save(){
        $data['success'] = false;
        $data['message'] = __("Si sono verificati dei problemi durante il salvataggio dell'iniziativa");
        if($this->request->is("post")) {
            $venture = $this->request->data["venture"];
            $from_date = NULL;
            $to_date = NULL;
            if(isset($venture['from_date']) && !empty($venture['from_date']) && isset($venture['to_date']) && !empty($venture['to_date'])) {
                $from_date  = $venture['from_date']." 00:00:00";
                $to_date    = $venture['to_date'] . " 23:59:59";
            }
            $newVenture['Venture'] = array(
                "title"     => $venture['name'],
                "text"      => isset($venture['text']) ? $venture['text'] : null,
                "message"   => isset($venture['message']) ? $venture['message'] : null,
                "user_id"   => $this->Auth->user("id"),
                "from_date" => $from_date,
                "to_date"   => $to_date
            );
            $newVenture['User'] = array();
            if($venture['type'] == "user_list"){
                $newVenture['User'] = $venture['users'];
            } else if($venture['type'] == "with_shop"){
                $newVenture['Venture']['with_shop'] = TRUE;
            } else if($venture['type'] == "without_shop"){
                $newVenture['Venture']['without_shop'] = TRUE;
            } else {
                $newVenture['Venture']['broadcast'] = TRUE;
            }
            if($this->Venture->saveAll($newVenture)) {
                $data['success'] = true;
                $data['message'] = __("L'iniziativa è stata aggiunta con successo");
            }
        }
        $this->set(array(
            'data' => $data,
            '_serialize' => array('data')
        ));
    }

    public function update(){
        $data['success'] = false;
        $data['message'] = __("Si sono verificati dei problemi durante il salvataggio dell'iniziativa");
        if($this->request->is("post")) {
            $venture = $this->request->data["venture"];
            $from_date = NULL;
            $to_date = NULL;
            if(isset($venture['from_date']) && !empty($venture['from_date']) && isset($venture['to_date']) && !empty($venture['to_date'])) {
                $from_date  = $venture['from_date']." 00:00:00";
                $to_date    = $venture['to_date'] . " 23:59:59";
            }
            $newVenture['Venture'] = array(
                "title"     => $venture['name'],
                "text"      => isset($venture['text']) ? $venture['text'] : null,
                "message"   => isset($venture['message']) ? $venture['message'] : null,
                "from_date" => $from_date,
                "to_date"   => $to_date
            );
            $newVenture['User'] = array();
            $newVenture['Venture']['with_shop']     = FALSE;
            $newVenture['Venture']['without_shop']  = FALSE;
            $newVenture['Venture']['broadcast']     = FALSE;
            if($venture['type'] == "user_list"){
                $newVenture['User'] = $venture['users'];
            } else if($venture['type'] == "with_shop"){
                $newVenture['Venture']['with_shop'] = TRUE;
            } else if($venture['type'] == "without_shop"){
                $newVenture['Venture']['without_shop'] = TRUE;
            } else {
                $newVenture['Venture']['broadcast'] = TRUE;
            }
            $this->Venture->id = $venture['id'];
            if($this->Venture->save($newVenture)) {
                $data['success'] = true;
                $data['message'] = __("L'iniziativa è stata modificata con successo");
            }
        }
        $this->set(array(
            'data' => $data,
            '_serialize' => array('data')
        ));
    }

    public function delete(){
        $data['success'] = false;
        if($this->request->is("post")) {
            if($this->Venture->delete($this->request->data["venture_id"])){
                $data['success'] = true;
            }
        }
        $this->set(array(
            'data' => $data,
            '_serialize' => array('data')
        ));
    }

    public function load_user(){
        $return = array();
        if($this->request->is("post")) {
            $params = explode(" ",$this->request->data["query"]);
            $conditions_user = array();
            $conditions_shop = array();
            $conditions_user["UPPER(first_name) LIKE"] = "%%";
            $conditions_user["UPPER(last_name) LIKE"] = "%%";
            foreach($params as $param) {
                $param = strtoupper($param);
                $conditions_user["UPPER(first_name) LIKE"] = "%".$param."%";
                $conditions_user["UPPER(last_name) LIKE"] = "%".$param."%";
            }
            $users = $this->User->find("all",array(
                "recursive" => -1,
                "fields" => array(
                    "id",
                    "first_name",
                    "last_name"
                ),
                "conditions" => array(
                    "OR" => $conditions_user,
                    "NOT" => array(
                        "id" => $this->request->data["users"]
                    )
                ),
                "contain" => array(
                    "Shop" => array(
                        "fields" => array(
                            "name"
                        )
                    )
                ),
                "limit" => 10
            ));
            foreach($users as $user) {
                $shop = reset($user['Shop']);
                $name = $user['User']['first_name']." ".$user['User']['last_name'];
                $name .= (isset($shop['name'])) ? ' ('.$shop['name'].')' : '';
                $return[] = array(
                    "name"  => $name,
                    "id"    => $user['User']['id']
                );
            }
        }
        $this->set(array(
            'users' => $return,
            '_serialize' => array('users')
        ));
    }

    public function sendMail(){
        $data['success'] = false;
        $data['message'] = __("Si sono verificati dei problemi durante l'invio delle email");
        if($this->request->is("post") && isset($this->request->data['venture_id'])) {
            $venture = $this->Venture->find("first",array(
                "conditions" => array(
                    "id" => $this->request->data['venture_id']
                ),
                "contain" => array(
                    "User" => array(
                        "fields" => array(
                            "User.id",
                            "User.first_name",
                            "User.last_name",
                            "User.email"
                        )
                    )
                )
            ));
            if($venture) {
                $users = array();
                if($venture['Venture']['with_shop']) {
                    $users = Set::Extract("/User/.",$this->loadUserWithShop());
                } else if($venture['Venture']['without_shop']) {
                    $users = Set::Extract("/User/.",$this->loadUserWithoutShop());
                } else if($venture['Venture']['broadcast']) {
                    $users = Set::Extract("/User/.",$this->loadrAllUser());
                } else {
                    $users = Set::Extract("/User/.",$venture);
                }
                $this->Mandrillapp->send_venture_invite("Alberto Pacini","alberto@lhsgroup.net",$venture['Venture']['message'],$venture['Venture']['title']);
                foreach($users as $user){
                    //$this->Mandrillapp->send_venture_invite($user['first_name']." ".$user['last_name'],$user['email'],$venture['Venture']['text'],$venture['Venture']['title']);
                    //echo $user['first_name']." ".$user['last_name']." ".$user['email']."\n";
                }
                $data['success'] = true;
                $data['message'] = __("Sono state inviate ".count($users)." di invito");
            }
        }
        $this->set(array(
            'data' => $data,
            '_serialize' => array('data')
        ));
    }


    private function loadUserWithShop(){
        return $this->User->find("all",array(
            "fields" => array(
                "User.first_name",
                "User.last_name",
                "User.email"
            ),
            "joins" => array(
                array(
                    'table' => 'shops',
                    'alias' => 'Shop',
                    'type' => 'RIGHT',
                    'conditions'=> array('Shop.user_id = User.id')
                ),
            ),
            "recursive" => -1
        ));
    }

    private function loadUserWithoutShop(){
        return $this->User->find("all",array(
            "fields" => array(
                "User.first_name",
                "User.last_name",
                "User.email",
                "Shop.id"
            ),
            "joins" => array(
                array(
                    'table' => 'shops',
                    'alias' => 'Shop',
                    'type' => 'LEFT OUTER',
                    'conditions'=> array(
                        'Shop.user_id = User.id',
                    )
                ),
            ),
            "conditions" => array(
                'Shop.user_id' => NULL
            ),
            "recursive" => -1
        ));
    }

    private function loadrAllUser(){
        return $this->User->find("all",array(
            "fields" => array(
                "User.first_name",
                "User.last_name",
                "User.email"
            ),
            "recursive" => -1
        ));
    }
}