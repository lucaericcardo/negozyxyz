(function() {
    'use strict';
    angular.module('plugin.VenturesMessage.services', [])
        .factory("VenturesMessageVenture", function ($http) {
            return {
                add: function(venture) {
                    return $http.post('/ventures_message/ventures/save.json',{venture:venture});
                },
                update: function(venture) {
                    return $http.post('/ventures_message/ventures/update.json',{venture:venture});
                },
                getList: function (n, page, row, orderType) {
                    return $http.get('/ventures_message/ventures/get_list/' + n + '/' + page + '/' + row + '/' + orderType+'.json');
                },
                remove: function(venture_id){
                    return $http.post('/ventures_message/ventures/delete.json',{venture_id:venture_id});
                },
                load: function(venture_id){
                    return $http.get('/ventures_message/ventures/load/' + venture_id+'.json');
                },
                sendMail: function(venture_id){
                    return $http.post('/ventures_message/ventures/sendMail.json',{venture_id:venture_id});
                }
            }
        })
        .factory("VenturesMessageUser", function ($http) {
            return {
                find: function(query,users) {
                    return $http.post('/ventures_message/ventures/load_user.json',{query:query,users:users});
                }
            }
        })
}).call(this);
