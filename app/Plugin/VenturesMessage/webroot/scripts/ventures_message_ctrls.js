(function() {
    'use strict';
    var ventures_message = angular.module('plugin.VenturesMessage.controllers', ['daterangepicker']);
    ventures_message.controller('VenturesMessageAddCtrl',  function($scope,$activityIndicator,VenturesMessageVenture,VenturesMessageUser) {
        $scope.venture = {
            name        : "",
            text        : "",
            message     : "",
            from_date   : "",
            to_date     : ""
        };
        $scope.onLoading = false;
        $scope.successSave = false;
        $scope.errorSave = false;

        $scope.types = [
            {id:"broadcast","name":"Tutti gli utenti"},
            {id:"with_shop","name":"Utenti con shop"},
            {id:"without_shop","name":"Utenti senza shop"},
            {id:"user_list","name":"Utenti definiti"}
        ];
        $scope.date_range_venture = {
            startDate: moment().subtract(1,"month"),
            endDate: moment()
        };
        $scope.$watch('date_range_venture', function(newDate) {
            if(newDate) {
                $scope.venture.from_date = moment(newDate.startDate).format("YYYY-MM-DD");
                $scope.venture.to_date = moment(newDate.endDate).format("YYYY-MM-DD");
            }

        }, false);
        $scope.users = [];
        $scope.loadUser = function(q){
            if(q) {
                VenturesMessageUser.find(q,$scope.venture.users).success(function(r){
                    $scope.users = r.users;
                })
            }
        }
        $scope.opts = {
            ranges: {
                'Last 7 Days': [moment().subtract('days', 6), moment()],
                'Last 30 Days': [moment().subtract('days', 29), moment()]
            }
        };
        $scope.save = function(){
            VenturesMessageVenture.add($scope.venture).success(function(r){
                if(r.data.success){
                    $scope.successSave = true;
                } else {
                    $scope.errorSave = true;
                }
            });
        }
    })
    .controller('VenturesMessageEditCtrl',  function($scope,$activityIndicator,VenturesMessageVenture,VenturesMessageUser,$routeParams,toaster) {
            $activityIndicator.startAnimating();
            $scope.venture = {};
            VenturesMessageVenture.load($routeParams.venture_id).success(function(r){
                $activityIndicator.stopAnimating();
                if(r.data.success){
                    $scope.venture  = r.data.venture;
                    $scope.users    = r.users;
                    $scope.date_range = {
                        startDate   : moment(r.data.venture.from_date,'YYYY-MM-DD hh:mm:ss'),
                        endDate     : moment(r.data.venture.to_date,'YYYY-MM-DD hh:mm:ss')
                    };
                }
            });
            $scope.types = [
                {id:"broadcast","name":"Tutti gli utenti"},
                {id:"with_shop","name":"Utenti con shop"},
                {id:"without_shop","name":"Utenti senza shop"},
                {id:"user_list","name":"Utenti definiti"}
            ];
            $scope.$watch('date_range', function(newDate) {
                if(newDate) {
                    $scope.venture.from_date = moment(newDate.startDate).format("YYYY-MM-DD");
                    $scope.venture.to_date = moment(newDate.endDate).format("YYYY-MM-DD");
                }

            }, false);
            $scope.loadUser = function(q){
                if(q) {
                    VenturesMessageUser.find(q,$scope.venture.users).success(function(r){
                        $scope.users = r.users;
                    })
                }
            }

            $scope.opts = {};
            $scope.update = function(){
                VenturesMessageVenture.update($scope.venture).success(function(r){
                    if(r.data.success) {
                        toaster.pop('success', "", r.data.message);
                    } else {
                        toaster.pop('error', "", r.data.message);
                    }
            });
        }
    })
    .controller('VenturesMessageMainCtrl',  function($scope,$activityIndicator,VenturesMessageVenture,toaster) {
            $scope.numPerPageOpt        = [5, 10, 20];
            $scope.numPerPage           = $scope.numPerPageOpt[1];
            $scope.currentPage          = 1;
            $scope.currentPageStores    = [];
            $scope.ventures             = [];
            $scope.totalVentures        = 0;
            $scope.row                  = 'created';
            $scope.orderType            = 'ASC';
            $scope.query                = '';

            $activityIndicator.startAnimating();
            $scope.collections = [];

            $scope.select = function(page) {
                $activityIndicator.startAnimating();
                $scope.deselectAll  = true;
                $scope.selectAll    = false;
                VenturesMessageVenture.getList($scope.numPerPage,page,$scope.row,$scope.orderType).success(function(r){
                    $scope.ventures        = r.ventures;
                    $scope.totalVentures   = r.count;
                    $activityIndicator.stopAnimating();
                });
            };

            $scope.onFilterChange = function() {
                $scope.select(1);
                $scope.currentPage = 1;
                return $scope.row = '';
            };
            $scope.onNumPerPageChange = function() {
                $scope.select(1);
                return $scope.currentPage = 1;
            };
            $scope.onOrderChange = function() {
                $scope.select(1);
                return $scope.currentPage = 1;
            };
            $scope.order = function(rowName,orderType) {
                if ($scope.row === rowName && $scope.orderType === orderType) {
                    return;
                }
                $activityIndicator.startAnimating();
                $scope.row = rowName;
                $scope.orderType = orderType;
                return $scope.onOrderChange();
            };

            $scope.deleteVenture = function(venture_id){
                VenturesMessageVenture.remove(venture_id).success(function(r){
                    if(r.data.success) {
                        angular.forEach($scope.ventures, function(value, key) {
                            if(value.id== venture_id) {
                                $scope.ventures.splice(key, 1);
                            }
                        });
                    }
                })
            }

            var init = function() {
                return $scope.select($scope.currentPage);
            };
            $scope.search = function(){
                $scope.select($scope.currentPage);
            }
            $scope.send = function(venture_id){
                VenturesMessageVenture.sendMail(venture_id).success(function(r){
                    if(r.data.success) {
                        toaster.pop('success', "", r.data.message);
                    } else {
                        toaster.pop('error', "", r.data.message);
                    }
                });
            }
            return init();
    })
}).call(this);
