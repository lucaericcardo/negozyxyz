<div class="lapagina" data-ng-controller="VenturesMessageEditCtrl">
    <nav class="navbar navbar-default navbar-static-top" role="navigation">
        <div class="navbar-header">
            <ul class="nav navbar-nav">
                <li>
                    <ol class="breadcrumb">
                        <li><a href="#/app/ventures_message"><?php echo __("Ventures Message"); ?></a></li>
                        <li class="active"><a><?php echo __("Modifica iniziativa"); ?></a></li>
                    </ol>
                </li>
            </ul>
        </div>
    </nav>
    <div class="page page-table">
        <div class="row">
            <div class="col-lg-10 col-lg-offset-1 col-md-12">
                <section class="panel panel-default">
                    <div class="panel-body">
                        <div class="row">
                            <div class="col-md-2">
                                <h4><?php echo __("Dati iniziativa"); ?></h4>
                            </div>
                            <div class="col-md-10">
                                <div class="form-group">
                                    <label for=""><?php echo __("Nome"); ?> *</label>
                                    <input type="text" class="form-control" required data-ng-model="venture.name">
                                </div>
                                <div class="form-group">
                                    <label for=""><?php echo __("Descrizione"); ?></label>
                                    <div text-angular data-ng-model="venture.text"></div>
                                </div>
                                <div class="form-group">
                                    <label for=""><?php echo __("Messaggio"); ?></label>
                                    <div text-angular data-ng-model="venture.message"></div>
                                </div>

                                <div class="form-group">
                                    <label for=""><?php echo __("Intervallo di validità"); ?></label>
                                </div>
                                <input type="text" class="form-control date-picker" options="opts" date-range-picker data-ng-model="date_range">
                            </div>
                        </div>
                        <hr/>
                        <div class="divider"></div>
                        <div class="row">
                            <div class="col-md-2">
                                <h4><?php echo __("Utenti"); ?></h4>
                            </div>
                            <div class="col-md-10">
                                <div class="form-group">
                                    <label for="">Tipologia</label>
                                    <select data-ng-options="type.id as type.name for type in types" class="form-control" data-ng-model="venture.type"></select>
                                </div>
                                <div data-ng-if="venture.type == 'user_list'" class="form-group">
                                    <label for="">Utenti</label>
                                    <ui-select multiple ng-model="venture.users" style="width: 100%;" theme="bootstrap">
                                        <ui-select-match placeholder="Seleziona gli utenti">{{$item.name}}</ui-select-match>
                                        <ui-select-choices  reset-search-input="true" refresh="loadUser($select.search)" refresh-delay="0" repeat="user.id as user in users | filter:$select.search">
                                            <div ng-bind-html="user.name | highlight: $select.search"></div>
                                        </ui-select-choices>
                                    </ui-select>
                                </div>
                            </div>
                        </div>
                        <hr/>
                        <div class="divider"></div>
                        <div class="row">
                            <div class="col-md-10 col-md-offset-2">
                                <button data-ng-click="update()" class="btn btn-success btn-lg" type="button"><?php echo __("Salva"); ?></button>
                            </div>
                        </div>
                    </div>
                </section>
            </div>
        </div>
    </div>
</div>
