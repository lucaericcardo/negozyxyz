<div class="lapagina" data-ng-controller="NewsStreamMainCtrl">
    <nav class="navbar navbar-default navbar-static-top" role="navigation">
        <div class="navbar-header">
            <ul class="nav navbar-nav">
                <li>
                    <ol class="breadcrumb">
                        <li class="active"><a><?php echo __("News Stream"); ?></a></li>
                    </ol>
                </li>
            </ul>
            <ul class="nav navbar-nav navbar-right">
                <div class="navbar-form">
                    <a href="#/app/news_stream/add" class="btn btn-success"><i class="fa fa-plus"></i> <?php echo __("Aggiungi news"); ?></a>
                </div>
            </ul>
        </div>
    </nav>
    <div class="page page-table">
        <div class="row">
            <div class="col-lg-10 col-lg-offset-1 col-md-12">
                <section class="panel panel-default table-dynamic">
                    <div class="panel-body" data-ng-show="ventures.length">
                        <table class="table table-bordered table-striped table-responsive">
                            <thead>
                            <tr>
                                <th>
                                    <div class="th">
                                        <?php echo __("Titolo"); ?>
                                        <span class="glyphicon glyphicon-chevron-up" data-ng-click=" order('name','ASC') " data-ng-class="{active: row == 'name' && orderType == 'ASC'}"></span>
                                        <span class="glyphicon glyphicon-chevron-down" data-ng-click=" order('name','DESC') " data-ng-class="{active: row == 'name' && orderType == 'DESC'}"></span>
                                    </div>
                                </th>
                                <th>
                                    <div class="th">
                                        <?php echo __("Data di inizio"); ?>
                                        <span class="glyphicon glyphicon-chevron-up" data-ng-click=" order('from_date','ASC') " data-ng-class="{active: row == 'from_date' && orderType == 'ASC'}"></span>
                                        <span class="glyphicon glyphicon-chevron-down" data-ng-click=" order('from_date','DESC') " data-ng-class="{active: row == 'from_date' && orderType == 'DESC'}"></span>
                                    </div>
                                </th>
                                <th>
                                    <div class="th">
                                        <?php echo __("Data di fine"); ?>
                                        <span class="glyphicon glyphicon-chevron-up" data-ng-click=" order('to_date','ASC') " data-ng-class="{active: row == 'to_date' && orderType == 'ASC'}"></span>
                                        <span class="glyphicon glyphicon-chevron-down" data-ng-click=" order('to_date','DESC') " data-ng-class="{active: row == 'to_date' && orderType == 'DESC'}"></span>
                                    </div>
                                </th>
                                <th>
                                    <div class="th">
                                        <?php echo __("Data di creazione"); ?>
                                        <span class="glyphicon glyphicon-chevron-up" data-ng-click=" order('created','ASC') " data-ng-class="{active: row == 'created' && orderType == 'ASC'}"></span>
                                        <span class="glyphicon glyphicon-chevron-down" data-ng-click=" order('created','DESC') " data-ng-class="{active: row == 'created' && orderType == 'DESC'}"></span>
                                    </div>
                                </th>
                                <th class="text-right" width="120">
                                    <div class="th">
                                        <?php echo __("Azioni"); ?>
                                    </div>
                                </th>
                            </tr>
                            </thead>
                            <tbody>
                            <tr data-ng-repeat="venture in ventures">
                                <td>{{venture.title}}</td>
                                <td>{{venture.timestamp_from_date | date:'dd/MM/yyyy  H:mm'}}</td>
                                <td>{{venture.timestamp_to_date | date:'dd/MM/yyyy  H:mm'}}</td>
                                <td>{{venture.timestamp_created | date:'dd/MM/yyyy  H:mm'}}</td>
                                <td class="text-right">
                                    <a class="btn btn-primary" href="#/app/news_stream/edit/{{venture.id}}"><i class="fa fa-pencil-square-o"></i></a>
                                    <button class="btn btn-danger" confirm-callback="deleteVenture(venture.id)" confirm-message="<?php echo __("Sei sicuro di voler eliminare la news?"); ?>" confirm-modal><i class="glyphicon glyphicon-trash"></i></button>
                                </td>
                            </tr>
                            </tbody>
                        </table>
                        <footer class="table-footer">
                            <div class="row">
                                <div class="col-md-6 page-num-info">
                                    <span>
                                        <?php echo __("Elementi per pagina"); ?>
                                        <select data-ng-model="numPerPage" data-ng-options="num for num in numPerPageOpt" data-ng-change="onNumPerPageChange()">
                                        </select>
                                    </span>
                                </div>
                                <div class="col-md-6 text-right pagination-container">
                                    <pagination class="pagination-sm" ng-model="currentPage" total-items="totalVentures" max-size="4" ng-change="select(currentPage)" items-per-page="numPerPage" rotate="false" boundary-links="true"></pagination>
                                </div>
                            </div>
                        </footer>
                    </div>
                    <div class="panel-body" data-ng-hide="ventures.length">
                        <div class="callout callout-info">
                            <p><?php echo __("Nessuna news presente"); ?></p>
                        </div>
                    </div>
                </section>
            </div>
        </div>
    </div>
</div>
