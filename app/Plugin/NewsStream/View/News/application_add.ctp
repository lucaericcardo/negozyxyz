<div class="lapagina" data-ng-controller="NewsStreamAddCtrl">
    <nav class="navbar navbar-default navbar-static-top" role="navigation">
        <div class="navbar-header">
            <ul class="nav navbar-nav">
                <li>
                    <ol class="breadcrumb">
                        <li><a href="#/app/news_stream/"><?php echo __("News Stream"); ?></a></li>
                        <li class="active"><a><?php echo __("Aggiungi news"); ?></a></li>
                    </ol>
                </li>
            </ul>
        </div>
    </nav>
    <div class="page page-table">
        <div class="row">
            <div class="col-lg-10 col-lg-offset-1 col-md-12">
                <section class="panel panel-default">
                    <div class="panel-body">
                        <div ng-show="successSave" class="callout callout-success">
                            <p><?php echo __("News aggiunta con successo"); ?></p>
                            <a href="#/app/news_stream"><?php echo __("Torna alla lista delle news"); ?></a>
                        </div>

                        <div ng-if="onLoading">
                            <p class="text-muted"><i class="fa fa-spinner fa-spin"></i> Caricamento {{current_image}} in corso.</p>
                            <progress class="progress-striped active">
                                <bar value="progress" type="success"><span>{{count_image}} / {{uploader.queue.length}}</span></bar>
                            </progress>

                        </div>
                        <div ng-if="onLoading || successSave">
                            <ul class="list-unstyled">
                                <li data-ng-repeat="success_image in success_images" class="text-success"><i class="fa fa-check"></i> {{success_image}} <b><?php echo __("aggiunto con successo"); ?></b></li>
                            </ul>
                            <ul class="list-unstyled">
                                <li data-ng-repeat="error_image in error_images" class="text-danger"><i class="fa fa-times"></i> {{error_image}} <b><?php echo __("errore nel caricamento"); ?></b></li>
                            </ul>
                        </div>
                        <div ng-show="errorSave" class="callout callout-danger">
                            <?php echo __("Si è verificato un problema durante l'aggiunta della collection."); ?>
                        </div>

                        <div ng-show="!onLoading && !successSave">
                            <div class="row">
                                <div class="col-md-2">
                                    <h4><?php echo __("Dati news"); ?></h4>
                                </div>
                                <div class="col-md-10">
                                    <div class="form-group">
                                        <label for=""><?php echo __("Nome"); ?></label>
                                        <input type="text" class="form-control" required data-ng-model="venture.name">
                                    </div>
                                    <div class="form-group">
                                        <label for=""><?php echo __("News Feed"); ?></label>
                                        <div text-angular data-ng-model="venture.feed"></div>
                                    </div>
                                    <div class="form-group">
                                        <label for=""><?php echo __("Link"); ?></label>
                                        <input type="text" class="form-control" data-ng-model="venture.link">
                                    </div>
                                    <div class="form-group">
                                        <label for=""><?php echo __("Intervallo di validità"); ?></label>
                                    </div>
                                    <input type="text" class="form-control date-picker" options="opts" date-range-picker data-ng-model="date_range_venture">
                                </div>
                            </div>
                            <hr/>
                            <div class="divider"></div>
                            <div class="row">
                                <div class="col-md-10 col-md-offset-2">
                                    <button data-ng-click="save()" class="btn btn-success btn-lg" type="button"><?php echo __("Salva"); ?></button>
                                </div>
                            </div>
                        </div>
                    </div>
                </section>
            </div>
        </div>
    </div>
</div>
