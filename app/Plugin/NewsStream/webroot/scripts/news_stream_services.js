(function() {
    'use strict';
    angular.module('plugin.NewsStream.services', [])
        .factory("NewsStreamNew", function ($http) {
            return {
                add: function(venture) {
                    return $http.post('/news_stream/news/save.json',{venture:venture});
                },
                update: function(venture) {
                    return $http.post('/news_stream/news/update.json',{venture:venture});
                },
                getList: function (n, page, row, orderType) {
                    return $http.get('/news_stream/news/get_list/' + n + '/' + page + '/' + row + '/' + orderType+'.json');
                },
                remove: function(venture_id){
                    return $http.post('/news_stream/news/delete.json',{venture_id:venture_id});
                },
                load: function(venture_id){
                    return $http.get('/news_stream/news/load/' + venture_id+'.json');
                }
            }
        })
}).call(this);
