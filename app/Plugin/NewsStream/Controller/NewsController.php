<?php
class NewsController extends NewsStreamAppController {

    public $components  = array("RequestHandler",'Paginator');
    public $uses        = array("News");

    public function application_index(){
        $this->layout = "ajax";
    }

    public function application_add(){
        $this->layout = "ajax";
    }

    public function application_edit(){
        $this->layout = "ajax";
    }

    public function load($venture_id = NULL){
        $data = array(
            "success" => false,
            "venture" => array()
        );
        if(isset($venture_id)) {
            $venture = $this->News->find("first",array(
                "conditions" => array(
                    "News.id" => $venture_id
                )
            ));
            if($venture) {
                $data['success'] = TRUE;

                $data['venture'] = array(
                    "id"        => $venture['News']['id'],
                    "name"      => $venture['News']['title'],
                    "feed"      => $venture['News']['text'],
                    "link"      => $venture['News']['link'],
                    "text_link" => $venture['News']['text_link'],
                    "from_date" => $venture['News']['from_date'],
                    "to_date"   => $venture['News']['to_date'],
                );
            }
        }
        $this->set(array(
            'data' => $data,
            '_serialize' => array('data')
        ));
    }

    public function get_list($n=10,$page=1,$row="created",$orderType="ASC"){
        $this->response->compress();
        $this->Paginator->settings = array(
            "recursive" => -1,
            "fields" => array(
                "News.id",
                "News.title",
                "News.timestamp_from_date",
                "News.timestamp_to_date",
                "News.timestamp_created"
            ),
            "order" => "News.".$row." ".$orderType,
            "limit" => $n,
            "page" => $page,
            "recursive" => -1
        );
        $count = $this->News->find("count",array(
            "recursive" => -1,
        ));
        $ventures = $this->Paginator->paginate('News');
        $ventures = Set::extract("/News/.",$ventures);
        $this->set(array(
            'ventures' => $ventures,
            'count' => $count,
            '_serialize' => array('ventures','count')
        ));
    }

    public function save(){
        $data['success'] = false;
        $data['message'] = __("Si sono verificati dei problemi durante il salvataggio della news");
        if($this->request->is("post")) {
            $venture = $this->request->data["venture"];
            $from_date = NULL;
            $to_date = NULL;
            if(isset($venture['from_date']) && !empty($venture['from_date']) && isset($venture['to_date']) && !empty($venture['to_date'])) {
                $from_date  = $venture['from_date']." 00:00:00";
                $to_date    = $venture['to_date'] . " 23:59:59";
            }
            $newNews['News'] = array(
                "title"     => $venture['name'],
                "text"      => isset($venture['feed']) ? $venture['feed'] : null,
                "link"      => isset($venture['link']) ? $venture['link'] : null,
                "from_date" => $from_date,
                "to_date"   => $to_date
            );
            if($this->News->saveAll($newNews)) {
                $data['success'] = true;
                $data['message'] = __("La news è stata aggiunta con successo");
            }
        }
        $this->set(array(
            'data' => $data,
            '_serialize' => array('data')
        ));
    }

    public function update(){
        $data['success'] = false;
        $data['message'] = __("Si sono verificati dei problemi durante il salvataggio dell'iniziativa");
        if($this->request->is("post")) {
            $venture = $this->request->data["venture"];
            $from_date = NULL;
            $to_date = NULL;
            if(isset($venture['from_date']) && !empty($venture['from_date']) && isset($venture['to_date']) && !empty($venture['to_date'])) {
                $from_date  = $venture['from_date']." 00:00:00";
                $to_date    = $venture['to_date'] . " 23:59:59";
            }
            $newNews['News'] = array(
                "title"     => $venture['name'],
                "text"      => isset($venture['feed']) ? $venture['feed'] : null,
                "link"      => isset($venture['link']) ? $venture['link'] : null,
                "from_date" => $from_date,
                "to_date"   => $to_date
            );
            $this->News->id = $venture['id'];
            if($this->News->save($newNews)) {
                $data['success'] = true;
                $data['message'] = __("L'iniziativa è stata modificata con successo");
            }
        }
        $this->set(array(
            'data' => $data,
            '_serialize' => array('data')
        ));
    }

    public function delete(){
        $data['success'] = false;
        if($this->request->is("post")) {
            if($this->News->delete($this->request->data["venture_id"])){
                $data['success'] = true;
            }
        }
        $this->set(array(
            'data' => $data,
            '_serialize' => array('data')
        ));
    }

}