<div class="lapagina" data-ng-controller="MeteoMancianoDashboardCtrl">
    <nav class="navbar navbar-default navbar-static-top" role="navigation">
        <div class="navbar-header">
            <ul class="nav navbar-nav">
                <li>
                    <ol class="breadcrumb">
                        <li class="active">Meteo Manciano</li>
                    </ol>
                </li>
            </ul>
        </div>
    </nav>

    <div class="page page-table">
        <div class="row">
            <div class="col-lg-10 col-lg-offset-1 col-md-12">
                <section class="panel panel-default table-dynamic">
                    <div class="panel-body">
                        <div data-ng-show="print">
                            <h1>{{print}}</h1>
                            <p>Clicca sul seguente link per accedere alla <a href="#/app/meteo_manciano/list">view successiva</a>!</p>
                        </div>
                    </div>
                </section>
            </div>
        </div>
    </div>
</div>