(function() {
    'use strict';
    angular.module('plugin.MeteoManciano.services', [])
        .factory('MeteoMancianoServices', function ($http) {
            return {
                hello: function() {
                    return $http.post('/meteo_manciano/services/hello.json');
                },
                congratulations: function() {
                    return $http.get('/meteo_manciano/services/congratulations.json');
                }
            }
        })
}).call(this);