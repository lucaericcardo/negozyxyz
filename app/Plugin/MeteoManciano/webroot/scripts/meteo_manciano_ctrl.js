(function() {
    'use strict';
    var meteo_manciano = angular.module('plugin.MeteoManciano.controllers', []);
    meteo_manciano.controller('MeteoMancianoDashboardCtrl',  function($scope,$activityIndicator,MeteoMancianoServices) {
        $activityIndicator.startAnimating();
        MeteoMancianoServices.hello().success(function(r) {
            if(r.data.success) {
                $scope.print = r.data.message;
            }
            $activityIndicator.stopAnimating();
        });
    }).controller('MeteoMancianoListCtrl',  function($scope,$activityIndicator,MeteoMancianoServices) {
        $activityIndicator.startAnimating();
        MeteoMancianoServices.congratulations().success(function(r) {
            if(r.data.success) {
                $scope.print = r.data.message;
            }
            $activityIndicator.stopAnimating();
        });
    })
}).call(this);