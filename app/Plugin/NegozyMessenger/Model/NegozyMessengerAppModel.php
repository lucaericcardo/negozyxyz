<?php
class NegozyMessengerAppModel extends AppModel {

    /**
     * Tutti i modelli estendono NegozyMessengerAppModel
     *  Per prendere la configurazione del db di negozy il nome è default
     */

    public $tablePrefix  = 'messenger_';
}