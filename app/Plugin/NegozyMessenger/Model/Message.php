<?php
class Message extends NegozyMessengerAppModel {

    public $actsAs = array('Containable');

    public $virtualFields = array(
        "timestamp" => "extract(epoch from Message.created at time zone 'Europe/Rome') * 1000"
    );

}