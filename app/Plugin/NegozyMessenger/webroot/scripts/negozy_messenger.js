NegozyMessengerViews = {
    "views": {
        "main": {
            "template": '/application/negozy_messenger.html',
            "url": '/app/negozy_messenger'
        },
        "message": {
            "template": '/application/negozy_messenger/message.html',
            "url": '/app/negozy_messenger/messages/:message_id'
        }
    }
};

