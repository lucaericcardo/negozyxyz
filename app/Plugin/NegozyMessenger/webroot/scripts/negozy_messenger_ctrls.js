(function() {
    'use strict';
    var negozy_messenger = angular.module('plugin.NegozyMessenger.controllers', ['ui.select2']);
    negozy_messenger
        .controller('NegozyMessengerMainCtrl',  function($rootScope, $scope,$activityIndicator,toaster,NegozyMessager) {
            $activityIndicator.startAnimating();
            $rootScope.sidebar_ready = true;
            $scope.messages = [];
            /*
            NegozyMessager.hello(1).success(function(r) {
                 $scope.data.text = r.data;
                 $scope.user_id = r.user_id;
                 $scope.shop_id = r.shop_id;
                 $activityIndicator.stopAnimating();
                 toaster.pop('success','','messaggio da' +$scope.user_id);
                 toaster.pop('error','','messaggio '+$scope.shop_id);
            });*/
            NegozyMessager.getAllMessages().success(function(r) {
                $activityIndicator.stopAnimating();
                $scope.messages = r.data;
                //toaster.pop('success','','Hai ' +r.data.length +" messaggi");

            });
            $scope.selected = function() {
                return NegozyMessager.selected($scope.messages, $scope.allSelected);
            }
            $scope.trashSelected = function() {
                NegozyMessager.trashSelected($scope.messages);
            }
            $scope.selectAll = function() {
                $scope.allSelected = true;
                NegozyMessager.selectAll($scope.messages);
            }
        })
        .controller('NegozyMessengerSidebarCtrl',  function($rootScope, $scope,$activityIndicator,toaster,NegozyMessager, $location) {
            $rootScope.countNewMessages = 0;
            $scope.view = $location.path();
            if($rootScope.sidebar_ready) {
                NegozyMessager.countNewMessages().success(function(r) {
                    if(r.data.success) {
                        $rootScope.countNewMessages = r.data.count;
                    }
                });
            }
        })
        .controller('NegozyMessengerSendCtrl',  function($rootScope, $scope,$activityIndicator,toaster,NegozyMessager) {
            $activityIndicator.startAnimating();
            $rootScope.sidebar_ready = true;
            $scope.messages = [];
            NegozyMessager.getAllSendMessages().success(function(r) {
                if(r.data.success) {
                    $scope.messages = r.data.messages;
                    $activityIndicator.stopAnimating();
                    //toaster.pop('success','','Hai ' +r.data.length +" messaggi");
                }
            });
            $scope.selected = function() {
                return NegozyMessager.selected($scope.messages, $scope.allSelected);
            }
            $scope.trashSelected = function() {
                NegozyMessager.trashSelected($scope.messages);
            }
            $scope.selectAll = function() {
                $scope.allSelected = true;
                NegozyMessager.selectAll($scope.messages);
            }
        })
        .controller('NegozyMessengerTrashCtrl',  function($rootScope, $scope,$activityIndicator,toaster,NegozyMessager) {
            $activityIndicator.startAnimating();
            $rootScope.sidebar_ready = true;
            $scope.messages = [];
            NegozyMessager.getAllTrashMessages().success(function(r) {
                if(r.data.success) {
                    $scope.messages = r.data.messages;
                    $activityIndicator.stopAnimating();
                    //toaster.pop('success','','Hai ' +r.data.length +" messaggi");
                }
            });
            $scope.selectAll = function() {
                $scope.allSelected = true;
                NegozyMessager.selectAll($scope.messages);
            }
            $scope.selected = function() {
                return NegozyMessager.selected($scope.messages, $scope.allSelected);
            }
            $scope.removeSelected = function() {
                NegozyMessager.removeSelected($scope.messages);
            }
        })
        .controller('NegozyMessengerReadCtrl',  function($rootScope, $scope,$routeParams,$activityIndicator,toaster,NegozyMessager,$location) {
            $activityIndicator.startAnimating();
            $rootScope.sidebar_ready = false;
            $scope.reply = {};
            $scope.message = {};
            NegozyMessager.readMessage($routeParams.message_id).success(function(r) {
                if(r.data.success) {
                    $rootScope.countNewMessages = r.data.countNewMessages;
                    $scope.owner = r.data.owner;
                    $scope.message = r.data.message;
                    $scope.sender = r.data.sender;
                    $scope.receiver = r.data.receiver;
                    $scope.replies_messages = r.data.replies_messages;
                    //$rootScope.sidebar_ready = true;
                    $activityIndicator.stopAnimating();
                }
            });

            $scope.trash = function(message_id) {
                $activityIndicator.startAnimating();
                NegozyMessager.trashMessage(message_id).success(function(r) {
                    if(r.data.success) {
                        $location.path('/app/negozy_messenger/trash');
                    }
                });
            }
            $scope.remove = function() {
                $activityIndicator.startAnimating();
                NegozyMessager.removeMessage($routeParams.message_id).success(function(r) {
                    if(r.data.success) {
                        $activityIndicator.stopAnimating();
                        $location.path('/app/negozy_messenger/trash');
                    }
                });
            }

            $scope.reply_to = function() {
                if($scope.owner) {
                    $scope.reply.receiver_to = $scope.receiver;
                } else {
                    $scope.reply.receiver_to = $scope.sender;
                }
                $scope.reply.message_to = $scope.message.id;
                $scope.reply.status = true;
            }

            $scope.sendMessage = function() {
                var receivers = [];
                receivers.push($scope.reply.receiver_to.id);

                NegozyMessager.sendMessage($scope.reply.message, receivers, $scope.reply.message_to).success(function(r) {
                    if(r.data.success) {
                        toaster.pop('success','',r.data.message);
                        $location.path('/app/negozy_messenger/');
                    } else {
                        toaster.pop('error','',r.data.message);
                    }
                });
            }

            $scope.cancelReply = function() {
                $scope.reply = {};
                $scope.reply.status = false;
            }

        })
        .controller('NegozyMessengerComposeCtrl',  function($rootScope, $scope,$activityIndicator,toaster,NegozyMessager,$location) {
            $rootScope.sidebar_ready = true;
            $scope.message = {};
            $scope.users = [];
            $scope.types = [
                {id:"broadcast","name":"Tutti gli utenti"},
                {id:"with_shop","name":"Utenti con shop"},
                {id:"without_shop","name":"Utenti senza shop"},
                {id:"user_list","name":"Utenti definiti"}
            ];

            $scope.loadUser = function(q){
                if(q) {
                    NegozyMessager.loadUser(q,$scope.message.receivers).success(function(r){
                        $scope.users = r.users;
                    })
                }
            }

            $scope.sendMessage = function() {
                NegozyMessager.sendMessage($scope.message).success(function(r) {
                    if(r.data.success) {
                        toaster.pop('success','',r.data.message);
                        $location.path('/app/negozy_messenger/send');
                    } else {
                        toaster.pop('error','',r.data.message);
                    }
                });
            }
        })
}).call(this);
