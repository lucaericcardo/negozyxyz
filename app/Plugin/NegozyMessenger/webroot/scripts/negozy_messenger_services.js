(function() {
    'use strict';
    angular.module('plugin.NegozyMessenger.services', [])
        .factory('NegozyMessager',function($http, $location,$activityIndicator,toaster) {
            return {
                getAllMessages : function(page) {
                    page = (page) ? page : 1;
                    return $http.post("/negozy_messenger/services/allMessages.json",{page:page});
                },
                loadFriends : function() {
                    return $http.get("/negozy_messenger/services/loadFriends.json");
                },
                loadUser : function(query,users) {
                    return $http.post('/negozy_messenger/services/load_user.json',{query:query,users:users});
                },
                sendMessage : function(message) {
                    return $http.post("/negozy_messenger/services/sendMessage.json",{message:message});
                },
                countNewMessages : function() {
                    return $http.get("/negozy_messenger/services/countNewMessages.json");
                },
                readMessage : function(message_id) {
                    return $http.post("/negozy_messenger/services/readMessage.json", {message_id:message_id});
                },
                getAllSendMessages : function() {
                    return $http.post("/negozy_messenger/services/getAllSendMessages.json");
                },
                getAllTrashMessages : function() {
                    return $http.post("/negozy_messenger/services/getAllTrashMessages.json");
                },
                trashMessage : function(message_id) {
                    return $http.post("/negozy_messenger/services/trashMessage.json", {message_id:message_id});
                },
                removeMessage : function(message_id) {
                    return $http.post("/negozy_messenger/services/removeMessage.json", {message_id:message_id});
                },
                selected : function(messages, allSelected) {
                    var selectOne = false;
                    for(var k in messages) {
                        if(messages[k].selected) {
                            selectOne = true;
                            break;
                        }
                    }
                    return allSelected || selectOne;
                },
                trashSelected : function(messages) {
                    $activityIndicator.startAnimating();
                    var trash = [];
                    for(var k in messages) {
                        if(messages[k].selected) {
                            trash.push(messages[k].id);
                        }
                    }
                    this.trashMessage(trash).success(function(r) {
                        if(r.data.success) {
                            $location.path('/app/negozy_messenger/trash');
                        } else {
                            $activityIndicator.stopAnimating();
                            toaster.pop('error','','Impossibile spostare i messaggi nel cestino');
                        }
                    });
                },
                selectAll : function(messages) {
                    for(var k in messages) {
                        messages[k].selected = true;
                    }
                },
                removeSelected : function(messages) {
                    $activityIndicator.startAnimating();
                    var trash = [];
                    for(var k in messages) {
                        if(messages[k].selected) {
                            trash.push(messages[k].id);
                        }
                    }
                    this.removeMessage(trash).success(function(r) {
                        if(r.data.success) {
                            $location.path('/app/negozy_messenger/trash');
                        } else {
                            $activityIndicator.stopAnimating();
                            toaster.pop('error','','Impossibile eliminarer i messaggi dal cestino');
                        }
                    });
                }

            }
        });

}).call(this);
