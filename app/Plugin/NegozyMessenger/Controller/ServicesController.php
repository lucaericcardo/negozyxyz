<?php
class ServicesController extends NegozyMessengerAppController {
    public $components = array('Mandrillapp');

    public function allMessages() {
        $this->response->compress();
        $this->loadModel("NegozyMessenger.Message");
        $this->loadModel("User");
        $messages = $this->Message->find("all",array(
            "conditions" => array(
                "Message.to_id" => $this->Auth->user("id"),
                "Message.removed_by_to" => FALSE,
                "Message.trashed_by_to" => FALSE,
                "Message.reply_to" => NULL,
                "Message.deleted" => false,
            ),
            "fields" => array(
                "Message.id",
                "Message.message",
                "Message.from_id",
                "Message.to_id",
                "Message.timestamp",
                'Message.readed',
            ),
            "recursive" => -1,
            'order' => array('Message.created DESC'),
        ));
        $return = array();
        foreach($messages as $message) {
            $return[] = array(
                "id" => $message['Message']['id'],
                "text" => String::truncate($message['Message']['message'], 50, array('ellipsis' => '...')),
                "user" => $this->getUser($message['Message']['from_id']),
                "date_message" => $message['Message']['timestamp'],
                "new" => ($message['Message']['readed']) ? false : true
            );
        }
        $this->set(array(
            "data" => $return,
            "_serialize"=>array("data")
        ));
    }

    public function load_user(){
        $this->response->compress();
        $this->loadModel("User");
        $return = array();
        if($this->request->is("post")) {
            $params = explode(" ",$this->request->data["query"]);
            $conditions_user = array();
            $conditions_shop = array();
            $conditions_user["UPPER(first_name) LIKE"] = "%%";
            $conditions_user["UPPER(last_name) LIKE"] = "%%";
            foreach($params as $param) {
                $param = strtoupper($param);
                $conditions_user["UPPER(first_name) LIKE"] = "%".$param."%";
                $conditions_user["UPPER(last_name) LIKE"] = "%".$param."%";
            }

            $users = $this->User->find("all",array(
                "recursive" => -1,
                "fields" => array(
                    "id",
                    "first_name",
                    "last_name",
                    "fullname",
                    "email"
                ),
                "conditions" => array(
                    "OR" => $conditions_user,
                    "NOT" => array(
                        "id" => Hash::extract($this->request->data["users"], '{n}.id')
                    )
                ),
                "contain" => array(
                    "Shop" => array(
                        "fields" => array(
                            "name"
                        )
                    )
                ),
                "limit" => 10
            ));
            foreach($users as $user) {
                $shop = reset($user['Shop']);
                $name = $user['User']['first_name']." ".$user['User']['last_name'];
                $name .= (isset($shop['name'])) ? ' ('.$shop['name'].')' : '';
                $return[] = array(
                    "name"  => $name,
                    "id"    => $user['User']['id'],
                    "fullname" => $user['User']['fullname'],
                    "email" => $user['User']['email']
                );
            }
        }
        $this->set(array(
            'users' => $return,
            '_serialize' => array('users')
        ));
    }

    private function loadUserWithShop(){
        return $this->User->find("all",array(
            "fields" => array(
                "User.fullname",
                "User.email",
                "User.id",
            ),
            "joins" => array(
                array(
                    'table' => 'shops',
                    'alias' => 'Shop',
                    'type' => 'RIGHT',
                    'conditions'=> array('Shop.user_id = User.id')
                ),
            ),
            "recursive" => -1
        ));
    }

    private function loadUserWithoutShop(){
        return $this->User->find("all",array(
            "fields" => array(
                "User.fullname",
                "User.email",
                "User.id",
                "Shop.id"
            ),
            "joins" => array(
                array(
                    'table' => 'shops',
                    'alias' => 'Shop',
                    'type' => 'LEFT OUTER',
                    'conditions'=> array(
                        'Shop.user_id = User.id',
                    )
                ),
            ),
            "conditions" => array(
                'Shop.user_id' => NULL
            ),
            "recursive" => -1
        ));
    }

    private function loadrAllUser(){
        return $this->User->find("all",array(
            "fields" => array(
                "User.id",
                "User.fullname",
                "User.email"
            ),
            "recursive" => -1
        ));
    }

    public function sendMessage(){
        $this->response->compress();
        $data['success'] = false;
        $data['message'] = __("Non è stato possibile inviare il messaggio");
        $this->loadModel("NegozyMessenger.Message");
        $this->loadModel("User");
        if($this->request->is("post")) {

            $type = isset($this->request->data['message']['type']) ? $this->request->data['message']['type'] : NULL;
            $text = isset($this->request->data['message']['text']) ? $this->request->data['message']['text'] : NULL;
            $replyTo = NULL;

            $users = array();
            if($type == 'with_shop') {
                $users = Set::Extract("/User/.",$this->loadUserWithShop());
            } else if($type == 'without_shop') {
                $users = Set::Extract("/User/.",$this->loadUserWithoutShop());
            } else if($type == 'broadcast') {
                $users = Set::Extract("/User/.",$this->loadrAllUser());
            } else if($type == 'user_list') {
                $users = isset($this->request->data['message']['receivers']) ? $this->request->data['message']['receivers'] : NULL;
            }


            if(isset($users) && isset($text)) {
                foreach ($users as $user) {
                    $newMessages = array(
                        "id" => NULL,
                        "from_id" => $this->Auth->user("id"),
                        "to_id" => $user['id'],
                        "message" => $text,
                        "reply_to" => $replyTo
                    );

                    if($this->Message->save($newMessages)) {
                        $data['success'] = true;
                        $data['message'] = __("Messaggio inviato con successo");

                        $from_full_name = $this->Auth->user("first_name")." ".$this->Auth->user("last_name");
                        //$this->Mandrillapp->negozy_messenger_send_message($from_full_name,$user['fullname'],$user['email'],$text);
                    }
                }
            }
        }
        $this->set(array(
            "data" => $data,
            "_serialize"=>array("data")
        ));
    }

    public function countNewMessages() {
        $this->response->compress();
        $data['success'] = false;

        if($this->request->is("get")) {
            $this->loadModel("NegozyMessenger.Message");
            $data['success'] = true;
            $data['count'] = $this->_countNewMessages();
        }

        $this->set(array(
            "data" => $data,
            "_serialize"=>array("data")
        ));
    }

    private function _countNewMessages() {
        $options = array(
            'conditions' => array(
                'Message.readed' => NULL,
                "Message.to_id" => $this->Auth->user("id"),
                "Message.deleted" => false
            ),

            'recursive' => -1
        );
        return $this->Message->find('count', $options);
    }

    private function permissionToMessage($message_id) {
        $options = array(
            'conditions' => array(
                "OR" => array(
                    "Message.from_id" => $this->Auth->user("id"),
                    "Message.to_id" => $this->Auth->user("id")
                ),
                'Message.id' => $message_id,
            ),
            'recursive' => -1
        );
        return $this->Message->find('count', $options);
    }

    public function readMessage() {
        $this->response->compress();
        $data['success'] = false;
        $data['message'] = null;
        $data['owner'] = false;
        $this->loadModel("NegozyMessenger.Message");
        $this->loadModel("User");
        if($this->request->is("post") && isset($this->request->data['message_id'])) {
            if($this->permissionToMessage($this->request->data['message_id'])) {
                if($this->touchMessageAsReaded($this->request->data['message_id'])) {
                    $data['success'] = true;
                    $data['message'] = $this->getMessage($this->request->data['message_id']);
                    if($data['message']['from_id'] == $this->Auth->user('id')) {
                        $data['owner'] = true;
                        $data['sender'] = array("id" => $this->Auth->user('id'), "fullname" => __('me'));
                        $data['receiver'] = array("id" => $data['message']['to_id'], "fullname" => $this->getUser($data['message']['to_id']));
                    } else if($data['message']['to_id'] == $this->Auth->user('id')) {
                        $data['receiver'] = array("id" => $this->Auth->user('id'), "fullname" => __('me'));
                        $data['sender'] = array("id" => $data['message']['from_id'], "fullname" => $this->getUser($data['message']['from_id']));
                    }
                    $data['countNewMessages'] = $this->_countNewMessages();
                }
            } else {
                $this->permissionViolation();
            }
        }
        $this->set(array(
            "data" => $data,
            "_serialize"=>array("data")
        ));
    }

    private function util_FlushMessage($message) {
        unset($message['from_id']);
        unset($message['to_id']);
        unset($message['trashed_by_to']);
        unset($message['trashed_by_from']);
        unset($message['removed_by_to']);
        unset($message['removed_by_from']);
        return $message;
    }

    private function permissionViolation() {
        $this->response->statusCode(401);
        $this->response->header(array('X-extra-redirect' => '#/app/negozy_messenger'));
        $this->response->send();
        $this->_stop();
    }
    private function touchMessageAsReaded($message_id) {
        $this->Message->id = $message_id;
        return $this->Message->saveField('readed', date("Y-m-d H:i:s"));
    }

    private function getUser($user_id) {
        $options = array(
            'conditions' => array(
                'User.id' => $user_id,
            ),
            'fields' => array(
                "User.fullname",
            ),
            'recursive' => -1
        );
        if($t = $this->User->find('first', $options)) {
            return $t['User']['fullname'];
        }
        return false;
    }

    private function getMessage($message_id) {
        $options = array(
            'conditions' => array(
                'Message.id' => $message_id,
                'Message.deleted' => false
            ),
            'fields' => array(
                "Message.id",
                "Message.message",
                "Message.from_id",
                "Message.to_id",
                "Message.timestamp"
            ),
            'recursive' => -1
        );
        if($t = $this->Message->find('first', $options)) {
            return $t['Message'];
        }
        return false;
    }

    public function getAllSendMessages() {
        $this->response->compress();
        $data['success'] = false;
        $data['messages'] = null;
        $this->loadModel("NegozyMessenger.Message");
        $this->loadModel("User");
        if($this->request->is("post")) {
            $options = array(
                "conditions" => array(
                    "Message.from_id" => $this->Auth->user("id"),
                    "Message.trashed_by_from" => FALSE,
                    "Message.removed_by_from" => FALSE,
                    "Message.deleted" => false,
                ),
                "fields" => array(
                    "Message.id",
                    "Message.message",
                    "Message.from_id",
                    "Message.to_id",
                    "Message.timestamp",
                    "Message.trashed_by_to",
                    "Message.removed_by_to",
                    "Message.trashed_by_from",
                    "Message.removed_by_from",
                ),
                "recursive" => -1,
                'order' => array('Message.created DESC'),
            );

            $messages = $this->Message->find('all', $options);
            $return = array();
            foreach($messages as $message) {
                $return[] = array(
                    "id" => $message['Message']['id'],
                    "text" => String::truncate($message['Message']['message'], 50, array('ellipsis' => '...')),
                    "user" => $textUser = $this->getUser($message['Message']['to_id']),
                    "date_message" => $message['Message']['timestamp'],
                );
            }
            $data['messages'] = $return;
            $data['success'] = true;
        }
        $this->set(array(
            "data" => $data,
            "_serialize"=>array("data")
        ));
    }

    public function getAllTrashMessages() {
        $this->response->compress();
        $data['success'] = false;
        $data['messages'] = null;
        $this->loadModel("NegozyMessenger.Message");
        $this->loadModel("User");
        if($this->request->is("post")) {
            $options = array(
                "conditions" => array(
                    "OR" => array(
                        "Message.from_id" => $this->Auth->user("id"),
                        "Message.to_id" => $this->Auth->user("id")
                    ),
                    "Message.reply_to" => NULL,
                    "Message.deleted" => false,
                ),
                "fields" => array(
                    "Message.id",
                    "Message.message",
                    "Message.to_id",
                    "Message.from_id",
                    "Message.timestamp",
                    "Message.trashed_by_to",
                    "Message.trashed_by_from",
                    "Message.removed_by_to",
                    "Message.removed_by_from"
                ),
                "recursive" => -1,
                'order' => array('Message.created DESC'),
            );

            $messages = $this->Message->find('all', $options);
            $return = array();
            foreach($messages as $message) {
                $messageToView = false;
                if($message['Message']['to_id'] == $this->Auth->user("id") && $message['Message']['trashed_by_to'] && !$message['Message']['removed_by_to']) {
                    //$textUser = $this->getUser($message['Message']['from_id']).', '.__('me');
                    $textUser = $this->getUser($message['Message']['from_id']);
                    $messageToView = true;
                } else if($message['Message']['from_id'] == $this->Auth->user("id") && $message['Message']['trashed_by_from'] && !$message['Message']['removed_by_from']) {
                    $textUser = $this->getUser($message['Message']['to_id']);
                    $messageToView = true;
                }
                if($messageToView) {
                    $return[] = array(
                        "id" => $message['Message']['id'],
                        "text" => String::truncate($message['Message']['message'], 50, array('ellipsis' => '...')),
                        "user" => $textUser,
                        "date_message" => $message['Message']['timestamp'],
                    );
                }
            }
            $data['messages'] = $return;
            $data['success'] = true;
        }
        $this->set(array(
            "data" => $data,
            "_serialize"=>array("data")
        ));
    }

    public function trashMessage() {
        $this->response->compress();
        $data['success'] = false;
        if($this->request->is("post") && isset($this->request->data['message_id'])) {
            $this->loadModel("NegozyMessenger.Message");
            if(!is_array($this->request->data['message_id'])) {
                $ids_to_trash = array($this->request->data['message_id']);
            } else {
                $ids_to_trash = $this->request->data['message_id'];
            }
            foreach($ids_to_trash as $id_to_trash) {
                if ($this->permissionToMessage($id_to_trash)) {
                    $message = $this->getMessage($id_to_trash);
                    $this->Message->id = $id_to_trash;
                    if ($message['from_id'] == $this->Auth->user('id')) {
                        $data['success'] = $this->Message->saveField('trashed_by_from', true);
                    } else if ($message['to_id'] == $this->Auth->user('id')) {
                        $data['success'] = $this->Message->saveField('trashed_by_to', true);
                    }
                } else {
                    $this->$this->permissionViolation();
                }
            }
        }
        $this->set(array(
            "data" => $data,
            "_serialize"=>array("data")
        ));
    }

    public function removeMessage() {
        $this->response->compress();
        $data['success'] = false;
        if($this->request->is("post") && isset($this->request->data['message_id'])) {
            $this->loadModel("NegozyMessenger.Message");

            /* Uguale anche per i trash */
            if(!is_array($this->request->data['message_id'])) {
                $ids_to_trash = array($this->request->data['message_id']);
            } else {
                $ids_to_trash = $this->request->data['message_id'];
            }
            foreach($ids_to_trash as $id_to_trash) {
                if ($this->permissionToMessage($id_to_trash)) {
                    $message = $this->getMessage($id_to_trash);
                    $this->Message->id = $id_to_trash;
                    if ($message['from_id'] == $this->Auth->user('id')) {
                        $data['success'] = $this->Message->saveField('removed_by_from', true);
                    } else if ($message['to_id'] == $this->Auth->user('id')) {
                        $data['success'] = $this->Message->saveField('removed_by_to', true);
                    }
                } else {
                    $this->$this->permissionViolation();
                }
            }
        }
        $this->set(array(
            "data" => $data,
            "_serialize"=>array("data")
        ));
    }
}