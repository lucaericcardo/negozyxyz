<?php
class MessagesController extends NegozyMessengerAppController {

    public function application_index(){}
    public function application_compose(){
        $this->loadModel('User');
        $this->set('is_admin', $this->User->find('count', array('conditions'=>array('id'=>$this->Auth->user('id'), 'is_admin'=>true), 'recursive'=>-1)));
    }
    public function application_send(){}
    public function application_trash() {}
    public function application_read_trash_message() {}
}