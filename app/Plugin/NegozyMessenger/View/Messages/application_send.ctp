<div class="lapagina" data-ng-controller="NegozyMessengerSendCtrl">
    <nav class="navbar navbar-default navbar-static-top" role="navigation">
        <div class="navbar-header">
            <ul class="nav navbar-nav">
                <li>
                    <ol class="breadcrumb">
                        <li class="active"><a><?php echo __("Negozy Messenger"); ?></a></li>
                    </ol>
                </li>
            </ul>
            <!--<button class="btn btn-danger"><?php echo __("Componi"); ?></button>-->
        </div>
    </nav>
    <div class="page page-table">
        <div class="row">
            <div class="col-sm-3">
                <?php echo $this->element('sidebar'); ?>
            </div>
            <div class="col-sm-9">
                <section class="panel panel-default mail-container">
                    <div class="panel-heading"><strong><span class="glyphicon glyphicon-th"></span> <?php echo __('Inviati'); ?></strong></div>
                    <div class="mail-options" data-ng-show="messages.length">
                        <label class="ui-checkbox"><input type="checkbox" data-ng-click="selectAll()"><span></span></label>
                        <span data-ng-hide="selected()"><?php echo __('Seleziona più messaggi per effettuare operazioni multiple'); ?>:</span>
                        <span data-ng-show="selected()">
                            <span><?php echo __('Se selezionati'); ?>: </span>
                            <button
                                class="btn btn-default"
                                confirm-callback="trashSelected()"
                                confirm-message="<?php echo __('Sei sicuro di voler spostare gli elementi selezionati nel cestino?'); ?>"
                                confirm-modal>
                                <i class="fa fa-trash-o"></i>
                            </button>
                        </span>
                    </div>
                    <table class="table table-hover">
                        <tbody>
                        <tr data-ng-show="!messages.length"><td colspan="4"><?php echo __('Nessun messaggio presente'); ?></td></tr>
                        <tr class="mail-unread" data-ng-show="messages.length" data-ng-repeat="message in messages">
                            <td>
                                <a href="javascript:;">
                                    <label class="ui-checkbox"><input type="checkbox" data-ng-model="message.selected" data-ng-checked="allSelected || message.selected"><span></span></label>
                                </a>
                            </td>
                            <td><a href="#/app/negozy_messenger/messages/{{message.id}}"><?php echo __('A'); ?>: {{message.user}} <span data-ng-show="message.count>1">({{message.count}})</span></a></td>
                            <td><a href="#/app/negozy_messenger/messages/{{message.id}}">{{message.text}}</a></td>
                            <td><a href="#/app/negozy_messenger/messages/{{message.id}}">{{message.date_message | date:'dd/MM/yyyy  H:mm'}}</a></td>
                        </tr>
                        </tbody></table>
                </section>
            </div>
        </div>
    </div>
</div>
