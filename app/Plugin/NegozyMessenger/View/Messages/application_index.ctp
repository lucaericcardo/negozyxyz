<div class="lapagina" data-ng-controller="NegozyMessengerReadCtrl">
    <nav class="navbar navbar-default navbar-static-top" role="navigation">
        <div class="navbar-header">
            <ul class="nav navbar-nav">
                <li>
                    <ol class="breadcrumb">
                        <li class="active"><a><?php echo __("Negozy Messenger"); ?></a></li>
                    </ol>
                </li>
            </ul>
            <!--<button class="btn btn-danger"><?php echo __("Componi"); ?></button>-->
        </div>
    </nav>
    <div class="page page-table">
        <div class="row">
            <div class="col-sm-3">
                <?php echo $this->element('sidebar'); ?>
            </div>
            <div class="col-sm-9">
                <!-- Leggi messaggio -->
                <section class="panel panel-default mail-container">
                    <div class="panel-heading"><strong><span class="glyphicon glyphicon-th"></span> <?php echo __('Leggi messaggio'); ?></strong></div>
                    <div class="panel-body">
                        <div class="mail-info">
                            <div class="row">
                                <div class="col-md-8">
                                    <strong>{{sender.fullname}}</strong> <?php echo __('a'); ?>
                                    <strong>{{receiver.fullname}}</strong>
                                </div>
                                <div class="col-md-4">
                                    <div class="pull-right">
                                        {{message.timestamp | date:'dd/MM/yyyy  H:mm'}}
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="mail-content" compile="message.message">
                        </div>
                        <div class="mail-actions">
                            <a class="btn btn-sm btn-primary pull-right" href="javascript:;" data-ng-click="reply_to()"><!-- data-ng-hide="owner" -->
                                <?php echo __('Scrivi');?> &nbsp;<i class="fa fa-pencil-square-o"></i>
                            </a>
                            <a
                                class="btn btn-sm btn-default pull-right"
                                href="javascript:;"
                                title="<?php echo __('Cancella la conversazione'); ?>"
                                confirm-modal
                                confirm-message="<?php echo __("Sei sicuro di volere spostare la conversazione nel cestino?")?>"
                                confirm-callback="trash(message.id)"
                                ><i class="fa fa-trash-o"></i></a>
                        </div>
                    </div>
                </section>
                <!-- /Leggi messaggio -->

                <!-- Rispondi -->
                <section class="panel panel-default mail-container mail-compose" data-ng-show="reply.status">
                    <div class="panel-heading"><strong><span class="glyphicon glyphicon-th"></span> <?php echo __("Rispondi"); ?></strong><button data-ng-click="cancelReply()" type="button" class="close" aria-label="Close"><span aria-hidden="true">&times;</span></button></div>
                    <div class="panel-body">
                        <form class="form-horizontal">
                            <div class="form-group">
                                <label for="mail_to" class="col-xs-2"><?php echo __("Destinatario"); ?></label>
                                <div class="col-xs-10">
                                    <input type="text" data-ng-model="reply.receiver_to.fullname" class="form-control" disabled>
                                </div>
                            </div>
                            <div class="form-group">
                                <label for="mail_to" class="col-xs-2"><?php echo __("Messaggio"); ?></label>
                                <div class="col-xs-10">
                                    <textarea data-ng-model="reply.message" class="form-control" rows="4"></textarea>
                                </div>
                            </div>
                        </form>
                        <div class="mail-actions">
                            <div class="btn btn-sm btn-primary pull-right" data-ng-click="sendMessage();"><?php echo __("Invia"); ?></div>
                        </div>
                    </div>
                </section>
                <!-- /Rispondi -->
            </div>
        </div>
    </div>
</div>

















