<div class="lapagina" data-ng-controller="NegozyMessengerReadCtrl">
    <nav class="navbar navbar-default navbar-static-top" role="navigation">
        <div class="navbar-header">
            <ul class="nav navbar-nav">
                <li>
                    <ol class="breadcrumb">
                        <li class="active"><a><?php echo __("Negozy Messenger"); ?></a></li>
                    </ol>
                </li>
            </ul>
            <!--<button class="btn btn-danger"><?php echo __("Componi"); ?></button>-->
        </div>
    </nav>
    <div class="page page-table">
        <div class="row">
            <div class="col-sm-3">
                <?php echo $this->element('sidebar'); ?>
            </div>
            <div class="col-sm-9">
                <section class="panel panel-default mail-container">
                    <div class="panel-heading"><strong><span class="glyphicon glyphicon-th"></span> <?php echo __('Leggi messaggio'); ?></strong></div>
                    <div class="panel-body">
                        <div class="mail-info">
                            <div class="row">
                                <div class="col-md-8">
                                    <strong>{{sender.fullname}}</strong> <?php echo __('a'); ?>
                                    <strong>{{receiver.fullname}}</strong>
                                </div>
                                <div class="col-md-4">
                                    <div class="pull-right">
                                        {{message.timestamp | date:'dd/MM/yyyy  H:mm'}}
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="mail-content">
                            {{message.message}}
                        </div>
                        <div class="mail-actions">
                            <a
                                class="btn btn-sm btn-danger pull-right"
                                href="javascript:;"
                                title="Rimuovi l'intera conversazione"
                                confirm-modal
                                confirm-message="<?php echo __("Sei sicuro di volere eliminare definitivamente la conversazione?")?>"
                                confirm-callback="remove()"><i class="fa fa-trash-o"></i></a>
                        </div>
                    </div>
                </section>

                <!-- Messaggi di risposta -->
                <section class="panel panel-default mail-container" data-ng-repeat="reply_mess in replies_messages">
                    <div class="panel-body">
                        <div class="mail-info">
                            <div class="row">
                                <div class="col-md-8">
                                    <strong>{{reply_mess.sender.fullname}}</strong> <?php echo __('a'); ?>
                                    <strong>{{reply_mess.receiver.fullname}}</strong>
                                </div>
                                <div class="col-md-4">
                                    <div class="pull-right">
                                        {{reply_mess.message.timestamp | date:'dd/MM/yyyy  H:mm'}}
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="mail-content">
                            {{reply_mess.message.message}}
                        </div>
                    </div>
                </section>
                <!-- / Messaggi di risposta -->
            </div>
        </div>
    </div>
</div>

















