<div class="lapagina" data-ng-controller="NegozyMessengerComposeCtrl">
    <nav class="navbar navbar-default navbar-static-top" role="navigation">
        <div class="navbar-header">
            <ul class="nav navbar-nav">
                <li>
                    <ol class="breadcrumb">
                        <li class="active"><a><?php echo __("Negozy Messenger"); ?></a></li>
                    </ol>
                </li>
            </ul>
            <!--<button class="btn btn-danger"><?php echo __("Componi"); ?></button>-->
        </div>
    </nav>
    <div class="page page-table">

        <div class="row">
            <div class="col-sm-3">
                <?php echo $this->element('sidebar'); ?>
            </div>
            <div class="col-sm-9">
                <section class="panel panel-default mail-container mail-compose">
                    <div class="panel-heading"><strong><span class="glyphicon glyphicon-th"></span><?php echo __("Componi messaggio"); ?></strong></div>
                    <div class="panel-body">
                        <form class="form-horizontal">
                            <?php
                            if($is_admin) {
                                echo
                                    '<div class="form-group">
                                        <label class="col-xs-2">'.__("Destinatari").'</label>
                                        <div class="col-xs-10">

                                            <label for="">Tipologia</label>
                                            <select data-ng-options="type.id as type.name for type in types" class="form-control" data-ng-model="message.type"></select>

                                            <div data-ng-if="message.type == \'user_list\'">
                                                <label for="mail_to">Utenti</label>
                                                <ui-select multiple data-ng-model="message.receivers" style="width: 100%;" theme="bootstrap">
                                                    <ui-select-match placeholder="'.__("Cerca destinatari").'">{{$item.name}}</ui-select-match>
                                                    <ui-select-choices  reset-search-input="true" refresh="loadUser($select.search)" refresh-delay="0" repeat="user in users | filter:$select.search">
                                                        <div ng-bind-html="user.name | highlight: $select.search"></div>
                                                    </ui-select-choices>
                                                </ui-select>
                                            </div>
                                        </div>
                                    </div>';
                            } else {
                                echo
                                    '<div class="form-group">
                                        <label class="col-xs-2">'.__("Destinatari").'</label>
                                        <div class="col-xs-10">
                                            <div data-ng-init="message.type = \'user_list\'">
                                                <ui-select multiple data-ng-model="message.receivers" style="width: 100%;" theme="bootstrap">
                                                    <ui-select-match placeholder="'.__("Cerca destinatari").'">{{$item.name}}</ui-select-match>
                                                    <ui-select-choices  reset-search-input="true" refresh="loadUser($select.search)" refresh-delay="0" repeat="user in users | filter:$select.search">
                                                        <div ng-bind-html="user.name | highlight: $select.search"></div>
                                                    </ui-select-choices>
                                                </ui-select>
                                            </div>
                                        </div>
                                    </div>';
                            }
                            ?>
                            <div class="form-group">
                                <label for="mail_to" class="col-xs-2"><?php echo __("Messaggio"); ?></label>
                                <div class="col-xs-10">
                                    <textarea data-ng-model="message.text" class="form-control" rows="4" placeholder="<?php echo __("Inserisci il messaggio"); ?>"></textarea>
                                </div>
                            </div>
                        </form>
                        <div class="mail-actions">
                            <div class="btn btn-sm btn-primary pull-right" data-ng-click="sendMessage();"><?php echo __("Invia"); ?></div>
                        </div>
                    </div>
                </section>
            </div>
    </div>
</div>