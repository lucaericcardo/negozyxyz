<section class="panel panel-default mail-categories" data-ng-controller="NegozyMessengerSidebarCtrl">
    <div class="panel-heading">
        <a class="btn btn-block btn-lg btn-primary" href="#/app/negozy_messenger/compose/"><?php echo __("Componi"); ?></a>
    </div>
    <ul class="list-group">
        <li class="list-group-item" data-ng-class="{'active': (view=='/app/negozy_messenger')}"><a href="#/app/negozy_messenger">
                <i class="fa fa-inbox"></i><?php echo __("Messaggi ricevuti"); ?>
                <span class="badge badge-info pull-right" data-ng-show="countNewMessages">{{countNewMessages}}</span>
            </a></li>
        <li class="list-group-item" data-ng-class="{'active': (view=='/app/negozy_messenger/send/')}"><a href="#/app/negozy_messenger/send">
                <i class="fa fa-envelope-o"></i><?php echo __("Messaggi inviati"); ?>
            </a></li>
        <li class="list-group-item" data-ng-class="{'active': (view=='/app/negozy_messenger/trash/')}"><a href="#/app/negozy_messenger/trash">
                <i class="fa fa-trash-o"></i><?php echo __("Cestino"); ?>
            </a></li>
    </ul>
</section>