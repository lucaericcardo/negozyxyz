<div class="lapagina" data-ng-controller="CronLogDashboardCtrl">
    <nav class="navbar navbar-default navbar-static-top" role="navigation">
        <div class="navbar-header">
            <ul class="nav navbar-nav">
                <li>
                    <ol class="breadcrumb">
                        <li class="active"><a><?php echo __("Cron Log"); ?></a></li>
                    </ol>
                </li>
            </ul>
        </div>
    </nav>
    <div class="page page-table">
        <div class="row">
            <div class="col-sm-2">
                <section class="panel panel-default mail-categories ng-scope">
                    <?php echo $this->element('sidebar'); ?>
                </section>
            </div>
            <div class="col-md-7">
                <div class="col-md-12">
                    <section class="panel panel-default">
                        <div class="panel-heading">
                            <div><strong><?php echo __("Utenti"); ?></strong></div>
                        </div>
                        <div class="panel-body">
                            <div class="row">
                                <div class="col-lg-2 col-xsm-6">
                                    <div class="panel mini-box">
                                        <span class="box-icon bg-success">
                                            <i class="fa fa-rocket"></i>
                                        </span>
                                        <div class="box-info">
                                            <p class="size-h2">{{stats.products}}</p>
                                            <p class="text-muted"><span data-i18n="Growth">Prodotti</span></p>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-lg-2 col-xsm-6">
                                    <div class="panel mini-box">
                                        <span class="box-icon bg-info">
                                            <i class="fa fa-users"></i>
                                        </span>
                                        <div class="box-info">
                                            <p class="size-h2">{{stats.users}}</p>
                                            <p class="text-muted"><span data-i18n="New users">Utenti</span></p>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-lg-2 col-xsm-6">
                                    <div class="panel mini-box">
                                        <span class="box-icon bg-danger">
                                            <i class="fa fa-shopping-cart"></i>
                                        </span>
                                        <div class="box-info">
                                            <p class="size-h2">{{stats.shops}}</p>
                                            <p class="text-muted"><span data-i18n="Sales">Shops</span></p>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-lg-2 col-xsm-6">
                                    <div class="panel mini-box">
                                        <span class="box-icon bg-danger">
                                            <i class="fa fa-shopping-cart"></i>
                                        </span>
                                        <div class="box-info">
                                            <p class="size-h2">{{stats.orders}}</p>
                                            <p class="text-muted"><span data-i18n="Sales">Ordini</span></p>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-lg-2 col-xsm-6">
                                    <div class="panel mini-box">
                                        <span class="box-icon bg-warning">
                                            <i class="fa fa-dollar"></i>
                                        </span>
                                        <div class="box-info">
                                            <p tooltip-placement="left" tooltip="Totale prodotti con prezzo minore di 10k " class="size-h2">{{stats.total_product_small | nrFormat}}</p>
                                            <p tooltip-placement="left" tooltip="Totale prodotti con prezzo maggiore di 10k " class="size-h2">{{stats.total_product_big | nrFormat}}</p>                                        </div>
                                    </div>
                                </div>
                                <div class="col-lg-2 col-xsm-6">
                                    <div class="panel mini-box">
                                        <span class="box-icon bg-danger">
                                            <i class="fa fa-dollar"></i>
                                        </span>
                                        <div class="box-info">
                                            <p class="size-h2">{{stats.orders_total | nrFormat}}</p>
                                            <p class="text-muted"><span data-i18n="Profit">Totale ordini</span></p>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </section>
                </div>

                <div class="col-md-12">
                    <section class="panel panel-default">
                        <div class="panel-heading">
                            <div class="pull-left"><strong><?php echo __("Utenti / Shops"); ?></strong></div>
                            <div class="pull-right"><input date-range-picker class="form-control date-picker" type="text" ng-model="date_user" options="opts"/></div>
                            <div class="clearfix"></div>
                        </div>
                        <div class="panel-body">
                            <area-chart dataChart="chartData"></area-chart>
                        </div>
                    </section>
                </div>
                <div class="col-md-12">
                    <section class="panel panel-default">
                        <div class="panel-heading">
                            <div class="pull-left"><strong><?php echo __("Sorgente utenti"); ?></strong></div>
                            <div class="pull-right"><input date-range-picker class="form-control date-picker" type="text" ng-model="date_user_source" options="opts"/></div>
                            <div class="clearfix"></div>
                        </div>
                        <div class="panel-body">
                            <area-chart dataChart="chartDataUserSource"></area-chart>
                        </div>
                    </section>
                </div>
                <div class="col-md-12">
                    <section class="panel panel-default">
                        <div class="panel-heading">
                            <div class="pull-left"><strong><?php echo __("Utenti attivi"); ?></strong></div>
                            <div class="pull-right"><input date-range-picker class="form-control date-picker" type="text" ng-model="date_user_active" options="opts"/></div>
                            <div class="clearfix"></div>
                        </div>
                        <div class="panel-body">
                            <area-chart  dataChart="chartDataUserActive"></area-chart>
                        </div>
                    </section>
                </div>
                <div class="col-md-12">
                    <section class="panel panel-default">
                        <div class="panel-heading">
                            <div class="pull-left"><strong><?php echo __("Azioni utenti"); ?></strong></div>
                            <div class="pull-right"><input date-range-picker class="form-control date-picker" type="text" ng-model="date_user_action" options="opts"/></div>
                            <div class="clearfix"></div>
                        </div>
                        <div class="panel-body">
                            <area-chart  dataChart="chartDataUserAction"></area-chart>
                        </div>
                    </section>
                </div>
            </div>
            <div class="col-md-3">
                <div class="col-md-12">
                    <section class="panel panel-default">
                        <div class="panel-heading"><strong><?php echo __("Tipologia di login"); ?></strong></div>
                        <div class="panel-body">
                            <pie-chart dataChart="chartDataUserType"></pie-chart>
                        </div>
                    </section>
                </div>
                <div class="col-md-12">
                    <section class="panel panel-default">
                        <div class="panel-heading"><strong><?php echo __("Utenti con shop"); ?></strong></div>
                        <div class="panel-body">
                            <pie-chart dataChart="chartDataUserShop"></pie-chart>
                        </div>
                    </section>
                </div>
            </div>
        </div>

    </div>
</div>
