<div class="lapagina" data-ng-controller="CronLogJobDetailsCtrl">
    <nav class="navbar navbar-default navbar-static-top" role="navigation">
        <div class="navbar-header">
            <ul class="nav navbar-nav">
                <li>
                    <ol class="breadcrumb">
                        <li class="active"><a><?php echo __("Cron Log"); ?></a></li>
                    </ol>
                </li>
            </ul>
        </div>
    </nav>
    <div class="page page-table">
        <div class="row">
            <div class="col-sm-2">
                <section class="panel panel-default mail-categories ng-scope">
                    <?php echo $this->element('sidebar'); ?>
                </section>
            </div>
            <div class="col-md-10">
                <div class="pull-right"><input date-range-picker class="form-control date-picker" type="text" ng-model="date_jobs_action" options="opts"/></div>
                <div class="col-md-12">
                    <div class="ui-timline-container">
                        <section data-ng-repeat="date in timeline_date" class="ui-timeline">
                                <article class="tl-item">
                                    <div class="tl-body">
                                        <div class="tl-entry">
                                            <div class="tl-caption">
                                                <a class="btn btn-primary btn-block" href="javascript:;">{{date}}</a>
                                            </div>
                                        </div>
                                    </div>
                                </article>
                                <article data-ng-repeat="v in timeline[date]" class="tl-item" data-ng-class-odd="'alt'">
                                    <div class="tl-body">
                                        <div class="tl-entry">
                                            <div class="tl-time">{{v.date}}</div>
                                            <div class="tl-icon square-icon sm" data-ng-class="v.color"><i class="{{v.icon}}"></i></div>
                                            <div class="tl-content">
                                                <h4 class="tl-tile text-primary">{{v.text}}</h4>
                                                <p>{{v.description}}</p>
                                            </div>
                                        </div>
                                    </div>
                                </article>
                        </section>
                    </div>
                </div>
            </div>

        </div>

    </div>
</div>
