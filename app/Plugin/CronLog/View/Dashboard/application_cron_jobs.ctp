<div class="lapagina" data-ng-controller="CronLogJobsCtrl">
    <nav class="navbar navbar-default navbar-static-top" role="navigation">
        <div class="navbar-header">
            <ul class="nav navbar-nav">
                <li>
                    <ol class="breadcrumb">
                        <li class="active"><a><?php echo __("Cron Log"); ?></a></li>
                    </ol>
                </li>
            </ul>
        </div>
    </nav>
    <div class="page page-table">
        <div class="row">
            <div class="col-sm-2">
                <section class="panel panel-default mail-categories ng-scope">
                    <?php echo $this->element('sidebar'); ?>
                </section>
            </div>
            <div class="col-md-10">
                <div class="col-md-12">
                    <section class="panel panel-default">
                        <div class="panel-heading">
                            <div class="pull-left"><strong><?php echo __("Jobs"); ?></strong></div>
                            <div class="pull-right"><input date-range-picker class="form-control date-picker" type="text" ng-model="date_jobs_action" options="opts"/></div>
                            <div class="clearfix"></div>
                        </div>
                        <div class="panel-body">
                            <area-chart dataChart="chartData"></area-chart>
                        </div>
                    </section>
                </div>
            </div>

        </div>

    </div>
</div>
