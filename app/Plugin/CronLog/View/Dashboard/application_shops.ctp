<div class="lapagina" data-ng-controller="CronLogShopCtrl">
    <nav class="navbar navbar-default navbar-static-top" role="navigation">
        <div class="navbar-header">
            <ul class="nav navbar-nav">
                <li>
                    <ol class="breadcrumb">
                        <li class="active"><a><?php echo __("Cron Log"); ?></a></li>
                    </ol>
                </li>
            </ul>
        </div>
    </nav>
    <div class="page page-table">
        <div class="row">
            <div class="col-sm-2">
                <section class="panel panel-default mail-categories ng-scope">
                    <?php echo $this->element('sidebar'); ?>
                </section>
            </div>
            <div class="col-md-10">
                <section class="panel panel-default table-dynamic">
                    <div class="table-responsive">
                        <table class="table table-bordered table-striped table-responsive">
                            <thead>
                            <tr>
                                <th>
                                    <div class="th">
                                        <?php echo __("Nome"); ?>
                                    </div>
                                </th>
                                <th>
                                    <div class="th">
                                        <?php echo __("Url"); ?>
                                    </div>
                                </th>
                                <th>
                                    <div class="th">
                                        <?php echo __("Utente"); ?>
                                    </div>
                                </th>
                            </tr>
                            </thead>
                            <tbody>
                            <tr data-ng-repeat="shop in shops">
                                <td>{{shop.name}}</td>
                                <td><a href="{{shop.url}}" target="_blank">{{shop.url}}</a></td>
                                <td>{{shop.user}}</td>
                            </tr>
                            </tbody>
                        </table>
                    </div>
                </section>
            </div>
        </div>

    </div>
</div>
