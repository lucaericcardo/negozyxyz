<?php
App::uses('CakeNumber', 'Utility');
class JobDataController extends CronLogAppController {

    public $components = array("RequestHandler","CronLog.DateRange");

    public function application_getJobData() {
        $return = array(
            "data"      => array(),
            "xkey"      => 'x',
            "ykeys"     => array("t","s","e"),
            "labels"    => array('Totali', 'Successo','Errore'),
            "smooth"    => false,
            "pointSize" => 0,
            "hideHover" => 'auto',
            "loaded"    => true,
            "lineColors"    => array("#E87E04","#4DAF7C","#C0392B")
        );
        if($this->request->is("post")) {
            $start_date = $this->request->data['start_date'];
            $end_date   = $this->request->data['end_date'];
        } else {
            $start_date = date("d-m-Y");
            $end_date   = date("d-m-Y", strtotime(date("Y-m-d") . "-1 month"));
        }
        $sqlDate = $this->DateRange->getDateFroSql($start_date,$end_date);
        $range = $this->DateRange->getRange($start_date,$end_date);
        foreach ( $range as $date ) {
            $date = $date->format ( 'Y-m-d' );
            $return['data'][$date] = array(
                'x' => $date,
                't' => 0,
                "s" => 0,
                "e" => 0
            );
        }
        $this->loadModel("CronLog");
        $dataJobsTotal = $this->CronLog->find("all",array(
            "recursive" => -1,
            "fields" => array(
                "COUNT(CronLog.id) AS count",
                "to_char(created , 'YYYY-MM-DD') AS date"
            ),
            "conditions" => array(
                "AND" => array(
                    "created >= " => $sqlDate['start_date'],
                    "created <= " => $sqlDate['end_date']
                )
            ),
            "group" => "date"
        ));
        $dataJobsSuccess = $this->CronLog->find("all",array(
            "recursive" => -1,
            "fields" => array(
                "COUNT(CronLog.id) AS count",
                "to_char(created , 'YYYY-MM-DD') AS date"
            ),
            "conditions" => array(
                "AND" => array(
                    "created >= " => $sqlDate['start_date'],
                    "created <= " => $sqlDate['end_date']
                ),
                "success" => TRUE
            ),
            "group" => "date"
        ));
        $dataJobsError = $this->CronLog->find("all",array(
            "recursive" => -1,
            "fields" => array(
                "COUNT(CronLog.id) AS count",
                "to_char(created , 'YYYY-MM-DD') AS date"
            ),
            "conditions" => array(
                "AND" => array(
                    "created >= " => $sqlDate['start_date'],
                    "created <= " => $sqlDate['end_date']
                ),
                "error" => TRUE
            ),
            "group" => "date"
        ));
        foreach($dataJobsTotal as $track) {
            $return['data'][$track[0]['date']]['t'] = $track[0]['count'];
        }
        foreach($dataJobsSuccess as $track) {
            $return['data'][$track[0]['date']]['s'] = $track[0]['count'];
        }
        foreach($dataJobsError as $track) {
            $return['data'][$track[0]['date']]['e'] = $track[0]['count'];
        }

        $return['data'] = array_values($return['data']);
        $this->set(array(
            'data' => $return,
            '_serialize' => array('data')
        ));
    }

    public function getJobDetails(){
        $this->response->compress();
        $this->loadModel("CronLog");
        if($this->request->is("post")) {
            $start_date = $this->request->data['start_date'];
            $end_date   = $this->request->data['end_date'];
        } else {
            $start_date = date("d-m-Y");
            $end_date   = date("d-m-Y", strtotime(date("Y-m-d") . "-7 days"));
        }
        $sqlDate = $this->DateRange->getDateFroSql($start_date,$end_date);
        $jobs = $this->CronLog->find("all",array(
            "recursive" => -1,
            "conditions" => array(
                "AND" => array(
                    "created >= " => $sqlDate['start_date'],
                    "created <= " => $sqlDate['end_date']
                )
            ),
            "order" => "created DESC"
        ));
        $return = array(
            'date'      => array(),
            'elements'  => array()
        );
        foreach($jobs as $job) {
            $date = explode(" ",$job['CronLog']['created']);
            $return['elements'][$date[0]][] = array(
                "description"   => $job['CronLog']['application']." esegue la funzione ".$job['CronLog']['function'],
                "text"          => $job['CronLog']['application'],
                "icon"          => ($job['CronLog']['success']) ? "fa fa-check" : "fa fa-close",
                "color"         => ($job['CronLog']['success']) ? "bg-success" : "bg-danger",
                "date"          => $job['CronLog']['created']
            );
        }


        $date = array_keys($return['elements']);
        $tmp_date = [];
        foreach($date as $key => $value) {
            $tmp_date[strtotime($value)] = $value;
        }
        krsort($tmp_date);
        $return['date'] = array_values($tmp_date);
        $this->set(array(
            'data' => $return,
            '_serialize' => array('data')
        ));
    }

}