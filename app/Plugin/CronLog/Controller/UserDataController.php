<?php
App::uses('CakeNumber', 'Utility');
class UserDataController extends CronLogAppController {

    public $components = array("RequestHandler","CronLog.DateRange");

    public function application_getUserData() {
        $return = array(
            "data"      => array(),
            "xkey"      => 'x',
            "ykeys"     => array("y","z"),
            "labels"    => array('Utenti', 'Negozi'),
            "smooth"    => false,
            "pointSize" => 0,
            "hideHover" => 'auto',
            "loaded"    => true,
            "lineColors"    => array("#95A5A6","#E87E04")
        );
        if($this->request->is("post")) {
            $start_date = $this->request->data['start_date'];
            $end_date   = $this->request->data['end_date'];
        } else {
            $start_date = date("d-m-Y");
            $end_date   = date("d-m-Y", strtotime(date("Y-m-d") . "-1 month"));
        }
        $sqlDate = $this->DateRange->getDateFroSql($start_date,$end_date);
        $range = $this->DateRange->getRange($start_date,$end_date);
        foreach ( $range as $date ) {
            $date = $date->format ( 'Y-m-d' );
            $return['data'][$date] = array(
                'y' => 0,
                'x' => $date,
                "z" => 0
            );
        }
        $this->loadModel("User");
        $this->loadModel("Shop");
        $dataUser = $this->User->find("all",array(
            "recursive" => -1,
            "fields" => array(
                "COUNT(User.id) AS count",
                "to_char(created , 'YYYY-MM-DD') AS date"
            ),
            "conditions" => array(
                "AND" => array(
                    "created >= " => $sqlDate['start_date'],
                    "created <= " => $sqlDate['end_date']
                )
            ),
            "group" => "date"
        ));
        $dataShop = $this->Shop->find("all",array(
            "recursive" => -1,
            "fields" => array(
                "COUNT(Shop.id) AS count",
                "to_char(created , 'YYYY-MM-DD') AS date"
            ),
            "conditions" => array(
                "AND" => array(
                    "created >= " => $sqlDate['start_date'],
                    "created <= " => $sqlDate['end_date']
                )
            ),
            "group" => "date"
        ));
        foreach($dataUser as $track) {
            $return['data'][$track[0]['date']]['y'] = $track[0]['count'];
        }
        foreach($dataShop as $track) {
            $return['data'][$track[0]['date']]['z'] = $track[0]['count'];
        }
        $return['data'] = array_values($return['data']);
        $this->set(array(
            'data' => $return,
            '_serialize' => array('data')
        ));
    }

    public function application_getUserDataSource() {
        $return = array(
            "data"      => array(),
            "xkey"      => 'x',
            "ykeys"     => array("y","z"),
            "labels"    => array('Landing page', 'Altro'),
            "smooth"    => false,
            "pointSize" => 0,
            "hideHover" => 'auto',
            "loaded"    => true,
            "lineColors"    => array("#C0392B","#E87E04")
        );
        if($this->request->is("post")) {
            $start_date = $this->request->data['start_date'];
            $end_date   = $this->request->data['end_date'];
        } else {
            $start_date = date("d-m-Y");
            $end_date   = date("d-m-Y", strtotime(date("Y-m-d") . "-1 month"));
        }
        $sqlDate = $this->DateRange->getDateFroSql($start_date,$end_date);
        $range = $this->DateRange->getRange($start_date,$end_date);
        foreach ( $range as $date ) {
            $date = $date->format ( 'Y-m-d' );
            $return['data'][$date] = array(
                'y' => 0,
                'x' => $date,
                "z" => 0
            );
        }
        $this->loadModel("User");
        $dataUserAltro = $this->User->find("all",array(
            "recursive" => -1,
            "fields" => array(
                "COUNT(User.id) AS count",
                "to_char(created , 'YYYY-MM-DD') AS date"
            ),
            "conditions" => array(
                "AND" => array(
                    "created >= " => $sqlDate['start_date'],
                    "created <= " => $sqlDate['end_date']
                ),

                "lead_source" => NULL

            ),
            "group" => "date"
        ));
        $dataShopLanding = $this->User->find("all",array(
            "recursive" => -1,
            "fields" => array(
                "COUNT(User.id) AS count",
                "to_char(created , 'YYYY-MM-DD') AS date"
            ),
            "conditions" => array(
                "AND" => array(
                    "created >= " => $sqlDate['start_date'],
                    "created <= " => $sqlDate['end_date']
                ),
                "lead_source" => 'Landing page'
            ),
            "group" => "date"
        ));
        foreach($dataShopLanding as $track) {
            $return['data'][$track[0]['date']]['y'] = $track[0]['count'];
        }
        foreach($dataUserAltro as $track) {
            $return['data'][$track[0]['date']]['z'] = $track[0]['count'];
        }
        $return['data'] = array_values($return['data']);
        $this->set(array(
            'data' => $return,
            '_serialize' => array('data')
        ));
    }

    public function application_getUserDataActive() {
        $return = array(
            "data"      => array(),
            "xkey"      => 'x',
            "ykeys"     => array("y"),
            "labels"    => ['Utenti'],
            "smooth"    => false,
            "pointSize" => 0,
            "hideHover" => 'auto',
            "loaded"    => true
        );
        if($this->request->is("post")) {
            $start_date = $this->request->data['start_date'];
            $end_date   = $this->request->data['end_date'];
        } else {
            $start_date = date("d-m-Y");
            $end_date   = date("d-m-Y", strtotime(date("Y-m-d") . "-1 month"));
        }
        $sqlDate = $this->DateRange->getDateFroSql($start_date,$end_date);
        $range = $this->DateRange->getRange($start_date,$end_date);
        foreach ( $range as $date ) {
            $date = $date->format ( 'Y-m-d' );
            $return['data'][$date] = array(
                'y' => 0,
                'x' => $date,
            );
        }
        $this->loadModel("UserAction");
        $dataUserActive = $this->UserAction->find("all",array(
            "recursive" => -1,
            "fields" => array(
                "COUNT(DISTINCT UserAction.user_id) AS count",
                "to_char(created , 'YYYY-MM-DD') AS date"
            ),
            "conditions" => array(
                "AND" => array(
                    "created >= " => $sqlDate['start_date'],
                    "created <= " => $sqlDate['end_date']
                )
            ),
            "group" => "date"
        ));
        foreach($dataUserActive as $track) {
            $return['data'][$track[0]['date']]['y'] = $track[0]['count'];
        }
        $return['data'] = array_values($return['data']);
        $this->set(array(
            'data' => $return,
            '_serialize' => array('data')
        ));
    }

    public function application_getUserDataAction() {
        $return = array(
            "data"          => array(),
            "xkey"          => 'x',
            "ykeys"         => array("y","z","t"),
            "labels"        => ['Successo','Errore',"Totali"],
            "smooth"        => false,
            "pointSize"     => 0,
            "hideHover"     => 'auto',
            "loaded"        => true,
            "lineColors"    => array("#4DAF7C","#C0392B","#E87E04")
        );
        if($this->request->is("post")) {
            $start_date = $this->request->data['start_date'];
            $end_date   = $this->request->data['end_date'];
        } else {
            $start_date = date("d-m-Y");
            $end_date   = date("d-m-Y", strtotime(date("Y-m-d") . "-1 month"));
        }
        $sqlDate = $this->DateRange->getDateFroSql($start_date,$end_date);
        $range = $this->DateRange->getRange($start_date,$end_date);
        foreach ( $range as $date ) {
            $date = $date->format ( 'Y-m-d' );
            $return['data'][$date] = array(
                'y' => 0,
                'z' => 0,
                't' => 0,
                'x' => $date,
            );
        }
        $this->loadModel("UserAction");
        $dataUserActionSuccess = $this->UserAction->find("all",array(
            "recursive" => -1,
            "fields" => array(
                "COUNT(UserAction.id) AS count",
                "to_char(created , 'YYYY-MM-DD') AS date",
            ),
            "conditions" => array(
                "AND" => array(
                    "created >= " => $sqlDate['start_date'],
                    "created <= " => $sqlDate['end_date']
                ),
                "success" => TRUE
            ),
            "group" => "date"
        ));
        $dataUserActionError = $this->UserAction->find("all",array(
            "recursive" => -1,
            "fields" => array(
                "COUNT(UserAction.id) AS count",
                "to_char(created , 'YYYY-MM-DD') AS date",
            ),
            "conditions" => array(
                "AND" => array(
                    "created >= " => $sqlDate['start_date'],
                    "created <= " => $sqlDate['end_date']
                ),
                "error" => TRUE
            ),
            "group" => "date"
        ));
        foreach($dataUserActionSuccess as $track) {
            $return['data'][$track[0]['date']]['y'] = $track[0]['count'];
            $return['data'][$track[0]['date']]['t'] = $track[0]['count'];
        }
        foreach($dataUserActionError as $track) {
            $return['data'][$track[0]['date']]['z'] = $track[0]['count'];
            $return['data'][$track[0]['date']]['t'] += $track[0]['count'];
        }
        $return['data'] = array_values($return['data']);
        $this->set(array(
            'data' => $return,
            '_serialize' => array('data')
        ));
    }

    public function application_getUserLoginType() {
        $return = array(
            "data"      => array(),
            "colors"    => array("#F89406","#1F3A93","#96281B"),
            "percent"   => true
        );
        $this->loadModel("User");
        $signUpUser = $this->User->find("count",array(
            "conditions" => array(
                "facebook_id"   => NULL,
                "google_id"     => NULL
            )
        ));
        $facebookUser = $this->User->find("count",array(
            "conditions" => array(
                "NOT" => array(
                    "facebook_id"   => NULL
                )
            )
        ));
        $googleUser = $this->User->find("count",array(
            "conditions" => array(
                "NOT" => array(
                    "google_id"   => NULL
                )
            )
        ));


        $total_users =  $googleUser + $facebookUser + $signUpUser;
        $signup = round(($signUpUser/$total_users*100));
        $facebook = round(($facebookUser/$total_users*100));
        $google = round(($googleUser/$total_users*100));
        $return['data'] = array(
            array("label" => "Form" , "value" => $signup),
            array("label" => "Facebook" , "value" => $facebook),
            array("label" => "Google +" , "value" => $google),
        );

        $this->set(array(
            'data' => $return,
            '_serialize' => array('data')
        ));
    }

    public function application_getUserWithShop() {
        $return = array(
            "data"      => array(),
            "colors"    => array("#03A678","#96281B"),
            "percent"   => true
        );
        $this->loadModel("User");
        $withShop = $this->User->find("count",array(
            "joins" => array(
                array(
                    'table' => 'shops',
                    'alias' => 'Shop',
                    'type' => 'right',
                    'conditions'=> array('Shop.user_id = User.id')
                ),
            ),
            "recursive" => -1
        ));
        $withoutShop = $this->User->find("count",array(
            "joins" => array(
                array(
                    'table' => 'shops',
                    'alias' => 'Shop',
                    'type' => 'left',
                    'conditions'=> array('Shop.user_id = User.id')
                ),
            ),
            "recursive" => -1
        ));

        $total_users =  $withShop + $withoutShop ;
        $with = round(($withShop/$total_users*100));
        $without = round(($withoutShop/$total_users*100));

        $return['data'] = array(
            array("label" => "Con shop" , "value" => $with),
            array("label" => "Senza shop" , "value" => $without)
        );

        $this->set(array(
            'data' => $return,
            '_serialize' => array('data')
        ));
    }


    public function getUserDetails(){
        $this->response->compress();
        $this->loadModel("User");
        $this->loadModel("Shop");
        $this->loadModel("UserAction");
        $user_id = NULL;
        if($this->request->is("post")) {
            if(isset($this->request->data['user_id']) && !empty($this->request->data['user_id'])) {
                $user_id = $this->request->data['user_id'];
            }
            $start_date = $this->request->data['start_date'];
            $end_date   = $this->request->data['end_date'];
        } else {
            $start_date = date("d-m-Y");
            $end_date   = date("d-m-Y", strtotime(date("Y-m-d") . "-7 days"));
        }
        $sqlDate = $this->DateRange->getDateFroSql($start_date,$end_date);
        $user_conditions = array(
            "AND" => array(
                "User.created >= " => $sqlDate['start_date'],
                "User.created <= " => $sqlDate['end_date']
            ),
            "User.is_admin"  => FALSE
        );
        if($user_id) {
            $user_conditions["User.id"] = $user_id;
        }
        $users = $this->User->find("all",array(
            "recursive" => -1,
            "fields" => array(
                "User.id",
                "User.fullname",
                "User.created"
            ),
            "conditions" => $user_conditions ,
            "order" => "created DESC"
        ));
        $return = array(
            'date'      => array(),
            'elements'  => array()
        );
        foreach($users as $user) {
            $date = explode(" ",$user['User']['created']);
            if(!isset($return['elements'][$date[0]]) || !isset($return['elements'][$date[0]][$user['User']['id']])) {
                $return['elements'][$date[0]][$user['User']['id']]['data'] = array(
                    "name"  => $user['User']['fullname'],
                    "id"    => $user['User']['id']
                );
            }
            $return['elements'][$date[0]][$user['User']['id']]['actions'][] = array(
                "description" => "Ha effettuato la registrazione.",
                "date" => $user['User']['created'],
                "link"          => NULL
            );
        }

        $shop_conditions = array(
            "AND" => array(
                "Shop.created >= " => $sqlDate['start_date'],
                "Shop.created <= " => $sqlDate['end_date']
            )
        );
        if($user_id) {
            $shop_conditions["Shop.user_id"] = $user_id;
        }

        $shops = $this->Shop->find("all",array(
                "recursive" => -1,
                "fields" => array(
                    "Shop.name",
                    "Shop.created",
                    "Shop.url"
                ),
                "conditions" => $shop_conditions,
                "contain" => array(
                    "User" => array(
                        "fields" => array(
                            "id",
                            "first_name",
                            "last_name"
                        ),
                        "is_admin"  => FALSE
                    )
                ),
                "order" => "Shop.created DESC"
            )
        );
        foreach($shops as $shop) {
            $date = explode(" ",$shop['Shop']['created']);
            if(!isset($return['elements'][$date[0]]) || !isset($return['elements'][$date[0]][$shop['User']['id']])) {
                $return['elements'][$date[0]][$shop['User']['id']]['data'] = array(
                    "name"  => $shop['User']['first_name']." ".$shop['User']['last_name'],
                    "id"    => $shop['User']['id']
                );
            }
            $return['elements'][$date[0]][$shop['User']['id']]['actions'][] = array(
                "description"   => "Ha creato lo shop ".$shop['Shop']['name'],
                "date"          => $shop['Shop']['created'],
                "link"          => "http://".$shop['Shop']['url'].".negozy.com"
            );
        }


        $user_join_conditions = array(
            'User.id = UserAction.user_id',
            'User.is_admin' => FALSE,
        );
        if(isset($user_id)) {
            $user_join_conditions['User.id'] = $user_id;
        }

        $user_actions = $this->UserAction->find("all",array(
            "conditions" => array(
                "AND" => array(
                    "UserAction.created >= " => $sqlDate['start_date'],
                    "UserAction.created <= " => $sqlDate['end_date']
                )
            ),
            "fields" => array(
                "User.id",
                "User.first_name",
                "User.last_name",
                "UserAction.*"
            ),
            "joins" => array(
                array(
                    'table' => 'users',
                    'alias' => 'User',
                    'type'  => 'INNER',
                    'conditions' => $user_join_conditions
                )
            )
        ));
        foreach($user_actions as $action) {
            if(isset($action['UserAction']['user_id']) && !empty($action['UserAction']['user_id'])) {
                $date = explode(" ", $action['UserAction']['created']);
                if (!isset($return['elements'][$date[0]]) || !isset($return['elements'][$date[0]][$action['UserAction']['user_id']])) {
                    $return['elements'][$date[0]][$action['UserAction']['user_id']]['data'] = array(
                        "name"  => $action['User']['first_name']." ".$action['User']['last_name'],
                        "id"    => $action['User']['id']
                    );
                }
                $return['elements'][$date[0]][$action['UserAction']['user_id']]['actions'][] = array(
                    "description"   => $action['UserAction']['action'],
                    "date"          => $action['UserAction']['created'],
                    "link"          => NULL
                );
            }
        }

        $date = array_keys($return['elements']);
        $tmp_date = [];
        foreach($date as $key => $value) {
            $tmp_date[strtotime($value)] = $value;
        }
        krsort($tmp_date);
        $return['date'] = array_values($tmp_date);
        $this->set(array(
            'data' => $return,
            '_serialize' => array('data')
        ));
    }

    public function application_loadShops(){
        $this->loadModel("Shop");
        $shops = $this->Shop->find("all",array(
            "fields" => array(
                "name",
                "site_url"
            ),
            "contain" => array(
                "User" => array(
                    "fields" => array(
                        "first_name",
                        "last_name",
                    )
                )
            ),
            "recursive" => -1
        ));
        $data = array();
        foreach($shops as $shop) {
            $data[] = array(
                "name"  => $shop['Shop']['name'],
                "url"   => $shop['Shop']['site_url'],
                "user"  => $shop['User']['first_name']." ".$shop['User']['last_name'],
            );
        }
        $this->set(array(
            'data' => $data,
            '_serialize' => array('data')
        ));
    }

    public function application_negozyStat(){
        $this->loadModel("User");
        $this->loadModel("Shop");
        $this->loadModel("Product");
        $this->loadModel("Order");
        $users      = $this->User->find("count",array(
            "conditions" => array(
                "User.is_admin" => FALSE
            ),
            "recursive" => -1
        ));
        $shops = $this->Shop->find("count",array(
            "recursive" => -1
        ));
        $orders = $this->Order->find("count",array(
            "conditions" => array(
                "NOT" => array(
                    "user_id" => 139
                )
            ),
            "recursive" => -1
        ));
        $orders_total = $this->Order->find("all",array(
                "fields" => array(
                    "SUM (Order.total_amount)::bigint"
                ),
                "conditions" => array(
                    "NOT" => array(
                        "user_id" => 139
                    )
                ),
                "recursive" => -1
            )
        );
        $products   = $this->Product->find("count",array(
            "conditions" => array(
                "Product.deleted" => false
            ),
            "joins" => array(
                array(
                    'table' => 'shops',
                    'alias' => 'Shop',
                    'type' => 'inner',
                    'conditions'=> array(
                        'Product.shop_id = Shop.id'
                    )
                ),
                array(
                    'table' => 'users',
                    'alias' => 'User',
                    'type' => 'right',
                    'conditions'=> array(
                        'User.id = Shop.user_id',
                        "User.is_admin" => FALSE
                    )
                ),
            ),

            "recursive" => -1
        ));
        $product_sum_small = $this->Product->find("all",array(
            "fields" => array(
                "SUM (Product.price)::bigint"
            ),
            "conditions" => array(
                "Product.deleted" => false,
                "Product.price <= " => 10000
            ),
            "joins" => array(
                array(
                    'table' => 'shops',
                    'alias' => 'Shop',
                    'type' => 'inner',
                    'conditions'=> array(
                        'Product.shop_id = Shop.id'
                    )
                ),
                array(
                    'table' => 'users',
                    'alias' => 'User',
                    'type' => 'right',
                    'conditions'=> array(
                        'User.id = Shop.user_id',
                        "User.is_admin" => FALSE
                    )
                ),
            ),
            "recursive" => -1
            )
        );
        $product_sum_big = $this->Product->find("all",array(
                "fields" => array(
                    "SUM (Product.price)::bigint"
                ),
                "conditions" => array(
                    "Product.deleted" => false,
                    "Product.price > " => 10000
                ),
                "joins" => array(
                    array(
                        'table' => 'shops',
                        'alias' => 'Shop',
                        'type' => 'inner',
                        'conditions'=> array(
                            'Product.shop_id = Shop.id'
                        )
                    ),
                    array(
                        'table' => 'users',
                        'alias' => 'User',
                        'type' => 'right',
                        'conditions'=> array(
                            'User.id = Shop.user_id',
                            "User.is_admin" => FALSE
                        )
                    ),
                ),
                "recursive" => -1
            )
        );
        $data = array(
            "users" => $users,
            "shops" => $shops,
            "orders" => $orders,
            "orders_total" => $orders_total[0][0]['sum'],
            "products" => $products,
            "total_product_small" => $product_sum_small[0][0]['sum'],
            "total_product_big" => $product_sum_big[0][0]['sum']
        );
        $this->set(array(
            'data' => $data,
            '_serialize' => array('data')
        ));
    }

}