<?php
class DashboardController extends CronLogAppController {

    public function application_index(){
        $this->layout = "ajax";
    }

    public function application_cronJobs(){
        $this->layout = "ajax";
    }

    public function application_userDetails(){
        $this->layout = "ajax";
    }

    public function application_jobDetails(){
        $this->layout = "ajax";
    }

    public function application_shops(){
        $this->layout = "ajax";
    }

}