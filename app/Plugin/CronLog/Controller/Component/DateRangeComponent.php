<?php
class DateRangeComponent extends Component {

    public function getRange($start_date=NULL,$end_date = NULL){
        if(!isset($start_date) || !isset($end_date)){
            $start_date = date("d-m-Y", strtotime( date("Y-m-d")  . "-1 month" ));
            $end_date = date("d-m-Y");
        }
        $fromdate = DateTime::createFromFormat ( 'd-m-Y', $start_date );
        $todate = DateTime::createFromFormat ( 'd-m-Y', $end_date );
        return new DatePeriod ( $fromdate, new DateInterval ( 'P1D' ), $todate->modify ( '+1 day' ) );

    }

    public function getDateFroSql($start_date=NULL,$end_date = NULL){
        if(isset($start_date) && isset($end_date)) {
            return array(
                "start_date" => date ( "Y-m-d H:i:s", strtotime ( $start_date ." 00:00:00" ) ),
                "end_date"   => date ( "Y-m-d H:i:s", strtotime ( $end_date ." 23:59:59" ) )
            );
        }
        return false;
    }

}