<?php
class UserAction extends CronLogAppModel {

    public $actsAs = array('Containable');

    public $belongsTo = array(
        "User" => array(
            'className' => 'User'
        )
    );

}