(function() {
    'use strict';
    var cron_log = angular.module('plugin.CronLog.controllers', ['daterangepicker']);
    cron_log.controller('CronLogDashboardCtrl',  function($scope,$activityIndicator,CronLogUserAction) {
        $activityIndicator.startAnimating();

        $scope.chartData            = {};
        $scope.chartDataUserAction  = {};
        $scope.chartDataUserSource  = {};
        $scope.chartDataUserAction  = {};
        $scope.chartDataUserType    = {};
        $scope.chartDataUserShop    = {};
        $scope.stats                = {};
        CronLogUserAction.getNegozyStat().success(function(r){
            $scope.stats = r.data;
        });
        CronLogUserAction.getUserDataType().success(function(r){
            $scope.chartDataUserType = r.data;
        })

        CronLogUserAction.getUserDataShop().success(function(r){
            $scope.chartDataUserShop = r.data;
        })

        $scope.date_user = {
            startDate: moment().subtract(1,"month"),
            endDate: moment()
        };

        $scope.date_user_source = {
            startDate: moment().subtract(1,"month"),
            endDate: moment()
        };

        $scope.date_user_active = {
            startDate: moment().subtract(1,"month"),
            endDate: moment()
        };

        $scope.date_user_action = {
            startDate: moment().subtract(1,"month"),
            endDate: moment()
        };

        $scope.$watch('date_user', function(newDate) {
            var startDate = moment(newDate.startDate).format("DD-MM-YYYY");
            var endDate = moment(newDate.endDate).format("DD-MM-YYYY");
            CronLogUserAction.getUserData(startDate,endDate).success(function(r){
                $scope.chartData = r.data;
            })
        }, false);

        $scope.$watch('date_user_active', function(newDate) {
            var startDate = moment(newDate.startDate).format("DD-MM-YYYY");
            var endDate = moment(newDate.endDate).format("DD-MM-YYYY");
            CronLogUserAction.getUserDataActive(startDate,endDate).success(function(r){
                $scope.chartDataUserActive = r.data;
            })
        }, false);

        $scope.$watch('date_user_action', function(newDate) {
            var startDate = moment(newDate.startDate).format("DD-MM-YYYY");
            var endDate = moment(newDate.endDate).format("DD-MM-YYYY");
            CronLogUserAction.getUserDataAction(startDate,endDate).success(function(r){
                $scope.chartDataUserAction = r.data;
            })
        }, false);

        $scope.$watch('date_user_source', function(newDate) {
            var startDate = moment(newDate.startDate).format("DD-MM-YYYY");
            var endDate = moment(newDate.endDate).format("DD-MM-YYYY");
            CronLogUserAction.getUserDataSource(startDate,endDate).success(function(r){
                $scope.chartDataUserSource = r.data;
            })
        }, false);


        $scope.opts = {
            ranges: {
                'Last 7 Days': [moment().subtract('days', 6), moment()],
                'Last 30 Days': [moment().subtract('days', 29), moment()]
            }
        };
        $activityIndicator.stopAnimating();
    })
    cron_log.controller('CronLogJobsCtrl',  function($scope,$activityIndicator,CronLogJobAction) {
        $scope.chartData        = {};
        $scope.date_jobs_action = {
            startDate: moment().subtract(1,"month"),
            endDate: moment()
        };
        $scope.$watch('date_jobs_action', function(newDate) {
            var startDate = moment(newDate.startDate).format("DD-MM-YYYY");
            var endDate = moment(newDate.endDate).format("DD-MM-YYYY");
            CronLogJobAction.getJobData(startDate,endDate).success(function(r){
                $scope.chartData = r.data;
            })
        }, false);

        $scope.opts = {
            ranges: {
                'Last 7 Days': [moment().subtract('days', 6), moment()],
                'Last 30 Days': [moment().subtract('days', 29), moment()]
            }
        };
        $activityIndicator.stopAnimating();
    })
    cron_log.controller('CronLogUserDetailsCtrl',  function($scope,$activityIndicator,CronLogUserAction) {
        $scope.timeline = [];
        $scope.date_jobs_action = {
            startDate: moment().subtract(7,"days"),
            endDate: moment()
        };
        $scope.user_id      = null;
        $scope.user_name    = null;
        $scope.user_actions = function(user_id,name){
            $scope.user_id      = (user_id) ? user_id : null;
            $scope.user_name    = (name) ? name : null;
            $activityIndicator.startAnimating();
            CronLogUserAction.getUserDetails($scope.startDate,$scope.endDate,$scope.user_id).success(function(r){
                $scope.timeline         = r.data.elements;
                $scope.timeline_date    = r.data.date;
                $activityIndicator.stopAnimating();
            })
        }
        $scope.$watch('date_jobs_action', function(newDate) {
            $activityIndicator.startAnimating();
            $scope.startDate = moment(newDate.startDate).format("DD-MM-YYYY");
            $scope.endDate = moment(newDate.endDate).format("DD-MM-YYYY");
            CronLogUserAction.getUserDetails($scope.startDate,$scope.endDate,$scope.user_id).success(function(r){
                $scope.timeline         = r.data.elements;
                $scope.timeline_date    = r.data.date;
                $activityIndicator.stopAnimating();
            })
        }, false);
    })
    cron_log.controller('CronLogJobDetailsCtrl',  function($scope,$activityIndicator,CronLogJobAction) {
        $scope.timeline = [];
        $scope.date_jobs_action = {
            startDate: moment().subtract(7,"days"),
            endDate: moment()
        };
        $scope.$watch('date_jobs_action', function(newDate) {
            $activityIndicator.startAnimating();
            $scope.startDate = moment(newDate.startDate).format("DD-MM-YYYY");
            $scope.endDate = moment(newDate.endDate).format("DD-MM-YYYY");
            CronLogJobAction.getJobDetails($scope.startDate,$scope.endDate).success(function(r){
                $scope.timeline         = r.data.elements;
                $scope.timeline_date    = r.data.date;
                $activityIndicator.stopAnimating();
            })
        }, false);
    })
    cron_log.controller('CronLogShopCtrl',  function($scope,$activityIndicator,CronLogUserAction) {
        $scope.shops = [];
        $activityIndicator.startAnimating();
        CronLogUserAction.loadShops().success(function(r){
            $scope.shops = r.data;
            $activityIndicator.stopAnimating();
        })
    })
    cron_log.filter("nrFormat", function() {
            return function(number) {
                var abs;
                if (number !== void 0) {
                    console.log(number);
                    abs = Math.abs(number);
                    if (abs >= Math.pow(10, 12)) {
                        number = (number / Math.pow(10, 12)).toFixed(1) + "T";
                    } else if (abs < Math.pow(10, 12) && abs >= Math.pow(10, 9)) {
                        number = (number / Math.pow(10, 9)).toFixed(1) + "b";
                    } else if (abs < Math.pow(10, 9) && abs >= Math.pow(10, 6)) {
                        number = (number / Math.pow(10, 6)).toFixed(1) + "M";
                    } else if (abs < Math.pow(10, 6) && abs >= Math.pow(10, 3)) {
                        number = (number / Math.pow(10, 3)).toFixed(1) + "K";
                    }
                    return number;
                }
            };
        });

}).call(this);
