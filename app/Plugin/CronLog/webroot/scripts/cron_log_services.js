(function() {
    'use strict';
    angular.module('plugin.CronLog.services', [])
        .factory("CronLogUserAction", function ($http) {
            return {
                getUserData: function(start_date,end_date) {
                    return $http.post('/application/cron_log/user_data/getUserData.json',{start_date:start_date,end_date:end_date});
                },
                getUserDataActive: function(start_date,end_date) {
                    return $http.post('/application/cron_log/user_data/getUserDataActive.json',{start_date:start_date,end_date:end_date});
                },
                getUserDataAction: function(start_date,end_date) {
                    return $http.post('/application/cron_log/user_data/getUserDataAction.json',{start_date:start_date,end_date:end_date});
                },
                getUserDataType: function() {
                    return $http.get('/application/cron_log/user_data/getUserLoginType.json');
                },
                getUserDataShop: function() {
                    return $http.get('/application/cron_log/user_data/getUserWithShop.json');
                },
                getUserDataSource: function(start_date,end_date) {
                    return $http.post('/application/cron_log/user_data/getUserDataSource.json',{start_date:start_date,end_date:end_date});
                },
                getUserDetails: function(start_date,end_date,user_id) {
                    return $http.post('/cron_log/user_data/getUserDetails.json',{start_date:start_date,end_date:end_date,user_id:user_id});
                },
                loadShops: function(){
                    return $http.get('/application/cron_log/user_data/loadShops.json');
                },
                getNegozyStat: function(){
                    return $http.get('/application/cron_log/user_data/negozyStat.json');
                }
            }
        })
        .factory("CronLogJobAction", function ($http) {
            return {
                getJobData: function(start_date,end_date) {
                    return $http.post('/application/cron_log/job_data/getJobData.json',{start_date:start_date,end_date:end_date});
                },
                getJobDetails: function(start_date,end_date) {
                    return $http.post('/cron_log/job_data/getJobDetails.json',{start_date:start_date,end_date:end_date});
                }
            }
        })
}).call(this);
