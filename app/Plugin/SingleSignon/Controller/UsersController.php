<?php
class UsersController extends SingleSignonAppController {

    public $components = array('RequestHandler','Paginator');
    public $uses        = array("User");
    public function get_list($n=10,$page=1,$row="created",$orderType="ASC",$query="") {
        $this->response->compress();
        $params = explode(" ",$query);
        $conditions_user = array();
        $conditions_shop = array();
        $conditions_user["UPPER(first_name) LIKE"] = "%%";
        $conditions_user["UPPER(last_name) LIKE"] = "%%";
        foreach($params as $param) {
            $param = strtoupper($param);
            $conditions_user["UPPER(first_name) LIKE"] = "%".$param."%";
            $conditions_user["UPPER(last_name) LIKE"] = "%".$param."%";
        }
        $this->Paginator->settings = array(
            "recursive" => -1,
            "fields" => array(
                "id",
                "first_name",
                "last_name",
                "timestamp_created"
            ),
            "conditions" => array(
                "OR" => $conditions_user
            ),
            "order" => "User.".$row." ".$orderType,
            "limit" => $n,
            "page" => $page,
            "contain" => array(
                "Shop" => array(
                    "fields" => array(
                        "name"
                    )
                )
            )
        );
        $count = $this->User->find("count",array(
            "recursive" => -1,
            "conditions" => array(
                "OR" => $conditions_user
            ),
        ));
        $users = $this->Paginator->paginate('User');
        $return = array();
        foreach($users as $user) {
            $shop = reset($user['Shop']);
            $return[] = array(
                "id"                => $user['User']['id'],
                "first_name"        => $user['User']['first_name'],
                "last_name"         => $user['User']['last_name'],
                "timestamp_created" => $user['User']['timestamp_created'],
                "shop"              => isset($shop['name']) ? $shop['name'] : ''
            );
        }
        $this->set(array(
            'users' => $return,
            'count' => $count,
            '_serialize' => array('users','count')
        ));
    }

    public function admin_user(){
        $data['success'] = false;
        if($this->request->is("post") && isset($this->request->data["user_id"])) {
            $user = $this->User->find("first", array(
                "conditions" => array(
                    "id" => $this->request->data["user_id"]
                ),
                "fields" => array(
                    "id",
                    "first_name",
                    "last_name",
                    "email",
                    "confirmed"
                ),
                "recursive" => -1
            ));
            $tmp_user = $this->Auth->user();
            if ($this->Auth->login($user['User'])) {
                $this->Session->write("tmp_user",$tmp_user);
                $data['success'] = true;
            }
        }
        $this->set(array(
            'data' => $data,
            '_serialize' => array('data')
        ));

    }

}