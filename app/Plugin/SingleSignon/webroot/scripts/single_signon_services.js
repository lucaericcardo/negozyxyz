(function() {
    'use strict';
    angular.module('plugin.SingleSignon.services', [])
        .factory("SingleSignonUser", function ($http) {
            return {
                getList: function (n, page, row, orderType,query) {
                    return $http.get('/single_signon/users/get_list/' + n + '/' + page + '/' + row + '/' + orderType + '/'+query+'.json');
                },
                admin_user: function(user_id){
                    return $http.post('/single_signon/users/admin_user.json',{user_id:user_id});
                }
            }
        })
}).call(this);
