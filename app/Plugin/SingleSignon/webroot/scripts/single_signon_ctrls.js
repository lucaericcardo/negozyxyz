(function() {
    'use strict';
    var single_signon = angular.module('plugin.SingleSignon.controllers', []);
    single_signon.controller('SingleSignonkMainCtrl',  function($scope,$activityIndicator,SingleSignonUser) {
        $scope.numPerPageOpt        = [5, 10, 20];
        $scope.numPerPage           = $scope.numPerPageOpt[1];
        $scope.currentPage          = 1;
        $scope.currentPageStores    = [];
        $scope.collections          = [];
        $scope.totalUsers           = 0;
        $scope.row                  = 'created';
        $scope.orderType            = 'ASC';
        $scope.query                = '';

        $activityIndicator.startAnimating();
        $scope.collections = [];

        $scope.select = function(page) {
            $activityIndicator.startAnimating();
            $scope.deselectAll  = true;
            $scope.selectAll    = false;
            SingleSignonUser.getList($scope.numPerPage,page,$scope.row,$scope.orderType,$scope.query).success(function(r){
                $scope.users        = r.users;
                $scope.totalUsers   = r.count;
                $activityIndicator.stopAnimating();
            });
        };

        $scope.onFilterChange = function() {
            $scope.select(1);
            $scope.currentPage = 1;
            return $scope.row = '';
        };
        $scope.onNumPerPageChange = function() {
            $scope.select(1);
            return $scope.currentPage = 1;
        };
        $scope.onOrderChange = function() {
            $scope.select(1);
            return $scope.currentPage = 1;
        };
        $scope.order = function(rowName,orderType) {
            if ($scope.row === rowName && $scope.orderType === orderType) {
                return;
            }
            $activityIndicator.startAnimating();
            $scope.row = rowName;
            $scope.orderType = orderType;
            return $scope.onOrderChange();
        };

        var init = function() {
            return $scope.select($scope.currentPage);
        };
        $scope.search = function(){
            $scope.select($scope.currentPage);
        }
        $scope.admin = function(user_id){
            SingleSignonUser.admin_user(user_id).success(function(r){
                if(r.data.success) {
                    window.location.assign("/");
                }
            })
        }
        return init();
    })
}).call(this);
