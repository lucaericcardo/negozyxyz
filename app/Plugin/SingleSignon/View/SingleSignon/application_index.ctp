<div class="lapagina" data-ng-controller="SingleSignonkMainCtrl">
    <nav class="navbar navbar-default navbar-static-top" role="navigation">
        <div class="navbar-header">
            <ul class="nav navbar-nav">
                <li>
                    <ol class="breadcrumb">
                        <li class="active"><a><?php echo __("Single Signon"); ?></a></li>
                    </ol>
                </li>
            </ul>
        </div>
    </nav>
    <div class="page page-table">
        <div class="col-md-12">
            <section class="panel panel-default table-dynamic">
                <div class="panel-body">
                    <div class="table-responsive">
                        <div class="input-group">
                            <input data-ng-model="query" type="text" class="form-control">
                            <span class="input-group-btn">
                                <button data-ng-click="search()" type="button" class="btn btn-default"><i class="fa fa-search"></i></button>
                            </span>
                        </div>


                        <table class="table table-bordered table-striped table-responsive">
                            <thead>
                            <tr>
                                <th>
                                    <div class="th">
                                        <?php echo __("Nome"); ?>
                                        <span class="glyphicon glyphicon-chevron-up" data-ng-click=" order('first_name','ASC') " data-ng-class="{active: row == 'first_name' && orderType == 'ASC'}"></span>
                                        <span class="glyphicon glyphicon-chevron-down" data-ng-click=" order('first_name','DESC') " data-ng-class="{active: row == 'first_name' && orderType == 'DESC'}"></span>
                                    </div>
                                </th>
                                <th>
                                    <div class="th">
                                        <?php echo __("Cognome"); ?>
                                        <span class="glyphicon glyphicon-chevron-up" data-ng-click=" order('last_name','ASC') " data-ng-class="{active: row == 'last_name' && orderType == 'ASC'}"></span>
                                        <span class="glyphicon glyphicon-chevron-down" data-ng-click=" order('last_name','DESC') " data-ng-class="{active: row == 'last_name' && orderType == 'DESC'}"></span>
                                    </div>
                                </th>
                                <th>
                                    <div class="th">
                                        <?php echo __("Shop"); ?>
                                    </div>
                                </th>
                                <th>
                                    <div class="th">
                                        <?php echo __("Data di creazione"); ?>
                                        <span class="glyphicon glyphicon-chevron-up" data-ng-click=" order('created','ASC') " data-ng-class="{active: row == 'created' && orderType == 'ASC'}"></span>
                                        <span class="glyphicon glyphicon-chevron-down" data-ng-click=" order('created','DESC') " data-ng-class="{active: row == 'created' && orderType == 'DESC'}"></span>
                                    </div>
                                </th>
                                <th class="text-right" width="120">
                                    <div class="th">
                                        <?php echo __("Azioni"); ?>
                                    </div>
                                </th>
                            </tr>
                            </thead>
                            <tbody>
                            <tr data-ng-repeat="user in users">
                                <td>{{user.first_name}}</td>
                                <td>{{user.last_name}}</td>
                                <td>{{user.shop}}</td>
                                <td>{{user.timestamp_created | date:'dd/MM/yyyy  H:mm'}}</td>
                                <td class="text-right">
                                    <a href="javascript:;" data-ng-click="admin(user.id)" class="btn-icon btn-icon-sm bg-primary"><i class="fa fa-desktop"></i></a>
                                </td>
                            </tr>
                            </tbody>
                        </table>
                    </div>
                    <footer class="table-footer">
                        <div class="row">
                            <div class="col-md-6 page-num-info">
                                    <span>
                                        <?php echo __("Elementi per pagina"); ?>
                                        <select data-ng-model="numPerPage" data-ng-options="num for num in numPerPageOpt" data-ng-change="onNumPerPageChange()">
                                        </select>
                                    </span>
                            </div>
                            <div class="col-md-6 text-right pagination-container">
                                <pagination class="pagination-sm" ng-model="currentPage" total-items="totalUsers" max-size="4" ng-change="select(currentPage)" items-per-page="numPerPage" rotate="false" boundary-links="true"></pagination>
                            </div>
                        </div>
                    </footer>
                </div>
            </section>
        </div>
    </div>
</div>
