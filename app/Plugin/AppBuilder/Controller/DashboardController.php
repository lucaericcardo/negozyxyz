<?php
class DashboardController extends AppBuilderAppController {

    public function application_index() {
        $this->layout = "ajax";
    }
    public function application_add() {
        $this->layout = "ajax";
    }
    public function application_edit() {
        $this->layout = "ajax";
    }
}