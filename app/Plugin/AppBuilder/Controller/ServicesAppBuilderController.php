<?php

class ServicesAppBuilderController extends AppBuilderAppController {
    public $uses = array("AppCategory", "AppCompanyDeveloper", "Application", "AppScreenshot", "AppIcon", "ShopAppInstalled");
    public $components = array('Image', 'AppBuilder.AppBuilder','Paginator');

    public function add() {
        $this->response->compress();
        $data['success'] = false;

        if($this->request->is('post') && $this->request->data('app')) {
            if($this->isUniqueRequestUri($this->request->data('request_uri'))) {
                $last_version = $this->request->data('app.last_version');
                if(settype($last_version, "float")) {
                    $this->request->data['app']['last_version'] = $last_version;
                    $this->request->data['app']['last_version'] = $this->request->data('app.last_version');
                } else {
                    $this->request->data['app']['last_version'] = 0.1;
                }
                if($this->Application->save($this->request->data('app'))) {

                    $zipfile = $this->AppBuilder->create($this->request->data('app.request_uri'), $this->request->data('app.name'));
                    $this->Application->saveField('zip_structure', fread(fopen($zipfile, "r"), filesize($zipfile)));
                    $this->AppBuilder->delete();

                    $data['success'] = true;
                }
            }
        }

        $this->set(array(
            "data" => $data,
            "_serialize"=>array("data")
        ));
    }

    private function isInstalled($app_id) {
        $options = array(
            'conditions' => array(
                'ShopAppInstalled.app_id' => $app_id,
                'ShopAppInstalled.shop_id' => $this->shop_id,
                'ShopAppInstalled.deleted' => false
            ),
            'fields' => array('ShopAppInstalled.id')
        );

        return $this->ShopAppInstalled->find('first', $options);
    }

    public function sendFile($application_id = null) {
        if($app_data = $this->getInfoAppById($application_id)) {
            if($thisAppId = $this->getAppIdByRequestUri($this->params->plugin)) {
                $this->_checkShop();
                if($this->isInstalled($thisAppId)) {
                    $this->response->body($app_data['zip_structure']);
                    $this->response->type('zip');
                    $this->response->download($app_data['request_uri'].'.zip');
                    // Return response object to prevent controller from trying to render
                    // a view
                    return $this->response;
                }
            }
        }

        $this->response->statusCode(401);
        $this->response->send();
        return $this->response;
    }

    private function getAppIdByRequestUri($request_uri) {
        $options = array(
            'conditions' => array(
                'Application.request_uri' => $request_uri
            ),
            'fields' => array(
                'Application.id',
            )
        );
        if($app = $this->Application->find('first', $options)) {
           return $app['Application']['id'];
        }
        return false;
    }

    private function getInfoAppById($application_id) {
        $options = array(
            'conditions' => array(
                'Application.id' => $application_id
            ),
            'fields' => array(
                'Application.zip_structure',
                'Application.request_uri'
            )
        );
        if($app = $this->Application->find('first', $options)) {
            $app = Set::extract('/Application/.', $app);
            $app = reset($app);
            $app['request_uri'] = Inflector::camelize($app['request_uri']);
            return $app;
        }
        return false;
    }

    public function getApplicationData() {
        $this->response->compress();
        $data['success'] = false;

        if($this->request->is('post') && $this->request->data('app_id')) {
            $options = array(
                'conditions' => array(
                    'Application.id' => $this->request->data('app_id'),
                ),
                'contain' => array(
                    'AppIcon' => array(
                        'fields' => array(
                            'AppIcon.id',
                            'AppIcon.url'
                        )
                    ),
                    'AppScreenshot' => array(
                        'fields' => array(
                            'AppScreenshot.id',
                            'AppScreenshot.url'
                        )
                    )
                ),
                'fields' => array(
                    'Application.name',
                    'Application.description',
                    'Application.enabled',
                    'Application.is_admin',
                    'Application.last_version',
                    'Application.request_uri',
                    'Application.status',
                    'Application.category_id',
                    'Application.company_id',
                ),
                'recursive' => -1
            );
            $data['application'] = $this->Application->find('first', $options);
            $data['success'] = true;
        }

        $this->set(array(
            "data" => $data,
            "_serialize"=>array("data")
        ));
    }

    private function appHaveInstallation($application_id) {
        $options = array(
            'conditions' => array(
                'Application.id' => $application_id,
            ),
            'contain' => array(
                'ShopAppInstalled' => array(
                    'fields' => array(
                        'ShopAppInstalled.id',
                    )
                )
            ),
            'fields' => array(
                'Application.id'
            ),
            'recursive' => -1
        );
        $application = $this->Application->find('first', $options);

        if($application) {
            if(isset($application['ShopAppInstalled'])) {
                if(count($application['ShopAppInstalled'])) {
                    return true;
                }
            }
        }
        return false;
    }

    private function getAppInfoById($application_id) {
        $options = array(
            'conditions' => array(
                'Application.id' => $application_id
            ),
            'fields' => array(
                'Application.request_uri',
                'Application.name',
            ),
            'recursive' => -1
        );
        if($app = $this->Application->find('first', $options)) {
            return array(
                'name' => $app['Application']['name'],
                'request_uri' => str_replace("_","",$app['Application']['request_uri'])
            );
        }
        return false;
    }

    public function uploadScreenshot($application_id = null) {
        $this->response->compress();
        $data = array(
            "success" => false,
            "url"   => false
        ) ;
        if(isset($application_id)) {
            $app_info = $this->getAppInfoById($application_id);
            if(isset($_FILES['file'])) {
                $types[] = array("height" => null,"width" => 800,"prefix" => "screenshot_");
                $image_data = $this->Image->uploadImage($_FILES['file'],'applications/'.$app_info['request_uri'],$types);
                if ($image_data['success']) {
                    $newImage = array();
                    $newImage['AppScreenshot'] = $image_data['image'];
                    $newImage['AppScreenshot']['url'] = 'http://cdn.negozy.com/images/applications/'.$app_info['request_uri'].'/'.$image_data['image']['path'].'screenshot_'.$image_data['image']['name'].'.'.$image_data['image']['ext'];
                    $newImage['AppScreenshot']['application_id'] = $application_id;
                    $newImage['AppScreenshot']['title'] = $app_info['name']. " Screenshot";
                    if($this->AppScreenshot->save($newImage)) {
                        $data['success']    = true;
                        $data['id']         = $this->AppScreenshot->id;
                        $data['url']        = $newImage['AppScreenshot']['url'];
                    }
                }
            }
        }

        $this->set(array(
            'data' => $data,
            '_serialize' => array('data')
        ));
    }

    public function uploadIcon($application_id = null, $icon_id = null) {
        $this->response->compress();
        $data = array(
            "success" => false,
            "url"   => false
        ) ;
        if(isset($application_id)) {
            $app_info = $this->getAppInfoById($application_id);
            if(isset($_FILES['file'])) {
                $types[] = array("height" => null,"width" => 128,"prefix" => "icon_");
                $image_data = $this->Image->uploadImage($_FILES['file'],'applications/'.$app_info['request_uri'],$types);
                if ($image_data['success']) {
                    $newImage = array();
                    if(((int)$icon_id) != null) {
                        $this->AppIcon->id = $icon_id;
                    }
                    $newImage['AppIcon'] = $image_data['image'];
                    $newImage['AppIcon']['url'] = 'http://cdn.negozy.com/images/applications/'.$app_info['request_uri'].'/'.$image_data['image']['path'].'icon_'.$image_data['image']['name'].'.'.$image_data['image']['ext'];
                    $newImage['AppIcon']['application_id'] = $application_id;
                    $newImage['AppIcon']['title'] = $app_info['name']. " Icon";
                    if($this->AppIcon->save($newImage)) {
                        $data['success']    = true;
                        $data['id']         = $this->AppIcon->id;
                        $data['url']        = $newImage['AppIcon']['url'];
                    }
                }
            }
        }

        $this->set(array(
            'data' => $data,
            '_serialize' => array('data')
        ));
    }

    public function removeIcon() {
        $data = array(
            "success" => false
        ) ;
        if($this->request->is('post') && $this->request->data('icon_id') && $this->request->data('app_id')) {
            $image = $this->AppIcon->find("first",
                array(
                    "conditions" => array(
                        "id"                => $this->request->data('icon_id'),
                        "application_id"    => $this->request->data('app_id')
                    ),
                    "recursive" => -1
                )
            );
            if($image) {
                $this->Image->remove_image($image['AppIcon']['name'].".".$image['AppIcon']['ext'],"shop/".$image['AppIcon']['path'],"image_");
                if($this->AppIcon->delete($image['AppIcon']['id'])) {
                    $data['success'] = true;
                }
            }
        }
        $this->set(array(
            'data' => $data,
            '_serialize' => array('data')
        ));
    }

    public function removeScreenshot() {
        $data = array(
            "success" => false
        ) ;
        if($this->request->is('post') && $this->request->data('screenshot_id') && $this->request->data('app_id')) {
            $image = $this->AppScreenshot->find("first",
                array(
                    "conditions" => array(
                        "id"                => $this->request->data('screenshot_id'),
                        "application_id"    => $this->request->data('app_id')
                    ),
                    "recursive" => -1
                )
            );
            if($image) {
                $this->Image->remove_image($image['AppScreenshot']['name'].".".$image['AppScreenshot']['ext'],"shop/".$image['AppScreenshot']['path'],"image_");
                if($this->AppScreenshot->delete($image['AppScreenshot']['id'])) {
                    $data['success'] = true;
                }
            }
        }
        $this->set(array(
            'data' => $data,
            '_serialize' => array('data')
        ));
    }

    public function deleteApplication() {
        $this->response->compress();
        $data['success'] = false;
        if($this->request->is('post') && $this->request->data('app_id')) {
            if(!$this->appHaveInstallation($this->request->data('app_id'))) {
                $this->Application->id = $this->request->data('app_id');
                if($this->Application->saveField('deleted', true)) {
                    $data['success'] = true;
                }
            }
        }

        $this->set(array(
            "data" => $data,
            "_serialize"=>array("data")
        ));
    }

    public function getData() {
        $this->response->compress();
        $data = array();
        $data['success'] = true;

        $data['categories'] = $this->getAppCategories();
        $data['developers'] = $this->getAppDeveloper();
        $data['states'] = $this->getAppEnumStatus();

        $this->set(array(
            "data" => $data,
            "_serialize"=>array("data")
        ));
    }

    public function existRequestUri() {
        $this->response->compress();
        $data = array();
        $data['success'] = false;

        if($this->request->is('post') && $this->request->data('request_uri')) {
            $data['success'] = true;
            $data['isUnique'] = $this->isUniqueRequestUri($this->request->data('request_uri'));
        }

        $this->set(array(
            "data" => $data,
            "_serialize"=>array("data")
        ));
    }

    private function isUniqueRequestUri($request_uri) {
        return !(boolean)count($this->Application->findByRequestUri($request_uri));
    }

    private function getAppDeveloper() {
        $devel = $this->AppCompanyDeveloper->find('list');
        $ret = array();
        foreach ($devel as $id=>$val) {
            $ret[] = array("id"=>$id, "label"=>$val);
        }
        return $ret;
    }

    private function getAppCategories() {
        $categories = $this->AppCategory->find('list');
        $ret = array();
        foreach ($categories as $id=>$val) {
            $ret[] = array("id"=>$id, "label"=>$val);
        }
        return $ret;
    }

    private function getAppEnumStatus() {
        $sql = "SELECT  e.enumlabel AS enum_value
                FROM    pg_type t JOIN
                        pg_enum e ON t.oid = e.enumtypid JOIN
                        pg_catalog.pg_namespace n ON n.oid = t.typnamespace
                WHERE   t.typname = 'app_status'";
        $res = $this->Application->query($sql);
        $ret = array();
        foreach($res as $state) {
            $ret[] = $state[0]['enum_value'];
        }
        return $ret;
    }

    public function getApplications($n=10,$page=1,$row="",$orderType="") {
        $this->response->compress();
        $data['success'] = true;
        $this->Paginator->settings = array(
            'conditions' => array(
                'Application.deleted' => false
            ),
            'contain' => array(
                'ShopAppInstalled' => array(
                    'conditions' => array(
                        'ShopAppInstalled.deleted' => false,
                    ),
                    'fields' => array(
                        'ShopAppInstalled.id',
                    ),
                    'Shop' => array(
                        'User' => array(
                            'fields' => array(
                                'User.fullname'
                            )
                        ),
                        'fields' => array(
                            'Shop.name'
                        )
                    ),
                )
            ),
            'fields' => array(
                'Application.name',
                'Application.request_uri',
                'Application.is_admin',
                'Application.timestamp',
                'Application.zip_structure',
                'Application.installation_count',
            ),
            'recursive' => -1,
            "order" => $row." ".$orderType,
            "limit" => $n,
            "page" => $page,
            "recursive" => -1
        );

        $data['count'] = $this->Application->find("count",array("conditions" => array("Application.deleted" => false,), "recursive" => -1));
        $result = $this->Paginator->paginate('Application');
        $data['applications'] = array();

        foreach($result as $app) {
            if($app['Application']['zip_structure'] != NULL) {
                $app['Application']['zip_structure'] = true;
            }
            $app['Application']['installation'] = array();
            if(isset($app['ShopAppInstalled'])) {
                foreach($app['ShopAppInstalled'] as $installation) {
                    $app['Application']['installation'][] = $installation['Shop']['name'].' di '. $installation['Shop']['User']['fullname'];
                }
            }
            $data['applications'][] = $app['Application'];
        }

        $this->set(array(
            "data" => $data,
            "_serialize"=>array("data")
        ));
    }
}
?>