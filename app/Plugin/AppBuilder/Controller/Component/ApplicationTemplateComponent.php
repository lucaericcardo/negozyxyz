<?php

class ApplicationTemplateComponent extends Component {

    public function get($template, $params) {
        $view = new View(null, false);
        $view->plugin = "AppBuilder";
        return $view->element('application/'.$template, $params);

    }
}