<?php
App::uses('File', 'Utility');
App::uses('Folder', 'Utility');
class AppBuilderComponent extends Component {
    public $components = array('AppBuilder.ApplicationTemplate', 'AppBuilder.Zip');
    CONST PATH = "/var/www/my.negozy.com/html/app/tmp";


    public $structure = array();

    private function defineFileEntity($templateUrl, $templateParams, $extension) {
        return array(
            'type' => 'file',
            'extension' => $extension,
            'template' => array(
                'url' => $templateUrl,
                'params' => $templateParams
            )
        );
    }

    private function defineFolderEntity($content = array()) {
        return array(
            'type' => 'folder',
            'content' => $content
        );
    }

    private function definePhpControllerStructure() {
        return $this->defineFolderEntity(array(
                'Component' => $this->defineFolderEntity(),
                $this->appControllerFilename => $this->defineFileEntity('app_controller', array(
                    'pluginAppControllerName' => $this->appControllerFilename
                ), 'php'),

                // Parte che potrebbe diventare dinamica
                'ExampleController' => $this->defineFileEntity('example_controller', array(
                    'controller_name'       => $this->exampleControllerFilename,
                    'app_controller_name'   => $this->appControllerFilename
                ), 'php'),
                'ServicesController' => $this->defineFileEntity('services_controller', array(
                    'controller_name'       => $this->servicesControllerFilename,
                    'app_controller_name'   => $this->appControllerFilename
                ), 'php')
                // Parte che potrebbe diventare dinamica
            )
        );
    }

    private function definePhpModelStructure() {
        return $this->defineFolderEntity();
    }

    private function definePhpViewStructure() {
        return $this->defineFolderEntity(array(
                // Parte che potrebbe diventare dinamica
                'Example' => $this->defineFolderEntity(array(
                        'application_index' => $this->defineFileEntity('view_dashboard', array(
                            'view_controller'       => $this->angularDashCtrlName,
                            'app_name'              => $this->app_name,
                            'request_uri'           => $this->request_uri
                        ), 'ctp'),
                        'application_list'  => $this->defineFileEntity('view_list', array(
                            'view_controller'       => $this->angularListCtrlName,
                            'app_name'              => $this->app_name,
                        ), 'ctp'))
                )
                // Parte che potrebbe diventare dinamica
            )
        );
    }

    private function definePhpWebrootStructure() {
        return $this->defineFolderEntity(array(
                'css' => $this->defineFolderEntity(),
                'js' => $this->defineFolderEntity(),

                // Parte che potrebbe diventare dinamica
                'script' => $this->defineFolderEntity(array(
                        $this->angularServicesFilename => $this->defineFileEntity('script_services', array(
                            'service'       => $this->angularServiceModuleName,
                            'request_uri'   => $this->request_uri,
                            'plugin_name'   => $this->pluginName
                        ), 'js'),
                        $this->angularControllerFilename => $this->defineFileEntity('script_ctrl', array(
                            'service'       => $this->angularServiceModuleName,
                            'dash_ctrl'     => $this->angularDashCtrlName,
                            'list_ctrl'     => $this->angularListCtrlName,
                            'request_uri'   => $this->request_uri,
                            'plugin_name'   => $this->pluginName
                        ), 'js'))
                )
                // Parte che potrebbe diventare dinamica
            )
        );
    }

    private function defineConfig() {
        return $this->defineFileEntity('config', array(
            'request_uri'       => $this->request_uri,
            'plugin_name'       => $this->pluginName,
            'ctrl_file_name'    => $this->angularControllerFilename,
            'service_file_name' => $this->angularServicesFilename
        ), 'json');
    }

    private function defineStructure() {
        $this->structure[$this->pluginName] = $this->defineFolderEntity(array(
                'Controller'    => $this->definePhpControllerStructure(),
                'Model'         => $this->definePhpModelStructure(),
                'View'          => $this->definePhpViewStructure(),
                'webroot'       => $this->definePhpWebrootStructure(),
                'config'        => $this->defineConfig()
            )
        );
    }

    private function isFile($entity) {
        return ($entity['type'] == 'file');
    }

    private function isFolder($entity) {
        return ($entity['type'] == 'folder');
    }

    private function isRootFolder($entityName) {
        return ($entityName == $this->pluginName);
    }

    private function createFolder($entityName) {
        return new Folder($this->folderLevel. DS .$entityName, true, 0777);
    }

    private function createFile($entityName, $entity) {
        $file = new File($this->folderLevel. DS .$entityName.'.'.$entity['extension'], true, 0777);
        $file->write($this->ApplicationTemplate->get($entity['template']['url'],$entity['template']['params']));
    }

    private function addLevel($entityName) {
        $this->folderLevel .= DS.$entityName;
    }

    private function removeLevel($entityName) {
        $this->folderLevel = str_replace(DS.$entityName,'',$this->folderLevel);
    }

    private function createStructure($structure) {
        foreach($structure as $entityName => $entity) {
            if($this->isFile($entity)) {
                $this->createFile($entityName, $entity);
                continue;
            } else if($this->isFolder($entity)) {
                if($this->isRootFolder($entityName)) {
                    $this->root = $this->createFolder($entityName);
                } else {
                    $this->createFolder($entityName);
                }
                $this->addLevel($entityName);
                $this->createStructure($entity['content']);
                $this->removeLevel($entityName);
            }
        }
    }

    private function init($request_uri, $app_name) {
        $this->request_uri = $request_uri;
        $this->app_name = $app_name;

        // camelize del request uri
        $this->pluginName = Inflector::camelize($request_uri);
        $this->folderLevel = self::PATH;
        $this->zipfile = $this->folderLevel. DS .$this->pluginName.'.zip';
    }

    private function defineEntityName() {
        $this->defineAngularEntityName();
        $this->definePhpEntityName();
    }

    private function defineAngularFileName() {
        $this->angularServicesFilename = $this->request_uri.'_services';
        $this->angularControllerFilename = $this->request_uri.'_ctrl';
    }

    private function defineAngularModuleName() {
        $this->angularServiceModuleName = $this->pluginName.'Services';
        $this->angularDashCtrlName  = $this->pluginName.'DashboardCtrl';
        $this->angularListCtrlName  = $this->pluginName.'ListCtrl';
    }

    private function defineAngularEntityName() {
        $this->defineAngularFileName();
        $this->defineAngularModuleName();
    }

    private function definePhpEntityName() {
        $this->appControllerFilename = $this->pluginName.'AppController';
        $this->exampleControllerFilename = 'ExampleController';
        $this->servicesControllerFilename = 'ServicesController';
    }

    public function create($request_uri, $app_name) {
        $this->init($request_uri, $app_name);
        $this->defineEntityName();
        $this->defineStructure();
        $this->createStructure($this->structure);

        $this->Zip->zipDir($this->root->path, $this->zipfile);
        // cancella la cartella del plugin dalla struttura di lavoro di cakephp
        if($this->root->delete()) {
            return $this->zipfile;
        }
    }


    /*
    private function foldersPaths() {
        $this->pluginRootPath = self::PATH. DS .$this->pluginName;
        $this->pluginControllerPath = $this->pluginRootPath. DS .'Controller';
        $this->pluginComponentPath = $this->pluginRootPath. DS .'Controller'. DS .'Component';
        $this->pluginModelPath = $this->pluginRootPath. DS .'Model';
        $this->pluginViewPath = $this->pluginRootPath. DS .'View';
        $this->pluginViewExamplePath = $this->pluginViewPath. DS .'Examples';
        $this->pluginWebrootPath = $this->pluginRootPath. DS .'webroot';
        $this->pluginWebrootCssPath = $this->pluginWebrootPath. DS .'css';
        $this->pluginWebrootJsPath = $this->pluginWebrootPath. DS .'js';
        $this->pluginWebrootScriptPath = $this->pluginWebrootPath. DS .'scripts';
    }

    private function filesPaths() {
        $this->pluginAppControllerPath = $this->pluginControllerPath. DS .$this->pluginAppControllerName.'.php';
        $this->pluginJSONConfigPath = $this->pluginRootPath. DS .'config.json';
        $this->pluginAngularControllerPath = $this->pluginWebrootScriptPath. DS .$this->pluginAngularControllerFileName;
        $this->pluginAngularServicesPath = $this->pluginWebrootScriptPath. DS .$this->pluginAngularServiceFileName;
        $this->pluginDashboardViewPath = $this->pluginViewExamplePath .DS .$this->pluginDashboardViewFileName;
        $this->pluginListViewPath = $this->pluginViewExamplePath .DS .$this->pluginListViewFileName;

        $this->pluginViewsControllerPath = $this->pluginControllerPath. DS .$this->pluginViewsControllerName.'.php';
        $this->pluginServicesControllerPath = $this->pluginControllerPath. DS .$this->pluginServicesControllerName.'.php';
    }

    private function filename() {
        $this->pluginAppControllerName = $this->pluginName."AppController";
        $this->pluginAngularControllerFileName = $this->request_uri.'_ctrl.js';
        $this->pluginAngularServiceFileName = $this->request_uri.'_services.js';

        $this->pluginAngularScriptServiceName = $this->pluginName.'Services';
        $this->pluginAngularScriptControllerDashboardName = $this->pluginName.'DashboardCtrl';
        $this->pluginAngularScriptControllerListName = $this->pluginName.'ListCtrl';

        $this->pluginDashboardViewName = 'application_index';
        $this->pluginListViewName = 'application_list';

        $this->pluginDashboardViewFileName = $this->pluginDashboardViewName.'.ctp';
        $this->pluginListViewFileName = $this->pluginListViewName.'.ctp';

        $this->pluginViewsControllerName = 'ExamplesController';
        $this->pluginServicesControllerName = 'ServicesController';

    }

    private function createFolders() {
        $this->root = new Folder($this->pluginRootPath, true, 0777);
        new Folder($this->pluginControllerPath, true, 0777);
        new Folder($this->pluginComponentPath, true, 0777);
        new Folder($this->pluginModelPath, true, 0777);
        new Folder($this->pluginViewExamplePath, true, 0777);
        new Folder($this->pluginWebrootPath, true, 0777);
        new Folder($this->pluginWebrootCssPath, true, 0777);
        new Folder($this->pluginWebrootJsPath, true, 0777);
        new Folder($this->pluginWebrootScriptPath, true, 0777);
    }

    private function createAppController() {
        $pluginAppControllerFile = new File($this->pluginAppControllerPath, true, 0777);
        $pluginAppControllerFile->write($this->ApplicationTemplate->get('app_controller',array('pluginAppControllerName'=>$this->pluginAppControllerName)));
    }

    private function createConfig() {
        $pluginConfigParams = array(
            'request_uri' => $this->request_uri,
            'plugin_name' => $this->pluginName,
            'ctrl_file_name' => $this->pluginAngularControllerFileName,
            'service_file_name' => $this->pluginAngularServiceFileName,
        );
        $pluginJSONConfigFile = new File($this->pluginJSONConfigPath, true, 0777);
        $pluginJSONConfigFile->write($this->ApplicationTemplate->get('config',$pluginConfigParams));
    }

    private function createAngularController() {
        $scriptControllParams = array(
            'service' => $this->pluginAngularScriptServiceName,
            'dash_ctrl' => $this->pluginAngularScriptControllerDashboardName,
            'list_ctrl' => $this->pluginAngularScriptControllerListName,
            'request_uri' => $this->request_uri,
            'plugin_name' => $this->pluginName
        );
        $pluginAngularScriptCtrlFile = new File($this->pluginAngularControllerPath, true, 0777);
        $pluginAngularScriptCtrlFile->write($this->ApplicationTemplate->get('script_ctrl', $scriptControllParams));
    }

    private function createAngularScript() {
        $scriptServiceParams = array(
            'service' => $this->pluginAngularScriptServiceName,
            'request_uri'=>$this->request_uri,
            'plugin_name' => $this->pluginName
        );
        $pluginAngularScriptServiceFile = new File($this->pluginAngularServicesPath, true, 0777);
        $pluginAngularScriptServiceFile->write($this->ApplicationTemplate->get('script_services', $scriptServiceParams));
    }

    private function createViews() {
        $view1Params = array(
            'view_controller' => $this->pluginAngularScriptControllerDashboardName,
            'app_name' => $this->app_name,
            'request_uri' => $this->request_uri
        );
        $file1 = new File($this->pluginDashboardViewPath, true, 0777);
        $file1->write($this->ApplicationTemplate->get('view_dashboard', $view1Params));

        $view2Params = array(
            'view_controller' => $this->pluginAngularScriptControllerListName,
            'app_name' => $this->app_name,
        );
        $file2 = new File($this->pluginListViewPath, true, 0777);
        $file2->write($this->ApplicationTemplate->get('view_list', $view2Params));
    }

    private function createPHPControllerExample() {
        $params = array(
            'controller_name'=>$this->pluginViewsControllerName,
            'app_controller_name'=>$this->pluginAppControllerName
        );

        $file = new File($this->pluginViewsControllerPath, true, 0777);
        $file->write($this->ApplicationTemplate->get('example_controller',$params));
    }

    private function createPHPControllerServices() {
        $params = array(
            'controller_name' => $this->pluginServicesControllerName,
            'app_controller_name' => $this->pluginAppControllerName
        );

        $file = new File($this->pluginServicesControllerPath, true, 0777);
        $file->write($this->ApplicationTemplate->get('services_controller',$params));
    }


    public function structure($request_uri, $app_name) {

        $this->request_uri = $request_uri;
        $this->app_name = $app_name;

        // camelize del request uri
        $this->pluginName = Inflector::camelize($request_uri);

        // Path struttura cartelle
        $this->foldersPaths();

        // Zip file
        $this->zipfile = $this->pluginRootPath.'.zip';

        // File name
        $this->filename();

        // Path file
        $this->filesPaths();

        // Creazione directories
        $this->createFolders();

        $this->createAppController();

        // Creazione app controller file
        $this->createAppController();

        // Creazione json config file
        $this->createConfig();

        // Creazione script angular controller
        $this->createAngularController();

        // Creazione script angular controller
        $this->createAngularScript();

        // Creazione script angular service
        $this->createAngularScript();

        // Creazione views
        $this->createViews();

        // Creazione controller php per le view
        $this->createPHPControllerExample();

        // Creazione controller services php
        $this->createPHPControllerServices();

        // crea lo zip
        $this->Zip->zipDir($this->pluginRootPath, $this->zipfile);
        // cancella la cartella del plugin dalla struttura di lavoro di cakephp
        if ($this->root->delete()) {
            // ritorna il path dello zip
            return $this->zipfile;
        }
    }
    */
    public function delete() {
        unlink($this->zipfile);
    }
}
?>