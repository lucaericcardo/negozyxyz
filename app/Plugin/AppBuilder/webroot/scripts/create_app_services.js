(function() {
    'use strict';
    angular.module('plugin.AppBuilder.services', [])
        .factory("AppBuilderServices", function ($http) {
            return {
                add: function(app) {
                    return $http.post('/app_builder/services_app_builder/add.json',{app:app});
                },
                getData: function() {
                    return $http.get('/app_builder/services_app_builder/getData.json');
                },
                existRequestUri: function(request_uri) {
                    return $http.post('/app_builder/services_app_builder/existRequestUri.json',{request_uri:request_uri});
                },
                getApplications: function(n, page, row, orderType) {
                    return $http.get('/app_builder/services_app_builder/getApplications/' + n + '/' + page + '/' + row + '/' + orderType + '.json');
                },
                deleteApplication: function(app_id) {
                    return $http.post('/app_builder/services_app_builder/deleteApplication.json', {app_id:app_id});
                },
                getApplicationData: function(app_id) {
                    return $http.post('/app_builder/services_app_builder/getApplicationData.json', {app_id:app_id});
                },
                removeScreenshot: function(app_id, screenshot_id) {
                    return $http.post('/app_builder/services_app_builder/removeScreenshot.json', {app_id:app_id, screenshot_id:screenshot_id});
                },
                removeIcon: function(app_id, icon_id) {
                    return $http.post('/app_builder/services_app_builder/removeIcon.json', {app_id:app_id, icon_id:icon_id});
                }
            }
        })
}).call(this);
