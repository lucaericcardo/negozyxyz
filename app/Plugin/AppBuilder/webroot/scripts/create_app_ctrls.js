(function() {
    'use strict';
    var create_app = angular.module('plugin.AppBuilder.controllers', []);
    create_app.controller('AppBuilderDashboardCtrl',  function($scope,$activityIndicator,AppBuilderServices, $location, $sce) {
        $activityIndicator.startAnimating();
        $scope.numPerPageOpt        = [5, 10, 20];
        $scope.numPerPage           = $scope.numPerPageOpt[1];
        $scope.currentPage          = 1;
        $scope.currentPageStores    = [];
        $scope.applications         = [];
        $scope.totalApplications    = 0;
        $scope.row                  = 'created';
        $scope.orderType            = 'DESC';

        $scope.deleteApplication = function(app_id) {
            AppBuilderServices.deleteApplication(app_id).success(function(r) {
                if(r.data.success) {
                    location.reload();
                }
                $activityIndicator.stopAnimating();
            });
        }

        $scope.select = function(page) {
            $scope.deselectAll  = true;
            $scope.selectAll    = false;

            AppBuilderServices.getApplications($scope.numPerPage,page,$scope.row,$scope.orderType).success(function(r) {
                if(r.data.success) {
                    $scope.applications = r.data.applications;
                    $scope.totalApplications = r.data.count;

                    for(var k in $scope.applications) {
                        var html = '';
                        for(var i in $scope.applications[k].installation) {
                            html += '<p>'+$scope.applications[k].installation[i]+'</p>';
                        }
                        $scope.applications[k].installation = html;
                    }
                }
                $activityIndicator.stopAnimating();
            });
        };

        $scope.onFilterChange = function() {
            $scope.select(1);
            $scope.currentPage = 1;
            return $scope.row = '';
        };
        $scope.onNumPerPageChange = function() {
            $scope.select(1);
            return $scope.currentPage = 1;
        };
        $scope.onOrderChange = function() {
            $scope.select(1);
            return $scope.currentPage = 1;
        };
        $scope.order = function(rowName,orderType) {
            if ($scope.row === rowName && $scope.orderType === orderType) {
                return;
            }
            $activityIndicator.startAnimating();
            $scope.row = rowName;
            $scope.orderType = orderType;
            return $scope.onOrderChange();
        };

        var init = function() {
            return $scope.select($scope.currentPage);
        };
        return init();

    }).controller('AppBuilderAddCtrl',  function($scope,$activityIndicator,AppBuilderServices,$location) {
        $activityIndicator.startAnimating();

        $scope.categories = [];
        $scope.developers = [];
        $scope.states = [];
        $scope.app = {};
        $scope.app.is_admin = false;
        $scope.app.enabled = false;
        $scope.notUniqueReqUri = false;

        AppBuilderServices.getData().success(function(r) {
            if(r.data.success) {
                $scope.categories = r.data.categories;
                $scope.developers = r.data.developers;
                $scope.states = r.data.states;

                $scope.app.category_id = $scope.categories[0].id;
                $scope.app.company_id = $scope.developers[0].id;
                $scope.app.status = $scope.states[0];
            }
            $activityIndicator.stopAnimating();
        });


        $scope.isUnique = function() {
            AppBuilderServices.existRequestUri($scope.app.request_uri).success(function(r) {
                if(r.data.success && !r.data.isUnique) {
                    $scope.notUniqueReqUri = true;
                } else if(r.data.isUnique) {
                    $scope.notUniqueReqUri = false;
                }
            });
        }

        $scope.add = function() {
            AppBuilderServices.add($scope.app).success(function(r) {
                if(r.data.success) {
                    $location.path("/app/app_builder");
                }
            });
        }
    }).controller('AppBuilderEditCtrl',  function($routeParams, FileUploader, $scope,$activityIndicator,AppBuilderServices,$location) {
        $activityIndicator.startAnimating();

        $scope.categories = [];
        $scope.developers = [];
        $scope.states = [];
        $scope.app = {};
        $scope.images = [];
        $scope.icon = {};
        $scope.icon.image = {};
        $scope.app.is_admin = false;
        $scope.app.enabled = true;
        $scope.notUniqueReqUri = false;

        AppBuilderServices.getData().success(function(r) {
            if(r.data.success) {
                $scope.categories = r.data.categories;
                $scope.developers = r.data.developers;
                $scope.states = r.data.states;

                AppBuilderServices.getApplicationData($routeParams.application_id).success(function(r) {
                    if(r.data.success) {
                        $scope.app = r.data.application.Application;
                        $scope.images = r.data.application.AppScreenshot;
                        $scope.icon.image = r.data.application.AppIcon;
                        $scope.setUrlUploaderIcon();
                    }
                    $activityIndicator.stopAnimating();
                });
            }
        });

        $scope.edit = function() {
            AppBuilderServices.add($scope.app).success(function(r) {
                if(r.data.success) {
                    $location.path("/app/app_builder");
                }
            });
        }

        $scope.uploaderScreenshot = new FileUploader({
            url: "/app_builder/services_app_builder/uploadScreenshot/"+$routeParams.application_id+".json",
            onBeforeUploadItem: function(item){
                $scope.onLoading = true;
                item.url = $scope.uploaderScreenshot.url;
                $scope.current_image = item._file.name;
            },
            onAfterAddingAll : function(item)  {
                $scope.progress = 0;
                $scope.uploaderScreenshot.uploadAll();
            },
            onProgressAll: function(progress){
                $scope.progress = progress;
            },
            onSuccessItem: function(item, response){
                if(response.data.success) {
                    var newImage = {
                        url: response.data.url,
                        id: response.data.id
                    }
                    $scope.images.push(newImage);
                    $scope.count_image++;
                }
                $scope.onLoading = false;
            },
            onErrorItem: function(item, response, status, headers){
                $scope.onLoading = false;
            }
        });

        $scope.uploaderIcon = new FileUploader({
            onBeforeUploadItem: function(item){
                $scope.icon.onLoading = true;
                item.url = $scope.uploaderIcon.url;
                $scope.icon.current_image = item._file.name;
            },
            onAfterAddingAll : function(item)  {
                $scope.icon.progress = 0;
                $scope.uploaderIcon.uploadAll();
            },
            onProgressAll: function(progress){
                $scope.icon.progress = progress;
            },
            onSuccessItem: function(item, response){
                if(response.data.success) {
                    $scope.icon.image = {
                        url: response.data.url,
                        id: response.data.id
                    };
                    $scope.setUrlUploaderIcon();
                    $scope.icon.count_image++;
                }
                $scope.icon.onLoading = false;
            },
            onErrorItem: function(item, response, status, headers){
                $scope.icon.onLoading = false;
            }
        });

        $scope.setUrlUploaderIcon = function() {
            $scope.uploaderIcon.url = "/app_builder/services_app_builder/uploadIcon/"+$routeParams.application_id+"/"+$scope.icon.image.id+".json";
        }

        $scope.removeIcon = function(icon_id) {
            if(icon_id) {
                AppBuilderServices.removeIcon($routeParams.application_id,icon_id).success(function (r) {
                    if(r.data.success) {
                        angular.forEach($scope.icon.image, function(value, key) {
                            if(value.id == icon_id) {
                                $scope.images.splice(key, 1);
                            }
                        });
                    }
                });
            }
        };

        $scope.removeScreenshot = function(screenshot_id) {
            if(screenshot_id) {
                AppBuilderServices.removeScreenshot($routeParams.application_id,screenshot_id).success(function (r) {
                    if(r.data.success) {
                        angular.forEach($scope.images, function(value, key) {
                            if(value.id == screenshot_id) {
                                $scope.images.splice(key, 1);
                            }
                        });
                    }
                });
            }
        };


    })
}).call(this);
