<?php
echo
'{
    "name" : "'.$request_uri.'",
    "views": {
        "dashboard": {
            "template": "/application/'.$request_uri.'/examples.html",
            "url": "/app/'.$request_uri.'"
        },
        "list": {
            "template": "/application/'.$request_uri.'/examples/list.html",
            "url": "/app/'.$request_uri.'/list"
        }
    },
    "js": [],
    "css": [],
    "scripts": {
        "plugin.'.$plugin_name.'.controllers":"/'.$request_uri.'/scripts/'.$ctrl_file_name.'.js",
        "plugin.'.$plugin_name.'.services":"/'.$request_uri.'/scripts/'.$service_file_name.'.js"
    }
}';