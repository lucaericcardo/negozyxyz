<?php
echo
"(function() {
    'use strict';
    angular.module('plugin.$plugin_name.services', [])
        .factory('$service', function (\$http) {
            return {
                hello: function() {
                    return \$http.post('/$request_uri/services/hello.json');
                },
                congratulations: function() {
                    return \$http.get('/$request_uri/services/congratulations.json');
                }
            }
        })
}).call(this);";