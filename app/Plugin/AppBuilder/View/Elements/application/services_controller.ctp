<?php
echo
'<?php
class '.$controller_name.' extends '.$app_controller_name.' {

    public function hello() {
        $this->response->compress();
        $data["success"] = true;

        $data["message"] = "Ciao ".$this->Auth->user("first_name"). " ".$this->Auth->user("last_name");

        $this->set(array(
            "data" => $data,
            "_serialize"=>array("data")
        ));
    }

    public function congratulations() {
        $this->response->compress();
        $data["success"] = true;

        $data["message"] = "Complimenti l\'applicazione è stata installata con successo!";

        $this->set(array(
            "data" => $data,
            "_serialize"=>array("data")
        ));
    }
}
';