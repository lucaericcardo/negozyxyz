<?php
echo
"(function() {
    'use strict';
    var $request_uri = angular.module('plugin.$plugin_name.controllers', []);
    $request_uri.controller('$dash_ctrl',  function(\$scope,\$activityIndicator,$service) {
        \$activityIndicator.startAnimating();
        $service.hello().success(function(r) {
            if(r.data.success) {
                \$scope.print = r.data.message;
            }
            \$activityIndicator.stopAnimating();
        });
    }).controller('$list_ctrl',  function(\$scope,\$activityIndicator,$service) {
        \$activityIndicator.startAnimating();
        $service.congratulations().success(function(r) {
            if(r.data.success) {
                \$scope.print = r.data.message;
            }
            \$activityIndicator.stopAnimating();
        });
    })
}).call(this);";