<div class="lapagina" data-ng-controller="AppBuilderDashboardCtrl">
    <nav class="navbar navbar-default navbar-static-top" role="navigation">
        <div class="navbar-header">
            <ul class="nav navbar-nav">
                <li>
                    <ol class="breadcrumb">
                        <li class="active"><?php echo __("App Builder"); ?></li>
                    </ol>
                </li>
            </ul>
            <ul class="nav navbar-nav navbar-right">
                <div class="navbar-form">
                    <a href="#/app/app_builder/add" class="btn btn-success" type="submit"><i class="fa fa-plus"></i> <?php echo __("Aggiungi Applicazione"); ?></a>
                </div>
            </ul>
        </div>
    </nav>

    <div class="page page-table">
        <div class="row">
            <div class="col-lg-10 col-lg-offset-1 col-md-12">
                <section class="panel panel-default table-dynamic">
                    <div class="panel-body">
                        <div class="table-responsive">
                            <table class="table table-bordered table-striped table-responsive">
                                <thead>
                                <tr>
                                    <th>
                                        <div class="th">
                                            <?php echo __("Nome"); ?>
                                            <span class="glyphicon glyphicon-chevron-up" data-ng-click=" order('name','ASC') " data-ng-class="{active: row == 'name' && orderType == 'ASC'}"></span>
                                            <span class="glyphicon glyphicon-chevron-down" data-ng-click=" order('name','DESC') " data-ng-class="{active: row == 'name' && orderType == 'DESC'}"></span>
                                        </div>
                                    </th>
                                    <th>
                                        <div class="th">
                                            <?php echo __("Request Uri"); ?>
                                            <span class="glyphicon glyphicon-chevron-up" data-ng-click=" order('request_uri','ASC') " data-ng-class="{active: row == 'request_uri' && orderType == 'ASC'}"></span>
                                            <span class="glyphicon glyphicon-chevron-down" data-ng-click=" order('request_uri','DESC') " data-ng-class="{active: row == 'request_uri' && orderType == 'DESC'}"></span>
                                        </div>
                                    </th>
                                    <th>
                                        <div class="th">
                                            <?php echo __("Data di creazione"); ?>
                                            <span class="glyphicon glyphicon-chevron-up" data-ng-click=" order('created','ASC') " data-ng-class="{active: row == 'created' && orderType == 'ASC'}"></span>
                                            <span class="glyphicon glyphicon-chevron-down" data-ng-click=" order('created','DESC') " data-ng-class="{active: row == 'created' && orderType == 'DESC'}"></span>
                                        </div>
                                    </th>
                                    <th>
                                        <div class="th">
                                            <?php echo __("N. Installazioni"); ?>
                                            <span class="glyphicon glyphicon-chevron-up" data-ng-click=" order('installation_count','ASC') " data-ng-class="{active: row == 'installation_count' && orderType == 'ASC'}"></span>
                                            <span class="glyphicon glyphicon-chevron-down" data-ng-click=" order('installation_count','DESC') " data-ng-class="{active: row == 'installation_count' && orderType == 'DESC'}"></span>
                                        </div>
                                    </th>
                                    <th>
                                        <div class="th">
                                            <?php echo __("Admin"); ?>
                                        </div>
                                    </th>
                                    <th class="text-right" width="120">
                                        <div class="th">
                                            <?php echo __("Azioni"); ?>
                                        </div>
                                    </th>
                                </tr>
                                </thead>
                                <tbody>


                                <tr data-ng-repeat="application in applications" data-ng-class="{'warning':(application.request_uri=='app_builder')}">
                                    <td>{{application.name}}</td>
                                    <td>{{application.request_uri}}</td>
                                    <td>{{application.timestamp | date:'dd/MM/yyyy  H:mm'}}</td>
                                    <td><a href="javascript:;" tooltip-html-unsafe="{{application.installation}}" tooltip-placement="bottom">{{application.installation_count}}</a></td>
                                    <td>{{(application.is_admin) ? 'Si' : 'No'}}</td>
                                    <td class="text-right">
                                        <a class="btn-icon btn-icon-sm bg-success" data-ng-show="application.zip_structure" title="<?php echo __('Scarica zip struttura'); ?>" data-ng-href="/app_builder/services_app_builder/sendFile/{{application.id}}.json"><i class="fa fa-download"></i></a>
                                        <a class="btn-icon btn-icon-sm bg-primary" data-ng-href="#/app/app_builder/edit/{{application.id}}"><i class="fa fa-pencil-square-o"></i></a>
                                        <a class="btn-icon btn-icon-sm bg-danger" data-ng-href="javascript:;" confirm-callback="deleteApplication(application.id)" confirm-message="<?php echo __("Sei sicuro di voler eliminare l'applicazione ?"); ?>" confirm-modal><i class="glyphicon glyphicon-trash"></i></a>
                                    </td>
                                </tr>
                                </tbody>
                            </table>
                        </div>
                        <footer class="table-footer">
                            <div class="row">
                                <div class="col-md-6 page-num-info">
                                    <span>
                                        <?php echo __("Elementi per pagina"); ?>
                                        <select data-ng-model="numPerPage" data-ng-options="num for num in numPerPageOpt" data-ng-change="onNumPerPageChange()">
                                        </select>
                                    </span>
                                </div>
                                <div class="col-md-6 text-right pagination-container">
                                    <pagination class="pagination-sm" ng-model="currentPage" total-items="totalApplications" max-size="4" ng-change="select(currentPage)" items-per-page="numPerPage" rotate="false" boundary-links="true"></pagination>
                                </div>
                            </div>
                        </footer>
                    </div>
                </section>
            </div>
        </div>
    </div>
</div>