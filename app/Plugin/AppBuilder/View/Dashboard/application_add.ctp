<div class="lapagina" data-ng-controller="AppBuilderAddCtrl">
    <nav class="navbar navbar-default navbar-static-top" role="navigation">
        <div class="navbar-header">
            <ul class="nav navbar-nav">
                <li>
                    <ol class="breadcrumb">
                        <li class="active"><a><?php echo __("App Builder"); ?></a></li>
                    </ol>
                </li>
            </ul>
            <ul class="nav navbar-nav navbar-right">
                <div class="navbar-form">
                    <a href="#/app/app_builder" class="btn btn-warning" type="submit"><i class="fa fa-arrow-left"></i> <?php echo __("Torna alla lista"); ?></a>
                </div>
            </ul>
        </div>
    </nav>
    <div class="page page-table">

        <div class="row">
            <div class="col-lg-10 col-lg-offset-1 col-md-12">
                <section class="panel panel-default table-dynamic ng-scope">
                    <div class="panel-body">
                        <form class="form-horizontal" role="form" name="form" novalidate>
                            <div class="form-group" data-ng-class="{'has-error':form.nome.$invalid && !form.nome.$pristine}">
                                <label for="nome" class="col-sm-2 control-label"><?php echo __('Nome'); ?></label>
                                <div class="col-sm-10">
                                    <input type="text" class="form-control" id="nome" name="nome" data-ng-model="app.name" placeholder="<?php echo __('Nome'); ?>" required>
                                </div>
                            </div>
                            <div class="form-group">
                                <label for="descrizione" class="col-sm-2 control-label"><?php echo __('Descrizione'); ?></label>
                                <div class="col-sm-10">
                                    <textarea class="form-control" id="descrizione" data-ng-model="app.description" placeholder="<?php echo __('Descrizione'); ?>"></textarea>
                                </div>
                            </div>
                            <div class="form-group" data-ng-class="{'has-error':(form.req_uri.$invalid && !form.req_uri.$pristine) || notUniqueReqUri}">
                                <label for="req_uri" class="col-sm-2 control-label"><?php echo __('Request uri'); ?></label>
                                <div class="col-sm-10">
                                    <input type="text" class="form-control" id="req_uri" name="req_uri" data-ng-model="app.request_uri" placeholder="<?php echo __('Request uri'); ?>" data-ng-blur="isUnique()" required>
                                    <small class="text-danger" data-ng-show="notUniqueReqUri"><?php echo __('Request uri già utilizzato. Scegliere un altro request uri!'); ?></small>
                                </div>
                            </div>
                            <div class="form-group">
                                <label for="last_version" class="col-sm-2 control-label"><?php echo __('Ultima versione'); ?></label>
                                <div class="col-sm-10">
                                    <input type="text" class="form-control" id="last_version" data-ng-model="app.last_version" placeholder="<?php echo __('Ultima versione'); ?>">
                                </div>
                            </div>
                            <div class="form-group">
                                <label for="req_uri" class="col-sm-2 control-label"><?php echo __('Categoria'); ?></label>
                                <div class="col-sm-10">
                                    <span class="ui-select" style="margin:inherit">
                                        <select data-ng-options="category.id as category.label for category in categories" data-ng-model="app.category_id"></select>
                                    </span>
                                </div>
                            </div>
                            <div class="form-group">
                                <label for="req_uri" class="col-sm-2 control-label"><?php echo __('Sviluppatore'); ?></label>
                                <div class="col-sm-10">
                                    <span class="ui-select" style="margin:inherit">
                                        <select data-ng-options="developer.id as developer.label for developer in developers" data-ng-model="app.company_id"></select>
                                    </span>
                                </div>
                            </div>
                            <div class="form-group">
                                <label for="req_uri" class="col-sm-2 control-label"><?php echo __('Stato'); ?></label>
                                <div class="col-sm-10">
                                    <span class="ui-select" style="margin:inherit">
                                        <select data-ng-options="state as state for state in states" data-ng-model="app.status"></select>
                                    </span>
                                </div>
                            </div>
                            <div class="form-group">
                                <label for="req_uri" class="col-sm-2 control-label"><?php echo __('Admin'); ?></label>
                                <div class="col-sm-10">
                                    <label class="switch switch-warning"><input type="checkbox" data-ng-model="app.is_admin"><i></i></label>
                                </div>
                            </div>
                            <div class="form-group">
                                <label for="req_uri" class="col-sm-2 control-label"><?php echo __('Abilitata'); ?></label>
                                <div class="col-sm-10">
                                    <label class="switch switch-warning"><input type="checkbox" data-ng-model="app.enabled"><i></i></label>
                                </div>
                            </div>
                            <div class="form-group">
                                <div class="col-sm-offset-2 col-sm-10">
                                    <button type="button" data-ng-click="add()" class="btn btn-success" data-ng-disabled="form.$invalid || notUniqueReqUri"><?php echo __('SALVA'); ?></button>
                                </div>
                            </div>
                        </form>
                    </div>
                </section>
            </div>
        </div>

    </div>
</div>
