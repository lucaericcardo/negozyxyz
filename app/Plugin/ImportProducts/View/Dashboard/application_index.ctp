<div class="lapagina" data-ng-controller="ImportProductsDashboardCtrl">
    <nav class="navbar navbar-default navbar-static-top" role="navigation">
        <div class="navbar-header">
            <ul class="nav navbar-nav">
                <li>
                    <ol class="breadcrumb">
                        <li class="active">Import Products</li>
                    </ol>
                </li>
            </ul>
        </div>
    </nav>

    <div class="page page-table">
        <div class="row">
            <div class="col-lg-10 col-lg-offset-1 col-md-12">
                <section class="panel panel-default table-dynamic">
                    <div class="panel-body">
                        <div data-ng-show="!loaded">
                            <div class="form-group">
                                <label>Shop</label>
                                <span class="ui-select">
                                    <select data-ng-model="data.shop_id" data-ng-options="shop.id as shop.name for shop in shops"></select>
                                </span>
                            </div>
                            <div class="form-group">
                                <input type="file" data-ng-model="data.csv" title="Upload CSV" data-ui-file-upload nv-file-select uploader="uploaderCSV">
                            </div>
                            <button data-ng-click="uploadCsv()" class="btn btn-success">Carica prodotti</button>
                        </div>
                        <div ng-show="loaded">
                            <table class="table">
                                <thead>
                                <tr>
                                    <th>Prodotto</th>
                                    <th>Status</th>
                                </tr>
                                </thead>
                                <tbody>
                                <tr data-ng-repeat="product in products">
                                    <td>{{product.name}}</td>
                                    <td><span class="label" data-ng-class="product.class">{{product.status}}</span></td>
                                </tr>
                                </tbody>
                            </table>
                        </div>
                    </div>
                </section>
            </div>
        </div>
    </div>
</div>