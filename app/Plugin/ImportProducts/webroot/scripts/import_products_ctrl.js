(function() {
    'use strict';
    var import_products = angular.module('plugin.ImportProducts.controllers', []);
    import_products.controller('ImportProductsDashboardCtrl',  function($scope,$activityIndicator,ImportProductsShop,FileUploader,$q) {
        $activityIndicator.startAnimating();
        $scope.shops = [];
        $scope.data = {
            "shop_id" : null,
            "csv"     : null
        };
        $scope.loaded = false;
        $scope.uploaderCSV = new FileUploader({
            url: "/import_products/services/upload_products",
            onBeforeUploadItem: function(item){
                item.url = $scope.uploaderCSV.url;
                $scope.onLoading = true;
            },
            onCompleteAll: function() {
                $scope.successSave  = true;
                $scope.onLoading    = false;
            },
            onProgressAll: function(progress){
                $scope.progress = progress;
            },
            onSuccessItem: function(item, response){
                $scope.loaded = true;
                $scope.products = response.data.products;
                var promises = [];

                angular.forEach($scope.products,function(val,k){
                    promises.push(ImportProductsShop.uploadImage($scope.products[k].id,$scope.products[k].image,k));
                });
                $q.all(promises).then(function success(data){
                    angular.forEach(data,function(val,k){
                        $scope.products[data[k].config.data.key].status = data[k].data.data.status;
                        $scope.products[data[k].config.data.key].class  = data[k].data.data.class;
                    });
                }, function failure(err){
                    // Can handle this is we want
                });
                /*
                for(var k in $scope.products) {
                    ImportProductsShop.uploadImage($scope.products[k].id,$scope.products[k].image).success(function(r){
                        if(r.data.success) {
                            $scope.products[k].status = r.data.status;
                            $scope.products[k].class = r.data.class;
                        }
                    })
                }*/
            },
            onErrorItem: function(item, response, status, headers){
                $scope.error_images.push(item._file.name);
            }
        });
        $scope.uploadCsv = function () {
            if ($scope.uploaderCSV.queue.length) {
                $scope.uploaderCSV.url =  $scope.uploaderCSV.url+"/"+$scope.data.shop_id+".json";
                console.log($scope.uploaderCSV.url);
                $scope.uploaderCSV.uploadAll();
            } else {
                $scope.successSave = true;
            }
        }

        ImportProductsShop.list().success(function(r) {
            $scope.shops = r.shops;
            $activityIndicator.stopAnimating();
        });
    }).controller('ImportProductsListCtrl',  function($scope,$activityIndicator,ImportProductsServices) {
        $activityIndicator.startAnimating();
        ImportProductsServices.congratulations().success(function(r) {
            if(r.data.success) {
                $scope.print = r.data.message;
            }
            $activityIndicator.stopAnimating();
        });
    })
}).call(this);