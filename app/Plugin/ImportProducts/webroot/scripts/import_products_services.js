(function() {
    'use strict';
    angular.module('plugin.ImportProducts.services', [])
        .factory('ImportProductsServices', function ($http) {
            return {
                hello: function() {
                    return $http.post('/import_products/services/hello.json');
                },
                congratulations: function() {
                    return $http.get('/import_products/services/congratulations.json');
                }
            }
        })
        .factory('ImportProductsShop', function ($http) {
            return {
                list: function() {
                    return $http.post('/import_products/services/shop_list.json');
                },
                uploadImage: function(product_id,image_url,k){
                    return $http.post('/import_products/services/upload_image.json',{product_id:product_id,image_url:image_url,key:k});
                }
            }
        })
}).call(this);