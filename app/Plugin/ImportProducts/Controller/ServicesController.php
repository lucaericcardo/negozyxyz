<?php
class ServicesController extends ImportProductsAppController {

    public $components = array("Image");

    public function hello() {
        $this->response->compress();
        $data["success"] = true;

        $data["message"] = "Ciao ".$this->Auth->user("first_name"). " ".$this->Auth->user("last_name");

        $this->set(array(
            "data" => $data,
            "_serialize"=>array("data")
        ));
    }

    public function congratulations() {
        $this->response->compress();
        $data["success"] = true;

        $data["message"] = "Complimenti l'applicazione è stata installata con successo!";

        $this->set(array(
            "data" => $data,
            "_serialize"=>array("data")
        ));
    }

    public function shop_list(){
        $this->loadModel("Shop");
        $shops = $this->Shop->find("all",array(
            "fields" => array(
                "id",
                "name"
            ),
            "recursive" => -1,
            "order" => "name ASC"
        ));
        $shops = Set::extract("/Shop/.",$shops);
        $this->set(array(
            "shops" => $shops,
            "_serialize"=>array("shops")
        ));
    }

    public function upload_products($shop_id = NULL){
        App::uses('CakeNumber', 'Utility');
        $data['products']   = array();
        $taxes          = array();
        $collections    = array();
        $this->loadModel("Product");
        $this->loadModel("ShopTax");
        $this->loadModel("Collection");
        if(isset($_FILES['file'])){
            $handle = fopen($_FILES['file']['tmp_name'], "r");
            $header = fgetcsv($handle);
            while (($row = fgetcsv($handle,10000,';')) !== FALSE) {
                $price = str_replace(",",".",$row[2]);
                $price = str_replace("'","",$price);
                $price = floatval($price);
                $price = CakeNumber::format($price, array(
                    'places' => 2,
                    'before' => false,
                    'escape' => false,
                    'decimals' => '.',
                    'thousands' => false
                ));
                $newProduct['Product'] = array(
                    "name"          => $row[0],
                    "description"   => $row[1],
                    "price"         => $price,
                    "quantity"      => (int)$row[3],
                    "published"     => $row[4],
                    "shop_id"       => $shop_id,
                    "url"           => Inflector::slug($row[0],"_"),
                    "type_quantity" => (int)$row[8]
                );
                $tax = $this->ShopTax->find("first",array(
                    "conditions" => array(
                        "tax"           => $row[5],
                        "product_tax"   => $row[6],
                        "shop_id"       => $shop_id
                    ),
                    "fields" => array(
                        "id"
                    ),
                    "recursive" => -1
                ));
                if(isset($taxes[$row[5]]) && isset($taxes[$row[5]][$row[6]])) {
                    $newProduct['Product']['shop_tax_id'] = $taxes[$row[5]][$row[6]];
                } else {
                    if ($tax) {
                        $taxes[$row[5]][$row[6]] = $tax['ShopTax']['id'];
                        $newProduct['Product']['shop_tax_id'] = $tax['ShopTax']['id'];
                    } else {
                        $newTax = array(
                            "id" => NULL,
                            "shop_id" => $shop_id,
                            "tax" => $row[5],
                            "product_tax" => $row[6],
                            "name" => "IVA",
                            "label" => "IVA " . $row[5] . "%"
                        );
                        $this->ShopTax->create();
                        if ($this->ShopTax->save($newTax)) {
                            $newProduct['Product']['shop_tax_id'] = $this->ShopTax->id;
                            $taxes[$row[5]][$row[6]] = $this->ShopTax->id;
                        }
                    }
                }

                if(isset($collections[$row[7]])) {
                    $newProduct['Collection'] = array($collections[$row[7]]);
                } else {
                    $collection = $this->Collection->find("first", array(
                        "conditions" => array(
                            "shop_id" => $shop_id,
                            "name" => $row[7]
                        ),
                        "fields" => array(
                            "id"
                        ),
                        "recursive" => -1
                    ));
                    if ($collection) {
                        $newProduct['Collection'] = array($collection['Collection']['id']);
                        $collections[$row[7]] = $collection['Collection']['id'];
                    } else {
                        $newCollection = array(
                            "id" => NULL,
                            "name" => $row[7],
                            "shop_id" => $shop_id,
                            "color" => "bg-cyan"
                        );
                        $this->Collection->create();
                        if ($this->Collection->save($newCollection)) {
                            $newProduct['Collection'] = array($this->Collection->id);
                            $collections[$row[7]] = $this->Collection->id;
                        }
                    }
                }
                if($this->Product->saveAll($newProduct)) {
                    $data['products'][] = array(
                        "id"        => $this->Product->id,
                        "name"      => $row[0],
                        "image"     => $row[8],
                        "status"    => "Pending",
                        "class"     => "label-info"
                    );
                }
            }
        }
        $this->set(array(
            "data" => $data,
            "_serialize"=>array("data")
        ));
    }

    public function upload_image(){
        $this->loadModel("ProductImage");
        $this->loadModel("ProductImage");
        $data['success'] = false;
        $data['url'] = null;
        $data['name'] = null;
        if($this->request->is("post")) {
            $tempFilename = TMP.'tempFiles/';
            $tempFilename .= uniqid().'_'.basename( $this->request->data['image_url']);
            if( $imgContent = file_get_contents( $this->request->data['image_url'] ) ){
                if( file_put_contents( $tempFilename , $imgContent ) ){
                    $size = getimagesize($tempFilename);
                    $image = array(
                        "name"      => $this->request->data['image_url'],
                        "type"      => $size['mime'],
                        "tmp_name"  => $tempFilename,
                        "error"     => 0,
                        "size"      => filesize($tempFilename)
                    );
                    $types = array();
                    $types[] = array("width" => 450, "height" => null, "prefix" => "thumb_");
                    $types[] = array("width" => 800, "height" => null, "prefix" => "image_");
                    $image_data = $this->Image->uploadImage($image,'products',$types);
                    if ($image_data['success']) {
                        $newImage = array();
                        $newImage['ProductImage'] = $image_data['image'];
                        $newImage['ProductImage']['product_id'] = $this->request->data['product_id'];
                        $last_image = $this->ProductImage->find("first",array(
                            "conditions" => array(
                                "product_id" => $this->request->data['product_id']
                            )
                        ));
                        if(!$last_image) {
                            $newImage['ProductImage']['order'] = 1;
                            $newImage['ProductImage']['primary'] = 1;
                        } else {
                            $newImage['ProductImage']['order'] = $last_image['ProductImage']['order'] + 1;
                            $newImage['ProductImage']['primary'] = 0;
                        }
                        if($this->ProductImage->save($newImage)) {
                            $data['success'] = true;
                            $data['url']    = "http://cdn.negozy.com/images/products/".$image_data['image']['path']."thumb_".$image_data['image']['name'].".".$image_data['image']['ext'];
                            $data['name']   = $image_data['image']['name'];
                            $data['status'] = "Uploaded";
                            $data['class']  = "label-success";
                        }
                    }


                }
            }
            $this->set(array(
                "data" => $data,
                "_serialize"=>array("data")
            ));
        }
    }
}
