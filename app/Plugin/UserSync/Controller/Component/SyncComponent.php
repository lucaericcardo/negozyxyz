<?php
class SyncComponent extends Component {

    public function getLastUserSync($source = NULL){
        if(isset($source)) {
            $this->UserSync = ClassRegistry::init('UserSync.UserSync');
            $last_sync_id =  $this->UserSync->find("first",
                array(
                    "conditions" => array(
                        "name" => $source
                    ),
                    "fields" => array(
                        "last_sync_id"
                    )
                )
            );
            if($last_sync_id) {
                return $last_sync_id['UserSync']['last_sync_id'];
            } else {
                return false;
            }
        }
    }

    public function updateMax($source = NULL){
        $this->UserSync = ClassRegistry::init('UserSync.UserSync');
        $max_user = $this->geMaxUser();
        $last_sync_id =  $this->UserSync->find("first",
            array(
                "conditions" => array(
                    "name" => $source
                ),
                "fields" => array(
                    "id"
                )
            )
        );
        if($last_sync_id) {
            $this->UserSync->id = $last_sync_id['UserSync']['id'];
            $this->UserSync->save(
                array("last_sync_id" => $max_user)
            );
        }
    }

    public function geMaxUser(){
        $this->User = ClassRegistry::init('User');
        $max_id =  $this->User->find("first",
            array(
                "fields" => array(
                    "max(User.id) as max_id"
                ),
                "conditions" => array(
                    "deleted" => false
                )
            )
        );
        return $max_id[0]['max_id'];
    }

}