<?php
class UserSyncCronJobComponent extends Component {

    public $components = array("UserSync.Sync");

    private $jobs = array();

    public function main(){
        return $this->jobs[] = array(
            "job_1" => array(
                "call" => "sync_LimeSurvey",
                "time" => '0 17 * * *'
            )
        );
    }

    public function sync_LimeSurvey(){
        $last_user_id = $this->Sync->getLastUserSync("limeSurvey");
        $this->User = ClassRegistry::init('User');
        $this->LimeSurvey = ClassRegistry::init('UserSync.LimeSurvey');
        if(isset($last_user_id)) {
            $users_to_sync = $this->User->find("all",array(
                "conditions" => array(
                    "id > " => $last_user_id,
                ),
                "recursive" => -1
            ));
        } else {
            $users_to_sync = $this->User->find("all",array(
                "recursive" => -1
            ));
        }
        $n = 0;
        foreach($users_to_sync as $user) {
            $newUser = array(
                "participant_id"    => $user['User']['lead_id'],
                "firstname"         => $user['User']['first_name'],
                "lastname"          => $user['User']['last_name'],
                "email"             => $user['User']['email'],
                "blacklisted"       => "N",
                "owner_uid"         => 1,
                "created_by"        => 1,
                "language"          => "it",
                "created"           => $user['User']['created'],
            );
            $this->LimeSurvey->create();
            if($this->LimeSurvey->save($newUser)){
                $n ++;
            }
        }
        $this->Sync->updateMax("limeSurvey");
        return "Aggiunti ".$n." utenti a lime survey";
    }



}