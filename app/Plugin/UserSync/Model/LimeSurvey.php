<?php
class LimeSurvey extends UserSyncAppModel {
    public $useDbConfig = 'lime_survey';
    public $useTable = "lime_participants";
    public $primaryKey = 'participant_id';
}