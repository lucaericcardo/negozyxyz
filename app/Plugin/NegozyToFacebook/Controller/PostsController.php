<?php
class PostsController extends NegozyToFacebookAppController {

    public $components = array('RequestHandler','NegozyToFacebook.FacebookPost','Paginator');

    public $uses = array(
        "NegozyToFacebook.Post"
    );

    public function application_add(){
        $this->layout = "ajax";
    }

    public function application_list(){
        $this->layout = "ajax";
    }

    public function application_share(){
        $this->layout = "ajax";
        $data['success'] = false;
        $data['message'] = __("Non è stato possibile pubblicare il tuo post");
        if($this->request->is("post")) {
            $this->loadModel('Product');
            $product = $this->Product->find("first",array(
                "conditions" => array(
                    "Product.id" => $this->request->data("product_id"),
                    "Product.shop_id" => $this->shop_id
                ),
                "fields" => array(
                    "Product.name",
                    "Product.url",
                    "Product.description",
                ),
                "contain" => array(
                    "Shop" => array(
                        "url",
                        "name"
                    ),
                    "ProductImage" => array(
                        "fields" => array(
                            "thumb"
                        ),
                        "conditions" => array(
                            "primary" => TRUE
                        ),
                        "limit" => 1
                    )
                )
            ));
            if($product) {
                $link = "http://".$product['Shop']['url'].".negozy.com/product/".$product['Product']['url'];
                $description = strip_tags($product['Product']['description']);
                $image = reset($product['ProductImage']);
                $image = $image['thumb'];
                $post_id = NULL;
                if($this->request->data['author']['type'] == "account") {
                    try {
                        if ($post_id = $this->FacebookPost->publishPost($this->request->data['access_token'], $link, $description, $this->request->data['description'], $image)) {
                            $data['success'] = true;
                            $data['message'] = __("Il tuo post è stato pubblicato con successo");
                        }
                    } catch(Exception $e){
                        //$data['success'] = true;
                        $data['message'] = $e->getMessage();
                    }
                } else if($post_id = $this->request->data['author']['type'] == "page" && $this->request->data['author']['access_token']) {
                    try {
                        if ($this->FacebookPost->publishPostPage($this->request->data['author']['access_token'], $this->request->data['author']['id'], $link, $description, $this->request->data['description'], $image)) {
                            $data['success'] = true;
                            $data['message'] = __("Il tuo post è stato pubblicato con successo");
                        }
                    } catch(Exception $e){
                        $data['message'] = $e->getMessage();
                        //$data['success'] = true;
                        //$data['message'] = __("Il tuo post è stato pubblicato con successo");
                    }
                }
                if($post_id) {
                    $newPost = array(
                        "id" => NULL,
                        "user_id"   => $this->Auth->user("id"),
                        "post_id"   => $post_id,
                        "author"    => $this->request->data['author']['name'],
                        "message"   => $this->request->data['description']
                    );
                    $this->Post->create();
                    $this->Post->save($newPost);
                }
            }
        }
        $this->set(array(
            'data' => $data,
            '_serialize' => array('data')
        ));
    }

    public function application_get_list($n=10,$page=1,$row="",$orderType="") {
        $this->response->compress();
        $this->Paginator->settings = array(
            "recursive" => -1,
            "conditions" => array(
                "Post.user_id" => $this->Auth->user("id")
            ),
            "fields" => array(
                "Post.author",
                "Post.message",
                "Post.timestamp_created",
            ),
            "order" => "Post.".$row." ".$orderType,
            "limit" => $n,
            "page" => $page
        );
        $count = $this->Post->find("count",array(
            "conditions" => array(
                "Post.user_id" => $this->Auth->user("id")
            ),
            "recursive" => -1
        ));
        $posts = $this->Paginator->paginate('Post');
        $posts = Set::extract('/Post/.',$posts);
        $this->set(array(
            'posts' => $posts,
            'count' => $count,
            '_serialize' => array('posts','count')
        ));
    }

}