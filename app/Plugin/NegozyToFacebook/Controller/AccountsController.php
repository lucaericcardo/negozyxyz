<?php
class AccountsController extends NegozyToFacebookAppController {

    public $uses = array(
        "NegozyToFacebook.Account"
    );

    public $components = array('RequestHandler','NegozyToFacebook.FacebookPost');

    public function application_index(){
        $this->layout = "ajax";
        $data['account'] = false;
        $account = $this->Account->find("first",array(
            "conditions" => array(
                "user_id" => $this->Auth->user("id")
            )
        ));
        if($account) {
            $data['account']['name']    = $account['Account']['full_name'];
            $data['account']['id']      = $account['Account']['facebook_id'];
        }
        $this->set(array(
            'data' => $data,
            '_serialize' => array('data')
        ));
    }

    public function application_list(){
        $arrayReturn = array();
        $dataReturn['success'] = false;
        $user = $this->FacebookPost->getUser($this->request->data['access_token']);
        $account = $this->Account->find("first", array(
            "conditions" => array(
                "user_id" => $this->Auth->user("id"),
                "facebook_id" => $user->getProperty("id")
            )
        ));
        if($account){
            $arrayReturn[] = array(
                "id"    => $account['Account']['facebook_id'],
                "name"  => $account['Account']['full_name'],
                "type"  => "account"
            );
            $dataReturn['success'] = true;
            $pages = $this->FacebookPost->getPages($this->request->data['access_token']);
            $data = $pages->getPropertyAsArray("data");
            foreach($data as $page) {
                $arrayReturn[] = array(
                    "name"          => $page->getProperty("name"),
                    "type"          => "page",
                    "access_token"  => $page->getProperty("access_token"),
                    "id"            => $page->getProperty("id")
                );
            }
        }
        $this->set(array(
            'data'          => $dataReturn,
            'accounts'      => $arrayReturn,
            '_serialize'    => array('accounts','data')
        ));
    }

    public function application_add_account(){
        $this->layout = "ajax";
        $data['success'] = false;
        $data['message'] = __("Non è stato possibile collegare il tuo account facebook");
        if($this->request->is("post")) {
            $user = $this->FacebookPost->getUser($this->request->data['access_token']);
            if($user) {
                $account = $this->Account->find("first", array(
                    "conditions" => array(
                        "user_id" => $this->Auth->user("id")
                    )
                ));

                $account['Account']['facebook_id'] = $user->getProperty("id");
                $account['Account']['full_name'] = $user->getProperty("name");
                $account['Account']['user_id'] = $this->Auth->user("id");

                if ($this->Account->save($account)) {
                    $data['success'] = true;
                    $data['message'] = __("Il tuo account facebook è stato aggiunto con successo");
                    $data['account']['name'] = $user->getProperty("name");
                    $data['account']['id'] = $user->getProperty("id");
                }
            }
        }
        $this->set(array(
            'data' => $data,
            '_serialize' => array('data')
        ));
    }

}