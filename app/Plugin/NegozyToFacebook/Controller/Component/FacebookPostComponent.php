<?php
App::import('Vendor', 'FacebookSignedRequestFromInputHelper', array('file' => 'Facebook'  . DS . 'FacebookSignedRequestFromInputHelper.php'));
App::import('Vendor', 'FacebookJavaScriptLoginHelper', array('file' => 'Facebook'  . DS . 'FacebookJavaScriptLoginHelper.php'));
App::import('Vendor', 'FacebookSDKException', array('file' => 'Facebook' . DS . 'FacebookSDKException.php'));
App::import('Vendor', 'FacebookRequestException', ['file' => 'Facebook' . DS . 'FacebookRequestException.php']);
App::import('Vendor', 'FacebookPermissionException', array('file' => 'Facebook' . DS . 'FacebookPermissionException.php'));
App::import('Vendor', 'SignedRequest', array('file' => 'Facebook' . DS . 'Entities'. DS .'SignedRequest.php'));
App::import('Vendor', 'FacebookSession', array('file' => 'Facebook' . DS . 'FacebookSession.php'));
App::import('Vendor', 'FacebookRequest', ['file' => 'Facebook' . DS . 'FacebookRequest.php']);
App::import('Vendor', 'FacebookAccessToken', ['file' => 'Facebook' . DS . 'Entities' . DS . 'AccessToken.php']);
App::import('Vendor', 'FacebookHttpable', ['file' => 'Facebook' . DS . 'HttpClients' . DS . 'FacebookHttpable.php']);
App::import('Vendor', 'FacebookCurlHttpClient', ['file' => 'Facebook' . DS . 'HttpClients' . DS . 'FacebookCurlHttpClient.php']);
App::import('Vendor', 'FacebookCurl', ['file' => 'Facebook' . DS . 'HttpClients' . DS . 'FacebookCurl.php']);
App::import('Vendor', 'FacebookRequestException', ['file' => 'Facebook' . DS . 'FacebookRequestException.php']);
App::import('Vendor', 'FacebookAuthorizationException', ['file' => 'Facebook' . DS . 'FacebookAuthorizationException.php']);
App::import('Vendor', 'FacebookResponse', ['file' => 'Facebook' . DS . 'FacebookResponse.php']);
App::import('Vendor', 'FacebookRequest', ['file' => 'Facebook' . DS . 'FacebookRequest.php']);
App::import('Vendor', 'GraphObject', ['file' => 'Facebook' . DS . 'GraphObject.php']);
App::import('Vendor', 'GraphSessionInfo', ['file' => 'Facebook' . DS . 'GraphSessionInfo.php']);
App::import('Vendor', 'GraphUser', ['file' => 'Facebook' . DS . 'GraphUser.php']);
use Facebook\FacebookSession;
use Facebook\FacebookRequest;
use Facebook\GraphUser;
use Facebook\FacebookJavaScriptLoginHelper;
use Facebook\FacebookRequestException;

class FacebookPostComponent extends Component {

    private $app_id = '298375923684296';
    private $secret = '827e1bd122ec6ce6ac8e41d000260a03';

    public function initialize(Controller $controller) {
        FacebookSession::setDefaultApplication($this->app_id, $this->secret);
    }

    public function getPages($access_token = NULL) {
        $session = $session = new FacebookSession($access_token);
        if($session && $session->validate()){
            $request = new FacebookRequest( $session, 'GET', '/me/accounts' );
            $response = $request->execute();
            $graphObject = $response->getGraphObject();
            return $graphObject;
        } else {
            return false;
        }
    }

    public function getUser($access_token = NULL) {
        $session = $session = new FacebookSession($access_token);
        if($session && $session->validate()){
            $request = new FacebookRequest( $session, 'GET', '/me' );
            $response = $request->execute();
            $graphObject = $response->getGraphObject();
            return $graphObject;
        } else {
            return false;
        }
    }

    public function getUserImage($access_token = NULL){
        $session = $session = new FacebookSession($access_token);
        if($session && $session->validate()){
            $request = new FacebookRequest( $session, 'GET', '/me/picture' );
            $response = $request->execute();
            $graphObject = $response->getGraphObject();
            return $graphObject;
        } else {
            return false;
        }
    }


    public function publishPostPage($access_token = NULL,$page_id=NULL,$link = NULL,$description=NULL,$message = NULL,$image=NULL){
        $session = $session = new FacebookSession($access_token);
        if($session && $session->validate()) {
            $response = (new FacebookRequest(
                $session,
                'POST',
                '/'.$page_id."/feed",
                array(
                    'link' => $link,
                    'message' => $message,
                    'description' => $description,
                    'picture' => $image
                )
            ))->execute()->getGraphObject();
            return $response->getProperty('id');
        }
        return false;
    }

    public function publishPost($access_token = NULL, $link = NULL,$description=NULL,$message = NULL,$image=NULL) {
        $session = $session = new FacebookSession($access_token);
        if($session && $session->validate()){
            $response = (new FacebookRequest(
                $session, 'POST', '/me/feed', array(
                    'link' => $link,
                    'message' => $message,
                    'description' => $description,
                    'picture' => $image
                )
            ))->execute()->getGraphObject();
            return $response->getProperty('id');
        } else {
            return false;
        }
    }

}