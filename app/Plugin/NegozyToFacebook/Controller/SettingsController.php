<?php
class SettingsController extends NegozyToFacebookAppController {

    public $components = array('NegozyToFacebook.FacebookPost');

    public function beforeFilter(){
        parent::beforeFilter();
        $this->_checkShop();
    }

    public function application_index(){
        $this->layout = "ajax";
    }

    public function application_publishProductPost($product_id = NULL){
        $this->layout = "ajax";
        $this->loadModel('Product');
        $product = $this->Product->find("first",array(
            "conditions" => array(
                "Product.id" => $product_id,
                "Product.shop_id" => $this->shop_id
            ),
            "fields" => array(
                "Product.name",
                "Product.url",
            ),
            "contain" => array(
                "Shop" => array(
                    "url",
                    "name",
                    "description"
                ),
                "ProductImage" => array(
                    "fields" => array(
                        "thumb"
                    ),
                    "conditions" => array(
                        "primary" => TRUE
                    ),
                    "limit" => 1
                )
            )
        ));
        if($product) {
            $link = "http://".$product['Shop']['url'].".negozy.com/product/".$product['Product']['url'];
            $description = $product['Shop']['description'];
            $message = __("Ho pubblicato un nuovo prodotto sul mio sito!");
            $image = reset($product['ProductImage']);
            $image = $image['thumb'];
            $this->FacebookPost->publishPost($link,$description,$message,$image);
        }
        die();
    }

}