(function() {
    'use strict';
    var negozy_to_facebook = angular.module('plugin.NegozyToFacebook.controllers', ['facebook']);
    negozy_to_facebook.run(function ($facebook,$rootScope) {
        $facebook.init({
            appId      : '298375923684296',
            xfbml      : true,
            status     : true,
            cookie     : true,
            version    : 'v2.2'
        })
        $facebook.Event.subscribe('auth.authResponseChange', function (r) {
            if(r.status == "connected") {
                $rootScope.negozy_to_facebook_logged = true;
                $rootScope.negozy_to_facebook_accessToken = r.authResponse.accessToken;
            }
        });
    }).controller('NegozyToFacebookMainCtrl',  function($rootScope,$scope,$facebook,NegozyToFacebookAccount,$activityIndicator,toaster) {
        $activityIndicator.startAnimating();
        $scope.account = {
            name    : null,
            id      : null
        };

        NegozyToFacebookAccount.get().success(function(r) {
            $scope.account = r.data.account;
            $activityIndicator.stopAnimating();
        });

        $scope.getUserInfo = function(){
            /*if($rootScope.negozy_to_facebook_logged) {
                NegozyToFacebookAccount.add($rootScope.negozy_to_facebook_accessToken).success(function(r) {
                    if(r.data.success) {
                        $scope.account = r.data.account;
                        toaster.pop('success', "", r.data.message);
                    } else {
                        toaster.pop('error', "", r.data.message);
                    }
                    $activityIndicator.stopAnimating();
                });
            } else {*/
                $facebook.login(function(response){
                    if(response.status == "connected") {
                        NegozyToFacebookAccount.add($rootScope.negozy_to_facebook_accessToken).success(function (r) {
                            if (r.data.success) {
                                $scope.account = r.data.account;
                                toaster.pop('success', "", r.data.message);
                            } else {
                                toaster.pop('error', "", r.data.message);
                            }
                            $activityIndicator.stopAnimating();
                        });
                    }
                }, {
                    scope: 'publish_actions,manage_pages',
                        return_scopes: true
                });
            //}
        }
    }).controller('NegozyToFacebookPostsCtrl',  function($rootScope,$scope,$activityIndicator,NegozyToFacebookPost) {
        $activityIndicator.startAnimating();
        $scope.numPerPageOpt        = [5, 10, 20];
        $scope.numPerPage           = $scope.numPerPageOpt[1];
        $scope.currentPage          = 1;
        $scope.currentPageStores    = [];
        $scope.posts                = [];
        $scope.totalPost            = 0;
        $scope.slectedCollectionId  = false;
        $scope.row                  = 'created';
        $scope.orderType            = 'DESC';
        $scope.select = function(page) {
            $activityIndicator.startAnimating();
            $scope.deselectAll  = true;
            $scope.selectAll    = false;
            NegozyToFacebookPost.getList($scope.numPerPage,page,$scope.row,$scope.orderType).success(function(r){
                $scope.posts        = r.posts;
                $scope.totalPost    = r.count;
                $activityIndicator.stopAnimating();
            });
        };

        $scope.onFilterChange = function() {
            $scope.select(1);
            $scope.currentPage = 1;
            return $scope.row = '';
        };
        $scope.onNumPerPageChange = function() {
            $scope.select(1);
            return $scope.currentPage = 1;
        };
        $scope.onOrderChange = function() {
            $scope.select(1);
            return $scope.currentPage = 1;
        };
        $scope.order = function(rowName,orderType) {
            if ($scope.row === rowName && $scope.orderType === orderType) {
                return;
            }
            $activityIndicator.startAnimating();
            $scope.row = rowName;
            $scope.orderType = orderType;
            return $scope.onOrderChange();
        };
        var init = function() {
            return $scope.select($scope.currentPage);
        };
        return init();

    }).controller('NegozyToFacebookButtonShareCtrl',  function($rootScope,$scope,$facebook,NegozyToFacebookAccount,$activityIndicator,$modal) {

        $scope.open = function (product_id) {
            //if(!$rootScope.negozy_to_facebook_logged) {
                $facebook.login(function(response) {
                    if(response.status == "connected") {
                        $modal.open({
                            templateUrl: '/application/negozy_to_facebook/posts/add.html',
                            controller: 'NegozyToFacebookModalShareCtrl',
                            resolve: {
                                product_id: function () {
                                    return product_id;
                                }
                            }
                        });
                    }
                }, {
                    scope: 'publish_actions,manage_pages',
                    return_scopes: true
                });
            /*} else {
                $modal.open({
                    templateUrl: '/application/negozy_to_facebook/posts/add.html',
                    controller: 'NegozyToFacebookModalShareCtrl',
                    resolve: {
                        product_id: function () {
                            return product_id;
                        }
                    }
                });
            }*/
        };

    }).controller('NegozyToFacebookModalShareCtrl',  function($rootScope,$scope,$facebook,NegozyToFacebookAccount,product_id,$modalInstance,$activityIndicator,toaster) {
            $scope.account_list = [];
            $scope.post = {
                author      : null,
                description : null,
                type        : null
            };
            $scope.logged = false;
            $scope.logged = $rootScope.negozy_to_facebook_logged;
            NegozyToFacebookAccount.getAccounts($rootScope.negozy_to_facebook_accessToken).success(function(r){
                if(r.data.success) {
                    $scope.account_list = r.accounts;
                    $scope.post.author = $scope.account_list[0];
                    $scope.logged = true;
                } else {
                    $scope.logged = false;
                }
            }).error(function(){
                $scope.logged = false;
                $modalInstance.close();
                $facebook.login(function(response) {
                    if(response.status == "connected") {

                    }
                }, {
                    scope: 'publish_actions,manage_pages',
                    return_scopes: true
                });
            });

            $scope.cancel = function () {
                $modalInstance.dismiss('cancel');
            };

            /*$facebook.login(function(response) {

            }, {
                scope: 'publish_actions,manage_pages',
                return_scopes: true
            });*/

            $scope.share = function(){
                $activityIndicator.startAnimating();
                $modalInstance.close();
                NegozyToFacebookAccount.share(product_id,$rootScope.negozy_to_facebook_accessToken,$scope.post.author,$scope.post.description).success(function(r){
                    $activityIndicator.stopAnimating();
                    if (r.data.success) {
                        toaster.pop('success', "", r.data.message);
                    } else {
                        toaster.pop('error', "", r.data.message);
                    }
                })

            }
        })
}).call(this);
