(function() {
    'use strict';
    angular.module('plugin.NegozyToFacebook.services', [])
        .factory("NegozyToFacebookAccount", function ($http) {
            return {
                add: function (access_token) {
                    return $http.post('/application/negozy_to_facebook/accounts/add_account.json',{access_token:access_token});
                },
                get: function() {
                    return $http.get('/application/negozy_to_facebook/accounts.json');
                },
                getAccounts: function(access_token){
                    return $http.post('/application/negozy_to_facebook/accounts/list.json',{access_token:access_token});
                },
                share: function(product_id,access_token,author,description){
                    return $http.post('/application/negozy_to_facebook/posts/share.json',{product_id:product_id,access_token:access_token,description:description,author:author});
                }
            }
        })
        .factory("NegozyToFacebookPost", function ($http) {
            return {
                getList: function (n, page, row, orderType) {
                    return $http.get('/application/negozy_to_facebook/posts/get_list/' + n + '/' + page + '/' + row + '/' + orderType + '.json');
                }
            }
        })

}).call(this);
