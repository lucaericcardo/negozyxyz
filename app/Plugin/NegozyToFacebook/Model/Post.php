<?php
class Post extends NegozyToFacebookAppModel {

    public $virtualFields = array(
        "timestamp_created" => "extract(epoch from Post.created at time zone 'Europe/Rome') * 1000"
    );
}