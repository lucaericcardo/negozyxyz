<div class="lapagina" data-ng-controller="NegozyToFacebookMainCtrl">
    <nav class="navbar navbar-default navbar-static-top" role="navigation">
        <div class="navbar-header">
            <ul class="nav navbar-nav">
                <li>
                    <ol class="breadcrumb">
                        <li class="active"><a><?php echo __("Negozy to Facebook"); ?></a></li>
                    </ol>
                </li>
            </ul>
        </div>
    </nav>
    <div class="page page-table">
        <div class="row">
            <div class="col-sm-2">
                <section class="panel panel-default mail-categories ng-scope">
                    <?php echo $this->element('sidebar'); ?>
                </section>
            </div>
            <div class="col-md-10">
                <section class="panel panel-default">
                    <div class="panel-body">
                        <img data-ng-src="https://graph.facebook.com/{{account.id}}/picture">
                        <p>{{account.name}}</p>
                    </div>
                </section>
                <button data-ng-click="getUserInfo()" class="btn btn-success btn-lg pull-right" type="button"><?php echo __('Connetti account'); ?></button>
            </div>
        </div>
    </div>
</div>
