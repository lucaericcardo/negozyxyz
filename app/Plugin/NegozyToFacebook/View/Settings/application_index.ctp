<h1>Contacts</h1>
<p>Following is a sortable list of your contacts</p>
<!-- A sortable list of contacts would go here....-->
<script>
    window.fbAsyncInit = function() {
        FB.init({
            appId      : '331952493676581',
            xfbml      : true,
            status     : true,
            cookie     : true,
            version    : 'v2.2'
        });
        FB.getLoginStatus(function(response) {
            console.log(response);
            if (response.status === 'connected') {
                console.log('Logged in.');
            } else {
                console.log("___");
                FB.login();
            }
        });
    };
    (function(d, s, id){
        var js, fjs = d.getElementsByTagName(s)[0];
        if (d.getElementById(id)) {return;}
        js = d.createElement(s); js.id = id;
        js.src = "//connect.facebook.net/it_IT/sdk.js";
        fjs.parentNode.insertBefore(js, fjs);
    }(document, 'script', 'facebook-jssdk'));
</script>