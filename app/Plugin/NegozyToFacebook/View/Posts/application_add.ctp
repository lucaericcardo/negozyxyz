<div data-ng-if="logged">
    <div class="modal-header">
        <h3 class="modal-title"><?php echo __("Pubblica nuovo post"); ?></h3>
    </div>
    <div class="modal-body">
        <form name="addPost" class="form-validation">
            <div class="form-group">
                <label for=""><?php echo __("Testo del post"); ?></label>
                <textarea class="form-control" data-ng-model="post.description"></textarea>
            </div>
            <div class="form-group">
                <label for=""><?php echo __("Pubblica come"); ?></label>
                <select class="form-control" data-ng-model="post.author" data-ng-options="account as account.name for account in account_list"></select>
            </div>
        </form>
    </div>
    <div class="modal-footer">
        <button class="btn btn-primary"  ng-click="share()"><?php echo __("Pubblica"); ?></button>
        <button class="btn btn-warning"  ng-click="cancel()"><?php echo __("Annulla"); ?></button>
    </div>
</div>

<div data-ng-if="!logged">
    <div class="modal-header">
        <h3 class="modal-title"><?php echo __("Attenzione"); ?></h3>
    </div>
    <div class="modal-body">
        <div class="alert alert-danger">
            <?php
            echo __("Per poter pubblicare un post devi effettuare l'accesso con il tuo account Facebook")
            ?>
        </div>
    </div>
    <div class="modal-footer">
        <button class="btn btn-primary"  ng-click="login()"><?php echo __("Accedi"); ?></button>
        <button class="btn btn-warning"  ng-click="cancel()"><?php echo __("Annulla"); ?></button>
    </div>
</div>