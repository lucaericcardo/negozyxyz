<div class="lapagina" data-ng-controller="NegozyToFacebookPostsCtrl">
    <nav class="navbar navbar-default navbar-static-top" role="navigation">
        <div class="navbar-header">
            <ul class="nav navbar-nav">
                <li>
                    <ol class="breadcrumb">
                        <li class="active"><a><?php echo __("Negozy to Facebook"); ?></a></li>
                    </ol>
                </li>
            </ul>
        </div>
    </nav>
    <div class="page page-table">
        <div class="row">
            <div class="col-sm-2">
                <section class="panel panel-default mail-categories ng-scope">
                    <?php echo $this->element('sidebar'); ?>
                </section>
            </div>
            <div class="col-md-10">
                <section class="panel panel-default table-dynamic">
                    <div class="panel-body">
                        <div class="table-responsive">
                            <table class="table table-bordered table-striped">
                                <thead>
                                <tr>
                                    <th>
                                        <div class="th">
                                            <?php echo __("Profilo"); ?>
                                            <span class="glyphicon glyphicon-chevron-up" data-ng-click=" order('author','ASC')" data-ng-class="{active: row == 'author' && orderType == 'ASC'}"></span>
                                            <span class="glyphicon glyphicon-chevron-down" data-ng-click=" order('author','DESC') " data-ng-class="{active: row == 'author' && orderType == 'DESC'}"></span>
                                        </div>
                                    </th>
                                    <th>
                                        <div class="th">
                                            <?php echo __("Messagggio"); ?>
                                            <span class="glyphicon glyphicon-chevron-up" data-ng-click=" order('message','ASC')" data-ng-class="{active: row == 'message' && orderType == 'ASC'}"></span>
                                            <span class="glyphicon glyphicon-chevron-down" data-ng-click=" order('message','DESC')" data-ng-class="{active: row == 'message' && orderType == 'DESC'}"></span>
                                        </div>
                                    </th>
                                    <th>
                                        <div class="th">
                                            <?php echo __("Data di pubblicazione"); ?>
                                            <span class="glyphicon glyphicon-chevron-up" data-ng-click=" order('created','ASC')" data-ng-class="{active: row == 'created' && orderType == 'ASC'}"></span>
                                            <span class="glyphicon glyphicon-chevron-down" data-ng-click=" order('created','DESC')" data-ng-class="{active: row == 'created' && orderType == 'DESC'}"></span>
                                        </div>
                                    </th>
                                </tr>
                                </thead>
                                <tbody>
                                <tr data data-ng-repeat="post in posts">
                                    <td>{{post.author}}</td>
                                    <td>{{post.message}}</td>
                                    <td>{{post.timestamp_created | date:'dd/MM/yyyy  H:mm'}}</td>
                                </tr>
                                </tbody>
                            </table>
                        </div>

                        <footer class="table-footer">
                            <div class="row">
                                <div class="col-md-6 page-num-info">
                                    <span>
                                        <?php echo __("Elementi per pagina"); ?>
                                        <select data-ng-model="numPerPage" data-ng-options="num for num in numPerPageOpt" data-ng-change="onNumPerPageChange()">
                                        </select>
                                    </span>
                                </div>
                                <div class="col-md-6 text-right pagination-container">
                                    <pagination class="pagination-sm" ng-model="currentPage" total-items="totalPost" max-size="4" ng-change="select(currentPage)" items-per-page="numPerPage" rotate="false" boundary-links="true"></pagination>
                                </div>
                            </div>
                        </footer>
                    </div>
                </section>
            </div>
        </div>
    </div>
</div>
