(function() {
    'use strict';
    var google_analytics_connector = angular.module('plugin.GoogleAnalyticsConnector.controllers', []);
    google_analytics_connector.controller('GoogleAnalyticsConnectorMainCtrl',  function($scope,$activityIndicator,toaster,GoogleAnalyticsConnectorCode) {
        $activityIndicator.startAnimating();
        $scope.data ={
            code: ""
        };
        GoogleAnalyticsConnectorCode.get().success(function(r){
            if(r.data.success && r.data.code){
                $scope.data.code = r.data.code;
            }
            $activityIndicator.stopAnimating();
        });
        $scope.update = function(){
            GoogleAnalyticsConnectorCode.update($scope.data.code).success(function(r){
                if (r.data.success) {
                    $scope.account = r.data.account;
                    toaster.pop('success', "", r.data.message);
                } else {
                    toaster.pop('error', "", r.data.message);
                }
                $activityIndicator.stopAnimating();
            });
        }
    })
}).call(this);
