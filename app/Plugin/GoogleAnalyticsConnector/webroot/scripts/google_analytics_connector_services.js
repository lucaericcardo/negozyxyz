(function() {
    'use strict';
    angular.module('plugin.GoogleAnalyticsConnector.services', [])
        .factory("GoogleAnalyticsConnectorCode", function ($http) {
            return {
                update: function (code) {
                    return $http.post('/application/google_analytics_connector/analytics_codes/update.json',{code:code});
                },
                get: function() {
                    return $http.get('/application/google_analytics_connector/analytics_codes.json');
                }
            }
        })
}).call(this);
