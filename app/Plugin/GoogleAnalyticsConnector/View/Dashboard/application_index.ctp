<div class="lapagina" data-ng-controller="GoogleAnalyticsConnectorMainCtrl">
    <nav class="navbar navbar-default navbar-static-top" role="navigation">
        <div class="navbar-header">
            <ul class="nav navbar-nav">
                <li>
                    <ol class="breadcrumb">
                        <li class="active"><a><?php echo __("Google Analytics Connector"); ?></a></li>
                    </ol>
                </li>
            </ul>
        </div>
    </nav>
    <div class="page page-table">
        <div class="page page-table ng-scope">
            <div class="row">
                <div class="col-lg-10 col-lg-offset-1 col-md-12">
                    <section class="panel panel-default">
                        <div class="panel-heading">
                            <h4><?php echo __("Inserici il tuo codice di google analytics"); ?></h4>
                        </div>
                        <div class="panel-body">
                            <form class="form-horizontal form-validation" name="google-analytics">
                                <div class="row">
                                    <div class="col-xs-10 col-xs-offset-1">
                                        <div class="form-group">
                                            <label for=""> <?php echo __("Codice Google Analytics"); ?> </label>
                                            <input data-ng-model="data.code" required  type="text" class="form-control input-lg" placeholder="<?php echo __("Es: UA-XXXXX-X"); ?>" />
                                        </div>
                                    </div>
                                </div>
                            </form>
                        </div>
                    </section>
                    <button class="btn btn-success btn-lg" data-ng-click="update()" ng-disabled="google-analytics.$invalid" type="button">Salva</button>
                </div>
            </div>
        </div>
    </div>
</div>
