<?php
class AnalyticsCodesController extends GoogleAnalyticsConnectorAppController {

    public $components = array("RequestHandler");
    public $uses = array("GoogleAnalyticsConnector.AnalyticsCode");

    public function application_update(){
        $data = array(
            "success" => false,
            "message" => __("Non è stato possibile salvare il codice di Google Analytics")
        );
        if($this->request->is("post") && isset($this->request->data['code']) && !empty($this->request->data['code'])) {
            $code = $this->AnalyticsCode->find("first",array(
                "conditions" => array(
                    "shop_id" => $this->shop_id
                ),
                "fields" => array(
                    "id"
                ),
                "recursive" => -1
            ));
            if($code) {
                $code['AnalyticsCode']['code'] = $this->request->data['code'];
            } else {
                $code['id']         = NULL;
                $code['shop_id']    = $this->shop_id;
                $code['code']       = $this->request->data['code'];
                $this->AnalyticsCode->create();
            }
            if($this->AnalyticsCode->save($code)){
                $data = array(
                    "success" => true,
                    "message" => __("ll codice di Google Analytics è stato salvato con successo")
                );
            }
        }
        $this->set(array(
            'data' => $data,
            '_serialize' => array('data')
        ));
    }

    public function application_index(){
        $data = array(
            "success"   => false,
            "code"      => false
        );
        $code = $this->AnalyticsCode->find("first",array(
            "conditions" => array(
                "shop_id" => $this->shop_id
            ),
            "fields" => array(
                "code"
            ),
            "recursive" => -1
        ));
        if($code) {
            $data['success']    = true;
            $data['code']       = $code['AnalyticsCode']['code'];
        }
        $this->set(array(
            'data' => $data,
            '_serialize' => array('data')
        ));
    }

}