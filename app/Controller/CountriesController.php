<?php
class CountriesController extends AppController {

    public $components = array('RequestHandler');

    public function index() {
        $this->response->compress();
        $countries = $this->Country->find('all',array(
            "recursive" => -1,
            "order"     => "name",
            "fields" => array(
                "id",
                "name",
                "alpha",
                "group_europe"
            )
        ));
        $countries =  Set::extract('/Country/.', $countries);

        $this->set(array(
            'countries' => $countries,
            '_serialize' => array('countries')
        ));
    }


}