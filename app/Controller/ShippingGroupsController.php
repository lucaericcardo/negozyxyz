<?php
class ShippingGroupsController extends AppController {

    public $components = array('RequestHandler','PriceHelper','UserAction');

    public $uses = array(
        'Shipping',
        'ShippingGroup',
        'ShopTax',
        'Country'
    );

    function beforeFilter(){
        parent::beforeFilter();
        $this->_asyncCheckAuth();
        $this->_checkShop();
    }

    public function view_add() {
        $this->response->compress();
        $this->layout = "ajax";
    }

    public function view_edit() {
        $this->response->compress();
        $this->layout = "ajax";
    }

    public function loadShippingConfig(){
        if($this->request->is("post") && isset($this->request->data['shipping_group_id']) && !empty($this->request->data['shipping_group_id'])){
            $unavailable_option = $this->loadUnavailableCountries($this->request->data['shipping_group_id']);
        } else {
            $unavailable_option   = array();
        }
        $default_country    = $this->Country->find("first",array(
            "joins" => array(
                array(
                    'table' => 'shops',
                    'alias' => 'Shop',
                    'type' => 'INNER',
                    'conditions' => array(
                        'Shop.state = Country.alpha',
                        "Shop.id" => $this->shop_id
                    )
                )
            )
        ));
        $this->set(array(
            'unavailable_option' => $unavailable_option,
            'default_country'   => $default_country['Country'],
            '_serialize' => array('default_country','unavailable_option')
        ));
    }

    public function loadShippingGroup(){
        $this->response->compress();
        $data = array(
            "success" => false
        );
        if ($this->request->is("post")) {
            $shipping_group_id = $this->request->data["shipping_group_id"];
            if($shipping_group_id) {
                $shipping_group = $this->ShippingGroup->find("first", array(
                    "conditions" => array(
                        "id" => $shipping_group_id,
                        "shop_id" => $this->shop_id
                    ),
                    "recursive" => -1
                ));
                if ($shipping_group) {
                    $shipping_data = array(
                        'id'            => $shipping_group['ShippingGroup']['id'],
                        'name'          => $shipping_group['ShippingGroup']['name'],
                    );
                    $data['success'] = true;
                    $data['shipping_group'] = $shipping_data;
                } else {
                    $this->response->statusCode(404);
                    $data['data']['message'] = "Shipping group not found";
                    $this->set(array(
                        'data' => $data,
                        '_serialize' => array('data')
                    ));
                    $this->response->send();
                    $this->_stop();
                }
            }
        }
        $this->set(array(
            'data' => $data,
            '_serialize' => array('data')
        ));
    }


    public function index(){
        $shipping_groups = $this->ShippingGroup->find("all",array(
            "conditions" => array(
                "shop_id" => $this->shop_id
            ),
            "fields" => array(
                "ShippingGroup.id",
                "ShippingGroup.name",
                "ShippingGroup.product_count",
            ),
            "contain" => array(
                "Shipping" => array(
                    "Country" => array(
                        "fields" => array(
                            "alpha"
                        )
                    ),
                    "ShopTax"
                )
            ),
            "recursive" => -1
        ));
        $shop_currency = $this->Shop->find("first",array(
            "conditions" => array(
                "id" => $this->shop_id
            ),
            "fields" => array(
                "currency"
            ),
            "recursive" => -1
        ));
        $currency = 'EUR';
        if($shop_currency) {
            $currency = $shop_currency['Shop']['currency'];
        }
        $return_data = array();
        if($shipping_groups){
            foreach($shipping_groups as $shipping_group) {
                $tmp_shipping = array(
                    'id'    =>  $shipping_group['ShippingGroup']['id'],
                    'name'  =>  $shipping_group['ShippingGroup']['name'],
                    'count' =>  $shipping_group['ShippingGroup']['product_count'],
                    'shipments' => array()
                );
                foreach($shipping_group['Shipping'] as $shipping){
                    $countries = array();
                    if(isset($shipping['Country'])) {
                        foreach ($shipping['Country'] as $country) {
                            $countries[] = $country['alpha'];
                        }
                    }
                    $price              = $this->PriceHelper->format_price($shipping['price'],$shipping['ShopTax']);
                    $additional_price   = $this->PriceHelper->format_price($shipping['additional_price'],$shipping['ShopTax']);
                    $shop_tax           = (isset($shipping['ShopTax']['label'])) ? $shipping['ShopTax']['label'] : '';
                    $tmp_shipping['shipments'][] = array(
                        'id'                => $shipping['id'],
                        'price'             => CakeNumber::currency($price, $currency),
                        'additional_price'  => CakeNumber::currency($additional_price, $currency),
                        'start_day'         => $shipping['start_day'],
                        'end_day'           => $shipping['end_day'],
                        'tax'               => $shop_tax,
                        'countries'         => $countries,
                        'country_type'      => $shipping['country_type'],
                        'count'
                    );
                }
                $return_data[] = $tmp_shipping;
            }
        }
        $this->set(array(
            'shipping_group_list' => $return_data,
            '_serialize' => array('shipping_group_list')
        ));
    }

    public function add(){
        $data = array(
            "success" => false
        );
        if($this->request->is("post")) {
            $shipping_data = $this->request->data("shipping_group");
            $newShippingGroup = array(
                'name'      => $shipping_data['name'],
                'shop_id'   => $this->shop_id
            );
            if($this->ShippingGroup->save($newShippingGroup)) {
                $newShipping['Shipping'] = array(
                    'price'             => $shipping_data['price'],
                    'additional_price'  => $shipping_data['additional_price'],
                    'start_day'         => $shipping_data['start_day'],
                    'end_day'           => $shipping_data['end_day'],
                    'shop_tax_id'       => $shipping_data['shop_tax_id'],
                    'shipping_group_id' => $this->ShippingGroup->id,
                    'country_type'      => $shipping_data['country_type']
                );
                if($shipping_data['countries'] && $shipping_data['country_type'] == 1) {
                    $newShipping['Country'] = $shipping_data['countries'];
                } else {
                    $newShipping['Country'] = array();
                }
                $this->Shipping->create();
                if($this->Shipping->save($newShipping)){
                    $currency = $this->getShopCurrency();
                    $countries = $this->Country->find("all",array(
                        "conditions" => array(
                            "id" => $shipping_data['countries']
                        ),
                        "fields" => array(
                            "alpha"
                        )
                    ));
                    $countries = Set::extract('/Country/alpha', $countries);
                    $shop_tax = $this->ShopTax->find("first",array(
                        "conditions" => array(
                            "id" => $shipping_data['shop_tax_id']
                        ),
                        "recursive" => -1
                    ));
                    $data['success']        = true;
                    $tax = (isset($shop_tax['ShopTax'])) ? $shop_tax['ShopTax'] : null;
                    $price = $this->PriceHelper->format_price($shipping_data['price'],$tax);
                    $additional_price = $this->PriceHelper->format_price($shipping_data['additional_price'],$tax);
                    $shop_tax_label = (isset($tax['label'])) ? $tax['label'] : '';
                    $data['shipping_group'] = array(
                        'id'        => $this->ShippingGroup->id,
                        'name'      => $shipping_data['name'],
                        'count'     =>  0,
                        'shipments' => array(
                            array(
                                'id'                => $this->Shipping->id,
                                'price'             => CakeNumber::currency($price,$currency),
                                'additional_price'  => CakeNumber::currency($additional_price,$currency),
                                'start_day'         => $shipping_data['start_day'],
                                'end_day'           => $shipping_data['end_day'],
                                'tax'               => $shop_tax_label,
                                'countries'         => $countries,
                                'country_type'      => $shipping_data['country_type']
                            )
                        )
                    );
                }
            }
        }
        $this->set(array(
            'data' => $data,
            '_serialize' => array('data')
        ));
    }

    public function update(){
        $data = array(
            "success" => false
        );
        if($this->request->is("post")) {
            $shipping_group_data = $this->request->data("shipping_group");
            $ckOwner = $this->ShippingGroup->find("count",array(
                "conditions" => array(
                    "shop_id" => $this->shop_id,
                    "id"      => $shipping_group_data['id']
                ),
                "recursive" => -1
            ));
            if($ckOwner) {
                $this->ShippingGroup->id = $shipping_group_data['id'];
                if ($this->ShippingGroup->saveField("name",$shipping_group_data['name'])) {
                    $data['success'] = true;
                }
            } else {
                $this->response->statusCode(404);
                $data['data']['message'] = "Shipping group not found";
                $this->set(array(
                    'data' => $data,
                    '_serialize' => array('data')
                ));
                $this->response->send();
                $this->_stop();
            }
        }
        $this->set(array(
            'data' => $data,
            '_serialize' => array('data')
        ));
    }

    public function delete(){
        $data = array(
            "success" => false
        );
        if($this->request->is("post")){
            $shipping_group_id = $this->request->data("shipping_group_id");
            if($this->ShippingGroup->find("count",array(
                "conditions" => array(
                    "shop_id" => $this->shop_id,
                    "id" => $shipping_group_id
                ),
                "recursive" => -1
            ))){
                if($this->ShippingGroup->delete($shipping_group_id)){
                    $data['success'] = true;
                }
            } else {
                $this->response->statusCode(404);
                $data['data']['message'] = "Shipping not found";
                $this->set(array(
                    'data' => $data,
                    '_serialize' => array('data')
                ));
                $this->response->send();
                $this->_stop();
            }
        }
        $this->set(array(
            'data' => $data,
            '_serialize' => array('data')
        ));
    }

    private function loadUnavailableCountries($shipping_group_id = NULL){
        $return = array(
            "countries" => array(),
            "european"  => true,
            "all"       => true
        );
        if(isset($shipping_group_id)){
            $shipments = $this->Shipping->find("all",array(
                "conditions" => array(
                    "shipping_group_id" => $shipping_group_id,
                ),
                "joins" => array(
                    array(
                        'table' => 'shipping_groups',
                        'alias' => 'ShippingGroup',
                        'type'  => 'LEFT',
                        'conditions' => array(
                            'ShippingGroup.id = Shipping.shipping_group_id',
                            "ShippingGroup.id" => $this->shop_id
                        )
                    )
                ),
                "contain" => array(
                    "Country"
                ),
                "recursive" => -1
            ));
            $countries = Set::extract("/Country/.",$shipments);
            foreach($countries as $country) {
                if($country) {
                    $return['countries'][] = array(
                        "id" => $country['id'],
                        "name" => $country['name'],
                        "alpha" => $country['alpha'],
                        "group_europe" => $country['group_europe'],
                    );
                }
            }
            foreach($shipments as $shipping) {
                if($shipping['Shipping']['country_type'] == 2){
                    $return['european'] = false;
                }

                if($shipping['Shipping']['country_type'] == 3){
                    $return['all'] = false;
                }
            }
        }
        return $return;
    }

}
