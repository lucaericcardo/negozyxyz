<?php
class UsersController extends AppController {

    public $components = array('RequestHandler','Mandrillapp','Image','UserAction','FacebookLogin','Google','TermTemplate','UserSignUp');
    public $uses = array(
        'User',
        'UserImage'
    );
    private $source = "Web Site";
    public function beforeFilter() {
        parent::beforeFilter();
        if ($this->params['action'] == 'google_signup' ||
            $this->params['action'] == 'facebook_signup' ||
            $this->params['action'] == 'facebook_signup' ||
            $this->params['action'] == 'oauth2callback' ||
            $this->params['action'] == 'facebook_success' ||
            $this->params['action'] == 'confirm_mail' ||
            $this->params['action'] == 'confirm_order' ||
            $this->params['action'] == 'association_cart'
        ) {
            $this->Auth->allow();
        }
    }

    public function change_password() {
        $this->layout = "auth";
    }

    public function view_profile() {
        $this->layout = "ajax";
    }

    public function view_setting() {
        $this->layout = "ajax";
    }

    public function loadUser() {
        $user = $this->User->find("first",array(
            "contain" => array(
                "UserImage" => array(
                    "fields" => array(
                        "UserImage.image"
                    )
                )
            ),
            "conditions" => array(
                "id" => $this->Auth->user("id")
            ),
            "fields" => array(
                'first_name',
                'last_name'
            ),
            "recursive" => -1
        ));
        $user_data = array();
        $image = null;
        if(isset($user['UserImage'])) {
            $image = reset($user['UserImage']);
            $image = $image['image'];
        }
        if($user) {
            $user_data = array(
                'first_name'    => $user['User']['first_name'],
                'last_name'     => $user['User']['last_name'],
                'image'         => $image
            );
        }
        $this->set(array(
            'user' => $user_data,
            '_serialize' => array('user')
        ));
    }

    public function updateAccount() {
        $data['success'] = false;
        $data['message'] = __("Non è stato possibile aggiornare il tuo profilo");
        if($this->request->is("post")) {
            $user = $this->User->find("first",array(
                "conditions" => array(
                    "id" => $this->Auth->user("id")
                ),
                "recursive" => -1
            ));
            $user_data = $this->request->data("user");
            $user['User']['first_name'] = $user_data['first_name'];
            $user['User']['last_name'] = $user_data['last_name'];
            if(isset($user_data['new_password']) && !empty($user_data['new_password'])) {
                $user['User']['password'] = $user_data['new_password'];
            }
            if($this->User->save($user)) {
                $data['success'] = true;
                $data['message'] = __("Il tuo profilo è stato aggiornato con successo.");
            }
        }
        $this->set(array(
            'data' => $data,
            '_serialize' => array('data')
        ));
    }

    public function getUser($user_url = NULL) {
        $user = array();
        if(isset($user_url)) {
            $user = $this->User->find("first",array(
                "conditions" => array(
                    "url" => $user_url
                ),
                "fields" => array(
                    "first_name",
                    "last_name",
                    "email"
                )
            ));
            $user = Set::extract("/User/.",$user);
            $user = reset($user);
        }

        $this->set(array(
            'user' => $user,
            '_serialize' => array('user')
        ));
    }

    public function logout() {
        $event = new CakeEvent('Model.User.logout', $this,array(
            'type' => "Logout effettuato",
        ));
        $this->getEventManager()->dispatch($event);
        if($this->Auth->logout()) {
            $this->redirect($this->redirect($this->negozy_config['logout_url']));
        }
    }

    public function loadProfile(){
        $data = array(
            "success" => false,
            "url"   => false
        ) ;
        if(isset($_FILES['file'])) {
            $types[] = array("height" => 180,"width" => null,"prefix" => "profile_");
            $image_data = $this->Image->uploadImage($_FILES['file'],'users',$types);
            if ($image_data['success']) {
                $newImage = array();
                $newImage['UserImage'] = $image_data['image'];
                $newImage['UserImage']['user_id'] = $this->Auth->user("id");
                $images = $this->UserImage->find("all",array(
                    "conditions" => array(
                        "user_id" => $this->Auth->user("id")
                    )
                ));
                foreach($images as $image) {
                    $this->Image->remove_image($image['UserImage']['name'].".".$image['UserImage']['ext'],"shops/".$image['UserImage']['path'],"profile_");
                    $this->UserImage->delete($image['UserImage']['id']);
                }
                if($this->UserImage->save($newImage)) {
                    $data['success'] = true;
                    $data['url'] = "/images/users/".$image_data['image']['path']."profile_".$image_data['image']['name'].".".$image_data['image']['ext'];
                }
            }
        }
        $this->set(array(
            'data' => $data,
            '_serialize' => array('data')
        ));
    }

    /*
	public function recoveryPassword(){
			
			$this->layout = "auth";
	}
    */



    /**
     * Login utente
     */

    private function existToken($token) {
        $options = array(
            'conditions' => array(
                'User.recovery_password_token' => $token
            ),
            'fields' => array(
                'User.id'
            ),
            'recursive' => -1
        );
        $user = $this->User->find('first', $options);
        if(count($user)) {
            return $user['User']['id'];
        }
        return false;
    }

    public function changePassword($token = NULL) {
        $this->layout = "auth";
        if($token) {

            $data['error'] = 'hide';
            $data['success'] = 'hide';
            $data['errorNoMatching'] = 'hide';
            $data['form'] = '';
            $data['token'] = $token;
        } else {
            if($this->request->is('post')) {
                $data['error'] = '';
                $data['success'] = 'hide';
                $data['errorNoMatching'] = '';
                $data['form'] = 'hide';
                if(isset($this->request->data['password']) && isset($this->request->data['token']) && isset($this->request->data['confirm_password'])) {
                    if($this->request->data['password'] == $this->request->data['confirm_password']) {
                        $data['errorNoMatching'] = 'hide';
                        if($this->User->id = $this->existToken($this->request->data['token'])) {
                            if($this->User->saveField('password', $this->request->data['password'])) {
                                $data['success'] = '';
                                $data['error'] = 'hide';
                                $event = new CakeEvent('Model.User.changePassword', $this,array(
                                    'type'      => "Cambio password",
                                    'user_id'   => $this->User->id
                                ));
                                $this->getEventManager()->dispatch($event);
                            }
                            $this->setRecoveryPasswordToken($this->User->id, NULL);
                        }
                    } else {
                        $data['success'] = 'hide';
                        $data['error'] = 'hide';
                        $data['errorNoMatching'] = '';
                        $data['form'] = '';
                    }
                }
            }
            $data['token'] = $this->request->data['token'];
        }
        $this->set($data);
    }

    private function getUserNameByMail($email) {
        $options = array(
            'conditions' => array(
                'User.email' => $email
            ),
            'fields' => array(
                'User.fullname',
                'User.id'
            ),
            'recursive' => -1
        );
        $user = $this->User->find('first', $options);
        if(count($user)) {
            return array('fullname'=>$user['User']['fullname'], 'id'=>$user['User']['id']);
        }
        return false;
    }

    private function existRecoveryPasswordToken($user_id) {
        $options = array(
            'conditions' => array(
                'User.id' => $user_id,
                'User.recovery_password_token !=' => NULL,
            ),
            'recursive' => -1
        );
        return $this->User->find('count', $options);
    }

    private function setRecoveryPasswordToken($user_id, $token) {
        $this->User->id = $user_id;
        if($this->User->saveField('recovery_password_token', $token)) {
            return $token;
        }
        return false;
    }

    public function recoveryPassword(){
        $this->layout = "auth";
        $data['success'] = 'hide';
        $data['error'] = 'hide';
        $data['form'] = '';
        $data['email'] = '';
        $data['resend'] = 'false';
        if($this->request->is('post')) {
            $data['error'] = '';
            if(isset($this->request->data['email'])) {
                if(is_array($user_info = $this->getUserNameByMail($this->request->data['email']))) {
                    if($this->existRecoveryPasswordToken($user_info['id']) && $this->request->data['resend'] == 'false') {   // evita che al ricaricamento di pagina venga inviata una nuova mail
                        $data['success'] = '';
                        $data['error'] = 'hide';
                        $data['form'] = 'hide';
                        $data['email'] = $this->request->data['email'];
                    } else if ($token = $this->setRecoveryPasswordToken($user_info['id'], md5(uniqid()))) {
                        $link_password = $_SERVER['HTTP_HOST'] . "/users/changePassword/" . $token;
                        $this->Mandrillapp->send_recovery_password($user_info['fullname'], $this->request->data['email'], $link_password);
                        $data['success'] = '';
                        $data['error'] = 'hide';
                        $data['form'] = 'hide';
                        $data['email'] = $this->request->data['email'];
                        $event = new CakeEvent('Model.User.recoveryPassword', $this,array(
                            'type'      => "Richiesta recupero password",
                            'user_id'   => $user_info['id']
                        ));
                        $this->getEventManager()->dispatch($event);
                    }
                }
            }
        }
        $this->set($data);
    }

    public function facebook_signup() {
        $this->autoRender = false;
        $this->redirect($this->FacebookLogin->getLoginUrl());
    }

    public function google_signup() {
        $this->autoRender = false;
        $this->redirect($this->Google->getLoginUrl());
    }

    public function oauth2callback(){
        if (isset($_GET['code'])) {
            return $this->redirect($this->Google->setAccessToken($_GET['code']));
        } else {
            $google_user = $this->Google->getUser();
            if(isset($google_user) && $google_user) {
                $user = $this->User->find("first", array(
                    "conditions" => array(
                        "email" => $google_user->email
                    ),
                    "fields" => array(
                        "id",
                        "first_name",
                        "last_name",
                        "email",
                        "google_id",
                        "confirmed",
                        "is_admin"
                    ),
                    "recursive" => -1
                ));
                if (!$user){
                    $user['User']['first_name'] = $google_user->givenName;
                    $user['User']['last_name'] = $google_user->familyName;
                    $user['User']['google_id'] = $google_user->id;
                    $user['User']['email'] = $google_user->email;
                    $user['User']['google_email'] = $google_user->email;
                    $user['User']['email_confirmed'] = "now()";
                    $user['User']['url'] = $this->_getUrlName($user['User']['first_name']." ".$user['User']['last_name']);
                    if ($this->User->save($user)) {
                        $user['User']['id'] = $this->User->id;
                        $sugarData['first_name'] = $user['User']['first_name'];
                        $sugarData['last_name'] = $user['User']['last_name'];
                        $sugarData['email'] = $user['User']['email'];
                        $sugarData['source'] = $this->source;
                        $this->Sugar = $this->Components->load('Sugar');
                        $lead_id = $this->Sugar->addLead($sugarData);
                        $this->User->saveField("lead_id",$lead_id);
                    }
                } else if(!$user['User']['google_id']){
                    $this->User->id = $user['User']['id'];
                    $user['User']['google_id'] = $google_user->id;
                    $user['User']['google_email'] = $google_user->email;
                    $this->User->save($user['User']);
                }

                if ($this->Auth->login($user['User'])) {
                    $event = new CakeEvent('Model.User.login', $this,array(
                        'type' => "Login effettuato con Google+",
                    ));
                    $this->getEventManager()->dispatch($event);
                    return $this->_loginRedirect();
                }
            }
        }
        return $this->redirect("/");
    }

    public function facebook_success(){
        $this->autoRender = false;
        $fb_user = $this->FacebookLogin->getUser();
        if(isset($fb_user)) {
            $user = $this->User->find("first", array(
                "conditions" => array(
                    "email" => $fb_user->getProperty("email")
                ),
                "fields" => array(
                    "id",
                    "first_name",
                    "last_name",
                    "email",
                    "facebook_id",
                    "confirmed",
                    "is_admin"
                ),
                "recursive" => -1
            ));
            if (!$user){
                $user['User']['id'] = NULL;
                $user['User']['first_name'] = $fb_user->getProperty("first_name");
                $user['User']['last_name'] = $fb_user->getProperty("last_name");
                $user['User']['facebook_id'] = $fb_user->getProperty("id");
                $user['User']['email'] = $fb_user->getProperty("email");
                $user['User']['facebook_email'] = $fb_user->getProperty("email");
                $user['User']['email_confirmed'] = "now()";
                $user['User']['url'] = $this->_getUrlName($user['User']['first_name']." ".$user['User']['last_name']);
                if ($this->User->save($user)) {
                    $user['User']['id'] = $this->User->id;
                    //Aggiunta Lead CRM
                    $sugarData['first_name'] = $user['User']['first_name'];
                    $sugarData['last_name'] = $user['User']['last_name'];
                    $sugarData['email'] = $user['User']['email'];
                    $sugarData['source'] = $this->source;
                    $this->Sugar = $this->Components->load('Sugar');
                    $lead_id = $this->Sugar->addLead($sugarData);
                    $this->User->saveField("lead_id",$lead_id);
                }
            } else if(!$user['User']['facebook_id']){
                $this->User->id = $user['User']['id'];
                $user['User']['facebook_email'] = $fb_user->getProperty("email");
                $user['User']['facebook_id'] = $fb_user->getProperty("id");
                $this->User->save($user);
            }
            if($user['User']['id']) {
                if ($this->Auth->login($user['User'])) {
                    $event = new CakeEvent('Model.User.login', $this,array(
                        'type' => "Login effettuato con Facebook",
                    ));
                    $this->getEventManager()->dispatch($event);
                    return $this->_loginRedirect();
                }
            }
        }
        return $this->redirect("/");
    }

    public function signin() {
        $this->layout = "auth";
        $signup_errors = array();
        $signin_errors = array();
        if($this->Auth->user("id")) {
            return $this->redirect($this->negozy_config['site_url']);
        }
        if($this->request->is("post")) {
            $data = $this->request->data;
            if(isset($data['Login'])) {
                $this->request->data['User'] = $data['Login'];
                if ($this->Auth->login()) {
                    $event = new CakeEvent('Model.User.login', $this,array(
                        'type' => "Login effettuato",
                    ));
                    $this->getEventManager()->dispatch($event);
                    return $this->_loginRedirect();
                } else {
                    $user_data = $this->User->find("first",array(
                        "conditions" => array(
                            "email" => $this->request->data['Login']['email']
                        ),
                        "fields" => array(
                            "id"
                        ),
                        "recursive" => -1
                    ));
                }
                unset($this->request->data['User']);
            } else if(isset($data['Singup'])) {
                App::uses('String', 'Utility');
                $data['User'] = $data['Singup'];
                $return = $this->UserSignUp->addUser($data);
                if($return['success']){
                    return $this->redirect($this->negozy_config['signup_redirect']);
                }
                $signup_errors = $return['errors'];
                /*
                $user['User'] = $data['Singup'];
                $user['User']['token_auth'] = String::uuid();
                $user['User']['email_confirmed'] = NULL;
                $user['User']['url'] = $this->_getUrlName($user['User']['first_name']." ".$user['User']['last_name']);
                if($this->User->save($user)){
                    //Aggiunta Lead CRM
                    $sugarData['first_name'] = $user['User']['first_name'];
                    $sugarData['last_name'] = $user['User']['last_name'];
                    $sugarData['email'] = $user['User']['email'];
                    $sugarData['source'] = $this->source;
                    $this->Sugar = $this->Components->load('Sugar');
                    $lead_id = $this->Sugar->addLead($sugarData);
                    if($this->User->saveField("lead_id",$lead_id)) {
                        if($this->Session->Read("Auth.redirect_url") && isset($this->params['url']['order'])) {
                            $pending_uid = String::uuid();
                            $user['User']['id'] = $this->User->id;
                            $user['User']['pending_token'] = $pending_uid;
                            if($this->User->saveField("pending_token",$pending_uid)) {
                                if ($this->Auth->login($user['User'])) {
                                    return $this->redirect($this->Session->Read("Auth.redirect_url"));
                                }
                            }
                        }
                        $full_name = $user['User']['first_name'] . " " . $user['User']['last_name'];
                        $email = $user['User']['email'];
                        $link =             $this->redirect($this->redirect($this->negozy_config['negozy_url']))."/users/confirm_mail/" . $user['User']['token_auth'];
                        $this->Mandrillapp->send_confirm_account($full_name, $email, $link);
                        return $this->redirect($this->negozy_config['site_url']);
                    }
                } else {
                    $signup_errors = $this->User->validationErrors;
                }*/

            }
        }

        $this->set("signup_errors",$signup_errors);
        $this->set("signin_errors",$signin_errors);
        $this->set("modal_terms", $this->TermTemplate->get('terms_conditions'));
        if(isset($this->params['url']['order'])) {
            $order = $this->params['url']['order'];
            if (isset($order)) {
                $order = $this->Session->read("Order." . $order);
                $this->set("shop_name", $order['shop']['name']);
                $this->set("shop_logo", $order['shop']['logo']);
                $this->set("show_shop",true);
            }
        } else {
            $this->set("show_shop",false);
        }
    }

    public function signinFlusso() {
        $this->layout = "auth";
    }

    public function confirm_mail($token = NULL){
        if(isset($token)){
            $user = $this->User->find("first",array(
                "conditions" => array(
                    "token_auth" => $token,
                    "email_confirmed" => null
                ),
                "field" => array(
                    "id",
                    "frist_name",
                    "last_name",
                    "email"
                ),
                "recursive" => -1
            ));
            if($user) {
                $this->User->id = $user['User']['id'];
                if($this->User->saveField("email_confirmed","now()")) {
                    if($this->Auth->login($user['User'])){
                        return $this->redirect($this->negozy_config['site_url']);
                    }
                }
            }
            $this->redirect("/");
        }
    }

    public function confirm_order($token = NULL){
        $this->loadModel("Order");
        if(isset($token)){
            $order = $this->Order->find("first",array(
                "conditions" => array(
                    "pending_token" => $token,
                ),
                "fields" => array(
                    "id",
                    "user_id"
                ),
                "recursive" => -1
            ));
            if($order) {
                $this->Order->id = $order['Order']['id'];
                if($this->Order->saveField("pending_token",NULL)) {
                    $user = $this->User->find("first",array(
                        "conditions" => array(
                            "id" => $order['Order']['user_id']
                        ),
                        "recursive" => -1
                    ));
                    $user['User']['email_confirmed'] = date("Y-m-d H:i:s");
                    $user['User']['pending_token'] = NULL;
                    $this->Auth->logout();
                    if($this->Auth->login($user['User'])){
                        return $this->redirect($this->negozy_config['site_url']);
                    }
                }
            }
            $this->redirect("/");
        }
    }

    protected  function _getUrlName($fullname = ""){
        if($fullname) {
            $fullname = strtolower($fullname);
            $url_name = Inflector::slug($fullname,"_");
            $exist_url = $this->User->find("first" , array(
                "conditions" => array(
                    "url LIKE" => "%".$url_name."%"
                ),
                "fields" => array(
                    "url"
                ),
                "order" => "url DESC",
                "recursive" => -1
            ));
            if($exist_url) {
                $exist_url = explode("_",$exist_url['User']['url']);
                $next_val = end($exist_url);
                $next_val = $next_val+1;
                $url_name .= "_".$next_val;
            }
            return $url_name;
        }
    }

    public function _loginRedirect(){
        $auth_redirect = $this->Session->Read("Auth.redirect_url");
        if(isset($auth_redirect) && !empty($auth_redirect)) {
            $this->Session->Delete("Auth.redirect_url");
            return $this->redirect($auth_redirect);
        }
        return $this->redirect($this->negozy_config['site_url']);
    }
    /**
     * Fine login utente
     */


    /**
     * Associazione carrello
     */
    public function association_cart($token_cart = NULL){
        if(isset($token_cart)) {
            $this->loadModel("Cart");
            $cart = $this->Cart->find("first",array(
                "conditions" => array(
                    "token" => $token_cart
                ),
                "fields" => array(
                    "shop_id",
                    "id"
                ),
                "contain" => array(
                    "Shop" => array(
                        "fields" => array(
                            "url"
                        )
                    )
                ),
                "recusive" => -1
            ));
            if($cart) {
                if($this->Auth->user("id")) {
                    $this->Cart->id = $cart['Cart']['id'];
                    if($this->Cart->saveField("user_id",$this->Auth->user("id"))){
                        return $this->redirect("/checkoutV1/index/".$cart['Shop']['url']."/".$token_cart);
                    }
                } else {
                    $this->Session->Write("Auth.redirect_url","/users/association_cart/".$token_cart);
                }
            }
        }
        return $this->redirect("/users/signin");
    }

}
?>