<?php
class LanguagesController extends AppController {

    public $components = array('RequestHandler','Paginator');
    public $uses = array('ShopLanguage','Language','PageContent','Shop');

    public function beforeFilter() {
        parent::beforeFilter();
        $this->_checkShop();
    }

    public function view_index(){
        $this->response->compress();
        $this->layout = "ajax";
    }

    public function view_add(){
        $this->response->compress();
        $this->layout = "ajax";
    }

    public function available_languages(){
        $this->response->compress();
        $languages = $this->Language->find('all',array(
            "recursive" => -1,
            "order"     => "lang",
            "fields" => array(
                "id",
                "lang"
            ),
            "conditions" => array(
                "ShopLanguage.id"       => NULL
            ),

            "joins" => array(
                array(
                    'table' => 'shop_languages',
                    'alias' => 'ShopLanguage',
                    'type' => 'LEFT',
                    'conditions' => array(
                        'Language.id = ShopLanguage.language_id',
                        "ShopLanguage.shop_id"  => $this->shop_id
                    )
                )
        ),
        ));
        $languages =  Set::extract('/Language/.', $languages);
        $this->set(array(
            'languages' => $languages,
            '_serialize' => array('languages')
        ));
    }

    public function list_languages()
    {
        $this->response->compress();
        $data = array();
        $data['languages'] = array();
        $data['default_language_id'] = $this->Shop->getDefaultLanguage($this->shop_id);
        $languages = $this->ShopLanguage->find("all", array(
            "conditions" => array(
                "shop_id" => $this->shop_id
            ),
            "joins" => array(
                array(
                    'table' => 'languages',
                    'alias' => 'Language',
                    'type' => 'RIGHT',
                    'conditions' => array(
                        'Language.id = ShopLanguage.language_id'
                    )
                )
            ),
            "fields" => array(
                "ShopLanguage.id",
                "Language.iso_code",
                "Language.lang",
                "Language.id",
                "ShopLanguage.timestamp_created"
            ),
            "order" => "ShopLanguage.order ASC",
            "recursive" => -1
        ));
        foreach ($languages as $language) {
            $data['languages'][] = array(
                "id" => $language['ShopLanguage']['id'],
                "lang" => $language['Language']['lang'],
                "code" => $language['Language']['iso_code'],
                "language_id" => $language['Language']['id'],
                "timestamp_created" => $language['ShopLanguage']['timestamp_created'],
            );
        }
        $this->set(array(
            'data' => $data,
            '_serialize' => array('data')
        ));

    }

    public function add(){
        $this->response->compress();
        $data = array();
        $data['success']    = false;
        $data['language']   = array();
        if($this->request->is("post")) {
            if(isset($this->request->data['language_id'])) {
                $available_language = $this->Language->find('first',array(
                    "recursive" => -1,
                    "fields" => array(
                        "id",
                        "lang",
                        "iso_code"
                    ),
                    "conditions" => array(
                        "ShopLanguage.id" => NULL,
                        "Language.id" => $this->request->data['language_id']
                    ),
                    "joins" => array(
                        array(
                            'table' => 'shop_languages',
                            'alias' => 'ShopLanguage',
                            'type' => 'LEFT',
                            'conditions' => array(
                                'Language.id = ShopLanguage.language_id',
                                'ShopLanguage.shop_id' => $this->shop_id
                            )
                        )
                    ),
                ));
                if($available_language) {
                    $data = array(
                        "ShopLanguage" => array(
                            "shop_id" => $this->shop_id,
                            "order" => $this->ShopLanguage->calcOrderLanguage($this->shop_id),
                            "language_id" => $this->request->data['language_id']
                        )
                    );
                    $this->ShopLanguage->create();
                    $data['success'] = (boolean)$this->ShopLanguage->save($data);
                    $data['language'] = array(
                        "id"                => $this->ShopLanguage->id,
                        "lang"              => $available_language['Language']['lang'],
                        "code"              => $available_language['Language']['iso_code'],
                        "timestamp_created" => time() * 1000
                    );
                }
            }
        }
        $this->set(array(
            'data' => $data,
            '_serialize' => array('data')
        ));
    }

    public function delete() {
        $data = array();
        $data['success'] = false;
        $data['message'] = __("Si è verificato un problema durante la rimozione della lingua.");
        if($this->request->is("post")) {
            $shop_language = $this->ShopLanguage->find("first",array(
                "conditions" => array(
                    "id"        => $this->request->data['language_id'],
                    "shop_id"   => $this->shop_id,
                    "NOT" => array(
                        "language_id" => $this->default_language_id
                    )
                ),
                "fields" => array(
                    "id"
                ),
                "recursive" => -1
            ));
            if($shop_language) {
                if($this->ShopLanguage->delete($this->request->data['language_id'])){
                    $this->PageContent->deleteAll(array(
                        "PageContent.lang_id"   => $this->request->data['language_id'],
                        "Page.shop_id"   => $this->shop_id
                    ));
                    $data['success'] = true;
                    $data['message'] = __("La lingua è stata rimossa con sucesso.");

                }
            }
        }
        $this->set(array(
            'data' => $data,
            '_serialize' => array('data')
        ));
    }

    public function getShopLanguage() {
        $default_lang = $this->language_id;
        if(!$default_lang) {
            $default_lang = $this->Shop->getDefaultLanguage($this->shop_id);
        }
        $shop_languages = $this->ShopLanguage->getShopLanguages($this->shop_id);

        $this->set(array(
            'languages' => $shop_languages,
            'default_language' => $default_lang,
            '_serialize' => array('languages', 'default_language')
        ));
    }

}