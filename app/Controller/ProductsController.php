<?php
class ProductsController extends AppController {

    public $components = array('RequestHandler','Paginator','Image','PriceHelper','UserAction','NegozyApps');
    public $uses = array(
        'Shop',
        'ShopLanguage',
        'Product',
        'ProductContent',
        'Tag',
        'ProductImage',
        'ShopTax',
        'ShippingGroup',
        'ProductCategory'
    );

    public function beforeFilter() {
        parent::beforeFilter();
        $this->_checkShop();
    }

    public function view_index() {
        $plugin_elements = $this->NegozyApps->render_button_product_list($this->shop_id);
        $this->set("plugin_elements",$plugin_elements);
        $this->response->compress();
        $this->layout = "ajax";
    }

    public function view_add() {
        $this->response->compress();
        $this->layout = "ajax";
    }

    public function view_edit($url = NULL) {
        $this->response->compress();
        $this->layout = "ajax";
    }

    public function get_list($n=10,$page=1,$row="",$orderType="",$language_id) {
        $matrix_languages = array();
        $default_lang = $this->language_id;

        $language_id = (int) $language_id;
        if($language_id) {
            $this->Session->write("language_id", $language_id);
            $default_lang = $language_id;
        }

        $shopLanguages = $this->ShopLanguage->getShopLanguages($this->shop_id);
        foreach($shopLanguages as $k => $v) {
            $matrix_languages[$k] = array();
        }

        $this->Paginator->settings = array(
            "recursive" => -1,
            "conditions" => array(
                "Product.shop_id"   => $this->shop_id,
                "Product.deleted"   => false
            ),
            "contain" => array(
                "ProductContent" => array(
                    "conditions" => array(
                        "ProductContent.language_id" => array_keys($shopLanguages)
                    ),
                    "fields" => array(
                        "ProductContent.id",
                        "ProductContent.language_id",
                        "ProductContent.name"
                    )
                ),
                "ProductImage" => array(
                    "fields" => array(
                        "ProductImage.path",
                        "ProductImage.name",
                        "ProductImage.ext",
                        "ProductImage.order"
                    ),
                    "conditions" => array(
                        "ProductImage.primary" => 1
                    ),
                    "order" => array(
                        "ProductImage.order"
                    )
                ),
                "ProductCategory" => array(
                    "fields" => array(
                        "ProductCategory.id",
                    ),
                    "ProductCategoryContent" => array(
                        "fields" => array(
                            "ProductCategoryContent.title"
                        ),
                        "conditions" => array(
                            "ProductCategoryContent.language_id" => $this->language_id
                        )
                    )
                ),
                "ShopTax" => array(
                    "fields" => array(
                        'tax',
                        'product_tax'
                    )
                )
            ),
            "joins" => array(
                array(
                    'table' => 'product_contents',
                    'alias' => 'ProductContent',
                    'type'  => 'LEFT',
                    'conditions' => array(
                        'Product.id = ProductContent.product_id',
                        'Product.shop_id' => $this->shop_id,
                        "ProductContent.language_id" => array_keys($shopLanguages)
                    )
                )
            ),
            "fields" => array(
                "Product.id",
                "Product.price",
                "Product.published",
                "Product.quantity",
                "Product.type_quantity",
            ),
            "limit" => $n,
            "page" => $page
        );

        $count = $this->Product->find("count",array(
            "conditions" => array(
                "Product.shop_id"   => $this->shop_id,
                "Product.deleted"   => false
            ),
            "recursive" => -1
        ));
        $tmp_products           = $this->Paginator->paginate('Product');
        $distinct_products      = array();
        $default_no_products    = array();
        foreach($tmp_products as $product) {
            $product_id                         = $product['Product']['id'];
            $distinct_products[$product_id]     = array();
            foreach($product['ProductContent'] as $product_content) {
                if(isset($matrix_languages[$product_content['language_id']])) {
                    $matrix_languages[$product_content['language_id']][$product_content['product_id']] = $product_content['id'];
                } else {
                    $matrix_languages[$product_content['language_id']] = array(
                        $product_content['product_id'] => $product_content['id']
                    );
                }
                $product_category = "---";
                if(isset($product['ProductCategory']) && isset($product['ProductCategory']['ProductCategoryContent']) && count($product['ProductCategory']['ProductCategoryContent'])) {
                    $product_category = $product['ProductCategory']['ProductCategoryContent'][0]['title'];
                }
                $image = (isset($product['ProductImage']) && count($product['ProductImage'])) ? $product['ProductImage'][0] : null;
                $currency = $this->_shopCurrecy();
                $price = $this->PriceHelper->format_price($product['Product']['price'],$product['ShopTax']);
                $price = CakeNumber::currency($price, $currency);

                $quantity = __("Richeista di disponibilità");
                if($product['Product']['type_quantity'] == 0){
                    $quantity = $product['Product']['quantity'];
                } else if ($product['Product']['type_quantity'] == 1) {
                    $quantity = __("Illimitati");
                }

                if($default_lang == $product_content['language_id']) {
                    $distinct_products[$product_id] = array(
                        'name'              => $product_content['name'],
                        'content_id'        => $product_content['id'],
                        'product_category'  => $product_category,
                        'image'             => $this->Image->build_path("products","thumb_",$image),
                        'price'             => $price,
                        'published'         => $product['Product']['published'],
                        'quantity'          => $quantity
                    );
                }
                if($this->default_language_id == $product_content['language_id']) {
                    $default_no_products[$product_id] = array(
                        'name'              => $product_content['name'],
                        'content_id'        => $product_content['id'],
                        'product_category'  => $product_category,
                        'image'             => $this->Image->build_path("products","thumb_",$image),
                        'price'             => $price,
                        'published'         => $product['Product']['published'],
                        'quantity'          => $quantity
                    );
                }
            }
        }
        $products = array();
        foreach($distinct_products as $product_id => $default_product) {
            if(!count($default_product)) {
                $tmpRow =array(
                    "id"                =>  $product_id,
                    "name"              =>  isset($default_no_products[$product_id]) ? $default_no_products[$product_id]['name'] : null,
                    'content_id'        =>  null,
                    'contents'          =>  array(),
                    'product_category'  =>  isset($default_no_products[$product_id]) ? $default_no_products[$product_id]['product_category'] : null,
                    'contents'          =>  array(),
                    'image'             =>  isset($default_no_products[$product_id]) ? $default_no_products[$product_id]['image'] : null,
                    'price'             =>  isset($default_no_products[$product_id]) ? $default_no_products[$product_id]['price'] : null,
                    'published'         =>  isset($default_no_products[$product_id]) ? $default_no_products[$product_id]['published'] : null,
                    'quantity'          =>  isset($default_no_products[$product_id]) ? $default_no_products[$product_id]['quantity'] : null
                );
            } else {
                $tmpRow =array(
                    'id'                =>  $product_id,
                    'name'              =>  $default_product['name'],
                    'content_id'        =>  $default_product['content_id'],
                    'product_category'  =>  $default_product['product_category'],
                    'contents'          =>  array(),
                    'image'             =>  $default_product['image'],
                    'price'             =>  $default_product['price'],
                    'published'         =>  $default_product['published'],
                    'quantity'          =>  $default_product['quantity']
                );
            }
            foreach($matrix_languages as $lang_id => $content) {
                $tmpRow['contents'][] = array(
                    'lang_id'       =>$lang_id,
                    'content_id'    => isset($matrix_languages[$lang_id][$product_id]) ? $matrix_languages[$lang_id][$product_id] : null
                );

            }
            $products[] = $tmpRow;
        }
        $this->response->compress();
        $this->set(array(
            'products'      => $products,
            'matrix'        => $matrix_languages,
            'count'         => $count,
            '_serialize'    => array('products','count','matrix')
        ));
    }

    public function add() {
        $data = array();
        $data['success'] = false;
        $data['message'] = __("Si è verificato un problema durante il salvataggio del prodotto.");
        if($this->request->is("post")) {
            $product_id         = $this->_saveProduct();
            $updated_content    = $this->_saveProductContents($product_id,$this->request->data['product']['content']['language_id'],null);
            if($product_id && $updated_content) {
                $data['success'] =  true;
                $data['product_id'] = $product_id;
                $data['message'] =  __("Prodotto modificato con successo!");
            }
        }
        $this->set(array(
            'data' => $data,
            '_serialize' => array('data')
        ));
    }

    public function delete() {
        $data = array();
        $data['success'] = false;
        $data['message'] = __("Si è verificato un problema durante la rimozione del prodotto.");
        if($this->request->is("post")) {
            $ck_owner = $this->Product->find("count",array(
                "conditions" => array(
                    "shop_id"   => $this->shop_id,
                    "id"        => $this->request->data['product_id']
                ),
                "recursive" => -1
            ));
            if($ck_owner) {
                $this->Product->id = $this->request->data['product_id'];
                $deleted = $this->Product->saveField("deleted",1);
                if ($deleted) {
                    $data['success'] = true;
                    $data['message'] = __("Il prodotto è stato rimosso con successo.");
                }
            }
        }
        $this->set(array(
            'data' => $data,
            '_serialize' => array('data')
        ));
    }

    public function clone_product() {
        $data = array();
        $data['success'] = false;
        $data['message'] = __("Si è verificato un problema durante la clonazione del prodotto.");
        if($this->request->is("post")) {
            $ck_owner = $this->Product->find("count",array(
                "conditions" => array(
                    "shop_id"   => $this->shop_id,
                    "id"        => $this->request->data['product_id']
                ),
                "recursive" => -1
            ));
            if($ck_owner) {
                $row = $this->Product->find("first",array(
                    "conditions" => array(
                        "id" => $this->request->data['product_id']
                    ),
                    "contain" => array(
                        "ProductContent",
                    ),
                    "recursive" => -1
                ));
                $row['Product']['id'] = NULL;
                foreach($row['ProductContent'] as $key => $value) {
                    $row['ProductContent'][$key]['id'] = NULL;
                    $row['ProductContent'][$key]['uri'] = $row['ProductContent'][$key]['uri'] . "-" . time();
                }
                $this->Product->create();
                $clone = $this->Product->saveAll($row);
                if ($clone) {
                    $data['success'] = true;
                    $data['message'] = __("Il prodotto è stato clonato con successo.");
                }
            }
        }
        $this->set(array(
            'data' => $data,
            '_serialize' => array('data')
        ));
    }

    public function deleteAll() {
        $data['success'] = false;
        $data['message'] = __("Prodotti rimossi con successo!");
        if($this->request->is("post")) {
            $product_ids            = $this->request->data["products"];
            if($product_ids) {
                $products = $this->Product->find("count",array(
                    "conditions" => array(
                        "id"        => $product_ids,
                        "shop_id"   => $this->shop_id
                    ),
                    "recursive" => -1
                ));
                $dataToSave = array();
                if(($products == count($product_ids))) {
                    foreach ($product_ids as $id) {
                        $dataToSave[] = array(
                            "Product" => array(
                                "id"        => $id,
                                "deleted"   => true
                            )
                        );
                    }
                }
                if($this->Product->saveAll($dataToSave)) {
                    $data['success'] = true;
                    $data['message'] = __("Prodotti rimossi con successo!");
                }
            }
        }
        $this->set(array(
            'data' => $data,
            '_serialize' => array('data')
        ));
    }

    public function update() {
        $data = array();
        $data['success'] = false;
        $data['message'] = __("Si è verificato un problema durante il salvataggio del prodotto.");
        if($this->request->is("post")) {
            $product = $this->Product->find("first",array(
                "conditions" => array(
                    "Product.id"        => $this->request->data['product']['id'],
                    "Product.shop_id"   => $this->shop_id
                ),
                "fields" => array(
                    "Product.id"
                ),
                "recursive" => -1
            ));
            if($product) {
                $content = $this->ProductContent->find("first",array(
                    "conditions" => array(
                        "id"            => $this->request->data['product']['content']['id'],
                        "language_id"   => $this->request->data['product']['content']['language_id'],
                        "product_id"    => $this->request->data['product']['id']
                    ),
                    "fields" => array(
                        "id",
                        "uri"
                    ),
                    "recursive" => -1
                ));
                $updated_product = $this->_saveProduct($this->request->data['product']['id']);
                $updated_content = $this->_saveProductContents($this->request->data['product']['id'],$this->request->data['product']['content']['language_id'],$content);
                if($updated_product && $updated_content) {
                    $this->_orderImages($this->request->data['product']['id'],$this->request->data['product']['images']);
                    $data['success'] =  true;
                    $data['message'] =  __("Prodotto modificato con successo!");
                }
            }
        }
        $this->set(array(
            'data' => $data,
            '_serialize' => array('data')
        ));
    }

    public function loadImage($product_id = NULL){
        $data = array(
           "success" => false,
            "url"   => false,
            "name"   => false
        ) ;
        if(isset($product_id) && !empty($product_id) && isset($_FILES['file'])) {
            $product = $this->Product->find("first",array(
                "recursive" => -1,
                "conditions" => array(
                    "id"        => $product_id,
                    "shop_id"   => $this->shop_id
                ),
                "fields" => array(
                    "id"
                )
            ));
            if($product) {
                $types = array();
                $types[] = array("width" => 450, "height" => null, "prefix" => "thumb_");
                $types[] = array("width" => 800, "height" => null, "prefix" => "image_");
                $image_data = $this->Image->uploadImage($_FILES['file'],'products',$types);
                if ($image_data['success']) {
                    $newImage = array();
                    $newImage['ProductImage'] = $image_data['image'];
                    $newImage['ProductImage']['product_id'] = $product['Product']['id'];
                    $last_image = $this->ProductImage->find("first",array(
                        "conditions" => array(
                            "product_id" => $product_id
                        )
                    ));
                    if(!$last_image) {
                        $newImage['ProductImage']['order'] = 1;
                        $newImage['ProductImage']['primary'] = 1;
                    } else {
                        $newImage['ProductImage']['order'] = $last_image['ProductImage']['order'] + 1;
                        $newImage['ProductImage']['primary'] = 0;
                    }
                    if($this->ProductImage->save($newImage)) {
                        $data['success']    = true;
                        $data['url']        = $this->Image->build_path("products", "thumb_", $image_data['image']);
                        $data['name']       = $image_data['image']['name'];
                    }
                }
            }
        }
        $this->set(array(
            'data' => $data,
            '_serialize' => array('data')
        ));
    }

    public function removeImage($product_id = null,$image = NULL) {
        $data = array(
            "success" => false
        ) ;
        if(isset($product_id) && isset($image)) {
            $product = $this->Product->find("first",
                array(
                    "conditions" => array(
                        "id" => $product_id,
                        "shop_id" => $this->shop_id
                    ),
                    "recursive" => -1
                )

            );
            if(isset($product['Product']['id'])) {
                $product_image = $this->ProductImage->find("first",
                    array(
                        "conditions" => array(
                            "ProductImage.name" => $image,
                            "ProductImage.product_id" => $product['Product']['id']
                        )
                    )
                );
                if($product_image) {
                    $this->Image->remove_image($product_image['ProductImage']['name'].".".$product_image['ProductImage']['ext'],"products/".$product_image['ProductImage']['path'],"thumb_");
                    $this->Image->remove_image($product_image['ProductImage']['name'].".".$product_image['ProductImage']['ext'],"products/".$product_image['ProductImage']['path'],"image_");
                    if($this->ProductImage->delete($product_image['ProductImage']['id'])) {
                        $images = $this->ProductImage->find("all",array(
                            "conditions" => array(
                                "ProductImage.product_id" => $product['Product']['id']
                            ),
                            "fileds" => array(
                                "ProductImage.id",
                                "ProductImage.name"
                            )
                        ));
                        $images = Set::extract("/ProductImage/.",$images);
                        $this->_orderImages($product['Product']['id'],$images);
                        $data['success'] = true;
                    }
                }
            }
        }

        $this->set(array(
            'data' => $data,
            '_serialize' => array('data')
        ));
    }

    public function loadProduct($id = NULL,$language_id=NULL){
        if(isset($id) && !empty($id) && isset($language_id) && !empty($language_id)) {

            $check_lang = $this->ShopLanguage->find("count",array(
                "conditions" => array(
                    "language_id"   => $language_id,
                    "shop_id"       => $this->shop_id
                ),
                "recursive" => -1
            ));
            if(!$check_lang) {
                $this->response->statusCode(404);
                $data['data']['message'] = "Page not found";
                $this->set(array(
                    'data' => $data,
                    '_serialize' => array('data')
                ));
                $this->response->send();
                $this->_stop();
            }

            $tmp_product = $this->Product->find("first",array(
                "conditions" => array(
                    "id"        => $id,
                    "shop_id"   => $this->shop_id,
                    "deleted"   => false
                ),
                "contain" => array(
                    "Tag" => array(
                        "fields" => array(
                            "Tag.id",
                            "Tag.text"
                        )
                    ),
                    "ProductImage" => array(
                        "fields" => array(
                            "ProductImage.path",
                            "ProductImage.name",
                            "ProductImage.ext",
                            "ProductImage.order"
                        ),
                        "order" => array(
                            "ProductImage.order"
                        )
                    ),
                    "ProductContent" => array(
                        "conditions" => array(
                            "language_id" => $language_id
                        )
                    )
                ),
                "fields" => array(
                    "Product.id",
                    "Product.price",
                    "Product.quantity",
                    "Product.type_quantity",
                    "Product.shop_tax_id",
                    "Product.shipping_group_id",
                    "Product.product_category_id",
                    "Product.published"
                ),
                "recursive" => -1
            ));

            if(!$tmp_product) {
                $this->response->statusCode(404);
                $data['data']['message'] = "Product not found";
                $this->set(array(
                    'data' => $data,
                    '_serialize' => array('data')
                ));
                $this->response->send();
                $this->_stop();
            }

            $tmp_product['Product']['unlimited'] = ($tmp_product['Product']['quantity']) ? false : true;
            $data = $this->_loadProductData();
            $images = array();
            $tags = array();
            if(isset($tmp_product['ProductImage']) && count($tmp_product['ProductImage'])) {
                foreach($tmp_product['ProductImage'] as $product_image) {
                    $images[] = array(
                        "image" => $this->Image->build_path("products", "thumb_", $product_image),
                        "name"  => $product_image['name'],
                        "order" => $product_image['order']
                    );
                }
            }
            foreach($tmp_product['Tag'] as $tag) {
                $tags[] = array(
                    "id"    => $tag['id'],
                    "text"  => $tag['text']
                );
            }
            $content = null;
            if(isset($tmp_product['ProductContent']) && count($tmp_product['ProductContent']))  {
                $content = $tmp_product['ProductContent'][0];
            }
            $product = array(
                "content" => array(
                    "id"                => $content['id'],
                    "language_id"       => $language_id,
                    "name"              => $content['name'],
                    "uri"               => $content['uri'],
                    "description"       => $content['description'],
                    "meta_keywords"     => json_decode($content['meta_keywords'],true),
                    "meta_description"  => $content['meta_description']
                ),
                "id"                    => $tmp_product['Product']['id'],
                "price"                 => floatval($tmp_product['Product']['price']),
                "quantity"              => $tmp_product['Product']['quantity'],
                "type_quantity"         => $tmp_product['Product']['type_quantity'],
                "published"             => $tmp_product['Product']['published'],
                "product_category_id"   => $tmp_product['Product']['product_category_id'],
                "shipping_group_id"     => $tmp_product['Product']['shipping_group_id'],
                "shop_tax_id"           => $tmp_product['Product']['shop_tax_id'],
                "tags"                  => $tags,
                "images"                => $images
            );
            $this->response->compress();
            $this->set(array(
                'data'          => $data,
                'product'       => $product,
                '_serialize'    => array('product','data')
            ));

        }
    }

    public function loadProductData(){
        $this->response->compress();
        $data = $this->_loadProductData();
        $this->set(array(
            'data' => $data,
            '_serialize' => array('data')
        ));
    }

    public function addToCollection(){
        $data['success'] = false;
        $data['message'] = __("Prodotti aggiunti alla collezione");
        if($this->request->is("post")) {
            $product_ids            = $this->request->data["products"];
            $product_category_id    = $this->request->data["product_category_id"];
            if($product_category_id && $product_ids) {
                $product_category = $this->ProductCategory->find("count",array(
                    "conditions" => array(
                        "id"        => $product_category_id,
                        "shop_id"   => $this->shop_id
                    ),
                    "recursive" => -1
                ));
                $products = $this->Product->find("count",array(
                    "conditions" => array(
                        "id"        => $product_ids,
                        "shop_id"   => $this->shop_id
                    ),
                    "recursive" => -1
                ));
                $dataToSave = array();
                if(($products == count($product_ids)) && $product_category) {
                    foreach ($product_ids as $id) {
                        $dataToSave[] = array(
                            "Product" => array(
                                "id"                    => $id,
                                "product_category_id"   => $product_category_id
                            )
                        );
                    }
                }
                if($this->Product->saveAll($dataToSave)) {
                    $data['success'] = true;
                    $data['message'] = __("Prodotti aggiunti alla collezione");
                }
            }
        }
        $this->set(array(
            'data' => $data,
            '_serialize' => array('data')
        ));
    }

    public function addToShippingGroup(){
        $data['success'] = false;
        $data['message'] = __("Non è stato possibile aggiungere i prodotti al gruppo di spedizione");
        if($this->request->is("post")) {
            $product_ids            = $this->request->data["products"];
            $shipping_group_id      = $this->request->data["shipping_group_id"];
            if($shipping_group_id && $product_ids) {
                $this->loadModel("ShippingGroup");
                $shipping_group = $this->ShippingGroup->find("first", array(
                    "conditions" => array(
                        "id" => $shipping_group_id,
                        "shop_id" => $this->shop_id
                    ),
                    "fields" => array(
                        "id"
                    ),
                    "recursive" => -1
                ));
                $products = $this->Product->find("count",array(
                    "conditions" => array(
                        "id"        => $product_ids,
                        "shop_id"   => $this->shop_id
                    ),
                    "recursive" => -1
                ));
                if ($shipping_group && ($products == count($product_ids))) {
                    $dataToSave = array();
                    foreach ($product_ids as $id) {
                        $dataToSave[] = array(
                            "Product" => array(
                                "id" => $id,
                                "shipping_group_id" => $shipping_group_id
                            )
                        );
                    }
                    if ($this->Product->saveAll($dataToSave, array('deep' => true))) {
                        $data['success'] = true;
                        $data['message'] = __("Prodotti aggiunti al gruppo di spedizione");
                    }
                }
            }
        }
        $this->set(array(
            'data' => $data,
            '_serialize' => array('data')
        ));
    }

    private function _ckUniqueProductURL($url = NULL){
        $count_url = false;
        if(isset($url) && !empty($url)) {
            $count_url = $this->Product->find("count",array(
                "conditions" => array(
                    "Product.shop_id"   => $this->shop_id,
                    "Product.url"       => $url
                )
            ));
        }
        return $count_url;
    }

    private function _calcTags($tags = array()) {
        $product_tags = array();
        if (count($tags)) {
            foreach ($tags as $tag) {
                $new_tag = array();
                if(isset($tag['id']) && !empty($tag['id'])) {
                    $product_tags[] = $tag['id'];
                } else {
                    $ck_tag = $this->Tag->find("first",array(
                        "conditions" => array(
                            "text"      => $tag['text'],
                            "shop_id"   => $this->shop_id
                        ),
                        "fields" => array(
                            "id"
                        )
                    ));
                    if($ck_tag) {
                        $product_tags[] = $ck_tag['Tag']['id'];
                    } else {
                        $new_tag['text']    = $tag['text'];
                        $new_tag['shop_id'] = $this->shop_id;
                        $this->Tag->create();
                        if ($this->Tag->save($new_tag)) {
                            $product_tags[] = $this->Tag->id;
                        }
                    }
                }
            }
        }
        return $product_tags;
    }

    private function _orderImages($product_id = NULL,$images = array()) {
        if(count($images) && isset($product_id)) {
            $order = 1;
            $primary = true;
            if(count($images)) {
                foreach ($images as $image) {
                    $db_image = $this->ProductImage->find("first", array(
                        "conditions" => array(
                            "name" => $image['name'],
                            "product_id" => $product_id
                        ),
                        "fields" => "id"
                    ));
                    if ($db_image) {
                        $db_image['ProductImage']['order'] = $order;
                        $db_image['ProductImage']['primary'] = $primary;
                        $this->ProductImage->id = $db_image['ProductImage']['id'];
                        if ($this->ProductImage->save($db_image)) {
                            $order++;
                            if ($primary) {
                                $primary = false;
                            }
                        }
                    }
                }
            }
        }
    }

    private function _loadProductData() {
        $arrayReturn = array();
        $taxes = $this->ShopTax->find("all",array(
            "conditions" => array(
                "shop_id" => $this->shop_id
            ),
            "fields" => array(
                'id',
                'label',
                'tax',
                'product_tax'
            ),
            "recursive" => -1
        ));
        $shipments = $this->ShippingGroup->find("all",array(
            "conditions" => array(
                "shop_id" => $this->shop_id
            ),
            "fields" => array(
                'id',
                'name'
            ),
            "recursive" => -1
        ));
        $arrayReturn['collections'] = $this->ProductCategory->get_graphical_tree($this->shop_id,$this->language_id);
        foreach($taxes as $tax) {
            $arrayReturn['taxes'][] = $tax['ShopTax'];
        }
        foreach($shipments as $shipping) {
            $arrayReturn['shipments'][] = $shipping['ShippingGroup'];
        }
        $shop = $this->Shop->find("first",array(
            "conditions" => array(
                "id" => $this->shop_id
            ),
            "recursive" => -1,
            "fields" => "url"
        ));
        $arrayReturn['shop'] = array(
            "url" => $shop['Shop']['url']
        );
        return $arrayReturn;
    }

    private function _saveProductContents($product_id = null, $language_id = NULL,$content = NULL) {
        if(!$content || ($content['ProductContent']['uri'] != $this->request->data['product']['content']['uri'])) {
            $ck_product_URL = $this->_ckUniqueURL($this->request->data['product']['content']['uri']);
            if ($ck_product_URL) {
                $this->request->data['product']['content']['uri'] = $this->request->data['product']['content']['uri'] . "-" . time();
            }
        }
        $data = array(
            "ProductContent" => array(
                'name'                  => (isset($this->request->data['product']['content']['name'])) ? $this->request->data['product']['content']['name'] : '',
                'description'           => (isset($this->request->data['product']['content']['description'])) ? $this->request->data['product']['content']['description'] : '',
                'meta_keywords'         => (isset($this->request->data['product']['content']['meta_keywords'])) ? json_encode($this->request->data['product']['content']['meta_keywords']) : null,
                'meta_description'      => (isset($this->request->data['product']['content']['meta_description'])) ? $this->request->data['product']['content']['meta_description'] : '',
                'uri'                   => (isset($this->request->data['product']['content']['uri'])) ? $this->request->data['product']['content']['uri'] : '',
                'product_id'            => $product_id,
                'language_id'           => $language_id,
            )
        );
        if($content && isset($content['ProductContent']['id'])) {
            $this->ProductContent->id = $content['ProductContent']['id'];
        } else {
            $this->ProductContent->create();
        }
        if($this->ProductContent->save($data)) {
            return true;
        }
        return false;
    }

    private function _saveProduct($product_id = null) {
        $data = array(
            "Product" => array(
                "price"               => (isset($this->request->data['product']['price'])) ? $this->request->data['product']['price'] : 0,
                "shop_tax_id"         => (isset($this->request->data['product']['shop_tax_id'])) ? $this->request->data['product']['shop_tax_id'] : null,
                "product_category_id" => (isset($this->request->data['product']['product_category_id'])) ? $this->request->data['product']['product_category_id'] : null,
                "shipping_group_id"   => (isset($this->request->data['product']['shipping_group_id'])) ? $this->request->data['product']['shipping_group_id'] : null,
                "quantity"            => (isset($this->request->data['product']['quantity'])) ? $this->request->data['product']['quantity'] : null,
                "type_quantity"       => (isset($this->request->data['product']['type_quantity'])) ? $this->request->data['product']['type_quantity'] : null,
                "published"           => (isset($this->request->data['product']['published'])) ? $this->request->data['product']['published'] : null,
            ),
            "Tag" => $this->_calcTags($this->request->data['product']['tags'])
        );
        if(isset($product_id)) {
            $this->Product->id = $product_id;
        } else {
            $this->Product->create();
            $data['Product']['shop_id'] = $this->shop_id;
        }
        if($this->Product->save($data)) {

            return $this->Product->id;
        }
        return false;
    }

    private function _ckUniqueURL($url = NULL){
        $count_url = false;
        if(isset($url) && !empty($url)) {
            $count_url = $this->ProductContent->find("count",array(
                "conditions" => array(
                    "ProductContent.uri" => $url
                ),
                "joins" => array(
                    array(
                        'table' => 'products',
                        'alias' => 'Product',
                        'type' => 'RIGHT',
                        'conditions' => array(
                            'Product.id = ProductContent.product_id',
                            'Product.shop_id' => $this->shop_id
                        )
                    )
                ),
                "recursive" => -1
            ));
        }
        return $count_url;
    }

    private function _shopCurrecy(){
        $shop_currency = $this->Shop->find("first",array(
            "conditions" => array(
                "id" => $this->shop_id
            ),
            "fields" => array(
                "currency"
            ),
            "recursive" => -1
        ));
        $currency = 'EUR';
        if($shop_currency) {
            $currency = $shop_currency['Shop']['currency'];
        }
        return $currency;
    }

}