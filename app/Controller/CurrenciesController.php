<?php
class CurrenciesController extends AppController {

    public $components = array('RequestHandler');



    public function index() {
        $currencies = $this->Currency->find('all');
        $currencies =  Set::extract('/Currency/.', $currencies);
        $this->set(array(
            'currencies' => $currencies,
            '_serialize' => array('currencies')
        ));
    }

}