<?php
class TagsController extends AppController {

    public $components = array('RequestHandler');
    public $uses = array(
        'Shop',
        'Product',
        'Tag'
    );

    public function beforeFilter() {
        parent::beforeFilter();
        $this->_checkShop();
    }

    public function index() {
        $data = array();
        $data['success'] = false;
        $data['tags'] = array();
        $tags = $this->Tag->find("all",array(
            "conditions" => array(
                "shop_id" => $this->shop_id
            ),
            "fields" => array(
                "id",
                "text"
            )
        ));
        $tags =  Set::extract('/Tag/.', $tags);
        $this->set(compact('tags'));
        $this->set('_serialize', 'tags');
    }

    public function search($query = ""){
        $data = array();
        $data['success'] = false;
        $data['tags'] = array();
        $tags = $this->Tag->find("all",array(
            "conditions" => array(
                "shop_id" => $this->shop_id,
                "text LIKE" => '%'.$query.'%'
            ),
            "fields" => array(
                "id",
                "text"
            )
        ));
        $tags =  Set::extract('/Tag/.', $tags);
        $this->set(compact('tags'));
        $this->set('_serialize', 'tags');
    }

}