<?php
class ShopsController extends AppController {

    public $uses = array(
        'Shop',
        'ShopLogo',
        'ShopCover',
        'Country',
        'User',
        'Currency',
        'ShopTax',
        'ShippingGroup',
        'Payment',
        'Domain'
    );

    public $components = array('RequestHandler','Image','UserAction','PayPal', 'CheckShop','DefaultShop');

    function beforeFilter(){
        parent::beforeFilter();
        $this->_asyncCheckAuth();
    }

    public function view_dashboard() {
        $this->response->compress();
        $this->_checkShop();
        $this->layout = "ajax";
    }

    public function view_modal_public_shop() {
        $this->response->compress();
        $this->layout = "ajax";
    }

    public function view_add() {
        $this->response->compress();
        $this->layout = "ajax";
    }

    public function view_settings() {
        $this->response->compress();
        $this->_checkShop();
        $this->layout = "ajax";
    }

    public function view_profile() {
        $this->response->compress();
        $this->layout = "ajax";
    }

    public function view_tax_information() {
        $this->response->compress();
        $this->layout = "ajax";
    }

    public function publicShop() {
        $this->_checkShop();
        $this->Shop->id = $this->shop_id;
        $data['success'] = $this->Shop->saveField("is_public",true);
        $this->set(array(
            'data' => $data,
            '_serialize' => array('data')
        ));
    }

    public function dashboardData() {
        $this->_checkShop();
        $data = $this->Shop->find("first",array(
            "recursive" => -1,
            "conditions" => array(
                "id" => $this->shop_id
            ),
            "fields" => array(
                "Shop.name",
                "Shop.product_count",
                "Shop.order_count",
                "Shop.product_category_count",
                "Shop.is_public",
                "Shop.site_url",
            )
        ));
        $data = Set::extract("/Shop/.",$data);
        $data = reset($data);
        $this->set(array(
            'data' => $data,
            '_serialize' => array('data')
        ));
    }
    public function isPublic() {
        $this->_checkShop();
        $data = $this->Shop->find("first",array(
            "recursive" => -1,
            "conditions" => array(
                "id" => $this->shop_id
            ),
            "fields" => array(
                "Shop.is_public",
            )
        ));
        $data = Set::extract("/Shop/.",$data);
        $data = reset($data);
        $this->set(array(
            'data' => array('success' => true, 'shop' => $data),
            '_serialize' => array('data')
        ));
    }


    public function checkShop() {
        $this->_checkShop();
        $data = $this->CheckShop->verify($this->shop_id);
        $actions = array();
        foreach ($data['actions'] as $action => $value){
            $actions[] = $this->CheckShop->getSuggest($action);
        }
        $this->set(array(
            'data' => array('actions' => $actions, 'success' => true, 'actions_count' => $data['count']),
            '_serialize' => array('data')
        ));
    }

    public function checkShopStatus(){
        $this->_checkShop();
        $data = array(
            "taxes" => FALSE,
            "shipping"  => FALSE,
            "payments"  => FALSE,
        );
        $shopTaxes = $this->ShopTax->find("count",array(
            "conditions" => array(
                "shop_id" => $this->shop_id
            ),
            "recursive" => -1
        ));
        if($shopTaxes) {
            $data['taxes'] = TRUE;
        }
        $shopShipping = $this->ShippingGroup->find("count",array(
            "conditions" => array(
                "shop_id" => $this->shop_id
            ),
            "recursive" => -1
        ));
        if($shopShipping) {
            $data['shipping'] = TRUE;
        }
        $payments = $this->Payment->find("count",array(
            "conditions" => array(
                "shop_id" => $this->shop_id
            ),
            "recursive" => -1
        ));
        if($payments) {
            $data['payments'] = TRUE;
        }
        $this->set(array(
            'data' => $data,
            '_serialize' => array('data')
        ));
    }

    public function checkShopName($shop_name = NULL) {
        $data = array();
        $data['success'] = false;
        if(isset($shop_name) && !empty($shop_name)){
            if(!$this->Shop->find("count",array(
                "conditions" => array(
                    "url" => $shop_name
                ),
                "recursive" => -1
            ))) {
                $data['success'] = true;
            }
        }

        $this->set(array(
            'data' => $data,
            '_serialize' => array('data')
        ));
    }

    public function checkVatNumber($vat_number = NULL) {
        $data = array();
        $data['success'] = false;
        if(isset($vat_number) && !empty($vat_number)){
            if(!$this->Shop->find("count",array(
                "conditions" => array(
                    "vat_number" => $vat_number
                ),
                "recursive" => -1
            ))) {
                $data['success'] = true;
            }
        }
        $this->set(array(
            'data' => $data,
            '_serialize' => array('data')
        ));
    }

    public function getCategories(){
        $this->loadModel("Macrocategory");
        $categories = $this->Macrocategory->find("all",array(
            "order" => "label",
            "recursive" => -1,
            "fields" => array(
                "id",
                "label"
            )
        ));
        $categories = Set::extract('/Macrocategory/.',$categories);
        $this->set(array(
            'categories' => $categories,
            '_serialize' => array('categories')
        ));
    }

    public function sendCode() {
        $data['success'] = false;
        if($this->request->is("post")) {
            $phone_number = $this->request->data['phone_number'];
            if(isset($phone_number) && !empty($phone_number)) {
                $this->Skebby = $this->Components->load('Skebby');
                $code = substr( str_shuffle( 'abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789' ) , 0 , 5 );
                $this->Session->write("verify_code",$code);
                $phone_number = "39".$phone_number;
                $this->Skebby->sendSms($phone_number,$code);
                $data['success'] = true;
            }
        }
        $this->set(array(
            'data' => $data,
            '_serialize' => array('data')
        ));
    }

    public function verifyCode(){
        $data['success'] = false;
        if($this->request->is("post")) {
            $code = $this->request->data['code'];
            if(isset($code) && !empty($code)) {
                $session_code = $this->Session->read("verify_code");
                if($code == $session_code) {
                    $data['success'] = true;
                }
            }
        }
        $this->set(array(
            'data' => $data,
            '_serialize' => array('data')
        ));
    }

    public function saveShop() {
        $data = array();
        $data['success'] = false;
        $user_id = $this->Auth->user('id');
        if(isset($user_id) && !empty($user_id)) {
            $shop_data['Shop'] = $this->request->data['shop'];
            $shop_data['Shop']['user_id'] = $user_id;
            $shop_data['Shop']['visible'] = false;
            $shop_data['Shop']['currency'] = 'EUR';
            if($this->Shop->save($shop_data)) {
                $domain =  array(
                    "domain"        => $shop_data['Shop']["url"].".negozy.com",
                    "never_expire"  => TRUE,
                    "shop_id"       => $this->Shop->id
                );
                $this->Domain->save($domain);
                $this->User->id = $user_id;
                $this->User->saveField("phone_number",$this->request->data['shop']['phone_number']);
                $data['success'] = true;
                $this->DefaultShop->buildDefaultShop($this->Shop->id);
                if(Configure::read("sugar_config.enabled")) {
                    $this->request->data['shop']['email'] = $this->Auth->user('email');
                    $this->request->data['shop']['first_name'] = $this->Auth->user('first_name');
                    $this->request->data['shop']['last_name'] = $this->Auth->user('last_name');
                    $this->Sugar = $this->Components->load('Sugar');
                    $this->Sugar->converteLead($this->Auth->user('lead_id'), $this->request->data['shop']);
                }
            }
        }
        $this->set(array(
            'data' => $data,
            '_serialize' => array('data')
        ));
    }

    public function index() {
        $this->_checkShop();
        $shop = $this->Shop->find("first",array(
            "contain" => array(
                "ShopLogo" => array(
                    "fields" => array(
                        "ShopLogo.image"
                    )
                ),
                "ShopCover" => array(
                    "fields" => array(
                        "ShopCover.image"
                    )
                )
            ),
            "conditions" => array(
                "Shop.id" => $this->shop_id,
                "Shop.user_id" => $this->user_id
            ),
            "fields" => array(
                "Shop.name"
            ),
            "recursive" => -1
        ));
        $shop_data  = Set::extract('/Shop/.',$shop);
        $logo       = Set::extract('/ShopLogo/image',$shop);
        $cover      = Set::extract('/ShopCover/image',$shop);
        $logo       = reset($logo);
        $cover      = reset($cover);
        $shop_data  = reset($shop_data);
        $this->set(array(
            'shop' => $shop_data,
            'logo' => $logo,
            'cover' => $cover,
            '_serialize' => array('shop','logo','cover')
        ));
    }

    public function loadSettings() {
        $this->_checkShop();
        $this->response->compress(true);
        $shop = $this->Shop->find("first",array(
            "contain" => array(
                "ShopLogo" => array(
                    "fields" => array(
                        "ShopLogo.image"
                    )
                ),
                "ShopCover" => array(
                    "fields" => array(
                        "ShopCover.image"
                    )
                )
            ),
            "conditions" => array(
                "Shop.id" => $this->shop_id,
                "Shop.user_id" => $this->user_id
            ),
            "fields" => array(
                "name",
                "url",
                "macrocategory_id",
                "phone_number",
                "email",
                "latitude",
                "longitude",
                "contact_address",
                "is_public"
            ),
            "recursive" => -1
        ));

        $shop_data  = Set::extract('/Shop/.',$shop);
        $shop_data  = reset($shop_data);
        $logo       = NULL;
        $cover      = NULL;
        if(count($shop['ShopLogo'])) {
            $logo = Set::extract('/ShopLogo/image',$shop);
            $logo = reset($logo);
        }
        if(count($shop['ShopCover'])) {
            $cover = Set::extract('/ShopCover/image', $shop);
            $cover = reset($cover);
        }
        $this->loadModel('Macrocategory');
        $categories = $this->Macrocategory->find("all",array(
            "fields" => array(
                "id",
                "label"
            ),
            "order" => array("label"),
            "recursive" => -1
        ));
        $categories =Set::extract('/Macrocategory/.',$categories);
        $this->set(array(
            'shop' => $shop_data,
            'categories' => $categories,
            'cover'      => $cover,
            'logo'       => $logo,
            '_serialize' => array('shop','categories','cover','logo')
        ));
    }

    public function loadTaxInformation() {
        $this->_checkShop();
        $this->response->compress(true);
        $shop = $this->Shop->find("first",array(
            "conditions" => array(
                "Shop.id" => $this->shop_id,
                "Shop.user_id" => $this->user_id
            ),
            "fields" => array(
                "business_name",
                "vat_number",
                "address",
                "cap",
                "city",
                "province",
                "state",
                "currency"
            ),
            "recursive" => -1
        ));
        $shop_data = Set::extract('/Shop/.',$shop);
        $shop_data = reset($shop_data);
        $countries = $this->Country->find('all');
        $countries =  Set::extract('/Country/.', $countries);
        $currencies = $this->Currency->find('all');
        $currencies =  Set::extract('/Currency/.', $currencies);
        $this->set(array(
            'shop' => $shop_data,
            'countries' => $countries,
            'currencies' => $currencies,
            '_serialize' => array('shop','countries','currencies')
        ));
    }


    public function updateShop() {
        $this->_checkShop();
        $data = array();
        $data['success'] = false;
        $data['message'] = __("Si è verificato un problema durante il salvataggio dei dati del negozio");
        if($this->request->is("post")) {
            $this->Shop->id = $this->shop_id;
            $shop = $this->Shop->find("first",array(
                "conditions" => array(
                    "id" => $this->shop_id
                ),
                "recursive" => -1
            ));
            if($shop) {
                $post_data = $this->request->data("shop");
                $shop_url = $shop['Shop']['url'];
                $shop['Shop']['url']                = isset($post_data["url"]) ? $post_data["url"] : $shop['Shop']['url'] ;
                $shop['Shop']['name']               = isset($post_data["name"]) ? $post_data["name"] : $shop['Shop']['name'];
                $shop['Shop']['is_public']          = isset($post_data["is_public"]) ? $post_data["is_public"] : $shop['Shop']['is_public'];
                $shop['Shop']['macrocategory_id']   = isset($post_data["macrocategory_id"]) ? $post_data["macrocategory_id"] : $shop['Shop']['macrocategory_id'] ;
                $shop['Shop']['phone_number']       = isset($post_data["phone_number"]) ? $post_data["phone_number"] : $shop['Shop']['phone_number'] ;
                $shop['Shop']['email']              = isset($post_data["email"]) ? $post_data["email"] : $shop['Shop']['email'] ;
                $shop['Shop']['latitude']           = isset($post_data["latitude"]) ? $post_data["latitude"] : $shop['Shop']['latitude'] ;
                $shop['Shop']['longitude']          = isset($post_data["longitude"]) ? $post_data["longitude"] : $shop['Shop']['longitude'] ;
                $shop['Shop']['contact_address']    = isset($post_data["contact_address"]) ? $post_data["contact_address"] : $shop['Shop']['contact_address'] ;
                if ($this->Shop->save($shop)) {
                    if(isset($post_data["url"])) {
                        $domain = $this->Domain->find("first", array(
                            "conditions" => array(
                                "domain" => $shop_url . ".negozy.com"
                            )
                        ));
                        if ($domain) {
                            $domain['Domain']['domain'] = $shop['Shop']['url'] . ".negozy.com";
                            $this->Domain->save($domain);
                        }
                    }
                    $data['success'] = true;
                    $data['message'] = __("Salvataggio dei dati del negozio riuscito con successo");
                }
            }
        }
        $this->set(array(
            'data' => $data,
            '_serialize' => array('data')
        ));
    }

    public function updateTaxInformation() {
        $this->_checkShop();
        $data = array();
        $data['success'] = false;
        $data['message'] = __("Si è verificato un problema durante il salvataggio dei dati del negozio");
        if($this->request->is("post")) {
            $this->Shop->id = $this->shop_id;
            $shop = $this->Shop->find("first",array(
                "conditions" => array(
                    "id" => $this->shop_id
                ),
                "recursive" => -1
            ));
            if($shop) {
                $post_data = $this->request->data("shop");
                $shop['Shop']['business_name']  = isset($post_data["business_name"]) ? $post_data["business_name"] : $shop['Shop']['business_name'];
                $shop['Shop']['vat_number']     = isset($post_data["vat_number"]) ? $post_data["vat_number"] : $shop['Shop']['vat_number'];
                $shop['Shop']['address']        = isset($post_data["address"]) ? $post_data["address"] : $shop['Shop']['address'];
                $shop['Shop']['cap']            = isset($post_data["cap"]) ? $post_data["cap"] : $shop['Shop']['cap'];
                $shop['Shop']['city']           = isset($post_data["city"]) ? $post_data["city"] : $shop['Shop']['city'];
                $shop['Shop']['province']           = isset($post_data["province"]) ? $post_data["province"] : $shop['Shop']['province'];
                $shop['Shop']['state']          = isset($post_data["state"]) ? $post_data["state"] : $shop['Shop']['state'];
                $shop['Shop']['currency']       = isset($post_data["currency"]) ? $post_data["currency"] : $shop['Shop']['currency'];
                if ($this->Shop->save($shop)) {
                    $data['success']    = true;
                    $data['message']   = __("Salvataggio dei dati del negozio riuscito con successo");
                }
            }
        }
        $this->set(array(
            'data' => $data,
            '_serialize' => array('data')
        ));
    }

    public function uploadLogo(){
        $this->_checkShop();
        $data = array(
            "success" => false,
            "url"   => false,
            "name"   => false
        ) ;
        if(isset($_FILES['file'])) {
            $types[] = array("height" => 200,"width" => null,"prefix" => "logo_");
            $image_data = $this->Image->uploadImage($_FILES['file'],'shops',$types);
            if ($image_data['success']) {
                $newImage = array();
                $newImage['ShopLogo'] = $image_data['image'];
                $newImage['ShopLogo']['shop_id'] = $this->shop_id;
                $images = $this->ShopLogo->find("all",array(
                   "conditions" => array(
                       "shop_id" => $this->shop_id
                   )
                ));
                foreach($images as $image) {
                    $this->Image->remove_image($image['ShopLogo']['name'].".".$image['ShopLogo']['ext'],"shops/".$image['ShopLogo']['path'],"logo_");
                    $this->ShopLogo->delete($image['ShopLogo']['id']);
                }
                if($this->ShopLogo->save($newImage)) {
                    $data['success'] = true;
                    $data['url'] = "http://cdn.negozy.com/images/shops/".$image_data['image']['path']."logo_".$image_data['image']['name'].".".$image_data['image']['ext'];
                }
            }
        }
        $this->set(array(
            'data' => $data,
            '_serialize' => array('data')
        ));
    }

    public function uploadCover(){
        $this->_checkShop();
        $data = array(
            "success" => false,
            "url"   => false,
            "name"   => false
        ) ;
        if(isset($_FILES['file'])) {
            $types[] = array("height" => 600,"width" => null,"prefix" => "cover_");
            $image_data = $this->Image->uploadImage($_FILES['file'],'shops',$types);
            if ($image_data['success']) {
                $newImage = array();
                $newImage['ShopCover'] = $image_data['image'];
                $newImage['ShopCover']['shop_id'] = $this->shop_id;
                $images = $this->ShopCover->find("all",array(
                    "conditions" => array(
                        "shop_id" => $this->shop_id
                    )
                ));
                foreach($images as $image) {
                    $this->Image->remove_image($image['ShopCover']['name'].".".$image['ShopCover']['ext'],"shops/".$image['ShopCover']['path'],"cover_");
                    $this->ShopCover->delete($image['ShopCover']['id']);
                }
                if($this->ShopCover->save($newImage)) {
                    $data['success'] = true;
                    $data['url'] = "http://cdn.negozy.com/images/shops/".$image_data['image']['path']."cover_".$image_data['image']['name'].".".$image_data['image']['ext'];
                }
            }
        }
        $this->set(array(
            'data' => $data,
            '_serialize' => array('data')
        ));
    }

    private function _defaultShop($shop_id){

    }

}