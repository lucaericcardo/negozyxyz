<?php
class SpsController extends AppController {
    public function beforeFilter(){
        parent::beforeFilter();
        $this->Auth->allow();
    }
    public function sps($sessid = NULL){
        $getsess = (isset($sessid)) ? $sessid: '';
        $this->set("getsess",$getsess);
        $this->set("user_id",$this->Auth->user("id"));
        $this->layout="ajax";
    }

}