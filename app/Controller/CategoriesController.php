<?php
class CategoriesController extends AppController {

    public $components = array('RequestHandler','UtilCategories');

    public function beforeFilter() {
        parent::beforeFilter();
        $this->_checkShop();
    }

    public function index() {
        $macrocategories = $this->UtilCategories->geTreeByShopId($this->shop_id);
        $this->set(compact('macrocategories'));
        $this->set('_serialize', 'macrocategories');

    }

    public function validateMacrocategory() {
        $this->loadModel('Macrocategory');

        $conditions = array(
            'conditions' => array(
                'OR' => array(
                            array(
                                'default' => true,
                                'upper(label)' => strtoupper($this->request->data['name'])
                            ),
                            array(
                                'default' => false,
                                'shop_id' => $this->shop_id,
                                'upper(label)' => strtoupper($this->request->data['name'])
                            )
                )
            ),
            'limit' => 1,
            'recursive' => -1
        );

        $data['success'] = !$this->Macrocategory->find('count', $conditions);
        $this->set(array(
            'data' => $data,
            '_serialize' => array('data')
        ));
    }
    public function validateCategory() {
        $this->loadModel('Category');

        $conditions = array(
            'conditions' => array(
                'OR' => array(
                    array(
                        'default' => true,
                        'upper(label)' => strtoupper($this->request->data['name'])
                    ),
                    array(
                        'default' => false,
                        'shop_id' => $this->shop_id,
                        'upper(label)' => strtoupper($this->request->data['name'])
                    )
                )
            ),
            'limit' => 1,
            'recursive' => -1
        );

        $data['success'] = !$this->Category->find('count', $conditions);
        $this->set(array(
            'data' => $data,
            '_serialize' => array('data')
        ));
    }
    public function validateSubcategory() {
        $this->loadModel('Subcategory');

        $conditions = array(
            'conditions' => array(
                'OR' => array(
                    array(
                        'default' => true,
                        'upper(label)' => strtoupper($this->request->data['name'])
                    ),
                    array(
                        'default' => false,
                        'shop_id' => $this->shop_id,
                        'upper(label)' => strtoupper($this->request->data['name'])
                    )
                )
            ),
            'limit' => 1,
            'recursive' => -1
        );

        $data['success'] = !$this->Subcategory->find('count', $conditions);
        $this->set(array(
            'data' => $data,
            '_serialize' => array('data')
        ));
    }
}