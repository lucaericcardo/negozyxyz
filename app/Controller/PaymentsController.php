<?php
class PaymentsController extends AppController {

    public $components = array('RequestHandler','UserAction');

    public function beforeFilter() {
        parent::beforeFilter();
        $this->_checkShop();
    }

    public function view_list(){
        $this->layout = "ajax";
    }

    public function index() {
        $payments = $this->Payment->find("all",array(
            "conditions" => array(
                "shop_id" => $this->shop_id
            )
        ));
        $return = array();
        foreach($payments as $payment) {
            $return[$payment['Payment']['name']] = array(
                "enabled"   => true,
                "id"        => $payment['Payment']['id'],
                "value"     => $payment['Payment']['value'],
                "price"     => $payment['Payment']['price'],
            );
        }
        $this->set(array(
            'payments' => $return,
            '_serialize' => array('payments')
        ));
    }

    public function save() {
        $data['success'] = false;
        $data['message'] = __("Si è verificato un problema durante il salvataggio dei pagamenti");
        if($this->request->is("post")) {
            $currentPayments = $this->Payment->find("list",array(
                "conditions" => array(
                    "shop_id" => $this->shop_id
                ),
                "fields" => array(
                    "id",
                    "name"
                )
            ));
            $payments = $this->request->data["payments"];
            $data['success'] = true;
            $data['message'] = __("Salvataggio riuscito con successo.");
            foreach ($payments as $type => $payment) {
                $val = (isset($payment['value'])) ? $payment['value'] : null;
                $price = (isset($payment['price'])) ? $payment['price'] : 0;
                if(!isset($payment['id']) && $payment['enabled']) {
                    $newPayment = array(
                        "id" => null,
                        "enabled" => true,
                        "name" => $type,
                        "value" => $val,
                        "price" => $price,
                        "shop_id" => $this->shop_id
                    );
                    $this->Payment->create();
                    if($this->Payment->save($newPayment)) {
                        $data['payments'][$type] = array(
                            "id"    => $this->Payment->id,
                            "enabled" => true,
                            "name"  => $type,
                            "value" => $val,
                            "price" => $price,
                        );
                    }
                } else if (isset($payment['id']) && $payment['enabled']) {
                    if(isset($currentPayments[$payment['id']])) {
                        $updatePayment = array(
                            "id" => $payment['id'],
                            "name" => $type,
                            "enabled" => true,
                            "value" => $payment['value'],
                            "price" => $price
                        );
                        if($this->Payment->save($updatePayment)) {
                            $data['payments'][$type] = array(
                                "id"    => $payment['id'],
                                "name"  => $type,
                                "value" => $price,
                                "price" => $payment['price'],
                            );
                        }
                        unset($currentPayments[$payment['id']]);
                    }
                }
            }
            foreach($currentPayments as $id => $name) {
                $this->Payment->delete($id);
            }
        }
        $this->set(array(
            'data' => $data,
            '_serialize' => array('data')
        ));
    }

}