<?php
class ProductCategoriesController extends AppController {

    public $components = array(
        'RequestHandler',
        'Paginator',
        'Image'
    );

    public $uses = array(
        'ProductCategory',
        'ProductCategoryContent',
        'ShopLanguage',
        'ProductCategoryImage'
    );

    public function beforeFilter() {
        parent::beforeFilter();
        $this->_checkShop();
    }

    public function view_index(){
        $this->response->compress();
        $this->layout = "ajax";
    }

    public function view_add(){
        $this->response->compress();
        $this->layout = "ajax";
    }

    public function view_edit(){
        $this->response->compress();
        $this->layout = "ajax";
    }

    public function view_modal(){
        $this->response->compress();
        $this->layout = "ajax";
    }

    public function get_list($n=10,$page=1,$row="",$orderType="ASC" ,$language_id = null,$parent_id = NULL) {

        $this->response->compress();
        $matrix_languages = array();
        $default_lang = $this->language_id;

        $language_id = (int) $language_id;
        if($language_id) {
            $this->Session->write("language_id", $language_id);
            $default_lang = $language_id;
        }

        $shopLanguages = $this->ShopLanguage->getShopLanguages($this->shop_id);
        foreach($shopLanguages as $k => $v) {
            $matrix_languages[$k] = array();
        }
        if(isset($parent_id)) {
            if(!$this->ProductCategory->find("count",array(
                "conditions" => array(
                    "id"        => $parent_id,
                    "shop_id"   => $this->shop_id
                ),
                "recursive" => -1
            ))){
                $this->response->statusCode(404);
                $data['data']['message'] = "Page not found";
                $this->set(array(
                    'data' => $data,
                    '_serialize' => array('data')
                ));
                $this->response->send();
                $this->_stop();
            }
        }
        $this->Paginator->settings = array(
            "recursive" => -1,
            "conditions" => array(
                "ProductCategory.shop_id"   => $this->shop_id,
                "ProductCategory.parent_id" => $parent_id
            ),
            "contain" => array(
                "ProductCategoryContent" => array(
                    "conditions" => array(
                        "ProductCategoryContent.language_id" => array_keys($shopLanguages)
                    ),
                    "fields" => array(
                        "ProductCategoryContent.id",
                        "ProductCategoryContent.language_id",
                        "ProductCategoryContent.title",
                        "ProductCategoryContent.timestamp_created"
                    )
                ),
                'ProductCategoryImage' => array(
                    "fields" => array(
                        "ProductCategoryImage.path",
                        "ProductCategoryImage.name",
                        "ProductCategoryImage.ext"
                    )
                )
            ),
            "fields" => array(
                "ProductCategory.id",
                "ProductCategory.product_count",
                "ProductCategory.children_count",
                "ProductCategory.order",
            ),
            "order" => "ProductCategory.order",
            "limit" => $n,
            "page" => $page
        );

        $count = $this->ProductCategory->find("count",array(
            "conditions" => array(
                "ProductCategory.shop_id"   => $this->shop_id,
                "ProductCategory.parent_id" => $parent_id
            ),
            "recursive" => -1
        ));
        $tmp_categories = $this->Paginator->paginate('ProductCategory');
        $distinct_categories    = array();
        $default_no_categories  = array();
        foreach($tmp_categories as $category) {
            $category_id  = $category['ProductCategory']['id'];
            $distinct_categories[$category_id] = array();
            foreach($category['ProductCategoryContent'] as $category_content) {
                if(isset($matrix_languages[$category_content['language_id']])) {
                    $matrix_languages[$category_content['language_id']][$category_content['product_category_id']] = $category_content['id'];
                } else {
                    $matrix_languages[$category_content['language_id']] = array(
                        $category_content['product_category_id'] => $category_content['id']
                    );
                }
                if($default_lang == $category_content['language_id']) {
                    $distinct_categories[$category_id] = array(
                        'title'         => $category_content['title'],
                        'content_id'    => $category_content['id'],
                        'created'       => $category_content['timestamp_created'],
                        'products'      => $category['ProductCategory']['product_count'],
                        'children'      => $category['ProductCategory']['children_count'],
                        'order'         => $category['ProductCategory']['order'],
                        'image'         => $this->Image->build_path("collections","image_",$category['ProductCategoryImage'])
                    );
                }
                if($this->default_language_id == $category_content['language_id']) {
                    $default_no_categories[$category_id] = array(
                        'title'         => $category_content['title'],
                        'content_id'    => $category_content['id'],
                        'created'       => $category_content['timestamp_created'],
                        'products'      => $category['ProductCategory']['product_count'],
                        'children'      => $category['ProductCategory']['children_count'],
                        'order'         => $category['ProductCategory']['order'],
                        'image'         => $this->Image->build_path("collections","image_",$category['ProductCategoryImage'])
                    );
                }
            }
        }
        $categories = array();
        foreach($distinct_categories as $category_id => $default_category) {
            if(!count($default_category)) {
                $tmpRow =array(
                    "id"            =>  $category_id,
                    "title"         =>  (isset($default_no_categories[$category_id])  ? $default_no_categories[$category_id]['title'] : '---'),
                    "products"      =>  (isset($default_no_categories[$category_id])) ? $default_no_categories[$category_id]['products'] : 0,
                    "children"      =>  (isset($default_no_categories[$category_id])) ? $default_no_categories[$category_id]['children'] : 0,
                    "order"         =>  (isset($default_no_categories[$category_id])) ? $default_no_categories[$category_id]['order'] : 0,
                    'content_id'    =>  null,
                    'created'       =>  null,
                    'contents'      =>  array(),
                    'image'         => (isset($default_no_categories[$category_id])) ? $default_no_categories[$category_id]['image'] : '',
                );
            } else {
                $tmpRow =array(
                    "id"            =>  $category_id,
                    "title"         =>  $default_category['title'],
                    "products"      =>  $default_category['products'],
                    "children"      =>  $default_category['children'],
                    "order"         =>  $default_category['order'],
                    'content_id'    =>  $default_category['content_id'],
                    'created'       =>  $default_category['created'],
                    'contents'      =>  array(),
                    'image'         =>  $default_category['image'],
                );
            }
            foreach($matrix_languages as $lang_id => $content) {
                $tmpRow['contents'][] = array(
                    'language_id'   => $lang_id,
                    'content_id'    => isset($matrix_languages[$lang_id][$category_id]) ? $matrix_languages[$lang_id][$category_id] : null
                );

            }
            $categories[] = $tmpRow;
        }

        $this->set(array(
            'categories'    => $categories,
            'matrix'        => $matrix_languages,
            'count'         => $count,
            'breadcrumb'    => $this->ProductCategory->get_breadcrumb_tree($this->shop_id,$language_id,$parent_id),
            '_serialize'    => array('categories','count','matrix','breadcrumb')
        ));
    }

    public function add(){
        $data = array();
        $data['success'] = false;
        $data['message'] = __("Si è verificato un problema durante la creazione della collezione.");
        if($this->request->is("post")) {
            if(isset($this->request->data['product_category']['parent_id'])) {
                if(!$this->ProductCategory->find("count",array(
                    "conditions" => array(
                        "id"        => $this->request->data['product_category']['parent_id'],
                        "shop_id"   => $this->shop_id
                    ),
                    "recursive" => -1
                ))){
                    $this->response->statusCode(404);
                    $data['data']['message'] = "Page not found";
                    $this->set(array(
                        'data' => $data,
                        '_serialize' => array('data')
                    ));
                    $this->response->send();
                    $this->_stop();
                }
            }
            $check_lang = $this->ShopLanguage->find("count",array(
                "conditions" => array(
                    "language_id"   => $this->request->data['language_id'],
                    "shop_id"       => $this->shop_id
                ),
                "recursive" => -1
            ));
            if(!$check_lang) {
                $this->response->statusCode(404);
                $data['data']['message'] = "Page not found";
                $this->set(array(
                    'data' => $data,
                    '_serialize' => array('data')
                ));
                $this->response->send();
                $this->_stop();
            }
            $order = ($this->request->data['product_category']['order']) ? $this->request->data['product_category']['order'] : $this->calcOrderCollection();
            $product_category_data = array(
                "ProductCategory" => array(
                    'shop_id'   => $this->shop_id,
                    'parent_id' => $this->request->data['product_category']['parent_id'],
                    'order'     => $order
                )
            );
            $this->ProductCategory->create();
            if($this->ProductCategory->save($product_category_data)) {
                $data['category_id']    = (int)$this->ProductCategory->id;
                $data['success']        = $this->_saveProductCategoryContents($this->ProductCategory->id,$this->request->data['language_id'],null);
                $data['message']        = __("Collezione creata con successo!");
            }
        }
        $this->set(array(
            'data' => $data,
            '_serialize' => array('data')
        ));
    }

    public function add_modal(){
        $data = array();
        $data['success']    = false;
        $data['message']    = __("Si è verificato un problema durante la creazione della collezione.");
        $data['tree']       = array();
        if($this->request->is("post")) {
            if(isset($this->request->data['product_category']['parent_id'])) {
                if(!$this->ProductCategory->find("count",array(
                    "conditions" => array(
                        "id"        => $this->request->data['product_category']['parent_id'],
                        "shop_id"   => $this->shop_id
                    ),
                    "recursive" => -1
                ))){
                    $this->response->statusCode(404);
                    $data['data']['message'] = "Page not found";
                    $this->set(array(
                        'data' => $data,
                        '_serialize' => array('data')
                    ));
                    $this->response->send();
                    $this->_stop();
                }
            }
            $product_category_data = array(
                "ProductCategory" => array(
                    'shop_id'   => $this->shop_id,
                    'parent_id' => $this->request->data['product_category']['parent_id']
                )
            );
            $this->ProductCategory->create();
            if($this->ProductCategory->save($product_category_data)) {
                $language_id = $this->Shop->getDefaultLanguage($this->shop_id);
                $data['collection_id']  = (int)$this->ProductCategory->id;
                $data['success']        = $this->_saveProductCategoryContents($this->ProductCategory->id,$language_id,null);
                $data['message']        = __("Collezione creata con successo!");
                $data['tree']           = $this->ProductCategory->get_graphical_tree($this->shop_id,$language_id);
            }
        }
        $this->set(array(
            'data' => $data,
            '_serialize' => array('data')
        ));
    }


    public function update() {
        $data = array();
        $data['success'] = false;
        $data['message'] = __("Si è verificato un problema durante il salvataggio della categoria.");
        if($this->request->is("post")) {
            $product_category = $this->ProductCategory->find("first",array(
                "conditions" => array(
                    "id"        => $this->request->data['product_category']['product_category_id'],
                    "shop_id"   => $this->shop_id
                ),
                "fields" => array(
                    "id"
                ),
                "recursive" => -1
            ));
            if($product_category) {
                $this->ProductCategory->id = $this->request->data['product_category']['product_category_id'];
                $this->ProductCategory->saveField("order",$this->request->data['product_category']['order']);
                $content = $this->ProductCategoryContent->find("first",array(
                    "conditions" => array(
                        "id"                    => $this->request->data['product_category']['content_id'],
                        "language_id"           => $this->request->data['product_category']['language_id'],
                        "product_category_id"   => $this->request->data['product_category']['product_category_id']
                    ),
                    "fields" => array(
                        "id",
                        "uri"
                    ),
                    "recursive" => -1
                ));
                $updated = $this->_saveProductCategoryContents($this->request->data['product_category']['product_category_id'],$this->request->data['product_category']['language_id'],$content);
                if($updated) {
                    $data['success'] =  true;
                    $data['message'] =  __("Collezione modificata con successo!");
                }
            }
        }
        $this->set(array(
            'data' => $data,
            '_serialize' => array('data')
        ));
    }

    public function delete() {
        $data = array();
        $data['success'] = false;
        $data['message'] = __("Si è verificato un problema durante la rimozione della collezione.");
        if($this->request->is("post")) {
            $ck_owner = $this->ProductCategory->find("count",array(
                "conditions" => array(
                    "shop_id"   => $this->shop_id,
                    "id"        => $this->request->data['product_category_id']
                ),
                "recursive" => -1
            ));
            if($ck_owner) {
                if ($data['success'] = (boolean)$this->ProductCategory->delete($this->request->data['product_category_id'])) {
                    $data['message'] = __("La collezione è stata cancallata correttamente");
                }
            }
        }
        $this->set(array(
            'data' => $data,
            '_serialize' => array('data')
        ));
    }

    public function delete_all(){
        $data['success'] = false;
        $data['message'] = __("Si è verificato un problema durante la rimozione delle collezioni");
        if($this->request->is("post")) {
            $product_category_ids  = $this->request->data["product_categories"];
            if($product_category_ids ) {
                $deleted = $this->ProductCategory->deleteAll(
                    array(
                        "ProductCategory.id"       => $product_category_ids,
                        "ProductCategory.shop_id"  => $this->shop_id
                    )
                );
                if ($deleted) {
                    $data['success'] = true;
                    $data['message'] = __("Le collezioni sono state rimosse con successo");
                }
            }
        }
        $this->set(array(
            'data' => $data,
            '_serialize' => array('data')
        ));
    }

    public function loadImage($product_category_id= NULL){
        $data = array(
            "success" => false,
            "url"   => false,
            "name"   => false
        ) ;
        if(isset($product_category_id) && !empty($product_category_id) && isset($_FILES['file'])) {
            $product_category = $this->ProductCategory->find("first",array(
                "recursive" => -1,
                "conditions" => array(
                    "ProductCategory.id"        => $product_category_id,
                    "ProductCategory.shop_id"   => $this->shop_id
                ),
                "contain" => array(
                    "ProductCategoryImage" => array(
                        "fields" => array(
                            "ProductCategoryImage.name",
                            "ProductCategoryImage.ext",
                            "ProductCategoryImage.path",
                            "ProductCategoryImage.id"
                        )
                    )
                ),
                "fields" => array(
                    "id"
                )
            ));
            if($product_category) {
                if(isset($product_category['ProductCategoryImage'])) {
                    $this->ProductCategoryImage->delete($product_category['ProductCategoryImage']['id']);
                    $this->Image->remove_image($product_category['ProductCategoryImage']['name'].'.'.$product_category['ProductCategoryImage']['ext'],'collections/'.$product_category['ProductCategoryImage']['path'], 'image_');
                }
                $types = array();
                $types[] = array("width" => 800, "height" => null, "prefix" => "image_");
                $image_data = $this->Image->uploadImage($_FILES['file'],'collections',$types);
                if ($image_data['success']) {
                    $newImage = array();
                    $newImage['ProductCategoryImage'] = $image_data['image'];
                    $newImage['ProductCategoryImage']['product_category_id'] = $product_category_id;
                    if($this->ProductCategoryImage->save($newImage)) {
                        $this->ProductCategory->id = $product_category_id;
                        $this->ProductCategory->saveField('image_id',$this->ProductCategoryImage->id);
                        $data['success']    = true;
                        $data['url']        = $this->Image->build_path("collections","image_",$image_data['image']);
                        $data['name']       = $image_data['image']['name'];
                    }
                }
            }
        }
        $this->set(array(
            'data' => $data,
            '_serialize' => array('data')
        ));
    }

    public function load(){
        $this->response->compress();
        if($this->request->is('post') && isset($this->request->data['product_category_id']) && isset($this->request->data['language_id'])) {
            $product_category = $this->ProductCategory->find("first",array(
                "conditions" => array(
                    "ProductCategory.id"        => $this->request->data['product_category_id'],
                    "ProductCategory.shop_id"   => $this->shop_id
                ),
                "contain" => array(
                    "ProductCategoryImage" => array(
                        "fields" => array(
                            "ProductCategoryImage.name",
                            "ProductCategoryImage.ext",
                            "ProductCategoryImage.path",
                            "ProductCategoryImage.id"
                        )
                    )
                ),
                "recursive" => -1
            ));
            if(!$product_category) {
                $this->response->statusCode(404);
                $data['data']['message'] = "Page not found";
                $this->set(array(
                    'data' => $data,
                    '_serialize' => array('data')
                ));
                $this->response->send();
                $this->_stop();
            }

            $check_lang = $this->ShopLanguage->find("count",array(
                "conditions" => array(
                    "language_id"   => $this->request->data['language_id'],
                    "shop_id"       => $this->shop_id
                ),
                "recursive" => -1
            ));
            if(!$check_lang) {
                $this->response->statusCode(404);
                $data['data']['message'] = "Page not found";
                $this->set(array(
                    'data' => $data,
                    '_serialize' => array('data')
                ));
                $this->response->send();
                $this->_stop();
            }
            $content = $this->ProductCategoryContent->find("first",array(
                "conditions" => array(
                    "product_category_id"   => $this->request->data['product_category_id'],
                    "language_id"           => $this->request->data['language_id'],
                ),
                "recursive" => -1
            ));
            $data = array(
                'title'                         => '',
                'description'                   => '',
                'uri'                           => '',
                'meta_keywords'                 => '',
                'meta_description'              => '',
                'order'                         => '',
                'product_category_id'           => $this->request->data['product_category_id'],
                'content_id'                    => null,
                'language_id'                   => $this->request->data['language_id']
            );
            $image = array(
                "url"   => null,
                "name"  => null
            );
            if(isset($product_category['ProductCategoryImage']['id'])) {
                $image = array(
                    "url"   => $this->Image->build_path("collections","image_",$product_category['ProductCategoryImage']),
                    "name"  => $product_category['ProductCategoryImage']['name']
                );
            }
            if($content) {
                $data['title']                       = $content['ProductCategoryContent']['title'];
                $data['description']                 = $content['ProductCategoryContent']['description'];
                $data['uri']                         = $content['ProductCategoryContent']['uri'];
                $data['meta_keywords']               = json_decode($content['ProductCategoryContent']['meta_keywords'],true);
                $data['meta_description']            = $content['ProductCategoryContent']['meta_description'];
                $data['content_id']                  = $content['ProductCategoryContent']['id'];
                $data['order']                       = $product_category['ProductCategory']['order'];
            }
            $this->set(array(
                'image'         => $image,
                'content'       => $data,
                '_serialize'    => array('content','image')
            ));
        }
    }

    public function tree_list(){
        $categories = $this->CategoryPage->get_graphical_tree($this->shop_id,$this->Shop->getDefaultLanguage($this->shop_id));
        $this->set(array(
            'categories'    => $categories,
            '_serialize'    => array('categories')
        ));
    }


    private function _saveProductCategoryContents($product_category_id = null, $language_id=NULL , $content = NULL) {
        if(!$content || ($content['ProductCategoryContent']['uri'] != $this->request->data['product_category']['uri'])) {
            $ck_page_category_URL = $this->_ckUniqueURL($this->request->data['product_category']['uri']);
            if ($ck_page_category_URL) {
                $this->request->data['product_category']['uri'] = $this->request->data['product_category']['uri'] . "-" . time();
            }
        }
        $data = array(
            "ProductCategoryContent" => array(
                'title'                 => (isset($this->request->data['product_category']['title'])) ? $this->request->data['product_category']['title'] : '',
                'description'           => (isset($this->request->data['product_category']['description'])) ? $this->request->data['product_category']['description'] : '',
                'meta_keywords'         => (isset($this->request->data['product_category']['meta_keywords'])) ? json_encode($this->request->data['product_category']['meta_keywords']) : null,
                'meta_description'      => (isset($this->request->data['product_category']['meta_description'])) ? $this->request->data['product_category']['meta_description'] : '',
                'uri'                   => (isset($this->request->data['product_category']['uri'])) ? $this->request->data['product_category']['uri'] : '',
                'product_category_id'   => $product_category_id,
                'language_id'           => $language_id,
            )
        );
        if($content && isset($content['ProductCategoryContent']['id'])) {
            $this->ProductCategoryContent->id = $content['ProductCategoryContent']['id'];
        } else {
            $this->ProductCategoryContent->create();
        }
        if($this->ProductCategoryContent->save($data)) {
            return true;
        }
        return false;
    }

    private function _ckUniqueURL($url = NULL){
        $count_url = false;
        if(isset($url) && !empty($url)) {
            $count_url = $this->ProductCategoryContent->find("count",array(
                "conditions" => array(
                    "ProductCategoryContent.uri" => $url
                ),
                "joins" => array(
                    array(
                        'table' => 'product_categories',
                        'alias' => 'ProductCategory',
                        'type' => 'RIGHT',
                        'conditions' => array(
                            'ProductCategory.id = ProductCategoryContent.product_category_id',
                            'ProductCategory.shop_id' => $this->shop_id
                        )
                    )
                ),
                "recursive" => -1
            ));
        }
        return $count_url;
    }

    /**
     * @return null
     * Ordine della categoria automatico
     */
    private function calcOrderCollection() {
        $return = NULL;
        $options = array(
            'conditions' => array(
                'shop_id' => $this->shop_id
            ),
            'fields' => array(
                'COALESCE(MAX("order"),-1)+1 as order'
            ),
            'recursive' => -1
        );

        if($order = $this->ProductCategory->find('first', $options)) {
            $return = $order[0]['order'];
        }
        return $return;
    }

}