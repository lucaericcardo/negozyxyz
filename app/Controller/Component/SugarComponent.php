<?php
class SugarComponent extends Component  {

    const SUGAR_PRODUCTS_MODULE_NAME                        = 'Prodotti';
    const SUGAR_LEADS_MODULE_NAME                           = 'Leads';
    const SUGAR_CONTACTS_MODULE_NAME                        = 'Contacts';
    const SUGAR_ACCOUNTS_MODULE_NAME                        = 'Accounts';
    const SUGAR_APPLICATION_MODULE_NAME                     = 'lhs_Applicazioni';
    const SUGAR_DEVELOPERS_MODULE_NAME                      = 'lhs_Developers';
    const SUGAR_RELATIONSHIP_DEVELOPERS_APPLICATIONS_NAME   = 'lhs_developers_lhs_applicazioni';
    const SUGAR_RELATIONSHIP_PRODUCTS_ACCOUNTS_NAME         = 'lhs_prodotti_accounts';

    public function getIndustriesAccount() {

        if(!Cache::read('SugarData.IndustryAccount')) {
            $industriesAccount_eng = $this->_get_module_fields(self::SUGAR_ACCOUNTS_MODULE_NAME, array('industry'))->response['module_fields']['industry']['options'];
            $industriesAccount_ita = array();
            foreach($industriesAccount_eng as $value => $data) {
                $industriesAccount_ita[] = array(
                    'name' => __($data['name']),
                    'value' => $data['value'],

                );
            }
            Cache::write('SugarData.IndustryAccount', $industriesAccount_ita);
        }
        return Cache::read('SugarData.IndustryAccount');
    }



    public function converteLead($lead_id, $data) {
        // aggiunge l'Azienda a cui il Lead deve essere convertito
        $account_id = $this->addAccount($data);

        // aggiunge il Contact a cui il Lead deve essere convertito
        $contact_id = $this->addContact($data);

        if($this->existLead($lead_id)) {
            for($i = 0; $i<4; $i++) {
                $this->updateLeadStatusToConverted($lead_id, $contact_id, $account_id);
                if(isset($this->response['id']) && !empty($this->response['id'])) {
                    return array('success'=> TRUE, 'data' => array('account_id' => $account_id, 'contact_id' => $contact_id));
                }
                usleep(500000); // sleep 0.5 seconds
            }
        }
        return array('success'=> FALSE, 'data' => array('account_id' => $account_id, 'contact_id' => $contact_id));
    }

    public function existLead($id) {
        return $this->existEntity(self::SUGAR_LEADS_MODULE_NAME, $id);
    }

    public function existAccount($id) {
        return $this->existEntity(self::SUGAR_ACCOUNTS_MODULE_NAME, $id);
    }

    public function existDeveloper($id) {
        return $this->existEntity(self::SUGAR_DEVELOPERS_MODULE_NAME, $id);
    }

    public function addAccount($data) {
        $params = array(
            array(
                "name" => "name",
                "value" => $data['name']
            ),
            array(
                "name" => "vat_number",
                "value" => $data['vat_number']
            ),
            array(
                "name" => "ownership",
                "value" => $data['last_name'].' '.$data['first_name']
            ),
            array(
                "name" => "website",
                "value" => $data['url']
            ),
            array(
                "name" => "industry",
                "value" => ""//$data['category']
            ),

            // billing data
            array(
                "name" => "billing_address_city",
                "value" => $data['city']
            ),
            array(
                "name" => "billing_address_country",
                "value" => $data['state']
            ),
            array(
                "name" => "billing_address_postalcode",
                "value" => $data['cap']
            ),
            array(
                "name" => "billing_address_state",
                "value" => $data['province']
            ),
            array(
                "name" => "billing_address_street",
                "value" => $data['address']
            ),
            // shipping data
            array(
                "name" => "shipping_address_city",
                "value" => $data['city']
            ),
            array(
                "name" => "shippingg_address_country",
                "value" => $data['state']
            ),
            array(
                "name" => "shipping_address_postalcode",
                "value" => $data['cap']
            ),
            array(
                "name" => "shipping_address_state",
                "value" => $data['province']
            ),
            array(
                "name" => "shipping_address_street",
                "value" => $data['address']
            )
        );
        return $this->_set_entry(self::SUGAR_ACCOUNTS_MODULE_NAME, $params)->response['id'];
    }

    /*
    public function addProduct($product_data, $account_data) {
        if(!$this->existAccount($account_data['sugar_id'])) {
            $account_data['sugar_id'] = $this->addAccount($account_data);
        }
        $params = array(
            array(
                "name" => "name",
                "value" => $product_data['name']
            ),
            array(
                "name" => "commercial_name",
                "value" => $product_data['commercial_name']
            ),
            array(
                "name" => "description",
                "value" => $product_data['description']
            ),
            array(
                "name" => "currency",
                "value" => $product_data['currency']
            ),
            array(
                "name" => "price",
                "value" => $product_data['price']
            )
        );
        $product_id = $this->_set_entry(self::SUGAR_PRODUCTS_MODULE_NAME, $params)->response['id'];
        $this->_set_relationship(
            self::SUGAR_DEVELOPERS_MODULE_NAME,
            $account_data['sugar_id'],
            self::SUGAR_RELATIONSHIP_PRODUCTS_ACCOUNTS_NAME,
            array($product_id)
        );
        if($this->response['created'] == 1 && $this->response['failed'] == 0) {
            return $product_id;
        }
        return false;
    }
    */

    public function addProducts($account_id, $many) {
        $name_value_list = array(
            array(
                'name' => 'product_count_c',
                'value' => $many
            ),
            array(
                'name' => 'id',
                'value' => $account_id
            ),
        );
        return $this->_set_entry(SELF::SUGAR_ACCOUNTS_MODULE_NAME,$name_value_list);
    }

    public function addContact($data) {
        $params = array(
            array(
                "name" => "first_name",
                "value" => $data['first_name']
            ),
            array(
                "name" => "last_name",
                "value" => $data['last_name']
            ),
            array(
                "name" => "email1",
                "value" => $data['email']
            ),
            array(
                "name" => "primary_address_city",
                "value" => $data['city']
            ),
            array(
                "name" => "primary_address_country",
                "value" => $data['state']
            ),
            array(
                "name" => "primary_address_postalcode",
                "value" => $data['cap']
            ),
            array(
                "name" => "primary_address_state",
                "value" => $data['province']
            ),
            array(
                "name" => "primary_address_street",
                "value" => $data['address']
            ),
            array(
                "name" => "description",
                "value" => ""//$data['category']
            ),
        );
        return $this->_set_entry(self::SUGAR_CONTACTS_MODULE_NAME, $params)->response['id'];
    }

    public function addLead($data) {
        $params = array(
            array(
                "name" => "first_name",
                "value" => $data['first_name']
            ),
            array(
                "name" => "last_name",
                "value" => $data['last_name']
            ),
            array(
                "name" => "email1",
                "value" => $data['email']
            ),
            array(
                "name" => "lead_source",
                "value" => $data['source']
            )
        );
        return $this->_set_entry(self::SUGAR_LEADS_MODULE_NAME, $params)->response['id'];
    }

    public function addDeveloper($data) {
        $params = array(
            array(
                "name" => "first_name",
                "value" => $data['first_name']
            ),
            array(
                "name" => "last_name",
                "value" => $data['last_name']
            ),
            array(
                "name" => "email",
                "value" => $data['email']
            )
        );
        return $this->_set_entry(self::SUGAR_DEVELOPERS_MODULE_NAME, $params)->response['id'];
    }

    /*
     * Funzione per aggiungere una nuova Applicazione al Developer
     * @params
     * $application_data dati dell'applicazione
     * $developer_data dati del developer
     * @return true operazione riuscita, false altrimenti
     *
     * @example
     * $application_data = array(
     *      "name"          => "app_name",
     *      "url"           => "app_url",
     *      "description"   => "description",
     * );
     * $developer_data = array(
     *      "sugar_id"      => "devel_sugar_id",
     *      "first_name"    => "devel_first_name",
     *      "last_name"     => "devel_last_name",
     *      "email"         => "devel_email",
     * );
     * $this->Sugar->addApplicationToDeveloper($application_data, $developer_data);
     */
    public function addApplicationToDeveloper($application_data, $developer_data) {

        if(!$this->existDeveloper($developer_data['sugar_id'])) {
            $developer_data['sugar_id'] = $this->addDeveloper($developer_data);
        }
        $params = array(
            array(
                "name" => "name",
                "value" => $application_data['name']
            ),
            array(
                "name" => "app_url",
                "value" => $application_data['url']
            ),
            array(
                "name" => "description",
                "value" => $application_data['description']
            )
        );
        $application_id = $this->_set_entry(self::SUGAR_APPLICATION_MODULE_NAME, $params)->response['id'];
        $this->_set_relationship(
                self::SUGAR_DEVELOPERS_MODULE_NAME,
                $developer_data['sugar_id'],
                self::SUGAR_RELATIONSHIP_DEVELOPERS_APPLICATIONS_NAME,
                array($application_id)
        );
        if($this->response['created'] == 1 && $this->response['failed'] == 0) {
            return $application_id;
        }
        return false;
    }

    public function updateLeadStatusToConverted($lead_id, $contact_id, $account_id) {
        $params = array(
            array(
                "name" => "id",
                "value" => $lead_id
            ),
            array(
                "name" => "converted",
                "value" => "1"
            ),
            array(
                "name" => "status",
                "value" => "Converted"
            ),
            array(
                "name" => "contact_id",
                "value" => $contact_id
            ),
            array(
                "name" => "account_id",
                "value" => $account_id
            ),
        );
        return $this->_set_entry(self::SUGAR_LEADS_MODULE_NAME, $params);
    }

    private function existEntity($entity_name, $id) {
        $this->_get_entry($entity_name, $id, array('id'));

        $t = $this->response['entry_list'];
        $t = reset($t);
        foreach($t['name_value_list'] as $k=>$el) {
            if($el['name'] == 'deleted' && $el['value'] == 1) {
                return false;
            }
        }
        return true;
    }

    private function _set_relationship($module_name, $module_id, $link_field_name, $related_ids = array(), $name_value_list = array(), $delete = 0) {
        $this->_login();
        $this->method = 'set_relationship';
        $this->parameters = array(
            //session id
            'session' => $this->session_id,

            //The name of the module.
            'module_name' => $module_name,

            //The ID of the specified module bean.
            'module_id' => $module_id,

            //The relationship name of the linked field from which to relate records.
            'link_field_name' => $link_field_name,

            //The list of record ids to relate
            'related_ids' => $related_ids,

            //Sets the value for relationship based fields
            'name_value_list' => $name_value_list,

            //Whether or not to delete the relationship. 0:create, 1:delete
            'delete'=> $delete,
        );
        return $this->_call();
    }

    private function _get_module_fields($module_name, $fields = array()) {
        $this->_login();
        $this->method = 'get_module_fields';
        $this->parameters = array(
            "session"       => $this->session_id,
            "module_name"   => $module_name,
            'fields'        => $fields,
        );
        return $this->_call();
    }

    private function _set_entry($module_name, $name_value_list = array()) {
        $this->_login();
        $this->method = 'set_entry';
        $this->parameters = array(
            "session"           => $this->session_id,
            "module_name"       => $module_name,
            "name_value_list"   => $name_value_list,
        );
        return $this->_call();
    }

    private function _get_entry($module_name, $id, $select_field = array()) {
        $this->_login();
        $this->method = 'get_entry';
        $this->parameters = array(
            "session"           => $this->session_id,
            "module_name"       => $module_name,
            'id'                => $id,
            'select_fields'     => $select_field
        );
        return $this->_call();
    }

    private function _login($name_value_list = array('id')) {
        $this->config = Configure::read('sugar_config');
        $this->method = 'login';
        $this->parameters = array(
            "user_auth" => array(
                "user_name" => $this->config['username'],
                "password"  => md5($this->config['password']),
                "version"   => $this->config['version']
            ),
            "application_name"  => $this->config['application_name'],
            "name_value_list"   => $name_value_list,
        );
        $this->_call();
        if(!isset($this->response['id'])) {
            die('Integrazione Crm non disponibile. Verificare le configurazioni di connessione.');
        }
        return $this->session_id = $this->response['id'];
    }

    private function _call(){
        App::uses('HttpSocket', 'Network/Http');
        $HttpSocket = new HttpSocket();
        // refresh del session id se è scaduta la sessione di sugarcrm
        $this->response = null;
        while(!$this->response) {
            $this->response = $HttpSocket->post(
                $this->config['url'],
                array(
                    "method"        => $this->method,
                    "input_type"    => "JSON",
                    "response_type" => "JSON",
                    "rest_data"     => json_encode($this->parameters)
                ),
                array('version' => '1.0')
            );
            if(!$this->response) {
                CakeSession::delete('Sugarcrm.session_id');
                $this->_login();
            }
        }
        $this->response = json_decode($this->response->body, true);
        return $this;
    }

}