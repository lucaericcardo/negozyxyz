<?php
class NegozyAppsComponent extends Component {

    public function getApplicationInstalled($shop_id = NULL){
        $this->ShopAppInstalled = ClassRegistry::init('ShopAppInstalled');
        return $this->ShopAppInstalled->find("all",
            array(
                "conditions" => array(
                    "ShopAppInstalled.shop_id" => $shop_id,
                    "ShopAppInstalled.deleted" => false
                ),
                "contain" => array(
                    "Application" => array(
                        "fields" => array(
                            "request_uri"
                        )
                    )
                ),
                "fields" => "ShopAppInstalled.app_id",
                "recursive" => -1
            )
        );

    }

    public function render_button_product_list($shop_id = NULL){
        $array_return = array();
        $apps = $this->getApplicationInstalled($shop_id);
        foreach($apps as $app) {
            $pluginName = Inflector::camelize($app['Application']['request_uri']);
            $className = $pluginName."Component";
            if(file_exists(APP. DS . "Plugin" . DS . $pluginName . DS . "Controller" . DS . "Component". DS . $className.".php")) {
                App::uses($className, $pluginName . '.Controller/Component');
                $pluginComponent = new $className(new ComponentCollection());
                if(method_exists($pluginComponent,'render_button_product_list')){
                    $array_return[] = $pluginComponent->render_button_product_list();
                }
            }
        }
        return $array_return;
    }

}