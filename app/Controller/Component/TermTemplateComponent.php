<?php
class TermTemplateComponent extends Component  {

    public function get($template_name) {
        $this->TermTemplate = ClassRegistry::init('TermTemplate');

        $options = array(
            'conditions' => array(
                'template_name' => $template_name
            ),
            'fields' => array('template_text')
        );
        $terms = $this->TermTemplate->find('first', $options);
        if(count($terms)) {
            return $terms['TermTemplate']['template_text'];
        }
        return false;
    }
}