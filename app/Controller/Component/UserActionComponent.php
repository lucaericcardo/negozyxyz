<?php
App::uses('CakeEventListener', 'Event');
App::uses('CakeEvent', 'Event');

class UserActionComponent extends Component implements CakeEventListener{

    public $components = array('Session','Auth');

    public function startup(Controller $controller) {
        parent::startup($controller);
        $controller->getEventManager()->attach($this);

        $product = ClassRegistry::init('Product');
        $product->getEventManager()->attach($this);

        $collection = ClassRegistry::init('Collection');
        $collection->getEventManager()->attach($this);

        $payment = ClassRegistry::init('Payment');
        $payment->getEventManager()->attach($this);

        $shipping = ClassRegistry::init('Shipping');
        $shipping->getEventManager()->attach($this);

        $taxes = ClassRegistry::init('ShopTax');
        $taxes->getEventManager()->attach($this);

        $shop = ClassRegistry::init('Shop');
        $shop->getEventManager()->attach($this);

        $shopTerm = ClassRegistry::init('ShopTerm');
        $shopTerm->getEventManager()->attach($this);

    }

    public function implementedEvents() {
        return array(
            'Model.Product.update'          => 'addSuccessAction',
            'Model.Product.add'             => 'addSuccessAction',
            'Model.Product.delete'          => 'addSuccessAction',
            'Model.Collection.update'       => 'addSuccessAction',
            'Model.Collection.add'          => 'addSuccessAction',
            'Model.Collection.delete'       => 'addSuccessAction',
            'Model.Payment.update'          => 'addSuccessAction',
            'Model.Payment.add'             => 'addSuccessAction',
            'Model.Payment.delete'          => 'addSuccessAction',
            'Model.Shipping.update'         => 'addSuccessAction',
            'Model.Shipping.add'            => 'addSuccessAction',
            'Model.Shipping.delete'         => 'addSuccessAction',
            'Model.ShopTax.update'          => 'addSuccessAction',
            'Model.ShopTax.add'             => 'addSuccessAction',
            'Model.ShopTax.delete'          => 'addSuccessAction',
            'Model.Shop.update'             => 'addSuccessAction',
            'Model.ShopTerm.update'         => 'addSuccessAction',
            'Model.ShopTerm.add'            => 'addSuccessAction',
            'Model.User.logout'             => 'addSuccessAction',
            'Model.User.login'              => 'addSuccessAction',
            'Model.User.loginError'         => 'addErrorAction',
            'Model.User.recoveryPassword'   => 'addSuccessAction',
            'Model.User.changePassword'     => 'addSuccessAction'
        );
    }

    public function addErrorAction(CakeEvent $event){
        $userActionModel = ClassRegistry::init('UserAction');
        $newUserAction = array(
            "id" => NULL,
            "user_id" => $event->data['user_id'],
            "error" => 1,
            "success" => 0,
            "action" => $event->data['type']
        );
        $userActionModel->Save($newUserAction);
    }
    public function addSuccessAction(CakeEvent $event){
        $userActionModel = ClassRegistry::init('UserAction');
        $newUserAction = array(
            "id" => NULL,
            "user_id" => (isset($event->data['user_id'])) ? $event->data['user_id'] : $this->Auth->user("id"),
            "error" => 0,
            "success" => 1,
            "action" => $event->data['type']
        );
        $userActionModel->Save($newUserAction);
    }

}