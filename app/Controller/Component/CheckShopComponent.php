<?php

class CheckShopComponent extends Component {
    private $shop_id;
    private $Shop;
    private $Collection;
    private $data_shop;
    private $collection_shop;
    private static $doAction = array(
        "cover" => "Dovresti aggiungere l'immagine di copertina del tuo negozio.",
        "logo"  => "Dovresti aggiungere il logo del tuo negozio.",
        "shipping" => "Dovresti aggiungere almeno un metodo di spedizione per il tuo negozio.",
        "payment" => "Dovresti aggiungere almeno un metodo di pagamento per il tuo negozio.",
        "terms_conditions" => "Dovresti aggiungere i termini e condizioni per il tuo negozio.",
        "privacy" => "Dovresti aggiungere la privacy per il tuo negozio.",
        "cancellation_policy" => "Dovresti aggiungere condizioni di restituzione per il tuo negozio.",
        "phone" => "Dovresti aggiungere il numero di telefono del tuo negozio.",
        "email" => "Dovresti impostare l'indirizzo email del tuo negozio.",
        "about_us" => "Dovresti aggiungere l'indirizzo del tuo negozio.",
        "contact_address" => "Dovresti impostare l'indirizzo del tuo negozio.",
        "coordinate" => "Dovresti completare la sezione <b>contatti</b> con il corretto indirizzo.",
        "products" => "Dovresti aggiungere almeno un prodotto.",
        "collection" => "Ti consigliamo di aggiungere delle collezioni per raggruppare i tui prodotti."

    );


    public function verify($shop_id) {
        $this->shop_id          = $shop_id;
        $this->Shop             = ClassRegistry::init('Shop');
        $this->ProductCategory  = ClassRegistry::init('ProductCategory');
        $this->Product          = ClassRegistry::init('Product');
        $this->data_shop        = $this->getDataShop();
        $data = array(
            'status' => true,
            'count'  => 0,
            'actions' => array(
                'cover'                 => (isset($this->data_shop['ShopCover']) && count($this->data_shop['ShopCover'])) ? true : false,
                'logo'                  => (isset($this->data_shop['ShopLogo']) && count($this->data_shop['ShopLogo'])) ? true : false,
                'shipping'              => (isset($this->data_shop['ShippingGroup']) && count($this->data_shop['ShippingGroup'])) ? true : false,
                'payment'               => (isset($this->data_shop['Payments']) && count($this->data_shop['Payments'])) ? true : false,
                'phone'                 => (!empty($this->data_shop['Shop']['phone_number'])) ? true : false,
                'email'                 => (!empty($this->data_shop['Shop']['email'])) ? true : false,
                'coordinate'            => (!empty($this->data_shop['Shop']['latitude']) && !empty($this->data_shop['Shop']['longitude'])) ? true : false,
                'contact_address'       => (!empty($this->data_shop['Shop']['contact_address'])) ? true : false,
                'products'              => ($this->getProductShop()) ? true : false,
                'collection'            => ($this->getCollectionShop()) ? true : false
            )
        );

        foreach($data['actions'] as $key => $action) {
            if(!$action) {
                $data['count']++;
            } else {
                unset($data['actions'][$key]);
            }
        }
        return $data;

    }

    public function getSuggest($action) {
        if(isset(self::$doAction[$action])) {
            return self::$doAction[$action];
        }
    }

    private function getDataShop() {
        $options = array(
            'conditions' => array(
                'Shop.id' => $this->shop_id
            ),
            'contain' => array(
                'ShopCover'     => array('fields' => array('id')),
                'ShopLogo'      => array('fields' => array('id')),
                'ShippingGroup' => array('fields' => array('id')),
                'Payments'      => array('fields' => array('id'))
            ),
            "recursive" => -1
        );
        return $this->Shop->find('first', $options);
    }

    private function getProductShop(){
        $options = array(
            'conditions' => array(
                'Product.shop_id' => $this->shop_id,
                'Product.deleted' => false,
            ),
            'recursive' => -1
        );
        return $this->Product->find('count', $options);
    }

    private function getCollectionShop() {
        $options = array(
            'conditions' => array(
                'shop_id' => $this->shop_id
            ),
            'recursive' => -1
        );
        return $this->ProductCategory->find('count', $options);
    }

}