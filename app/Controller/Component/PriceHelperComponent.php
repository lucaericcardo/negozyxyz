<?php
App::uses('CakeNumber', 'Utility');
class PriceHelperComponent extends Component {

    public function format_price($price = 0,$vat = NULL){
        if(isset($vat['id'])) {
            $vat_value = $vat['tax'];
            if(!$vat['product_tax']) {
                $price_vat = ($price * $vat_value) / 100;
                $price += $price_vat;
            }
        }
        return CakeNumber::precision($price,2);
    }

    public function vat_price($price = 0,$vat = NULL){
        $return = 0;
        if(isset($vat['id'])) {
            $vat_value = $vat['tax'];
            if($vat['product_tax']) {
                $return = $price - ($price / (1 + ($vat_value / 100)));
            } else {
                $return =  ($price * $vat_value) / 100;
            }
        }
        return CakeNumber::precision($return,2);
    }

    public function no_vat_price($price = 0,$vat = NULL){
        $return = $price;
        if(isset($vat['id'])) {
            $vat_value = $vat['tax'];
            if($vat['product_tax']) {
                $return =  ($price / (1 +  ($vat_value / 100)));
            }
        }
        return CakeNumber::precision($return,2);
    }

}