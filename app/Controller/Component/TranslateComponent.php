<?php

class TranslateComponent extends Component {

    public $documents = array();
    public $words = array();
    public $sourceLang = 'italian';
    public $targetLang = 'english';
    public $countWords = 0;
    public $negozy_fee = 1.2;
    public $quote = 0;
    public $status = 'quote';
    public $response_request_quote = null;
    public $objectId=null;
    public $shop_id;
    public $jobType='P';

    private static $cid = 'lhsgroup';
    private static $password = 'Lighthouse2011';

    public function init($srcLang, $trgLang, $documents, $words) {
        $this->targetLang = $trgLang;
        $this->sourceLang = $srcLang;

        foreach ($documents as $d) {
            $this->pushDocument($d['text'], $d['source']);
        }

        foreach ($words as $w) {
            $this->pushWord($w['text'], $w['source']);
        }
    }

    public function pushDocument($text, $source = array()) {
        $array = array(
            'text' => $text,
            'source' => $source,
            'type' => 'document',
            'object_id' => uniqid()
        );
        $this->documents[] = $array;
    }

    public function pushWord($text, $source = array()) {
        $array = array(
            'text' => $text,
            'source' => $source,
            'type' => 'word',
            'object_id' => uniqid()
        );
        $this->words[] = $array;
    }
    public function generateText() {
        $txt = array();
        foreach($this->documents as $doc) {
            $txt[] = strip_tags($doc['text']);
        }
        foreach($this->words as $word) {
            $txt[] = strip_tags($word['text']);
        }
        return implode(' ', $txt);
    }

    public function countWords() {
        $txt = $this->generateText();
        return str_word_count($txt);
    }
    public function countWordApi() {
        App::uses('HttpSocket', 'Network/Http');
        $post = array(
            'action' => 'getWordcount',
            'text' => $this->generateText(),
            'src' => $this->sourceLang,
            'trg' => $this->targetLang
        );
        $HttpSocket = new HttpSocket();
        $response = $HttpSocket->post('http://www.translated.net/it/entrypoint.php', $post);

        $response = json_decode($response->body, true);
        return $response['netTotal'];
    }
    public function requestQuote() {
        App::uses('HttpSocket', 'Network/Http');
        $post = $this->generateUrlAPI();
        $post = 'f=quote&s='.$this->sourceLang.'&jt='.$this->jobType.'&df=html&t='.$this->targetLang.'&w='.$this->countWordApi().'&cid='.self::$cid.'&p='.self::$password;
        $HttpSocket = new HttpSocket();
        $response = $HttpSocket->post('http://www.translated.net/hts/', $post);
        $response = json_encode($response->body, true);
        $response = explode('\n',$response);
        $this->response_request_quote = false;
        if($response[1] == 'OK') {
            $price = (double) $response[4];
            $this->response_request_quote = array(
                'status' => $response[1],
                'delivery_date' => $response[2],
                'word_count' => $response[3],
                'price' => $price,
                'quote_id' => (int) $response[5],
                'negozy_price' => $this->negozy_fee *$price
            );
        }
        return $this->response_request_quote;
    }
    private function generateUrlAPI() {
        $data = array(
            'f'=>'quote', // richiesta di una quotazione
            's'=>$this->sourceLang,
            't'=>$this->targetLang,
            'jt' => $this->jobType,
            'df' => 'html',
            'v' => $this->countWordApi(),
            'cid' => self::$cid,
            'p' => self::$password,
            'instructions' => 'instructions are in the image at the top of the document, these instructions can also be removed in the document delivery',
            'endpoint' => 'http://api.negozy.com/translated/delivery/'.$this->objectId
        );
        return http_build_query($data);
    }
    public function save() {
        $dataToStore = array_diff_key(get_object_vars($this), get_class_vars(get_parent_class($this)));
        $this->Translated = ClassRegistry::init('Translated');
        $this->Translated->create();
        if($this->Translated->save($dataToStore)) {
            $this->objectId = $this->Translated->id;
            return $this->Translated->id;
        }
        return false;
    }
    public function loadFromDB($shop_id=null,$objectId=null) {
        if($objectId && $shop_id) {
            $this->Translated = ClassRegistry::init('Translated');
            $options = array('condition'=>array('shop_id'=>$shop_id,'_id'=>$objectId));
            if($response = $this->Translated->find('first',$options)) {
                foreach($response['Translated'] as $k=>$v) {
                    $this->$k = $v;
                }
            }
        }
    }
    public function generateDocumentTranslated() {
        $html ='
<link rel="stylesheet" type="text/css" href="http://cdn.negozy.com/images/generic/translated.css" />
    <p align="center"><img src="http://cdn.negozy.com/images/generic/instructions-translated1.jpg" /></p>
';
        $closeTag="";
        if(count($this->words)) {
            $html .= '<div data-start-object="StartSignleWords" data-end-object="'.$closeTag.'" class="dontremove"></div>';
            foreach($this->words as $k=>$data) {
               $html .='<p class="wordDistinct" data-negozyOb-id="'.$data['object_id'].'">'.strip_tags($data['text']).'</p>';
            }
            $closeTag ='StopSignleWords';
        }
        if(count($this->documents)) {
            foreach($this->documents as $k=>$data) {
                if ($data['text']) {
                    $html .= '<div data-start-object="' . $data['object_id'] . '" data-end-object="' . $closeTag . '" class="dontremove"></div>';
                    $html .= $data['text'];
                    $closeTag = $data['object_id'];
                }

            }
        }
        $html .='<div data-start-object="" data-end-object="'.$closeTag.'" class="dontremove"></div>';
        return $html;
    }

}
