<?php
class NotifiesComponent extends Component
{

    public function sendShopNotify($receiver_id,$sender_id,$message,$type,$order_id){
        $this->ShopNotify = ClassRegistry::init('ShopNotify');
        $data = array();
        $data['receiver_id']    = (int)$receiver_id;
        $data['sender_id']      = (int)$sender_id;
        $data['message']        = $message;
        $data['visualized']     = false;
        $data['readed']         = false;
        $data['type']           = $type;
        $data['order_id']       = (int)$order_id;
        $this->ShopNotify->create();
        if($this->ShopNotify->save($data)) {
            return $this->ShopNotify->id;
        }
        return false;
    }

    public function getShopNotifies($receiver_id) {
        $this->ShopNotify = ClassRegistry::init('ShopNotify');
        $options = array(
            "conditions" => array(
                "receiver_id" => $receiver_id,
            ),
            "order" => array("ShopNotify.created" => -1)
        );
        $notifies = Set::extract('/ShopNotify/.',$this->ShopNotify->find('all', $options));
        return $notifies;
    }

    public function viewShopNotifies($receiver_id){
        $this->ShopNotify = ClassRegistry::init('ShopNotify');
        $this->User = ClassRegistry::init('User');
        $this->ShopNotify->updateAll(
            array("visualized" => true),
            array("receiver_id" => $receiver_id,"visualized" => false)
        );
        $this->User->id = $receiver_id;
        $this->User->saveField("shop_notify_count",0);
    }

    public function sendUserNotify($receiver_id,$sender_id,$message,$type,$order_id){
        $this->UserNotify = ClassRegistry::init('UserNotify');
        $data = array();
        $data['receiver_id']    = (int)$receiver_id;
        $data['sender_id']      = (int)$sender_id;
        $data['message']        = $message;
        $data['visualized']     = false;
        $data['readed']         = false;
        $data['type']           = $type;
        $data['order_id']       = (int)$order_id;
        $this->UserNotify->create();
        if($this->UserNotify->save($data)) {
            return $this->UserNotify->id;
        }
        return false;
    }

    public function getUserNotifies($receiver_id) {
        $this->UserNotify = ClassRegistry::init('UserNotify');
        $options = array(
            "conditions" => array(
                "receiver_id" => $receiver_id,
            ),
            "order" => array("UserNotify.created" => -1)
        );
        $notifies = Set::extract('/UserNotify/.',$this->UserNotify->find('all', $options));
        return $notifies;
    }

    public function viewUserNotifies($receiver_id){
        $this->UserNotify = ClassRegistry::init('UserNotify');
        $this->User = ClassRegistry::init('User');
        $this->UserNotify->updateAll(
            array("visualized" => true),
            array("receiver_id" => $receiver_id,"visualized" => false)
        );
        $this->User->id = $receiver_id;
        $this->User->saveField("user_notify_count",0);
    }

}