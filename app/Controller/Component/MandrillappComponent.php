<?php
App::import('Vendor', 'ElephantIOEngineInterface', array('file' => 'ElephantIO'  . DS . 'EngineInterface.php'));
App::import('Vendor', 'ElephantIOClient', array('file' => 'ElephantIO'  . DS . 'Client.php'));
App::import('Vendor', 'ElephantIOPayload', array('file' => 'ElephantIO'  . DS . 'AbstractPayload.php'));
App::import('Vendor', 'ElephantIOAbstractSocketIO', array('file' => 'ElephantIO' . DS . 'Engine' . DS . 'AbstractSocketIO.php'));
App::import('Vendor', 'ElephantIOEngine', array('file' => 'ElephantIO' . DS . 'Engine' . DS . 'SocketIO' . DS . 'Version1X.php'));
App::import('Vendor', 'ElephantIOSession', array('file' => 'ElephantIO' . DS . 'Engine' . DS . 'SocketIO' . DS . 'Session.php'));
App::import('Vendor', 'ElephantIOEncoder', array('file' => 'ElephantIO' . DS . 'Payload' . DS . 'Encoder.php'));
App::import('Vendor', 'ElephantIOServerConnectionFailureException', array('file' => 'ElephantIO'  . DS . 'Exception' . DS. 'ServerConnectionFailureException.php'));
use ElephantIO\Client,
    ElephantIO\Engine\SocketIO\Version1X;

class MandrillappComponent extends Component {

    public function send_error_user_log($full_name = NULL,$email = NULL,$data = NULL) {
        $message = array(
            "html" => "",
            "text" => "",
            "subject" => "",
            "from_email" => "",
            "from_name" => "",
            "to" => array(
                array(
                    "email" => $email,
                    "name"  => $full_name,
                    "type"  => array()
                )
            ),
            "headers" => array(
                "Reply-To" => "noreply@negozy.com"
            ),
            "merge_language" => "mailchimp",

            "global_merge_vars"=>  array(
                array(
                    "name" => "TEXT",
                    "content" => "text"
                )
            ),
            "merge_vars" => array(
                array(
                    "rcpt" => $email,
                    "vars" => array(
                        array(
                            "name" => "TEXT",
                            "content" => $data
                        )
                    )
                )
            )
        );

        $template_name = "negozy_exception";
        $template_content = array(
            array(
                "name" => "TEXT",
                "content" => $data
            )
        );

        $client = new Client(new Version1X('http://nodejshost:3001'));
        $client->initialize();
        $client->emit('send_mail', ["message" => $message,"async" => true,"template_name" => $template_name,"template_content" => $template_content]);
        $client->close();
    }

    public function send_confirm_account($full_name = NULL,$email = NULL,$link = NULL) {
        $message = array(
            "html" => "",
            "text" => "",
            "subject" => "",
            "from_email" => "",
            "from_name" => "",
            "to" => array(
                    array(
                        "email" => $email,
                        "name"  => $full_name,
                        "type"  => array()
                    )
            ),
            "headers" => array(
                "Reply-To" => "noreply@negozy.com"
            ) ,
            "tags" => array(
                "conferma registrazione"
            ),
            "merge_language" => "mailchimp",

            "global_merge_vars"=>  array(
                array(
                    "name" => "FULLNAME",
                    "content" => "name"
                ),
                array(
                    "name" => "LINK",
                    "content" => "http://it.negozy.com"
                )
            ),
            "merge_vars" => array(
                array(
                    "rcpt" => $email,
                    "vars" => array(
                        array(
                            "name" => "FULLNAME",
                            "content" => $full_name
                        ),
                        array(
                            "name" => "LINK",
                            "content" => $link
                        )
                    )
                )
            )
        );

        $template_name = "confirm_mail";
        $template_content = array(
            array(
                "name" => "FULLNAME",
                "content" => $full_name
            ),
            array(
                "name" => "LINK",
                "content" => $link
            )
        );

        $client = new Client(new Version1X('http://nodejshost:3001'));
        $client->initialize();
        $client->emit('send_mail', ["message" => $message,"async" => true,"template_name" => $template_name,"template_content" => $template_content]);
        $client->close();
    }


    public function send_enable_account($full_name = NULL,$email = NULL) {
        $message = array(
            "html" => "",
            "text" => "",
            "subject" => "",
            "from_email" => "",
            "from_name" => "",
            "to" => array(
                array(
                    "email" => $email,
                    "name"  => $full_name,
                    "type"  => array()
                )
            ),
            "headers" => array(
                "Reply-To" => "noreply@negozy.com"
            ) ,
            "merge_language" => "mailchimp",
            "global_merge_vars"=>  array(
                array(
                    "name" => "FULLNAME",
                    "content" => "name"
                )
            ),
            "merge_vars" => array(
                array(
                    "rcpt" => $email,
                    "vars" => array(
                        array(
                            "name" => "FULLNAME",
                            "content" => $full_name
                        )
                    )
                )
            )
        );

        $template_name = "Benvenuto su Negozy";
        $template_content = array(
            array(
                "name" => "FULLNAME",
                "content" => $full_name
            ),
        );

        $client = new Client(new Version1X('http://nodejshost:3001'));
        $client->initialize();
        $client->emit('send_mail', ["message" => $message,"async" => true,"template_name" => $template_name,"template_content" => $template_content]);
        $client->close();
    }

    public function send_enable_account_and_order($full_name = NULL,$email = NULL,$order_uid=NULL,$address=NULL,$shop_name=NULL,$link = NULL,$shop_logo=NULL,$table= NULL,$shipping = NULL,$payments = NULL) {
        $message = array(
            "html" => "",
            "text" => "",
            "subject" => "",
            "from_email" => "",
            "from_name" => "",
            "to" => array(
                array(
                    "email" => $email,
                    "name"  => $full_name,
                    "type"  => array()
                )
            ),
            "headers" => array(
                "Reply-To" => "noreply@negozy.com"
            ) ,
            "merge_language" => "mailchimp",
            "global_merge_vars"=>  array(
                array(
                    "name" => "FULLNAME",
                    "content" => "name"
                ),
                array(
                    "name" => "IDORDER",
                    "content" => "order_id"
                ),
                array(
                    "name" => "MERCHANT",
                    "content" => "shop_name"
                ),
                array(
                    "name" => "DELIVERY",
                    "content" => "address"
                ),
                array(
                    "name" => "CONFIRMORDER",
                    "content" => "link"
                ),
                array(
                    "name" => "DATE",
                    "content" => "data"
                ),
                array(
                    "name" => "LOGOMERCHANT",
                    "content" => NULL
                ),
                array(
                    "name" => "ORDERDETAILS",
                    "content" => NULL
                ),
                array(
                    "name" => "PAYMENT",
                    "content" => NULL
                ),
                array(
                    "name" => "SHIPPING",
                    "content" => NULL
                )
            ),
            "merge_vars" => array(
                array(
                    "rcpt" => $email,
                    "vars" => array(
                        array(
                            "name" => "FULLNAME",
                            "content" => $full_name
                        ),
                        array(
                            "name" => "IDORDER",
                            "content" => $order_uid
                        ),
                        array(
                            "name" => "MERCHANT",
                            "content" => $shop_name
                        ),
                        array(
                            "name" => "DELIVERY",
                            "content" => $address
                        ),
                        array(
                            "name" => "CONFIRMORDER",
                            "content" => $link
                        ),
                        array(
                            "name" => "DATE",
                            "content" => date("d-m-Y")
                        ),
                        array(
                            "name" => "LOGOMERCHANT",
                            "content" => $shop_logo
                        ),
                        array(
                            "name" => "ORDERDETAILS",
                            "content" => $table
                        ),
                        array(
                            "name" => "PAYMENT",
                            "content" => $payments
                        ),
                        array(
                            "name" => "SHIPPING",
                            "content" => $shipping
                        )
                    )
                )
            )
        );

        $template_name = "first_order_unsigned";
        $template_content = array(
            array(
                array(
                    "name" => "FULLNAME",
                    "content" => $full_name
                ),
                array(
                    "name" => "IDORDER",
                    "content" => $order_uid
                ),
                array(
                    "name" => "MERCHANT",
                    "content" => $shop_name
                ),
                array(
                    "name" => "DELIVERY",
                    "content" => $address
                ),
                array(
                    "name" => "CONFIRMORDER",
                    "content" => $link
                ),
                array(
                    "name" => "DATE",
                    "content" => date("d-m-Y")
                ),
                array(
                    "name" => "LOGOMERCHANT",
                    "content" => $shop_logo
                ),
                array(
                    "name" => "ORDERDETAILS",
                    "content" => $table
                ),
                array(
                    "name" => "PAYMENT",
                    "content" => $payments
                ),
                array(
                    "name" => "SHIPPING",
                    "content" => $shipping
                )
            ),
        );

        $client = new Client(new Version1X('http://nodejshost:3001'));
        $client->initialize();
        $client->emit('send_mail', ["message" => $message,"async" => true,"template_name" => $template_name,"template_content" => $template_content]);
        $client->close();
    }

    public function send_order($full_name = NULL,$email = NULL,$order_uid=NULL,$address=NULL,$shop_name=NULL,$shop_logo=NULL,$table,$shipping = NULL,$payments = NULL) {
        $message = array(
            "html" => "",
            "text" => "",
            "subject" => "",
            "from_email" => "",
            "from_name" => "",
            "to" => array(
                array(
                    "email" => $email,
                    "name"  => $full_name,
                    "type"  => array()
                )
            ),
            "headers" => array(
                "Reply-To" => "noreply@negozy.com"
            ) ,
            "merge_language" => "mailchimp",
            "global_merge_vars"=>  array(
                array(
                    "name" => "FULLNAME",
                    "content" => "name"
                ),
                array(
                    "name" => "IDORDER",
                    "content" => "order_id"
                ),
                array(
                    "name" => "MERCHANT",
                    "content" => "shop_name"
                ),
                array(
                    "name" => "DELIVERY",
                    "content" => "address"
                ),
                array(
                    "name" => "DATE",
                    "content" => "data"
                ),
                array(
                    "name" => "LOGOMERCHANT",
                    "content" => NULL
                ),
                array(
                    "name" => "ORDERDETAILS",
                    "content" => NULL
                ),
                array(
                    "name" => "PAYMENT",
                    "content" => NULL
                ),
                array(
                    "name" => "SHIPPING",
                    "content" => NULL
                )
            ),
            "merge_vars" => array(
                array(
                    "rcpt" => $email,
                    "vars" => array(
                        array(
                            "name" => "FULLNAME",
                            "content" => $full_name
                        ),
                        array(
                            "name" => "IDORDER",
                            "content" => $order_uid
                        ),
                        array(
                            "name" => "MERCHANT",
                            "content" => $shop_name
                        ),
                        array(
                            "name" => "DELIVERY",
                            "content" => $address
                        ),
                        array(
                            "name" => "DATE",
                            "content" => date("d-m-Y")
                        ),
                        array(
                            "name" => "LOGOMERCHANT",
                            "content" => $shop_logo
                        ),
                        array(
                            "name" => "ORDERDETAILS",
                            "content" => $table
                        ),
                        array(
                            "name" => "PAYMENT",
                            "content" => $payments
                        ),
                        array(
                            "name" => "SHIPPING",
                            "content" => $shipping
                        )
                    )
                )
            )
        );

        $template_name = "purchase_mail";
        $template_content = array(
            array(
                array(
                    "name" => "FULLNAME",
                    "content" => $full_name
                ),
                array(
                    "name" => "IDORDER",
                    "content" => $order_uid
                ),
                array(
                    "name" => "MERCHANT",
                    "content" => $shop_name
                ),
                array(
                    "name" => "DELIVERY",
                    "content" => $address
                ),
                array(
                    "name" => "DATE",
                    "content" => date("d-m-Y")
                ),
                array(
                    "name" => "LOGOMERCHANT",
                    "content" => $shop_logo
                ),
                array(
                    "name" => "ORDERDETAILS",
                    "content" => $table
                ),
                array(
                    "name" => "PAYMENT",
                    "content" => $payments
                ),
                array(
                    "name" => "SHIPPING",
                    "content" => $shipping
                )
            ),
        );

        $client = new Client(new Version1X('http://nodejshost:3001'));
        $client->initialize();
        $client->emit('send_mail', ["message" => $message,"async" => true,"template_name" => $template_name,"template_content" => $template_content]);
        $client->close();
    }

    public function send_order_to_merchant($full_name = NULL,$email = NULL,$address=NULL,$shop_name=NULL,$shop_logo=NULL,$table,$shipping = NULL,$payments = NULL) {
        $message = array(
            "html" => "",
            "text" => "",
            "subject" => "",
            "from_email" => "",
            "from_name" => "",
            "to" => array(
                array(
                    "email" => $email,
                    "name"  => $full_name,
                    "type"  => array()
                )
            ),
            "headers" => array(
                "Reply-To" => "noreply@negozy.com"
            ) ,
            "merge_language" => "mailchimp",
            "global_merge_vars"=>  array(
                array(
                    "name" => "FULLNAME",
                    "content" => "name"
                ),
                array(
                    "name" => "MERCHANT",
                    "content" => "shop_name"
                ),
                array(
                    "name" => "DELIVERY",
                    "content" => "address"
                ),
                array(
                    "name" => "DATE",
                    "content" => "data"
                ),
                array(
                    "name" => "LOGOMERCHANT",
                    "content" => NULL
                ),
                array(
                    "name" => "ORDERDETAILS",
                    "content" => NULL
                ),
                array(
                    "name" => "PAYMENT",
                    "content" => NULL
                ),
                array(
                    "name" => "SHIPPING",
                    "content" => NULL
                )
            ),
            "merge_vars" => array(
                array(
                    "rcpt" => $email,
                    "vars" => array(
                        array(
                            "name" => "FULLNAME",
                            "content" => $full_name
                        ),
                        array(
                            "name" => "MERCHANT",
                            "content" => $shop_name
                        ),
                        array(
                            "name" => "DELIVERY",
                            "content" => $address
                        ),
                        array(
                            "name" => "DATE",
                            "content" => date("d-m-Y")
                        ),
                        array(
                            "name" => "LOGOMERCHANT",
                            "content" => $shop_logo
                        ),
                        array(
                            "name" => "ORDERDETAILS",
                            "content" => $table
                        ),
                        array(
                            "name" => "PAYMENT",
                            "content" => $payments
                        ),
                        array(
                            "name" => "SHIPPING",
                            "content" => $shipping
                        )
                    )
                )
            )
        );

        $template_name = "order_recive";
        $template_content = array(
            array(
                array(
                    "name" => "FULLNAME",
                    "content" => $full_name
                ),
                array(
                    "name" => "MERCHANT",
                    "content" => $shop_name
                ),
                array(
                    "name" => "DELIVERY",
                    "content" => $address
                ),
                array(
                    "name" => "DATE",
                    "content" => date("d-m-Y")
                ),
                array(
                    "name" => "LOGOMERCHANT",
                    "content" => $shop_logo
                ),
                array(
                    "name" => "ORDERDETAILS",
                    "content" => $table
                ),
                array(
                    "name" => "PAYMENT",
                    "content" => $payments
                ),
                array(
                    "name" => "SHIPPING",
                    "content" => $shipping
                )
            ),
        );

        $client = new Client(new Version1X('http://nodejshost:3001'));
        $client->initialize();
        $client->emit('send_mail', ["message" => $message,"async" => true,"template_name" => $template_name,"template_content" => $template_content]);
        $client->close();
    }

    public function send_request_availability($full_name = NULL,$buyer_fullname = NULL,$table= NULL,$email) {
        $message = array(
            "html" => "",
            "text" => "",
            "subject" => "",
            "from_email" => "",
            "from_name" => "",
            "to" => array(
                array(
                    "email" => $email,
                    "name"  => $full_name,
                    "type"  => array()
                )
            ),
            "headers" => array(
                "Reply-To" => "noreply@negozy.com"
            ) ,
            "merge_language" => "mailchimp",
            "global_merge_vars"=>  array(
                array(
                    "name" => "FULLNAME",
                    "content" => NULL
                ),
                array(
                    "name" => "BUYER_FULLNAME",
                    "content" => NULL
                ),
                array(
                    "name" => "ORDERDETAILS",
                    "content" => NULL
                ),
            ),
            "merge_vars" => array(
                array(
                    "rcpt" => $email,
                    "vars" => array(
                        array(
                            "name" => "FULLNAME",
                            "content" => $full_name
                        ),
                        array(
                            "name" => "BUYER_FULLNAME",
                            "content" => $buyer_fullname
                        ),
                        array(
                            "name" => "ORDERDETAILS",
                            "content" => $table
                        ),
                    )
                )
            )
        );

        $template_name = "request_availability";
        $template_content = array(
            array(
                array(
                    "name" => "FULLNAME",
                    "content" => $full_name
                ),
                array(
                    "name" => "BUYER_FULLNAME",
                    "content" => $buyer_fullname
                ),
                array(
                    "name" => "ORDERDETAILS",
                    "content" => $table
                ),
            ),
        );

        $client = new Client(new Version1X('http://nodejshost:3001'));
        $client->initialize();
        $client->emit('send_mail', ["message" => $message,"async" => true,"template_name" => $template_name,"template_content" => $template_content]);
        $client->close();
    }

    public function send_request_available($full_name = NULL,$shop_name = NULL,$table= NULL,$email,$link,$subject) {
        $message = array(
            "html" => "",
            "text" => "",
            "subject" => $subject,
            "from_email" => "noreply@negozy.com",
            "from_name" => "Negozy",
            "to" => array(
                array(
                    "email" => $email,
                    "name"  => $full_name,
                    "type"  => array()
                )
            ),
            "headers" => array(
                "Reply-To" => "noreply@negozy.com"
            ) ,
            "merge_language" => "mailchimp",
            "global_merge_vars"=>  array(
                array(
                    "name" => "FULLNAME",
                    "content" => NULL
                ),
                array(
                    "name" => "SHOP_NAME",
                    "content" => NULL
                ),
                array(
                    "name" => "ORDERDETAILS",
                    "content" => NULL
                ),
                array(
                    "name" => "BUY_LINK",
                    "content" => NULL
                ),
            ),
            "merge_vars" => array(
                array(
                    "rcpt" => $email,
                    "vars" => array(
                        array(
                            "name" => "FULLNAME",
                            "content" => $full_name
                        ),
                        array(
                            "name" => "SHOP_NAME",
                            "content" => $shop_name
                        ),
                        array(
                            "name" => "ORDERDETAILS",
                            "content" => $table
                        ),
                        array(
                            "name" => "BUY_LINK",
                            "content" => $link
                        )
                    )
                )
            )
        );

        $template_name = "request_available";
        $template_content = array(
            array(
                array(
                    "name" => "FULLNAME",
                    "content" => $full_name
                ),
                array(
                    "name" => "SHOP_NAME",
                    "content" => $shop_name
                ),
                array(
                    "name" => "ORDERDETAILS",
                    "content" => $table
                ),
                array(
                    "name" => "BUY_LINK",
                    "content" => $link
                )
            ),
        );

        $client = new Client(new Version1X('http://nodejshost:3001'));
        $client->initialize();
        $client->emit('send_mail', ["message" => $message,"async" => true,"template_name" => $template_name,"template_content" => $template_content]);
        $client->close();
    }

    public function send_request_unavailable($full_name = NULL,$shop_name = NULL,$table= NULL,$email,$subject) {
        $message = array(
            "html" => "",
            "text" => "",
            "subject" => $subject,
            "from_email" => "noreply@negozy.com",
            "from_name" => "Negozy",
            "to" => array(
                array(
                    "email" => $email,
                    "name"  => $full_name,
                    "type"  => array()
                )
            ),
            "headers" => array(
                "Reply-To" => "noreply@negozy.com"
            ) ,
            "merge_language" => "mailchimp",
            "global_merge_vars"=>  array(
                array(
                    "name" => "FULLNAME",
                    "content" => NULL
                ),
                array(
                    "name" => "SHOP_NAME",
                    "content" => NULL
                ),
                array(
                    "name" => "ORDERDETAILS",
                    "content" => NULL
                )
            ),
            "merge_vars" => array(
                array(
                    "rcpt" => $email,
                    "vars" => array(
                        array(
                            "name" => "FULLNAME",
                            "content" => $full_name
                        ),
                        array(
                            "name" => "SHOP_NAME",
                            "content" => $shop_name
                        ),
                        array(
                            "name" => "ORDERDETAILS",
                            "content" => $table
                        )
                    )
                )
            )
        );

        $template_name = "request_unavailable";
        $template_content = array(
            array(
                array(
                    "name" => "FULLNAME",
                    "content" => $full_name
                ),
                array(
                    "name" => "SHOP_NAME",
                    "content" => $shop_name
                ),
                array(
                    "name" => "ORDERDETAILS",
                    "content" => $table
                )
            ),
        );

        $client = new Client(new Version1X('http://nodejshost:3001'));
        $client->initialize();
        $client->emit('send_mail', ["message" => $message,"async" => true,"template_name" => $template_name,"template_content" => $template_content]);
        $client->close();
    }

    public function send_recovery_password($full_name = NULL,$email = NULL,$link_password = NULL) {
        $message = array(
            "html" => "",
            "text" => "",
            "subject" => "",
            "from_email" => "",
            "from_name" => "",
            "to" => array(
                array(
                    "email" => $email,
                    "name"  => $full_name,
                    "type"  => array()
                )
            ),
            "headers" => array(
                "Reply-To" => "noreply@negozy.com"
            ) ,
            "tags" => array(
                "recupero password"
            ),
            "merge_language" => "mailchimp",

            "global_merge_vars"=>  array(
                array(
                    "name" => "FULLNAME",
                    "content" => "name"
                ),
                array(
                    "name" => "LINKPASSWORD",
                    "content" => "http://it.negozy.com"
                )
            ),
            "merge_vars" => array(
                array(
                    "rcpt" => $email,
                    "vars" => array(
                        array(
                            "name" => "FULLNAME",
                            "content" => $full_name
                        ),
                        array(
                            "name" => "LINKPASSWORD",
                            "content" => $link_password
                        )
                    )
                )
            )
        );

        $template_name = "forgot_password_mail";
        $template_content = array(
            array(
                "name" => "FULLNAME",
                "content" => $full_name
            ),
            array(
                "name" => "LINKPASSWORD",
                "content" => $link_password
            )
        );

        $client = new Client(new Version1X('http://nodejshost:3001'));
        $client->initialize();
        $client->emit('send_mail', ["message" => $message,"async" => true,"template_name" => $template_name,"template_content" => $template_content]);
        $client->close();
    }

    public function wake_up_shop($full_name = NULL,$email = NULL,$list = NULL,$shop_link=NULL,$shop_name = NULL) {
        $message = array(
            "html" => "",
            "text" => "",
            "subject" => "",
            "from_email" => "",
            "from_name" => "",
            "to" => array(
                array(
                    "email" => $email,
                    "name"  => $full_name,
                    "type"  => array()
                )
            ),
            "headers" => array(
                "Reply-To" => "noreply@negozy.com"
            ) ,
            "merge_language" => "mailchimp",

            "global_merge_vars"=>  array(
                array(
                    "name" => "FULLNAME",
                    "content" => "name"
                ),
                array(
                    "name" => "LINK",
                    "content" => "http://it.negozy.com"
                ),
                array(
                    "name" => "SHOPLINK",
                    "content" => ""
                ),
                array(
                    "name" => "SHOPNAME",
                    "content" => ""
                )
            ),
            "merge_vars" => array(
                array(
                    "rcpt" => $email,
                    "vars" => array(
                        array(
                            "name" => "FULLNAME",
                            "content" => $full_name
                        ),
                        array(
                            "name" => "LISTSUGGESTION",
                            "content" => $list
                        ),
                        array(
                            "name" => "SHOPLINK",
                            "content" => $shop_link
                        ),
                        array(
                            "name" => "SHOPNAME",
                            "content" => $shop_name
                        )
                    )
                )
            )
        );

        $template_name = "wake_up_shop";
        $template_content = array(
            array(
                "name" => "FULLNAME",
                "content" => $full_name
            ),
            array(
                "name" => "LISTSUGGESTION",
                "content" => $list
            ),
            array(
                "name" => "SHOPLINK",
                "content" => $shop_link
            ),
            array(
                "name" => "SHOPNAME",
                "content" => $shop_name
            )
        );

        $client = new Client(new Version1X('http://nodejshost:3001'));
        $client->initialize();
        $client->emit('send_mail', ["message" => $message,"async" => true,"template_name" => $template_name,"template_content" => $template_content]);
        $client->close();
    }

    public function negozy_messenger_send_message($from_full_name = NULL,$to_full_name = NULL,$email = NULL,$text = NULL) {
        $message = array(
            "html" => "",
            "text" => "",
            "subject" => "Nuovo messaggio da ".$from_full_name,
            "from_email" => "",
            "from_name" => "",
            "to" => array(
                array(
                    "email" => $email,
                    "name"  => $to_full_name,
                    "type"  => array()
                )
            ),
            "headers" => array(
                "Reply-To" => "noreply@negozy.com"
            ) ,
            "tags" => array(
            ),
            "merge_language" => "mailchimp",

            "global_merge_vars"=>  array(
                array(
                    "name" => "FROM_FULLNAME",
                    "content" => "name"
                ),
                array(
                    "name" => "TO_FULLNAME",
                    "content" => "name"
                ),
                array(
                    "name" => "MESSAGE",
                    "content" => ""
                )
            ),
            "merge_vars" => array(
                array(
                    "rcpt" => $email,
                    "vars" => array(
                        array(
                            "name" => "FROM_FULLNAME",
                            "content" => $from_full_name
                        ),
                        array(
                            "name" => "TO_FULLNAME",
                            "content" => $to_full_name
                        ),
                        array(
                            "name" => "MESSAGE",
                            "content" => $text
                        )
                    )
                )
            )
        );

        $template_name = "negozy_messanger_send_message";
        $template_content = array(
            array(
                "name" => "FROM_FULLNAME",
                "content" => $from_full_name
            ),
            array(
                "name" => "TO_FULLNAME",
                "content" => $to_full_name
            ),
            array(
                "name" => "MESSAGE",
                "content" => $text
            )
        );

        $client = new Client(new Version1X('http://nodejshost:3001'));
        $client->initialize();
        $client->emit('send_mail', ["message" => $message,"async" => true,"template_name" => $template_name,"template_content" => $template_content]);
        $client->close();
    }

    public function negozy_messenger_send_blast_message($emails = array()) {
        $message = array(
            "html" => "",
            "text" => "",
            "subject" => "",
            "from_email" => "",
            "from_name" => "",
            "to" => $emails,
            "headers" => array(
                "Reply-To" => "negozy@cybereport.com"
            ) ,
            "tags" => array(
            )
        );
        $template_name = "blast_negozy";
        $template_content = array();
        $client = new Client(new Version1X('http://nodejshost:3001'));
        $client->initialize();
        $client->emit('send_mail', ["message" => $message,"async" => true,"template_name" => $template_name,"template_content" => $template_content]);
        $client->close();
    }

    public function send_venture_invite($full_name = NULL,$email = NULL,$text=NULL,$subject) {
        $message = array(
            "html" => "",
            "text" => "",
            "subject" => $subject,
            "from_email" => "",
            "from_name" => "",
            "to" => array(
                array(
                    "email" => $email,
                    "name"  => $full_name,
                    "type"  => array()
                )
            ),
            "headers" => array(
                "Reply-To" => "noreply@negozy.com"
            ) ,
            "merge_language" => "mailchimp",
            "global_merge_vars"=>  array(
                array(
                    "name" => "FULLNAME",
                    "content" => "name"
                ),
                array(
                    "name" => "TEXT",
                    "content" => "name"
                )
            ),
            "merge_vars" => array(
                array(
                    "rcpt" => $email,
                    "vars" => array(
                        array(
                            "name" => "FULLNAME",
                            "content" => $full_name
                        ),
                        array(
                            "name" => "TEXT",
                            "content" => $text
                        )
                    )
                )
            )
        );

        $template_name = "venture_invite";
        $template_content = array(
            array(
                "name" => "FULLNAME",
                "content" => $full_name
            ),
            array(
                "name" => "TEXT",
                "content" => $text
            )
        );

        $client = new Client(new Version1X('http://nodejshost:3001'));
        $client->initialize();
        $client->emit('send_mail', ["message" => $message,"async" => true,"template_name" => $template_name,"template_content" => $template_content]);
        $client->close();
    }


}