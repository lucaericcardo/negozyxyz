<?php
class DefaultShopComponent extends Component
{

    public function buildDefaultShop($shop_id){
        $this->CategoryPage         = ClassRegistry::init('CategoryPage');
        $this->CategoryPageContent  = ClassRegistry::init('CategoryPageContent');
        $this->Page                 = ClassRegistry::init('Page');
        $this->PageContent          = ClassRegistry::init('PageContent');
        $this->ShopLanguage         = ClassRegistry::init('ShopLanguage');
        $this->_saveShopLanguage($shop_id);
        $this->_savePageCategories($shop_id);
        $this->_savePageCategoriesTerms($shop_id);
    }


    private function _savePageCategories($shop_id){

        $default_category = array(
            "id"        => null,
            "shop_id"   => $shop_id,
            "type"      => "default",
            "default"   => true,
        );
        $this->CategoryPage->save($default_category);
        $default_category_content = array(
            "id"                => null,
            "title"             => "Default",
            "uri"               => "default",
            "language_id"       => 63,
            "category_page_id"  => $this->CategoryPage->id
        );
        $this->CategoryPageContent->save($default_category_content);

        $this->_saveHomePage($this->CategoryPage->id,$shop_id);
        $this->_saveContact($this->CategoryPage->id,$shop_id);
        $this->_saveProductsPage($this->CategoryPage->id,$shop_id);
        $this->_saveCheckoutPage($shop_id);
    }

    private function _savePageCategoriesTerms($shop_id){
        $default_category = array(
            "id"        => null,
            "shop_id"   => $shop_id,
            "type"      => "terms",
            "default"   => true
        );
        $this->CategoryPage->save($default_category);
        $term_category_content = array(
            "id"                => null,
            "title"             => "Termini e condizioni",
            "uri"               => "termini-e-condizioni",
            "language_id"       => 63,
            "category_page_id"  => $this->CategoryPage->id
        );
        $this->CategoryPageContent->save($term_category_content);
        $this->_saveTerm($this->CategoryPage->id,$shop_id);

    }

    private function _saveHomePage($default_category_id,$shop_id){
        $home_page = array(
            "id"                => NULL,
            "default"           => true,
            "category_page_id"  => $default_category_id,
            "type"              => "home",
            "shop_id"           => $shop_id,
            "order"             => 1,
            "published"         => true
        );
        $this->Page->save($home_page);
        $content = array(
            "id"        => null,
            "page_id"   => $this->Page->id,
            "lang_id"   => 63,
            "title"     => "Home",
            "uri"       => "home",
            "html"      => ""
        );
        $this->PageContent->save($content);
    }

    private function _saveContact($default_category_id,$shop_id){
        $contact = array(
            "id"                => NULL,
            "default"           => true,
            "category_page_id"  => $default_category_id,
            "type"              => "contact",
            "shop_id"           => $shop_id,
            "order"             => 2,
            "published"         => true
        );
        $this->Page->save($contact);
        $content = array(
            "id"        => null,
            "page_id"   => $this->Page->id,
            "lang_id"   => 63,
            "title"     => "Contatti",
            "uri"       => "contatti",
        );
        $this->PageContent->save($content);
    }

    private function _saveProductsPage($default_category_id,$shop_id){
        $contact = array(
            "id"                => NULL,
            "default"           => true,
            "category_page_id"  => $default_category_id,
            "type"              => "products",
            "shop_id"           => $shop_id,
            "order"             => 3,
            "published"         => true
        );
        $this->Page->save($contact);
        $content = array(
            "id"        => null,
            "page_id"   => $this->Page->id,
            "lang_id"   => 63,
            "title"     => "Prodotti",
            "uri"       => "prodotti",
        );
        $this->PageContent->save($content);
    }

    private function _saveCheckoutPage($shop_id){
        $contact = array(
            "id"                => NULL,
            "default"           => true,
            "category_page_id"  => null,
            "type"              => "checkout",
            "shop_id"           => $shop_id,
            "order"             => 4,
            "published"         => true
        );
        $this->Page->save($contact);
        $content = array(
            "id"        => null,
            "page_id"   => $this->Page->id,
            "lang_id"   => 63,
            "title"     => "Checkout",
            "uri"       => "checkout",
        );
        $this->PageContent->save($content);
    }

    private function _saveTerm($term_category_id,$shop_id){
        $title = "Termini e condizioni";
        $contact = array(
            "id"                => NULL,
            "default"           => true,
            "category_page_id"  => $term_category_id,
            "type"              => "term",
            "shop_id"           => $shop_id,
            "order"             => 1,
            "published"         => true
        );
        $this->Page->save($contact);
        $content = array(
            "id" => null,
            "page_id" => $this->Page->id,
            "lang_id" => 63,
            "title" => $title,
            "uri" => Inflector::slug($title, "-"),
            "html" => ""
        );
        $this->PageContent->save($content);
    }

    private function _saveShopLanguage($shop_id){
        $tmp_shop_language = array(
            "id"            => NULL,
            "shop_id"       => $shop_id,
            "language_id"   => 63,
            "order"         => 0
        );
        $this->ShopLanguage->save($tmp_shop_language);
    }
}