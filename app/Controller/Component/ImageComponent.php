<?php
App::import('Vendor', 'GoogleAutoload', ['file' => 'Google' . DS . 'autoload.php']);
class ImageComponent extends Component  {


    private $allowedExts = array("gif", "jpeg", "jpg", "png");
    private $allowedType = array("image/gif","image/jpeg","image/jpg","image/pjpeg","image/x-png","image/png");
    private $image = NULL;
    private $path = NULL;
    private $cdn_path = NULL;
    private $cdn_url = "http://cdn.negozy.com";

    public function uploadImage($file,$sub_folder = null,$types=array()){
        $array_return =array();
        $array_return['success'] = false;
        $array_return['image']['path'] = false;
        $array_return['image']['name'] = false;
        $array_return['image']['ext'] = false;
        $this->image = $file;
        $temp = explode(".", $this->image['name']);
        $extension = strtolower(end($temp));
        if (in_array($extension, $this->allowedExts) && in_array($file['type'],$this->allowedType)) {
            if ($file["error"] > 0) {
                return false;
            } else {
                $path = $this->_randomPath( $this->image["name"]);
                $this->path = WWW_ROOT."images/".$sub_folder."/".$path;
                $this->cdn_path = "images/".$sub_folder."/".$path;
                if(!file_exists($this->path)){
                    $this->_creareFolder($path,$sub_folder);
                }
                $image_name =  uniqid(false,true);
                foreach($types as $type){
                    $this->createImage($image_name,$type['prefix'],$type['width'],$type['height'],$extension);
                }
                $array_return['success'] = true;
                $array_return['image']['path'] = $path;
                $array_return['image']['name'] = $image_name;
                $array_return['image']['ext'] = $extension;
            }
        }
        return $array_return;
    }

    public function remove_image($image= NULL,$folder = NULL,$prefix=""){
        if(isset($image) && isset($folder)) {
            if(file_exists(WWW_ROOT."images/".$folder."/".$prefix.$image)){
                return unlink(WWW_ROOT."images/".$folder."/".$prefix.$image);
            }
        }
        return false;
    }

    protected function createImage($image_name,$prefix="",$custom_width=null,$custom_height = null,$extension)
    {
        if ($this->image['type'] == "image/jpeg") {
            $img = imagecreatefromjpeg($this->image['tmp_name']);
        } else if ($this->image['type'] == "image/png") {
            $img = imagecreatefrompng($this->image['tmp_name']);
        } else if ($this->image['type'] == "image/gif") {
            $img = imagecreatefromgif($this->image['tmp_name']);
        }

        $width = imagesx($img);
        $height = imagesy($img);
        if (isset($custom_width) && !isset($custom_height)) {
            $image_ratio = $width / $height;
            $new_width = $custom_width;
            $new_height = floor($custom_width / $image_ratio);
            /*if($width >= $custom_width ) {
                $image_ratio = $width / $height;
                $new_width = $custom_width;
                $new_height = floor($custom_width / $image_ratio);
            } else {
                $new_width = $width;
                $new_height = $height;
            }*/
        } else if (!isset($custom_width) && isset($custom_height)) {
            $image_ratio = $height / $width;
            $new_height = $custom_height;
            $new_width = floor($new_height / $image_ratio);
            /*if($height >= $custom_height ) {
                $image_ratio = $height / $width;
                $new_height = $custom_height;
                $new_width = floor($new_height / $image_ratio);
            } else {
                $new_width = $width;
                $new_height = $height;
            }*/
        } else {
            $new_height = $custom_height;
            $new_width = $custom_width;
        }

        $tmp_img = imagecreatetruecolor($new_width, $new_height);

        imagesavealpha($tmp_img, true);
        if ($this->image['type'] == "image/png") {
            $trans_background = imagecolorallocatealpha($tmp_img, 255, 255, 255, 0);
            imagefill($tmp_img, 0, 0, $trans_background);
            imagealphablending($tmp_img, false);
        }

        imagecopyresized( $tmp_img, $img, 0, 0, 0, 0, $new_width, $new_height, $width, $height);
        if($this->image['type'] == "image/jpeg") {
            imagejpeg($tmp_img, $this->path .$prefix . $image_name .".". $extension, 80);
        } else if($this->image['type'] == "image/png") {
            imagepng($tmp_img, $this->path .$prefix . $image_name .".". $extension, 8);
        } else if ($this->image['type'] == "image/gif"){
            imagegif($tmp_img, $this->path .$prefix . $image_name .".". $extension,8);
        }
        $this->upload2CDN($this->cdn_path.$prefix . $image_name .".". $extension,$this->path .$prefix . $image_name .".". $extension,$this->image['type']);
    }


    protected function upload2CDN($filename,$local_file,$type) {
        $client = new Google_Client();
        $client->setApplicationName("negozy");
        $credential = new Google_Auth_AssertionCredentials(
            "869284165055-94f7u6knlfq56v9rg4388tpd5mlpbvoj@developer.gserviceaccount.com",
            ['https://www.googleapis.com/auth/devstorage.full_control'],
            file_get_contents(APP."Vendor/Google/src/Google/cert/google.p12")
        );
        $client->setAssertionCredentials($credential);
        if($client->getAuth()->isAccessTokenExpired()) {
            $client->getAuth()->refreshTokenWithAssertion($credential);
        }
        $storage = new Google_Service_Storage($client);
        $file_name = $filename;
        $obj = new Google_Service_Storage_StorageObject();
        $obj->setName($file_name);
        $obj->setContentType($type);
        $obj->setContentDisposition("inline");
        $storage->objects->insert(
            "cdn.negozy.com",
            $obj,
            ['name' => $file_name, 'data' => file_get_contents($local_file), 'uploadType' => 'media','mimeType' => $type,'predefinedAcl' => 'publicread']
        );
    }

    protected function _creareFolder($path = NULL,$sub_folder){
        if(isset($path)) {
            $folders = explode("/",$path);
            $final_path = WWW_ROOT."images/".$sub_folder;
            foreach($folders as $dir) {
                $final_path .= "/".$dir;
                if(!file_exists($final_path)) {
                    if(!mkdir($final_path, 0777, true)) {
                        return false;
                    }
                }
            }
            return true;
        }
    }

    protected function _randomPath($string, $level = 3) {
        if (!$string) {
            throw new Exception(__('First argument is not a string!', true));
        }

        $string = crc32($string);
        $decrement = 0;
        $path = null;

        for ($i = 0; $i < $level; $i++) {
            $decrement = $decrement -2;
            $path .= sprintf("%02d" . DS, substr('000000' . $string, $decrement, 2));
        }

        return $path;
    }

    public function build_path($folder="",$prefix="image_",$image = array()){
        if(isset($image['path']) && isset($image['name']) && isset($image['ext'])) {
            return $this->cdn_url . "/images/" . $folder . "/" . $image['path'] . $prefix . $image['name'] . "." . $image['ext'];
        }
        return $this->cdn_url.'/negozy/Logos/no_logo.png';
    }

}