<?php
class UtilCategoriesComponent extends Component {
    public function geTreeBySubcategoryId($id) {
        $this->Subcategory = ClassRegistry::init('Subcategory');
        $this->Subcategory->Behaviors->load('Containable');
        return $this->Subcategory->find('first', array(
            'contain' => array(
                'Category' => array(
                    'fields' => array(
                        'id',
                        'label'
                    ),
                    'order' => 'Category.label',
                    'Macrocategory' => array(
                        'fields' => array(
                            'id',
                            'label'
                        ),
                        'order' => 'Macrocategory.label',
                    )
                )
            ),
            'conditions' => array(
                'Subcategory.id' => $id
            ),
            'fields' => array(
                'id',
                'label'
            ),
            'order' => 'Subcategory.label'
        ));
    }

    public function getShopCategories($shop_id = NULL) {
        $this->Product = ClassRegistry::init('Product');
        return $this->Product->find("all",array(
            "contain" => array(
                "Subcategory" => array(
                    "fields" => array(
                        "Subcategory.id",
                        "Subcategory.label"
                    ),
                    "Category" => array(
                        "fields" => array(
                            "Category.id",
                            "Category.label"
                        ),
                        "Macrocategory" => array(
                            "fields" => array(
                                "Macrocategory.id",
                                "Macrocategory.label"
                            ),
                        )
                    )
                )
            ),
           "conditions" => array(
               "Product.shop_id" => $shop_id,
               "Product.deleted" => false
           ),
            "fields" => array('DISTINCT Product.subcategory_id'),
           "recursive" => -1

        ));
    }

    public function geTreeByShopId($shop_id = NULL) {
        //if(!Cache::read('UtilCategories.Categories')) {
            $this->Macrocategory = ClassRegistry::init('Macrocategory');
            $this->Macrocategory->Behaviors->load('Containable');
            $macrocategories = $this->Macrocategory->find('all', array(
                'contain' => array(
                    'Category' => array(
                        'conditions' => array(
                            'OR' => array(
                                array(
                                    'default' => true,
                                ),
                                array(
                                    'default' => false,
                                    'shop_id' => $shop_id
                                )
                            )
                        ),
                        'fields' => array(
                            'label'
                        ),
                        'Subcategory' => array(
                            'conditions' => array(
                                'OR' => array(
                                    array(
                                        'default' => true,
                                    ),
                                    array(
                                        'default' => false,
                                        'shop_id' => $shop_id
                                    )
                                )
                            ),
                            'fields' => array(
                                'label'
                            )
                        )
                    )
                ),
                'fields' => array(
                    'label'
                ),
                'conditions' => array(
                    'OR' => array(
                        array(
                            'default' => true,
                        ),
                        array(
                            'default' => false,
                            'shop_id' => $shop_id
                        )
                    )
                )
            ));
            Cache::write('UtilCategories.Categories', $macrocategories);
            return $macrocategories;
        //}
        return Cache::read('UtilCategories.Categories');
        
    }
}