<?php
class NotifiesTemplateComponent extends Component {

    private $controller;

    public function initialize(Controller $controller) {
        $this->controller = $controller;
    }

    public function get($template, $params = array()) {
        $view = new View($this->controller, false);
        return $view->element('notifies/'.$template, $params);

    }
}