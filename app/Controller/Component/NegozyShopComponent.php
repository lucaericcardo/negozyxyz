<?php
class NegozyShopComponent extends Component {

    // notifiche di tipo 0: richiesta di disponibilità
    // notifiche di tipo 1: risposta alla richiesta

    public function createNegozyation($product_id, $product_info, $seller_id, $buyer_id, $shop_id, $message, $stepper_data) {
        $data = array();
        $this->Negozyation = ClassRegistry::init('Negozyation');
        $p_tmp = reset($product_info['ProductImage']);
        $data['product'] = array(
                        "id"        => $product_id,
                        "name"      => $product_info['Product']['name'],
                        "thumb"     => reset($p_tmp),
                        "messages"  => array()
        );
        $data['status'] = 'open';
        $data['data_start'] = new MongoDate();
        $data['data_keep_alive'] = new MongoDate(strtotime("+ 30 day"));
        $data['data_closed'] = 'null';
        $data['shop'] = array("id"=>$shop_id);
        $data['seller'] = array("id"=>$seller_id);
        $data['buyer'] = array("id"=>$buyer_id);
        $data['messages'] = array($message);
        $data['steppers'] = array();
        $data['steppers'][] = array(
            'message' => $message,
            'data' => array($stepper_data)
        );
        $this->Negozyation->create();
        if($this->Negozyation->save($data)) {
            if($notify_id = $this->sendNotification($this->Negozyation->id, $message, $seller_id, $buyer_id, 0)) {
                return array('negoziatyon_id' => $this->Negozyation->id, 'notify_id' => $notify_id);
            }
        }
        return false;
    }

    public function closeNegozyation($negozyation_id) {
        $this->Negozyation = ClassRegistry::init('Negozyation');
        //$this->Negozyation->id = $negozyation_id;
        $data = array(
            'Negozyation' => array(
                'id' => $negozyation_id,
                'status'=>'closed',
                'data_closed' => new MongoDate(),
            )
        );
        if($this->Negozyation->save($data)) {
            return true;
        }
        return false;
    }

    public function existNegozyation($product_id, $seller_id, $buyer_id, $shop_id) {
        $this->Negozyation = ClassRegistry::init('Negozyation');

        $options = array(
            'conditions' => array(
                'seller.id' => $seller_id,
                'product.id' => $product_id,
                'buyer.id' => $buyer_id,
                'status' => 'open'
            )
        );
        if($this->Negozyation->find('count', $options)) {

            return true;
        }
        return false;
    }

    public function sendNotification($negozyation_id, $message, $receiver_id, $sender_id, $type) {
        $this->ShopNotify = ClassRegistry::init('ShopNotify');
        $data = array();
        $data['receiver_id'] = $receiver_id;
        $data['sender_id'] = $sender_id;
        $data['negozyation_id'] = $negozyation_id;
        $data['message'] = $message;
        $data['visualized'] = false;
        $data['readed'] = false;
        $data['type'] = $type;

        $this->ShopNotify->create();
        if($this->ShopNotify->save($data)) {
            return $this->ShopNotify->id;
        }
        return false;
    }

    public function countUserNotifies($receiver_id) {
        $this->ShopNotify = ClassRegistry::init('ShopNotify');
        $options = array(
            "conditions" => array(
                "receiver_id" => $receiver_id,
                "visualized" => false
            ),
            "fields" => array(
                "ShopNotify.negozyation_id"
            )
        );
        $notifies = $this->ShopNotify->find('all', $options);
        $count = 0;
        if(count($notifies)) {
            foreach($notifies as $notify) {
                $negoziation = Hash::extract($this->getNegozyation($notify['ShopNotify']['negozyation_id']), 'Negozyation');
                if($negoziation['buyer']['id'] == $receiver_id) {
                    $count++;
                }
            }
        }
        return $count;
    }

    public function viewsUserNotifies($receiver_id) {
        $this->UserNotify = ClassRegistry::init('UserNotify');
        $options = array(
            "conditions" => array(
                "receiver_id" => $receiver_id,
                /*"type" => 0,
                "readed" => false*/
            ),
            //"limit" => 5,
            "order" => array("UserNotify.created" => -1)
        );
        $notifies = $this->UserNotify->find('all', $options);
        $return = array();
        if(count($notifies)) {
            foreach($notifies as $notify) {
                $negoziation = Hash::extract($this->getNegozyation($notify['UserNotify']['negozyation_id']), 'Negozyation');
                if($negoziation['buyer']['id'] == $receiver_id) {
                    $notify['UserNotify']['product_id'] = $negoziation['product']['id'];
                    $notify['UserNotify']['product_thumb'] = $negoziation['product']['thumb'];
                    $notify['UserNotify']['status'] = $negoziation['status'];
                    //$notify['UserNotify']['visualized'] = true;
                    //$this->UserNotify->save($notify['UserNotify']);
                    $return[] = $notify;
                }
            }
        }
        //var_dump($return);
        return $return;
    }

    public function viewsShopNotifies($receiver_id, $shop_id) {
        $this->ShopNotify = ClassRegistry::init('ShopNotify');
        $options = array(
            "conditions" => array(
                "receiver_id" => $receiver_id,
            ),
            "order" => array("ShopNotify.created" => -1)
        );
        $notifies = $this->ShopNotify->find('all', $options);
        $return = array();
        if(count($notifies)) {
            foreach($notifies as $notify) {
                $negoziation = Hash::extract($this->getNegozyation($notify['ShopNotify']['negozyation_id']), 'Negozyation');
                if($negoziation['shop']['id'] == $shop_id) {
                    $notify['ShopNotify']['product_id'] = $negoziation['product']['id'];
                    $notify['ShopNotify']['product_thumb'] = $negoziation['product']['thumb'];
                    $notify['ShopNotify']['status'] = $negoziation['status'];
                    $return[] = $notify;
                }
            }
        }
        return $return;
    }

    public function getNegoziatyonStateById($negoziatyon_id) {
        $this->Negozyation = ClassRegistry::init('Negozyation');

        $options = array(
            'conditions' => array(
                'Negozyation.id' => $negoziatyon_id
            ),
            'fields' => array('Negozyation.status'),
            'recursive' => -1
        );
        $negoziatyon = $this->Negozyation->find('first', $options);
        if(count($negoziatyon)) {
            return $negoziatyon['Negozyation']['status'];
        }
        return false;
    }

    public function readNotification($notify_id) {
        $this->ShopNotify = ClassRegistry::init('ShopNotify');
        $notify['id'] = $notify_id;
        $notify['ShopNotify']['readed'] = true;
        if($this->ShopNotify->save($notify)) {
            return true;
        }
        return false;
    }
    public function readNegozyation($seller_id, $notify_id) {
        if($this->readNotification($notify_id)) {
            $this->Negozyation = ClassRegistry::init('Negozyation');
            $options = array(
                'conditions' => array(
                    'seller.id' => $seller_id
                )
            );
            $negozyation = $this->Negozyation->find('first', $options);
            if($negozyation) {
                return $negozyation;
            }
        }
        return false;
    }

    public function updateNegozyation($negozyation_id, $message, $receiver_id, $sender_id, $data = array()) {
        $this->Negozyation = ClassRegistry::init('Negozyation');
        //$this->Negozyation->id = $negozyation_id;

        $dataToMongo = array(
            'Negozyation' => $data
        );

        if($this->Negozyation->save($dataToMongo)) {
            if($id = $this->sendUserNotification($negozyation_id, $message, $receiver_id, $sender_id, 1)) {
                return $id;
            }
        }
        return false;
    }

    private function sendUserNotification($negozyation_id, $message, $receiver_id, $sender_id, $type) {
        $this->UserNotify = ClassRegistry::init('UserNotify');
        $data = array();
        $data['receiver_id'] = $receiver_id;
        $data['sender_id'] = $sender_id;
        $data['negozyation_id'] = $negozyation_id;
        $data['message'] = $message;
        $data['visualized'] = false;
        $data['readed'] = false;
        $data['type'] = $type;

        $this->UserNotify->create();
        if($this->UserNotify->save($data)) {
            return $this->UserNotify->id;
        }
        return false;
    }


    public function getProductQuantityByNegozyationId($negoziatyon_id) {
        $this->Negozyation = ClassRegistry::init('Negozyation');

        $options = array(
            'conditions' => array(
                'Negozyation.id' => $negoziatyon_id
            ),
            'fields' => array('Negozyation.steppers'),
            'recursive' => -1
        );
        $negoziatyon = $this->Negozyation->find('first', $options);
        if(count($negoziatyon)) {
            $tmp = reset($negoziatyon['Negozyation']['steppers']);
            return $tmp['data'][0]['availability_request'];
        }
        return false;
    }

    public function getNegozyation($negoziatyon_id) {
        $this->Negozyation = ClassRegistry::init('Negozyation');
        return $this->Negozyation->read(null, $negoziatyon_id);
    }

    public function updateNotify($notify_id, $data = array()) {
        $this->ShopNotify = ClassRegistry::init('ShopNotify');

        $this->ShopNotify->id = $notify_id;
        $dataToMongo = array(
            'ShopNotify' => $data
        );

        if($this->ShopNotify->save($dataToMongo)) {
            return true;
        }
        return false;
    }

    public function getShopNameById($shop_id) {
        $this->Shop = ClassRegistry::init('Shop');
        $options = array(
            'conditions' => array(
                'Shop.id' => $shop_id
            ),
            'fields' => array('Shop.name')
        );
        $shop = $this->Shop->find('first', $options);
        if(count($shop)) {
            return $shop['Shop']['name'];
        }
        return false;
    }
    public function getShopIdByNegoziatyonId($negoziatyon_id) {
        $this->Negozyation = ClassRegistry::init('Negozyation');

        $options = array(
            'conditions' => array(
                'Negozyation.id' => $negoziatyon_id
            ),
            'fields' => array('shop.id'),
            'recursive' => -1
        );
        $negoziatyon = $this->Negozyation->find('first', $options);
        if(count($negoziatyon)) {
            return $negoziatyon['Negozyation']['shop']['id'];
        }
        return false;
    }
    public function getShopNameByNegoziatyonId($negoziatyon_id) {
        return $this->getShopNameById($this->getShopIdByNegoziatyonId($negoziatyon_id));
    }
    public function getSellerIdByNegoziatyonId($negoziatyon_id) {
        $this->Negozyation = ClassRegistry::init('Negozyation');

        $options = array(
            'conditions' => array(
                'Negozyation.id' => $negoziatyon_id
            ),
            'fields' => array('Negozyation.seller.id'),
            'recursive' => -1
        );
        $negoziatyon = $this->Negozyation->find('first', $options);
        if(count($negoziatyon)) {
            return $negoziatyon['Negozyation']['seller']['id'];
        }
        return false;
    }
    public function getBuyerIdByNegoziatyonId($negoziatyon_id) {
        $this->Negozyation = ClassRegistry::init('Negozyation');

        $options = array(
            'conditions' => array(
                'Negozyation.id' => $negoziatyon_id
            ),
            'fields' => array('Negozyation.buyer.id'),
            'recursive' => -1
        );
        $negoziatyon = $this->Negozyation->find('first', $options);
        if(count($negoziatyon)) {
            return $negoziatyon['Negozyation']['buyer']['id'];
        }
        return false;
    }

    public function getProductInfoByNegoziatyonId($negoziatyon_id) {
        $this->Negozyation = ClassRegistry::init('Negozyation');

        $options = array(
            'conditions' => array(
                'Negozyation.id' => $negoziatyon_id
            ),
            'fields' => array('Negozyation.product'),
            'recursive' => -1
        );
        $negoziatyon = $this->Negozyation->find('first', $options);
        if(count($negoziatyon)) {
            return $negoziatyon['Negozyation']['product'];
        }
        return false;
    }

    public function getRequest($user_id) {
        $this->Negozyation = ClassRegistry::init('Negozyation');
        $options = array(
            "conditions" => array(
                "buyer.id" => $user_id
            ),
            "fields" => array(
                "_id",
                "product",
                "status",
                "shop.id",
                "steppers",
                "created",
                "modified",
                "data_closed"
            ),
            "order" => array("created" => -1)
        );
        $negozyations = $this->Negozyation->find('all', $options);
        //$log = $this->ShopNotify->getDataSource()->getLog(false, false);
        //var_dump($log);

        if(count($negozyations)) {
            foreach($negozyations as &$negozyation) {
                $c = count($negozyation['Negozyation']['steppers']);
                if($c == 1) { // stato richiesta disponibilità
                    $negozyation['Negozyation']['actions'] = __('In attesa di risposta');
                } else if($c = 2) { // stato risposta alla richiesta di disponibilità
                    // aggiornare la relativa notifica come visualizzata
                    $this->setNotificationAsViewd($negozyation['Negozyation']['id'], $user_id);
                    $negozyation['Negozyation']['actions'] = 'buy';
                    if($negozyation['Negozyation']['status'] == 'closed' && !$negozyation['Negozyation']['steppers'][1]['data']['available']) {
                        // negoziazione chiusa perchè il prodotto richieasto non è disponibile
                        $negozyation['Negozyation']['actions'] = __('Nessuna disponibilità');
                    } else if($negozyation['Negozyation']['status'] == 'closed') {
                        // negoziazione chiusa e perchè è stato effettuato il pagamento
                        $negozyation['Negozyation']['actions'] = __('Acquistato');
                    }
                }

                if($negozyation['Negozyation']['data_closed'] != null && $negozyation['Negozyation']['data_closed'] != 'null') {
                    $negozyation['Negozyation']['data_closed'] = strtotime($negozyation['Negozyation']['data_closed']);
                } else {
                    $negozyation['Negozyation']['data_closed'] = strtotime($negozyation['Negozyation']['modified']);
                }

                $tmp = reset($negozyation['Negozyation']['steppers']);
                $negozyation['Negozyation']['product_quantity'] = $tmp['data'][0]['availability_request'];
                $negozyation['Negozyation']['created'] = strtotime($negozyation['Negozyation']['created']);
                $negozyation['Negozyation']['to'] = $this->getShopName($negozyation['Negozyation']['shop']['id']);
                $negozyation['Negozyation']['product_id'] = $negozyation['Negozyation']['product']['id'];
                $negozyation['Negozyation']['product_thumb'] = $negozyation['Negozyation']['product']['thumb'];
                $negozyation['Negozyation']['product_name'] = $negozyation['Negozyation']['product']['name'];
                unset($negozyation['Negozyation']['product']);
                unset($negozyation['Negozyation']['shop']);
                unset($negozyation['Negozyation']['steppers']);
                unset($negozyation['Negozyation']['modified']);
            }
        }
        return Hash::extract($negozyations, '{n}.Negozyation');
    }

    private function setNotificationAsViewd($negoziation_id, $receiver_id) {
        $this->UserNotify = ClassRegistry::init('UserNotify');

        return $this->UserNotify->updateAll(
            array('visualized' => true), // campo da aggiornare
            array('UserNotify.negozyation_id' => $negoziation_id, 'UserNotify.receiver_id' => $receiver_id) // condizioni
        );

    }

    private function getShopName($shop_id) {
        $this->Shop = ClassRegistry::init('Shop');
        $otpions = array(
            'conditions' => array(
                'Shop.id' => $shop_id,
            ),
            'fields' => array(
                'Shop.name'
            ),
            'recursive' => -1
        );
        $shop = $this->Shop->find('first', $otpions);
        if($shop) {
            return $shop['Shop']['name'];
        }
        return false;
    }

    private function setShopNotificationAsViewd($negoziation_id, $receiver_id) {
        $this->ShopNotify = ClassRegistry::init('ShopNotify');

        return $this->ShopNotify->updateAll(
            array('visualized' => true), // campo da aggiornare
            array('ShopNotify.negozyation_id' => $negoziation_id, 'ShopNotify.receiver_id' => $receiver_id) // condizioni
        );
    }

    private function getNotifyId($negoziation_id, $receiver_id) {
        $this->ShopNotify = ClassRegistry::init('ShopNotify');
        $options = array(
            'conditions' => array(
                'ShopNotify.negozyation_id' => $negoziation_id,
                'ShopNotify.receiver_id' => $receiver_id
            ),
            'fields' => array('ShopNotify.id')
        );
        if($notify = $this->ShopNotify->find('first',$options)) {
            return $notify['ShopNotify']['id'];
        }

        return false;
    }

    public function getShopRequest($user_id, $shop_id) {
        $this->Negozyation = ClassRegistry::init('Negozyation');
        $options = array(
            "conditions" => array(
                "seller.id" => $user_id,
                "shop.id" => $shop_id
            ),
            "fields" => array(
                "_id",
                "product",
                "status",
                "shop.id",
                "steppers",
                "created",
                "modified",
                "data_closed"
            ),
            "order" => array("created" => -1)
        );
        $negozyations = $this->Negozyation->find('all', $options);
        //$log = $this->ShopNotify->getDataSource()->getLog(false, false);
        //var_dump($log);

        if(count($negozyations)) {
            foreach($negozyations as &$negozyation) {
                $notify_id = null;
                $c = count($negozyation['Negozyation']['steppers']);
                if($c == 1) { // stato richiesta disponibilità
                    $negozyation['Negozyation']['actions'] = 'take_disponibility';
                    $notify_id = $this->getNotifyId($negozyation['Negozyation']['id'], $user_id);
                } else if($c = 2) { // stato risposta alla richiesta di disponibilità
                    // aggiornare la relativa notifica come visualizzata
                    $this->setShopNotificationAsViewd($negozyation['Negozyation']['id'], $user_id);
                    //$negozyation['Negozyation']['actions'] = 'sell';
                    if($negozyation['Negozyation']['status'] == 'closed' && !$negozyation['Negozyation']['steppers'][1]['data']['available']) {
                        // negoziazione chiusa perchè il prodotto richieasto non è disponibile
                        $negozyation['Negozyation']['actions'] = __('Nessuna disponibilità');
                    } else if($negozyation['Negozyation']['status'] == 'closed') {
                        // negoziazione chiusa e perchè è stato effettuato il pagamento
                        $negozyation['Negozyation']['actions'] = __('Venduto');
                    }
                }

                if($negozyation['Negozyation']['data_closed'] != null && $negozyation['Negozyation']['data_closed'] != 'null') {
                    $negozyation['Negozyation']['data_closed'] = strtotime($negozyation['Negozyation']['data_closed']);
                } else {
                    $negozyation['Negozyation']['data_closed'] = strtotime($negozyation['Negozyation']['modified']);
                }

                $tmp = reset($negozyation['Negozyation']['steppers']);
                $negozyation['Negozyation']['product_quantity'] = $tmp['data'][0]['availability_request'];
                $negozyation['Negozyation']['created'] = strtotime($negozyation['Negozyation']['created']);
                $negozyation['Negozyation']['to'] = $this->getShopName($negozyation['Negozyation']['shop']['id']);
                $negozyation['Negozyation']['product_id'] = $negozyation['Negozyation']['product']['id'];
                $negozyation['Negozyation']['product_thumb'] = $negozyation['Negozyation']['product']['thumb'];
                $negozyation['Negozyation']['product_name'] = $negozyation['Negozyation']['product']['name'];
                $negozyation['Negozyation']['notify_id'] = $notify_id;
                unset($negozyation['Negozyation']['product']);
                unset($negozyation['Negozyation']['shop']);
                unset($negozyation['Negozyation']['steppers']);
                unset($negozyation['Negozyation']['modified']);
            }
        }
        return Hash::extract($negozyations, '{n}.Negozyation');
    }
}