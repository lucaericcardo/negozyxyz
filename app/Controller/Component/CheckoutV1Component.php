<?php
App::uses('CakeNumber', 'Utility');
class CheckoutV1Component extends Component {

    public $components = array('Session','PriceHelper');

    public function setCountry($token = NULL,$country_id = NULL) {
        $cartModel = ClassRegistry::init('Cart');
        $cart = $cartModel->find("first",array(
            "conditions" => array(
                "token" => $token
            ),
            "recursive" => -1
        ));
        $cart_data = json_decode($cart['Cart']['data'],TRUE);
        $wrong_shipping             = FALSE;
        $wrong_shipping_products    = array();
        $cart_data['shipping_subtotal'] = 0;
        $cart_data['tax_subtotal']      = 0;
        $cart_data['shipping_taxable']  = 0;
        $cart_data['shipping_tax']      = 0;
        $loaded_shipping_group  = [];
        foreach($cart_data['products'] as $key =>  $product) {
            $shipping_price = ($this->loadShippingCost($product['id'],$country_id,$cart_data['shop']['id']));
            if((!isset($loaded_shipping_group[$shipping_price['Shipping']['id']])) || (!isset($loaded_shipping_group[$shipping_price['Shipping']['id']]) && $shipping_price['Shipping']['additional_price']) || ($loaded_shipping_group[$shipping_price['Shipping']['id']] && $shipping_price['Shipping']['additional_price'])) {
                $loaded_shipping_group[$shipping_price['Shipping']['id']] = $shipping_price['Shipping']['id'];
                if ($shipping_price) {
                    if ($product['quantity'] > 1) {
                        $price = ($shipping_price['Shipping']['price']) + ($shipping_price['Shipping']['additional_price'] * ($product['quantity'] - 1));
                    } else {
                        $price = $shipping_price['Shipping']['price'];
                    }
                    $shipping_tax = array(
                        "value" => ($shipping_price['ShopTax']['tax']) ? $shipping_price['ShopTax']['tax'] : 0,
                        "price_included" => ($shipping_price['ShopTax']['product_tax']) ? true : false
                    );
                    $cart_data['products'][$key]['shipping_unit'] = array(
                        "price_no_tax" => $this->PriceHelper->no_vat_price($shipping_price['Shipping']['price'], $shipping_price['ShopTax']),
                        "price_tax" => $this->PriceHelper->vat_price($shipping_price['Shipping']['price'], $shipping_price['ShopTax']),
                        "price_total" => $this->PriceHelper->format_price($shipping_price['Shipping']['price'], $shipping_price['ShopTax']),
                        "additional_no_tax" => $this->PriceHelper->no_vat_price($shipping_price['Shipping']['additional_price'], $shipping_price['ShopTax']),
                        "additional_tax" => $this->PriceHelper->vat_price($shipping_price['Shipping']['additional_price'], $shipping_price['ShopTax']),
                        "additional_total" => $this->PriceHelper->format_price($shipping_price['Shipping']['additional_price'], $shipping_price['ShopTax']),
                        "tax" => $shipping_tax
                    );
                    $cart_data['products'][$key]['shipping_total']['price_no_tax'] = $this->PriceHelper->no_vat_price($price, $shipping_price['ShopTax']);
                    $cart_data['products'][$key]['shipping_total']['price_tax'] = $this->PriceHelper->vat_price($price, $shipping_price['ShopTax']);
                    $cart_data['products'][$key]['shipping_total']['price_total'] = $this->PriceHelper->format_price($price, $shipping_price['ShopTax']);
                    $cart_data['shipping_subtotal'] += $this->PriceHelper->format_price($price, $shipping_price['ShopTax']);
                    $cart_data['shipping_taxable'] += $this->PriceHelper->no_vat_price($price, $shipping_price['ShopTax']);
                    $cart_data['shipping_tax'] += $this->PriceHelper->vat_price($price, $shipping_price['ShopTax']);
                    $cart_data['tax_subtotal'] += ($cart_data['products'][$key]['product_total']['price_tax'] + $this->PriceHelper->vat_price($price, $shipping_price['ShopTax']));
                } else {
                    $wrong_shipping = TRUE;
                    $wrong_shipping_products[] = array(
                        "id" => $product['id'],
                        "name" => $product['name']
                    );
                }
            }
        }
        $cart_data['total']                     = $cart_data['shipping_subtotal'] + $cart_data['product_subtotal'] + $cart_data['extra_subtotal'];
        $cart_data['wrong_shipping']            = $wrong_shipping;
        $cart_data['wrong_shipping_products']   = $wrong_shipping_products;
        $json_data = json_encode($cart_data);
        $cartModel->id = $cart['Cart']['id'];
        if($cartModel->saveField("data",$json_data)) {
            return true;
        }
        return true;
    }

    public function setPayment($token=NULL,$payment_id = NULL){
        if(isset($token) && isset($payment_id)) {
            $cartModel = ClassRegistry::init('Cart');
            $cart = $cartModel->find("first",array(
                "conditions" => array(
                    "token" => $token
                ),
                "recursive" => -1
            ));
            if($cart) {
                $cart_data = json_decode($cart['Cart']['data'], TRUE);
                $paymentModel = ClassRegistry::init('Payment');
                $payment = $paymentModel->find("first",array(
                    "conditions" => array(
                        "id"        => $payment_id,
                        "shop_id"   => $cart_data['shop']['id']
                    ),
                    "recursive" -1
                ));
                if($payment) {
                    $payment_name = "";
                    if($payment['Payment']['name'] == "bonifico") {
                        $payment_name = __("Bonifico bancario");
                    } else if ($payment['Payment']['name'] == "money") {
                        $payment_name = __("Pagamento diretto al negoziante");
                    } else if ($payment['Payment']['name'] == "contrassegno") {
                        $payment_name = __("Contrassegno");
                    } else if ($payment['Payment']['name'] == "paypal") {
                        $payment_name = __("PayPal o carta di credito");}
                    $cart_data['payment'] = array(
                        "id"    => $payment['Payment']['id'],
                        "name"  => $payment['Payment']['name'],
                        "label" => $payment_name,
                        "value" => $payment['Payment']['value'],
                        "price" => $payment['Payment']['price']
                    );
                    $payment['Payment'];
                    $cart_data['extra_subtotal']    = $payment['Payment']['price'];
                    $cart_data['total']             = $cart_data['shipping_subtotal'] + $cart_data['product_subtotal'] + $cart_data['extra_subtotal'];
                    $json_data = json_encode($cart_data);
                    $cartModel->id = $cart['Cart']['id'];
                    if($cartModel->saveField("data",$json_data)) {
                        return true;
                    }
                }
            }
        }
        return false;
    }

    public function setUser($token=NULL,$shipping_address = NULL,$billing_address = NULL){
        if(isset($token) && isset($shipping_address) && isset($billing_address)) {
            $cartModel = ClassRegistry::init('Cart');
            $cart = $cartModel->find("first",array(
                "conditions" => array(
                    "token" => $token
                ),
                "recursive" => -1
            ));
            if($cart) {
                $cart_data = json_decode($cart['Cart']['data'], TRUE);
                $cart_data['user'] = array(
                    "shipping_address"  => $shipping_address,
                    "billing_address"   => (isset($billing_address['same_shipping']) && $billing_address['same_shipping'] == 1) ? $shipping_address : $billing_address
                );
                $json_data = json_encode($cart_data);
                $cartModel->id = $cart['Cart']['id'];
                if($cartModel->saveField("data",$json_data)) {
                    return true;
                }
            }
        }
        return false;
    }

    private function loadShippingCost($product_id = NULL,$country_id=NULL,$shop_id = NULL){
        $shippingGroupModel = ClassRegistry::init('ShippingGroup');
        $data = $shippingGroupModel->find("first",array(
            "conditions" => array(
                "ShippingGroup.shop_id" => $shop_id,
                "Product.id"            => $product_id,
                'OR' =>
                    array(
                        array('AND' => array(
                            array('Shipping.country_type' => 1),
                            array('CountryShipping.country_id' => $country_id)
                        )),
                        array('AND' => array(
                            array('Shipping.country_type' => 2),
                            array('Country.group_europe' => true)
                        )),
                        array(
                            array('Shipping.country_type' => 3)
                        )
                    )
            ),
            "fields" => array(
                "Shipping.*",
            ),
            "joins" => array(
                array(
                    "table" => "products",
                    'alias' => 'Product',
                    'type' => 'RIGHT',
                    'conditions' => array(
                        'Product.shipping_group_id = ShippingGroup.id',
                    )
                ),
                array(
                    "table" => "shipments",
                    'alias' => 'Shipping',
                    'type' => 'INNER',
                    'conditions' => array(
                        'Shipping.shipping_group_id = ShippingGroup.id',
                    )
                ),
                array(
                    "table" => "countries_shipments",
                    'alias' => 'CountryShipping',
                    'type' => 'LEFT',
                    'conditions' => array(
                        'CountryShipping.shipping_id = Shipping.id',
                    )
                ),
                array(
                    "table" => "countries",
                    'alias' => 'Country',
                    'type' => 'LEFT',
                    'conditions' => array(
                        'Country.id' => $country_id
                    )
                )
            ),
            'order' => 'Shipping.country_type ASC',
            "recursive" => -1
        ));
        if(isset($data['Shipping'])) {
            $shipping_tax = NULL;
            if (isset($data['Shipping']['shop_tax_id'])) {
                $shoptaxModel = ClassRegistry::init('ShopTax');
                $shipping_tax = $shoptaxModel->find("first", array(
                    "conditions" => array(
                        "id" => $data['Shipping']['shop_tax_id']
                    ),
                    "fields" => array(
                        'tax',
                        'product_tax',
                        'id'
                    ),
                    "recursive" => -1
                ));
            }
            return array(
                "Shipping" => $data['Shipping'],
                "ShopTax" => $shipping_tax['ShopTax']
            );
        }
        return false;

    }


}