<?php
class CheckoutComponent extends Component {

    public $components = array('Session','PriceHelper');

    public function cart($token = NULL) {
        if(isset($token) && $this->Session->check("Order.".$token)){
            $total = $this->Session->read("Order.".$token.".product.price_total") + $this->Session->read("Order.".$token.".shipping.price_total") + $this->Session->read("Order.".$token.".payment.price_total");
            $total_no_tax = $this->Session->read("Order.".$token.".product.price_no_tax") + $this->Session->read("Order".$token."shipping.price_no_tax");
            $total_tax = $this->Session->read("Order.".$token.".product.price_tax") + $this->Session->read("Order.".$token.".shipping.price_tax");
            $this->Session->write('Order.'.$token.'.total_tax', $total_tax);
            $this->Session->write('Order.'.$token.'.total_no_tax', $total_no_tax);
            $this->Session->write('Order.'.$token.'.total', $total);
            return $this->Session->read("Order.".$token);
        }
        return false;
    }

    public function assignOrderToCustomer($token = NULL,$user_id = NULL){
        if(isset($token) && $this->Session->check("Order.".$token)){
            $this->Session->write('Order.'.$token.'.user.id', $user_id);
        }
    }

    public function assignOrderToNegotiation($token = NULL,$negotiation_id= NULL){
        if(isset($token) && $this->Session->check("Order.".$token)){
            $this->Session->write('Order.'.$token.'.negotiation_id', $negotiation_id);
            return true;
        }
        return false;
    }

    public function addProductToCart($product_id = NULL,$quantity = 1) {

        //$this->Session->delete('Order');
        $token = $this->setEmptyCart();
        $productModel = ClassRegistry::init('Product');
        $product = $productModel->find("first",array(
            "conditions" => array(
                "Product.id"        => $product_id,
                "Product.deleted"   => false,
                "Product.published" => true
            ),
            "fields" => array(
                "Product.id",
                "Product.name",
                "Product.price",
                "Product.type_quantity",
                "Product.quantity",
                "Product.shop_id",
            ),
            "contain" => array(
                "ProductImage" => array(
                    "fields" => array(
                        "image"
                    )
                ),
                "ShopTax" => array(
                    "fields" => array(
                        'ShopTax.tax',
                        'ShopTax.product_tax'
                    )
                ),
                "Shop" => array(
                    "fields" => array(
                        "Shop.name",
                        "Shop.currency",
                        "Shop.url",
                    ),
                    "ShopLogo" => array(
                        "fields" => array(
                            "image"
                        )
                    )
                )
            ),
            "recursive" => -1
        ));
        if (!is_numeric($quantity) || !$quantity) {
            $quantity = 1;
        }
        if($product) {
            $shop_image = NULL;
            if (isset($product['Shop']['ShopLogo'])) {
                $shop_image = reset($product['Shop']['ShopLogo']);
                $shop_image = $shop_image['image'];
            }

            $product_image = NULL;
            if (isset($product['ProductImage'])) {
                $product_image = reset($product['ProductImage']);
                $product_image = $product_image['image'];
            }

            $price_no_tax   = $this->PriceHelper->no_vat_price($product['Product']['price'],$product['ShopTax']);
            $price_tax      = $this->PriceHelper->vat_price($product['Product']['price'],$product['ShopTax']);
            $price_total    = $this->PriceHelper->format_price($product['Product']['price'],$product['ShopTax']);

            $this->Session->write('Order.'.$token.'.product.quantity', $quantity);
            $this->Session->write('Order.'.$token.'.product.id', $product['Product']['id']);
            $this->Session->write('Order.'.$token.'.product.name', $product['Product']['name']);
            $this->Session->write('Order.'.$token.'.product.image', $product_image);
            $this->Session->write('Order.'.$token.'.product.type_quantity', $product['Product']['type_quantity']);
            $this->Session->write('Order.'.$token.'.product.warehouse_quantity', $product['Product']['quantity']);

            $this->Session->write('Order.'.$token.'.product.unit.price_no_tax', $price_no_tax);
            $this->Session->write('Order.'.$token.'.product.unit.price_tax', $price_tax);
            $this->Session->write('Order.'.$token.'.product.unit.price_total', $price_total);

            $this->Session->write('Order.'.$token.'.product.price_no_tax', ($price_no_tax * $quantity));
            $this->Session->write('Order.'.$token.'.product.price_tax', ($price_tax * $quantity));
            $this->Session->write('Order.'.$token.'.product.price_total', ($price_total * $quantity));

            $this->Session->write('Order.'.$token.'.shop.id', $product['Shop']['id']);
            $this->Session->write('Order.'.$token.'.shop.name', $product['Shop']['name']);
            $this->Session->write('Order.'.$token.'.shop.currency', $product['Shop']['currency']);
            $this->Session->write('Order.'.$token.'.shop.url', $product['Shop']['url']);
            $this->Session->write('Order.'.$token.'.shop.logo', $shop_image);

            return $token;
        }
        return false;
    }

    public function addShipping($shipping_id = NULL,$token = NULL){
        if(isset($shipping_id)) {
            if($this->Session->check("Order")) {
                $shippingModel = ClassRegistry::init('Shipping');
                $shipping = $shippingModel->find("first",array(
                    "conditions" => array(
                        "Shipping.shop_id" => $this->Session->read("Order.".$token.".shop.id"),
                        "Shipping.id" => $shipping_id
                    ),
                    "contain" => array(
                        "ShopTax" => array(
                            "id",
                            "tax",
                            "product_tax"
                        )
                    ),
                    "recursive" => -1
                ));
                if($shipping) {

                    $quantity = $this->Session->read('Order.'.$token.'.product.quantity');

                    $price_no_tax   = $this->PriceHelper->no_vat_price($shipping['Shipping']['price'], $shipping['ShopTax']);
                    $price_tax      = $this->PriceHelper->vat_price($shipping['Shipping']['price'], $shipping['ShopTax']);
                    $price_total    = $this->PriceHelper->format_price($shipping['Shipping']['price'], $shipping['ShopTax']);

                    $this->Session->write('Order.'.$token.'.shipping.id', $shipping_id);
                    $this->Session->write('Order.'.$token.'.shipping.name', $shipping['Shipping']['name']);
                    $this->Session->write('Order.'.$token.'.shipping.start_day', $shipping['Shipping']['start_day']);
                    $this->Session->write('Order.'.$token.'.shipping.end_day', $shipping['Shipping']['end_day']);
                    $this->Session->write('Order.'.$token.'.shipping.price_no_tax', ($price_no_tax * $quantity));
                    $this->Session->write('Order.'.$token.'.shipping.price_tax', ($price_tax * $quantity));
                    $this->Session->write('Order.'.$token.'.shipping.price_total', ($price_total * $quantity));
                    $this->Session->write('Order.'.$token.'.shipping.unit.price_no_tax', $price_no_tax);
                    $this->Session->write('Order.'.$token.'.shipping.unit.price_tax', $price_tax);
                    $this->Session->write('Order.'.$token.'.shipping.unit.price_total', $price_total);

                    return true;

                }
            }
        }
        return false;
    }

    public function addPayment($payment_id = NULL,$token = NULL){
        if(isset($payment_id) && isset($token)) {
            if($this->Session->check("Order")) {
                $paymentModel = ClassRegistry::init('Payment');
                $payment = $paymentModel->find("first",array(
                    "conditions" => array(
                        "Payment.shop_id" => $this->Session->read("Order.".$token.".shop.id"),
                        "Payment.id" => $payment_id
                    ),
                    "recursive" => -1
                ));
                if($payment) {
                    $price_total    = $this->PriceHelper->format_price($payment['Payment']['price']);
                    $payment_name = "";
                    if($payment['Payment']['name'] == "bonifico") {
                        $payment_name = __("Bonifico bancario");
                    } else if ($payment['Payment']['name'] == "money") {
                        $payment_name = __("Pagamento diretto al negoziante");
                    } else if ($payment['Payment']['name'] == "contrassegno") {
                        $payment_name = __("Contrassegno");
                    } else if ($payment['Payment']['name'] == "paypal") {
                        $payment_name = __("PayPal o carta di credito");}

                    $this->Session->write('Order.'.$token.'.payment.id', $payment_id);
                    $this->Session->write('Order.'.$token.'.payment.name', $payment['Payment']['name']);
                    $this->Session->write('Order.'.$token.'.payment.price_total', $price_total);
                    $this->Session->write('Order.'.$token.'.payment.value', $payment['Payment']['value']);
                    $this->Session->write('Order.'.$token.'.payment.label', $payment_name);
                    return true;
                }
            }
        }
        return false;
    }

    private function setEmptyCart(){

        $token = md5(uniqid('', true));
        $this->Session->write('Order.'.$token.'.product.quantity', NULL);
        $this->Session->write('Order.'.$token.'.product.id', NULL);
        $this->Session->write('Order.'.$token.'.product.name', NULL);
        $this->Session->write('Order.'.$token.'.product.type_quantity', NULL);
        $this->Session->write('Order.'.$token.'.product.warehouse_quantity', NULL);
        $this->Session->write('Order.'.$token.'.product.unit.price_no_tax', 0);
        $this->Session->write('Order.'.$token.'.product.unit.price_tax', 0);
        $this->Session->write('Order.'.$token.'.product.unit.price_total', 0);
        $this->Session->write('Order.'.$token.'.product.price_no_tax', 0);
        $this->Session->write('Order.'.$token.'.product.price_tax', 0);
        $this->Session->write('Order.'.$token.'.product.price_total', 0);

        $this->Session->write('Order.'.$token.'.shipping.id', NULL);
        $this->Session->write('Order.'.$token.'.shipping.name', NULL);
        $this->Session->write('Order.'.$token.'.shipping.start_day', 0);
        $this->Session->write('Order.'.$token.'.shipping.end_day', 0);
        $this->Session->write('Order.'.$token.'.shipping.unit.price_no_tax', 0);
        $this->Session->write('Order.'.$token.'.shipping.unit.price_tax', 0);
        $this->Session->write('Order.'.$token.'.shipping.unit.price_total', 0);
        $this->Session->write('Order.'.$token.'.shipping.price_no_tax', 0);
        $this->Session->write('Order.'.$token.'.shipping.price_tax', 0);
        $this->Session->write('Order.'.$token.'.shipping.price_total', 0);

        $this->Session->write('Order.'.$token.'.payment.id', NULL);
        $this->Session->write('Order.'.$token.'.payment.name', NULL);
        $this->Session->write('Order.'.$token.'.payment.value', NULL);
        $this->Session->write('Order.'.$token.'.payment.price_total', 0);

        $this->Session->write('Order.'.$token.'.shop.id', NULL);
        $this->Session->write('Order.'.$token.'.shop.name', NULL);
        $this->Session->write('Order.'.$token.'.shop.logo', NULL);

        $this->Session->write('Order.'.$token.'.user.id', NULL);
        $this->Session->write('Order.'.$token.'.completed', FALSE);
        $this->Session->write('Order.'.$token.'.negotiation_id', FALSE);

        return $token;
    }

}