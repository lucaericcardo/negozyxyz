<?php
App::import('Vendor', 'GoogleAutoload', ['file' => 'Google' . DS . 'autoload.php']);
class GoogleComponent extends Component {

    private $client_id = "869284165055-aisg56l6bd9idvml88im752tmuaumhon.apps.googleusercontent.com";
    private $client_secret = "ZaOEXYQG6yp8DODvVoMhMgp1";
    private $redirect_uri = "http://my.negozy.com/users/oauth2callback";

    public function getLoginUrl(){
        $client = new Google_Client();
        $client->setClientId($this->client_id);
        $client->setClientSecret($this->client_secret);
        $client->setRedirectUri($this->redirect_uri);
        $client->setApprovalPrompt('auto');
        $client->setScopes("https://www.googleapis.com/auth/userinfo.email");
        return $client->createAuthUrl();
    }

    public function setAccessToken($code = NULL) {
        if(isset($code) && $code) {
            $client = new Google_Client();
            $client->setClientId($this->client_id);
            $client->setClientSecret($this->client_secret);
            $client->setRedirectUri($this->redirect_uri);
            $client->setApprovalPrompt('auto');
            $client->authenticate($code);
            CakeSession::write("accessToken",$client->getAccessToken());
            return $this->redirect_uri;
        }
    }

    public function getUser(){
        $accessToken = CakeSession::read("accessToken");
        if(isset($accessToken)) {
            $client = new Google_Client();
            $client->setClientId($this->client_id);
            $client->setClientSecret($this->client_secret);
            $client->setRedirectUri($this->redirect_uri);
            $client->setApprovalPrompt('auto');
            $client->setAccessToken($accessToken);
            $plus = new Google_Service_Oauth2($client);
            $userinfo = $plus->userinfo;
            return $userinfo->get();
        }
    }

}