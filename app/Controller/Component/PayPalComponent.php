<?php
App::import('Vendor', 'paypal_autoload', array('file' => 'merchant-sdk-php-master/PPBootStrap.php'));
class PayPalComponent extends Component {

    public $return_url = null;
    public $error_url  = null;

    public function setOrder($order = array(),$uid_order = NULL){
        if($order && isset($uid_order)) {
            $seller_email = $this->loadSellerEmail($order['shop']['id']);
            if($seller_email) {

                $currencyCode = $order['shop']['currency'];
                $paymentDetails = new PaymentDetailsType();

                $itemTotalValue = 0;
                $taxTotalValue = 0;

                foreach($order['products'] as $product) {
                    $itemDetails            = new PaymentDetailsItemType();
                    $itemDetails->Name      = $product['name'];
                    $itemDetails->Amount    = new BasicAmountType($currencyCode, $product['product_unit']['price_no_tax']);
                    $itemDetails->Quantity  = $product['quantity'];
                    $itemDetails->Tax       = new BasicAmountType($currencyCode, $product['product_unit']['price_tax']);
                    $itemTotalValue         += $product['product_total']['price_no_tax'];
                    $taxTotalValue          += $product['product_total']['price_tax'];
                    $paymentDetails->PaymentDetailsItem[] = $itemDetails;
                }

                $itemDetails            = new PaymentDetailsItemType();
                $itemDetails->Name      = 'Extra';
                $itemDetails->Amount    = new BasicAmountType($currencyCode, $order['payment']['price']);
                $itemDetails->Quantity  = 1;
                $itemTotalValue         += $order['payment']['price'];
                $paymentDetails->PaymentDetailsItem[] = $itemDetails;

                $paymentDetails->ShippingTotal  = new BasicAmountType($currencyCode, $order['shipping_subtotal']);
                $paymentDetails->ItemTotal      = new BasicAmountType($currencyCode, $itemTotalValue);
                $paymentDetails->TaxTotal       = new BasicAmountType($currencyCode, $taxTotalValue);
                $paymentDetails->OrderTotal     = new BasicAmountType($currencyCode, $order['total']);

                $paymentDetails->PaymentAction = "Order";
                $setECReqDetails = new SetExpressCheckoutRequestDetailsType();
                $setECReqDetails->PaymentDetails[0] = $paymentDetails;
                $setECReqDetails->CancelURL = FULL_BASE_URL.$this->error_url;
                $setECReqDetails->ReturnURL = FULL_BASE_URL.$this->return_url;

                if(isset($order['shop']['logo'])){
                    $setECReqDetails->cppheaderimage = $order['shop']['logo'];
                } else {
                    $setECReqDetails->BrandName = $order['shop']['name'];
                }

                $sellerDetails = new SellerDetailsType();
                $sellerDetails->PayPalAccountID = $seller_email;
                $paymentDetails->SellerDetails  = $sellerDetails;

                $setECReqDetails->AllowNote = TRUE;
                $setECReqType               = new SetExpressCheckoutRequestType();
                $setECReqType->SetExpressCheckoutRequestDetails = $setECReqDetails;
                $setECReq = new SetExpressCheckoutReq();
                $setECReq->SetExpressCheckoutRequest = $setECReqType;

                $paypalService = new PayPalAPIInterfaceServiceService(Configuration::getAcctAndConfig());
                $setECResponse = $paypalService->SetExpressCheckout($setECReq);
                if(isset($setECResponse)) {
                    if($setECResponse->Ack =='Success') {
                        $token = $setECResponse->Token;
                        return Configure::read("negozy_config.paypal_url").'/webscr?cmd=_express-checkout&token=' . $token;
                    }
                }
            }
        }
        return false;
    }

    public function doPayment($order = array(),$uid_order = NULL,$token = NULL){
        if($order && isset($token) && isset($uid_order)) {
            $getExpressCheckoutDetailsRequest = new GetExpressCheckoutDetailsRequestType($token);
            $getExpressCheckoutReq = new GetExpressCheckoutDetailsReq();
            $getExpressCheckoutReq->GetExpressCheckoutDetailsRequest = $getExpressCheckoutDetailsRequest;
            $paypalService = new PayPalAPIInterfaceServiceService(Configuration::getAcctAndConfig());
            $getECResponse = $paypalService->GetExpressCheckoutDetails($getExpressCheckoutReq);
            $payerd_id = $getECResponse->GetExpressCheckoutDetailsResponseDetails->PayerInfo->PayerID;
            if(isset($payerd_id)) {

                $orderTotal= new BasicAmountType($order['shop']['currency'], $order['total']);
                $paymentDetails= new PaymentDetailsType();
                $paymentDetails->OrderTotal = $orderTotal;

                $seller_email = $this->loadSellerEmail($order['shop']['id']);
                $sellerDetails = new SellerDetailsType();
                $sellerDetails->PayPalAccountID = $seller_email;
                $paymentDetails->SellerDetails  = $sellerDetails;

                $DoECRequestDetails = new DoExpressCheckoutPaymentRequestDetailsType();
                $DoECRequestDetails->PayerID = $payerd_id;
                $DoECRequestDetails->Token = $token;
                $DoECRequestDetails->PaymentDetails[0] = $paymentDetails;
                $DoECRequest = new DoExpressCheckoutPaymentRequestType();
                $DoECRequest->DoExpressCheckoutPaymentRequestDetails = $DoECRequestDetails;
                $DoECReq = new DoExpressCheckoutPaymentReq();
                $DoECReq->DoExpressCheckoutPaymentRequest = $DoECRequest;
                $DoECResponse = $paypalService->DoExpressCheckoutPayment($DoECReq);
                if($DoECResponse->Ack == "Success") {
                    return $DoECResponse;
                }
            }
        }
        return false;
    }


    private function loadSellerEmail($shop_id = NULL){
        $paymentModel = ClassRegistry::init('Payment');
        $payment_option =  $paymentModel->find("first",array(
            "conditions" => array(
                "shop_id" => $shop_id,
                "name" => "paypal"
            ),
            "fields" => array(
                "value"
            )
        ));

        if($payment_option) {
            return $payment_option['Payment']['value'];
        }
        return null;
    }

}