<?php
class UserSignUpComponent extends Component {

    public $components = array(
        'Sugar',
        'Mandrillapp'
    );


    /**
     * <code>
     * @param array $data['User'] => array(
     *      'first_name'    => 'first_name',
     *      'last_name'     => 'last_name',
     *      'email'         => 'email',
     *      'password'      => 'password'
     *      'source'      => 'source'
     * );
     * </code>
     * @return array
     */
    public function addUser($data = array()){
        $return = array(
            "success"   => false,
            "user_id"   => null,
            "errors"    => array()
        );
        if(isset($data['User'])) {
            $this->User = ClassRegistry::init('User');
            $this->User->set($data);
            if ($this->User->validates()) {
                if ($data) {
                    $data['User']['url'] = $this->_generateUrlName($data['User']['first_name'] . " " . $data['User']['last_name']);
                    $data['User']['token_auth'] = String::uuid();
                    $data['User']['email_confirmed'] = NULL;
                    if ($this->User->saveAll($data)) {
                        $full_name = $data['User']['first_name'] . " " . $data['User']['last_name'];
                        $email = $data['User']['email'];
                        $link = Configure::read('negozy_config.my_url') . "/users/confirm_mail/" . $data['User']['token_auth'];
                        if(Configure::read("sugar_config.enabled")) {
                            $sugarData['first_name']    = $data['User']['first_name'];
                            $sugarData['last_name']     = $data['User']['last_name'];
                            $sugarData['email']         = $data['User']['email'];
                            $sugarData['source']        = $this->source;
                            $lead_id = $this->Sugar->addLead($sugarData);
                            if ($this->User->saveField("lead_id", $lead_id)) {
                                $this->Mandrillapp->send_confirm_account($full_name, $email, $link);
                                $return['success'] = true;
                                $return['user_id'] = $this->User->id;
                            }
                        } else {
                            $this->Mandrillapp->send_confirm_account($full_name, $email, $link);
                            $return['success'] = true;
                            $return['user_id'] = $this->User->id;
                        }
                    }
                }
            }
            $return['errors'] = $this->User->validationErrors;
        }
        return $return;

    }

    protected  function _generateUrlName($fullname = ""){
        if($fullname) {
            $fullname   = strtolower($fullname);
            $url_name   = Inflector::slug($fullname,"_");
            $this->User = ClassRegistry::init('User');
            $exist_url = $this->User->find("first" , array(
                "conditions" => array(
                    "url LIKE" => "%".$url_name."%"
                ),
                "fields" => array(
                    "url"
                ),
                "order" => "url DESC",
                "recursive" => -1
            ));
            if($exist_url) {
                $exist_url = explode("_",$exist_url['User']['url']);
                $next_val = end($exist_url);
                $next_val = $next_val+1;
                $url_name .= "_".$next_val;
            }
            return $url_name;
        }
    }

}