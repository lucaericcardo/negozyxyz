<?php
App::import('Vendor', 'FacebookRedirectLoginHelper', array('file' => 'Facebook'  . DS . 'FacebookRedirectLoginHelper.php'));
App::import('Vendor', 'NegozyFacebookredirectLoginHelper', array('file' => 'Facebook'  . DS . 'NegozyFacebookredirectLoginHelper.php'));
App::import('Vendor', 'FacebookSDKException', array('file' => 'Facebook' . DS . 'FacebookSDKException.php'));
App::import('Vendor', 'FacebookSession', array('file' => 'Facebook' . DS . 'FacebookSession.php'));
App::import('Vendor', 'FacebookRequest', ['file' => 'Facebook' . DS . 'FacebookRequest.php']);
App::import('Vendor', 'FacebookRequestException', ['file' => 'Facebook' . DS . 'FacebookRequestException.php']);
App::import('Vendor', 'FacebookAccessToken', ['file' => 'Facebook' . DS . 'Entities' . DS . 'AccessToken.php']);
App::import('Vendor', 'FacebookHttpable', ['file' => 'Facebook' . DS . 'HttpClients' . DS . 'FacebookHttpable.php']);
App::import('Vendor', 'FacebookCurlHttpClient', ['file' => 'Facebook' . DS . 'HttpClients' . DS . 'FacebookCurlHttpClient.php']);
App::import('Vendor', 'FacebookCurl', ['file' => 'Facebook' . DS . 'HttpClients' . DS . 'FacebookCurl.php']);
App::import('Vendor', 'FacebookRequestException', ['file' => 'Facebook' . DS . 'FacebookRequestException.php']);
App::import('Vendor', 'FacebookAuthorizationException', ['file' => 'Facebook' . DS . 'FacebookAuthorizationException.php']);
App::import('Vendor', 'FacebookResponse', ['file' => 'Facebook' . DS . 'FacebookResponse.php']);
App::import('Vendor', 'FacebookRequest', ['file' => 'Facebook' . DS . 'FacebookRequest.php']);
App::import('Vendor', 'GraphObject', ['file' => 'Facebook' . DS . 'GraphObject.php']);
App::import('Vendor', 'GraphUser', ['file' => 'Facebook' . DS . 'GraphUser.php']);
use Facebook\FacebookSession;
use Facebook\FacebookRedirectLoginHelper;
use Facebook\FacebookRequest;
use Facebook\GraphUser;
class FacebookLoginComponent extends Component {

    private $redirect_url = 'http://my.negozy.com/users/facebook_success';
    private $app_id = '298375923684296';
    private $secret = '827e1bd122ec6ce6ac8e41d000260a03';

    public function initialize(Controller $controller) {
        FacebookSession::setDefaultApplication($this->app_id, $this->secret);
    }

    public function getLoginUrl() {
        $helper = new NegozyFacebookRedirectLoginHelper(
            $this->redirect_url,
            $this->app_id,
            $this->secret
        );
        $helper->disableSessionStatusCheck();
        return  $helper->getLoginUrl(array("email"));

    }

    public function getUser() {
        $helper = new NegozyFacebookRedirectLoginHelper(
            $this->redirect_url,
            $this->app_id,
            $this->secret
        );
       $session = $helper->getSessionFromRedirect();
       if($session) {
           $request = new FacebookRequest( $session, 'GET', '/me' );
           $response = $request->execute();
           $graphObject = $response->getGraphObject();
           return $graphObject;
       }
       return NULL;
    }

}