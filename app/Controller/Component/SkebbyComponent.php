<?php
App::import('Vendor', 'ElephantIOEngineInterface', array('file' => 'ElephantIO'  . DS . 'EngineInterface.php'));
App::import('Vendor', 'ElephantIOClient', array('file' => 'ElephantIO'  . DS . 'Client.php'));
App::import('Vendor', 'ElephantIOPayload', array('file' => 'ElephantIO'  . DS . 'AbstractPayload.php'));
App::import('Vendor', 'ElephantIOAbstractSocketIO', array('file' => 'ElephantIO' . DS . 'Engine' . DS . 'AbstractSocketIO.php'));
App::import('Vendor', 'ElephantIOEngine', array('file' => 'ElephantIO' . DS . 'Engine' . DS . 'SocketIO' . DS . 'Version1X.php'));
App::import('Vendor', 'ElephantIOSession', array('file' => 'ElephantIO' . DS . 'Engine' . DS . 'SocketIO' . DS . 'Session.php'));
App::import('Vendor', 'ElephantIOEncoder', array('file' => 'ElephantIO' . DS . 'Payload' . DS . 'Encoder.php'));
App::import('Vendor', 'ElephantIOServerConnectionFailureException', array('file' => 'ElephantIO'  . DS . 'Exception' . DS. 'ServerConnectionFailureException.php'));
use ElephantIO\Client,
    ElephantIO\Engine\SocketIO\Version1X;

class SkebbyComponent extends Component {

    public function sendSms($phone_number= NULL,$message=NULL){

        $client = new Client(new Version1X('http://nodejshost:3001'));
        $client->initialize();
        $client->emit('send_sms', ["message" => $message,"phone_number" => $phone_number]);
        $client->close();

    }

}