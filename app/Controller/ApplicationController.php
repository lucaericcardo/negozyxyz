<?php
class ApplicationController extends AppController {

    public $uses = array(
        "Application"
    );

    public function beforeFilter(){
        parent::beforeFilter();
        $this->_checkShop();
        $this->_asyncCheckAuth();
        $checkStatus = false;
        if(isset($this->params->plugin) && isset($this->shop_id)) {
            $checkApplication = $this->Application->find("first",
                array(
                    "conditions" => array(
                        "request_uri" => $this->params->plugin
                    ),
                    "fields" => array(
                        "id"
                    ),
                    "contain" => array(
                        "ShopAppInstalled" => array(
                            "conditions" => array(
                                "shop_id" => $this->shop_id,
                                "deleted" => false
                            )
                        )
                    ),
                    "recursive" => -1
                )
            );
            if(count($checkApplication['ShopAppInstalled'])) {
                $checkStatus = true;
            }
        }
        if(!$checkStatus) {
            $this->response->statusCode(401);
            $data['data']['message'] = "Unauthorized";
            $this->set(array(
                'tabs' => $data,
                '_serialize' => array('data')
            ));
            $this->response->send();
            $this->_stop();
        }
    }
}