<?php
class PageCategoriesController extends AppController {

    public $components = array(
        'RequestHandler',
        'Paginator'
    );

    public $uses = array(
        'CategoryPage',
        'CategoryPageContent',
        'ShopLanguage'
    );

    public function beforeFilter() {
        parent::beforeFilter();
        $this->_checkShop();
    }

    public function view_index(){
        $this->response->compress();
        $this->layout = "ajax";
    }

    public function view_add(){
        $this->response->compress();
        $this->layout = "ajax";
    }

    public function view_edit(){
        $this->response->compress();
        $this->layout = "ajax";
    }

    public function view_modal(){
        $this->response->compress();
        $this->layout = "ajax";
    }

    public function get_list($n=10,$page=1,$row="",$orderType="ASC" ,$language_id = null,$parent_id = NULL) {
        $this->response->compress();
        $matrix_languages = array();
        $default_lang = $this->language_id;

        $language_id = (int) $language_id;
        if($language_id) {
            $this->Session->write("language_id", $language_id);
            $default_lang = $language_id;
        }

        $shopLanguages = $this->ShopLanguage->getShopLanguages($this->shop_id);
        foreach($shopLanguages as $k => $v) {
            $matrix_languages[$k] = array();
        }

        $this->Paginator->settings = array(
            "recursive" => -1,
            "conditions" => array(
                "CategoryPage.shop_id"      => $this->shop_id,
                "CategoryPage.parent_id"    => $parent_id
            ),
            "contain" => array(
                "CategoryPageContent" => array(
                    "conditions" => array(
                        "CategoryPageContent.language_id" => array_keys($shopLanguages)
                    ),
                    "fields" => array(
                        "CategoryPageContent.id",
                        "CategoryPageContent.language_id",
                        "CategoryPageContent.title",
                        "CategoryPageContent.timestamp_created"
                    )
                )
            ),
            "fields" => array(
                "CategoryPage.id",
                "CategoryPage.page_count",
                "CategoryPage.children_count",
                "CategoryPage.default",
            ),
            "limit" => $n,
            "page" => $page
        );

        $count = $this->CategoryPage->find("count",array(
            "conditions" => array(
                "CategoryPage.shop_id"      => $this->shop_id,
                "CategoryPage.parent_id"    => $parent_id
            ),
            "recursive" => -1
        ));
        $tmp_categories = $this->Paginator->paginate('CategoryPage');

        $distinct_categories    = array();
        $default_no_categories  = array();
        foreach($tmp_categories as $category) {
            $category_id  = $category['CategoryPage']['id'];
            $distinct_categories[$category_id] = array();
            foreach($category['CategoryPageContent'] as $category_content) {
                if(isset($matrix_languages[$category_content['language_id']])) {
                    $matrix_languages[$category_content['language_id']][$category_content['category_page_id']] = $category_content['id'];
                } else {
                    $matrix_languages[$category_content['language_id']] = array(
                        $category_content['category_page_id'] => $category_content['id']
                    );
                }
                if($default_lang == $category_content['language_id']) {
                    $distinct_categories[$category_id] = array(
                        'title'         => $category_content['title'],
                        'content_id'    => $category_content['id'],
                        'created'       => $category_content['timestamp_created'],
                        'pages'         => $category['CategoryPage']['page_count'],
                        'default'       => $category['CategoryPage']['default'],
                        'children'      => $category['CategoryPage']['children_count']
                    );
                }
                if($this->default_language_id == $category_content['language_id']) {
                    $default_no_categories[$category_id] = array(
                        'title'         => $category_content['title'],
                        'content_id'    => $category_content['id'],
                        'created'       => $category_content['timestamp_created'],
                        'pages'         => $category['CategoryPage']['page_count'],
                        'pages'         => $category['CategoryPage']['default'],
                        'children'      => $category['CategoryPage']['children_count']
                    );
                }
            }
        }
        $categories = array();
        foreach($distinct_categories as $category_id => $default_category) {
            if(!count($default_category)) {
                $tmpRow =array(
                    "category_id"   =>  $category_id,
                    "title"         =>  (isset($default_no_categories[$category_id]) ? $default_no_categories[$category_id]['title'] : '---'),
                    "pages"         =>  (isset($default_no_categories[$category_id])) ? $default_no_categories[$category_id]['pages'] : 0,
                    "children"      =>  (isset($default_no_categories[$category_id])) ? $default_no_categories[$category_id]['children'] : 0,
                    "children"      =>  (isset($default_no_categories[$category_id])) ? $default_no_categories[$category_id]['default'] : null,
                    'content_id'    =>  null,
                    'created'       =>  null,
                    'contents'      =>  array()
                );
            } else {
                $tmpRow =array(
                    "category_id"   =>  $category_id,
                    "title"         =>  $default_category['title'],
                    "pages"         =>  $default_category['pages'],
                    "children"      =>  $default_category['children'],
                    "default"       =>  $default_category['default'],
                    'content_id'    =>  $default_category['content_id'],
                    'created'       =>  $default_category['created'],
                    'contents'      =>  array()
                );
            }
            foreach($matrix_languages as $lang_id => $content) {
                $tmpRow['contents'][] = array(
                    'lang_id'       =>$lang_id,
                    'content_id'    => isset($matrix_languages[$lang_id][$category_id]) ? $matrix_languages[$lang_id][$category_id] : null
                );

            }
            $categories[] = $tmpRow;
        }

        $this->set(array(
            'categories'    => $categories,
            'matrix'        => $matrix_languages,
            'count'         => $count,
            '_serialize'    => array('categories','count','matrix','breadcrumb')
        ));
    }

    public function add(){
        $data = array();
        $data['success'] = false;
        $data['message'] = __("Si è verificato un problema durante la creazione della categoria.");
        if($this->request->is("post")) {
            $page_category_data = array(
                "CategoryPage" => array(
                    'shop_id'   => $this->shop_id,
                    'parent_id' => $this->request->data['category_page']['parent_id']
                )
            );
            $this->CategoryPage->create();
            if($this->CategoryPage->save($page_category_data)) {
                $data['category_id']    = (int)$this->CategoryPage->id;
                $data['success']        = $this->_savePageCategoryContents($this->CategoryPage->id,$this->request->data['language_id'],null);
                $data['message']        = __("Categoria creata con successo!");
            }
        }
        $this->set(array(
            'data' => $data,
            '_serialize' => array('data')
        ));
    }

    public function update() {
        $data = array();
        $data['success'] = false;
        $data['message'] = __("Si è verificato un problema durante il salvataggio della categoria.");
        if($this->request->is("post")) {
            $category_page = $this->CategoryPage->find("first",array(
                "conditions" => array(
                    "CategoryPage.id"        => $this->request->data['category_page']['category_page_id'],
                    "CategoryPage.shop_id"   => $this->shop_id
                ),
                "fields" => array(
                    "CategoryPage.id"
                ),
                "recursive" => -1
            ));
            if($category_page) {
                $category_page_content = $this->CategoryPageContent->find("first",array(
                    "conditions" => array(
                        "id"                => $this->request->data['category_page']['page_content_id'],
                        "language_id"       => $this->request->data['category_page']['language_id'],
                        "category_page_id"  => $this->request->data['category_page']['category_page_id']
                    ),
                    "fields" => array(
                        "id",
                        "uri"
                    ),
                    "recursive" => -1
                ));
                $data['success'] = $this->_savePageCategoryContents($this->request->data['category_page']['category_page_id'],$this->request->data['language_id'],$category_page_content);
                $data['message'] = __("Categoria modificata con successo!");
            }
        }
        $this->set(array(
            'data' => $data,
            '_serialize' => array('data')
        ));
    }

    public function delete() {
        $data = array();
        $data['success'] = false;
        $data['message'] = __("Si è verificato un problema durante la rimozione della categoria.");
        if($this->request->is("post")) {
            $ck_page = $this->CategoryPage->find("count",array(
                "conditions" => array(
                    "id"      => $this->request->data['id'],
                    "shop_id" => $this->shop_id,
                    "default" => NULL
                ),
                "recursive" => -1
            ));
            if($ck_page) {
                if ($data['success'] = (boolean)$this->CategoryPage->delete($this->request->data['id'])) {
                    $data['message'] = __("La categoria è stata cancallata correttamente");
                }
            }
        }
        $this->set(array(
            'data' => $data,
            '_serialize' => array('data')
        ));
    }

    public function load(){
        $this->response->compress();
        if($this->request->is('post') && isset($this->request->data['category_page_id']) && isset($this->request->data['language_id'])) {
            $category_page = $this->CategoryPage->find("first",array(
                "conditions" => array(
                    "CategoryPage.id"        => $this->request->data['category_page_id'],
                    "CategoryPage.shop_id"   => $this->shop_id
                )
            ));

            if($category_page) {
                $categoryPageContent = $this->CategoryPageContent->find("first",array(
                    "conditions" => array(
                        "CategoryPageContent.category_page_id"  => $this->request->data['category_page_id'],
                        "CategoryPageContent.language_id"       => $this->request->data['language_id'],
                    ),
                    "recursive" => -1
                ));
                $content = array(
                    'title'             => '',
                    'uri'               => '',
                    'category_page_id'  => $this->request->data['category_page_id'],
                    'page_content_id'   => null,
                    'language_id'       => $this->request->data['language_id']
                );
                if($categoryPageContent) {
                    $content = array(
                        'title'             => $categoryPageContent['CategoryPageContent']['title'],
                        'uri'               => $categoryPageContent['CategoryPageContent']['uri'],
                        'category_page_id'  => $this->request->data['category_page_id'],
                        'page_content_id'   => $categoryPageContent['CategoryPageContent']['id'],
                        'language_id'       => $this->request->data['language_id']
                    );
                }
                $this->set(array(
                    'content'       => $content,
                    '_serialize'    => array('content')
                ));
            } else {
                $this->response->statusCode(404);
                $data['data']['message'] = "Page not found";
                $this->set(array(
                    'data' => $data,
                    '_serialize' => array('data')
                ));
                $this->response->send();
                $this->_stop();
            }
        } else {
            $this->response->statusCode(404);
            $data['data']['message'] = "Page not found";
            $this->set(array(
                'data' => $data,
                '_serialize' => array('data')
            ));
            $this->response->send();
            $this->_stop();
        }
    }

    public function tree_list(){
        $categories = $this->CategoryPage->get_graphical_tree($this->shop_id,$this->Shop->getDefaultLanguage($this->shop_id));
        $this->set(array(
            'categories'    => $categories,
            '_serialize'    => array('categories')
        ));
    }

    public function delete_all(){
        $data['success'] = false;
        $data['message'] = __("Si è verificato un problema durante la rimozione delle categorie");
        if($this->request->is("post")) {
            $category_page_ids  = $this->request->data["category_page"];
            if($category_page_ids ) {
                $deleted = $this->CategoryPage->deleteAll(
                    array(
                        "CategoryPage.id"       => $category_page_ids ,
                        "CategoryPage.shop_id"  => $this->shop_id,
                        "CategoryPage.default"  => NULL
                    )
                );
                if ($deleted) {
                    $data['success'] = true;
                    $data['message'] = __("Le categorie sono state rimosse con successo");
                }
            }
        }
        $this->set(array(
            'data' => $data,
            '_serialize' => array('data')
        ));
    }

    private function _savePageCategoryContents($category_page_id = null,$language_id=NULL,$content = NULL) {
        if(!$content || ($content['CategoryPageContent']['uri'] != $this->request->data['category_page']['uri'])) {
            $ck_page_category_URL = $this->_ckUniquePageCategoryURL($this->request->data['category_page']['uri']);
            if ($ck_page_category_URL) {
                $this->request->data['category_page']['uri'] = $this->request->data['category_page']['uri'] . "-" . time();
            }
        }
        $category_data = array(
            "CategoryPageContent" => array(
                'language_id'       => $language_id,
                'title'             => $this->request->data['category_page']['title'],
                'uri'               => $this->request->data['category_page']['uri'],
                'category_page_id'  => $category_page_id,
            )
        );
        if($content && isset($content['CategoryPageContent']['id'])) {
            $this->CategoryPageContent->id = $content['CategoryPageContent']['id'];
        } else {
            $this->CategoryPageContent->create();
        }
        if($this->CategoryPageContent->save($category_data)) {
            return true;
        }
        return false;
    }

    private function _ckUniquePageCategoryURL($url = NULL){
        $count_url = false;
        if(isset($url) && !empty($url)) {
            $count_url = $this->CategoryPageContent->find("count",array(
                "conditions" => array(
                    "CategoryPageContent.uri" => $url
                ),
                "joins" => array(
                    array(
                        'table' => 'category_page',
                        'alias' => 'CategoryPage',
                        'type' => 'RIGHT',
                        'conditions' => array(
                            'CategoryPage.id = CategoryPageContent.category_page_id',
                            'CategoryPage.shop_id' => $this->shop_id
                        )
                    )
                ),
                "recursive" => -1
            ));
        }
        return $count_url;
    }


}