<?php
class PagesController extends AppController {

    public $components = array('RequestHandler','Paginator','Image');
    public $uses = array('Page', 'PageContent', 'ShopLanguage', 'Shop', 'CategoryPage', 'CategoryPageContent', 'PageImage');

    public function beforeFilter() {
        parent::beforeFilter();
        $this->_checkShop();
    }

    public function view_index() {
        $this->response->compress();
        $this->layout = "ajax";
    }

    public function view_add() {
        $this->response->compress();
        $this->layout = "ajax";
    }

    public function view_seo() {
        $this->response->compress();
        $this->layout = "ajax";
    }

    public function view_modal_add_category_page() {
        $this->response->compress();
        $this->layout = "ajax";
    }

    public function view_edit() {
        $this->response->compress();
        $this->layout = "ajax";
    }


    /**
     * @param int $n
     * @param int $page
     * @param string $row
     * @param string $orderType
     * @param null $language_id
     * Lista delle pagine
     */
    public function get_list($n=10,$page=1,$row="",$orderType="ASC", $language_id = null) {
        $this->response->compress();
        $matrix_languages = array();
        $default_lang = $this->language_id;
        $language_id = (int) $language_id;

        if($language_id) {
            $this->Session->write("language_id", $language_id);
            $default_lang = $language_id;
        }

        $shopLanguages = $this->ShopLanguage->getShopLanguages($this->shop_id);
        foreach($shopLanguages as $k => $v) {
            $matrix_languages[$k] = array();
        }

        $this->Paginator->settings = array(
            "recursive" => -1,
            "conditions" => array(
                "Page.shop_id" => $this->shop_id
            ),
            "contain" => array(
                "PageContent" => array(
                    "conditions" => array(
                        "PageContent.lang_id" => array_keys($shopLanguages)
                    ),
                    "fields" => array(
                        "PageContent.id",
                        "PageContent.lang_id",
                        "PageContent.title",
                        "PageContent.timestamp_created"
                    )
                ),
                "CategoryPage" => array(
                    "fields" => array(
                        "CategoryPage.id"
                    ),
                    "CategoryPageContent" =>array(
                        "conditions" => array(
                            "CategoryPageContent.language_id" => $this->language_id
                        ),
                        "fields" => array(
                            "CategoryPageContent.title"
                        )
                    )
                )
            ),
            "order" => "Page.order",
            "fields" => array(
                "Page.id",
                "Page.order",
                "Page.default",
                "Page.published",
            ),
            "limit" => $n,
            "page" => $page
        );

        $count = $this->Page->find("count",array(
            "conditions" => array(
                "Page.shop_id" => $this->shop_id
            ),
            "recursive" => -1
        ));
        $tmp_pages = $this->Paginator->paginate('Page');

        $distinct_pages = array();
        $default_no_pages = array();
        foreach($tmp_pages as $page) {
            $idP  = $page['Page']['id'];
            $distinct_pages[$idP] = array();
            foreach($page['PageContent'] as $page_content) {
                if(isset($matrix_languages[$page_content['lang_id']])) {
                    $matrix_languages[$page_content['lang_id']][$page_content['page_id']] = $page_content['id'];
                } else {
                    $matrix_languages[$page_content['lang_id']] = array($page_content['page_id'] => $page_content['id']);
                }
                if($default_lang == $page_content['lang_id']) {
                    $distinct_pages[$idP] = array(
                        'title'         => $page_content['title'],
                        'content_id'    => $page_content['id'],
                        'order'         => $page['Page']['order'],
                        'published'     => $page['Page']['published'],
                        'created'       => $page_content['timestamp_created'],
                        'category_page' => (isset($page['CategoryPage']['CategoryPageContent']) && count($page['CategoryPage']['CategoryPageContent'])) ? $page['CategoryPage']['CategoryPageContent'][0] : null,
                        'default'       => $page['Page']['default']
                    );
                }
                if($this->default_language_id == $page_content['lang_id']) {
                    $default_no_pages[$idP] = array(
                        'title'         => $page_content['title'],
                        'content_id'    => $page_content['id'],
                        'order'         => $page['Page']['order'],
                        'published'     => $page['Page']['published'],
                        'created'       => $page_content['timestamp_created'],
                        'category_page' => (isset($page['CategoryPage']['CategoryPageContent']) && count($page['CategoryPage']['CategoryPageContent'])) ? $page['CategoryPage']['CategoryPageContent'][0] : null,
                        'default'       => $page['Page']['default']
                    );
                }
            }
        }
        $pages = array();
        foreach($distinct_pages as $idPage => $default_page) {
            if(!count($default_page)) {
                $tmpRow =array(
                    "page_id"       => $idPage,
                    "title"         => (isset($default_no_pages[$idPage]) ? $default_no_pages[$idPage]['title'] : '---'),
                    'category_page' => (isset($default_no_pages[$idPage])) ? $default_no_pages[$idPage]['category_page'] : '---',
                    'default'       => (isset($default_no_pages[$idPage])) ? $default_no_pages[$idPage]['default'] : null,
                    'order'         => (isset($default_no_pages[$idPage])) ? $default_no_pages[$idPage]['order'] : null,
                    'published'     => (isset($default_no_pages[$idPage])) ? $default_no_pages[$idPage]['published'] : null,
                    'content_id'    => null,
                    'created'       => null,
                    'contents'      => array()
                );
            } else {
                $tmpRow =array(
                    "page_id"       => $idPage,
                    "title"         => $default_page['title'],
                    'category_page' => $default_page['category_page'],
                    'default'       => $default_page['default'],
                    'order'         => $default_page['order'],
                    'published'     => $default_page['published'],
                    'content_id'    => $default_page['content_id'],
                    'created'       => $default_page['created'],
                    'contents'      => array()
                );
            }
            foreach($matrix_languages as $lang_id=>$content) {
                $tmpRow['contents'][] = array(
                    'lang_id'       => $lang_id,
                    'content_id'    => isset($matrix_languages[$lang_id][$idPage]) ? $matrix_languages[$lang_id][$idPage] : null
                );
            }
            $pages[] = $tmpRow;
        }

        $this->set(array(
            'pages'         => $pages,
            'matrix'        => $matrix_languages,
            'count'         => $count,
            'categories'    => $categories = $this->CategoryPage->get_graphical_tree($this->shop_id,$this->Shop->getDefaultLanguage($this->shop_id)),
            '_serialize'    => array('pages','count','matrix','categories')
        ));
    }

    /**
     * @return null
     * Ordine della pagina automatico
     */
    private function calcOrderPage() {
        $return = NULL;
        $options = array(
            'conditions' => array(
                'shop_id' => $this->shop_id
            ),
            'fields' => array(
                'COALESCE(MAX("order"),-1)+1 as order'
            ),
            'recursive' => -1
        );

        if($order = $this->Page->find('first', $options)) {
            $return = $order[0]['order'];
        }
        return $return;
    }

    /**
     * Aggiunta pagina
     */
    public function add() {
        $data = array();
        $data['success'] = false;
        $data['message'] = __("Si è verificato un problema durante la creazione della pagina.");
        if($this->request->is("post")) {
            $order = (isset($this->request->data['page']['order'])) ? $this->request->data['page']['order'] : $this->calcOrderPage();
            $dataForPage = array(
                "Page" => array(
                    'order'             => $order,
                    'shop_id'           => $this->shop_id,
                    'category_page_id'  => (isset($this->request->data['page']['category_page_id'])) ? $this->request->data['page']['category_page_id'] : null,
                    'published'         => (isset($this->request->data['page']['published'])) ? $this->request->data['page']['published'] : false
                )
            );
            $this->Page->create();
            if($this->Page->save($dataForPage)) {
                $data['page_id'] = $this->Page->id;
                $data['success'] = $this->_savePageContents($data['page_id'],$this->request->data['page']['lang_id']);
                $data['message'] = __("La pagina è stata creata!");
            }
        }
        $this->set(array(
            'data' => $data,
            '_serialize' => array('data')
        ));
    }

    /**
     * Modifica pagina
     */
    public function update() {
        $data = array();
        $data['success'] = false;
        $data['message'] = __("Si è verificato un problema durante il salvataggio della pagina.");
        if($this->request->is("post")) {
            $page = $this->Page->find("first",array(
                "conditions" => array(
                    "Page.id"        => $this->request->data['page']['page_id'],
                    "Page.shop_id"   => $this->shop_id
                ),
                "fields" => array(
                    "Page.id",
                    "Page.default"
                ),
                "recursive" => -1
            ));
            if($page) {
                $this->Page->id = $page['Page']['id'];
                if(!$page['Page']['default']) {
                    $page_data = array(
                        "category_page_id"  => $this->request->data['page']['category_page_id'],
                        "order"             => $this->request->data['page']['order'],
                        'published'         => $this->request->data['page']['published']
                    );
                    $this->Page->save($page_data);
                } else {
                    $page_data = array(
                        "order"             => $this->request->data['page']['order'],
                        'published'         => $this->request->data['page']['published']
                    );
                    $this->Page->save($page_data);
                }
                $page_content = $this->PageContent->find("first",array(
                    "conditions" => array(
                        "PageContent.id"        => $this->request->data['page']['page_content_id'],
                        "PageContent.lang_id"   => $this->request->data['page']['language_id'],
                        "PageContent.page_id"   => $this->request->data['page']['page_id']
                    ),
                    "fields" => array(
                        "id",
                        "uri"
                    ),
                    "recursive" => -1
                ));
                if($this->_savePageContents($this->request->data['page']['page_id'],$this->request->data['page']['language_id'],$page_content)) {
                    $this->_orderImages($this->request->data['images'], $this->request->data['page']['page_id']);
                    $data['success'] = true;
                    $data['message'] = __("Pagina modificata con successo.");
                    $data['page_content_id'] = $this->PageContent->id;
                }

            }
        }
        $this->set(array(
            'data' => $data,
            '_serialize' => array('data')
        ));
    }

    /**
     * Aggiornamento multiplo di pagine
     */
    public function add_to_category(){
        $data['success'] = false;
        $data['message'] = __("Pagine aggiunte alla collezione");
        if($this->request->is("post")) {
            $page_ids       = $this->request->data["pages"];
            $category_id    = $this->request->data["category_id"];
            if($page_ids && $category_id) {
                $category = $this->CategoryPage->find("first",array(
                    "conditions" => array(
                        "id"        => $category_id,
                        "shop_id"   => $this->shop_id
                    ),
                    "fields" => array(
                        "id"
                    ),
                    "recursive" => -1
                ));
                if($category) {
                    $saved = $this->Page->UpdateAll(
                        array("Page.category_page_id" => $category_id),
                        array("Page.id" => $page_ids,"Page.shop_id" => $this->shop_id,"Page.default" => NULL)
                    );
                    if ($saved) {
                        $this->Page->updateCounterCache(array(
                            "category_page_id" => $category_id
                        ));
                        $data['success'] = true;
                        $data['message'] = __("Le pagine sono state aggiunte alla categoria con successo");
                    }
                }
            }
        }
        $this->set(array(
            'data' => $data,
            '_serialize' => array('data')
        ));
    }

    /**
     * Rimozione multipla di pagine
     */
    public function delete_all(){
        $data['success'] = false;
        $data['message'] = __("Si è verificato un problema durante la rimozione delle pagine");
        if($this->request->is("post")) {
            $page_ids       = $this->request->data["pages"];
            if($page_ids) {
                $deleted = $this->Page->deleteAll(
                    array(
                        "Page.id"       => $page_ids,
                        "Page.shop_id"  => $this->shop_id,
                        "NOT"           => array(
                            "Page.default"  => true
                        )
                    )
                );
                if ($deleted) {
                    $data['success'] = true;
                    $data['message'] = __("Le pagine sono state rimosse con successo");
                }
            }
        }
        $this->set(array(
            'data' => $data,
            '_serialize' => array('data')
        ));
    }

    /**
     * @param $page_id
     * @return bool
     * Salvataggio contenuti pagina
     */
    private function _savePageContents($page_id = null,$language_id = null,$page_content = null) {
        if(!$page_content || (isset($page_content['PageContent']) && ($page_content['PageContent']['uri'] != $this->request->data['page']['uri']))) {
            $ck_page_URL = $this->_ckUniquePageURL($this->request->data['page']['uri']);
            if($ck_page_URL) {
                $product_data['url'] = $this->request->data['page']['uri']."-".time();
            }
        }
        $dataForPageContent = array(
            "PageContent" => array(
                'lang_id'           => $language_id,
                'page_id'           => $page_id,
                'title'             => $this->request->data['page']['title'],
                'meta_description'  => isset($this->request->data['page']['meta_description']) ? $this->request->data['page']['meta_description'] : null,
                'meta_keywords'     => isset($this->request->data['page']['meta_keywords']) ? json_encode($this->request->data['page']['meta_keywords']) : null,
                'html'              => isset($this->request->data['page']['html']) ? $this->request->data['page']['html'] : null,
                'uri'               => $this->request->data['page']['uri']
            )
        );
        if($page_content && isset($page_content['PageContent'])) {
            $this->PageContent->id = $page_content['PageContent']['id'];
        } else {
            $this->PageContent->create();
        }
        if($this->PageContent->save($dataForPageContent)) {
            return true;
        }
        return false;
    }


    public function deletePage() {
        $data = array();
        $data['success'] = false;
        $data['message'] = __("Si è verificato un problema durante la rimozione della pagina.");
        if($this->request->is("post")) {
            $ck_page = $this->Page->find("count",array(
                "conditions" => array(
                    "id"        => $this->request->data['idPage'],
                    "shop_id"   => $this->shop_id,
                    "default"   => NULL
                ),
                "recursive" => -1
            ));
            if($ck_page) {
                if ($data['success'] = (boolean)$this->Page->delete($this->request->data['idPage'])) {
                    $data['message'] = __("La pagina è stata cancallata correttamente");
                }
            }
        }
        $this->set(array(
            'data' => $data,
            '_serialize' => array('data')
        ));
    }


    public function savePageCategory() {
        $data['success'] = false;
        $data['message'] = __("Si è verificato un problema durante la creazione della categoria");
        if($this->request->is("post")) {
            $dataToSave = array(
                'CategoryPage' => array(
                    'parent_id' => isset($this->request->data['category']['parent_category_page_id']) ? $this->request->data['category']['parent_category_page_id']: null,
                    'shop_id' => $this->shop_id,
                )
            );
            $this->CategoryPage->create();
            if($this->CategoryPage->save($dataToSave)) {
                $dataToSave = array(
                    'CategoryPageContent' => array(
                        'category_page_id'  => $this->CategoryPage->id,
                        'uri'               => $this->request->data['category']['uri'],
                        'title'             => $this->request->data['category']['title'],
                        'language_id'       => $this->Shop->getDefaultLanguage($this->shop_id)
                    )
                );
                $this->CategoryPageContent->create();
                if($data['success'] = (boolean) $this->CategoryPageContent->save($dataToSave)) {
                    $data['message'] = __("Categoria creata con successo");
                    $data['category_page_id'] = $this->CategoryPage->id;
                }
            }
        }
        $this->set(array(
            'data' => $data,
            '_serialize' => array('data')
        ));
    }

    /**
     * Caricamento dati per la modifica della pagina
     */
    public function loadPageData(){
        $data = array(
            "success"       => false,
            "categories"    => array(),
            "images"        => array()
        );
        if($this->request->is("post")) {
            if(isset($this->request->data['page_id'])) {
                $page = $this->Page->find("first",array(
                    "conditions" => array(
                        "id"        => $this->request->data['page_id'],
                        "shop_id"   => $this->shop_id
                    ),
                    "contain" => array(
                        "PageImage" => array(
                            "fields" => array(
                                "name",
                                "image",
                                "order"
                            ),
                            "order" => "PageImage.order"
                        )
                    )
                ));
                if(!$page) {
                    $this->response->statusCode(404);
                    $data['data']['message'] = "Page not found";
                    $this->set(array(
                        'data' => $data,
                        '_serialize' => array('data')
                    ));
                    $this->response->send();
                    $this->_stop();
                }
                $data['images'] = $page['PageImage'];
            }
            $shop_language = $this->ShopLanguage->find('count', array(
                'conditions'=>array(
                    'language_id'   => $this->request->data["language_id"],
                    'shop_id'       => $this->shop_id
                )
            ));
            if($shop_language) {
                $data['success'] = true;
                $data['categories'] = $this->CategoryPage->get_graphical_tree($this->shop_id,$this->Shop->getDefaultLanguage($this->shop_id));
            }
        }
        $this->set(array(
            'data' => $data,
            '_serialize' => array('data')
        ));
    }

    /**
     * @param null $id
     * Caricamento immagine pagina
     */
    public function loadImage($id = NULL){
        $data = array(
            "success" => false,
            "url"   => false,
            "name"   => false
        ) ;
        if(isset($id) && !empty($id) && isset($_FILES['file'])) {
            $page = $this->Page->find("first",array(
                "recursive" => -1,
                "conditions" => array(
                    "id" => $id,
                    "shop_id" => $this->shop_id
                ),
                "fields" => array(
                    "id"
                )
            ));
            if($page) {
                $types = array();
                $types[] = array("width" => 980, "height" => null, "prefix" => "image_");
                $image_data = $this->Image->uploadImage($_FILES['file'],'shops',$types);
                if ($image_data['success']) {
                    $newImage = array();
                    $newImage['PageImage'] = $image_data['image'];
                    $newImage['PageImage']['page_id'] = $page['Page']['id'];
                    $last_image = $this->PageImage->find("first",array(
                        "conditions" => array(
                            "page_id" => $page['Page']['id']
                        )
                    ));
                    if(!$last_image) {
                        $newImage['PageImage']['order'] = 1;
                        $newImage['PageImage']['primary'] = 1;
                    } else {
                        $newImage['PageImage']['order'] = $last_image['PageImage']['order'] + 1;
                        $newImage['PageImage']['primary'] = 0;
                    }
                    if($this->PageImage->save($newImage)) {
                        $data['success'] = true;
                        $data['url'] = "http://cdn.negozy.com/images/shops/".$image_data['image']['path']."image_".$image_data['image']['name'].".".$image_data['image']['ext'];
                        $data['name'] = $image_data['image']['name'];
                    }
                }
            }
        }
        $this->set(array(
            'data' => $data,
            '_serialize' => array('data')
        ));
    }

    /**
     * @param null $page_id
     * @param null $image
     * Rimozione immagine pagina
     */
    public function removeImage($page_id = null,$image = NULL) {
        $data = array(
            "success" => false
        ) ;
        if(isset($page_id) && isset($image)) {
            $page = $this->Page->find("first",
                array(
                    "conditions" => array(
                        "id"        => $page_id,
                        "shop_id"   => $this->shop_id
                    ),
                    "recursive" => -1
                )

            );
            if(isset($page['Page']['id'])) {
                $page_image = $this->PageImage->find("first",
                    array(
                        "conditions" => array(
                            "PageImage.name"    => $image,
                            "PageImage.page_id" => $page['Page']['id']
                        )
                    )
                );
                if($page_image) {
                    $this->Image->remove_image($page_image['PageImage']['name'].".".$page_image['PageImage']['ext'],"pages/".$page_image['PageImage']['path'],"image_");
                    if($this->PageImage->delete($page_image['PageImage']['id'])) {
                        $images = $this->PageImage->find("all",array(
                            "conditions" => array(
                                "PageImage.page_id" => $page['Page']['id']
                            ),
                            "fileds" => array(
                                "PageImage.id",
                                "PageImage.name"
                            )
                        ));
                        $images = Set::extract("/PageImage/.",$images);
                        $this->_orderImages($images,$page['Page']['id']);
                        $data['success'] = true;
                    }
                }
            }
        }
        $this->set(array(
            'data' => $data,
            '_serialize' => array('data')
        ));
    }

    public function loadPage() {
        $this->response->compress();
        if($this->request->is('post') && isset($this->request->data['page_id']) && isset($this->request->data['language_id'])) {
            $page = $this->Page->find("first",array(
                "conditions" => array(
                    "id"        => $this->request->data['page_id'],
                    "shop_id"   => $this->shop_id
                ),
                "contain" => array(
                    "PageImage" => array(
                        "fields" => array(
                            "name",
                            "image",
                            "order",
                        ),
                        "order" => "PageImage.order"
                    )
                )
            ));

            if($page) {
                $pageContent = $this->PageContent->find("first",array(
                    "conditions" => array(
                        "PageContent.page_id" => $this->request->data['page_id'],
                        "PageContent.lang_id" => $this->request->data['language_id'],
                    ),
                    "recursive" => -1
                ));
                $page_content = array(
                    'title'             => '',
                    'meta_description'  => '',
                    'meta_keywords'     => array(),
                    'html'              => '',
                    'uri'               => '',
                    'order'             => $page['Page']['order'],
                    'published'         => $page['Page']['published'],
                    'category_page_id'  => $page['Page']['category_page_id'],
                    'page_id'           => $this->request->data['page_id'],
                    'page_content_id'   => null,
                    'language_id'       => $this->request->data['language_id']
                );
                if($pageContent) {
                    $page_content = array(
                        'title'             => $pageContent['PageContent']['title'],
                        'meta_description'  => $pageContent['PageContent']['meta_description'],
                        'meta_keywords'     => json_decode($pageContent['PageContent']['meta_keywords'],true),
                        'html'              => $pageContent['PageContent']['html'],
                        'uri'               => $pageContent['PageContent']['uri'],
                        'category_page_id'  => $page['Page']['category_page_id'],
                        'page_content_id'   => $pageContent['PageContent']['id'],
                        'page_id'           => $this->request->data['page_id'],
                        'language_id'       => $this->request->data['language_id'],
                        'order'             => $page['Page']['order'],
                        'published'         => $page['Page']['published'],
                    );
                }
                $categories = $this->CategoryPage->get_graphical_tree($this->shop_id,$this->Shop->getDefaultLanguage($this->shop_id));
                $this->set(array(
                    'page_content'  => $page_content,
                    'categories'    => $categories,
                    'images'        => $page['PageImage'],
                    '_serialize' => array('page_content', 'categories' , 'images')
                ));
            } else {
                $this->response->statusCode(404);
                $data['data']['message'] = "Page not found";
                $this->set(array(
                    'data' => $data,
                    '_serialize' => array('data')
                ));
                $this->response->send();
                $this->_stop();
            }
        }

    }

    /**
     * @param array $images
     * @param null $page_id
     * Ordinamento immagini di pagina
     */
    private function _orderImages($images = array(),$page_id = NULL) {
        if(count($images) && isset($page_id)) {
            $order = 1;
            foreach ($images as $image) {
                $db_image = $this->PageImage->find("first",array(
                    "conditions" => array(
                        "name"      => $image['name'],
                        "page_id"   => $page_id
                    ),
                    "fields" => "id"
                ));
                if($db_image) {
                    $db_image['PageImage']['order']     = $order;
                    $this->PageImage->id = $db_image['PageImage']['id'];
                    if($this->PageImage->save($db_image)) {
                        $order++;
                    }
                }
            }
        }
    }

    /**
     * @param null $url
     * @return bool
     * URI univico della pagina
     */
    private function _ckUniquePageURL($url = NULL){
        $count_url = false;
        if(isset($url) && !empty($url)) {
            $count_url = $this->PageContent->find("count",array(
                "conditions" => array(
                    "PageContent.uri" => $url
                ),
                "joins" => array(
                    array(
                        'table' => 'pages',
                        'alias' => 'Page',
                        'type' => 'RIGHT',
                        'conditions' => array(
                            'Page.id = PageContent.page_id',
                            'Page.shop_id' => $this->shop_id
                        )
                    )
                ),
                "recursive" => -1
            ));
        }
        return $count_url;
    }

}