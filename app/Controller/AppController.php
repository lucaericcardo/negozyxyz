<?php
App::uses('Controller', 'Controller');
class AppController extends Controller {

    public $helpers = array('Form' => array('className' => 'Bs3Helpers.Bs3Form'));
    public $negozy_config;

    public $components = array(
        'Session',
        'Cookie',
        'Auth' => array(
            'loginAction' => array(
                'controller' => 'users',
                'action' => 'signin'
            ),
            'loginRedirect' => array(
                'controller' => 'dashboard'
            ),
            'authError' => 'Did you really think you are allowed to see that?',
            'authenticate' => array(
                'Form' => array(
                    'fields' => array(
                        'username' => 'email',
                        'password' => 'password'),
                    'passwordHasher' => array(
                        'className' => 'Simple',
                        'hashType' => 'sha256'
                    ),
                    'scope' => array('User.email_confirmed != ' => null)
                )
            )
        )
    );

    function beforeFilter() {
        $this->negozy_config = Configure::read('negozy_config');
        $this->Cookie->name = 'negozy';
        $this->Cookie->time =  3600;
        $this->Cookie->path = '/negozy/';
        $this->Cookie->domain = $this->negozy_config['negozy_domain'];
        $this->Cookie->key = '4fBp56Hewsdfpo89!';

        $this->Cookie->httpOnly = true;
        if ($this->params['controller'] == 'pages' ||
            $this->params['controller'] == 'ad' ||
            $this->params['controller'] == 'sps' ||
            $this->params['controller'] == 'carts' ||
            $this->params['action'] == 'recoveryPassword' ||
            $this->params['action'] == 'changePassword'
        ) {
            $this->Auth->allow();
        }
        if($this->params['controller'] == 'landing_page') {
            $this->Auth->allow();
        }

    }


    protected function _asyncCheckAuth() {
        if(!$this->Auth->user("id")) {
            $this->response->statusCode(401);
            $data['data']['message'] = "Unauthorized";
            $this->set(array(
                'tabs' => $data,
                '_serialize' => array('data')
            ));
            $this->response->send();
            $this->_stop();
        }
    }

    protected function _checkShop() {
        $this->shop_id = $this->Session->read("shop_id");
        $this->user_id = $this->Auth->user("id");
        $this->language_id = $this->Session->read("language_id");
        $this->default_language_id = $this->Session->read("default_language_id");
        $ckOwnerShop = false;
        if(isset($this->shop_id) &&  !empty($this->shop_id) && isset($this->user_id) && !empty($this->user_id)) {
            $this->loadModel('Shop');
            $ckOwnerShop = $this->Shop->find("count",array(
                "conditions" => array(
                    "id"        => $this->shop_id,
                    "user_id"   => $this->user_id
                ),
                "recursive" => -1
            ));
        }
        if(!$ckOwnerShop) {
            $this->response->statusCode(401);
            $data['data']['message'] = "Unauthorized";
            $this->set(array(
                'tabs' => $data,
                '_serialize' => array('data')
            ));
            $this->response->send();
            $this->_stop();
        }
    }

    protected function _loadShop() {
        $this->loadModel('Shop');
        return $this->Shop->find("first",array(
            "conditions" => array(
                "user_id" =>$this->Auth->user("id")
            ),
            "contain" => array(
                "ShopLogo" => array(
                    "fields" => array(
                        "ShopLogo.image"
                    )
                )
            ),
            "fields" => array(
                "Shop.url",
                "Shop.name",
                "Shop.site_url",
                "Shop.is_public"
            ),
            "recursive" => -1
        ));
    }

    protected function loadApplicationInstalled(){
        $this->loadModel("ShopAppInstalled");
        return $this->ShopAppInstalled->find("first",
            array(
                "conditions" => array(
                    "ShopAppInstalled.shop_id" => $this->shop_id,
                    "ShopAppInstalled.deleted" => false
                ),
                "contain" => array(
                    "Application" => array(
                        "fields" => array(
                            "request_uri"
                        )
                    )
                ),
                "fields" => "ShopAppInstalled.app_id",
                "recursive" => -1
            )
        );
    }

    protected function getShopCurrency(){
        $shop_currency = $this->Shop->find("first",array(
            "conditions" => array(
                "id" => $this->shop_id
            ),
            "fields" => array(
                "currency"
            ),
            "recursive" => -1
        ));
        $currency = 'EUR';
        if($shop_currency) {
            $currency = $shop_currency['Shop']['currency'];
        }
        return $currency;
    }

    protected function _loadTopBarData(){
        $this->loadModel("User");
        $user = $this->User->find("first",array(
            "contain" => array(
                "UserImage" => array(
                    "fields" => array(
                        "UserImage.image"
                    )
                )
            ),
            "conditions" => array(
                "id" => $this->Auth->user("id")
            ),
            "fields" => array(
                'first_name',
                'last_name',
                'id',
                'email'
            ),
            "recursive" => -1
        ));
        $image = null;
        if(isset($user['UserImage']) && count($user['UserImage'])) {
            $image = reset($user['UserImage']);
            if($image) {
                $image = $image['image'];
            }
        }
        $shop = $this->_loadShop();
        $this->set("shop",$shop);
        $this->set("user",$user['User']);
        $this->set("image",$image);
    }

}
