<?php
class TaxesController extends AppController {

    public $uses = array(
        'ShopTax'
    );

    public $components = array('RequestHandler','UserAction');


    function beforeFilter(){
        parent::beforeFilter();
        $this->_asyncCheckAuth();
        $this->_checkShop();
    }

    public function view_list() {
        $this->response->compress();
        $this->response->expires('+5 days');
        $this->layout = "ajax";
    }

    public function view_add() {
        $this->response->compress();
        $this->response->expires('+5 days');
        $this->layout = "ajax";
    }

    public function view_edit() {
        $this->response->compress();
        $this->response->expires('+5 days');
        $this->layout = "ajax";
    }

    public function loadTax(){
        $this->response->compress();
        $data = array(
            "success" => false
        );
        if ($this->request->is("post")) {
            $tax_id = $this->request->data["tax_id"];
            if($tax_id) {
                $tax = $this->ShopTax->find("first", array(
                    "conditions" => array(
                        "id" => $tax_id,
                        "shop_id" => $this->shop_id
                    )
                ));
                if ($tax) {
                    $data['success'] = true;
                    $shipping_data = array(
                        'id'            => $tax['ShopTax']['id'],
                        "label"         => $tax['ShopTax']['label'],
                        "name"          => $tax['ShopTax']['name'],
                        "product_tax"   => $tax['ShopTax']['product_tax'],
                        "tax"           => $tax['ShopTax']['tax']
                    );
                    $data['tax'] = $shipping_data;
                } else {
                    $this->response->statusCode(404);
                    $data['data']['message'] = "Tax not found";
                    $this->set(array(
                        'data' => $data,
                        '_serialize' => array('data')
                    ));
                    $this->response->send();
                    $this->_stop();
                }
            }
        }
        $this->set(array(
            'data' => $data,
            '_serialize' => array('data')
        ));
    }

    public function add(){
        $data = array(
            "success" => false
        );
        if($this->request->is("post")) {
            $shopTax = $this->request->data("tax");
            $newShopTax['ShopTax'] = array(
                "label" => $shopTax['label'],
                "name" => $shopTax['name'],
                "shop_id" => $this->shop_id,
                "product_tax" => $shopTax['product_tax'],
                "tax" => $shopTax['tax']
            );
            $this->ShopTax->create();
            if($this->ShopTax->saveAll($newShopTax)){
                $data['success'] = true;
                $data['tax_id'] = $this->ShopTax->id;
            }
        }
        $this->set(array(
            'data' => $data,
            '_serialize' => array('data')
        ));
    }

    public function update(){
        $data = array(
            "success" => false
        );
        if($this->request->is("post")) {
            $tax_data = $this->request->data("tax");
            $ckOwner = $this->ShopTax->find("count",array(
                "conditions" => array(
                    "shop_id" => $this->shop_id,
                    "id"      => $tax_data['id']
                )
            ));
            if($ckOwner) {
                $updateTax = array(
                    "id"            => $tax_data['id'],
                    "label"         => $tax_data['label'],
                    "name"          => $tax_data['name'],
                    "product_tax"   => $tax_data['product_tax'],
                    "tax"           => $tax_data['tax']
                );
                if ($this->ShopTax->save($updateTax)) {
                    $data['success'] = true;
                }
            } else {
                $this->response->statusCode(404);
                $data['data']['message'] = "Tax not found";
                $this->set(array(
                    'data' => $data,
                    '_serialize' => array('data')
                ));
                $this->response->send();
                $this->_stop();
            }
        }
        $this->set(array(
            'data' => $data,
            '_serialize' => array('data')
        ));
    }

    public function delete() {
        $data = array(
            "success" => false
        );
        if($this->request->is("post")){
            $shopTaxID = $this->request->data("tax_id");
            if($deleteTax = $this->ShopTax->find("first",array(
                "conditions" => array(
                    "shop_id" => $this->shop_id,
                    "id" => $shopTaxID
                ),
                "fields" => array(
                    "id",
                    "label"
                ),
                "recursive" => -1
            ))){
                if($this->ShopTax->delete($shopTaxID )){
                    $data['success'] = true;
                }
            }
        }
        $this->set(array(
            'data' => $data,
            '_serialize' => array('data')
        ));
    }

    public function index(){
        $taxes = $this->ShopTax->find("all",array(
            "conditions" => array(
                "shop_id" => $this->shop_id
            ),
            "recursive" => -1
        ));

        $taxes_list = array();
        $empty_tax = array(
            "id" => NULL,
            "label" => __("Nessuna imposta"),
            "tax" => 0,
            "product_tax" => false
        );
        $taxes_list[] = $empty_tax;
        foreach($taxes as $tax) {
            $taxes_list[] = array(
                "id" => $tax['ShopTax']['id'],
                "tax" => $tax['ShopTax']['tax'],
                "product_tax" => $tax['ShopTax']['product_tax'],
                "name" => $tax['ShopTax']['name'],
                "label" => $tax['ShopTax']['label']
            );
        }
        $this->set(array(
            'taxes' => $taxes_list,
            '_serialize' => array('taxes')
        ));
    }


}