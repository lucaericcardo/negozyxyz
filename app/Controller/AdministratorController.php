<?php
class AdministratorController extends AppController {

    public $uses = array(
        'AdvTracking',
        'User',
        'Macrocategory',
        'Category',
        'Subcategory'
    );

    public $components = array('RequestHandler','Paginator','Mandrillapp');

    public function view_dashboard() {
        $this->layout = "ajax";
    }

    public function view_users_list() {
        $this->layout = "ajax";
    }

    public function view_macrocategories() {
        $this->layout = "ajax";
    }

    public function view_add_macrocategory() {
        $this->layout = "ajax";
    }

    public function view_categories() {
        $this->layout = "ajax";
    }

    public function view_add_categories() {
        $this->layout = "ajax";
    }

    public function view_subcategories() {
        $this->layout = "ajax";
    }
    public function view_add_subcategories() {
        $this->layout = "ajax";
    }

    public function loadData(){
        $pieData = $this->_getDataPieChart();
        $users = $this->_getUsers();
        $this->set(array(
            'pieData'   => $pieData,
            'users'     => $users,
            '_serialize' => array('pieData','users')
        ));
    }

    public function enableUser() {
        $data['success'] = false;
        if($this->request->is("post")) {
            $user_id = $this->request->data["user_id"];
            $user = $this->User->find("first",array(
                "conditions" => array(
                    "id" => $user_id,
                    "confirmed" => false
                ),
                "fields" => array(
                    "fullname",
                    "email",
                    "id"
                ),
                "recursive" => -1
            ));
            if($user) {
                $this->User->id = $user['User']['id'];
                if($this->User->saveField('confirmed',true)){
                    $data['success'] = true;
                    $this->Mandrillapp->send_enable_account($user['User']['fullname'],$user['User']['email']);
                }
            }
        }
        $this->set(array(
            'data'     => $data,
            '_serialize' => array('data')
        ));
    }

    public function loadUsers($n=10,$page=1,$row="",$orderType=""){

        $this->Paginator->settings = array(
            "recursive" => -1,
            "contain" => array(
                "UserImage"
            ),
            "fields" => array(
                'User.first_name',
                'User.last_name',
                'User.email',
                'User.created',
                'User.confirmed',
                'User.email_confirmed',
            ),
            "order" => $row." ".$orderType,
            "limit" => $n,
            "page" => $page
        );

        $count = $this->User->find("count",array(
            "recursive" => -1
        ));
        $tmp_users = $this->Paginator->paginate('User');
        $users = array();
        foreach($tmp_users as $user) {
            $tmp_user = $user['User'];
            $tmp_user['image'] = (isset($user['UserImage'][0]['image'])) ? $user['UserImage'][0]['image'] : null;
            $users[] = $tmp_user;
        }
        $this->set(array(
            'users' => $users,
            'count' => $count,
            '_serialize' => array('users','count')
        ));

    }

    private function _getUsers() {
        $users = $this->User->find("count",array(
            'recursive' => -1
        ));
        return $users;
    }

    private function _getDataPieChart(){
        $return = array();
        $data = $this->AdvTracking->find('all',array(
                'fields' => array(
                    'COUNT(AdvTracking.source) AS count',
                    'AdvTracking.source'
                ),
                'group' => 'AdvTracking.source'

            )
        );
        foreach($data as $track) {
            $return[] = array(
                'label' => $track['AdvTracking']['source'],
                'value' => $track[0]['count']
            );
        }
        return $return;
    }

    public function loadMacrocategories() {

        $macrocategories = $this->Macrocategory->find("all",array(
            "recursive" => -1,
            "conditions" => array(
                "shop_id" => NULL,
                "default" => true
            )
        ));
        $macrocategories = Set::extract('/Macrocategory/.',$macrocategories);
        $this->set(array(
            'macrocategories'   => $macrocategories,
            '_serialize' => array('macrocategories')
        ));
    }

    public function saveMacrocategories (){
        $data['success'] = false;
        if($this->request->is("post")) {
            $macrocategories = $this->request->data["macrocategories"];
            $macrocategories = explode("\n",$macrocategories);
            foreach($macrocategories as $macrocategory) {
                $this->Macrocategory->create();
                $newMacro = array(
                    'label' => $macrocategory,
                    'default' => true,
                    'shop_id' => null,
                    'deleted' => false,
                    'reference_id' => null
                );
                $this->Macrocategory->save($newMacro);
            }
            $data['success'] = true;
        }
        $this->set(array(
            'data'   => $data,
            '_serialize' => array('data')
        ));
    }

    public function loadCategories() {
        $categories = $this->Category->find("all", array(
            "conditions" => array(
                "shop_id" => NULL,
                "default" => true
            ),
            "recursive" => -1
        ));
        $categories = Set::extract('/Category/.',$categories);
        $this->set(array(
            'categories'   => $categories,
            '_serialize' => array('categories')
        ));
    }

    public function saveCategories (){
        $data['success'] = false;
        if($this->request->is("post")) {
            $categories = $this->request->data["categories"];
            $categories = explode("\n",$categories);
            foreach($categories as $category) {
                $this->Category->create();
                $newCat = array(
                    'label' => $category,
                    'default' => true,
                    'shop_id' => null,
                    'deleted' => false,
                    'reference_id' => null,
                    'macrocategory_id' => $this->request->data["macrocategory_id"]
                );
                $this->Category->save($newCat);
            }
            $data['success'] = true;
        }
        $this->set(array(
            'data'   => $data,
            '_serialize' => array('data')
        ));
    }

    public function loadSubcategories() {
        $subcategories = $this->Subcategory->find("all", array(
            "conditions" => array(
                "shop_id" => NULL,
                "default" => true
            ),
            "recursive" => -1
        ));
        $subcategories = Set::extract('/Subcategory/.',$subcategories);
        $this->set(array(
            'subcategories'   => $subcategories,
            '_serialize' => array('subcategories')
        ));
    }

    public function saveSubcategories (){
        $data['success'] = false;
        if($this->request->is("post")) {
            $subcategories = $this->request->data["subcategories"];
            $subcategories = explode("\n",$subcategories);
            foreach($subcategories as $subcategory) {
                $this->Subcategory->create();
                $newCat = array(
                    'label' => $subcategory,
                    'default' => true,
                    'shop_id' => null,
                    'deleted' => false,
                    'reference_id' => null,
                    'category_id' => $this->request->data["category_id"]
                );
                $this->Subcategory->save($newCat);
            }
            $data['success'] = true;
        }
        $this->set(array(
            'data'   => $data,
            '_serialize' => array('data')
        ));
    }

}