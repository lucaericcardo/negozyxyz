<?php

class TranslateController extends AppController {
    public $uses = array('Product');
    public function beforeFilter() {
        parent::beforeFilter();
        //$this->_checkShop();
    }
    public function index($shId=null) {
        $shId = ($shId) ? $shId : $this->shop_id;
        $this->layout = 'ajax';
        $this->autoRender=false;
        $this->Translate = $this->Components->load('Translate');
        $products = $this->Product->find("all",array(
            "conditions" => array(
                "Product.shop_id" => $shId,
                "Product.deleted" => false
            ),
            "fields" => array(
                "Product.id",
                "Product.name",
                "Product.description"
            ),
            "recursive" => -1
        ));
        $documents = array();
        $words = array();
        foreach($products as $p) {
            $documents[] = array('text'=>$p['Product']['description'], 'source' => array('shop_id' => $shId, 'product_id' => $p['Product']['id']));
            $words[] = array('text'=>$p['Product']['name'], 'source' => array('shop_id' => $shId, 'product_id' => $p['Product']['id']));
        }

        $this->Translate->init('italian', 'english', $documents, $words);
        $this->Translate->save();
        $response = $this->Translate->requestQuote();
        var_dump($response);
        echo $this->Translate->generateDocumentTranslated();
        //DOTO nella request quote va fatto il salvataggio dei dati relativi alla quote
    }
}