<?php
class NotifiesController extends  AppController {

    public $components = array(
        'RequestHandler',
        'Notifies',
        'RTN',
        'NotifiesTemplate',
        'Checkout',
    );

    public function beforeFilter(){
        $this->_asyncCheckAuth();
    }

    public function view_shop_notifies() {
        $this->layout = "ajax";
    }

    public function index(){
        $this->loadModel('User');
        $options = array(
            'conditions' => array(
                'User.id' => $this->Auth->user('id')
            ),
            'fields' => array(
                'User.shop_notify_count',
                'User.user_notify_count'
            ),
            'recursive' => -1
        );
        $user = $this->User->find('first', $options);
        $notifies = array(
            'shop_notify_count' => 0,
            'user_notify_count' => 0
        );
        if(count($user)) {
            $notifies = array(
                'shop_notify_count' => $user['User']['shop_notify_count'],
                'user_notify_count' => $user['User']['user_notify_count']
            );
        }
        $this->set(array(
            'notifies' => $notifies,
            '_serialize' => array('notifies')
        ));
    }

    public function shop_notifies() {
        $this->_checkShop();
        $notifies = $this->Notifies->getShopNotifies($this->Auth->user('id'));
        $this->Notifies->viewShopNotifies($this->Auth->user('id'));
        $this->set(array(
            'notifies'      => $notifies,
            '_serialize'    => array('notifies')
        ));
    }

    public function shop_timeline(){
        $return = array();
        $this->ShopNotify = ClassRegistry::init('ShopNotify');
        $options = array(
            "conditions" => array(
                "receiver_id" => $this->Auth->user("id"),
            ),
            "order" => array("ShopNotify.created" => -1)
        );
        $notifies = $this->ShopNotify->find('all', $options);
        $this->loadModel("User");
        $this->loadModel("Order");
        foreach($notifies as $notify) {
            $date = date("d F Y",$notify['ShopNotify']['created']->sec);
            $time = date("H:i",$notify['ShopNotify']['created']->sec);
            if(!isset($return[$date])) {
                $return[$date] = array();
            }
            $customer = $this->User->find("first",array(
                "fields" => array(
                    "first_name",
                    "last_name"
                ),
                "conditions" => array(
                    "id" => $notify['ShopNotify']['sender_id']
                ),
                "recursive" => -1
            ));
            $order = NULL;
            if(isset($notify['ShopNotify']['order_id'])) {
                $order = $this->Order->find("first", array(
                    "fields" => array(
                        "id",
                        "order_uid"
                    ),
                    "conditions" => array(
                        "id" => $notify['ShopNotify']['order_id']
                    ),
                    "recursive" => -1
                ));
            }
            $link = '#';
            if($order) {
                if($notify['ShopNotify']['type'] == 2) {
                    $link = '#/orders/'.$order['Order']['id'];
                } else if ($notify['ShopNotify']['type'] == 0){
                    $link = '#/orders/request/'.$order['Order']['id'];
                }
            }
            $return[$date][] = array(
                "from"          => $customer['User']['first_name']." ".$customer['User']['last_name'],
                "message"       => $notify['ShopNotify']['message'],
                "type"          => $notify['ShopNotify']['type'],
                "time"          => $time,
                "order_uid"     => ($order) ? $order['Order']['order_uid'] : false,
                "link"          => $link
            );
        }
        $data = array();
        foreach ($return as $date => $values) {
            $data[] = array(
                "date"      => $date,
                "values"    => $values
            );
        }
        $this->set(array(
            'notifies' => $data,
            '_serialize' => array('notifies')
        ));
    }

    public function user_notifies() {
        $this->_checkShop();
        $notifies = $this->Notifies->getUserNotifies($this->Auth->user('id'));
        $this->Notifies->viewUserNotifies($this->Auth->user('id'));
        $this->set(array(
            'notifies'      => $notifies,
            '_serialize'    => array('notifies')
        ));
    }

    public function user_timeline(){
        $return = array();
        $this->UserNotify = ClassRegistry::init('UserNotify');
        $options = array(
            "conditions" => array(
                "receiver_id" => $this->Auth->user("id"),
            ),
            "order" => array("created" => -1),
            "recursive" => -1
        );
        $notifies = $this->UserNotify->find('all', $options);
        $this->loadModel("Shop");
        $this->loadModel("Order");
        foreach($notifies as $notify) {
            $date = date("d F Y",$notify['UserNotify']['created']->sec);
            $time = date("H:i",$notify['UserNotify']['created']->sec);
            if(!isset($return[$date])) {
                $return[$date] = array();
            }
            $shop = $this->Shop->find("first",array(
                "fields" => array(
                    "name"
                ),
                "conditions" => array(
                    "id" => $notify['UserNotify']['sender_id']
                ),
                "recursive" => -1
            ));
            $order = NULL;
            if(isset($notify['UserNotify']['order_id'])) {
                $order = $this->Order->find("first", array(
                    "fields" => array(
                        "id",
                        "order_uid"
                    ),
                    "conditions" => array(
                        "id" => $notify['UserNotify']['order_id']
                    ),
                    "recursive" => -1
                ));
            }
            $link = '#';
            if($order) {
                $link = '#/order/'.$order['Order']['id'];
            }
            $return[$date][] = array(
                "from"          => $shop['Shop']['name'],
                "message"       => $notify['UserNotify']['message'],
                "type"          => $notify['UserNotify']['type'],
                "time"          => $time,
                "order_uid"     => ($order) ? $order['Order']['order_uid'] : false,
                "link"          => $link
            );
        }
        $data = array();
        foreach ($return as $date => $values) {
            $data[] = array(
                "date"      => $date,
                "values"    => $values
            );
        }
        $this->set(array(
            'notifies' => $data,
            '_serialize' => array('notifies')
        ));
    }

    public function getCountNotififies() {
    }

    /* RTN Section **/
    public function getAuthRealtimeNotify() {
        $dataPayload = array();
        $dataPayload['user']['id'] = $this->Auth->user('id');
        $dataPayload['date'] = time();
        $dataPayload['verme'] = "N3g0Z4";
        $data = array();
        $data['hash'] = Security::hash(json_encode($dataPayload), 'md5', false);
        $data['user'] = $dataPayload['user'];
        $data['date'] = $dataPayload['date'];
        $this->set(array(
            'data' => $data,
            '_serialize' => array('data')
        ));
    }
}