<?php
class ShipmentsController extends AppController {

    public $uses = array(
        'Shipping',
        'ShippingGroup',
        'ShopTax',
        'Country'
    );

    public $components = array('RequestHandler','PriceHelper','UserAction');

    function beforeFilter(){
        parent::beforeFilter();
        $this->_asyncCheckAuth();
        $this->_checkShop();
    }

    public function view_list() {
        $this->response->compress();
        $this->layout = "ajax";
    }

    public function view_add() {
        $this->response->compress();
        $this->layout = "ajax";
    }

    public function view_edit() {
        $this->response->compress();
        $this->layout = "ajax";
    }

    public function loadShipping(){
        $this->response->compress();
        $data = array(
            "success" => false
        );
        if ($this->request->is("post")) {
            $shipping_id = $this->request->data["shipping_id"];
            if($shipping_id) {
                $shipping = $this->Shipping->find("first", array(
                    "conditions" => array(
                        "Shipping.id" => $shipping_id,
                    ),
                    "joins" => array(
                        array(
                            'table' => 'shipping_groups',
                            'alias' => 'ShippingGroups',
                            'type' => 'RIGHT',
                            'conditions' => array(
                                'ShippingGroups.id = Shipping.shipping_group_id',
                                'ShippingGroups.shop_id' => $this->shop_id
                            )
                        )
                    ),
                    "contain" => array(
                        "ShopTax",
                    ),
                    "recursive" => -1
                ));
                if ($shipping) {
                    $data['success'] = true;
                    $shipping_data = array(
                        'id'                => $shipping['Shipping']['id'],
                        'additional_price'  => $shipping['Shipping']['additional_price'],
                        'price'             => $shipping['Shipping']['price'],
                        'start_day'         => $shipping['Shipping']['start_day'],
                        'end_day'           => $shipping['Shipping']['end_day'],
                        'shop_tax_id'       => $shipping['Shipping']['shop_tax_id'],
                    );
                    $data['shipping'] = $shipping_data;
                } else {
                    $this->response->statusCode(404);
                    $data['data']['message'] = "Shipping not found";
                    $this->set(array(
                        'data' => $data,
                        '_serialize' => array('data')
                    ));
                    $this->response->send();
                    $this->_stop();
                }
            }
        }
        $this->set(array(
            'data' => $data,
            '_serialize' => array('data')
        ));
    }

    public function add(){
        $data = array(
            "success" => false
        );
        if($this->request->is("post") && isset($this->request->data['shipping'])){
            $shipping_data = $this->request->data['shipping'];
            $checkShippingGroup = $this->ShippingGroup->find("count",array(
                "conditions" => array(
                    "shop_id"   => $this->shop_id,
                    "id"        => $shipping_data['shipping_group_id']
                ),
                "recursive"     => -1
            ));
            if($checkShippingGroup) {
                $newShipping['Shipping'] = array(
                    "price"             => $shipping_data['price'],
                    "additional_price"  => $shipping_data['additional_price'],
                    "shop_tax_id"       => $shipping_data['shop_tax_id'],
                    "start_day"         => $shipping_data['start_day'],
                    "end_day"           => $shipping_data['end_day'],
                    "shipping_group_id" => $shipping_data['shipping_group_id'],
                    'country_type'      => $shipping_data['country_type']
                );
                if($shipping_data['countries'] && $shipping_data['country_type'] == 1) {
                    $newShipping['Country'] = $shipping_data['countries'];
                } else {
                    $newShipping['Country'] = array();
                }
                if($this->Shipping->save($newShipping)){
                    $currency = $this->getShopCurrency();
                    $shop_tax = $this->ShopTax->find("first",array(
                        "conditions" => array(
                            "id" => $shipping_data['shop_tax_id']
                        ),
                        "recursive" => -1
                    ));
                    $countries = $this->Country->find("all",array(
                        "conditions" => array(
                            "id" => $shipping_data['countries']
                        ),
                        "fields" => array(
                            "alpha"
                        ),
                        "recursive" => -1
                    ));
                    $countries = Set::extract('/Country/alpha', $countries);
                    $tax = (isset($shop_tax['ShopTax'])) ? $shop_tax['ShopTax'] : null;
                    $shop_tax_label = (isset($tax['label'])) ? $tax['label'] : '';
                    $price = $this->PriceHelper->format_price($shipping_data['price'],$tax);
                    $additional_price = $this->PriceHelper->format_price($shipping_data['additional_price'],$tax);
                    $data = array(
                        "success" => true,
                        "shipping" => array(
                            'id'                => $this->Shipping->id,
                            'price'             => CakeNumber::currency($price,$currency),
                            'additional_price'  => CakeNumber::currency($additional_price,$currency),
                            'start_day'         => $shipping_data['start_day'],
                            'end_day'           => $shipping_data['end_day'],
                            'tax'               => $shop_tax_label,
                            'countries'         => $countries,
                            'country_type'      => $shipping_data['country_type']
                        )
                    );
                }
            }
        }

        $this->set(array(
            'data' => $data,
            '_serialize' => array('data')
        ));
    }

    public function update(){
        $data = array(
            "success" => false
        );
        if($this->request->is("post")) {
            $shipping_data = $this->request->data("shipping");
            $shipping = $this->Shipping->find("first", array(
                "conditions" => array(
                    "Shipping.id" => $shipping_data['id'],
                ),
                "joins" => array(
                    array(
                        'table' => 'shipping_groups',
                        'alias' => 'ShippingGroups',
                        'type' => 'RIGHT',
                        'conditions' => array(
                            'ShippingGroups.id = Shipping.shipping_group_id',
                            'ShippingGroups.shop_id' => $this->shop_id
                        )
                    )
                ),
                "contain" => array(
                    "ShopTax",
                    "Country"
                ),
                "recursive" => -1
            ));
            if($shipping) {
                $newShipping['Shipping'] = array(
                    "id"                => $shipping_data['id'],
                    "price"             => $shipping_data['price'],
                    "additional_price"  => $shipping_data['additional_price'],
                    "start_day"         => $shipping_data['start_day'],
                    "end_day"           => $shipping_data['end_day'],
                    "shop_tax_id"       => $shipping_data['shop_tax_id'],
                );
                if ($this->Shipping->save($newShipping)) {
                    $currency = $this->getShopCurrency();
                    $shop_tax = $this->ShopTax->find("first",array(
                        "conditions" => array(
                            "id" => $shipping_data['shop_tax_id']
                        ),
                        "recursive" => -1
                    ));
                    $countries = Set::extract('/Country/alpha', $shipping);
                    $tax = (isset($shop_tax['ShopTax'])) ? $shop_tax['ShopTax'] : null;
                    $shop_tax_label = (isset($tax['label'])) ? $tax['label'] : '';
                    $price = $this->PriceHelper->format_price($shipping_data['price'],$tax);
                    $additional_price = $this->PriceHelper->format_price($shipping_data['additional_price'],$tax);
                    $data = array(
                        "success" => true,
                        "shipping" => array(
                            'id'                => $shipping_data['id'],
                            'price'             => CakeNumber::currency($price,$currency),
                            'additional_price'  => CakeNumber::currency($additional_price,$currency),
                            'start_day'         => $shipping_data['start_day'],
                            'end_day'           => $shipping_data['end_day'],
                            'tax'               => $shop_tax_label,
                            'countries'         => $countries,
                            'country_type'      => $shipping['Shipping']['country_type']
                        )
                    );
                }
            } else {
                $this->response->statusCode(404);
                $data['data']['message'] = "Shipping not found";
                $this->set(array(
                    'data' => $data,
                    '_serialize' => array('data')
                ));
                $this->response->send();
                $this->_stop();
            }
        }
        $this->set(array(
            'data' => $data,
            '_serialize' => array('data')
        ));
    }

    public function delete() {
        $data = array(
            "success" => false
        );
        if($this->request->is("post")){
            $shippingId = $this->request->data("shipping_id");
            $shipping = $this->Shipping->find("first",array(
                "conditions" => array(
                    "Shipping.id" => $shippingId
                ),
                "joins" => array(
                    array(
                        'table' => 'shipping_groups',
                        'alias' => 'ShippingGroups',
                        'type' => 'RIGHT',
                        'conditions' => array(
                            'ShippingGroups.id = Shipping.shipping_group_id',
                            'ShippingGroups.shop_id' => $this->shop_id
                        )
                    )
                ),
                "recursive" => -1
            ));
            if($shipping){
                if($this->Shipping->delete($shippingId)){
                    $data['success'] = true;
                }
            } else {
                $this->response->statusCode(404);
                $data['data']['message'] = "Shipping not found";
                $this->set(array(
                    'data' => $data,
                    '_serialize' => array('data')
                ));
                $this->response->send();
                $this->_stop();
            }
        }
        $this->set(array(
            'data' => $data,
            '_serialize' => array('data')
        ));
    }
}