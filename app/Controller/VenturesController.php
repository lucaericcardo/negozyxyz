<?php
class VenturesController extends AppController {

    public $uses = array(
        'User',
        'Venture'
    );

    public function beforeFilter() {
        parent::beforeFilter();
        if(!$this->Auth->user()){
            $this->redirect("http://it.negozy.com/signin");
        }
        if($this->Auth->user("pending_token")) {
            $this->redirect("http://it.negozy.com/");
        }
        $this->_loadTopBarData();
    }

    public function index(){
        $this->layout = "venture";
        $this->render('empty');
    }

    public function get_list(){
        $ventures = array();
        if($this->shop_id){
            /**
             * Utente con shop
             */
            $ventures = $this->Venture->find("all",array(
                "conditions" => array(
                    "AND" => array(
                        "with_shop"     => TRUE,
                        "without_shop"  => FALSE,
                        "broadcast"     => TRUE
                    )
                ),
                "recursive" => -1
            ));
        } else {
            /**
             * Utente senza shop
             */
            $ventures = $this->Venture->find("all",array(
                "conditions" => array(
                    "AND" => array(
                        "with_shop"     => FALSE,
                        "without_shop"  => TRUE,
                        "broadcast"     => TRUE
                    )
                ),
                "joins" => array(
                    array(
                        'table' => 'ventures_users',
                        'alias' => 'VentureUsers',
                        'type' => 'RIGHT',
                        'conditions'=> array(
                            'VentureUsers.user_id = User.id'
                        )
                    ),
                ),
                "recursive" => -1
            ));
        }
    }

    public function view_list(){
        $this->layout = "ajax";
    }

    public function view_details(){
        $this->layout = "ajax";
    }



}