<?php
class ThemesController extends AppController {

    public $uses = array(
        "ThemeSite",
        "ThemeIcon"
    );

    public $components = array('RequestHandler');

    public function beforeFilter() {
        parent::beforeFilter();
        $this->_checkShop();
    }

    public function view_list(){
        $this->layout = "ajax";
    }

    public function view_theme(){
        $this->layout = "ajax";
    }

    public function load_theme(){
        $data['success'] = false;
        $data['theme']   = array();
        $data['theme_active']  = null;
        if($this->request->is("post") && isset($this->request->data["theme_uri"])) {
            $data['theme_active'] = 1;
            $this->loadModel("Shop");
            $shop = $this->Shop->find("first",array(
                "conditions" => array(
                    "id" => $this->shop_id
                ),
                "fields" => array(
                    "theme_id"
                ),
                "recursive" => -1
            ));
            if($shop['Shop']['theme_id']) {
                $data['theme_active'] = $shop['Shop']['theme_id'];
            }
            $theme = $this->ThemeSite->find("first",array(
               "recursive" => -1,
                "conditions" => array(
                    "uri" => $this->request->data["theme_uri"]
                ),
                "fields" => array(
                    "id",
                    "name",
                    "uri",
                    "description"
                ),
                "contain" => array(
                    "ThemeIcon" => array(
                        "fields" => array(
                            "id",
                            "name",
                            "path",
                            "ext"
                        )
                    ),
                    "ThemeScreenshot" => array(
                        "fields" => array(
                            "id",
                            "name",
                            "path",
                            "ext"
                        )
                    )
                ),
            ));
            if($theme) {
                $icon = "";
                if(isset($theme['ThemeIcon']['id'])) {
                    $icon = "http://cdn.negozy.com/images/themes/".$theme['ThemeIcon']['name'].".".$theme['ThemeIcon']['ext'];
                }
                $screenshots = array();
                if(count($theme['ThemeScreenshot'])) {
                    foreach($theme['ThemeScreenshot'] as $screenshot) {
                        $screenshots[] = "http://cdn.negozy.com/images/themes/screenshot/".$screenshot['name'].".".$screenshot['ext'];
                    }
                }
                $data['theme'] = array(
                    "id" => $theme['ThemeSite']['id'],
                    "name" => $theme['ThemeSite']['name'],
                    "description" => $theme['ThemeSite']['description'],
                    "uri"   => $theme['ThemeSite']['uri'],
                    "icon" => $icon,
                    "screenshots" => $screenshots
                );
            }
        }

        $this->set(array(
            'data' => $data,
            '_serialize' => array('data')
        ));

    }

    public function get_list(){
        $theme_install = 1;
        $this->loadModel("Shop");
        $shop = $this->Shop->find("first",array(
            "conditions" => array(
                "id" => $this->shop_id
            ),
            "fields" => array(
                "theme_id"
            ),
            "recursive" => -1
        ));
        if($shop['Shop']['theme_id']) {
            $theme_install = $shop['Shop']['theme_id'];
        }
        $themes = $this->ThemeSite->find("all",array(
            "recursive" => -1,
            "fields" => array(
                "id",
                "name",
                "uri"
            ),
            "contain" => array(
                "ThemeIcon" => array(
                    "fields" => array(
                        "id",
                        "name",
                        "path",
                        "ext"
                    )
                )
            ),
            "order" => "ThemeSite.id ASC"
        ));
        $return = array();
        foreach($themes as $theme) {
            $icon = "";
            if(isset($theme['ThemeIcon']['id'])) {
                $icon = "http://cdn.negozy.com/images/themes/".$theme['ThemeIcon']['name'].".".$theme['ThemeIcon']['ext'];
            }
            $return[] = array(
                "id" => $theme['ThemeSite']['id'],
                "name" => $theme['ThemeSite']['name'],
                "uri"   => $theme['ThemeSite']['uri'],
                "icon" => $icon
            );
        }
        $this->set(array(
            'themes' => $return,
            "active" => $theme_install,
            '_serialize' => array('themes','active')
        ));

    }

    public function enable_theme(){
        $data['success'] = FALSE;
        $data['message'] = __("Non è stato possibile abilitare il tema");
        if($this->request->is("post") && isset($this->request->data["theme_id"])) {
            $this->loadModel("Shop");
            $this->Shop->id = $this->shop_id;
            if($this->Shop->saveField("theme_id",$this->request->data["theme_id"])){
                $data['success'] = TRUE;
                $data['message'] = __("Il tema è stato abilitato con sucesso");
            }
        }
        $this->set(array(
            'data' => $data,
            '_serialize' => array('data')
        ));
    }

}