<?php
class NewsletterController extends AppController {

    public $uses = array("Newsletter");
    public $components = array('Mandrillapp');

    public function index(){
        return;
        $this->layout = "ajax";
        $this->autoRender = false;
        $emails = $this->Newsletter->find("all",array(
            "fields" => array(
                "email"
            )
        ));
        $toArray = array();
        foreach ($emails as $k => $email) {
            $toArray[] = array(
                "email" => $email['Newsletter']['email'],
                "name"  => "",
                "type"  => ""
            );
        }
        $this->Mandrillapp->negozy_messenger_send_blast_message($toArray);
    }

}