<?php
class CollectionsController extends AppController {

    public $components = array('RequestHandler','Paginator', 'Image','UserAction');
    public $uses = array('Collection', 'CollectionImage', 'Color');

    public function beforeFilter() {
        parent::beforeFilter();
        $this->_checkShop();
    }

    public function view_list(){
        $this->layout = "ajax";
    }

    public function view_add(){
        $this->layout = "ajax";
    }

    public function view_edit(){
        $this->layout = "ajax";
    }

    public function view_modal_add(){
        $this->layout = "ajax";
    }

    public function get_list($n=10,$page=1,$row="",$orderType=""){
        $this->response->compress();
        $this->Paginator->settings = array(
            "conditions" => array(
                "shop_id" => $this->shop_id
            ),
            "fields" => array(
                "id",
                "name",
                "timestamp_created",
                "color"
            ),
            "contain" => array(
                "CollectionImage" => array(
                    "fields" => array(
                        "CollectionImage.image"
                    ),
                    "conditions" => array(
                        "CollectionImage.primary" => 1
                    )
                )
            ),
            "order" => $row." ".$orderType,
            "limit" => $n,
            "page" => $page,
            "recursive" => -1
        );
        $count = $this->Collection->find("count",array(
            "conditions" => array(
                "shop_id" => $this->shop_id,
            ),
            "recursive" => -1
        ));
        $collections = $this->Paginator->paginate('Collection');
        $return_collections = array();
        foreach($collections as $collection) {
            $image = NULL;
            if(isset($collection['CollectionImage'])) {
                $image = reset($collection['CollectionImage']);
                $image = $image['image'];
            }
            $return_collections[] = array(
                "id" => $collection['Collection']['id'],
                "name" => $collection['Collection']['name'],
                "color" => $collection['Collection']['color'],
                "timestamp_created" => $collection['Collection']['timestamp_created'],
                "image" => $image
            );
        }
        $this->set(array(
            'collections'   => $return_collections,
            'count'         => $count,
            '_serialize'    => array('collections','count')
        ));
    }

    public function collection_list(){
        $collections = $this->Collection->find("all",array(
            "conditions" => array(
                "shop_id" => $this->shop_id
            ),
            "fields" => array(
                "id",
                "name",
                "timestamp_created"
            ),
            "recursive" => -1
        ));
        $collections = Set::extract('/Collection/.',$collections);
        $this->set(array(
            'collections' => $collections,
            '_serialize' => array('collections')
        ));
    }

    public function update(){
        $data = array(
            "success" => false,
            "message"   => __("Si è verificato un errore durante il salvataggio della collezione")
        );
        if($this->request->is("post")) {
            $collection_data = $this->request->data("collection");
            $ckOwner = $this->Collection->find("count",array(
                "conditions" => array(
                    "Collection.shop_id" => $this->shop_id,
                    "Collection.id"      => $collection_data['id']
                )
            ));
            if($ckOwner) {
                $updateTax = array(
                    "id"            => $collection_data['id'],
                    "name"          => $collection_data['name'],
                    "description"   => $collection_data['description'],
                    "color"         => $collection_data['color']['key'],
                );
                if ($this->Collection->save($updateTax)) {
                    $data['success'] = true;
                    $data['message'] = __("La collezione è stata modificata con successo");
                }
            } else {
                $this->response->statusCode(404);
                $data['data']['message'] = "Collection not found";
                $this->set(array(
                    'data' => $data,
                    '_serialize' => array('data')
                ));
                $this->response->send();
                $this->_stop();
            }
        }
        $this->set(array(
            'data' => $data,
            '_serialize' => array('data')
        ));
    }

    public function get($collection_id){
        $collection = $this->Collection->find("first",array(
            "conditions" => array(
                "shop_id" => $this->shop_id,
                "id"      => $collection_id
            ),
            "contain" => array(
                "CollectionImage" => array(
                    "fields" => array(
                        "CollectionImage.name",
                        "CollectionImage.image",
                        "CollectionImage.order"
                    )
                )
            ),
            "fields" => array(
                "Collection.id",
                "Collection.name",
                "Collection.description",
                "Collection.color"
            ),
            "recursive" => -1
        ));
        if($collection) {
            $images = $collection['CollectionImage'];
            $collection = Set::extract('/Collection/.', $collection);
            $collection = reset($collection);
            $this->set(array(
                'collection' => $collection,
                'images' => $images,
                '_serialize' => array('collection', 'images', 'colors')
            ));
        } else {
            $this->response->statusCode(404);
            $data['data']['message'] = "Collection not found";
            $this->set(array(
                'data' => $data,
                '_serialize' => array('data')
            ));
            $this->response->send();
            $this->_stop();
        }
    }

    public function delete(){
        $data['success'] = false;
        if($this->request->is("post")) {
            $collection_id = $this->request->data["collection_id"];
            $collection = $this->Collection->find("first",array(
                "conditions" => array(
                    "id" => $collection_id,
                    "shop_id" => $this->shop_id
                ),
                "contain" => array(
                    "CollectionImage" => array(
                        "fields" => array(
                            "CollectionImage.name",
                            "CollectionImage.image",
                            "CollectionImage.ext",
                            "CollectionImage.path"
                        )
                    )
                ),
                "recursive" => -1
            ));
            if($collection && count($collection)) {
                if($this->Collection->delete($collection_id)){
                    if(isset($collection['CollectionImage']) && count($collection['CollectionImage'])) {
                        $images = reset($collection['CollectionImage']);
                        $this->Image->remove_image($images['name'].'.'.$images['ext'],'collections/'.$images['path'], 'image_');
                    }
                    $data['success'] = true;
                }
            }
        }
        $this->set(array(
            'data' => $data,
            '_serialize' => array('data')
        ));
    }

    public function add() {
        $data['success'] = false;
        $data['collection'] = false;
        if($this->request->is("post")) {
            $collection = $this->request->data["collection"];
            $newCollection = array(
                "id" => null,
                "name" => $collection['name'],
                "shop_id" => $this->shop_id,
                "description"   => $collection['description'],
                "color" => $collection['color']['key'],
            );
            if($this->Collection->save($newCollection)){
                $newCollection['id'] = $this->Collection->id;
                $data['success']    = true;
                $data['collection'] = $newCollection;
            }
        }
        $this->set(array(
            'data' => $data,
            '_serialize' => array('data')
        ));
    }

    public function loadImage($collection_id = NULL){
        $data = array(
            "success" => false,
            "url"   => false,
            "name"   => false
        ) ;
        if(isset($collection_id) && !empty($collection_id) && isset($_FILES['file'])) {
            $collection = $this->Collection->find("first",array(
                "recursive" => -1,
                "conditions" => array(
                    "id" => $collection_id,
                    "shop_id" => $this->shop_id
                ),
                "contain" => array(
                    "CollectionImage" => array(
                        "fields" => array(
                            "CollectionImage.name",
                            "CollectionImage.image",
                            "CollectionImage.order",
                            "CollectionImage.ext",
                            "CollectionImage.path",
                            "CollectionImage.id"
                        )
                    )
                ),
                "fields" => array(
                    "id"
                )
            ));
            if($collection) {
                if(isset($collection['CollectionImage']) && count($collection['CollectionImage'])) {
                    $images = reset($collection['CollectionImage']);
                    $this->CollectionImage->delete($images['id']);
                    $this->Image->remove_image($images['name'].'.'.$images['ext'],'collections/'.$images['path'], 'image_');
                }
                $types = array();
                $types[] = array("width" => 800, "height" => null, "prefix" => "image_");
                $image_data = $this->Image->uploadImage($_FILES['file'],'collections',$types);
                if ($image_data['success']) {
                    $newImage = array();
                    $newImage['CollectionImage'] = $image_data['image'];
                    $newImage['CollectionImage']['collection_id'] = $collection['Collection']['id'];
                    $last_image = $this->CollectionImage->find("first",array(
                        "conditions" => array(
                            "collection_id" => $collection['Collection']['id']
                        )
                    ));
                    if(!$last_image) {
                        $newImage['CollectionImage']['order'] = 1;
                        $newImage['CollectionImage']['primary'] = 1;
                    } else {
                        $newImage['CollectionImage']['order'] = $last_image['CollectionImage']['order'] + 1;
                        $newImage['CollectionImage']['primary'] = 0;
                    }
                    if($this->CollectionImage->save($newImage)) {
                        $data['success'] = true;
                        $data['url'] = "/images/collections/".$image_data['image']['path']."image_".$image_data['image']['name'].".".$image_data['image']['ext'];
                        $data['name'] = $image_data['image']['name'];
                    }
                }
            }
        }
        $this->set(array(
            'data' => $data,
            '_serialize' => array('data')
        ));
    }

    /**
     * @return null
     * Ordine della categoria automatico
     */
    private function calcOrderCollection() {
        $return = NULL;
        $options = array(
            'conditions' => array(
                'shop_id' => $this->shop_id
            ),
            'fields' => array(
                'COALESCE(MAX("order"),-1)+1 as order'
            ),
            'recursive' => -1
        );

        if($order = $this->Page->find('first', $options)) {
            $return = $order[0]['order'];
        }
        return $return;
    }
}