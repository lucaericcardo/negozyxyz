<?php
class AccountController extends AppController {

    public $components = array('RequestHandler','Paginator');

    public $uses = array(
        'User',
        'Order'
    );

    public function beforeFilter() {
        parent::beforeFilter();
        if(!$this->Auth->user()){
            $this->redirect("http://it.negozy.com/signin");
        }
        if($this->Auth->user("pending_token")) {
            $this->redirect("http://it.negozy.com/");
        }
        $this->_loadTopBarData();
    }

    public function view_account() {
        $this->layout = "ajax";
    }

    public function view_orders() {
        $this->layout = "ajax";
    }

    public function view_order_details() {
        $this->layout = "ajax";
    }

    public function view_notifies() {
        $this->layout = "ajax";
    }

    public function index(){
        $this->layout = "account";
        $this->render('empty');
    }

    public function updateAccount() {
        $data['success'] = false;
        $data['message'] = __("Non è stato possibile aggiornare il tuo profilo");
        if($this->request->is("post")) {
            $user = $this->User->find("first",array(
                "conditions" => array(
                    "id" => $this->Auth->user("id")
                ),
                "recursive" => -1
            ));
            $user_data = $this->request->data("user");
            $user['User']['first_name'] = $user_data['first_name'];
            $user['User']['last_name'] = $user_data['last_name'];
            if(isset($user_data['new_password']) && !empty($user_data['new_password'])) {
                $user['User']['password'] = $user_data['new_password'];
            }
            if($this->User->save($user)) {
                $data['success'] = true;
                $data['message'] = __("Il tuo profilo è stato aggiornato con successo.");
            }
        }
        $this->set(array(
            'data' => $data,
            '_serialize' => array('data')
        ));
    }

    public function loadUser() {
        $user = $this->User->find("first",array(
            "contain" => array(
                "UserImage" => array(
                    "fields" => array(
                        "UserImage.image"
                    )
                )
            ),
            "conditions" => array(
                "User.id" => $this->Auth->user("id")
            ),
            "fields" => array(
                'User.first_name',
                'User.last_name'
            ),
            "recursive" => -1
        ));
        $user_data = array();
        $image = null;
        if(isset($user['UserImage'])) {
            $image = reset($user['UserImage']);
            $image = $image['image'];
        }
        if($user) {
            $user_data = array(
                'first_name'    => $user['User']['first_name'],
                'last_name'     => $user['User']['last_name'],
                'image'         => $image
            );
        }
        $this->set(array(
            'user' => $user_data,
            '_serialize' => array('user')
        ));
    }

    public function get_list($n=10,$page=1,$row="",$orderType="") {
        $this->Paginator->settings = array(
            "recursive" => -1,
            "conditions" => array(
                "Order.user_id" => $this->Auth->user("id")
            ),
            "fields" => array(
                "Order.id",
                "Order.order_uid",
                "Order.timestamp_created",
                "Order.total_amount",
                "Order.data",
                "Order.request",
                "Order.available"
            ),
            "contain" => array(
                "OrderStatus" => array(
                    "fields" => array(
                        "name"
                    )
                ),
                "Cart" => array(
                    "fields" => array(
                        "token"
                    )
                ),
                "Shop" => array(
                    "fields" => array(
                        "name",
                        "url"
                    ),
                    "Domain" => array(
                        "order" => "id"
                    )
                )
            ),
            "order" => "Order.".$row." ".$orderType,
            "limit" => $n,
            "page" => $page
        );
        $count = $this->Order->find("count",array(
            "conditions" => array(
                "user_id" => $this->Auth->user("id")
            ),
            "recursive" => -1
        ));
        $tmp_orders = $this->Paginator->paginate('Order');
        $orders = array();
        foreach($tmp_orders as $order) {
            $order_data = json_decode($order['Order']['data'],TRUE);
            $order_currency = $order_data['shop']['currency'];
            $domain = reset($order['Shop']['Domain']);
            if($order['Order']['request']){
                $status = __("Non disponibile");
                $class  = "label label-danger";
                if(!isset($order['Order']['available'])){
                    $status = __("Richiesta in attesa");
                    $class  = "label label-danger";
                } else if (isset($order['Order']['available']) && $order['Order']['available']) {
                    $status = __("Disponibile");
                    $class  = "label label-success";
                }
                $orders[] = array(
                    "id"            => $order['Order']['id'],
                    "uid"           => $order['Order']['order_uid'],
                    "status"        => $status,
                    "label_class"   => $class,
                    "date"          => $order['Order']['timestamp_created'],
                    "amount"        => CakeNumber::currency($order['Order']['total_amount'], $order_currency),
                    "shop"          => $order['Shop']['name'],
                    "link"          => "http://".$domain['domain'],
                    "request"       => true,
                    "available"     => $order['Order']['available'],
                    "request_link"  => ($order['Order']['available']) ? "/checkoutV1/index/".$order['Shop']['url']."/".$order['Cart']['token'] : null
                );
            } else {
                $orders[] = array(
                    "id"            => $order['Order']['id'],
                    "uid"           => $order['Order']['order_uid'],
                    "status"        => $order['OrderStatus']['name'],
                    "label_class"   => "label label-primary",
                    "date"          => $order['Order']['timestamp_created'],
                    "amount"        => CakeNumber::currency($order['Order']['total_amount'], $order_currency),
                    "shop"          => $order['Shop']['name'],
                    "link"          => "http://".$domain['domain'],
                    "request"       => false,
                    "available"     => false,
                    "request_link"  => null
                );
            }
        }

        $this->set(array(
            'orders'        => $orders,
            'count'         => $count,
            '_serialize'    => array('orders','count')
        ));

    }

    public function loadOrder(){
        if($this->request->is("post")) {
            $order_id = $this->request->data['order_id'];
            $order = $this->Order->find("first", array(
                "conditions" => array(
                    "Order.id" => $order_id,
                    "Order.user_id" => $this->Auth->user("id"),
                ),
                "fields" => array(
                    "Order.data",
                    "Order.order_uid",
                    "Order.timestamp_created",
                    "Order.order_status_id",
                    "Order.request",
                    "Order.available",
                ),
                "contain" => array(
                    "OrderStatus" => array(
                        "fields" => array(
                            "OrderStatus.name"
                        )
                    )
                ),
                "recursive" => -1
            ));
            if ($order) {
                $data = json_decode($order['Order']['data'], true);
                $products = array();
                foreach($data['products'] as $product) {
                    $products[] = array(
                        "image" => $product['image'],
                        "name" => $product['name'],
                        "unit" => CakeNumber::currency($product['product_unit']['price_total'], $data['shop']['currency']),
                        "quantity" => $product['quantity'],
                        "price" => CakeNumber::currency($product['product_total']['price_total'], $data['shop']['currency']),
                    );
                }
                $total = array(
                    "shipping_taxable"  => CakeNumber::currency($data['shipping_taxable'], $data['shop']['currency']),
                    "product_taxable"   => CakeNumber::currency($data['product_taxable'], $data['shop']['currency']),
                    "shipping_tax"      => CakeNumber::currency($data['shipping_tax'], $data['shop']['currency']),
                    "product_tax"       => CakeNumber::currency($data['product_tax'], $data['shop']['currency']),
                    "tax"               => CakeNumber::currency($data['tax_subtotal'], $data['shop']['currency']),
                    "extra"             => CakeNumber::currency($data['payment']['price'], $data['shop']['currency']),
                    "amount"            => CakeNumber::currency($data['total'], $data['shop']['currency'])
                );
                $payment = array(
                    "label" => $data['payment']['label'],
                    "value" => $data['payment']['value'],
                    "price" => CakeNumber::currency($data['payment']['price'], $data['shop']['currency'])
                );

                $order_data = array(
                    "order" => array(
                        "uid"       => $order['Order']['order_uid'],
                        "date"      => $order['Order']['timestamp_created'],
                        "status"    => $order['OrderStatus']['name'],
                        "request"   => (int) $order['Order']['request'],
                        "available" => (int) $order['Order']['available'],
                    ),
                    "payment" => $payment,
                    "shipping_address" => array(
                        "full_name"     => $data['user']['shipping_address']['first_name'] . " " . $data['user']['shipping_address']['last_name'],
                        "address"       => $data['user']['shipping_address']['address'],
                        "address"       => $data['user']['shipping_address']['postal_code'],
                        "city"          => $data['user']['shipping_address']['city'],
                        "province"      => $data['user']['shipping_address']['province'],
                        "country"       => $data['user']['shipping_address']['country'],
                        "phone_number"  => $data['user']['shipping_address']['phone_number'],
                    ),
                    "billing_address" => array(
                        "full_name"     => $data['user']['billing_address']['first_name'] . " " . $data['user']['billing_address']['last_name'],
                        "address"       => $data['user']['billing_address']['address'],
                        "address"       => $data['user']['billing_address']['postal_code'],
                        "city"          => $data['user']['billing_address']['city'],
                        "province"      => $data['user']['billing_address']['province'],
                        "country"       => $data['user']['billing_address']['country'],
                        "phone_number"  => $data['user']['billing_address']['phone_number'],
                    ),
                    "products" => $products,
                    "total" => $total
                );

                $this->set(array(
                    'order'         => $order_data,
                    '_serialize'    => array('order')
                ));
            } else {
                $this->response->statusCode(404);
                $data['data']['message'] = "Order not found";
                $this->set(array(
                    'data' => $data,
                    '_serialize' => array('data')
                ));
                $this->response->send();
                $this->_stop();
            }
        }
    }

}