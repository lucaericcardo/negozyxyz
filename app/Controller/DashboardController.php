<?php
class DashboardController extends AppController {

    public $uses = array(
        'User'
    );

    public function beforeFilter() {

        parent::beforeFilter();
        if(!$this->Auth->user()){
            $this->redirect($this->negozy_config['login_url']);
        }
        if($this->Auth->user("pending_token")) {
            $this->redirect($this->redirect($this->negozy_config['site_url']));
        }
        $this->_loadTopBarData();
    }

    public function main(){
        $shop = $this->_loadShop();
        if($shop) {
            return $this->redirect("/dashboard/shop/".$shop['Shop']['url']."/#/dashboard");
        }
        $this->layout = "main";
        $this->render('empty');
    }

    public function shop($shop_url = NULL) {
        $shop = NULL;
        if(isset($shop_url) && !empty($shop_url)) {
            $this->loadModel('Shop');
            $shop = $this->Shop->find("first",array(
                "conditions" => array(
                    "user_id" =>$this->Auth->user("id"),
                    "url" => $shop_url
                ),
                "fields" => array(
                    "id",
                    "language_id"
                ),
                "recursive" => -1
            ));
        }
        if(isset($shop) && !empty($shop)) {
            $apps_data = $this->_loadApplication($shop['Shop']['id']);
            $this->layout = "shop";
            $this->Session->write("shop_id",$shop['Shop']['id']);
            $this->Session->write("language_id",$shop['Shop']['language_id']);
            $this->Session->write("default_language_id",$shop['Shop']['language_id']);
            $this->set("applications",$apps_data['apps']);
            $this->set("applications_config",$apps_data['configs']);
            $this->render('empty');
        } else {
            $this->Auth->logout();
            $this->redirect("/");
        }
    }

    private function _loadCollectionTree(){
        $this->loadModel('Collection');
        $collections = $this->Collection->find("all",array(
            "fields" => array(
                "DISTINCT id",
                "name"
            ),
            "conditions" => array(
                "shop_id" => $this->shop_id
            ),
            'joins' => array(
                array(
                    'table' => 'products_collections',
                    'alias' => 'ProductsCollections',
                    'type' => 'RIGHT',
                    'conditions' => array(
                        'ProductsCollections.collection_id = Collection.id'
                    )
                )
            ),
            "recursive" => -1
        ));
        return $collections;
    }

    private function _loadCategoryTree(){
        $this->loadModel('Product');
        $subcategories = $this->Product->find("all",array(
            "conditions" => array(
                "Product.deleted" => false,
                "Product.shop_id" => $this->shop_id,
            ),
            "fields" => array(
                "DISTINCT Product.subcategory_id"
            ),
            "contain" => array(
                "Subcategory" => array(
                    "fields" => array(
                        "Subcategory.id",
                        "Subcategory.label"
                    ),
                    "Category" => array(
                        "fields" => array(
                            "Category.id",
                            "Category.label"
                        ),
                        "Macrocategory" => array(
                            "fields" => array(
                                "Macrocategory.id",
                                "Macrocategory.label"
                            )
                        )
                    )
                )
            ),
            "recursive" => -1
        ));
        $tree = array();
        foreach ($subcategories as $subcategory) {
            $macrocategory = $subcategory['Subcategory']['Category']['Macrocategory'];
            $category = $subcategory['Subcategory']['Category'];
            $subcategory = $subcategory['Subcategory'];
            if(!isset($tree[$macrocategory['id']])) {
                $tree[$macrocategory['id']]['macrocategory'] = array(
                    "id"    => $macrocategory['id'],
                    "label" => $macrocategory['label']
                );
            }
            if(!isset($tree[$macrocategory['id']]['category'][$category['id']])){
                $tree[$macrocategory['id']]['category'][$category['id']] = array(
                    "id" => $category['id'],
                    "label" => $category['label'],
                    "subcategories" => array()
                );
            }
            $tree[$macrocategory['id']]['category'][$category['id']]['subcategories'][] = array(
                "id"    => $subcategory['id'],
                "label" => $subcategory['label']
            );
        }
        return $tree;
    }

    private function _loadApplication($shop_id){
        App::uses('File', 'Utility');
        $this->loadModel('ShopAppInstalled');
        $applications = $this->ShopAppInstalled->find("all",array(
            "conditions" => array(
                "ShopAppInstalled.shop_id" => $shop_id,
                "ShopAppInstalled.deleted" => false
            ),
            "contain" => array(
                "Application" => array(
                    "fields" => array(
                        "name",
                        "request_uri"
                    )
                )
            ),
            "recursive" => -1
        ));
        $apps = Set::extract("/Application/.",$applications);
        $configs = array();
        foreach($apps as $app) {
            $pluginName = Inflector::camelize($app['request_uri']);
            $app_config = APP.DS."Plugin".DS.$pluginName.DS."config.json";
            $file = new File($app_config);
            $configs[] = json_decode($file->read(),TRUE);
        }
        return array("apps" => $apps,"configs" => $configs);
    }

}
?>