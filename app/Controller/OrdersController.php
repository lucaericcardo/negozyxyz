<?php
class OrdersController extends AppController {

    public $components = array(
        'RequestHandler',
        'Paginator',
        'RTN',
        'NotifiesTemplate',
        'Notifies',
        'Mandrillapp'
    );

    public function beforeFilter() {
        parent::beforeFilter();
        $this->_checkShop();
    }

    public function view_list() {
        $this->layout = "ajax";
    }

    public function view_order() {
        $this->layout = "ajax";
    }

    public function view_request() {
        $this->layout = "ajax";
    }

    public function get_list($n=10,$page=1,$row="",$orderType="") {
        $this->Paginator->settings = array(
            "recursive" => -1,
            "conditions" => array(
                "shop_id" => $this->shop_id
            ),
            "fields" => array(
                "id",
                "order_uid",
                "timestamp_created",
                "total_amount",
                "data",
                "request",
                "available"
            ),
            "contain" => array(
                "OrderStatus" => array(
                    "fields" => array(
                        "name"
                    )
                ),
                "User" => array(
                    "fields" => array(
                        "first_name",
                        "last_name",
                        "email"
                    )
                )
            ),
            "order" => "Order.".$row." ".$orderType,
            "limit" => $n,
            "page" => $page
        );
        $count = $this->Order->find("count",array(
            "conditions" => array(
                "shop_id" => $this->shop_id,
            ),
            "recursive" => -1
        ));
        $tmp_orders = $this->Paginator->paginate('Order');
        $orders = array();
        foreach($tmp_orders as $order) {
            $order_data = json_decode($order['Order']['data'],true);
            $order_currency = $order_data['shop']['currency'];
            if($order['Order']['request']){
                $status = __("Non disponibile");
                $class  = "label label-danger";
                if(!isset($order['Order']['available'])){
                    $status = __("Richiesta in attesa");
                    $class  = "label label-danger";
                } else if (isset($order['Order']['available']) && $order['Order']['available']) {
                    $status = __("Disponibile");
                    $class  = "label label-success";
                }
                $orders[] = array(
                    "id"            => $order['Order']['id'],
                    "uid"           => $order['Order']['order_uid'],
                    "user"          => $order['User']['first_name'] . " " . $order['User']['last_name'],
                    "email"         => $order['User']['email'],
                    "status"        => $status,
                    "label_class"   => $class,
                    "date"          => $order['Order']['timestamp_created'],
                    "amount"        => CakeNumber::currency($order['Order']['total_amount'], $order_currency),
                    "request"       => true
                );
            } else {
                $orders[] = array(
                    "id"            => $order['Order']['id'],
                    "uid"           => $order['Order']['order_uid'],
                    "user"          => $order['User']['first_name'] . " " . $order['User']['last_name'],
                    "email"         => $order['User']['email'],
                    "status"        => $order['OrderStatus']['name'],
                    "label_class"   => "label label-primary",
                    "date"          => $order['Order']['timestamp_created'],
                    "amount"        => CakeNumber::currency($order['Order']['total_amount'], $order_currency),
                    "request"       => false
                );
            }
        }

        $this->set(array(
            'orders'        => $orders,
            'count'         => $count,
            '_serialize'    => array('orders','count')
        ));

    }

    public function changeStatus(){
        $data['success'] = FALSE;
        $data['message'] = __("Non è stato possibile salvare lo stato dell'ordine");
        if($this->request->is("post")) {
            $order_id = $this->request->data['order_id'];
            $status_id = $this->request->data['order_status'];
            $order = $this->Order->find("first",array(
                "conditions" => array(
                    "Order.id" => $order_id,
                    "Order.shop_id" => $this->shop_id
                ),
                "fields" => array(
                    "id"
                ),
                "recursive" => -1
            ));
            if($order) {
                $this->Order->id = $order['Order']['id'];
                if($this->Order->saveField("order_status_id",$status_id)) {
                    $data['success'] = TRUE;
                    $data['message'] = __("Lo stato dell'ordine è stato modificato con successo");
                }
            }
        }
        $this->set(array(
            'data'        => $data,
            '_serialize'    => array('data')
        ));
    }

    public function changeAilability(){
        $data['success'] = FALSE;
        $data['message'] = __("Non è stato possibile salvare lo stato dell'ordine");
        if($this->request->is("post")) {
            $order_id   = $this->request->data['order_id'];
            $available  = $this->request->data['available'];
            $order = $this->Order->find("first",array(
                "conditions" => array(
                    "Order.id"      => $order_id,
                    "Order.shop_id" => $this->shop_id,
                    "Order.request" => 1
                ),
                "contain" => array(
                    "Cart" => array(
                        "fields" => array(
                            "id",
                            "token",
                            "data"
                        )
                    ),
                    "User" => array(
                        "fields" => array(
                            "first_name",
                            "last_name",
                            "email"
                        )
                    ),
                    "Shop" => array(
                        "fields" => array(
                            "name",
                            "url"
                        )
                    )
                ),
                "fields" => array(
                    "id",
                    "user_id",
                    "shop_id",
                    "order_uid"
                ),
                "recursive" => -1
            ));
            if($order) {
                $this->Order->id = $order['Order']['id'];
                if($this->Order->saveField("available",$available)) {

                    $text_notify = __("L'ordine effettuato presso %s è disponibile.",$order['Shop']['name']);
                    $cart_data = json_decode($order['Cart']['data'],TRUE);

                    $view = new View($this, false);
                    $view->set("order",$cart_data);
                    $table      = $view->element('email/table');

                    if(!$available){
                        $text_notify = __("L'ordine effettuato presso %s non è disponibile.",$order['Shop']['name']);
                        $subject    = "Ordine n.".$order['Order']['order_uid']." non disponibile";
                        $this->Mandrillapp->send_request_unavailable($order['User']['first_name']." ".$order['User']['last_name'],$order['Shop']['name'],$table,$order['User']['email'],$subject);

                        $this->loadModel("Cart");
                        $this->Cart->id = $order['Cart']['id'];
                        $this->Cart->save(array(
                            "completed" => false,
                            "available" => false
                        ));
                    } else {
                        $subject    = "Ordine n.".$order['Order']['order_uid']." disponibile";
                        $link       = $this->negozy_config['my_url']."checkoutV1/index/".$order['Shop']['url']."/".$order['Cart']['token'];
                        $this->Mandrillapp->send_request_available($order['User']['first_name']." ".$order['User']['last_name'],$order['Shop']['name'],$table,$order['User']['email'],$link,$subject);

                        $this->loadModel("Cart");
                        $this->Cart->id = $order['Cart']['id'];
                        $this->Cart->save(array(
                            "completed" => false,
                            "available" => true
                        ));
                    }

                    $this->Notifies->sendUserNotify($order['Order']['user_id'],$order['Order']['shop_id'],$text_notify,1,$order_id);

                    $dataToNodejs['receiver_id']    = $order['Order']['user_id'];
                    $dataToNodejs['user_notify']    = true;
                    $dataToNodejs['toasty_html']    = $this->NotifiesTemplate->get('request',array("text" => $text_notify));

                    $this->RTN->send($dataToNodejs);

                    $data['success'] = TRUE;
                    $data['message'] = __("Lo stato dell'ordine è stato modificato con successo");
                }
            }
        }
        $this->set(array(
            'data'        => $data,
            '_serialize'    => array('data')
        ));
    }

    public function loadOrder(){
        $order_data = array();
        if($this->request->is("post")) {
            $order_id = $this->request->data['order_id'];
            $order = $this->Order->find("first", array(
                "conditions" => array(
                    "Order.id" => $order_id,
                    "Order.shop_id" => $this->shop_id,
                ),
                "fields" => array(
                    "Order.data",
                    "Order.order_uid",
                    "Order.timestamp_created",
                    "Order.order_status_id",
                    "Order.request",
                    "Order.available",
                ),
                "recursive" => -1
            ));
            if ($order) {
                $data = json_decode($order['Order']['data'], true);
                $products = array();
                foreach($data['products'] as $product) {
                    $products[] = array(
                        "image" => $product['image'],
                        "name" => $product['name'],
                        "unit" => CakeNumber::currency($product['product_unit']['price_total'], $data['shop']['currency']),
                        "quantity" => $product['quantity'],
                        "price" => CakeNumber::currency($product['product_total']['price_total'], $data['shop']['currency']),
                    );
                }
                $total = array(
                    "shipping_taxable"  => CakeNumber::currency($data['shipping_taxable'], $data['shop']['currency']),
                    "product_taxable"   => CakeNumber::currency($data['product_taxable'], $data['shop']['currency']),
                    "shipping_tax"      => CakeNumber::currency($data['shipping_tax'], $data['shop']['currency']),
                    "product_tax"       => CakeNumber::currency($data['product_tax'], $data['shop']['currency']),
                    "tax"               => CakeNumber::currency($data['tax_subtotal'], $data['shop']['currency']),
                    "extra"             => CakeNumber::currency($data['payment']['price'], $data['shop']['currency']),
                    "amount"            => CakeNumber::currency($data['total'], $data['shop']['currency'])
                );
                $payment = array(
                    "label" => $data['payment']['label'],
                    "value" => $data['payment']['value'],
                    "price" => CakeNumber::currency($data['payment']['price'], $data['shop']['currency'])
                );
                $order_data = array(
                    "order" => array(
                        "uid"       => $order['Order']['order_uid'],
                        "date"      => $order['Order']['timestamp_created'],
                        "status"    => $order['Order']['order_status_id'],
                        "available" => (int) $order['Order']['available'],
                    ),
                    "payment" => $payment,
                    "shipping_address" => array(
                        "full_name"     => $data['user']['shipping_address']['first_name'] . " " . $data['user']['shipping_address']['last_name'],
                        "address"       => $data['user']['shipping_address']['address'],
                        "postal_code"   => $data['user']['shipping_address']['postal_code'],
                        "city"          => $data['user']['shipping_address']['city'],
                        "province"      => $data['user']['shipping_address']['province'],
                        "country"       => $data['user']['shipping_address']['country'],
                        "phone_number"  => $data['user']['shipping_address']['phone_number'],
                        "business_name" => $data['user']['shipping_address']['business_name'],
                        "tax_code"      => $data['user']['shipping_address']['tax_code']
                    ),
                    "billing_address" => array(
                        "full_name"     => $data['user']['billing_address']['first_name'] . " " . $data['user']['billing_address']['last_name'],
                        "address"       => $data['user']['billing_address']['address'],
                        "postal_code"   => $data['user']['billing_address']['postal_code'],
                        "city"          => $data['user']['billing_address']['city'],
                        "province"      => $data['user']['billing_address']['province'],
                        "country"       => $data['user']['billing_address']['country'],
                        "phone_number"  => $data['user']['billing_address']['phone_number'],
                        "business_name" => $data['user']['billing_address']['business_name'],
                        "tax_code"      => $data['user']['billing_address']['tax_code']
                    ),
                    "products" => $products,
                    "total" => $total
                );
                if($order['Order']['request']) {
                    $status = array(
                        array("id" => 0,"name" => "Non disponibile"),
                        array("id" => 1,"name" => "Disponibile")
                    );
                } else {
                    $this->loadModel("OrderStatus");
                    $orderStatus = $this->OrderStatus->find("all", array(
                        "fields" => array(
                            "id",
                            "name"
                        ),
                        "recursive" => -1
                    ));
                    $status = Set::extract("/OrderStatus/.",$orderStatus);
                }

                $this->set(array(
                    'order_status'  => $status,
                    'order'         => $order_data,
                    '_serialize'    => array('order','order_status')
                ));
            } else {
                $this->response->statusCode(404);
                $data['data']['message'] = "Order not found";
                $this->set(array(
                    'data' => $data,
                    '_serialize' => array('data')
                ));
                $this->response->send();
                $this->_stop();
            }
        }
    }
}