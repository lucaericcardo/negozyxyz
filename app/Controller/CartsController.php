<?php
App::import('Vendor', 'paypal_autoload', array('file' => 'merchant-sdk-php-master/PPBootStrap.php'));

class CartsController extends AppController {

    public $uses = array('Product','UserAddress','Country');

    public $components = array('CheckoutV1','RequestHandler');

    public function shop(){
        $this->loadModel("User");
        $user = $this->User->find("first",array(
            "contain" => array(
                "UserImage" => array(
                    "fields" => array(
                        "UserImage.image"
                    )
                )
            ),
            "conditions" => array(
                "id" => $this->Auth->user("id")
            ),
            "fields" => array(
                'first_name',
                'last_name',
                'id',
                'email'
            ),
            "recursive" => -1
        ));
        $image = 'http://cdn.negozy.com/negozy/Img/g1.jpg';
        if(isset($user['UserImage']) && count($user['UserImage'])) {
            $image = reset($user['UserImage']);
            if($image) {
                $image = $image['image'];
            }
        }
        $this->set("image",$image);
        $this->layout = "cart";

    }

    public function load(){
        $cart = $this->CheckoutV1->formatCart(55);
        $this->set(array(
            'cart' => $cart,
            '_serialize' => array('cart')
        ));
    }

    public function addProduct(){
        $cart = array();
        $data['success'] = false;
        if($this->request->is("post")){
            $token =  $this->CheckoutV1->addProductToCart($this->request->data['product_id'],$this->request->data['quantity'],$this->request->data['shop_id']);
            if($token){
                $cart = $this->CheckoutV1->formatCart($this->request->data['shop_id']);
                $data['success'] = true;
            }
        }
        $this->set(array(
            'data' => $data,
            'cart' => $cart,
            '_serialize' => array('data', 'cart')
        ));
    }

    public function updateProduct(){
        $cart = array();
        $data['success'] = false;
        if($this->request->is("post")){
            $token =  $this->CheckoutV1->updateProductToCart($this->request->data['product_id'],$this->request->data['quantity'],$this->request->data['shop_id']);
            if($token){
                $cart = $this->CheckoutV1->formatCart($this->request->data['shop_id']);
                $data['success'] = true;
            }
        }
        $this->set(array(
            'data' => $data,
            'cart' => $cart,
            '_serialize' => array('data', 'cart')
        ));
    }

    public function removeProduct(){
        $cart = array();
        $data['success'] = false;
        if($this->request->is("post")){
            if($this->CheckoutV1->removeProductToCart($this->request->data['product_id'],$this->request->data['shop_id'])){
                $cart = $this->CheckoutV1->formatCart($this->request->data['shop_id']);
                $data['success'] = true;
            }
        }
        $this->set(array(
            'data' => $data,
            'cart' => $cart,
            '_serialize' => array('data', 'cart')
        ));
    }


    public function index() {

        $url = dirname('http://' . $_SERVER['SERVER_NAME'] . ':' . $_SERVER['SERVER_PORT'] . $_SERVER['REQUEST_URI']);
        $returnUrl = "$url/../ExpressCheckout/GetExpressCheckout.php";
        $cancelUrl = "$url/SetExpressCheckout.php" ;

        $currencyCode = 'EUR';
        $paymentDetails = new PaymentDetailsType();
        $paymentDetails->OrderTotal = new BasicAmountType($currencyCode, 200);
        $paymentDetails->PaymentAction = 'Order';

        $sellerDetails = new SellerDetailsType();
        $sellerDetails->PayPalAccountID = "stocazzo@gmail.com";
        $paymentDetails->SellerDetails = $sellerDetails;


        $setECReqDetails = new SetExpressCheckoutRequestDetailsType();
        $setECReqDetails->PaymentDetails[0] = $paymentDetails;
        $setECReqDetails->CancelURL = $cancelUrl;
        $setECReqDetails->ReturnURL = $returnUrl;
        $setECReqType = new SetExpressCheckoutRequestType();
        $setECReqType->SetExpressCheckoutRequestDetails = $setECReqDetails;
        $setECReq = new SetExpressCheckoutReq();
        $setECReq->SetExpressCheckoutRequest = $setECReqType;
        $paypalService = new PayPalAPIInterfaceServiceService(Configuration::getAcctAndConfig());
        try {
            $setECResponse = $paypalService->SetExpressCheckout($setECReq);
        } catch (Exception $ex) {

        }
        if(isset($setECResponse)) {
            if($setECResponse->Ack =='Success') {
                $token = $setECResponse->Token;
                // Redirect to paypal.com here
                $payPalURL = 'https://www.paypal.com/webscr?cmd=_express-checkout&token=' . $token;
                echo" <a href=$payPalURL><b>* Redirect to PayPal to login </b></a><br>";
            }
        }
        die();



        /*
        $paymentDetails = new PaymentDetailsType();

        $setExpressCheckoutRequestDetails = new SetExpressCheckoutRequestDetailsType();
        $setExpressCheckoutRequestDetails->ReturnURL = "http://localhost/return";
        $setExpressCheckoutRequestDetails->CancelURL = "http://localhost/cancel";

        $orderTotal = new BasicAmountType("EUR","2000.00");
        $paymentDetails->OrderTotal = $orderTotal;

        $paymentDetails->PaymentAction = "Order";

        $sellerDetails = new SellerDetailsType();
        $sellerDetails->PayPalAccountID = "paypal@lhsgroup.net";
        $paymentDetails->SellerDetails = $sellerDetails;

        $paymentDetails->PaymentRequestID = "QWERTY";
        $paymentDetails->NotifyURL = "http://localhost/ipn";

        $shipToAddress = new AddressType();
        $shipToAddress->Street1 = "Ape Way";
        $shipToAddress->CityName = "Austin";
        $shipToAddress->StateOrProvince = "TX";
        $shipToAddress->Country = "US";
        $shipToAddress->PostalCode = "78750";

        $paymentDetails->ShipToAddress = $shipToAddress;

        $setExpressCheckoutRequestDetails->PaymentDetails = $paymentDetails;
        $setExpressCheckoutReq = new SetExpressCheckoutReq();
        $setExpressCheckoutRequest = new SetExpressCheckoutRequestType($setExpressCheckoutRequestDetails);
        $setExpressCheckoutReq->SetExpressCheckoutRequest = $setExpressCheckoutRequest;
        $service = new PayPalAPIInterfaceServiceService();
        try {
            $response = $service->SetExpressCheckout($setExpressCheckoutReq);
        } catch (Exception $ex) {
            echo ("Error Message : " + $ex->getMessage());
        }
        if ($response->Ack == "Success") {
            // ### Redirecting to PayPal for authorization
            // Once you get the "Success" response, needs to authorise the
            // transaction by making buyer to login into PayPal. For that,
            // need to construct redirect url using EC token from response.
            // For example,
            // `redirectURL="https://www.sandbox.paypal.com/cgi-bin/webscr?cmd=_express-checkout&token=". $response->Token();`
            // Express Checkout Token
            return $this->redirect("https://www.paypal.com/webscr?cmd=_express-checkout&token=". $response->Token);
            echo ("EC Token:" . $response->Token);
        }
            // ### Error Values
            // Access error values from error list using getter methods
        else {
            echo  ("API Error Message : ". $response->Errors[0]->LongMessage);
        }
        return $response;


        if($this->Auth->user("id")){
            $userAddress = $this->UserAddress->find("first",array(
                "conditions" => array(
                    "user_id" => $this->Auth->user("id")
                )
            ));
            if($this->request->is(array("post","put"))) {
                $newUserAddress = $this->request->data;
                if($userAddress) {
                    $newUserAddress['UserAddress']['id'] = $userAddress['UserAddress']['id'];
                    $newUserAddress['UserAddress']['user_id'] = $this->Auth->user("id");
                }
                $this->UserAddress->save($newUserAddress);

            }
            if($userAddress && !$this->request->data) {
                $this->request->data = $userAddress;
            }
        }

        $order = $this->Checkout->cart();
        $this->set("countries",$this->Country->find("list",array(
            "fields" => array(
                "id",
                "name"
            ),
            "order" => "name ASC"
        )));
        $this->set("order",$order);
        */
    }


    public function login(){

    }

    public function address(){

    }

    public function shipping(){

    }

    public function payments(){

    }

    public function confirm(){

    }


    /*
    public $components = array('RequestHandler');

    public function beforeFilter() {
        parent::beforeFilter();
        $this->_checkShop();
    }

    public function view_thanks() {
        $this->layout = 'ajax';
    }

    public function view_payment_choice() {
        $this->layout = 'ajax';
    }
    public function view_payment_review() {
        $this->layout = 'ajax';
    }

    public function addProductToCart() {
        $data = array();
        $data['success'] = false;
        if($this->request->is("post")) {
            $product_id = $this->request->data["product_id"];
            $quantity = $this->request->data["quantity"];
            $product = $this->Product->find("first" , array(
                    "conditions" => array(
                        "id" => $product_id
                    ),
                    "recursive" => -1
                )
            );
            if(isset($product_id)) {
                $data['success'] = true;
                $this->Session->write("cart",array(
                    "product_id" => $product_id,
                    "quantity"   => $quantity,
                    "product"    => $product
                ));
            }
        }
        $this->set(array(
            'data' => $data,
            '_serialize' => array('data')
        ));
    }

    public function loadReviewData() {
        $data['data'] = $this->Session->read("data");
        $this->set(array(
            'data' => $data,
            '_serialize' => array('data')
        ));
    }

    public function setData() {
        $data['success'] = false;
        if($this->request->is("post")) {
            $cart_data = $this->request->data["cart"];
            $this->Session->write("data",$cart_data);
            $data['success'] = true;
        }
        $this->set(array(
            'data' => $data,
            '_serialize' => array('data')
        ));
    }

    public function loadProduct(){
        $cart_data = $this->Session->read("data");
        $cart = $this->Session->read("cart");
        $product_id = $cart["product_id"];
        $quantity = $cart["quantity"];
        if ($product_id) {
            $tmp_data = $this->Product->find("first", array(
                "conditions" => array(
                    "Product.id" => $product_id
                ),
                "recursive" => -1,
                "fields" => array(
                    "id",
                    "name",
                    "price"
                ),
                "contain" => array(
                    "Shop" => array(
                        "fields" => array(
                            "id",
                            "name"
                        ),
                        "Shipping" => array(
                            "price",
                            "name",
                            "start_day",
                            "end_day"
                        ),
                        "Payments" => array(
                            "fields" => array(
                                "id",
                                "name",
                                "price"
                            )
                        )
                    ),
                    "ShopTax" => array(
                        "fields" => array(
                            "id",
                            "name",
                            "tax",
                            "product_tax"
                        )
                    )
                )
            ));
            $tax_price = 0;
            $procut_price = ($tmp_data['Product']['price'] * $quantity);
            if ($tmp_data['ShopTax']) {
                if ($tmp_data['ShopTax']['product_tax']) {
                    $tax_price = ($procut_price * $tmp_data['ShopTax']['tax']) / 100;
                    $procut_price = $procut_price - $tax_price;
                    $total_price = ($tmp_data['Product']['price'] * $quantity);
                } else {
                    $tax_price = ($procut_price * $tmp_data['ShopTax']['tax']) / 100;
                    $procut_price = $procut_price + $tax_price;
                    $total_price = $procut_price;
                }
            }
            $data['cart'] = array(
                "product_price" => $procut_price,
                "tax_price"     => $tax_price,
                "total_price"   => $total_price,
                "product"       => $tmp_data['Product'],
                "quantity"      => $quantity
            );
            $data['payments'] = $tmp_data['Shop']['Payments'];
            $data['shipments'] = $tmp_data['Shop']['Shipping'];
            if ($this->Auth->user("id")) {
                $data['user'] = array(
                    "id" => $this->Auth->user("id"),
                    "first_name" => $this->Auth->user("first_name"),
                    "last_name" => $this->Auth->user("last_name"),
                );
            }
        }
        $this->set(array(
            'data' => $data,
            'cart_data' => $cart_data,
            '_serialize' => array('data','cart_data')
        ));
    }

    public function saveOrder(){
        $data['success'] = false;
        $this->loadModel("Order");
        $cart_data = $this->Session->read("data");
        $order = array(
            "id" => NULL,
            "shop_id" => $this->shop_id,
            "data"  => json_encode($cart_data)
        );
        if($this->Order->save($order)){
            $data['success'] = true;
        }
        $this->set(array(
            'data' => $data,
            '_serialize' => array('data')
        ));
    }*/

}