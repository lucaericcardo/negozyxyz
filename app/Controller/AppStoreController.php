<?php
class AppStoreController extends AppController {

    public $uses = array('AppCategory', 'Application', 'AppCompanyDeveloper', 'ShopAppInstalled', 'User');
    public $components = array('RequestHandler');

    function beforeFilter(){
        parent::beforeFilter();
        $this->_checkShop();
        $this->_asyncCheckAuth();
    }

    public function view_index() {
        $this->layout = "ajax";
    }

    public function view_apps(){
        $this->layout = "ajax";
    }
    public function view_app(){
        $this->layout = "ajax";
    }

    public function getApplicationsGroupByCategories() {
        $data = array();
        $data['success'] = false;

        $conditions = array();
        if(!$this->userIsAdmin()) {
            $conditions['is_admin'] = false;
        }

        $options = array(
            'contain' => array(
                'Application' => array(
                    'conditions' => $conditions,
                    'fields' => array(
                        'Application.name',
                        'Application.request_uri'
                    ),
                    'AppCompanyDeveloper' => array(
                        'fields' => array(
                            'AppCompanyDeveloper.name'
                        )
                    ),
                    'AppIcon' => array(
                        'fields' => array(
                            'AppIcon.url'
                        )
                    )
                ),
            ),
            'fields' => array(
                'AppCategory.name'
            ),
            'recursive' => -1
        );
        $applicationsGroupByCategories = $this->parseAdmin($this->AppCategory->find("all", $options));
        if($applicationsGroupByCategories){
            $data['success'] = true;
            $data['catsapps'] = $applicationsGroupByCategories;
        }

        $this->set(array(
            'data' => $data,
            '_serialize' => array('data')
        ));
    }

    private function userIsAdmin() {
        return (boolean) count($this->User->findByIdAndIsAdmin($this->Auth->user('id'), true));
    }

    private function parseAdmin($apps) {
        $ret = array();
        foreach($apps as $app) {
            if(count($app['Application'])) {
                $ret[] = $app;
            }
        }
        return $ret;
    }

    public function getPageData() {
        $data = array();
        $data['success'] = false;
        $data['installed'] = false;

        if(isset($this->request->data['request_uri']) && ! empty($this->request->data['request_uri'])) {
            $options = array(
                'conditions' => array(
                    'Application.request_uri' => $this->request->data['request_uri']
                ),
                'fields' => array(
                    'Application.name',
                    'Application.request_uri',
                    'Application.description',
                    'Application.category_id'
                ),
                'contain' => array(
                    'AppCompanyDeveloper' => array(
                        'fields' => array(
                            'AppCompanyDeveloper.name'
                        ),
                        'AppCompanyIcon' => array(
                            'fields' => array(
                                'AppCompanyIcon.url'
                            )
                        )
                    ),
                    'AppIcon' => array(
                        'fields' => array(
                            'AppIcon.url'
                        )
                    ),
                    'AppScreenshot' => array(
                        'fields' => array(
                            'AppScreenshot.url',
                            'AppScreenshot.title'
                        )
                    )
                ),
                'recursive' => -1
            );

            if($data['appData'] = $this->Application->find("first", $options)) {
                $data['otherCompanyApp'] = $this->getOtherCompanyApplication($data['appData']['AppCompanyDeveloper']['id'], $data['appData']['Application']['id']);
                $data['otherCategoryApp'] = $this->getOtherCategoryApplication($data['appData']['Application']['category_id'], $data['appData']['Application']['id']);
                if($this->isInstalled($data['appData']['Application']['id'], false)) {
                    $data['installed'] = true;
                }
                $data['success'] = true;
            }
        }

        $this->set(array(
            'data' => $data,
            '_serialize' => array('data')
        ));
    }

    private function isInstalled($app_id, $exist = true, $deleted = false) {
        $options = array(
            'conditions' => array(
                'ShopAppInstalled.app_id' => $app_id,
                'ShopAppInstalled.shop_id' => $this->shop_id
            ),
            'fields' => array('ShopAppInstalled.id')
        );
        if(!$exist) {
            $options['conditions']['ShopAppInstalled.deleted'] = $deleted;
        }
        return $this->ShopAppInstalled->find('first', $options);
    }

    private function getOtherCompanyApplication($company_id, $app_exlude_id) {
        $conditions = array();
        $conditions['NOT'] = array('Application.id' => $app_exlude_id);
        if(!$this->userIsAdmin()) {
            $conditions['is_admin'] = false;
        }

        $options = array(
            'conditions' => array(
                'AppCompanyDeveloper.id' => $company_id
            ),
            'fields' => array(
                'AppCompanyDeveloper.name'
            ),
            'contain' => array(
                'Application' => array(
                    'conditions' => $conditions,
                    'fields' => array(
                        'Application.name',
                        'Application.request_uri'
                    ),
                    'AppIcon' => array(
                        'fields' => array(
                            'AppIcon.url'
                        )
                    )
                )
            ),
            'recursive' => -1
        );
        return $this->AppCompanyDeveloper->find("first", $options);
    }

    private function getOtherCategoryApplication($category_id, $app_exlude_id) {
        $conditions = array();
        $conditions['NOT'] = array('Application.id' => $app_exlude_id);
        if(!$this->userIsAdmin()) {
            $conditions['is_admin'] = false;
        }
        $options = array(
            'conditions' => array(
                'AppCategory.id' => $category_id
            ),
            'fields' => array(
                'AppCategory.name'
            ),
            'contain' => array(
                'Application' => array(
                    'conditions' => $conditions,
                    'fields' => array(
                        'Application.name',
                        'Application.request_uri'
                    ),
                    'AppIcon' => array(
                        'fields' => array(
                            'AppIcon.url'
                        )
                    ),
                    'AppCompanyDeveloper' => array(
                        'fields' => array(
                            'AppCompanyDeveloper.name'
                        )
                    ),
                )
            ),
            'recursive' => -1
        );
        return $this->AppCategory->find("first", $options);
    }

    private function appIsAdmin($app_id) {
        return (boolean)count($this->Application->findByIdAndIsAdmin($app_id, TRUE));
    }

    public function installApp() {
        $data['success'] = false;
        if(isset($this->request->data['request_uri']) && !empty($this->request->data['request_uri'])
            && isset($this->request->data['app_id']) && !empty($this->request->data['app_id'])) {
            $res = false;
            $userIsAdmin = $this->userIsAdmin();
            $appIsAdmin = $this->appIsAdmin($this->request->data['app_id']);
            if($userIsAdmin || (!$userIsAdmin && !$appIsAdmin)) {
                $data = $this->isInstalled($this->request->data['app_id']);
                if($data) {
                    $data['ShopAppInstalled']['deleted'] = false;
                    $res = $this->ShopAppInstalled->save($data);
                } else {
                    $this->ShopAppInstalled->create();
                    $res = $this->ShopAppInstalled->save(
                        array(
                            'app_id'=>$this->request->data['app_id'],
                            'shop_id' => $this->shop_id,
                            'deleted' => false
                        )
                    );
                }
                $data['success'] = $res;
            }
        }
        $this->set(array(
            'data' => $data,
            '_serialize' => array('data')
        ));
    }

    public function removeApp() {
        $data['success'] = false;
        if(isset($this->request->data['request_uri']) && !empty($this->request->data['request_uri'])
            && isset($this->request->data['app_id']) && !empty($this->request->data['app_id'])) {

            $data = $this->isInstalled($this->request->data['app_id']);
            $res = false;
            if($data) {
                $data['ShopAppInstalled']['deleted'] = true;
                $res = $this->ShopAppInstalled->save($data);
            }
            $data['success'] = $res;
        }
        $this->set(array(
            'data' => $data,
            '_serialize' => array('data')
        ));
    }

    public function getTokenPay(){
        App::import('Vendor', 'Braintree', array('file' => 'Braintree/vendor/autoload.php'));
        Braintree_Configuration::environment('sandbox');
        Braintree_Configuration::merchantId('nkffybxg5y9dncwv');
        Braintree_Configuration::publicKey('pjcz28mvn4kjzb37');
        Braintree_Configuration::privateKey('cb7c25ba44927cd759ef342a676b4318');
        $data['token'] = Braintree_ClientToken::generate();
        $this->set(array(
            'data' => $data,
            '_serialize' => array('data')
        ));
    }

    public function createPayment()
    {
        if ($this->request->is("post") && isset($this->request->data['nonce'])) {
            App::import('Vendor', 'Braintree', array('file' => 'Braintree/vendor/autoload.php'));
            Braintree_Configuration::environment('sandbox');
            Braintree_Configuration::merchantId('nkffybxg5y9dncwv');
            Braintree_Configuration::publicKey('pjcz28mvn4kjzb37');
            Braintree_Configuration::privateKey('cb7c25ba44927cd759ef342a676b4318');
            $result = Braintree_Transaction::sale(array(
                'amount' => '100.00',
                'paymentMethodNonce' => $this->request->data['nonce']
            ));
            var_dump($result);
            die();
        }
        die();
    }

}