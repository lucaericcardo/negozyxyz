<?php
class CheckoutV1Controller extends AppController {

    public $uses = array(
        'Product',
        'Country',
        'Payment',
        'Order',
        'OrderItem',
        'User',
        'UserAddressShipping',
        'UserAddressBilling',
        'Cart'
    );

    public $components = array(
        'CheckoutV1',
        'PayPal',
        'Mandrillapp',
        'UserSignUp',
        'RTN',
        'NotifiesTemplate',
        'Notifies'
    );


    public function beforeFilter(){
        parent::beforeFilter();
        $this->Auth->allow();
    }

    public function index($shop_url = NULL,$token = NULL){

        $this->layout = "checkoutV1";
        $cart = $this->_findCart($shop_url,$token);

        if(!$cart) {
            $this->Session->write("cart.error","10404");
            return $this->redirect("/checkoutV1/error/".$shop_url."/".$token);
        }

        if($cart['Cart']['completed']) {
            $this->Session->write("cart.error","10405");
            return $this->redirect("/checkoutV1/error/".$shop_url."/".$token);
        }

        $check_owner = $this->_checkOwnerCart($cart['Cart']['user_id']);
        if(!$check_owner) {
            $this->Session->write("cart.error","10401");
            return $this->redirect("/checkoutV1/error/".$shop_url."/".$token);
        }

        $cart_data = json_decode($cart['Cart']['data'],TRUE);
        if ($this->request->is("post")) {
            $valid = true;
            //Carello non loggato
            if(!$cart['Cart']['user_id']) {
                $this->User->set($this->request->data['User']);
                if ($this->User->validates(array('fieldList' => array('email', 'password')))) {
                    $this->Session->write("Buyer", $this->request->data['User']);
                } else {
                    $valid = false;
                }
            }

            //Indirizzo di spedizione
            $this->UserAddressShipping->set($this->request->data['UserAddressShipping']);
            if ($this->UserAddressShipping->validates()) {
                $this->Session->write("BuyerAddress", $this->request->data['UserAddressShipping']);
            } else {
                $valid = false;
            }

            //Indirizzo di fatturazione
            if (isset($this->request->data['UserAddressBilling']) && !$this->request->data['UserAddressBilling']['same_shipping']) {
                $this->UserAddressBilling->set($this->request->data['UserAddressBilling']);
                if ($this->UserAddressBilling->validates()) {
                    $this->Session->write("BuyerBilling", $this->request->data['UserAddressBilling']);
                } else {
                    $valid = false;
                }
            } else {
                $this->Session->write("BuyerBilling", array("same_shipping" => 1));
            }

            if($valid) {
                if($this->CheckoutV1->setCountry($token,$this->request->data['UserAddressShipping']['country_id']) && $this->CheckoutV1->setPayment($token,$this->request->data['Payment']['type']) && $this->CheckoutV1->setUser($token,$this->request->data['UserAddressShipping'],$this->request->data['UserAddressBilling'])) {
                    return $this->redirect("/checkoutV1/summary/".$shop_url."/".$token);
                }
            }

        } else {
            if(!$this->Session->read("BuyerAddress") && $this->Auth->user() && $cart['Cart']['user_id']) {
                $user_address_shipping = $this->UserAddressShipping->find("first",array(
                    "conditions" => array(
                        "user_id" => $this->Auth->user("id"),
                    ),
                    "fields" => array(
                        "first_name",
                        "last_name",
                        "address",
                        "postal_code",
                        "city",
                        "province",
                        "phone_number",
                        "country_id",
                        "business_name",
                        "tax_code"
                    ),
                    "recursive" => -1
                ));
                if($user_address_shipping) {
                    $this->Session->write("BuyerAddress", $user_address_shipping['UserAddressShipping']);
                }
            }

            if(!$cart['Cart']['user_id']) {
                $this->request->data['User'] = $this->Session->read("Buyer");
                $this->Session->write("BuyerBilling",array("same_shipping" => 1));
            }

            $this->request->data['UserAddressShipping'] = $this->Session->read("BuyerAddress");
            $this->request->data['UserAddressBilling']  = $this->Session->read("BuyerBilling");
            $this->request->data['Payment']['type']  = $cart_data['payment']['id'];
        }

        $payments = $this->Payment->find("all", array(
            "conditions" => array(
                "shop_id" => $cart['Shop']['id'],
            ),
            "fields" => array(
                "id",
                "name",
                "price"
            ),
            "recursive" => -1
        ));
        $payment_options = array();
        foreach ($payments as $payment) {
            $payment_name = "";
            if ($payment['Payment']['name'] == "bonifico") {
                $payment_name = __("Bonifico bancario");
            } else if ($payment['Payment']['name'] == "money") {
                $payment_name = __("Pagamento diretto al negoziante");
            } else if ($payment['Payment']['name'] == "contrassegno") {
                $payment_name = __("Contrassegno");
            } else if ($payment['Payment']['name'] == "paypal") {
                $payment_name = __("PayPal o carta di credito");
            }
            $payment_options[$payment['Payment']['id']] = array(
                "id" => $payment['Payment']['id'],
                "name" => $payment_name,
                "price" => $payment['Payment']['price']
            );
        }

        $this->set("logged", ($cart['Cart']['user_id']) ? true : false);
        $this->set("payments", $payment_options);
        $this->set("cart", $cart_data);
        $this->set("available", $cart['Cart']['available']);
        $this->set("countries", $this->Country->find("list", array(
            "fields" => array(
                "id",
                "name"
            ),
            "recursive" => -1,
            "order" => "name ASC"
        )));
    }

    public function summary($shop_url = NULL,$token = NULL){
        $this->layout = "checkoutV1";
        $cart = $this->_findCart($shop_url,$token);

        if(!$cart) {
            $this->Session->write("cart.error","10404");
            return $this->redirect("/checkoutV1/error/".$shop_url."/".$token);
        }

        if($cart['Cart']['completed']) {
            $this->Session->write("cart.error","10405");
            return $this->redirect("/checkoutV1/error/".$shop_url."/".$token);
        }

        $check_owner = $this->_checkOwnerCart($cart['Cart']['user_id']);
        if(!$check_owner) {
            $this->Session->write("cart.error","10401");
            return $this->redirect("/checkoutV1/error/".$shop_url."/".$token);
        }

        $cart_data = json_decode($cart['Cart']['data'],TRUE);
        $this->set("cart", $cart_data);
        $this->set("available", $cart['Cart']['available']);
        $this->set("shipping_address",$this->Session->read("BuyerAddress"));
        $this->set("billing_address",$this->Session->read("BuyerBilling"));
        $this->set("countries", $this->Country->find("list", array(
            "fields" => array(
                "id",
                "name"
            ),
            "recursive" => -1,
            "order" => "name ASC"
        )));

    }

    public function process($shop_url = NULL,$token = NULL){
        $this->autoRender = false;
        $cart = $this->_findCart($shop_url,$token);
        if(!$cart) {
            $this->Session->write("cart.error","10404");
            return $this->redirect("/checkoutV1/error/".$shop_url."/".$token);
        }
        if($cart['Cart']['completed']) {
            $this->Session->write("cart.error","10405");
            return $this->redirect("/checkoutV1/error/".$shop_url."/".$token);
        }
        $check_owner = $this->_checkOwnerCart($cart['Cart']['user_id']);
        if(!$check_owner) {
            $this->Session->write("cart.error","10401");
            return $this->redirect("/checkoutV1/error/".$shop_url."/".$token);
        }
        $cart_data = json_decode($cart['Cart']['data'],TRUE);
        if($cart_data['request_availability'] && !$cart['Cart']['available']) {
            if ($this->_saveOrder($cart)) {
                return $this->redirect("/checkoutV1/success/" . $shop_url . "/" . $token);
            }
        } else {
            if($cart_data['payment']['name'] == "paypal") {
                $this->PayPal->return_url   = "/checkoutV1/process/".$shop_url."/".$token;
                $this->PayPal->error_url    = "/checkoutV1/process/".$shop_url."/".$token;
                if(isset($this->request->query['token'])) {
                    if($payment_data = $this->PayPal->doPayment($cart_data,$token,$this->request->query['token'])){
                        if ($this->_saveOrder($cart,$payment_data)) {
                            return $this->redirect("/checkoutV1/success/" . $shop_url . "/" . $token);
                        }
                    }
                    $this->Session->write("cart.error","10401");
                    return $this->redirect("/checkoutV1/error/".$shop_url."/".$token);
                } else {
                    $paypal = $this->PayPal->setOrder($cart_data,$token);
                    if($paypal) {
                        return $this->redirect($paypal);
                    }
                    $this->Session->write("cart.error","10401");
                    return $this->redirect("/checkoutV1/error/".$shop_url."/".$token);
                }
            } else {
                if ($this->_saveOrder($cart)) {
                    return $this->redirect("/checkoutV1/success/" . $shop_url . "/" . $token);
                }
            }
        }
        $this->Session->write("cart.error", "10502");
        return $this->redirect("/checkoutV1/error/" . $shop_url . "/" . $token);
    }

    public function error($shop_url = NULL,$token = NULL)
    {
        $this->layout = "checkoutV1";
        $cart = $this->_findCart($shop_url,$token);
        $cart_data =  NULL;
        if($cart)
            $cart_data = json_decode($cart['Cart']['data'],TRUE);

        $error_code = $this->Session->read("cart.error");
        $error = Configure::read("checkout_config.error");

        $this->set("error",$error[$error_code]);

        $this->set("cart", $cart_data);
    }

    public function success($shop_url = NULL,$token = NULL)
    {
        $this->layout = "checkoutV1";

        $cart = $this->_findCart($shop_url,$token);
        if(!$cart) {
            $this->Session->write("cart.error","10404");
            return $this->redirect("/checkoutV1/error/".$shop_url."/".$token);
        }
        if(!$cart['Cart']['completed']) {
            $this->Session->write("cart.error","10407");
            return $this->redirect("/checkoutV1/error/".$shop_url."/".$token);
        }
        $check_owner = $this->_checkOwnerCart($cart['Cart']['user_id']);
        if(!$check_owner) {
            $this->Session->write("cart.error","10401");
            return $this->redirect("/checkoutV1/error/".$shop_url."/".$token);
        }
        $cart_data = json_decode($cart['Cart']['data'],TRUE);
        $this->set("cart", $cart_data);
    }

    private function _checkOwnerCart($user_id = NULL){
        if($this->Auth->user()) {
            if(isset($user_id) && ($user_id != $this->Auth->user("id"))) {
                return false;
            }
        } else if (!$this->Auth->user() && $user_id) {
            return false;
        }
        return true;
    }

    private function _findCart($shop_url = NULL,$token = NULL){
        return $this->Cart->find("first",array(
            "conditions" => array(
                "token"         => $token
            ),
            "fields" => array(
                "Cart.data",
                "Cart.id",
                "Cart.user_id",
                "Cart.completed",
                "Cart.available",
                "Shop.id"
            ),
            array(
                "joins" => array(
                    array(
                        'table' => 'shops',
                        'alias' => 'Shop',
                        'type' => 'RIGHT',
                        'conditions' => array(
                            'Shop.id = Cart.shop_id',
                            "Shop.url" => $shop_url
                        )
                    )
                )
            )
        ));
    }

    private function _saveOrder($cart = NULL,$payment_data = NULL){
        if(isset($cart)) {
            $cart_data = json_decode($cart['Cart']['data'],TRUE);
            $user_id = NULL;
            if(!$cart['Cart']['user_id']) {
                $data_signup = $this->Session->read("Buyer");
                $user['User'] = array(
                    "first_name"    => $cart_data['user']['shipping_address']['first_name'],
                    "last_name"     => $cart_data['user']['shipping_address']['last_name'],
                    "email"         => $data_signup['email'],
                    "password"      => $data_signup['password'],
                    "source"        => $cart_data['shop']['name']
                );
                $user['UserAddressShipping'] = array(
                    "first_name"    => $cart_data['user']['shipping_address']['first_name'],
                    "last_name"     => $cart_data['user']['shipping_address']['last_name'],
                    "address"       => $cart_data['user']['shipping_address']['address'],
                    "postal_code"   => $cart_data['user']['shipping_address']['postal_code'],
                    "city"          => $cart_data['user']['shipping_address']['city'],
                    "province"      => $cart_data['user']['shipping_address']['province'],
                    "phone_number"  => $cart_data['user']['shipping_address']['phone_number'],
                    "country_id"    => $cart_data['user']['shipping_address']['country_id'],
                    "business_name" => $cart_data['user']['shipping_address']['business_name'],
                    "tax_code"      => $cart_data['user']['shipping_address']['tax_code']
                );
                $user['UserAddressBilling'] = array(
                    "first_name"    => $cart_data['user']['billing_address']['first_name'],
                    "last_name"     => $cart_data['user']['billing_address']['last_name'],
                    "address"       => $cart_data['user']['billing_address']['address'],
                    "postal_code"   => $cart_data['user']['billing_address']['postal_code'],
                    "city"          => $cart_data['user']['billing_address']['city'],
                    "province"      => $cart_data['user']['billing_address']['province'],
                    "phone_number"  => $cart_data['user']['billing_address']['phone_number'],
                    "country_id"    => $cart_data['user']['billing_address']['country_id'],
                    "business_name" => $cart_data['user']['billing_address']['business_name'],
                    "tax_code"      => $cart_data['user']['billing_address']['tax_code']
                );
                $full_name  = $cart_data['user']['shipping_address']['first_name']." ".$cart_data['user']['shipping_address']['last_name'];
                $email      = $data_signup['email'];
                $response   = $this->UserSignUp->addUser($user);
                if($response['success']){
                    $user_id = $response['user_id'];
                }
            } else if($this->Auth->user()) {
                $user_id = $this->Auth->user("id");
                $full_name  = $this->Auth->user("first_name")." ".$this->Auth->user("last_name");
                $email      = $this->Auth->user("email");
            }
            $today = date("Ymd");
            $rand = strtoupper(substr(uniqid(sha1(time())),0,4));
            $uid = $today . $rand;
            $this->loadModel("Country");
            $countryShipping = $this->Country->find("first", array(
                "fields" => array(
                    "name"
                ),
                "conditions" => array(
                    "id" => $cart_data['user']['shipping_address']['country_id']
                ),
                "recursive" => -1
            ));
            $countryBilling = $this->Country->find("first", array(
                "fields" => array(
                    "name"
                ),
                "conditions" => array(
                    "id" => $cart_data['user']['shipping_address']['country_id']
                ),
                "recursive" => -1
            ));
            $cart_data['user']['shipping_address']['country']   = $countryShipping['Country']['name'];
            $cart_data['user']['billing_address']['country']    = $countryBilling['Country']['name'];
            $newOrder = array(
                "Order" => array(
                    "shop_id"           => $cart_data['shop']['id'],
                    "order_uid"         => $uid,
                    "user_id"           => $user_id,
                    "total_amount"      => $cart_data['total'],
                    "order_status_id"   => 1,
                    "data"              => json_encode($cart_data),
                    "cart_id"           => $cart['Cart']['id'],
                    "payment_data"      => json_encode($payment_data),
                    "request"           => ($cart_data['request_availability'] && !$cart['Cart']['available']) ? $cart_data['request_availability'] : false,
                    "available"         => NULL
                )
            );
            $address = $this->Session->read("BuyerAddress");
            if($cart['Cart']['available']) {
                $order = $this->Order->find("first",array(
                    "conditions" => array(
                        "Cart.id" => $cart['Cart']['id']
                    ),
                    "fields" => array(
                        "Order.id"
                    ),
                    "contain" => array(
                        "Cart" => array(
                            "fields" => array(
                                "Cart.id"
                            )
                        )
                    ),
                    "recursive" => -1
                ));
                $this->Order->id = $order['Order']['id'];
            }
            if ($this->Order->save($newOrder)) {
                $this->Cart->id = $cart['Cart']['id'];
                $this->Cart->saveField("completed",true);
                if(!$this->_checkProductQuantity($this->Order->id,$cart_data)) {
                    $this->Order->delete($this->Order->id);
                    $this->Session->write("cart.error","10504");
                    return $this->redirect("/checkoutV1/error/".$cart_data['shop']['url']."/".$cart_data['token']);
                }
                if(($cart_data['request_availability'] && !$cart['Cart']['available'])) {
                    $this->_sendRequest($user_id,$full_name,$address,$email,$uid,$cart_data,$this->Order->id);
                } else {
                    $this->_sendOrder($user_id,$full_name,$address,$email,$uid,$cart_data,$this->Order->id);
                }

                return true;
            }
        }
        return false;
    }

    private function _sendOrder($user_id,$full_name,$address,$email,$uid,$cart_data,$order_id)
    {
        $address = $address['first_name']." ".$address['last_name']." ".$address['address']." ".$address['city'] ." ".$address['postal_code'] ." ".$address['province'];
        $view = new View($this, false);
        $view->set("order",$cart_data);
        $table = $view->element('email/table');
        $shipping = "";
        $payments = $cart_data['payment']['label']." <br /> ".$cart_data['payment']['value'];

        $seller_data = $this->_getSellerIdShopId($cart_data['shop']['id']);
        $this->Mandrillapp->send_order($full_name,$email,$uid,$address,$cart_data['shop']['name'],$cart_data['shop']['image'],$table,$shipping,$payments);
        $this->Mandrillapp->send_order_to_merchant( $seller_data['first_name'] . " " . $seller_data['last_name'],$seller_data['email'],$address,$cart_data['shop']['name'],$cart_data['shop']['image'],$table,$shipping,$payments);

        $text_order = __('Hai ricevuto un nuovo ordine da %s',$full_name);

        $dataToNodejs['shop_notify']    = true;
        $dataToNodejs['receiver_id']    = $seller_data['id'];
        $dataToNodejs['toasty_html']    = $this->NotifiesTemplate->get('new_order',array("text" => $text_order));

        $this->RTN->send($dataToNodejs);
        $this->Notifies->sendShopNotify($seller_data['id'],$user_id,$text_order,2,$order_id);
    }

    private function _sendRequest($user_id,$full_name,$address,$email,$uid,$cart_data,$order_id)
    {
        $seller_data = $this->_getSellerIdShopId($cart_data['shop']['id']);

        $text_order = __('Hai ricevuto una nuova richieta di disponibilità da %s',$full_name);

        $view = new View($this, false);
        $view->set("order",$cart_data);
        $table = $view->element('email/table');

        $this->Mandrillapp->send_request_availability($seller_data['first_name'] . " " . $seller_data['last_name'],$full_name,$table,$seller_data['email']);

        $dataToNodejs['shop_notify']    = true;
        $dataToNodejs['receiver_id']    = $seller_data['id'];
        $dataToNodejs['toasty_html']    = $this->NotifiesTemplate->get('new_order',array("text" => $text_order));

        $this->RTN->send($dataToNodejs);
        $this->Notifies->sendShopNotify($seller_data['id'],$user_id,$text_order,0,$order_id);
    }

    private function _checkProductQuantity($order_id= NULL,$cart_data = NULL){
        $success = true;
        if(isset($cart_data) && isset($order_id)) {
            foreach($cart_data['products'] as $product) {
                $row_product = $this->Product->find("first",array(
                    "conditions" => array(
                        "id" => $product['id']
                    ),
                    "fields" => array(
                        "type_quantity",
                        "quantity"
                    ),
                    "recursive" => -1
                ));
                if($row_product) {
                    if($row_product['Product']['type_quantity'] == 0){
                        $remaining  = ($row_product['Product']['quantity'] - $product['quantity']);
                        $this->Product->id = $product['id'];
                        $this->Product->saveField("quantity",$remaining);
                    }
                    $new_order_item = array(
                        "OrderItem" => array(
                            "order_id"      => $order_id,
                            "product_id"    => $product['id'],
                            "quantity"      => $product['quantity'],
                            "data"          => json_encode($product)
                        )
                    );
                    $this->OrderItem->create();
                    $this->OrderItem->save($new_order_item);
                } else {
                    $success = false;
                }
            }
        }
        return $success;
    }

    private function _getSellerIdShopId($shop_id)
    {
        $this->loadModel('Shop');
        $options = array(
            'conditions' => array(
                'Shop.id' => $shop_id
            ),
            'fields' => array('Shop.id'),
            'contain' => array(
                'User' => array(
                    'fields' => array(
                        'User.id',
                        'User.first_name',
                        'User.last_name',
                        'User.email',
                    )
                )
            ),
            'recursive' => -1
        );
        $tmp = Set::extract('/User/.', $this->Shop->find('first', $options));
        return $tmp[0];
    }


}