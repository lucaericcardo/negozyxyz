<?php
class ExportController extends AppController {

    public $components = array('RequestHandler');

    public function beforeFilter(){
        $this->Auth->allow();
    }

    /*public function export_users(){
        $this->loadModel("User");
        $users = $this->User->find("all",array(
            "fields" => array(
                "id"
            ),
            "recursive" => -1
        ));

        $users = Set::extract("/User/id",$users);
        $this->set(array(
            'users'          => $users,
            '_serialize'    => array('users')
        ));
    }*/

    public function export_shop($user_id = NULL){

        /**
         * Start User
         */
        /*
        $this->loadModel("User");
        $user = $this->User->find("first",array(
            "conditions" => array(
                "User.id" => $user_id
            ),
            "recursive" => -1
        ));*/
        /**
         * End User
         */

        /**
         * Start Shop
         */
        $this->loadModel("Shop");
        $shop = $this->Shop->find("first",array(
            "conditions" => array(
                "user_id" => $user_id
            ),
            "fields" => array(
                "id",
                "name",
                "url",
                "vat_number",
                "address",
                "city",
                "cap",
                "state",
                "province",
                //"business_name",
                //"description",
                "visible",
                "currency",
                //"macrocategory_id",
                "phone_number",
                "email",
                //"about_us",
                "latitude",
                "longitude",
                "contact_address",
                "theme_id",
                "is_public",
                "site_url"
            ),
            "contain" => array(
                "ShopLogo",
                "ShopCover",
                "Domain"
            ),
            "recursive" => -1
        ));
        /**
         * End Shop
         */
        $collections = array();
        $tags = array();
        $products = array();
        if($shop) {
            $shop_id = $shop['Shop']['id'];
            /**
             * Start ShopTax

            $this->loadModel("ShopTax");
            $tmp_shop_tax = $this->ShopTax->find("all", array(
                "conditions" => array(
                    "shop_id" => $shop_id
                ),
                "recursive" => -1
            ));
            foreach ($tmp_shop_tax as $shop_tax) {
                $shop_taxes[] = $shop_tax['ShopTax'];
            }*/
            /**
             * End ShopTax
             */

            /**
             * Start Payment

            $this->loadModel("Payment");
            $tmp_payment = $this->Payment->find("all", array(
                "conditions" => array(
                    "shop_id" => $shop_id
                ),
                "recursive" => -1
            ));
            foreach ($tmp_payment as $payment) {
                $payments[] = $payment['Payment'];
            }*/
            /**
             * End Payment
             */

            /**
             * Start Shipping
             */
            /*
            $this->loadModel("Shipping");
            $tmp_shipping = $this->Shipping->find("all", array(
                "conditions" => array(
                    "shop_id" => $shop_id
                ),
                "recursive" => -1
            ));
            foreach ($tmp_shipping as $shipment) {
                $shipping[] = $shipment['Shipping'];
            }*/
            /**
             * End Shipping
             */

            /**
             * Start Collection
             */
            $this->loadModel("ProductCategory");
            $tmp_collection = $this->ProductCategory->find("all", array(
                "conditions" => array(
                    "shop_id" => $shop_id
                ),
                "contain" => array(
                    "ProductCategoryImage",
                    "ProductCategoryContent"
                ),
                "recursive" => -1
            ));
            foreach ($tmp_collection as $collection) {
                $tmp_collection = array(
                    "id" => $collection['ProductCategory']['id'],
                    "content" => array(
                        "name"          => $collection['ProductCategoryContent'][0]['title'],
                        "description"   => $collection['ProductCategoryContent'][0]['description'],
                    )
                );
                if (isset($collection['ProductCategoryImage'])) {
                        $tmp_collection['images'][] = "http://cdn.negozy.com/images/collections/".$collection['ProductCategoryImage']['path']."/image_".$collection['ProductCategoryImage']['name'].".".$collection['ProductCategoryImage']['ext'];
                }
                $collections[] = $tmp_collection;
            }

            /**
             * End Collection
             */

            /**
             * Start Tags
             */
            $this->loadModel("Tag");
            $tmp_tags = $this->Tag->find("all", array(
                "conditions" => array(
                    "shop_id" => $shop_id
                ),
                "recursive" => -1
            ));
            foreach ($tmp_tags as $tag) {
                $tags[] = $tag['Tag'];
            }
            /**
             * End Tags
             */


            /**
             * Start Product
             */
            $this->loadModel("Product");
            $tmp_products = $this->Product->find("all", array(
                "conditions" => array(
                    "Product.shop_id" => $shop_id
                ),
                "fields" => array(
                    "price",
                    "quantity",
                    "type_quantity",
                    "deleted",
                    "published",
                    "shop_tax_id",
                    //"name",
                    //"description",
                    //
                    //
                    //"url"
                ),
                "contain" => array(
                    "ProductCategory",
                    "ProductImage",
                    "ProductContent",
                    "Tag"
                ),
                "recursive" => -1
            ));
            foreach ($tmp_products as $product) {
                $tmp_product = array(
                    "price" => $product['Product']["price"],
                    "quantity" => $product['Product']["quantity"],
                    "type_quantity" => $product['Product']["type_quantity"],
                    "deleted" => $product['Product']["deleted"],
                    "published" => $product['Product']["published"],
                    //"shop_tax_id" => $product['Product']["shop_tax_id"],
                    "content" => array(
                        "name" => $product['ProductContent'][0]["name"],
                        "description" => $product['ProductContent'][0]["description"],
                        "url" => $product['ProductContent'][0]["uri"]
                    ),
                    "images" => array(),
                    "tags" => array()
                );
                if (isset($product['ProductImage'])) {
                    foreach ($product['ProductImage'] as $image) {
                        $tmp_product['images'][] = $image['image'];
                    }
                    foreach ($product['Tag'] as $tag) {
                        $tmp_product['tags'][] = $tag['id'];
                    }
                    $tmp_product['collection'][] = $product['ProductCategory']['id'];
                }
                $products[] = $tmp_product;
            }
            /**
             * End Product
             */
        }
        $this->set(array(
            'shop'          => $shop,
            //'shipping'      => $shipping,
            'collections'   => $collections,
            //'shop_taxes'    => $shop_taxes,
            //'payment'       => $payment,
            'tags'          => $tags,
            'products'      => $products,
            //'_serialize'    => array('user','shop','tags','collections','shop_taxes','shipping','payment','products')
            '_serialize'    => array('shop','tags','collections','products')
        ));
    }

}