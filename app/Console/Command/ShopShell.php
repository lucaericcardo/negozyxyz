<?php
App::uses('Component', 'Controller','View');
App::uses('ComponentCollection', 'Controller','View');
App::import('Component', 'CheckShop');
App::import('Component', 'Mandrillapp');

class ShopShell extends AppShell {

    public $uses = array('Shop');

    public $suggest = array(
        'cover' => ''
    );

    public function main(){
        
        $Collection = new ComponentCollection();
        $CheckShop = new CheckShopComponent($Collection);
        //Cerco shoppe
        $shops = $this->Shop->find("all",array(
            "recursive" => -1,
            "fields" => array(
                "Shop.id",
                "Shop.name",
                "Shop.url"
            ),
            "contain" => array(
                "User" => array(
                    "fields" => array(
                        "first_name",
                        "last_name",
                        "email"
                    )
                )
            )
        ));
        foreach ($shops as $shop){
            $data = $CheckShop->verify($shop['Shop']['id']);
            $actions = array();
            foreach ($data['actions'] as $action => $value){
                $actions[] = $CheckShop->getSuggest($action);
            }
            if($data['count'] && $shop['User']['email']) {
                $mandrill = new MandrillappComponent($Collection);
                $view = new View();
                $view->set("actions", $actions);
                $list = $view->element('email/actions_list');
                $fullname = $shop['User']['first_name'] . " " . $shop['User']['last_name'];
                $url = "http://".$shop['Shop']['url'].".negozy.com";
                $mandrill->wake_up_shop($fullname, $shop['User']['email'], $list,$url,$shop['Shop']['name']);
            }

        }

    }

}