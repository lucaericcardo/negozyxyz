<?php
App::uses('Component', 'Controller','View');
App::uses('ComponentCollection', 'Controller','View');
App::import('Vendor', 'CronFieldInterface', array('file' => 'Cron'  . DS . 'FieldInterface.php'));
App::import('Vendor', 'CronFieldFactory', array('file' => 'Cron'  . DS . 'FieldFactory.php'));
App::import('Vendor', 'CronAbstractField', array('file' => 'Cron'  . DS . 'AbstractField.php'));
App::import('Vendor', 'CronCronExpression', array('file' => 'Cron'  . DS . 'CronExpression.php'));
App::import('Vendor', 'CronDayOfMonthField', array('file' => 'Cron'  . DS . 'DayOfMonthField.php'));
App::import('Vendor', 'CronDayOfWeekField', array('file' => 'Cron'  . DS . 'DayOfWeekField.php'));
App::import('Vendor', 'CronHoursField', array('file' => 'Cron'  . DS . 'HoursField.php'));
App::import('Vendor', 'CronMinutesField', array('file' => 'Cron'  . DS . 'MinutesField.php'));
App::import('Vendor', 'CronMonthField', array('file' => 'Cron'  . DS . 'MonthField.php'));
App::import('Vendor', 'CronYearField', array('file' => 'Cron'  . DS . 'YearField.php'));

class JobsShell extends AppShell {

    public $uses = array('Application','CronLog');

    public function main() {
        $apps = $this->Application->find("all",
            array(
                "recursive" => -1
            )
        );
        foreach($apps as $app) {
            $pluginName = Inflector::camelize($app['Application']['request_uri']);
            if(file_exists(APP. DS . "Plugin" . DS . $pluginName . DS . "Controller" . DS . "Component". DS . $pluginName."CronJobComponent.php")){
                App::uses($pluginName."CronJobComponent", $pluginName . '.Controller/Component');
                $Collection = new ComponentCollection();
                $component_name = $pluginName."CronJobComponent";
                $jobComponent = new $component_name($Collection);
                $jobs = $jobComponent->main();
                if(count($jobs)) {
                    foreach ($jobs as $job) {
                        $cron = Cron\CronExpression::factory($job['time']);
                        if ($cron->isDue()) {
                            $function_name = $job['call'];
                            $newLog = array(
                                "id" => NULL,
                                "application" => $app['Application']['name'],
                                "function" => $function_name,
                                "error" => FALSE,
                                "success" => FALSE
                            );
                            try {
                                if (method_exists($jobComponent, $function_name)) {
                                    $jobComponent->$function_name();
                                    $newLog['success'] = TRUE;
                                } else {
                                    $newLog['error'] = TRUE;
                                    $newLog['data'] = "La funzione non esiste";
                                }
                            } catch (Exception $e) {
                                $newLog['error'] = TRUE;
                                $newLog['data'] = $e->getMessage();
                            }
                            $this->CronLog->create();
                            $this->CronLog->save($newLog);
                        }
                    }
                }
            }
        }
    }

}