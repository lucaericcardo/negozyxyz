<?php
class ImportShell extends AppShell {

    private $user               = array();
    private $shop               = array();
    private $shop_taxes         = array();
    private $payments           = array();
    private $shipping           = array();
    private $product_categories = array();
    private $products           = array();
    private $tags               = array();

    public $uses = array(
        "User",
        "UserAddressShipping",
        "UserImage",
        "Shop",
        "ShopLogo",
        "ShopCover",
        "Domain",
        "ShopTax",
        "Payment",
        "ShippingGroup",
        "Shipping",
        "ProductCategory",
        "ProductCategoryContent",
        "ProductCategoryImage",
        "Product",
        "ProductContent",
        "ProductImage",
        "Tag",
        "CategoryPage",
        "CategoryPageContent",
        "Page",
        "PageContent",
        "PageImage",
        "ShopLanguage"
    );

    public function main()
    {
        App::uses('HttpSocket', 'Network/Http');
        $HttpSocket = new HttpSocket();
        $users = $HttpSocket->get('http://my.negozy.com/export/export_users.json');
        $users = json_decode($users,true);
        foreach($users['users'] as $key => $user) {
            $results = $HttpSocket->get('http://my.negozy.com/export/export_shop/'.$user.'.json');
            $result = json_decode($results, true);
            $this->_saveUser($result['user']);
            if (isset($result['shop']) && !empty($result['shop'])) {
                $this->_saveShop($result['shop']);
                $this->_saveShopLanguage();
                $this->_saveShopTaxes($result['shop_taxes']);
                $this->_savePaymets($result['payment']);
                $this->_saveShipping($result['shipping']);
                $this->_saveProductCategories($result['collections']);
                $this->_saveTags($result['tags']);
                $this->_saveProducts($result['products']);
                $this->_savePageCategories($result['shop']);
                $this->_savePageCategoriesTerms($result['shop']);

                /**
                * Reset
                * */
                $this->user                 = array();
                $this->shop                 = array();
                $this->shop_taxes           = array();
                $this->payments             = array();
                $this->shipping             = array();
                $this->product_categories   = array();
                $this->products             = array();
                $this->tags                 = array();

                echo "Ho importato lo user ".$user ." ed il suo shop" . "\n";

            } else {
                echo "Ho importato lo user ".$user ." senza shop" . "\n";
            }
        }
    }

    private function _saveUser($user){
        $tmp_user = $user['User'];
        $tmp_user['id'] = NULL;
        $this->User->create();
        if($this->User->save($tmp_user)) {
            $tmp_user_address = $user['UserAddress'];
            $tmp_user_address['id'] = NULL;
            $tmp_user_address['user_id'] = $this->User->id;
            $tmp_user_address['address_type'] = "shipping";
            $this->UserAddressShipping->save($tmp_user_address);

            foreach ($user['UserImage'] as $tmp_image) {
                $tmp_image['id'] = NULL;
                $tmp_image['user_id'] = $this->User->id;
                $this->UserImage->save($tmp_image);
            }

            $this->user = array(
                "new_id" => $this->User->id,
                "old_id" => $user['User']['id']
            );
        } else {
            var_dump($this->User->validationErrors);
        }

    }

    private function _saveShop($shop){
        $tmp_shop = $shop['Shop'];
        $tmp_shop['id']             = NULL;
        $tmp_shop['user_id']        = $this->user['new_id'];
        $tmp_shop['language_id']    = 63;
        $tmp_shop['theme_id']       = 2;
        $this->Shop->create();
        if($this->Shop->save($tmp_shop)){
            $tmp_shop_logo = (isset($shop['ShopLogo']) && isset($shop['ShopLogo'][0])) ? $shop['ShopLogo'][0] : null;
            if(isset($tmp_shop_logo)){
                $tmp_shop_logo['id']        = NULL;
                $tmp_shop_logo['shop_id']   = $this->Shop->id;
                $this->ShopLogo->create();
                $this->ShopLogo->save($tmp_shop_logo);
            }
            $tmp_shop_cover = (isset($shop['ShopCover']) && isset($shop['ShopCover'][0])) ? $shop['ShopCover'][0]: null;
            if(isset($tmp_shop_cover)){
                $tmp_shop_cover['id']       = NULL;
                $tmp_shop_cover['shop_id']  = $this->Shop->id;
                $this->ShopCover->create();
                $this->ShopCover->save($tmp_shop_cover);
            }

            $tmp_shop_domains = $shop['Domain'];
            if(count($tmp_shop_domains)){
                foreach($tmp_shop_domains as $tmp_shop_domain) {
                    $tmp_shop_domain['id']      = NULL;
                    $tmp_shop_domain['shop_id'] = $this->Shop->id;
                    $this->Domain->create();
                    $this->Domain->save($tmp_shop_domain);
                }
            }

            $this->shop = array(
                "new_id"    => $this->Shop->id,
                "old_id"    => $shop['Shop']['id']
            );
        } else {
            var_dump($this->Shop->validationErrors);
            die();
        }
    }

    private function _saveShopLanguage(){
        $tmp_shop_language = array(
            "id"            => NULL,
            "shop_id"       => $this->shop['new_id'],
            "language_id"   => 63,
            "order"         => 0
        );
        $this->ShopLanguage->create();
        $this->ShopLanguage->save($tmp_shop_language);
    }

    private function _saveShopTaxes($shop_taxes){
        foreach($shop_taxes as $shop_tax) {
            $tmp_shop_tax = $shop_tax;
            $tmp_shop_tax['id'] = NULL;
            $tmp_shop_tax['shop_id'] = $this->shop['new_id'];
            $this->ShopTax->create();
            if($this->ShopTax->save($tmp_shop_tax)){
                $this->shop_taxes[$shop_tax['id']] = array(
                    "new_id" => $this->ShopTax->id,
                    "old_id" => $shop_tax['id']
                );
            }
        }
    }

    private function _savePaymets($payments){
        if(count($payments)) {
            foreach ($payments as $payment) {
                $tmp_payment = $payment;
                $tmp_payment['id'] = NULL;
                $tmp_payment['shop_id'] = $this->shop['new_id'];
                $this->Payment->create();
                if ($this->Payment->save($tmp_payment)) {
                    $this->payments[$payment['id']] = array(
                        "new_id" => $this->Payment->id,
                        "old_id" => $payment['id']
                    );
                }
            }
        }
    }

    private function _saveShipping($shipping){
        foreach($shipping as $shipment) {
            $tmp_shipping_group = array(
                "id"        => NULL,
                "name"      => $shipment['name'],
                "shop_id"   => $this->shop['new_id']
            );
            $this->ShippingGroup->create();
            if($this->ShippingGroup->save($tmp_shipping_group)) {
                $shop_tax_id = NULL;
                if (isset($shipment['shop_tax_id']) && isset($this->shop_taxes[$shipment['shop_tax_id']])) {
                    $shop_tax_id = $this->shop_taxes[$shipment['shop_tax_id']]['new_id'];
                }
                $tmp_shipping['Shipping'] = array(
                    "id"                => NULL,
                    "price"             => $shipment['price'],
                    "additional_price"  => $shipment['price'],
                    "start_day"         => $shipment['start_day'],
                    "end_day"           => $shipment['end_day'],
                    "shop_tax_id"       => $shop_tax_id,
                    "shipping_group_id" => $this->ShippingGroup->id,
                    "country_type"      => 1
                );
                $tmp_shipping['Country']['Country'][] = 115;
                $this->Shipping->create();
                $this->Shipping->saveAll($tmp_shipping);
                $this->shipping[] = array(
                    "old_id" => $shipment['id'],
                    "new_id" => $this->ShippingGroup->id
                );
            }
        }
    }

    private function _saveProductCategories($collections){
        $order = 1;
        foreach($collections as $collection)
        {
            $tmp_collection = array(
                "id"        => NULL,
                "shop_id"   => $this->shop['new_id']
            );
            $this->ProductCategory->create();
            if($this->ProductCategory->save($tmp_collection)) {
                $uri = Inflector::slug($collection['content']['name'], "-");
                if ($this->_ckUniqueURLCategory($uri)) {
                    $uri .= "-" . time();
                }
                $tmp_collection_content = array(
                    "id"                    => NULL,
                    "product_category_id"   => $this->ProductCategory->id,
                    "language_id"           => 63,
                    "title"                 => $collection['content']['name'],
                    "description"           => $collection['content']['description'],
                    "uri"                   => $uri,
                    "order"                 => $order
                );
                $this->ProductCategoryContent->create();
                $this->ProductCategoryContent->save($tmp_collection_content);
                if(isset($collection['images']) && count($collection['images'])) {
                    foreach ($collection['images'] as $image) {
                        $tmp_img = $image;
                        $tmp_img['id'] = NULL;
                        $tmp_img['product_category_id'] = $this->ProductCategory->id;
                        $this->ProductCategoryImage->create();
                        $this->ProductCategoryImage->save($tmp_img);
                    }
                }

                $this->product_categories[$collection['id']] = array(
                    "new_id" => $this->ProductCategory->id,
                    "old_id" => $collection['id']
                );
                $order++;
            }
        }
    }

    private function _saveProducts($products){
        foreach($products as $product) {
            $product_category_id = null;
            $shop_tax_id = null;
            if (isset($product['collection']) && count($product['collection'])) {
                $tmp_product_category = $product['collection'][0];
                if (isset($tmp_product_category) && isset($this->product_categories[$tmp_product_category])) {
                    $product_category_id = $this->product_categories[$tmp_product_category]['new_id'];
                }
            }

            if (isset($product['shop_tax_id']) && isset($this->shop_taxes[$product['shop_tax_id']])) {
                $shop_tax_id = $this->shop_taxes[$product['shop_tax_id']]['new_id'];
            }
            $shipping_group_id = (isset($this->shipping[0])) ? $this->shipping[0]['new_id'] : null;
            $tmp_product['Product'] = array(
                "id"                    => NULL,
                "price"                 => $product['price'],
                "quantity"              => $product['quantity'],
                "type_quantity"         => $product['type_quantity'],
                "deleted"               => $product['deleted'],
                "published"             => $product['published'],
                "product_category_id"   => $product_category_id,
                "shop_tax_id"           => $shop_tax_id,
                "shipping_group_id"     => $shipping_group_id,
                "shop_id"               => $this->shop['new_id']
            );
            $tmp_product['Tag']['Tag'] = array();
            if(count($product['tags'])) {
                foreach ($product['tags'] as $tag) {
                    if (isset($this->tags[$tag])) {
                        $tag_id = $this->tags[$tag]['new_id'];
                        $tmp_product['Tag']['Tag'][] = $tag_id;
                    }
                }
            }
            $this->Product->create();
            if($this->Product->save($tmp_product)) {
                $tmp_product_content = array(
                    "id"            => null,
                    "product_id"    => $this->Product->id,
                    "language_id"   => 63,
                    "name"          => $product['content']['name'],
                    "description"   => $product['content']['description'],
                    "uri"           => $product['content']['url']
                );
                $this->ProductContent->create();
                $this->ProductContent->save($tmp_product_content);
                foreach ($product['images'] as $image) {
                    $tmp_img = $image;
                    $tmp_img['id'] = NULL;
                    $tmp_img['product_id'] = $this->Product->id;
                    $this->ProductImage->create();
                    $this->ProductImage->save($tmp_img);
                }
            }

        }
    }

    private function _saveTags($tags){
        foreach($tags as $tag) {
            $tmp_tag = $tag;
            $tmp_tag['id']      = null;
            $tmp_tag['shop_id'] = $this->shop['new_id'];
            $this->Tag->create();
            if($this->Tag->save($tmp_tag)) {
                $this->tags[$tag['id']] = array(
                    "new_id" => $this->Tag->id,
                    "old_id" => $tag['id']
                );
            }
        }
    }

    private function _ckUniqueURLCategory($url = NULL){
        $count_url = false;
        if(isset($url) && !empty($url)) {
            $count_url = $this->ProductCategoryContent->find("count",array(
                "conditions" => array(
                    "ProductCategoryContent.uri" => $url
                ),
                "joins" => array(
                    array(
                        'table' => 'product_categories',
                        'alias' => 'ProductCategory',
                        'type' => 'RIGHT',
                        'conditions' => array(
                            'ProductCategory.id = ProductCategoryContent.product_category_id',
                            'ProductCategory.shop_id' => $this->shop['new_id']
                        )
                    )
                ),
                "recursive" => -1
            ));
        }
        return $count_url;
    }

    private function _savePageCategories($shop){
        $default_category = array(
            "id"        => null,
            "shop_id"   => $this->shop['new_id'],
            "type"      => "default",
            "default"   => true,
            "shop_id"   => $this->shop['new_id']
        );
        $this->CategoryPage->create();
        $this->CategoryPage->save($default_category);
        $default_category_content = array(
            "id"                => null,
            "title"             => "Default",
            "uri"               => "default",
            "language_id"       => 63,
            "category_page_id"  => $this->CategoryPage->id
        );
        $this->CategoryPageContent->create();
        $this->CategoryPageContent->save($default_category_content);
        $this->_saveHomePage($this->CategoryPage->id,$shop);
        $this->_saveAbout($this->CategoryPage->id,$shop);
        $this->_saveContact($this->CategoryPage->id);
        $this->_saveProductsPage($this->CategoryPage->id);
        $this->_saveCheckoutPage();
    }

    private function _savePageCategoriesTerms($shop){
        $default_category = array(
            "id"        => null,
            "shop_id"   => $this->shop['new_id'],
            "type"      => "terms",
            "default"   => true,
            "shop_id"   => $this->shop['new_id']
        );
        $this->CategoryPage->create();
        $this->CategoryPage->save($default_category);
        $term_category_content = array(
            "id"                => null,
            "title"             => "Termini e condizioni",
            "uri"               => "termini-e-condizioni",
            "language_id"       => 63,
            "category_page_id"  => $this->CategoryPage->id
        );
        $this->CategoryPageContent->create();
        $this->CategoryPageContent->save($term_category_content);
        $order = 1;
        foreach($shop['ShopTerm'] as $term) {
            $this->_saveTerm($this->CategoryPage->id,$term,$order);
            $order++;
        }
    }

    private function _saveHomePage($default_category_id,$shop){
        $home_page = array(
            "id"                => NULL,
            "default"           => true,
            "category_page_id"  => $default_category_id,
            "type"              => "home",
            "shop_id"           => $this->shop['new_id'],
            "order"             => 1,
            "published"         => 1
        );
        $this->Page->create();
        $this->Page->save($home_page);
        $content = array(
            "id"        => null,
            "page_id"   => $this->Page->id,
            "lang_id"   => 63,
            "title"     => "Home",
            "uri"       => "home",
            "html"      => $shop['Shop']['description']
        );
        $this->PageContent->create();
        $this->PageContent->save($content);
        if(count($shop['ShopImage'])) {
            foreach($shop['ShopImage'] as $image) {
                $tmp_img = $image;
                $tmp_img['id'] = NULL;
                $tmp_img['page_id'] = $this->Page->id;
                $this->PageImage->create();
                $this->PageImage->save($tmp_img);
            }
        }
    }

    private function _saveAbout($default_category_id,$shop){
        $about = array(
            "id"                => NULL,
            "category_page_id"  => $default_category_id,
            "shop_id"           => $this->shop['new_id'],
            "order"             => 2,
            "published"         => 1
        );
        $this->Page->save($about);
        $content = array(
            "id"        => null,
            "page_id"   => $this->Page->id,
            "lang_id"   => 63,
            "title"     => "Chi siamo",
            "uri"       => "chi-siamo",
            "html"      => $shop['Shop']['about_us']
        );
        $this->PageContent->save($content);
    }

    private function _saveContact($default_category_id){
        $contact = array(
            "id"                => NULL,
            "default"           => true,
            "category_page_id"  => $default_category_id,
            "type"              => "contact",
            "shop_id"           => $this->shop['new_id'],
            "order"             => 3,
            "published"         => 1
        );
        $this->Page->save($contact);
        $content = array(
            "id"        => null,
            "page_id"   => $this->Page->id,
            "lang_id"   => 63,
            "title"     => "Contatti",
            "uri"       => "contatti",
        );
        $this->PageContent->save($content);
    }

    private function _saveProductsPage($default_category_id){
        $contact = array(
            "id"                => NULL,
            "default"           => true,
            "category_page_id"  => $default_category_id,
            "type"              => "products",
            "shop_id"           => $this->shop['new_id'],
            "order"             => 4,
            "published"         => 1
        );
        $this->Page->save($contact);
        $content = array(
            "id"        => null,
            "page_id"   => $this->Page->id,
            "lang_id"   => 63,
            "title"     => "Prodotti",
            "uri"       => "prodotti",
        );
        $this->PageContent->save($content);
    }

    private function _saveCheckoutPage(){
        $contact = array(
            "id"                => NULL,
            "default"           => true,
            "category_page_id"  => null,
            "type"              => "checkout",
            "shop_id"           => $this->shop['new_id'],
            "order"             => 5,
            "published"         => 1
        );
        $this->Page->save($contact);
        $content = array(
            "id"        => null,
            "page_id"   => $this->Page->id,
            "lang_id"   => 63,
            "title"     => "Checkout",
            "uri"       => "checkout",
        );
        $this->PageContent->save($content);
    }

    private function _saveTerm($term_category_id,$term,$order){
        if($term['text']) {
            $title = "Termini e condizioni";
            if($term['type'] == "cancellationPolicy") {
                $title = "Policy di cancellazione";
            } else if($term['type'] == "privacy") {
                $title = "Privacy";
            }
            $contact = array(
                "id"                => NULL,
                "default"           => true,
                "category_page_id"  => $term_category_id,
                "type"              => "term",
                "shop_id"           => $this->shop['new_id'],
                "order"             => $order,
                "published"         => 1
            );
            $this->Page->save($contact);
            $content = array(
                "id" => null,
                "page_id" => $this->Page->id,
                "lang_id" => 63,
                "title" => $title,
                "uri" => Inflector::slug($title, "-"),
                "html" => $term['text']
            );
            $this->PageContent->save($content);
        }
    }

}