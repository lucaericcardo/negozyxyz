<?php
App::uses('CakeEvent', 'Event');
class Shipping extends AppModel {

    public $useTable = "shipments";

    public $actsAs = array('Containable');

    public $hasAndBelongsToMany = array(
        'Country' => array(
            'className' => 'Country',
            'foreignKey' => 'shipping_id',
            'associationForeignKey' => 'country_id',
        )
    );

    public $belongsTo = array(
        "ShopTax" => array(
            'className' => 'ShopTax'
        )
    );

    /*
    public function afterSave($created,$options= array()) {
        if($created) {
            $event = new CakeEvent('Model.Shipping.add', $this, array(
                'type' => "Metodo di spedizione ".$this->data['Shipping']['name']." aggiunto con successo",
            ));
            $this->getEventManager()->dispatch($event);
        } else {
            $event = new CakeEvent('Model.Shipping.update', $this, array(
                'type' => "Metodo di spedizione ".$this->data['Shipping']['name']." modificato con successo",
            ));
            $this->getEventManager()->dispatch($event);
        }
    }*/

    /*
    public function afterDelete() {
        $event = new CakeEvent('Model.Shipping.delete', $this, array(
            'type' => "Metodo di spedizione eliminato con successo"
        ));
        $this->getEventManager()->dispatch($event);
    }
    */

}