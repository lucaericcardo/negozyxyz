<?php
class Payment extends AppModel {

    public function afterSave($created,$options= array()) {
        if($created) {
            $event = new CakeEvent('Model.Payment.add', $this, array(
                'type' => "Metodo di pagamento ".$this->data['Payment']['name']." aggiunta con successo",
            ));
            $this->getEventManager()->dispatch($event);
        } else {
            $event = new CakeEvent('Model.Payment.update', $this, array(
                'type' => "Metodo di pagamento ".$this->data['Payment']['name']." modificata con successo",
            ));
            $this->getEventManager()->dispatch($event);
        }
    }

    public function afterDelete() {
        $event = new CakeEvent('Model.Payment.delete', $this, array(
            'type' => "Metodo di pagamento eliminato con successo"
        ));
        $this->getEventManager()->dispatch($event);
    }

}