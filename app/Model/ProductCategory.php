<?php
class ProductCategory extends AppModel {

    public  $actsAs = array('Containable');
    private $graphic_tree = array();
    private $breadcrumb_tree = array();

    public  $hasMany = array(
        "ProductCategoryContent" => array(
            'className' => 'ProductCategoryContent',
            'dependent' => true
        ),
        "Products" => array(
            'className'     => 'Products',
            'dependent'     => false
        ),
        "Children" => array(
            'className'     => 'ProductCategory',
            'foreignKey'    => 'parent_id',
            'dependent'     => true
        )
    );

    public $hasOne = array(
        'ProductCategoryImage'
    );

    public $belongsTo = array(
        'Parent'=>array(
            'className'     =>'ProductCategory',
            'foreignKey'    =>'parent_id',
            'counterCache'  => 'children_count'
        ),
        "Shop" => array(
            'className' => 'Shop',
            'counterCache' => true
        )

    );

    public function get_array_tree($shop_id = NULL,$language_id=NULL){
        $data = $this->load_data($shop_id,$language_id);
        return $this->build_array_tree($data,null);
    }

    public function get_graphical_tree($shop_id = NULL,$language_id = NULL){
        $data = $this->load_data($shop_id,$language_id);
        $this->build_graphical_tree($data,null,0);
        return $this->graphic_tree;
    }

    public function get_breadcrumb_tree($shop_id = NULL,$language_id = NULL,$parent_id = NULL){
        $data = $this->load_data($shop_id,$language_id);
        $this->build_array_breadcrumb($data,$parent_id);
        $return = array_reverse($this->breadcrumb_tree);
        array_unshift($return, array(
            "title" => "Le tue collezioni",
            "url"   => "#/collections"
        ));
        return $return;
    }

    private function load_data($shop_id = NULL,$language_id = NULL) {
        return $this->find("all",array(
            "conditions" => array(
                "shop_id" => $shop_id
            ),
            "fields" => array(
                "id",
                "parent_id"
            ),
            "contain" => array(
                "ProductCategoryContent" => array(
                    "conditions" => array(
                        "language_id" => $language_id
                    )
                )
            ),
            "order" => "parent_id ASC"
        ));
    }

    private function build_array_tree($categories, $parent_id = null) {
        $result = array();
        foreach ($categories as $category) {
            $name = (isset($category['ProductCategoryContent'][0]['title'])) ? $category['ProductCategoryContent'][0]['title'] : '';
            if ($category['ProductCategory']['parent_id'] == $parent_id) {
                $result[] = array(
                    "id"            => $category['ProductCategory']['id'],
                    "name"          => $name,
                    "categories"    => $this->build_array_tree($categories,$category['ProductCategory']['id'])
                );
            }
        }
        if (count($result) > 0){
            return $result;
        }
        return null;
    }

    private function build_graphical_tree($categories, $parent_id=null, $level=0) {
        foreach ($categories as $category) {
            $name = (isset($category['ProductCategoryContent'][0]['title'])) ? $category['ProductCategoryContent'][0]['title'] : '';
            if ($category['ProductCategory']['parent_id'] ==  $parent_id) {
                $this->graphic_tree[] = array(
                    "id"    => $category['ProductCategory']['id'],
                    "name"  => str_repeat("-", $level)." ".$name
                );
                $this->build_graphical_tree($categories, $category['ProductCategory']['id'], $level+1);
            }
        }
    }

    private function build_array_breadcrumb($categories, $parent_id = null) {
        if($parent_id) {
            foreach ($categories as $category) {
                $name = (isset($category['ProductCategoryContent'][0]['title'])) ? $category['ProductCategoryContent'][0]['title'] : '';
                if (isset($parent_id) && ($category['ProductCategory']['id'] == $parent_id)) {
                    $this->breadcrumb_tree[] = array(
                        "title" => $name,
                        "url"   => "#/collections/" . $category['ProductCategory']['id']
                    );
                    $this->build_array_breadcrumb($categories, $category['ProductCategory']['parent_id']);
                }
            }
        }
    }

}