<?php
class Subcategory extends AppModel {

    public $belongsTo = array(
        'Category'
    );

    public $hasMany = array(
        'Product' => array(
            "class" => 'Product'
        )
    );

}