<?php
class AppCategory extends AppModel {
    public $actsAs = array('Containable');
    public $hasMany = array(
        'Application' => array(
            'className' => 'Application',
            'foreignKey' => 'category_id'
        )
    );
}