<?php
class ThemeSite extends AppModel{
    public $useTable = "themes";
    public $actsAs = array('Containable');
    public $hasOne = array(
        "ThemeIcon" => array(
            'className' => 'ThemeIcon'
        )
    );
    public $hasMany = array(
        "ThemeScreenshot" => array(
            'className' => 'ThemeScreenshot'
        )
    );

}