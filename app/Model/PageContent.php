<?php
class PageContent extends AppModel {

    public $virtualFields = array(
        "timestamp_created" => "extract(epoch from PageContent.created at time zone 'Europe/Rome') * 1000"
    );

    public $belongsTo = array(
        'Language' => array(
            'className' => 'Language',
            'foreignKey' => 'lang_id'
        ),
        'Page' => array(
            'className' => 'Page',
            'foreignKey' => 'page_id'
        ),
    );
}