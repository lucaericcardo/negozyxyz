<?php
class UserImage extends  AppModel {

    public $virtualFields = array(
        "image" => "CONCAT('http://cdn.negozy.com/images/users/', UserImage.path,'profile_', UserImage.name,'.',UserImage.ext)"
    );

    var $belongsTo = array(
        'User' => array(
            'className' => 'User'
        )
    );

}