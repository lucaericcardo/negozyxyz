<?php
class ShopTax extends Model {

    public $actsAs = array('Containable');

    public $validate = array(
        'name' => array(
            'rule' => 'notEmpty',
            'required' => true
        ),
        'tax' => array(
            "notEmpty" => array(
                'rule' => 'notEmpty',
                'required' => true
            )
        )
    );

    public function afterSave($created,$options= array()) {
        if($created) {
            $event = new CakeEvent('Model.ShopTax.add', $this, array(
                'type' => "Imposta ".$this->data['ShopTax']['label']." aggiunta con successo",
            ));
            $this->getEventManager()->dispatch($event);
        } else {
            $event = new CakeEvent('Model.ShopTax.update', $this, array(
                'type' => "Imposta ".$this->data['ShopTax']['label']." modificata con successo",
            ));
            $this->getEventManager()->dispatch($event);
        }
    }

    public function afterDelete() {
        $event = new CakeEvent('Model.ShopTax.delete', $this, array(
            'type' => "Imposta di pagamento eliminato con successo"
        ));
        $this->getEventManager()->dispatch($event);
    }

}
