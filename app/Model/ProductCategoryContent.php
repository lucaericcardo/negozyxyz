<?php
class ProductCategoryContent extends AppModel {
    public $virtualFields = array(
        "timestamp_created" => "extract(epoch from ProductCategoryContent.created at time zone 'Europe/Rome') * 1000"
    );
}