<?php
class Page extends AppModel {

    public $actsAs = array('Containable');

    public $virtualFields = array(
        "timestamp_created" => "extract(epoch from Page.created at time zone 'Europe/Rome') * 1000"
    );

    public $belongsTo = array(
        "Shop" => array(
            'className' => 'Shop',
        ),
        "CategoryPage" => array(
            'className'     => 'CategoryPage',
            'counterCache'  => true
        )
    );

    public $hasMany = array(
        "PageContent" => array(
            'dependent' => true
        ),
        "PageImage" => array(
            'dependent' => true
        )
    );
}