<?php
class ProductImage extends AppModel{

    public $virtualFields = array(
        "image" => "CONCAT('http://cdn.negozy.com/images/products/', ProductImage.path,'image_', ProductImage.name,'.',ProductImage.ext)",
        "thumb" => "CONCAT('http://cdn.negozy.com/images/products/', ProductImage.path,'thumb_', ProductImage.name,'.',ProductImage.ext)"
    );

}