<?php
App::uses('SimplePasswordHasher', 'Controller/Component/Auth');
class User extends AppModel {

    public $actsAs = array('Containable');

    public $virtualFields = array(
        "fullname" => "CONCAT(User.first_name,' ', User.last_name)",
        "timestamp_created" => "extract(epoch from User.created at time zone 'Europe/Rome') * 1000"
    );


    public $validate = array(
        'first_name' => array(
            'rule' => 'notEmpty',
            'required' => true,
            'message' => 'Inserisci il tuo nome'
        ),
        'last_name' => array(
            'rule' => 'notEmpty',
            'required' => true,
            'message' => 'Inserisci il tuo cognome'
        ),
        /*'password' => array(
            'rule' => 'alphaNumeric',
            //'required' => false,
            'message' => 'Inserisci la password'
        ),*/
        'email' => array(
            'Email' => array(
                'rule' => 'email',
                'required' => true,
                'message' => 'Inserci una mail valida'
            ),
            'Unique' => array(
                'rule' => 'isUnique',
                'message' => 'Email già registrata'
            )
        )
    );

    public $hasOne = array(
        "UserAddressShipping" => array(
            'className' => 'UserAddressShipping'
        ),
        "UserAddressBilling" => array(
            'className' => 'UserAddressBilling'
        )
    );

    public $hasMany = array(
        "UserImage" => array(
            'className' => 'UserImage'
        ),
        "Shop" => array(
            'className' => 'Shop'
        )
    );

    public function beforeSave($options = array()) {
        if (!empty($this->data[$this->alias]['password'])) {
            $passwordHasher = new SimplePasswordHasher(array('hashType' => 'sha256'));
            $this->data[$this->alias]['password'] = $passwordHasher->hash(
                $this->data[$this->alias]['password']
            );
        }
        return true;
    }

}
?>