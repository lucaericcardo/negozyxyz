<?php
class ShippingGroup extends AppModel {
    public $actsAs = array('Containable');
    public $hasMany = array(
        "Shipping" => array(
            'className' => 'Shipping',
            'dependent' => true
        ),
        "Product" => array(
            'className' => 'Product'
        )
    );
}