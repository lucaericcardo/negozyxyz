<?php

class Application extends AppModel {
    public $actsAs = array('Containable');

    public $virtualFields = array(
        "timestamp" => "extract(epoch from Application.created at time zone 'Europe/Rome') * 1000"
    );


    public $belongsTo = array(
        "AppCompanyDeveloper" => array(
            'className' => 'AppCompanyDeveloper',
            'foreignKey' => 'company_id'
        ),
        "AppCategory" => array(
            'className' => 'AppCategory',
            'foreignKey' => 'category_id'
        )
    );
    public $hasOne = array(
        "AppIcon" => array(
            'className' => 'AppIcon'
        )
    );

    public $hasMany = array(
        "AppScreenshot" => array(
            'className' => 'AppScreenshot'
        ),
        "ShopAppInstalled" => array(
            'className' => 'ShopAppInstalled',
            'foreignKey' => 'app_id',
            'conditions' => array('ShopAppInstalled.deleted' => false),

        ),
    );
}