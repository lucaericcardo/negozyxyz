<?php
class Order extends AppModel {

    public $actsAs = array('Containable');

    public $virtualFields = array(
        "timestamp_created" => "extract(epoch from Order.created at time zone 'Europe/Rome') * 1000"
    );

    public $belongsTo = array(
        "Shop" => array(
            'className' => 'Shop',
            'counterCache' => true
        ),
        "User" => array(
            'className' => 'User'
        ),
        "OrderStatus" => array(
            'className' => 'OrderStatus'
        ),
        "Cart" => array(
            'className' => 'Cart'
        )
    );

    public $validate = array(
        'shop_id' => array(
            'rule' => 'notEmpty',
            'required' => true
        ),
        'user_id' => array(
            'rule' => 'notEmpty',
            'required' => true
        ),
        'total_amount' => array(
            'rule' => 'notEmpty',
            'required' => true
        ),
        'order_status_id' => array(
            'rule' => 'notEmpty',
            'required' => true
        ),
        'data' => array(
            'rule' => 'notEmpty',
            'required' => true
        )
    );

}