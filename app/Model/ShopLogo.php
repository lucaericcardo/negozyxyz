<?php
class ShopLogo extends AppModel {

    public $virtualFields = array(
        "image" => "CONCAT('http://cdn.negozy.com/images/shops/', ShopLogo.path,'logo_', ShopLogo.name,'.',ShopLogo.ext)"
    );

}