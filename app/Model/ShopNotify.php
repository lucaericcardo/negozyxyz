<?php
class ShopNotify extends AppModel {
    public $useDbConfig = 'mongo';

    public $belongsTo = array(
        'User' => array(
            'className' => 'User',
            'foreignKey' => 'receiver_id',
            'counterCache' => array(
                'shop_notify_count' => array('ShopNotify.visualized' => false)
            )
        )
    );

}