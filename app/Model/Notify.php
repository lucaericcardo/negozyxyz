<?php
class Notify extends AppModel {

    public $virtualFields = array(
        "timestamp" => "extract(epoch from Notify.created at time zone 'Europe/Rome') * 1000"
    );

    var $belongsTo = array(
        'Post' => array(
            'className' => 'Post'
        ),
        "User" => array(
            'className' => 'User',
            'counterCache' => array(
                'post_notify_count' => array('Notify.viewed' => false)
            )
        )
    );

}