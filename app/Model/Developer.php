<?php

class Developer extends AppModel {
    public $belongsTo = array(
        "User" => array(
            'className' => 'User'
        )
    );
}