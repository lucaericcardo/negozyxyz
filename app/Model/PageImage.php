<?php
class PageImage extends AppModel{

    public $virtualFields = array(
        "image" => "CONCAT('http://cdn.negozy.com/images/shops/', PageImage.path,'image_', PageImage.name,'.',PageImage.ext)",
    );

}