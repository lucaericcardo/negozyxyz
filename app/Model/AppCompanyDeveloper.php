<?php

class AppCompanyDeveloper extends AppModel {
    public $useTable = "app_company_developer";
    public $actsAs = array('Containable');
    public $hasOne = array(
        'AppCompanyIcon' => array(
            'className' => 'AppCompanyIcon',
            'foreignKey' => 'company_id',
        )
    );

    public $hasMany = array(
        'Application' => array(
            'className' => 'Application',
            'foreignKey' => 'company_id'
        )
    );
}