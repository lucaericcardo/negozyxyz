<?php
class ShopAppInstalled extends AppModel {

    public $actsAs = array('Containable');
    public $belongsTo = array(
        "Application" => array(
            'className' => 'Application',
            'foreignKey' => 'app_id',
            'counterCache' => 'installation_count',
            'counterScope' => array(
                'Application.deleted' => false,
                'ShopAppInstalled.deleted' => false
            )
        ),
        "Shop" => array(
            'className' => 'Shop'
        )
    );

}