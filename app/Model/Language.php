<?php
class Language extends AppModel {

    public $belongsTo = array(
        "Shop" => array(
            'className' => 'Shop',
        )
    );

    public $hasMany = array(
        "PageContent"
    );

}