<?php
class Cart extends AppModel {

    public $actsAs = array('Containable');

    public $belongsTo = array(
        "Shop" => array(
            'className' => 'Shop',
        )
    );

}