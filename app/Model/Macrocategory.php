<?php
class Macrocategory extends AppModel {
    public $actsAs = array('Containable');

    public $hasMany = array(
        'Category'
    );

}