<?php
class CategoryPageContent extends AppModel {
    public $virtualFields = array(
        "timestamp_created" => "extract(epoch from CategoryPageContent.created at time zone 'Europe/Rome') * 1000"
    );
}