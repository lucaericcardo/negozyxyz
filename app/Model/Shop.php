<?php
App::uses('CakeEvent', 'Event');
class Shop extends AppModel {

    public $actsAs = array('Containable');

    public $virtualFields = array("site_url" => "CONCAT('http://', Shop.url, '.negozy.com')");

    public $hasMany = array(
        "ShopLogo" => array(
            'className' => 'ShopLogo'
        ),
        "ShopCover" => array(
            'className' => 'ShopCover'
        ),
        "ShippingGroup" => array(
            'className' => 'ShippingGroup'
        ),
        "Payments" => array(
            'className' => 'Payments'
        ),
        "ShopTerm" => array(
            'className' => 'ShopTerm'
        ),
        "Domain" => array(
            'className' => 'Domain'
        )
    );

    public $belongsTo = array(
        "User" => array(
            'className' => 'User'
        ),
        "Macrocategory" => array(
            'className' => 'Macrocategory'
        )
    );

    public $validate = array(
        'name' => array(
            'rule' => 'notEmpty',
            'required' => true
        ),
        'url' => array(
            'Unique' => array(
                'rule' => 'isUnique'
            ),
            'NotEmty' => array(
                'rule' => 'notEmpty',
                'required' => true
            )
        ),
        'vat_number' => array(
            'NotEmty' => array(
                'rule' => 'notEmpty',
                'required' => true
            )
        ),
        'address' => array(
            'rule' => 'notEmpty',
            'required' => true
        ),
        'city' => array(
            'rule' => 'notEmpty',
            'required' => true
        ),
        'cap' => array(
            'rule' => 'numeric',
            'required' => true
        ),
        'state' => array(
            'rule' => 'notEmpty',
            'required' => true
        ),
        'province' => array(
            'rule' => 'notEmpty',
            'required' => true
        ),
        'business_name' => array(
            'rule' => 'notEmpty',
            'required' => true
        ),
        /*'macrocategory_id' => array(
            'rule' => 'notEmpty',
            'required' => true
        )*/
    );

    public function afterSave($created,$options= array()) {
        $event = new CakeEvent('Model.Shop.update', $this, array(
            'type' => "Dati negozio modificati con successo",
        ));
        $this->getEventManager()->dispatch($event);
    }

    public function getDefaultLanguage($shop_id) {
        $options = array(
            'conditions' => array('id' => $shop_id),
            'fields' => array('language_id'),
            'recursive' => -1
        );
        $language = $this->find('first', $options);
        return $language['Shop']['language_id'];
    }

}