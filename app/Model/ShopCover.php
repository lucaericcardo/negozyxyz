<?php
class ShopCover extends AppModel {

    public $virtualFields = array(
        "image" => "CONCAT('http://cdn.negozy.com/images/shops/', ShopCover.path,'cover_', ShopCover.name,'.',ShopCover.ext)"
    );

}