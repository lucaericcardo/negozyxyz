<?php
App::uses('CakeEvent', 'Event');
class Product extends AppModel {
    public $actsAs = array('Containable');

    public $virtualFields = array(
        "timestamp_updated" => "extract(epoch from Product.updated at time zone 'Europe/Rome') * 1000"
    );

    public $hasAndBelongsToMany = array(
        'Tag' => array(
            'className' => 'Tag'
        )
    );
    public $hasMany = array(
        "ProductImage" => array(
            'className' => 'ProductImage'
        ),
        "ProductContent" => array(
            'className' => 'ProductContent',
            'dependent' => true
        ),
    );
    public $belongsTo = array(
        "Shop" => array(
            'className' => 'Shop',
            'counterCache' => array(
                'product_count' => array('Product.deleted' => false)
            )
        ),
        "ShopTax" => array(
            'className' => 'ShopTax'
        ),
        "ShippingGroup" => array(
            'className'     => 'ShippingGroup',
            'counterCache'  => TRUE
        ),
        "ProductCategory" => array(
            'className'     => 'ProductCategory',
            'counterCache'  => TRUE
        )
    );
    public function afterSave($created,$options= array()) {
        /*if($created) {
            $event = new CakeEvent('Model.Product.add', $this, array(
                'type' => "Prodotto ".$this->data['Product']['name']." aggiunto con successo"
            ));
            $this->getEventManager()->dispatch($event);
        } else {
            if(isset($this->data['Product']['deleted'])) {
                $event = new CakeEvent('Model.Product.delete', $this, array(
                    'type' => "Prodotto ".$this->data['Product']['name']." eliminato con successo"
                ));
                $this->getEventManager()->dispatch($event);
            } else {
                $product = (isset($this->data['Product']['name'])) ? isset($this->data['Product']['name']) : isset($this->data['Product']['id']);

                $event = new CakeEvent('Model.Product.update', $this, array(
                    'type' => "Prodotto " . $product  . " modificato con successo"
                ));
                $this->getEventManager()->dispatch($event);
            }
        }*/
    }
}