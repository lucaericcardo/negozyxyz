<?php
class News extends AppModel {
    public $useTable = 'news';

    public $actsAs = array('Containable');

    public $virtualFields = array(
        "timestamp_created"     => "extract(epoch from News.created at time zone 'Europe/Rome') * 1000",
        "timestamp_from_date"   => "extract(epoch from News.from_date at time zone 'Europe/Rome') * 1000",
        "timestamp_to_date"     => "extract(epoch from News.to_date at time zone 'Europe/Rome') * 1000"
    );
}