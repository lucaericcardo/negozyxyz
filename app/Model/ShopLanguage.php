<?php
class ShopLanguage extends AppModel {
    public $actsAs = array('Containable');

    public $virtualFields = array(
        "timestamp_created" => "extract(epoch from ShopLanguage.created at time zone 'Europe/Rome') * 1000"
    );

    public $belongsTo = array(
        'Language' => array(
            'className' => 'Language'
        )
    );

    public function calcOrderLanguage($shop_id) {
        $return = NULL;
        $options = array(
            'conditions' => array(
                'shop_id' => $shop_id
            ),
            'fields' => array(
                'COALESCE(MAX("order"),-1)+1 as order'
            )
        );
        if($order = $this->find('first', $options)) {
            $return = $order[0]['order'];
        }
        return $return;
    }

    public function getShopLanguages($shop_id) {
        $options = array(
            'conditions' => array('shop_id' => $shop_id),
            'fields' => array('language_id'),
            'contain' => array(
                'Language' => array(
                    'fields' => array(
                        'Language.lang'
                    )
                )
            ),
            'recursive' => -1
        );

        return Hash::combine($this->find('all', $options), '{n}.ShopLanguage.language_id', '{n}.Language.lang');
    }
}