<?php
class Venture extends AppModel {

    public $actsAs = array('Containable');

    public $virtualFields = array(
        "timestamp_created"     => "extract(epoch from Venture.created at time zone 'Europe/Rome') * 1000",
        "timestamp_from_date"   => "extract(epoch from Venture.from_date at time zone 'Europe/Rome') * 1000",
        "timestamp_to_date"     => "extract(epoch from Venture.to_date at time zone 'Europe/Rome') * 1000"
    );

    public $hasAndBelongsToMany = array(
        'User' => array(
            'className' => 'User',
            'joinTable' => 'ventures_users'
        )
    );

}