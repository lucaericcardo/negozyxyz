<?php
class UserAddressBilling extends AppModel {

    public $actsAs = array('Containable');

    public $useTable = "user_address";

    public $belongsTo = array(
        "Country" => array(
            'className' => 'Country'
        )
    );

    public $validate = array(
        'first_name' => array(
            'rule' => 'notEmpty',
            'required' => true,
            'message' => 'Inserisci il tuo nome'
        ),
        'last_name' => array(
            'rule' => 'notEmpty',
            'required' => true,
            'message' => 'Inserisci il tuo cognome'
        ),
        'address' => array(
            'rule' => 'notEmpty',
            'required' => true,
            'message' => 'Inserisci il tuo indirizzo'
        ),
        'city' => array(
            'rule' => 'notEmpty',
            'required' => true,
            'message' => 'Inserisci la tua città'
        ),
        'postal_code' => array(
            'rule' => 'notEmpty',
            'required' => true,
            'message' => 'Inserisci il cap della tua città'
        ),
        'province' => array(
            'rule' => 'notEmpty',
            'required' => true,
            'message' => 'Inserisci la provincia della tua città'
        ),
        'country_id' => array(
            'rule' => 'notEmpty',
            'required' => true,
            'message' => 'Seleziona la tua nazionalità'
        ),
        'phone_number' => array(
            'rule' => 'notEmpty',
            'required' => true,
            'message' => 'Inserici il tuo numero di telefono'
        )
    );

    public function beforeSave($options = array()) {
        $this->data[$this->alias]['address_type'] = "billing";
        return true;
    }

}