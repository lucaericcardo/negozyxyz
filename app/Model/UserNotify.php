<?php
class UserNotify extends AppModel {
    public $useDbConfig = 'mongo';

    public $belongsTo = array(
        'User' => array(
            'className' => 'User',
            'foreignKey' => 'receiver_id',
            'counterCache' => array(
                'user_notify_count' => array('UserNotify.visualized' => false)
            )
        )
    );

}