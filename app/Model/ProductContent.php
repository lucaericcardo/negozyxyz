<?php
class ProductContent extends AppModel {
    public $virtualFields = array(
        "timestamp_created" => "extract(epoch from ProductContent.created at time zone 'Europe/Rome') * 1000"
    );
}